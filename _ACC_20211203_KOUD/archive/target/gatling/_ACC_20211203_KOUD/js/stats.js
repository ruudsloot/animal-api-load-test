var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "8099",
        "ok": "7437",
        "ko": "662"
    },
    "minResponseTime": {
        "total": "5",
        "ok": "5",
        "ko": "9"
    },
    "maxResponseTime": {
        "total": "61841",
        "ok": "61841",
        "ko": "60023"
    },
    "meanResponseTime": {
        "total": "10392",
        "ok": "7165",
        "ko": "46647"
    },
    "standardDeviation": {
        "total": "21372",
        "ok": "17811",
        "ko": "24344"
    },
    "percentiles1": {
        "total": "66",
        "ok": "61",
        "ko": "60000"
    },
    "percentiles2": {
        "total": "1354",
        "ok": "518",
        "ko": "60000"
    },
    "percentiles3": {
        "total": "60033",
        "ok": "60035",
        "ko": "60001"
    },
    "percentiles4": {
        "total": "60088",
        "ok": "60092",
        "ko": "60006"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 5567,
    "percentage": 69
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 278,
    "percentage": 3
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1592,
    "percentage": 20
},
    "group4": {
    "name": "failed",
    "count": 662,
    "percentage": 8
},
    "meanNumberOfRequestsPerSecond": {
        "total": "11.186",
        "ok": "10.272",
        "ko": "0.914"
    }
},
contents: {
"req_get-landing-pag-93979": {
        type: "REQUEST",
        name: "GET landing page",
path: "GET landing page",
pathFormatted: "req_get-landing-pag-93979",
stats: {
    "name": "GET landing page",
    "numberOfRequests": {
        "total": "675",
        "ok": "675",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "7",
        "ok": "7",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "486",
        "ok": "486",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "44",
        "ok": "44",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "47",
        "ok": "47",
        "ko": "-"
    },
    "percentiles1": {
        "total": "29",
        "ok": "29",
        "ko": "-"
    },
    "percentiles2": {
        "total": "55",
        "ok": "55",
        "ko": "-"
    },
    "percentiles3": {
        "total": "133",
        "ok": "133",
        "ko": "-"
    },
    "percentiles4": {
        "total": "228",
        "ok": "228",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 675,
    "percentage": 100
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.932",
        "ok": "0.932",
        "ko": "-"
    }
}
    },"req_get-database-05cff": {
        type: "REQUEST",
        name: "GET Database",
path: "GET Database",
pathFormatted: "req_get-database-05cff",
stats: {
    "name": "GET Database",
    "numberOfRequests": {
        "total": "675",
        "ok": "671",
        "ko": "4"
    },
    "minResponseTime": {
        "total": "13",
        "ok": "13",
        "ko": "20"
    },
    "maxResponseTime": {
        "total": "1499",
        "ok": "1499",
        "ko": "66"
    },
    "meanResponseTime": {
        "total": "84",
        "ok": "84",
        "ko": "36"
    },
    "standardDeviation": {
        "total": "125",
        "ok": "125",
        "ko": "18"
    },
    "percentiles1": {
        "total": "46",
        "ok": "47",
        "ko": "29"
    },
    "percentiles2": {
        "total": "85",
        "ok": "86",
        "ko": "39"
    },
    "percentiles3": {
        "total": "241",
        "ok": "243",
        "ko": "61"
    },
    "percentiles4": {
        "total": "691",
        "ok": "691",
        "ko": "65"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 656,
    "percentage": 97
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 13,
    "percentage": 2
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 2,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 4,
    "percentage": 1
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.932",
        "ok": "0.927",
        "ko": "0.006"
    }
}
    },"req_get-suppressed--91726": {
        type: "REQUEST",
        name: "GET suppressed-ui",
path: "GET suppressed-ui",
pathFormatted: "req_get-suppressed--91726",
stats: {
    "name": "GET suppressed-ui",
    "numberOfRequests": {
        "total": "675",
        "ok": "675",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "7",
        "ok": "7",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "655",
        "ok": "655",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "49",
        "ok": "49",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "71",
        "ok": "71",
        "ko": "-"
    },
    "percentiles1": {
        "total": "32",
        "ok": "32",
        "ko": "-"
    },
    "percentiles2": {
        "total": "51",
        "ok": "51",
        "ko": "-"
    },
    "percentiles3": {
        "total": "125",
        "ok": "125",
        "ko": "-"
    },
    "percentiles4": {
        "total": "418",
        "ok": "418",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 668,
    "percentage": 99
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 7,
    "percentage": 1
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.932",
        "ok": "0.932",
        "ko": "-"
    }
}
    },"req_post-sync-custo-6f7e3": {
        type: "REQUEST",
        name: "POST sync-customer-info",
path: "POST sync-customer-info",
pathFormatted: "req_post-sync-custo-6f7e3",
stats: {
    "name": "POST sync-customer-info",
    "numberOfRequests": {
        "total": "675",
        "ok": "522",
        "ko": "153"
    },
    "minResponseTime": {
        "total": "15",
        "ok": "276",
        "ko": "15"
    },
    "maxResponseTime": {
        "total": "60003",
        "ok": "59825",
        "ko": "60003"
    },
    "meanResponseTime": {
        "total": "21366",
        "ok": "17312",
        "ko": "35197"
    },
    "standardDeviation": {
        "total": "21666",
        "ok": "17124",
        "ko": "28692"
    },
    "percentiles1": {
        "total": "13149",
        "ok": "12064",
        "ko": "60000"
    },
    "percentiles2": {
        "total": "39831",
        "ok": "31802",
        "ko": "60000"
    },
    "percentiles3": {
        "total": "60000",
        "ok": "49656",
        "ko": "60001"
    },
    "percentiles4": {
        "total": "60001",
        "ok": "57507",
        "ko": "60001"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 10,
    "percentage": 1
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 39,
    "percentage": 6
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 473,
    "percentage": 70
},
    "group4": {
    "name": "failed",
    "count": 153,
    "percentage": 23
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.932",
        "ok": "0.721",
        "ko": "0.211"
    }
}
    },"req_get-customer-in-bfc70": {
        type: "REQUEST",
        name: "GET customer-info",
path: "GET customer-info",
pathFormatted: "req_get-customer-in-bfc70",
stats: {
    "name": "GET customer-info",
    "numberOfRequests": {
        "total": "675",
        "ok": "674",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "9",
        "ok": "9",
        "ko": "12"
    },
    "maxResponseTime": {
        "total": "954",
        "ok": "954",
        "ko": "12"
    },
    "meanResponseTime": {
        "total": "88",
        "ok": "88",
        "ko": "12"
    },
    "standardDeviation": {
        "total": "129",
        "ok": "130",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60",
        "ok": "60",
        "ko": "12"
    },
    "percentiles2": {
        "total": "91",
        "ok": "91",
        "ko": "12"
    },
    "percentiles3": {
        "total": "187",
        "ok": "187",
        "ko": "12"
    },
    "percentiles4": {
        "total": "866",
        "ok": "866",
        "ko": "12"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 655,
    "percentage": 97
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 19,
    "percentage": 3
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.932",
        "ok": "0.931",
        "ko": "0.001"
    }
}
    },"req_get-helixs-sync-9b1f7": {
        type: "REQUEST",
        name: "GET helixs-sync",
path: "GET helixs-sync",
pathFormatted: "req_get-helixs-sync-9b1f7",
stats: {
    "name": "GET helixs-sync",
    "numberOfRequests": {
        "total": "675",
        "ok": "669",
        "ko": "6"
    },
    "minResponseTime": {
        "total": "7",
        "ok": "7",
        "ko": "9"
    },
    "maxResponseTime": {
        "total": "905",
        "ok": "905",
        "ko": "104"
    },
    "meanResponseTime": {
        "total": "57",
        "ok": "57",
        "ko": "37"
    },
    "standardDeviation": {
        "total": "82",
        "ok": "82",
        "ko": "32"
    },
    "percentiles1": {
        "total": "36",
        "ok": "36",
        "ko": "24"
    },
    "percentiles2": {
        "total": "56",
        "ok": "56",
        "ko": "36"
    },
    "percentiles3": {
        "total": "177",
        "ok": "178",
        "ko": "88"
    },
    "percentiles4": {
        "total": "385",
        "ok": "385",
        "ko": "101"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 663,
    "percentage": 98
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 6,
    "percentage": 1
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 6,
    "percentage": 1
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.932",
        "ok": "0.924",
        "ko": "0.008"
    }
}
    },"req_post-sync-66165": {
        type: "REQUEST",
        name: "POST SYNC",
path: "POST SYNC",
pathFormatted: "req_post-sync-66165",
stats: {
    "name": "POST SYNC",
    "numberOfRequests": {
        "total": "675",
        "ok": "179",
        "ko": "496"
    },
    "minResponseTime": {
        "total": "11",
        "ok": "1411",
        "ko": "11"
    },
    "maxResponseTime": {
        "total": "60023",
        "ok": "59776",
        "ko": "60023"
    },
    "meanResponseTime": {
        "total": "45337",
        "ok": "28536",
        "ko": "51401"
    },
    "standardDeviation": {
        "total": "21904",
        "ok": "16417",
        "ko": "20422"
    },
    "percentiles1": {
        "total": "60000",
        "ok": "26209",
        "ko": "60000"
    },
    "percentiles2": {
        "total": "60000",
        "ok": "42989",
        "ko": "60000"
    },
    "percentiles3": {
        "total": "60001",
        "ok": "55817",
        "ko": "60001"
    },
    "percentiles4": {
        "total": "60006",
        "ok": "59581",
        "ko": "60007"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 179,
    "percentage": 27
},
    "group4": {
    "name": "failed",
    "count": 496,
    "percentage": 73
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.932",
        "ok": "0.247",
        "ko": "0.685"
    }
}
    },"req_get-all-docs--2-9867e": {
        type: "REQUEST",
        name: "GET ALL_DOCS #2",
path: "GET ALL_DOCS #2",
pathFormatted: "req_get-all-docs--2-9867e",
stats: {
    "name": "GET ALL_DOCS #2",
    "numberOfRequests": {
        "total": "675",
        "ok": "674",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "24",
        "ok": "24",
        "ko": "30"
    },
    "maxResponseTime": {
        "total": "17351",
        "ok": "17351",
        "ko": "30"
    },
    "meanResponseTime": {
        "total": "1280",
        "ok": "1281",
        "ko": "30"
    },
    "standardDeviation": {
        "total": "1658",
        "ok": "1659",
        "ko": "0"
    },
    "percentiles1": {
        "total": "757",
        "ok": "759",
        "ko": "30"
    },
    "percentiles2": {
        "total": "1547",
        "ok": "1548",
        "ko": "30"
    },
    "percentiles3": {
        "total": "4465",
        "ok": "4466",
        "ko": "30"
    },
    "percentiles4": {
        "total": "7430",
        "ok": "7432",
        "ko": "30"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 232,
    "percentage": 34
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 182,
    "percentage": 27
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 260,
    "percentage": 39
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.932",
        "ok": "0.931",
        "ko": "0.001"
    }
}
    },"req_get-info-43845": {
        type: "REQUEST",
        name: "GET INFO",
path: "GET INFO",
pathFormatted: "req_get-info-43845",
stats: {
    "name": "GET INFO",
    "numberOfRequests": {
        "total": "675",
        "ok": "675",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5",
        "ok": "5",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1676",
        "ok": "1676",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "48",
        "ok": "48",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "97",
        "ok": "97",
        "ko": "-"
    },
    "percentiles1": {
        "total": "27",
        "ok": "27",
        "ko": "-"
    },
    "percentiles2": {
        "total": "52",
        "ok": "52",
        "ko": "-"
    },
    "percentiles3": {
        "total": "131",
        "ok": "131",
        "ko": "-"
    },
    "percentiles4": {
        "total": "269",
        "ok": "269",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 671,
    "percentage": 99
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 2,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 2,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.932",
        "ok": "0.932",
        "ko": "-"
    }
}
    },"req_get-health-chec-caf6c": {
        type: "REQUEST",
        name: "GET HEALTH-CHECK",
path: "GET HEALTH-CHECK",
pathFormatted: "req_get-health-chec-caf6c",
stats: {
    "name": "GET HEALTH-CHECK",
    "numberOfRequests": {
        "total": "675",
        "ok": "675",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "50",
        "ok": "50",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1178",
        "ok": "1178",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "81",
        "ok": "81",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "78",
        "ok": "78",
        "ko": "-"
    },
    "percentiles1": {
        "total": "66",
        "ok": "66",
        "ko": "-"
    },
    "percentiles2": {
        "total": "80",
        "ok": "80",
        "ko": "-"
    },
    "percentiles3": {
        "total": "142",
        "ok": "142",
        "ko": "-"
    },
    "percentiles4": {
        "total": "228",
        "ok": "228",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 672,
    "percentage": 100
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 3,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.932",
        "ok": "0.932",
        "ko": "-"
    }
}
    },"req_get-database--2-e526d": {
        type: "REQUEST",
        name: "GET Database #2",
path: "GET Database #2",
pathFormatted: "req_get-database--2-e526d",
stats: {
    "name": "GET Database #2",
    "numberOfRequests": {
        "total": "675",
        "ok": "674",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "8",
        "ok": "8",
        "ko": "29"
    },
    "maxResponseTime": {
        "total": "843",
        "ok": "843",
        "ko": "29"
    },
    "meanResponseTime": {
        "total": "54",
        "ok": "54",
        "ko": "29"
    },
    "standardDeviation": {
        "total": "90",
        "ok": "90",
        "ko": "0"
    },
    "percentiles1": {
        "total": "32",
        "ok": "32",
        "ko": "29"
    },
    "percentiles2": {
        "total": "50",
        "ok": "50",
        "ko": "29"
    },
    "percentiles3": {
        "total": "142",
        "ok": "142",
        "ko": "29"
    },
    "percentiles4": {
        "total": "568",
        "ok": "570",
        "ko": "29"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 665,
    "percentage": 99
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 9,
    "percentage": 1
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.932",
        "ok": "0.931",
        "ko": "0.001"
    }
}
    },"req_changes-feed-4d6ae": {
        type: "REQUEST",
        name: "CHANGES FEED",
path: "CHANGES FEED",
pathFormatted: "req_changes-feed-4d6ae",
stats: {
    "name": "CHANGES FEED",
    "numberOfRequests": {
        "total": "674",
        "ok": "674",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "712",
        "ok": "712",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "61841",
        "ok": "61841",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "56286",
        "ok": "56286",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "12134",
        "ok": "12134",
        "ko": "-"
    },
    "percentiles1": {
        "total": "60038",
        "ok": "60038",
        "ko": "-"
    },
    "percentiles2": {
        "total": "60058",
        "ok": "60058",
        "ko": "-"
    },
    "percentiles3": {
        "total": "60134",
        "ok": "60134",
        "ko": "-"
    },
    "percentiles4": {
        "total": "60367",
        "ok": "60367",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 1,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 673,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.931",
        "ok": "0.931",
        "ko": "-"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
