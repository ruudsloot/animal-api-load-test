var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "4500",
        "ok": "4470",
        "ko": "30"
    },
    "minResponseTime": {
        "total": "8",
        "ok": "8",
        "ko": "11"
    },
    "maxResponseTime": {
        "total": "60313",
        "ok": "60313",
        "ko": "60011"
    },
    "meanResponseTime": {
        "total": "7443",
        "ok": "7117",
        "ko": "56005"
    },
    "standardDeviation": {
        "total": "18121",
        "ok": "17696",
        "ko": "14960"
    },
    "percentiles1": {
        "total": "66",
        "ok": "65",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "965",
        "ok": "916",
        "ko": "60004"
    },
    "percentiles3": {
        "total": "60032",
        "ok": "60032",
        "ko": "60010"
    },
    "percentiles4": {
        "total": "60081",
        "ok": "60081",
        "ko": "60011"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 3124,
    "percentage": 69
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 262,
    "percentage": 6
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1084,
    "percentage": 24
},
    "group4": {
    "name": "failed",
    "count": 30,
    "percentage": 1
},
    "meanNumberOfRequestsPerSecond": {
        "total": "8.272",
        "ok": "8.217",
        "ko": "0.055"
    }
},
contents: {
"req_get-landing-pag-93979": {
        type: "REQUEST",
        name: "GET landing page",
path: "GET landing page",
pathFormatted: "req_get-landing-pag-93979",
stats: {
    "name": "GET landing page",
    "numberOfRequests": {
        "total": "375",
        "ok": "375",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "8",
        "ok": "8",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "670",
        "ok": "670",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "52",
        "ok": "52",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "67",
        "ok": "67",
        "ko": "-"
    },
    "percentiles1": {
        "total": "36",
        "ok": "36",
        "ko": "-"
    },
    "percentiles2": {
        "total": "56",
        "ok": "56",
        "ko": "-"
    },
    "percentiles3": {
        "total": "166",
        "ok": "166",
        "ko": "-"
    },
    "percentiles4": {
        "total": "320",
        "ok": "320",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 373,
    "percentage": 99
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 2,
    "percentage": 1
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.689",
        "ok": "0.689",
        "ko": "-"
    }
}
    },"req_get-database-05cff": {
        type: "REQUEST",
        name: "GET Database",
path: "GET Database",
pathFormatted: "req_get-database-05cff",
stats: {
    "name": "GET Database",
    "numberOfRequests": {
        "total": "375",
        "ok": "375",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "15",
        "ok": "15",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "591",
        "ok": "591",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "70",
        "ok": "70",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "64",
        "ok": "64",
        "ko": "-"
    },
    "percentiles1": {
        "total": "53",
        "ok": "53",
        "ko": "-"
    },
    "percentiles2": {
        "total": "79",
        "ok": "79",
        "ko": "-"
    },
    "percentiles3": {
        "total": "198",
        "ok": "198",
        "ko": "-"
    },
    "percentiles4": {
        "total": "351",
        "ok": "351",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 374,
    "percentage": 100
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 1,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.689",
        "ok": "0.689",
        "ko": "-"
    }
}
    },"req_get-suppressed--91726": {
        type: "REQUEST",
        name: "GET suppressed-ui",
path: "GET suppressed-ui",
pathFormatted: "req_get-suppressed--91726",
stats: {
    "name": "GET suppressed-ui",
    "numberOfRequests": {
        "total": "375",
        "ok": "375",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "8",
        "ok": "8",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "388",
        "ok": "388",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "41",
        "ok": "41",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "45",
        "ok": "45",
        "ko": "-"
    },
    "percentiles1": {
        "total": "30",
        "ok": "30",
        "ko": "-"
    },
    "percentiles2": {
        "total": "45",
        "ok": "45",
        "ko": "-"
    },
    "percentiles3": {
        "total": "100",
        "ok": "100",
        "ko": "-"
    },
    "percentiles4": {
        "total": "307",
        "ok": "307",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 375,
    "percentage": 100
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.689",
        "ok": "0.689",
        "ko": "-"
    }
}
    },"req_post-sync-custo-6f7e3": {
        type: "REQUEST",
        name: "POST sync-customer-info",
path: "POST sync-customer-info",
pathFormatted: "req_post-sync-custo-6f7e3",
stats: {
    "name": "POST sync-customer-info",
    "numberOfRequests": {
        "total": "375",
        "ok": "373",
        "ko": "2"
    },
    "minResponseTime": {
        "total": "47",
        "ok": "138",
        "ko": "47"
    },
    "maxResponseTime": {
        "total": "60001",
        "ok": "26837",
        "ko": "60001"
    },
    "meanResponseTime": {
        "total": "3655",
        "ok": "3514",
        "ko": "30024"
    },
    "standardDeviation": {
        "total": "5913",
        "ok": "5156",
        "ko": "29977"
    },
    "percentiles1": {
        "total": "1177",
        "ok": "1177",
        "ko": "30024"
    },
    "percentiles2": {
        "total": "3040",
        "ok": "3014",
        "ko": "45013"
    },
    "percentiles3": {
        "total": "13578",
        "ok": "13378",
        "ko": "57003"
    },
    "percentiles4": {
        "total": "24127",
        "ok": "23492",
        "ko": "59401"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 39,
    "percentage": 10
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 122,
    "percentage": 33
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 212,
    "percentage": 57
},
    "group4": {
    "name": "failed",
    "count": 2,
    "percentage": 1
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.689",
        "ok": "0.686",
        "ko": "0.004"
    }
}
    },"req_get-customer-in-bfc70": {
        type: "REQUEST",
        name: "GET customer-info",
path: "GET customer-info",
pathFormatted: "req_get-customer-in-bfc70",
stats: {
    "name": "GET customer-info",
    "numberOfRequests": {
        "total": "375",
        "ok": "375",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "10",
        "ok": "10",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "338",
        "ok": "338",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "41",
        "ok": "41",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "34",
        "ok": "34",
        "ko": "-"
    },
    "percentiles1": {
        "total": "33",
        "ok": "33",
        "ko": "-"
    },
    "percentiles2": {
        "total": "46",
        "ok": "46",
        "ko": "-"
    },
    "percentiles3": {
        "total": "107",
        "ok": "107",
        "ko": "-"
    },
    "percentiles4": {
        "total": "165",
        "ok": "165",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 375,
    "percentage": 100
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.689",
        "ok": "0.689",
        "ko": "-"
    }
}
    },"req_get-helixs-sync-9b1f7": {
        type: "REQUEST",
        name: "GET helixs-sync",
path: "GET helixs-sync",
pathFormatted: "req_get-helixs-sync-9b1f7",
stats: {
    "name": "GET helixs-sync",
    "numberOfRequests": {
        "total": "375",
        "ok": "375",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "9",
        "ok": "9",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "231",
        "ok": "231",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "39",
        "ok": "39",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "31",
        "ok": "31",
        "ko": "-"
    },
    "percentiles1": {
        "total": "32",
        "ok": "32",
        "ko": "-"
    },
    "percentiles2": {
        "total": "46",
        "ok": "46",
        "ko": "-"
    },
    "percentiles3": {
        "total": "94",
        "ok": "94",
        "ko": "-"
    },
    "percentiles4": {
        "total": "187",
        "ok": "187",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 375,
    "percentage": 100
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.689",
        "ok": "0.689",
        "ko": "-"
    }
}
    },"req_post-sync-66165": {
        type: "REQUEST",
        name: "POST SYNC",
path: "POST SYNC",
pathFormatted: "req_post-sync-66165",
stats: {
    "name": "POST SYNC",
    "numberOfRequests": {
        "total": "375",
        "ok": "347",
        "ko": "28"
    },
    "minResponseTime": {
        "total": "11",
        "ok": "566",
        "ko": "11"
    },
    "maxResponseTime": {
        "total": "60011",
        "ok": "59946",
        "ko": "60011"
    },
    "meanResponseTime": {
        "total": "26588",
        "ok": "24065",
        "ko": "57861"
    },
    "standardDeviation": {
        "total": "20280",
        "ok": "18687",
        "ko": "11133"
    },
    "percentiles1": {
        "total": "20469",
        "ok": "18469",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "46246",
        "ok": "42611",
        "ko": "60005"
    },
    "percentiles3": {
        "total": "60000",
        "ok": "56702",
        "ko": "60010"
    },
    "percentiles4": {
        "total": "60009",
        "ok": "58215",
        "ko": "60011"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 2,
    "percentage": 1
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 345,
    "percentage": 92
},
    "group4": {
    "name": "failed",
    "count": 28,
    "percentage": 7
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.689",
        "ok": "0.638",
        "ko": "0.051"
    }
}
    },"req_get-all-docs--2-9867e": {
        type: "REQUEST",
        name: "GET ALL_DOCS #2",
path: "GET ALL_DOCS #2",
pathFormatted: "req_get-all-docs--2-9867e",
stats: {
    "name": "GET ALL_DOCS #2",
    "numberOfRequests": {
        "total": "375",
        "ok": "375",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "40",
        "ok": "40",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5915",
        "ok": "5915",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1072",
        "ok": "1072",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "949",
        "ok": "949",
        "ko": "-"
    },
    "percentiles1": {
        "total": "781",
        "ok": "781",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1306",
        "ok": "1306",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3013",
        "ok": "3013",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4804",
        "ok": "4804",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 98,
    "percentage": 26
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 132,
    "percentage": 35
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 145,
    "percentage": 39
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.689",
        "ok": "0.689",
        "ko": "-"
    }
}
    },"req_get-info-43845": {
        type: "REQUEST",
        name: "GET INFO",
path: "GET INFO",
pathFormatted: "req_get-info-43845",
stats: {
    "name": "GET INFO",
    "numberOfRequests": {
        "total": "375",
        "ok": "375",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "9",
        "ok": "9",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1612",
        "ok": "1612",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "93",
        "ok": "93",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "173",
        "ok": "173",
        "ko": "-"
    },
    "percentiles1": {
        "total": "44",
        "ok": "44",
        "ko": "-"
    },
    "percentiles2": {
        "total": "89",
        "ok": "89",
        "ko": "-"
    },
    "percentiles3": {
        "total": "279",
        "ok": "279",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1175",
        "ok": "1175",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 368,
    "percentage": 98
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 1,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 6,
    "percentage": 2
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.689",
        "ok": "0.689",
        "ko": "-"
    }
}
    },"req_get-health-chec-caf6c": {
        type: "REQUEST",
        name: "GET HEALTH-CHECK",
path: "GET HEALTH-CHECK",
pathFormatted: "req_get-health-chec-caf6c",
stats: {
    "name": "GET HEALTH-CHECK",
    "numberOfRequests": {
        "total": "375",
        "ok": "375",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "52",
        "ok": "52",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1187",
        "ok": "1187",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "93",
        "ok": "93",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "79",
        "ok": "79",
        "ko": "-"
    },
    "percentiles1": {
        "total": "71",
        "ok": "71",
        "ko": "-"
    },
    "percentiles2": {
        "total": "102",
        "ok": "102",
        "ko": "-"
    },
    "percentiles3": {
        "total": "178",
        "ok": "178",
        "ko": "-"
    },
    "percentiles4": {
        "total": "417",
        "ok": "417",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 374,
    "percentage": 100
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.689",
        "ok": "0.689",
        "ko": "-"
    }
}
    },"req_get-database--2-e526d": {
        type: "REQUEST",
        name: "GET Database #2",
path: "GET Database #2",
pathFormatted: "req_get-database--2-e526d",
stats: {
    "name": "GET Database #2",
    "numberOfRequests": {
        "total": "375",
        "ok": "375",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "9",
        "ok": "9",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1109",
        "ok": "1109",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "55",
        "ok": "55",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "86",
        "ok": "86",
        "ko": "-"
    },
    "percentiles1": {
        "total": "37",
        "ok": "37",
        "ko": "-"
    },
    "percentiles2": {
        "total": "65",
        "ok": "65",
        "ko": "-"
    },
    "percentiles3": {
        "total": "136",
        "ok": "136",
        "ko": "-"
    },
    "percentiles4": {
        "total": "229",
        "ok": "229",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 373,
    "percentage": 99
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 2,
    "percentage": 1
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.689",
        "ok": "0.689",
        "ko": "-"
    }
}
    },"req_changes-feed-4d6ae": {
        type: "REQUEST",
        name: "CHANGES FEED",
path: "CHANGES FEED",
pathFormatted: "req_changes-feed-4d6ae",
stats: {
    "name": "CHANGES FEED",
    "numberOfRequests": {
        "total": "375",
        "ok": "375",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "836",
        "ok": "836",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "60313",
        "ok": "60313",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "57517",
        "ok": "57517",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "11335",
        "ok": "11335",
        "ko": "-"
    },
    "percentiles1": {
        "total": "60037",
        "ok": "60037",
        "ko": "-"
    },
    "percentiles2": {
        "total": "60058",
        "ok": "60058",
        "ko": "-"
    },
    "percentiles3": {
        "total": "60103",
        "ok": "60103",
        "ko": "-"
    },
    "percentiles4": {
        "total": "60210",
        "ok": "60210",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 2,
    "percentage": 1
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 373,
    "percentage": 99
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.689",
        "ok": "0.689",
        "ko": "-"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
