var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "495",
        "ok": "495",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "936",
        "ok": "936",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "10430",
        "ok": "10430",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2501",
        "ok": "2501",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "1045",
        "ok": "1045",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2293",
        "ok": "2293",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2773",
        "ok": "2773",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4244",
        "ok": "4244",
        "ko": "-"
    },
    "percentiles4": {
        "total": "7045",
        "ok": "7045",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 2,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 493,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.231",
        "ok": "1.231",
        "ko": "-"
    }
},
contents: {
"req_animal-api-sync-9e54f": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_112168",
path: "Animal-api sync for customer NL_112168",
pathFormatted: "req_animal-api-sync-9e54f",
stats: {
    "name": "Animal-api sync for customer NL_112168",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2524",
        "ok": "2524",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2524",
        "ok": "2524",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2524",
        "ok": "2524",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2524",
        "ok": "2524",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2524",
        "ok": "2524",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2524",
        "ok": "2524",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2524",
        "ok": "2524",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-f5a2a": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_125092",
path: "Animal-api sync for customer NL_125092",
pathFormatted: "req_animal-api-sync-f5a2a",
stats: {
    "name": "Animal-api sync for customer NL_125092",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2163",
        "ok": "2163",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2163",
        "ok": "2163",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2163",
        "ok": "2163",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2163",
        "ok": "2163",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2163",
        "ok": "2163",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2163",
        "ok": "2163",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2163",
        "ok": "2163",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-0f1c3": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_128277",
path: "Animal-api sync for customer NL_128277",
pathFormatted: "req_animal-api-sync-0f1c3",
stats: {
    "name": "Animal-api sync for customer NL_128277",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2343",
        "ok": "2343",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2343",
        "ok": "2343",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2343",
        "ok": "2343",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2343",
        "ok": "2343",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2343",
        "ok": "2343",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2343",
        "ok": "2343",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2343",
        "ok": "2343",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-1d77d": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_109531",
path: "Animal-api sync for customer NL_109531",
pathFormatted: "req_animal-api-sync-1d77d",
stats: {
    "name": "Animal-api sync for customer NL_109531",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2116",
        "ok": "2116",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2116",
        "ok": "2116",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2116",
        "ok": "2116",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2116",
        "ok": "2116",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2116",
        "ok": "2116",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2116",
        "ok": "2116",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2116",
        "ok": "2116",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e9ad2": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_121119",
path: "Animal-api sync for customer NL_121119",
pathFormatted: "req_animal-api-sync-e9ad2",
stats: {
    "name": "Animal-api sync for customer NL_121119",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2641",
        "ok": "2641",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2641",
        "ok": "2641",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2641",
        "ok": "2641",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2641",
        "ok": "2641",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2641",
        "ok": "2641",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2641",
        "ok": "2641",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2641",
        "ok": "2641",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-86f14": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_121285",
path: "Animal-api sync for customer NL_121285",
pathFormatted: "req_animal-api-sync-86f14",
stats: {
    "name": "Animal-api sync for customer NL_121285",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1675",
        "ok": "1675",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1675",
        "ok": "1675",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1675",
        "ok": "1675",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1675",
        "ok": "1675",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1675",
        "ok": "1675",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1675",
        "ok": "1675",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1675",
        "ok": "1675",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d5299": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104311",
path: "Animal-api sync for customer NL_104311",
pathFormatted: "req_animal-api-sync-d5299",
stats: {
    "name": "Animal-api sync for customer NL_104311",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2206",
        "ok": "2206",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2206",
        "ok": "2206",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2206",
        "ok": "2206",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2206",
        "ok": "2206",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2206",
        "ok": "2206",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2206",
        "ok": "2206",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2206",
        "ok": "2206",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-51e6e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117693",
path: "Animal-api sync for customer NL_117693",
pathFormatted: "req_animal-api-sync-51e6e",
stats: {
    "name": "Animal-api sync for customer NL_117693",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3036",
        "ok": "3036",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3036",
        "ok": "3036",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3036",
        "ok": "3036",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3036",
        "ok": "3036",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3036",
        "ok": "3036",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3036",
        "ok": "3036",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3036",
        "ok": "3036",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-44547": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117813",
path: "Animal-api sync for customer NL_117813",
pathFormatted: "req_animal-api-sync-44547",
stats: {
    "name": "Animal-api sync for customer NL_117813",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2290",
        "ok": "2290",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2290",
        "ok": "2290",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2290",
        "ok": "2290",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2290",
        "ok": "2290",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2290",
        "ok": "2290",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2290",
        "ok": "2290",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2290",
        "ok": "2290",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-76899": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_121301",
path: "Animal-api sync for customer NL_121301",
pathFormatted: "req_animal-api-sync-76899",
stats: {
    "name": "Animal-api sync for customer NL_121301",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2015",
        "ok": "2015",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2015",
        "ok": "2015",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2015",
        "ok": "2015",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2015",
        "ok": "2015",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2015",
        "ok": "2015",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2015",
        "ok": "2015",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2015",
        "ok": "2015",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-857a0": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_127307",
path: "Animal-api sync for customer NL_127307",
pathFormatted: "req_animal-api-sync-857a0",
stats: {
    "name": "Animal-api sync for customer NL_127307",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1316",
        "ok": "1316",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1316",
        "ok": "1316",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1316",
        "ok": "1316",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1316",
        "ok": "1316",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1316",
        "ok": "1316",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1316",
        "ok": "1316",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1316",
        "ok": "1316",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-8b245": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_120606",
path: "Animal-api sync for customer NL_120606",
pathFormatted: "req_animal-api-sync-8b245",
stats: {
    "name": "Animal-api sync for customer NL_120606",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2784",
        "ok": "2784",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2784",
        "ok": "2784",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2784",
        "ok": "2784",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2784",
        "ok": "2784",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2784",
        "ok": "2784",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2784",
        "ok": "2784",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2784",
        "ok": "2784",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-1ba07": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105320",
path: "Animal-api sync for customer NL_105320",
pathFormatted: "req_animal-api-sync-1ba07",
stats: {
    "name": "Animal-api sync for customer NL_105320",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1587",
        "ok": "1587",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1587",
        "ok": "1587",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1587",
        "ok": "1587",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1587",
        "ok": "1587",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1587",
        "ok": "1587",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1587",
        "ok": "1587",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1587",
        "ok": "1587",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-07d8c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_115607",
path: "Animal-api sync for customer NL_115607",
pathFormatted: "req_animal-api-sync-07d8c",
stats: {
    "name": "Animal-api sync for customer NL_115607",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3073",
        "ok": "3073",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3073",
        "ok": "3073",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3073",
        "ok": "3073",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3073",
        "ok": "3073",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3073",
        "ok": "3073",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3073",
        "ok": "3073",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3073",
        "ok": "3073",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d643b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_129420",
path: "Animal-api sync for customer NL_129420",
pathFormatted: "req_animal-api-sync-d643b",
stats: {
    "name": "Animal-api sync for customer NL_129420",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1116",
        "ok": "1116",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1116",
        "ok": "1116",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1116",
        "ok": "1116",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1116",
        "ok": "1116",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1116",
        "ok": "1116",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1116",
        "ok": "1116",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1116",
        "ok": "1116",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-5b7d7": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_113415",
path: "Animal-api sync for customer NL_113415",
pathFormatted: "req_animal-api-sync-5b7d7",
stats: {
    "name": "Animal-api sync for customer NL_113415",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3727",
        "ok": "3727",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3727",
        "ok": "3727",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3727",
        "ok": "3727",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3727",
        "ok": "3727",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3727",
        "ok": "3727",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3727",
        "ok": "3727",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3727",
        "ok": "3727",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-05129": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117717",
path: "Animal-api sync for customer NL_117717",
pathFormatted: "req_animal-api-sync-05129",
stats: {
    "name": "Animal-api sync for customer NL_117717",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1526",
        "ok": "1526",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1526",
        "ok": "1526",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1526",
        "ok": "1526",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1526",
        "ok": "1526",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1526",
        "ok": "1526",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1526",
        "ok": "1526",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1526",
        "ok": "1526",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a175e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_207835",
path: "Animal-api sync for customer NL_207835",
pathFormatted: "req_animal-api-sync-a175e",
stats: {
    "name": "Animal-api sync for customer NL_207835",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1157",
        "ok": "1157",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1157",
        "ok": "1157",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1157",
        "ok": "1157",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1157",
        "ok": "1157",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1157",
        "ok": "1157",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1157",
        "ok": "1157",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1157",
        "ok": "1157",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-3bf26": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_108699",
path: "Animal-api sync for customer NL_108699",
pathFormatted: "req_animal-api-sync-3bf26",
stats: {
    "name": "Animal-api sync for customer NL_108699",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1112",
        "ok": "1112",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1112",
        "ok": "1112",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1112",
        "ok": "1112",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1112",
        "ok": "1112",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1112",
        "ok": "1112",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1112",
        "ok": "1112",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1112",
        "ok": "1112",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-2b97a": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_161984",
path: "Animal-api sync for customer NL_161984",
pathFormatted: "req_animal-api-sync-2b97a",
stats: {
    "name": "Animal-api sync for customer NL_161984",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2710",
        "ok": "2710",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2710",
        "ok": "2710",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2710",
        "ok": "2710",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2710",
        "ok": "2710",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2710",
        "ok": "2710",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2710",
        "ok": "2710",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2710",
        "ok": "2710",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-5243f": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111939",
path: "Animal-api sync for customer NL_111939",
pathFormatted: "req_animal-api-sync-5243f",
stats: {
    "name": "Animal-api sync for customer NL_111939",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2791",
        "ok": "2791",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2791",
        "ok": "2791",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2791",
        "ok": "2791",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2791",
        "ok": "2791",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2791",
        "ok": "2791",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2791",
        "ok": "2791",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2791",
        "ok": "2791",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b6219": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_144583",
path: "Animal-api sync for customer NL_144583",
pathFormatted: "req_animal-api-sync-b6219",
stats: {
    "name": "Animal-api sync for customer NL_144583",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1603",
        "ok": "1603",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1603",
        "ok": "1603",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1603",
        "ok": "1603",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1603",
        "ok": "1603",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1603",
        "ok": "1603",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1603",
        "ok": "1603",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1603",
        "ok": "1603",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-43dcc": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_112405",
path: "Animal-api sync for customer NL_112405",
pathFormatted: "req_animal-api-sync-43dcc",
stats: {
    "name": "Animal-api sync for customer NL_112405",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1962",
        "ok": "1962",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1962",
        "ok": "1962",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1962",
        "ok": "1962",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1962",
        "ok": "1962",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1962",
        "ok": "1962",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1962",
        "ok": "1962",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1962",
        "ok": "1962",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d545c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_131064",
path: "Animal-api sync for customer NL_131064",
pathFormatted: "req_animal-api-sync-d545c",
stats: {
    "name": "Animal-api sync for customer NL_131064",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2614",
        "ok": "2614",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2614",
        "ok": "2614",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2614",
        "ok": "2614",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2614",
        "ok": "2614",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2614",
        "ok": "2614",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2614",
        "ok": "2614",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2614",
        "ok": "2614",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-93205": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_120176",
path: "Animal-api sync for customer NL_120176",
pathFormatted: "req_animal-api-sync-93205",
stats: {
    "name": "Animal-api sync for customer NL_120176",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2777",
        "ok": "2777",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2777",
        "ok": "2777",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2777",
        "ok": "2777",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2777",
        "ok": "2777",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2777",
        "ok": "2777",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2777",
        "ok": "2777",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2777",
        "ok": "2777",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-bf978": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_118830",
path: "Animal-api sync for customer NL_118830",
pathFormatted: "req_animal-api-sync-bf978",
stats: {
    "name": "Animal-api sync for customer NL_118830",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2126",
        "ok": "2126",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2126",
        "ok": "2126",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2126",
        "ok": "2126",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2126",
        "ok": "2126",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2126",
        "ok": "2126",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2126",
        "ok": "2126",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2126",
        "ok": "2126",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-28852": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_116904",
path: "Animal-api sync for customer NL_116904",
pathFormatted: "req_animal-api-sync-28852",
stats: {
    "name": "Animal-api sync for customer NL_116904",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2619",
        "ok": "2619",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2619",
        "ok": "2619",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2619",
        "ok": "2619",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2619",
        "ok": "2619",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2619",
        "ok": "2619",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2619",
        "ok": "2619",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2619",
        "ok": "2619",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-3566a": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_107884",
path: "Animal-api sync for customer NL_107884",
pathFormatted: "req_animal-api-sync-3566a",
stats: {
    "name": "Animal-api sync for customer NL_107884",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2433",
        "ok": "2433",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2433",
        "ok": "2433",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2433",
        "ok": "2433",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2433",
        "ok": "2433",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2433",
        "ok": "2433",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2433",
        "ok": "2433",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2433",
        "ok": "2433",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c24b5": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_122429",
path: "Animal-api sync for customer NL_122429",
pathFormatted: "req_animal-api-sync-c24b5",
stats: {
    "name": "Animal-api sync for customer NL_122429",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2052",
        "ok": "2052",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2052",
        "ok": "2052",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2052",
        "ok": "2052",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2052",
        "ok": "2052",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2052",
        "ok": "2052",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2052",
        "ok": "2052",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2052",
        "ok": "2052",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-31f54": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_122305",
path: "Animal-api sync for customer NL_122305",
pathFormatted: "req_animal-api-sync-31f54",
stats: {
    "name": "Animal-api sync for customer NL_122305",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2243",
        "ok": "2243",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2243",
        "ok": "2243",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2243",
        "ok": "2243",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2243",
        "ok": "2243",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2243",
        "ok": "2243",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2243",
        "ok": "2243",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2243",
        "ok": "2243",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-8ebc3": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_212864",
path: "Animal-api sync for customer BE_212864",
pathFormatted: "req_animal-api-sync-8ebc3",
stats: {
    "name": "Animal-api sync for customer BE_212864",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2257",
        "ok": "2257",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2257",
        "ok": "2257",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2257",
        "ok": "2257",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2257",
        "ok": "2257",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2257",
        "ok": "2257",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2257",
        "ok": "2257",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2257",
        "ok": "2257",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a5160": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_156494",
path: "Animal-api sync for customer NL_156494",
pathFormatted: "req_animal-api-sync-a5160",
stats: {
    "name": "Animal-api sync for customer NL_156494",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5179",
        "ok": "5179",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5179",
        "ok": "5179",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5179",
        "ok": "5179",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5179",
        "ok": "5179",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5179",
        "ok": "5179",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5179",
        "ok": "5179",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5179",
        "ok": "5179",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-34ba1": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_165365",
path: "Animal-api sync for customer BE_165365",
pathFormatted: "req_animal-api-sync-34ba1",
stats: {
    "name": "Animal-api sync for customer BE_165365",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2936",
        "ok": "2936",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2936",
        "ok": "2936",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2936",
        "ok": "2936",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2936",
        "ok": "2936",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2936",
        "ok": "2936",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2936",
        "ok": "2936",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2936",
        "ok": "2936",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-9ec36": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_124747",
path: "Animal-api sync for customer NL_124747",
pathFormatted: "req_animal-api-sync-9ec36",
stats: {
    "name": "Animal-api sync for customer NL_124747",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2402",
        "ok": "2402",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2402",
        "ok": "2402",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2402",
        "ok": "2402",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2402",
        "ok": "2402",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2402",
        "ok": "2402",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2402",
        "ok": "2402",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2402",
        "ok": "2402",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b9862": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_161770",
path: "Animal-api sync for customer NL_161770",
pathFormatted: "req_animal-api-sync-b9862",
stats: {
    "name": "Animal-api sync for customer NL_161770",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2938",
        "ok": "2938",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2938",
        "ok": "2938",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2938",
        "ok": "2938",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2938",
        "ok": "2938",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2938",
        "ok": "2938",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2938",
        "ok": "2938",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2938",
        "ok": "2938",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b2272": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105223",
path: "Animal-api sync for customer NL_105223",
pathFormatted: "req_animal-api-sync-b2272",
stats: {
    "name": "Animal-api sync for customer NL_105223",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2574",
        "ok": "2574",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2574",
        "ok": "2574",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2574",
        "ok": "2574",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2574",
        "ok": "2574",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2574",
        "ok": "2574",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2574",
        "ok": "2574",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2574",
        "ok": "2574",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-49a96": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_128642",
path: "Animal-api sync for customer NL_128642",
pathFormatted: "req_animal-api-sync-49a96",
stats: {
    "name": "Animal-api sync for customer NL_128642",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1984",
        "ok": "1984",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1984",
        "ok": "1984",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1984",
        "ok": "1984",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1984",
        "ok": "1984",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1984",
        "ok": "1984",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1984",
        "ok": "1984",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1984",
        "ok": "1984",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-5c4eb": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_123093",
path: "Animal-api sync for customer NL_123093",
pathFormatted: "req_animal-api-sync-5c4eb",
stats: {
    "name": "Animal-api sync for customer NL_123093",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1802",
        "ok": "1802",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1802",
        "ok": "1802",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1802",
        "ok": "1802",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1802",
        "ok": "1802",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1802",
        "ok": "1802",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1802",
        "ok": "1802",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1802",
        "ok": "1802",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-90ee6": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105389",
path: "Animal-api sync for customer NL_105389",
pathFormatted: "req_animal-api-sync-90ee6",
stats: {
    "name": "Animal-api sync for customer NL_105389",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2754",
        "ok": "2754",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2754",
        "ok": "2754",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2754",
        "ok": "2754",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2754",
        "ok": "2754",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2754",
        "ok": "2754",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2754",
        "ok": "2754",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2754",
        "ok": "2754",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-0908d": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105707",
path: "Animal-api sync for customer NL_105707",
pathFormatted: "req_animal-api-sync-0908d",
stats: {
    "name": "Animal-api sync for customer NL_105707",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2224",
        "ok": "2224",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2224",
        "ok": "2224",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2224",
        "ok": "2224",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2224",
        "ok": "2224",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2224",
        "ok": "2224",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2224",
        "ok": "2224",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2224",
        "ok": "2224",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-0a82c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_124910",
path: "Animal-api sync for customer NL_124910",
pathFormatted: "req_animal-api-sync-0a82c",
stats: {
    "name": "Animal-api sync for customer NL_124910",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1296",
        "ok": "1296",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1296",
        "ok": "1296",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1296",
        "ok": "1296",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1296",
        "ok": "1296",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1296",
        "ok": "1296",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1296",
        "ok": "1296",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1296",
        "ok": "1296",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d54bb": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_108024",
path: "Animal-api sync for customer NL_108024",
pathFormatted: "req_animal-api-sync-d54bb",
stats: {
    "name": "Animal-api sync for customer NL_108024",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4689",
        "ok": "4689",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4689",
        "ok": "4689",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4689",
        "ok": "4689",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4689",
        "ok": "4689",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4689",
        "ok": "4689",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4689",
        "ok": "4689",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4689",
        "ok": "4689",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c0ea2": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_126187",
path: "Animal-api sync for customer NL_126187",
pathFormatted: "req_animal-api-sync-c0ea2",
stats: {
    "name": "Animal-api sync for customer NL_126187",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1588",
        "ok": "1588",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1588",
        "ok": "1588",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1588",
        "ok": "1588",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1588",
        "ok": "1588",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1588",
        "ok": "1588",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1588",
        "ok": "1588",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1588",
        "ok": "1588",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-7a1f1": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_151229",
path: "Animal-api sync for customer BE_151229",
pathFormatted: "req_animal-api-sync-7a1f1",
stats: {
    "name": "Animal-api sync for customer BE_151229",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1555",
        "ok": "1555",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1555",
        "ok": "1555",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1555",
        "ok": "1555",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1555",
        "ok": "1555",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1555",
        "ok": "1555",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1555",
        "ok": "1555",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1555",
        "ok": "1555",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-72904": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_103783",
path: "Animal-api sync for customer NL_103783",
pathFormatted: "req_animal-api-sync-72904",
stats: {
    "name": "Animal-api sync for customer NL_103783",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2710",
        "ok": "2710",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2710",
        "ok": "2710",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2710",
        "ok": "2710",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2710",
        "ok": "2710",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2710",
        "ok": "2710",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2710",
        "ok": "2710",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2710",
        "ok": "2710",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-54314": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_103881",
path: "Animal-api sync for customer NL_103881",
pathFormatted: "req_animal-api-sync-54314",
stats: {
    "name": "Animal-api sync for customer NL_103881",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2596",
        "ok": "2596",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2596",
        "ok": "2596",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2596",
        "ok": "2596",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2596",
        "ok": "2596",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2596",
        "ok": "2596",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2596",
        "ok": "2596",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2596",
        "ok": "2596",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-fb3ec": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_115893",
path: "Animal-api sync for customer NL_115893",
pathFormatted: "req_animal-api-sync-fb3ec",
stats: {
    "name": "Animal-api sync for customer NL_115893",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2459",
        "ok": "2459",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2459",
        "ok": "2459",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2459",
        "ok": "2459",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2459",
        "ok": "2459",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2459",
        "ok": "2459",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2459",
        "ok": "2459",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2459",
        "ok": "2459",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d90a9": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_119649",
path: "Animal-api sync for customer NL_119649",
pathFormatted: "req_animal-api-sync-d90a9",
stats: {
    "name": "Animal-api sync for customer NL_119649",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2081",
        "ok": "2081",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2081",
        "ok": "2081",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2081",
        "ok": "2081",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2081",
        "ok": "2081",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2081",
        "ok": "2081",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2081",
        "ok": "2081",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2081",
        "ok": "2081",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c4ec4": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104910",
path: "Animal-api sync for customer NL_104910",
pathFormatted: "req_animal-api-sync-c4ec4",
stats: {
    "name": "Animal-api sync for customer NL_104910",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1840",
        "ok": "1840",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1840",
        "ok": "1840",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1840",
        "ok": "1840",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1840",
        "ok": "1840",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1840",
        "ok": "1840",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1840",
        "ok": "1840",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1840",
        "ok": "1840",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e498b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110382",
path: "Animal-api sync for customer NL_110382",
pathFormatted: "req_animal-api-sync-e498b",
stats: {
    "name": "Animal-api sync for customer NL_110382",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2096",
        "ok": "2096",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2096",
        "ok": "2096",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2096",
        "ok": "2096",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2096",
        "ok": "2096",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2096",
        "ok": "2096",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2096",
        "ok": "2096",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2096",
        "ok": "2096",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-2a4dd": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_125297",
path: "Animal-api sync for customer NL_125297",
pathFormatted: "req_animal-api-sync-2a4dd",
stats: {
    "name": "Animal-api sync for customer NL_125297",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1950",
        "ok": "1950",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1950",
        "ok": "1950",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1950",
        "ok": "1950",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1950",
        "ok": "1950",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1950",
        "ok": "1950",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1950",
        "ok": "1950",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1950",
        "ok": "1950",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c1c6a": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_135207",
path: "Animal-api sync for customer NL_135207",
pathFormatted: "req_animal-api-sync-c1c6a",
stats: {
    "name": "Animal-api sync for customer NL_135207",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2621",
        "ok": "2621",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2621",
        "ok": "2621",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2621",
        "ok": "2621",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2621",
        "ok": "2621",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2621",
        "ok": "2621",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2621",
        "ok": "2621",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2621",
        "ok": "2621",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c170f": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_109428",
path: "Animal-api sync for customer NL_109428",
pathFormatted: "req_animal-api-sync-c170f",
stats: {
    "name": "Animal-api sync for customer NL_109428",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2157",
        "ok": "2157",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2157",
        "ok": "2157",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2157",
        "ok": "2157",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2157",
        "ok": "2157",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2157",
        "ok": "2157",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2157",
        "ok": "2157",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2157",
        "ok": "2157",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-690f7": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_129681",
path: "Animal-api sync for customer NL_129681",
pathFormatted: "req_animal-api-sync-690f7",
stats: {
    "name": "Animal-api sync for customer NL_129681",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1955",
        "ok": "1955",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1955",
        "ok": "1955",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1955",
        "ok": "1955",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1955",
        "ok": "1955",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1955",
        "ok": "1955",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1955",
        "ok": "1955",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1955",
        "ok": "1955",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-de0b1": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_164131",
path: "Animal-api sync for customer NL_164131",
pathFormatted: "req_animal-api-sync-de0b1",
stats: {
    "name": "Animal-api sync for customer NL_164131",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1975",
        "ok": "1975",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1975",
        "ok": "1975",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1975",
        "ok": "1975",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1975",
        "ok": "1975",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1975",
        "ok": "1975",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1975",
        "ok": "1975",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1975",
        "ok": "1975",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-3e31b": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_149431",
path: "Animal-api sync for customer BE_149431",
pathFormatted: "req_animal-api-sync-3e31b",
stats: {
    "name": "Animal-api sync for customer BE_149431",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1821",
        "ok": "1821",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1821",
        "ok": "1821",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1821",
        "ok": "1821",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1821",
        "ok": "1821",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1821",
        "ok": "1821",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1821",
        "ok": "1821",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1821",
        "ok": "1821",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-cb71c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_120709",
path: "Animal-api sync for customer NL_120709",
pathFormatted: "req_animal-api-sync-cb71c",
stats: {
    "name": "Animal-api sync for customer NL_120709",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2296",
        "ok": "2296",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2296",
        "ok": "2296",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2296",
        "ok": "2296",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2296",
        "ok": "2296",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2296",
        "ok": "2296",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2296",
        "ok": "2296",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2296",
        "ok": "2296",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-5a7e6": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105010",
path: "Animal-api sync for customer NL_105010",
pathFormatted: "req_animal-api-sync-5a7e6",
stats: {
    "name": "Animal-api sync for customer NL_105010",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4015",
        "ok": "4015",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4015",
        "ok": "4015",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4015",
        "ok": "4015",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4015",
        "ok": "4015",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4015",
        "ok": "4015",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4015",
        "ok": "4015",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4015",
        "ok": "4015",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-0eddf": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110820",
path: "Animal-api sync for customer NL_110820",
pathFormatted: "req_animal-api-sync-0eddf",
stats: {
    "name": "Animal-api sync for customer NL_110820",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2814",
        "ok": "2814",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2814",
        "ok": "2814",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2814",
        "ok": "2814",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2814",
        "ok": "2814",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2814",
        "ok": "2814",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2814",
        "ok": "2814",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2814",
        "ok": "2814",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-558a2": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_116270",
path: "Animal-api sync for customer NL_116270",
pathFormatted: "req_animal-api-sync-558a2",
stats: {
    "name": "Animal-api sync for customer NL_116270",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1868",
        "ok": "1868",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1868",
        "ok": "1868",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1868",
        "ok": "1868",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1868",
        "ok": "1868",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1868",
        "ok": "1868",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1868",
        "ok": "1868",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1868",
        "ok": "1868",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-3833c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_124824",
path: "Animal-api sync for customer NL_124824",
pathFormatted: "req_animal-api-sync-3833c",
stats: {
    "name": "Animal-api sync for customer NL_124824",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1393",
        "ok": "1393",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1393",
        "ok": "1393",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1393",
        "ok": "1393",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1393",
        "ok": "1393",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1393",
        "ok": "1393",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1393",
        "ok": "1393",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1393",
        "ok": "1393",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a3967": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117770",
path: "Animal-api sync for customer NL_117770",
pathFormatted: "req_animal-api-sync-a3967",
stats: {
    "name": "Animal-api sync for customer NL_117770",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2612",
        "ok": "2612",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2612",
        "ok": "2612",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2612",
        "ok": "2612",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2612",
        "ok": "2612",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2612",
        "ok": "2612",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2612",
        "ok": "2612",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2612",
        "ok": "2612",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-5a613": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_194583",
path: "Animal-api sync for customer NL_194583",
pathFormatted: "req_animal-api-sync-5a613",
stats: {
    "name": "Animal-api sync for customer NL_194583",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2627",
        "ok": "2627",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2627",
        "ok": "2627",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2627",
        "ok": "2627",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2627",
        "ok": "2627",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2627",
        "ok": "2627",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2627",
        "ok": "2627",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2627",
        "ok": "2627",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c7c5d": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_129379",
path: "Animal-api sync for customer NL_129379",
pathFormatted: "req_animal-api-sync-c7c5d",
stats: {
    "name": "Animal-api sync for customer NL_129379",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2325",
        "ok": "2325",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2325",
        "ok": "2325",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2325",
        "ok": "2325",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2325",
        "ok": "2325",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2325",
        "ok": "2325",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2325",
        "ok": "2325",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2325",
        "ok": "2325",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-91ebb": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_120778",
path: "Animal-api sync for customer NL_120778",
pathFormatted: "req_animal-api-sync-91ebb",
stats: {
    "name": "Animal-api sync for customer NL_120778",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1463",
        "ok": "1463",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1463",
        "ok": "1463",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1463",
        "ok": "1463",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1463",
        "ok": "1463",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1463",
        "ok": "1463",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1463",
        "ok": "1463",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1463",
        "ok": "1463",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-6d45c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_123012",
path: "Animal-api sync for customer NL_123012",
pathFormatted: "req_animal-api-sync-6d45c",
stats: {
    "name": "Animal-api sync for customer NL_123012",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1493",
        "ok": "1493",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1493",
        "ok": "1493",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1493",
        "ok": "1493",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1493",
        "ok": "1493",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1493",
        "ok": "1493",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1493",
        "ok": "1493",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1493",
        "ok": "1493",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-3ff93": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111287",
path: "Animal-api sync for customer NL_111287",
pathFormatted: "req_animal-api-sync-3ff93",
stats: {
    "name": "Animal-api sync for customer NL_111287",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1724",
        "ok": "1724",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1724",
        "ok": "1724",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1724",
        "ok": "1724",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1724",
        "ok": "1724",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1724",
        "ok": "1724",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1724",
        "ok": "1724",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1724",
        "ok": "1724",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-968ea": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_152278",
path: "Animal-api sync for customer BE_152278",
pathFormatted: "req_animal-api-sync-968ea",
stats: {
    "name": "Animal-api sync for customer BE_152278",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2278",
        "ok": "2278",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2278",
        "ok": "2278",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2278",
        "ok": "2278",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2278",
        "ok": "2278",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2278",
        "ok": "2278",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2278",
        "ok": "2278",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2278",
        "ok": "2278",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b979d": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_134839",
path: "Animal-api sync for customer NL_134839",
pathFormatted: "req_animal-api-sync-b979d",
stats: {
    "name": "Animal-api sync for customer NL_134839",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1772",
        "ok": "1772",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1772",
        "ok": "1772",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1772",
        "ok": "1772",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1772",
        "ok": "1772",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1772",
        "ok": "1772",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1772",
        "ok": "1772",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1772",
        "ok": "1772",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-4a2f0": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_119479",
path: "Animal-api sync for customer NL_119479",
pathFormatted: "req_animal-api-sync-4a2f0",
stats: {
    "name": "Animal-api sync for customer NL_119479",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1903",
        "ok": "1903",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1903",
        "ok": "1903",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1903",
        "ok": "1903",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1903",
        "ok": "1903",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1903",
        "ok": "1903",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1903",
        "ok": "1903",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1903",
        "ok": "1903",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-04f19": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_142633",
path: "Animal-api sync for customer NL_142633",
pathFormatted: "req_animal-api-sync-04f19",
stats: {
    "name": "Animal-api sync for customer NL_142633",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2593",
        "ok": "2593",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2593",
        "ok": "2593",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2593",
        "ok": "2593",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2593",
        "ok": "2593",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2593",
        "ok": "2593",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2593",
        "ok": "2593",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2593",
        "ok": "2593",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c6934": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_144844",
path: "Animal-api sync for customer BE_144844",
pathFormatted: "req_animal-api-sync-c6934",
stats: {
    "name": "Animal-api sync for customer BE_144844",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2481",
        "ok": "2481",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2481",
        "ok": "2481",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2481",
        "ok": "2481",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2481",
        "ok": "2481",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2481",
        "ok": "2481",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2481",
        "ok": "2481",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2481",
        "ok": "2481",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-ee05d": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_106934",
path: "Animal-api sync for customer NL_106934",
pathFormatted: "req_animal-api-sync-ee05d",
stats: {
    "name": "Animal-api sync for customer NL_106934",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2548",
        "ok": "2548",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2548",
        "ok": "2548",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2548",
        "ok": "2548",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2548",
        "ok": "2548",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2548",
        "ok": "2548",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2548",
        "ok": "2548",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2548",
        "ok": "2548",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-26bf9": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110058",
path: "Animal-api sync for customer NL_110058",
pathFormatted: "req_animal-api-sync-26bf9",
stats: {
    "name": "Animal-api sync for customer NL_110058",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1830",
        "ok": "1830",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1830",
        "ok": "1830",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1830",
        "ok": "1830",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1830",
        "ok": "1830",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1830",
        "ok": "1830",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1830",
        "ok": "1830",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1830",
        "ok": "1830",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a42d0": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105360",
path: "Animal-api sync for customer NL_105360",
pathFormatted: "req_animal-api-sync-a42d0",
stats: {
    "name": "Animal-api sync for customer NL_105360",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2179",
        "ok": "2179",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2179",
        "ok": "2179",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2179",
        "ok": "2179",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2179",
        "ok": "2179",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2179",
        "ok": "2179",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2179",
        "ok": "2179",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2179",
        "ok": "2179",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-35753": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_118795",
path: "Animal-api sync for customer NL_118795",
pathFormatted: "req_animal-api-sync-35753",
stats: {
    "name": "Animal-api sync for customer NL_118795",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1763",
        "ok": "1763",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1763",
        "ok": "1763",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1763",
        "ok": "1763",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1763",
        "ok": "1763",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1763",
        "ok": "1763",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1763",
        "ok": "1763",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1763",
        "ok": "1763",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-09a8d": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117444",
path: "Animal-api sync for customer NL_117444",
pathFormatted: "req_animal-api-sync-09a8d",
stats: {
    "name": "Animal-api sync for customer NL_117444",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1671",
        "ok": "1671",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1671",
        "ok": "1671",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1671",
        "ok": "1671",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1671",
        "ok": "1671",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1671",
        "ok": "1671",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1671",
        "ok": "1671",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1671",
        "ok": "1671",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-7c5cb": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_118794",
path: "Animal-api sync for customer NL_118794",
pathFormatted: "req_animal-api-sync-7c5cb",
stats: {
    "name": "Animal-api sync for customer NL_118794",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1721",
        "ok": "1721",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1721",
        "ok": "1721",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1721",
        "ok": "1721",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1721",
        "ok": "1721",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1721",
        "ok": "1721",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1721",
        "ok": "1721",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1721",
        "ok": "1721",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e202d": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_112066",
path: "Animal-api sync for customer NL_112066",
pathFormatted: "req_animal-api-sync-e202d",
stats: {
    "name": "Animal-api sync for customer NL_112066",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1940",
        "ok": "1940",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1940",
        "ok": "1940",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1940",
        "ok": "1940",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1940",
        "ok": "1940",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1940",
        "ok": "1940",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1940",
        "ok": "1940",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1940",
        "ok": "1940",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-be37d": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_106236",
path: "Animal-api sync for customer NL_106236",
pathFormatted: "req_animal-api-sync-be37d",
stats: {
    "name": "Animal-api sync for customer NL_106236",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2142",
        "ok": "2142",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2142",
        "ok": "2142",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2142",
        "ok": "2142",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2142",
        "ok": "2142",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2142",
        "ok": "2142",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2142",
        "ok": "2142",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2142",
        "ok": "2142",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-ca7e3": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_134297",
path: "Animal-api sync for customer NL_134297",
pathFormatted: "req_animal-api-sync-ca7e3",
stats: {
    "name": "Animal-api sync for customer NL_134297",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2009",
        "ok": "2009",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2009",
        "ok": "2009",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2009",
        "ok": "2009",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2009",
        "ok": "2009",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2009",
        "ok": "2009",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2009",
        "ok": "2009",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2009",
        "ok": "2009",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-993d0": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_160647",
path: "Animal-api sync for customer NL_160647",
pathFormatted: "req_animal-api-sync-993d0",
stats: {
    "name": "Animal-api sync for customer NL_160647",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5174",
        "ok": "5174",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5174",
        "ok": "5174",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5174",
        "ok": "5174",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5174",
        "ok": "5174",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5174",
        "ok": "5174",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5174",
        "ok": "5174",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5174",
        "ok": "5174",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-1e171": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111917",
path: "Animal-api sync for customer NL_111917",
pathFormatted: "req_animal-api-sync-1e171",
stats: {
    "name": "Animal-api sync for customer NL_111917",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2085",
        "ok": "2085",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2085",
        "ok": "2085",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2085",
        "ok": "2085",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2085",
        "ok": "2085",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2085",
        "ok": "2085",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2085",
        "ok": "2085",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2085",
        "ok": "2085",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-5e0b3": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_106214",
path: "Animal-api sync for customer NL_106214",
pathFormatted: "req_animal-api-sync-5e0b3",
stats: {
    "name": "Animal-api sync for customer NL_106214",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2436",
        "ok": "2436",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2436",
        "ok": "2436",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2436",
        "ok": "2436",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2436",
        "ok": "2436",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2436",
        "ok": "2436",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2436",
        "ok": "2436",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2436",
        "ok": "2436",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a7313": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_123953",
path: "Animal-api sync for customer NL_123953",
pathFormatted: "req_animal-api-sync-a7313",
stats: {
    "name": "Animal-api sync for customer NL_123953",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2768",
        "ok": "2768",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2768",
        "ok": "2768",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2768",
        "ok": "2768",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2768",
        "ok": "2768",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2768",
        "ok": "2768",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2768",
        "ok": "2768",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2768",
        "ok": "2768",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-7cf01": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104284",
path: "Animal-api sync for customer NL_104284",
pathFormatted: "req_animal-api-sync-7cf01",
stats: {
    "name": "Animal-api sync for customer NL_104284",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2221",
        "ok": "2221",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2221",
        "ok": "2221",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2221",
        "ok": "2221",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2221",
        "ok": "2221",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2221",
        "ok": "2221",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2221",
        "ok": "2221",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2221",
        "ok": "2221",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-588aa": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_214938",
path: "Animal-api sync for customer NL_214938",
pathFormatted: "req_animal-api-sync-588aa",
stats: {
    "name": "Animal-api sync for customer NL_214938",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2474",
        "ok": "2474",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2474",
        "ok": "2474",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2474",
        "ok": "2474",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2474",
        "ok": "2474",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2474",
        "ok": "2474",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2474",
        "ok": "2474",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2474",
        "ok": "2474",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-6ebe1": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104335",
path: "Animal-api sync for customer NL_104335",
pathFormatted: "req_animal-api-sync-6ebe1",
stats: {
    "name": "Animal-api sync for customer NL_104335",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3025",
        "ok": "3025",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3025",
        "ok": "3025",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3025",
        "ok": "3025",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3025",
        "ok": "3025",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3025",
        "ok": "3025",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3025",
        "ok": "3025",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3025",
        "ok": "3025",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d86cb": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105153",
path: "Animal-api sync for customer NL_105153",
pathFormatted: "req_animal-api-sync-d86cb",
stats: {
    "name": "Animal-api sync for customer NL_105153",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2025",
        "ok": "2025",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2025",
        "ok": "2025",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2025",
        "ok": "2025",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2025",
        "ok": "2025",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2025",
        "ok": "2025",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2025",
        "ok": "2025",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2025",
        "ok": "2025",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b6645": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_126020",
path: "Animal-api sync for customer NL_126020",
pathFormatted: "req_animal-api-sync-b6645",
stats: {
    "name": "Animal-api sync for customer NL_126020",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2156",
        "ok": "2156",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2156",
        "ok": "2156",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2156",
        "ok": "2156",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2156",
        "ok": "2156",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2156",
        "ok": "2156",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2156",
        "ok": "2156",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2156",
        "ok": "2156",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-7ae4d": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_137252",
path: "Animal-api sync for customer NL_137252",
pathFormatted: "req_animal-api-sync-7ae4d",
stats: {
    "name": "Animal-api sync for customer NL_137252",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1778",
        "ok": "1778",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1778",
        "ok": "1778",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1778",
        "ok": "1778",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1778",
        "ok": "1778",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1778",
        "ok": "1778",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1778",
        "ok": "1778",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1778",
        "ok": "1778",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-f2327": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_107409",
path: "Animal-api sync for customer NL_107409",
pathFormatted: "req_animal-api-sync-f2327",
stats: {
    "name": "Animal-api sync for customer NL_107409",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2147",
        "ok": "2147",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2147",
        "ok": "2147",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2147",
        "ok": "2147",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2147",
        "ok": "2147",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2147",
        "ok": "2147",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2147",
        "ok": "2147",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2147",
        "ok": "2147",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-9b90a": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_114569",
path: "Animal-api sync for customer NL_114569",
pathFormatted: "req_animal-api-sync-9b90a",
stats: {
    "name": "Animal-api sync for customer NL_114569",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1629",
        "ok": "1629",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1629",
        "ok": "1629",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1629",
        "ok": "1629",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1629",
        "ok": "1629",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1629",
        "ok": "1629",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1629",
        "ok": "1629",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1629",
        "ok": "1629",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-56e97": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_130716",
path: "Animal-api sync for customer NL_130716",
pathFormatted: "req_animal-api-sync-56e97",
stats: {
    "name": "Animal-api sync for customer NL_130716",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2026",
        "ok": "2026",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2026",
        "ok": "2026",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2026",
        "ok": "2026",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2026",
        "ok": "2026",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2026",
        "ok": "2026",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2026",
        "ok": "2026",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2026",
        "ok": "2026",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-4b1c0": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_164635",
path: "Animal-api sync for customer BE_164635",
pathFormatted: "req_animal-api-sync-4b1c0",
stats: {
    "name": "Animal-api sync for customer BE_164635",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2091",
        "ok": "2091",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2091",
        "ok": "2091",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2091",
        "ok": "2091",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2091",
        "ok": "2091",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2091",
        "ok": "2091",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2091",
        "ok": "2091",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2091",
        "ok": "2091",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-0be59": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_137530",
path: "Animal-api sync for customer NL_137530",
pathFormatted: "req_animal-api-sync-0be59",
stats: {
    "name": "Animal-api sync for customer NL_137530",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2062",
        "ok": "2062",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2062",
        "ok": "2062",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2062",
        "ok": "2062",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2062",
        "ok": "2062",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2062",
        "ok": "2062",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2062",
        "ok": "2062",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2062",
        "ok": "2062",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e4170": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_205597",
path: "Animal-api sync for customer BE_205597",
pathFormatted: "req_animal-api-sync-e4170",
stats: {
    "name": "Animal-api sync for customer BE_205597",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2577",
        "ok": "2577",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2577",
        "ok": "2577",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2577",
        "ok": "2577",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2577",
        "ok": "2577",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2577",
        "ok": "2577",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2577",
        "ok": "2577",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2577",
        "ok": "2577",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-5e066": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_118716",
path: "Animal-api sync for customer NL_118716",
pathFormatted: "req_animal-api-sync-5e066",
stats: {
    "name": "Animal-api sync for customer NL_118716",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2216",
        "ok": "2216",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2216",
        "ok": "2216",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2216",
        "ok": "2216",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2216",
        "ok": "2216",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2216",
        "ok": "2216",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2216",
        "ok": "2216",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2216",
        "ok": "2216",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-3598c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_107584",
path: "Animal-api sync for customer NL_107584",
pathFormatted: "req_animal-api-sync-3598c",
stats: {
    "name": "Animal-api sync for customer NL_107584",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1929",
        "ok": "1929",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1929",
        "ok": "1929",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1929",
        "ok": "1929",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1929",
        "ok": "1929",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1929",
        "ok": "1929",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1929",
        "ok": "1929",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1929",
        "ok": "1929",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-6b068": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111950",
path: "Animal-api sync for customer NL_111950",
pathFormatted: "req_animal-api-sync-6b068",
stats: {
    "name": "Animal-api sync for customer NL_111950",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2649",
        "ok": "2649",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2649",
        "ok": "2649",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2649",
        "ok": "2649",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2649",
        "ok": "2649",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2649",
        "ok": "2649",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2649",
        "ok": "2649",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2649",
        "ok": "2649",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-34e6f": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_129959",
path: "Animal-api sync for customer NL_129959",
pathFormatted: "req_animal-api-sync-34e6f",
stats: {
    "name": "Animal-api sync for customer NL_129959",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2104",
        "ok": "2104",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2104",
        "ok": "2104",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2104",
        "ok": "2104",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2104",
        "ok": "2104",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2104",
        "ok": "2104",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2104",
        "ok": "2104",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2104",
        "ok": "2104",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-21b03": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_109721",
path: "Animal-api sync for customer NL_109721",
pathFormatted: "req_animal-api-sync-21b03",
stats: {
    "name": "Animal-api sync for customer NL_109721",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3151",
        "ok": "3151",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3151",
        "ok": "3151",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3151",
        "ok": "3151",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3151",
        "ok": "3151",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3151",
        "ok": "3151",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3151",
        "ok": "3151",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3151",
        "ok": "3151",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-24d15": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117331",
path: "Animal-api sync for customer NL_117331",
pathFormatted: "req_animal-api-sync-24d15",
stats: {
    "name": "Animal-api sync for customer NL_117331",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2039",
        "ok": "2039",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2039",
        "ok": "2039",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2039",
        "ok": "2039",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2039",
        "ok": "2039",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2039",
        "ok": "2039",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2039",
        "ok": "2039",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2039",
        "ok": "2039",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-4479f": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_134641",
path: "Animal-api sync for customer NL_134641",
pathFormatted: "req_animal-api-sync-4479f",
stats: {
    "name": "Animal-api sync for customer NL_134641",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1539",
        "ok": "1539",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1539",
        "ok": "1539",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1539",
        "ok": "1539",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1539",
        "ok": "1539",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1539",
        "ok": "1539",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1539",
        "ok": "1539",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1539",
        "ok": "1539",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-2ac98": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_119983",
path: "Animal-api sync for customer NL_119983",
pathFormatted: "req_animal-api-sync-2ac98",
stats: {
    "name": "Animal-api sync for customer NL_119983",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2642",
        "ok": "2642",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2642",
        "ok": "2642",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2642",
        "ok": "2642",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2642",
        "ok": "2642",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2642",
        "ok": "2642",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2642",
        "ok": "2642",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2642",
        "ok": "2642",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-53e53": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_122724",
path: "Animal-api sync for customer NL_122724",
pathFormatted: "req_animal-api-sync-53e53",
stats: {
    "name": "Animal-api sync for customer NL_122724",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2208",
        "ok": "2208",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2208",
        "ok": "2208",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2208",
        "ok": "2208",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2208",
        "ok": "2208",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2208",
        "ok": "2208",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2208",
        "ok": "2208",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2208",
        "ok": "2208",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-97824": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_129903",
path: "Animal-api sync for customer NL_129903",
pathFormatted: "req_animal-api-sync-97824",
stats: {
    "name": "Animal-api sync for customer NL_129903",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2293",
        "ok": "2293",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2293",
        "ok": "2293",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2293",
        "ok": "2293",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2293",
        "ok": "2293",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2293",
        "ok": "2293",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2293",
        "ok": "2293",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2293",
        "ok": "2293",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-9db7a": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_125745",
path: "Animal-api sync for customer NL_125745",
pathFormatted: "req_animal-api-sync-9db7a",
stats: {
    "name": "Animal-api sync for customer NL_125745",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2460",
        "ok": "2460",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2460",
        "ok": "2460",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2460",
        "ok": "2460",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2460",
        "ok": "2460",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2460",
        "ok": "2460",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2460",
        "ok": "2460",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2460",
        "ok": "2460",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-62afe": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_128808",
path: "Animal-api sync for customer NL_128808",
pathFormatted: "req_animal-api-sync-62afe",
stats: {
    "name": "Animal-api sync for customer NL_128808",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3178",
        "ok": "3178",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3178",
        "ok": "3178",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3178",
        "ok": "3178",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3178",
        "ok": "3178",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3178",
        "ok": "3178",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3178",
        "ok": "3178",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3178",
        "ok": "3178",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d8b1c": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_214192",
path: "Animal-api sync for customer BE_214192",
pathFormatted: "req_animal-api-sync-d8b1c",
stats: {
    "name": "Animal-api sync for customer BE_214192",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1479",
        "ok": "1479",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1479",
        "ok": "1479",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1479",
        "ok": "1479",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1479",
        "ok": "1479",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1479",
        "ok": "1479",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1479",
        "ok": "1479",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1479",
        "ok": "1479",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-22fcc": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110755",
path: "Animal-api sync for customer NL_110755",
pathFormatted: "req_animal-api-sync-22fcc",
stats: {
    "name": "Animal-api sync for customer NL_110755",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2206",
        "ok": "2206",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2206",
        "ok": "2206",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2206",
        "ok": "2206",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2206",
        "ok": "2206",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2206",
        "ok": "2206",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2206",
        "ok": "2206",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2206",
        "ok": "2206",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-1330d": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_145902",
path: "Animal-api sync for customer BE_145902",
pathFormatted: "req_animal-api-sync-1330d",
stats: {
    "name": "Animal-api sync for customer BE_145902",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2358",
        "ok": "2358",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2358",
        "ok": "2358",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2358",
        "ok": "2358",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2358",
        "ok": "2358",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2358",
        "ok": "2358",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2358",
        "ok": "2358",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2358",
        "ok": "2358",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-2cece": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110572",
path: "Animal-api sync for customer NL_110572",
pathFormatted: "req_animal-api-sync-2cece",
stats: {
    "name": "Animal-api sync for customer NL_110572",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2076",
        "ok": "2076",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2076",
        "ok": "2076",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2076",
        "ok": "2076",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2076",
        "ok": "2076",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2076",
        "ok": "2076",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2076",
        "ok": "2076",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2076",
        "ok": "2076",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-184df": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110881",
path: "Animal-api sync for customer NL_110881",
pathFormatted: "req_animal-api-sync-184df",
stats: {
    "name": "Animal-api sync for customer NL_110881",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2026",
        "ok": "2026",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2026",
        "ok": "2026",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2026",
        "ok": "2026",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2026",
        "ok": "2026",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2026",
        "ok": "2026",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2026",
        "ok": "2026",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2026",
        "ok": "2026",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-61fa0": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_214412",
path: "Animal-api sync for customer NL_214412",
pathFormatted: "req_animal-api-sync-61fa0",
stats: {
    "name": "Animal-api sync for customer NL_214412",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2063",
        "ok": "2063",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2063",
        "ok": "2063",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2063",
        "ok": "2063",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2063",
        "ok": "2063",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2063",
        "ok": "2063",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2063",
        "ok": "2063",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2063",
        "ok": "2063",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-2f673": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_112286",
path: "Animal-api sync for customer NL_112286",
pathFormatted: "req_animal-api-sync-2f673",
stats: {
    "name": "Animal-api sync for customer NL_112286",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1471",
        "ok": "1471",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1471",
        "ok": "1471",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1471",
        "ok": "1471",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1471",
        "ok": "1471",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1471",
        "ok": "1471",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1471",
        "ok": "1471",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1471",
        "ok": "1471",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-7a391": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_125864",
path: "Animal-api sync for customer NL_125864",
pathFormatted: "req_animal-api-sync-7a391",
stats: {
    "name": "Animal-api sync for customer NL_125864",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1970",
        "ok": "1970",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1970",
        "ok": "1970",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1970",
        "ok": "1970",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1970",
        "ok": "1970",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1970",
        "ok": "1970",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1970",
        "ok": "1970",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1970",
        "ok": "1970",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-844f0": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_128832",
path: "Animal-api sync for customer NL_128832",
pathFormatted: "req_animal-api-sync-844f0",
stats: {
    "name": "Animal-api sync for customer NL_128832",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2517",
        "ok": "2517",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2517",
        "ok": "2517",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2517",
        "ok": "2517",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2517",
        "ok": "2517",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2517",
        "ok": "2517",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2517",
        "ok": "2517",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2517",
        "ok": "2517",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-4e25d": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104043",
path: "Animal-api sync for customer NL_104043",
pathFormatted: "req_animal-api-sync-4e25d",
stats: {
    "name": "Animal-api sync for customer NL_104043",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2211",
        "ok": "2211",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2211",
        "ok": "2211",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2211",
        "ok": "2211",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2211",
        "ok": "2211",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2211",
        "ok": "2211",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2211",
        "ok": "2211",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2211",
        "ok": "2211",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-25426": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_114351",
path: "Animal-api sync for customer NL_114351",
pathFormatted: "req_animal-api-sync-25426",
stats: {
    "name": "Animal-api sync for customer NL_114351",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1624",
        "ok": "1624",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1624",
        "ok": "1624",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1624",
        "ok": "1624",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1624",
        "ok": "1624",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1624",
        "ok": "1624",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1624",
        "ok": "1624",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1624",
        "ok": "1624",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-dfb65": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_108668",
path: "Animal-api sync for customer NL_108668",
pathFormatted: "req_animal-api-sync-dfb65",
stats: {
    "name": "Animal-api sync for customer NL_108668",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1902",
        "ok": "1902",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1902",
        "ok": "1902",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1902",
        "ok": "1902",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1902",
        "ok": "1902",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1902",
        "ok": "1902",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1902",
        "ok": "1902",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1902",
        "ok": "1902",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-997d2": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111823",
path: "Animal-api sync for customer NL_111823",
pathFormatted: "req_animal-api-sync-997d2",
stats: {
    "name": "Animal-api sync for customer NL_111823",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2376",
        "ok": "2376",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2376",
        "ok": "2376",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2376",
        "ok": "2376",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2376",
        "ok": "2376",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2376",
        "ok": "2376",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2376",
        "ok": "2376",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2376",
        "ok": "2376",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d5cc8": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_103579",
path: "Animal-api sync for customer NL_103579",
pathFormatted: "req_animal-api-sync-d5cc8",
stats: {
    "name": "Animal-api sync for customer NL_103579",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1858",
        "ok": "1858",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1858",
        "ok": "1858",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1858",
        "ok": "1858",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1858",
        "ok": "1858",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1858",
        "ok": "1858",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1858",
        "ok": "1858",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1858",
        "ok": "1858",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-96467": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_134577",
path: "Animal-api sync for customer NL_134577",
pathFormatted: "req_animal-api-sync-96467",
stats: {
    "name": "Animal-api sync for customer NL_134577",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1989",
        "ok": "1989",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1989",
        "ok": "1989",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1989",
        "ok": "1989",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1989",
        "ok": "1989",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1989",
        "ok": "1989",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1989",
        "ok": "1989",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1989",
        "ok": "1989",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-4c254": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105650",
path: "Animal-api sync for customer NL_105650",
pathFormatted: "req_animal-api-sync-4c254",
stats: {
    "name": "Animal-api sync for customer NL_105650",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2019",
        "ok": "2019",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2019",
        "ok": "2019",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2019",
        "ok": "2019",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2019",
        "ok": "2019",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2019",
        "ok": "2019",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2019",
        "ok": "2019",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2019",
        "ok": "2019",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-5ea6b": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_194847",
path: "Animal-api sync for customer BE_194847",
pathFormatted: "req_animal-api-sync-5ea6b",
stats: {
    "name": "Animal-api sync for customer BE_194847",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3236",
        "ok": "3236",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3236",
        "ok": "3236",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3236",
        "ok": "3236",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3236",
        "ok": "3236",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3236",
        "ok": "3236",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3236",
        "ok": "3236",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3236",
        "ok": "3236",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a4924": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117341",
path: "Animal-api sync for customer NL_117341",
pathFormatted: "req_animal-api-sync-a4924",
stats: {
    "name": "Animal-api sync for customer NL_117341",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1730",
        "ok": "1730",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1730",
        "ok": "1730",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1730",
        "ok": "1730",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1730",
        "ok": "1730",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1730",
        "ok": "1730",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1730",
        "ok": "1730",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1730",
        "ok": "1730",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-12331": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_113115",
path: "Animal-api sync for customer NL_113115",
pathFormatted: "req_animal-api-sync-12331",
stats: {
    "name": "Animal-api sync for customer NL_113115",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3036",
        "ok": "3036",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3036",
        "ok": "3036",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3036",
        "ok": "3036",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3036",
        "ok": "3036",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3036",
        "ok": "3036",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3036",
        "ok": "3036",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3036",
        "ok": "3036",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-13f62": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_149314",
path: "Animal-api sync for customer BE_149314",
pathFormatted: "req_animal-api-sync-13f62",
stats: {
    "name": "Animal-api sync for customer BE_149314",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2058",
        "ok": "2058",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2058",
        "ok": "2058",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2058",
        "ok": "2058",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2058",
        "ok": "2058",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2058",
        "ok": "2058",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2058",
        "ok": "2058",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2058",
        "ok": "2058",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-500bc": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117068",
path: "Animal-api sync for customer NL_117068",
pathFormatted: "req_animal-api-sync-500bc",
stats: {
    "name": "Animal-api sync for customer NL_117068",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1539",
        "ok": "1539",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1539",
        "ok": "1539",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1539",
        "ok": "1539",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1539",
        "ok": "1539",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1539",
        "ok": "1539",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1539",
        "ok": "1539",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1539",
        "ok": "1539",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-6d4c5": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_122660",
path: "Animal-api sync for customer NL_122660",
pathFormatted: "req_animal-api-sync-6d4c5",
stats: {
    "name": "Animal-api sync for customer NL_122660",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1250",
        "ok": "1250",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1250",
        "ok": "1250",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1250",
        "ok": "1250",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1250",
        "ok": "1250",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1250",
        "ok": "1250",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1250",
        "ok": "1250",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1250",
        "ok": "1250",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-cb570": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104777",
path: "Animal-api sync for customer NL_104777",
pathFormatted: "req_animal-api-sync-cb570",
stats: {
    "name": "Animal-api sync for customer NL_104777",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3081",
        "ok": "3081",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3081",
        "ok": "3081",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3081",
        "ok": "3081",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3081",
        "ok": "3081",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3081",
        "ok": "3081",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3081",
        "ok": "3081",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3081",
        "ok": "3081",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-49e6e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_122219",
path: "Animal-api sync for customer NL_122219",
pathFormatted: "req_animal-api-sync-49e6e",
stats: {
    "name": "Animal-api sync for customer NL_122219",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1812",
        "ok": "1812",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1812",
        "ok": "1812",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1812",
        "ok": "1812",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1812",
        "ok": "1812",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1812",
        "ok": "1812",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1812",
        "ok": "1812",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1812",
        "ok": "1812",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b0552": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_122008",
path: "Animal-api sync for customer NL_122008",
pathFormatted: "req_animal-api-sync-b0552",
stats: {
    "name": "Animal-api sync for customer NL_122008",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4981",
        "ok": "4981",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4981",
        "ok": "4981",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4981",
        "ok": "4981",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4981",
        "ok": "4981",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4981",
        "ok": "4981",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4981",
        "ok": "4981",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4981",
        "ok": "4981",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-7d526": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105576",
path: "Animal-api sync for customer NL_105576",
pathFormatted: "req_animal-api-sync-7d526",
stats: {
    "name": "Animal-api sync for customer NL_105576",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2149",
        "ok": "2149",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2149",
        "ok": "2149",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2149",
        "ok": "2149",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2149",
        "ok": "2149",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2149",
        "ok": "2149",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2149",
        "ok": "2149",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2149",
        "ok": "2149",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a6cf9": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104168",
path: "Animal-api sync for customer NL_104168",
pathFormatted: "req_animal-api-sync-a6cf9",
stats: {
    "name": "Animal-api sync for customer NL_104168",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3527",
        "ok": "3527",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3527",
        "ok": "3527",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3527",
        "ok": "3527",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3527",
        "ok": "3527",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3527",
        "ok": "3527",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3527",
        "ok": "3527",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3527",
        "ok": "3527",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-6809e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117264",
path: "Animal-api sync for customer NL_117264",
pathFormatted: "req_animal-api-sync-6809e",
stats: {
    "name": "Animal-api sync for customer NL_117264",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2603",
        "ok": "2603",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2603",
        "ok": "2603",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2603",
        "ok": "2603",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2603",
        "ok": "2603",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2603",
        "ok": "2603",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2603",
        "ok": "2603",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2603",
        "ok": "2603",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-cb15a": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_114531",
path: "Animal-api sync for customer NL_114531",
pathFormatted: "req_animal-api-sync-cb15a",
stats: {
    "name": "Animal-api sync for customer NL_114531",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2300",
        "ok": "2300",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2300",
        "ok": "2300",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2300",
        "ok": "2300",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2300",
        "ok": "2300",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2300",
        "ok": "2300",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2300",
        "ok": "2300",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2300",
        "ok": "2300",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-aa469": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_158913",
path: "Animal-api sync for customer BE_158913",
pathFormatted: "req_animal-api-sync-aa469",
stats: {
    "name": "Animal-api sync for customer BE_158913",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2467",
        "ok": "2467",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2467",
        "ok": "2467",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2467",
        "ok": "2467",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2467",
        "ok": "2467",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2467",
        "ok": "2467",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2467",
        "ok": "2467",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2467",
        "ok": "2467",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-fc480": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_121609",
path: "Animal-api sync for customer NL_121609",
pathFormatted: "req_animal-api-sync-fc480",
stats: {
    "name": "Animal-api sync for customer NL_121609",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2237",
        "ok": "2237",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2237",
        "ok": "2237",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2237",
        "ok": "2237",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2237",
        "ok": "2237",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2237",
        "ok": "2237",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2237",
        "ok": "2237",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2237",
        "ok": "2237",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-2fa8f": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_103719",
path: "Animal-api sync for customer NL_103719",
pathFormatted: "req_animal-api-sync-2fa8f",
stats: {
    "name": "Animal-api sync for customer NL_103719",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2574",
        "ok": "2574",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2574",
        "ok": "2574",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2574",
        "ok": "2574",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2574",
        "ok": "2574",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2574",
        "ok": "2574",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2574",
        "ok": "2574",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2574",
        "ok": "2574",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c7578": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105839",
path: "Animal-api sync for customer NL_105839",
pathFormatted: "req_animal-api-sync-c7578",
stats: {
    "name": "Animal-api sync for customer NL_105839",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2981",
        "ok": "2981",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2981",
        "ok": "2981",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2981",
        "ok": "2981",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2981",
        "ok": "2981",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2981",
        "ok": "2981",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2981",
        "ok": "2981",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2981",
        "ok": "2981",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-ca9a4": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_123159",
path: "Animal-api sync for customer NL_123159",
pathFormatted: "req_animal-api-sync-ca9a4",
stats: {
    "name": "Animal-api sync for customer NL_123159",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1448",
        "ok": "1448",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1448",
        "ok": "1448",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1448",
        "ok": "1448",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1448",
        "ok": "1448",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1448",
        "ok": "1448",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1448",
        "ok": "1448",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1448",
        "ok": "1448",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d58c5": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_115162",
path: "Animal-api sync for customer NL_115162",
pathFormatted: "req_animal-api-sync-d58c5",
stats: {
    "name": "Animal-api sync for customer NL_115162",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1535",
        "ok": "1535",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1535",
        "ok": "1535",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1535",
        "ok": "1535",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1535",
        "ok": "1535",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1535",
        "ok": "1535",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1535",
        "ok": "1535",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1535",
        "ok": "1535",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d43d8": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_112453",
path: "Animal-api sync for customer NL_112453",
pathFormatted: "req_animal-api-sync-d43d8",
stats: {
    "name": "Animal-api sync for customer NL_112453",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2043",
        "ok": "2043",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2043",
        "ok": "2043",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2043",
        "ok": "2043",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2043",
        "ok": "2043",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2043",
        "ok": "2043",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2043",
        "ok": "2043",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2043",
        "ok": "2043",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-fa50f": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_194938",
path: "Animal-api sync for customer NL_194938",
pathFormatted: "req_animal-api-sync-fa50f",
stats: {
    "name": "Animal-api sync for customer NL_194938",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3039",
        "ok": "3039",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3039",
        "ok": "3039",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3039",
        "ok": "3039",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3039",
        "ok": "3039",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3039",
        "ok": "3039",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3039",
        "ok": "3039",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3039",
        "ok": "3039",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d3cf7": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_123704",
path: "Animal-api sync for customer NL_123704",
pathFormatted: "req_animal-api-sync-d3cf7",
stats: {
    "name": "Animal-api sync for customer NL_123704",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1321",
        "ok": "1321",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1321",
        "ok": "1321",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1321",
        "ok": "1321",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1321",
        "ok": "1321",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1321",
        "ok": "1321",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1321",
        "ok": "1321",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1321",
        "ok": "1321",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e5ad2": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_108858",
path: "Animal-api sync for customer NL_108858",
pathFormatted: "req_animal-api-sync-e5ad2",
stats: {
    "name": "Animal-api sync for customer NL_108858",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3112",
        "ok": "3112",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3112",
        "ok": "3112",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3112",
        "ok": "3112",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3112",
        "ok": "3112",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3112",
        "ok": "3112",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3112",
        "ok": "3112",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3112",
        "ok": "3112",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-23adc": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110836",
path: "Animal-api sync for customer NL_110836",
pathFormatted: "req_animal-api-sync-23adc",
stats: {
    "name": "Animal-api sync for customer NL_110836",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2150",
        "ok": "2150",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2150",
        "ok": "2150",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2150",
        "ok": "2150",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2150",
        "ok": "2150",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2150",
        "ok": "2150",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2150",
        "ok": "2150",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2150",
        "ok": "2150",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-8d897": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_133100",
path: "Animal-api sync for customer NL_133100",
pathFormatted: "req_animal-api-sync-8d897",
stats: {
    "name": "Animal-api sync for customer NL_133100",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2584",
        "ok": "2584",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2584",
        "ok": "2584",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2584",
        "ok": "2584",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2584",
        "ok": "2584",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2584",
        "ok": "2584",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2584",
        "ok": "2584",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2584",
        "ok": "2584",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e921e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_122446",
path: "Animal-api sync for customer NL_122446",
pathFormatted: "req_animal-api-sync-e921e",
stats: {
    "name": "Animal-api sync for customer NL_122446",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2197",
        "ok": "2197",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2197",
        "ok": "2197",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2197",
        "ok": "2197",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2197",
        "ok": "2197",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2197",
        "ok": "2197",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2197",
        "ok": "2197",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2197",
        "ok": "2197",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-fd312": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117690",
path: "Animal-api sync for customer NL_117690",
pathFormatted: "req_animal-api-sync-fd312",
stats: {
    "name": "Animal-api sync for customer NL_117690",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2556",
        "ok": "2556",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2556",
        "ok": "2556",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2556",
        "ok": "2556",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2556",
        "ok": "2556",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2556",
        "ok": "2556",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2556",
        "ok": "2556",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2556",
        "ok": "2556",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c6f3a": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_119414",
path: "Animal-api sync for customer NL_119414",
pathFormatted: "req_animal-api-sync-c6f3a",
stats: {
    "name": "Animal-api sync for customer NL_119414",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2077",
        "ok": "2077",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2077",
        "ok": "2077",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2077",
        "ok": "2077",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2077",
        "ok": "2077",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2077",
        "ok": "2077",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2077",
        "ok": "2077",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2077",
        "ok": "2077",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a8229": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_129350",
path: "Animal-api sync for customer NL_129350",
pathFormatted: "req_animal-api-sync-a8229",
stats: {
    "name": "Animal-api sync for customer NL_129350",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5802",
        "ok": "5802",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5802",
        "ok": "5802",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5802",
        "ok": "5802",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5802",
        "ok": "5802",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5802",
        "ok": "5802",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5802",
        "ok": "5802",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5802",
        "ok": "5802",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e7e4c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105106",
path: "Animal-api sync for customer NL_105106",
pathFormatted: "req_animal-api-sync-e7e4c",
stats: {
    "name": "Animal-api sync for customer NL_105106",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1869",
        "ok": "1869",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1869",
        "ok": "1869",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1869",
        "ok": "1869",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1869",
        "ok": "1869",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1869",
        "ok": "1869",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1869",
        "ok": "1869",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1869",
        "ok": "1869",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c3423": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_112704",
path: "Animal-api sync for customer NL_112704",
pathFormatted: "req_animal-api-sync-c3423",
stats: {
    "name": "Animal-api sync for customer NL_112704",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5042",
        "ok": "5042",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5042",
        "ok": "5042",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5042",
        "ok": "5042",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5042",
        "ok": "5042",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5042",
        "ok": "5042",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5042",
        "ok": "5042",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5042",
        "ok": "5042",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c2f39": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_157878",
path: "Animal-api sync for customer BE_157878",
pathFormatted: "req_animal-api-sync-c2f39",
stats: {
    "name": "Animal-api sync for customer BE_157878",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2523",
        "ok": "2523",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2523",
        "ok": "2523",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2523",
        "ok": "2523",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2523",
        "ok": "2523",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2523",
        "ok": "2523",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2523",
        "ok": "2523",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2523",
        "ok": "2523",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-fee59": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117006",
path: "Animal-api sync for customer NL_117006",
pathFormatted: "req_animal-api-sync-fee59",
stats: {
    "name": "Animal-api sync for customer NL_117006",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1459",
        "ok": "1459",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1459",
        "ok": "1459",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1459",
        "ok": "1459",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1459",
        "ok": "1459",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1459",
        "ok": "1459",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1459",
        "ok": "1459",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1459",
        "ok": "1459",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-bab68": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_134874",
path: "Animal-api sync for customer NL_134874",
pathFormatted: "req_animal-api-sync-bab68",
stats: {
    "name": "Animal-api sync for customer NL_134874",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2125",
        "ok": "2125",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2125",
        "ok": "2125",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2125",
        "ok": "2125",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2125",
        "ok": "2125",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2125",
        "ok": "2125",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2125",
        "ok": "2125",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2125",
        "ok": "2125",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-3e99c": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_214193",
path: "Animal-api sync for customer BE_214193",
pathFormatted: "req_animal-api-sync-3e99c",
stats: {
    "name": "Animal-api sync for customer BE_214193",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3249",
        "ok": "3249",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3249",
        "ok": "3249",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3249",
        "ok": "3249",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3249",
        "ok": "3249",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3249",
        "ok": "3249",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3249",
        "ok": "3249",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3249",
        "ok": "3249",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b2bc0": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_162281",
path: "Animal-api sync for customer BE_162281",
pathFormatted: "req_animal-api-sync-b2bc0",
stats: {
    "name": "Animal-api sync for customer BE_162281",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3875",
        "ok": "3875",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3875",
        "ok": "3875",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3875",
        "ok": "3875",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3875",
        "ok": "3875",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3875",
        "ok": "3875",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3875",
        "ok": "3875",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3875",
        "ok": "3875",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-046eb": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_127329",
path: "Animal-api sync for customer NL_127329",
pathFormatted: "req_animal-api-sync-046eb",
stats: {
    "name": "Animal-api sync for customer NL_127329",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "983",
        "ok": "983",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "983",
        "ok": "983",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "983",
        "ok": "983",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "983",
        "ok": "983",
        "ko": "-"
    },
    "percentiles2": {
        "total": "983",
        "ok": "983",
        "ko": "-"
    },
    "percentiles3": {
        "total": "983",
        "ok": "983",
        "ko": "-"
    },
    "percentiles4": {
        "total": "983",
        "ok": "983",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-7cc26": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_131009",
path: "Animal-api sync for customer NL_131009",
pathFormatted: "req_animal-api-sync-7cc26",
stats: {
    "name": "Animal-api sync for customer NL_131009",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2004",
        "ok": "2004",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2004",
        "ok": "2004",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2004",
        "ok": "2004",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2004",
        "ok": "2004",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2004",
        "ok": "2004",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2004",
        "ok": "2004",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2004",
        "ok": "2004",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-52d91": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_163987",
path: "Animal-api sync for customer BE_163987",
pathFormatted: "req_animal-api-sync-52d91",
stats: {
    "name": "Animal-api sync for customer BE_163987",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2644",
        "ok": "2644",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2644",
        "ok": "2644",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2644",
        "ok": "2644",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2644",
        "ok": "2644",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2644",
        "ok": "2644",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2644",
        "ok": "2644",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2644",
        "ok": "2644",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-9b768": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_115732",
path: "Animal-api sync for customer NL_115732",
pathFormatted: "req_animal-api-sync-9b768",
stats: {
    "name": "Animal-api sync for customer NL_115732",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2230",
        "ok": "2230",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2230",
        "ok": "2230",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2230",
        "ok": "2230",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2230",
        "ok": "2230",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2230",
        "ok": "2230",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2230",
        "ok": "2230",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2230",
        "ok": "2230",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c1337": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104350",
path: "Animal-api sync for customer NL_104350",
pathFormatted: "req_animal-api-sync-c1337",
stats: {
    "name": "Animal-api sync for customer NL_104350",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3740",
        "ok": "3740",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3740",
        "ok": "3740",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3740",
        "ok": "3740",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3740",
        "ok": "3740",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3740",
        "ok": "3740",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3740",
        "ok": "3740",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3740",
        "ok": "3740",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-de861": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_138595",
path: "Animal-api sync for customer NL_138595",
pathFormatted: "req_animal-api-sync-de861",
stats: {
    "name": "Animal-api sync for customer NL_138595",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1359",
        "ok": "1359",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1359",
        "ok": "1359",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1359",
        "ok": "1359",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1359",
        "ok": "1359",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1359",
        "ok": "1359",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1359",
        "ok": "1359",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1359",
        "ok": "1359",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-cc8e5": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_123944",
path: "Animal-api sync for customer NL_123944",
pathFormatted: "req_animal-api-sync-cc8e5",
stats: {
    "name": "Animal-api sync for customer NL_123944",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1999",
        "ok": "1999",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1999",
        "ok": "1999",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1999",
        "ok": "1999",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1999",
        "ok": "1999",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1999",
        "ok": "1999",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1999",
        "ok": "1999",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1999",
        "ok": "1999",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-033e7": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_113983",
path: "Animal-api sync for customer NL_113983",
pathFormatted: "req_animal-api-sync-033e7",
stats: {
    "name": "Animal-api sync for customer NL_113983",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1407",
        "ok": "1407",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1407",
        "ok": "1407",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1407",
        "ok": "1407",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1407",
        "ok": "1407",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1407",
        "ok": "1407",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1407",
        "ok": "1407",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1407",
        "ok": "1407",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-cb74a": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_131236",
path: "Animal-api sync for customer NL_131236",
pathFormatted: "req_animal-api-sync-cb74a",
stats: {
    "name": "Animal-api sync for customer NL_131236",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1758",
        "ok": "1758",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1758",
        "ok": "1758",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1758",
        "ok": "1758",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1758",
        "ok": "1758",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1758",
        "ok": "1758",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1758",
        "ok": "1758",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1758",
        "ok": "1758",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-4c33d": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_131701",
path: "Animal-api sync for customer NL_131701",
pathFormatted: "req_animal-api-sync-4c33d",
stats: {
    "name": "Animal-api sync for customer NL_131701",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1621",
        "ok": "1621",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1621",
        "ok": "1621",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1621",
        "ok": "1621",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1621",
        "ok": "1621",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1621",
        "ok": "1621",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1621",
        "ok": "1621",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1621",
        "ok": "1621",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-98e1b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_129146",
path: "Animal-api sync for customer NL_129146",
pathFormatted: "req_animal-api-sync-98e1b",
stats: {
    "name": "Animal-api sync for customer NL_129146",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4425",
        "ok": "4425",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4425",
        "ok": "4425",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4425",
        "ok": "4425",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4425",
        "ok": "4425",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4425",
        "ok": "4425",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4425",
        "ok": "4425",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4425",
        "ok": "4425",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e8633": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117217",
path: "Animal-api sync for customer NL_117217",
pathFormatted: "req_animal-api-sync-e8633",
stats: {
    "name": "Animal-api sync for customer NL_117217",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1470",
        "ok": "1470",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1470",
        "ok": "1470",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1470",
        "ok": "1470",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1470",
        "ok": "1470",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1470",
        "ok": "1470",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1470",
        "ok": "1470",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1470",
        "ok": "1470",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-748ee": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_118193",
path: "Animal-api sync for customer NL_118193",
pathFormatted: "req_animal-api-sync-748ee",
stats: {
    "name": "Animal-api sync for customer NL_118193",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1582",
        "ok": "1582",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1582",
        "ok": "1582",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1582",
        "ok": "1582",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1582",
        "ok": "1582",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1582",
        "ok": "1582",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1582",
        "ok": "1582",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1582",
        "ok": "1582",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-ac942": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_162293",
path: "Animal-api sync for customer BE_162293",
pathFormatted: "req_animal-api-sync-ac942",
stats: {
    "name": "Animal-api sync for customer BE_162293",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3133",
        "ok": "3133",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3133",
        "ok": "3133",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3133",
        "ok": "3133",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3133",
        "ok": "3133",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3133",
        "ok": "3133",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3133",
        "ok": "3133",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3133",
        "ok": "3133",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-18df0": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_132381",
path: "Animal-api sync for customer NL_132381",
pathFormatted: "req_animal-api-sync-18df0",
stats: {
    "name": "Animal-api sync for customer NL_132381",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1923",
        "ok": "1923",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1923",
        "ok": "1923",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1923",
        "ok": "1923",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1923",
        "ok": "1923",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1923",
        "ok": "1923",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1923",
        "ok": "1923",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1923",
        "ok": "1923",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-7e345": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111388",
path: "Animal-api sync for customer NL_111388",
pathFormatted: "req_animal-api-sync-7e345",
stats: {
    "name": "Animal-api sync for customer NL_111388",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2572",
        "ok": "2572",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2572",
        "ok": "2572",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2572",
        "ok": "2572",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2572",
        "ok": "2572",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2572",
        "ok": "2572",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2572",
        "ok": "2572",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2572",
        "ok": "2572",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d3a0e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_118547",
path: "Animal-api sync for customer NL_118547",
pathFormatted: "req_animal-api-sync-d3a0e",
stats: {
    "name": "Animal-api sync for customer NL_118547",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2231",
        "ok": "2231",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2231",
        "ok": "2231",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2231",
        "ok": "2231",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2231",
        "ok": "2231",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2231",
        "ok": "2231",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2231",
        "ok": "2231",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2231",
        "ok": "2231",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-3631b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105143",
path: "Animal-api sync for customer NL_105143",
pathFormatted: "req_animal-api-sync-3631b",
stats: {
    "name": "Animal-api sync for customer NL_105143",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3112",
        "ok": "3112",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3112",
        "ok": "3112",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3112",
        "ok": "3112",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3112",
        "ok": "3112",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3112",
        "ok": "3112",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3112",
        "ok": "3112",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3112",
        "ok": "3112",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-f99c5": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_131859",
path: "Animal-api sync for customer NL_131859",
pathFormatted: "req_animal-api-sync-f99c5",
stats: {
    "name": "Animal-api sync for customer NL_131859",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1749",
        "ok": "1749",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1749",
        "ok": "1749",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1749",
        "ok": "1749",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1749",
        "ok": "1749",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1749",
        "ok": "1749",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1749",
        "ok": "1749",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1749",
        "ok": "1749",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-ba235": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_109254",
path: "Animal-api sync for customer NL_109254",
pathFormatted: "req_animal-api-sync-ba235",
stats: {
    "name": "Animal-api sync for customer NL_109254",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2386",
        "ok": "2386",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2386",
        "ok": "2386",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2386",
        "ok": "2386",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2386",
        "ok": "2386",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2386",
        "ok": "2386",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2386",
        "ok": "2386",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2386",
        "ok": "2386",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e87b4": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_118457",
path: "Animal-api sync for customer NL_118457",
pathFormatted: "req_animal-api-sync-e87b4",
stats: {
    "name": "Animal-api sync for customer NL_118457",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3104",
        "ok": "3104",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3104",
        "ok": "3104",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3104",
        "ok": "3104",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3104",
        "ok": "3104",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3104",
        "ok": "3104",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3104",
        "ok": "3104",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3104",
        "ok": "3104",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-84066": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_136422",
path: "Animal-api sync for customer NL_136422",
pathFormatted: "req_animal-api-sync-84066",
stats: {
    "name": "Animal-api sync for customer NL_136422",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1811",
        "ok": "1811",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1811",
        "ok": "1811",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1811",
        "ok": "1811",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1811",
        "ok": "1811",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1811",
        "ok": "1811",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1811",
        "ok": "1811",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1811",
        "ok": "1811",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-f4249": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_138300",
path: "Animal-api sync for customer NL_138300",
pathFormatted: "req_animal-api-sync-f4249",
stats: {
    "name": "Animal-api sync for customer NL_138300",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2976",
        "ok": "2976",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2976",
        "ok": "2976",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2976",
        "ok": "2976",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2976",
        "ok": "2976",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2976",
        "ok": "2976",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2976",
        "ok": "2976",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2976",
        "ok": "2976",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-32bc3": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_103585",
path: "Animal-api sync for customer NL_103585",
pathFormatted: "req_animal-api-sync-32bc3",
stats: {
    "name": "Animal-api sync for customer NL_103585",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2130",
        "ok": "2130",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2130",
        "ok": "2130",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2130",
        "ok": "2130",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2130",
        "ok": "2130",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2130",
        "ok": "2130",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2130",
        "ok": "2130",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2130",
        "ok": "2130",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-787cb": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_143564",
path: "Animal-api sync for customer NL_143564",
pathFormatted: "req_animal-api-sync-787cb",
stats: {
    "name": "Animal-api sync for customer NL_143564",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1400",
        "ok": "1400",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1400",
        "ok": "1400",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1400",
        "ok": "1400",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1400",
        "ok": "1400",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1400",
        "ok": "1400",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1400",
        "ok": "1400",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1400",
        "ok": "1400",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-0f436": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_107376",
path: "Animal-api sync for customer NL_107376",
pathFormatted: "req_animal-api-sync-0f436",
stats: {
    "name": "Animal-api sync for customer NL_107376",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3186",
        "ok": "3186",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3186",
        "ok": "3186",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3186",
        "ok": "3186",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3186",
        "ok": "3186",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3186",
        "ok": "3186",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3186",
        "ok": "3186",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3186",
        "ok": "3186",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-20cd3": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110661",
path: "Animal-api sync for customer NL_110661",
pathFormatted: "req_animal-api-sync-20cd3",
stats: {
    "name": "Animal-api sync for customer NL_110661",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2877",
        "ok": "2877",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2877",
        "ok": "2877",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2877",
        "ok": "2877",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2877",
        "ok": "2877",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2877",
        "ok": "2877",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2877",
        "ok": "2877",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2877",
        "ok": "2877",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-eb26b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_118799",
path: "Animal-api sync for customer NL_118799",
pathFormatted: "req_animal-api-sync-eb26b",
stats: {
    "name": "Animal-api sync for customer NL_118799",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2332",
        "ok": "2332",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2332",
        "ok": "2332",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2332",
        "ok": "2332",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2332",
        "ok": "2332",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2332",
        "ok": "2332",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2332",
        "ok": "2332",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2332",
        "ok": "2332",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c1388": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_122597",
path: "Animal-api sync for customer NL_122597",
pathFormatted: "req_animal-api-sync-c1388",
stats: {
    "name": "Animal-api sync for customer NL_122597",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4226",
        "ok": "4226",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4226",
        "ok": "4226",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4226",
        "ok": "4226",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4226",
        "ok": "4226",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4226",
        "ok": "4226",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4226",
        "ok": "4226",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4226",
        "ok": "4226",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-28731": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_136181",
path: "Animal-api sync for customer NL_136181",
pathFormatted: "req_animal-api-sync-28731",
stats: {
    "name": "Animal-api sync for customer NL_136181",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2207",
        "ok": "2207",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2207",
        "ok": "2207",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2207",
        "ok": "2207",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2207",
        "ok": "2207",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2207",
        "ok": "2207",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2207",
        "ok": "2207",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2207",
        "ok": "2207",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-da9fa": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_212118",
path: "Animal-api sync for customer NL_212118",
pathFormatted: "req_animal-api-sync-da9fa",
stats: {
    "name": "Animal-api sync for customer NL_212118",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1907",
        "ok": "1907",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1907",
        "ok": "1907",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1907",
        "ok": "1907",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1907",
        "ok": "1907",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1907",
        "ok": "1907",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1907",
        "ok": "1907",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1907",
        "ok": "1907",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-2b764": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111875",
path: "Animal-api sync for customer NL_111875",
pathFormatted: "req_animal-api-sync-2b764",
stats: {
    "name": "Animal-api sync for customer NL_111875",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1994",
        "ok": "1994",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1994",
        "ok": "1994",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1994",
        "ok": "1994",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1994",
        "ok": "1994",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1994",
        "ok": "1994",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1994",
        "ok": "1994",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1994",
        "ok": "1994",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-56095": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_125359",
path: "Animal-api sync for customer NL_125359",
pathFormatted: "req_animal-api-sync-56095",
stats: {
    "name": "Animal-api sync for customer NL_125359",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2189",
        "ok": "2189",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2189",
        "ok": "2189",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2189",
        "ok": "2189",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2189",
        "ok": "2189",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2189",
        "ok": "2189",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2189",
        "ok": "2189",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2189",
        "ok": "2189",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-7c52f": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_112020",
path: "Animal-api sync for customer NL_112020",
pathFormatted: "req_animal-api-sync-7c52f",
stats: {
    "name": "Animal-api sync for customer NL_112020",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2984",
        "ok": "2984",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2984",
        "ok": "2984",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2984",
        "ok": "2984",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2984",
        "ok": "2984",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2984",
        "ok": "2984",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2984",
        "ok": "2984",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2984",
        "ok": "2984",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-1586b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_113408",
path: "Animal-api sync for customer NL_113408",
pathFormatted: "req_animal-api-sync-1586b",
stats: {
    "name": "Animal-api sync for customer NL_113408",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5494",
        "ok": "5494",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5494",
        "ok": "5494",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5494",
        "ok": "5494",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5494",
        "ok": "5494",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5494",
        "ok": "5494",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5494",
        "ok": "5494",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5494",
        "ok": "5494",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-bca62": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_112986",
path: "Animal-api sync for customer NL_112986",
pathFormatted: "req_animal-api-sync-bca62",
stats: {
    "name": "Animal-api sync for customer NL_112986",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3952",
        "ok": "3952",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3952",
        "ok": "3952",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3952",
        "ok": "3952",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3952",
        "ok": "3952",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3952",
        "ok": "3952",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3952",
        "ok": "3952",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3952",
        "ok": "3952",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d8a50": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_109324",
path: "Animal-api sync for customer NL_109324",
pathFormatted: "req_animal-api-sync-d8a50",
stats: {
    "name": "Animal-api sync for customer NL_109324",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4018",
        "ok": "4018",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4018",
        "ok": "4018",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4018",
        "ok": "4018",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4018",
        "ok": "4018",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4018",
        "ok": "4018",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4018",
        "ok": "4018",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4018",
        "ok": "4018",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d4b4d": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_123556",
path: "Animal-api sync for customer NL_123556",
pathFormatted: "req_animal-api-sync-d4b4d",
stats: {
    "name": "Animal-api sync for customer NL_123556",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2087",
        "ok": "2087",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2087",
        "ok": "2087",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2087",
        "ok": "2087",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2087",
        "ok": "2087",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2087",
        "ok": "2087",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2087",
        "ok": "2087",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2087",
        "ok": "2087",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-bc7d0": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_151016",
path: "Animal-api sync for customer BE_151016",
pathFormatted: "req_animal-api-sync-bc7d0",
stats: {
    "name": "Animal-api sync for customer BE_151016",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3622",
        "ok": "3622",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3622",
        "ok": "3622",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3622",
        "ok": "3622",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3622",
        "ok": "3622",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3622",
        "ok": "3622",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3622",
        "ok": "3622",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3622",
        "ok": "3622",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a0b51": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_118379",
path: "Animal-api sync for customer NL_118379",
pathFormatted: "req_animal-api-sync-a0b51",
stats: {
    "name": "Animal-api sync for customer NL_118379",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2876",
        "ok": "2876",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2876",
        "ok": "2876",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2876",
        "ok": "2876",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2876",
        "ok": "2876",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2876",
        "ok": "2876",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2876",
        "ok": "2876",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2876",
        "ok": "2876",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b6f25": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_119444",
path: "Animal-api sync for customer NL_119444",
pathFormatted: "req_animal-api-sync-b6f25",
stats: {
    "name": "Animal-api sync for customer NL_119444",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2687",
        "ok": "2687",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2687",
        "ok": "2687",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2687",
        "ok": "2687",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2687",
        "ok": "2687",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2687",
        "ok": "2687",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2687",
        "ok": "2687",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2687",
        "ok": "2687",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b9666": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_207943",
path: "Animal-api sync for customer BE_207943",
pathFormatted: "req_animal-api-sync-b9666",
stats: {
    "name": "Animal-api sync for customer BE_207943",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2560",
        "ok": "2560",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2560",
        "ok": "2560",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2560",
        "ok": "2560",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2560",
        "ok": "2560",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2560",
        "ok": "2560",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2560",
        "ok": "2560",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2560",
        "ok": "2560",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-662d1": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_211608",
path: "Animal-api sync for customer BE_211608",
pathFormatted: "req_animal-api-sync-662d1",
stats: {
    "name": "Animal-api sync for customer BE_211608",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2694",
        "ok": "2694",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2694",
        "ok": "2694",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2694",
        "ok": "2694",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2694",
        "ok": "2694",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2694",
        "ok": "2694",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2694",
        "ok": "2694",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2694",
        "ok": "2694",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-58eb2": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_125881",
path: "Animal-api sync for customer NL_125881",
pathFormatted: "req_animal-api-sync-58eb2",
stats: {
    "name": "Animal-api sync for customer NL_125881",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1582",
        "ok": "1582",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1582",
        "ok": "1582",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1582",
        "ok": "1582",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1582",
        "ok": "1582",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1582",
        "ok": "1582",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1582",
        "ok": "1582",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1582",
        "ok": "1582",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-cd728": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117817",
path: "Animal-api sync for customer NL_117817",
pathFormatted: "req_animal-api-sync-cd728",
stats: {
    "name": "Animal-api sync for customer NL_117817",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2391",
        "ok": "2391",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2391",
        "ok": "2391",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2391",
        "ok": "2391",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2391",
        "ok": "2391",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2391",
        "ok": "2391",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2391",
        "ok": "2391",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2391",
        "ok": "2391",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d0381": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_126231",
path: "Animal-api sync for customer NL_126231",
pathFormatted: "req_animal-api-sync-d0381",
stats: {
    "name": "Animal-api sync for customer NL_126231",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2372",
        "ok": "2372",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2372",
        "ok": "2372",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2372",
        "ok": "2372",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2372",
        "ok": "2372",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2372",
        "ok": "2372",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2372",
        "ok": "2372",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2372",
        "ok": "2372",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-1164c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104369",
path: "Animal-api sync for customer NL_104369",
pathFormatted: "req_animal-api-sync-1164c",
stats: {
    "name": "Animal-api sync for customer NL_104369",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2857",
        "ok": "2857",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2857",
        "ok": "2857",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2857",
        "ok": "2857",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2857",
        "ok": "2857",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2857",
        "ok": "2857",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2857",
        "ok": "2857",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2857",
        "ok": "2857",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c2774": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_130077",
path: "Animal-api sync for customer NL_130077",
pathFormatted: "req_animal-api-sync-c2774",
stats: {
    "name": "Animal-api sync for customer NL_130077",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3076",
        "ok": "3076",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3076",
        "ok": "3076",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3076",
        "ok": "3076",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3076",
        "ok": "3076",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3076",
        "ok": "3076",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3076",
        "ok": "3076",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3076",
        "ok": "3076",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-da2a1": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105775",
path: "Animal-api sync for customer NL_105775",
pathFormatted: "req_animal-api-sync-da2a1",
stats: {
    "name": "Animal-api sync for customer NL_105775",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2751",
        "ok": "2751",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2751",
        "ok": "2751",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2751",
        "ok": "2751",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2751",
        "ok": "2751",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2751",
        "ok": "2751",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2751",
        "ok": "2751",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2751",
        "ok": "2751",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-12657": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_212029",
path: "Animal-api sync for customer BE_212029",
pathFormatted: "req_animal-api-sync-12657",
stats: {
    "name": "Animal-api sync for customer BE_212029",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3278",
        "ok": "3278",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3278",
        "ok": "3278",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3278",
        "ok": "3278",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3278",
        "ok": "3278",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3278",
        "ok": "3278",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3278",
        "ok": "3278",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3278",
        "ok": "3278",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a231f": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_119073",
path: "Animal-api sync for customer NL_119073",
pathFormatted: "req_animal-api-sync-a231f",
stats: {
    "name": "Animal-api sync for customer NL_119073",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2511",
        "ok": "2511",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2511",
        "ok": "2511",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2511",
        "ok": "2511",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2511",
        "ok": "2511",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2511",
        "ok": "2511",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2511",
        "ok": "2511",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2511",
        "ok": "2511",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a5dcb": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_113563",
path: "Animal-api sync for customer NL_113563",
pathFormatted: "req_animal-api-sync-a5dcb",
stats: {
    "name": "Animal-api sync for customer NL_113563",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "6644",
        "ok": "6644",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "6644",
        "ok": "6644",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "6644",
        "ok": "6644",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "6644",
        "ok": "6644",
        "ko": "-"
    },
    "percentiles2": {
        "total": "6644",
        "ok": "6644",
        "ko": "-"
    },
    "percentiles3": {
        "total": "6644",
        "ok": "6644",
        "ko": "-"
    },
    "percentiles4": {
        "total": "6644",
        "ok": "6644",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-0d921": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_124334",
path: "Animal-api sync for customer NL_124334",
pathFormatted: "req_animal-api-sync-0d921",
stats: {
    "name": "Animal-api sync for customer NL_124334",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2020",
        "ok": "2020",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2020",
        "ok": "2020",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2020",
        "ok": "2020",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2020",
        "ok": "2020",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2020",
        "ok": "2020",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2020",
        "ok": "2020",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2020",
        "ok": "2020",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a5308": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_129853",
path: "Animal-api sync for customer NL_129853",
pathFormatted: "req_animal-api-sync-a5308",
stats: {
    "name": "Animal-api sync for customer NL_129853",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1640",
        "ok": "1640",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1640",
        "ok": "1640",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1640",
        "ok": "1640",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1640",
        "ok": "1640",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1640",
        "ok": "1640",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1640",
        "ok": "1640",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1640",
        "ok": "1640",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a8587": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_112236",
path: "Animal-api sync for customer NL_112236",
pathFormatted: "req_animal-api-sync-a8587",
stats: {
    "name": "Animal-api sync for customer NL_112236",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2160",
        "ok": "2160",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2160",
        "ok": "2160",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2160",
        "ok": "2160",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2160",
        "ok": "2160",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2160",
        "ok": "2160",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2160",
        "ok": "2160",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2160",
        "ok": "2160",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c1a9b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105397",
path: "Animal-api sync for customer NL_105397",
pathFormatted: "req_animal-api-sync-c1a9b",
stats: {
    "name": "Animal-api sync for customer NL_105397",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2385",
        "ok": "2385",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2385",
        "ok": "2385",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2385",
        "ok": "2385",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2385",
        "ok": "2385",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2385",
        "ok": "2385",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2385",
        "ok": "2385",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2385",
        "ok": "2385",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-f47d9": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_162268",
path: "Animal-api sync for customer NL_162268",
pathFormatted: "req_animal-api-sync-f47d9",
stats: {
    "name": "Animal-api sync for customer NL_162268",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3862",
        "ok": "3862",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3862",
        "ok": "3862",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3862",
        "ok": "3862",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3862",
        "ok": "3862",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3862",
        "ok": "3862",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3862",
        "ok": "3862",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3862",
        "ok": "3862",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-7141e": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_195066",
path: "Animal-api sync for customer BE_195066",
pathFormatted: "req_animal-api-sync-7141e",
stats: {
    "name": "Animal-api sync for customer BE_195066",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2786",
        "ok": "2786",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2786",
        "ok": "2786",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2786",
        "ok": "2786",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2786",
        "ok": "2786",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2786",
        "ok": "2786",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2786",
        "ok": "2786",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2786",
        "ok": "2786",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-9fb14": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_115364",
path: "Animal-api sync for customer NL_115364",
pathFormatted: "req_animal-api-sync-9fb14",
stats: {
    "name": "Animal-api sync for customer NL_115364",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2370",
        "ok": "2370",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2370",
        "ok": "2370",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2370",
        "ok": "2370",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2370",
        "ok": "2370",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2370",
        "ok": "2370",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2370",
        "ok": "2370",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2370",
        "ok": "2370",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-29ae3": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_107774",
path: "Animal-api sync for customer NL_107774",
pathFormatted: "req_animal-api-sync-29ae3",
stats: {
    "name": "Animal-api sync for customer NL_107774",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3910",
        "ok": "3910",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3910",
        "ok": "3910",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3910",
        "ok": "3910",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3910",
        "ok": "3910",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3910",
        "ok": "3910",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3910",
        "ok": "3910",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3910",
        "ok": "3910",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e24f9": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_125084",
path: "Animal-api sync for customer NL_125084",
pathFormatted: "req_animal-api-sync-e24f9",
stats: {
    "name": "Animal-api sync for customer NL_125084",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2064",
        "ok": "2064",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2064",
        "ok": "2064",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2064",
        "ok": "2064",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2064",
        "ok": "2064",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2064",
        "ok": "2064",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2064",
        "ok": "2064",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2064",
        "ok": "2064",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-6bcf1": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_130692",
path: "Animal-api sync for customer NL_130692",
pathFormatted: "req_animal-api-sync-6bcf1",
stats: {
    "name": "Animal-api sync for customer NL_130692",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2307",
        "ok": "2307",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2307",
        "ok": "2307",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2307",
        "ok": "2307",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2307",
        "ok": "2307",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2307",
        "ok": "2307",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2307",
        "ok": "2307",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2307",
        "ok": "2307",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-42cac": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117138",
path: "Animal-api sync for customer NL_117138",
pathFormatted: "req_animal-api-sync-42cac",
stats: {
    "name": "Animal-api sync for customer NL_117138",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2929",
        "ok": "2929",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2929",
        "ok": "2929",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2929",
        "ok": "2929",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2929",
        "ok": "2929",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2929",
        "ok": "2929",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2929",
        "ok": "2929",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2929",
        "ok": "2929",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-7ddaa": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_127532",
path: "Animal-api sync for customer NL_127532",
pathFormatted: "req_animal-api-sync-7ddaa",
stats: {
    "name": "Animal-api sync for customer NL_127532",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2750",
        "ok": "2750",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2750",
        "ok": "2750",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2750",
        "ok": "2750",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2750",
        "ok": "2750",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2750",
        "ok": "2750",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2750",
        "ok": "2750",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2750",
        "ok": "2750",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-35067": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_145405",
path: "Animal-api sync for customer BE_145405",
pathFormatted: "req_animal-api-sync-35067",
stats: {
    "name": "Animal-api sync for customer BE_145405",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3816",
        "ok": "3816",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3816",
        "ok": "3816",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3816",
        "ok": "3816",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3816",
        "ok": "3816",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3816",
        "ok": "3816",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3816",
        "ok": "3816",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3816",
        "ok": "3816",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-2e40c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_131445",
path: "Animal-api sync for customer NL_131445",
pathFormatted: "req_animal-api-sync-2e40c",
stats: {
    "name": "Animal-api sync for customer NL_131445",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2236",
        "ok": "2236",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2236",
        "ok": "2236",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2236",
        "ok": "2236",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2236",
        "ok": "2236",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2236",
        "ok": "2236",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2236",
        "ok": "2236",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2236",
        "ok": "2236",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-3eb24": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_103773",
path: "Animal-api sync for customer NL_103773",
pathFormatted: "req_animal-api-sync-3eb24",
stats: {
    "name": "Animal-api sync for customer NL_103773",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4037",
        "ok": "4037",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4037",
        "ok": "4037",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4037",
        "ok": "4037",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4037",
        "ok": "4037",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4037",
        "ok": "4037",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4037",
        "ok": "4037",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4037",
        "ok": "4037",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-db524": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104670",
path: "Animal-api sync for customer NL_104670",
pathFormatted: "req_animal-api-sync-db524",
stats: {
    "name": "Animal-api sync for customer NL_104670",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3153",
        "ok": "3153",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3153",
        "ok": "3153",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3153",
        "ok": "3153",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3153",
        "ok": "3153",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3153",
        "ok": "3153",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3153",
        "ok": "3153",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3153",
        "ok": "3153",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-8824d": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_129197",
path: "Animal-api sync for customer NL_129197",
pathFormatted: "req_animal-api-sync-8824d",
stats: {
    "name": "Animal-api sync for customer NL_129197",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3910",
        "ok": "3910",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3910",
        "ok": "3910",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3910",
        "ok": "3910",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3910",
        "ok": "3910",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3910",
        "ok": "3910",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3910",
        "ok": "3910",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3910",
        "ok": "3910",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d2007": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_214023",
path: "Animal-api sync for customer BE_214023",
pathFormatted: "req_animal-api-sync-d2007",
stats: {
    "name": "Animal-api sync for customer BE_214023",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2680",
        "ok": "2680",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2680",
        "ok": "2680",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2680",
        "ok": "2680",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2680",
        "ok": "2680",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2680",
        "ok": "2680",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2680",
        "ok": "2680",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2680",
        "ok": "2680",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e79fc": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_114849",
path: "Animal-api sync for customer NL_114849",
pathFormatted: "req_animal-api-sync-e79fc",
stats: {
    "name": "Animal-api sync for customer NL_114849",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2628",
        "ok": "2628",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2628",
        "ok": "2628",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2628",
        "ok": "2628",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2628",
        "ok": "2628",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2628",
        "ok": "2628",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2628",
        "ok": "2628",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2628",
        "ok": "2628",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-6b259": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_118526",
path: "Animal-api sync for customer NL_118526",
pathFormatted: "req_animal-api-sync-6b259",
stats: {
    "name": "Animal-api sync for customer NL_118526",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1628",
        "ok": "1628",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1628",
        "ok": "1628",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1628",
        "ok": "1628",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1628",
        "ok": "1628",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1628",
        "ok": "1628",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1628",
        "ok": "1628",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1628",
        "ok": "1628",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-1f585": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_123243",
path: "Animal-api sync for customer NL_123243",
pathFormatted: "req_animal-api-sync-1f585",
stats: {
    "name": "Animal-api sync for customer NL_123243",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2187",
        "ok": "2187",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2187",
        "ok": "2187",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2187",
        "ok": "2187",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2187",
        "ok": "2187",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2187",
        "ok": "2187",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2187",
        "ok": "2187",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2187",
        "ok": "2187",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-360a2": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_120728",
path: "Animal-api sync for customer NL_120728",
pathFormatted: "req_animal-api-sync-360a2",
stats: {
    "name": "Animal-api sync for customer NL_120728",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3230",
        "ok": "3230",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3230",
        "ok": "3230",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3230",
        "ok": "3230",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3230",
        "ok": "3230",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3230",
        "ok": "3230",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3230",
        "ok": "3230",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3230",
        "ok": "3230",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-964ea": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_131647",
path: "Animal-api sync for customer NL_131647",
pathFormatted: "req_animal-api-sync-964ea",
stats: {
    "name": "Animal-api sync for customer NL_131647",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2355",
        "ok": "2355",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2355",
        "ok": "2355",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2355",
        "ok": "2355",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2355",
        "ok": "2355",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2355",
        "ok": "2355",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2355",
        "ok": "2355",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2355",
        "ok": "2355",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-90a7a": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_158870",
path: "Animal-api sync for customer NL_158870",
pathFormatted: "req_animal-api-sync-90a7a",
stats: {
    "name": "Animal-api sync for customer NL_158870",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "7517",
        "ok": "7517",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "7517",
        "ok": "7517",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "7517",
        "ok": "7517",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "7517",
        "ok": "7517",
        "ko": "-"
    },
    "percentiles2": {
        "total": "7517",
        "ok": "7517",
        "ko": "-"
    },
    "percentiles3": {
        "total": "7517",
        "ok": "7517",
        "ko": "-"
    },
    "percentiles4": {
        "total": "7517",
        "ok": "7517",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-ee74d": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_149842",
path: "Animal-api sync for customer BE_149842",
pathFormatted: "req_animal-api-sync-ee74d",
stats: {
    "name": "Animal-api sync for customer BE_149842",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2396",
        "ok": "2396",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2396",
        "ok": "2396",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2396",
        "ok": "2396",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2396",
        "ok": "2396",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2396",
        "ok": "2396",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2396",
        "ok": "2396",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2396",
        "ok": "2396",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-edbd5": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_156716",
path: "Animal-api sync for customer NL_156716",
pathFormatted: "req_animal-api-sync-edbd5",
stats: {
    "name": "Animal-api sync for customer NL_156716",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2310",
        "ok": "2310",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2310",
        "ok": "2310",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2310",
        "ok": "2310",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2310",
        "ok": "2310",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2310",
        "ok": "2310",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2310",
        "ok": "2310",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2310",
        "ok": "2310",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b6490": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_128966",
path: "Animal-api sync for customer NL_128966",
pathFormatted: "req_animal-api-sync-b6490",
stats: {
    "name": "Animal-api sync for customer NL_128966",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2270",
        "ok": "2270",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2270",
        "ok": "2270",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2270",
        "ok": "2270",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2270",
        "ok": "2270",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2270",
        "ok": "2270",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2270",
        "ok": "2270",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2270",
        "ok": "2270",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-5eb0d": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_129758",
path: "Animal-api sync for customer NL_129758",
pathFormatted: "req_animal-api-sync-5eb0d",
stats: {
    "name": "Animal-api sync for customer NL_129758",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2004",
        "ok": "2004",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2004",
        "ok": "2004",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2004",
        "ok": "2004",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2004",
        "ok": "2004",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2004",
        "ok": "2004",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2004",
        "ok": "2004",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2004",
        "ok": "2004",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-7665a": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_107608",
path: "Animal-api sync for customer NL_107608",
pathFormatted: "req_animal-api-sync-7665a",
stats: {
    "name": "Animal-api sync for customer NL_107608",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2686",
        "ok": "2686",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2686",
        "ok": "2686",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2686",
        "ok": "2686",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2686",
        "ok": "2686",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2686",
        "ok": "2686",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2686",
        "ok": "2686",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2686",
        "ok": "2686",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-352f1": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_120975",
path: "Animal-api sync for customer NL_120975",
pathFormatted: "req_animal-api-sync-352f1",
stats: {
    "name": "Animal-api sync for customer NL_120975",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2156",
        "ok": "2156",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2156",
        "ok": "2156",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2156",
        "ok": "2156",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2156",
        "ok": "2156",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2156",
        "ok": "2156",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2156",
        "ok": "2156",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2156",
        "ok": "2156",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a1b1c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105501",
path: "Animal-api sync for customer NL_105501",
pathFormatted: "req_animal-api-sync-a1b1c",
stats: {
    "name": "Animal-api sync for customer NL_105501",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3405",
        "ok": "3405",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3405",
        "ok": "3405",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3405",
        "ok": "3405",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3405",
        "ok": "3405",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3405",
        "ok": "3405",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3405",
        "ok": "3405",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3405",
        "ok": "3405",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-558ec": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_115428",
path: "Animal-api sync for customer NL_115428",
pathFormatted: "req_animal-api-sync-558ec",
stats: {
    "name": "Animal-api sync for customer NL_115428",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1191",
        "ok": "1191",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1191",
        "ok": "1191",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1191",
        "ok": "1191",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1191",
        "ok": "1191",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1191",
        "ok": "1191",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1191",
        "ok": "1191",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1191",
        "ok": "1191",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e201e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110096",
path: "Animal-api sync for customer NL_110096",
pathFormatted: "req_animal-api-sync-e201e",
stats: {
    "name": "Animal-api sync for customer NL_110096",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2373",
        "ok": "2373",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2373",
        "ok": "2373",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2373",
        "ok": "2373",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2373",
        "ok": "2373",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2373",
        "ok": "2373",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2373",
        "ok": "2373",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2373",
        "ok": "2373",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-66a67": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_115297",
path: "Animal-api sync for customer NL_115297",
pathFormatted: "req_animal-api-sync-66a67",
stats: {
    "name": "Animal-api sync for customer NL_115297",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2486",
        "ok": "2486",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2486",
        "ok": "2486",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2486",
        "ok": "2486",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2486",
        "ok": "2486",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2486",
        "ok": "2486",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2486",
        "ok": "2486",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2486",
        "ok": "2486",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e614b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_114481",
path: "Animal-api sync for customer NL_114481",
pathFormatted: "req_animal-api-sync-e614b",
stats: {
    "name": "Animal-api sync for customer NL_114481",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2311",
        "ok": "2311",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2311",
        "ok": "2311",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2311",
        "ok": "2311",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2311",
        "ok": "2311",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2311",
        "ok": "2311",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2311",
        "ok": "2311",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2311",
        "ok": "2311",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a326b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_109292",
path: "Animal-api sync for customer NL_109292",
pathFormatted: "req_animal-api-sync-a326b",
stats: {
    "name": "Animal-api sync for customer NL_109292",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3064",
        "ok": "3064",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3064",
        "ok": "3064",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3064",
        "ok": "3064",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3064",
        "ok": "3064",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3064",
        "ok": "3064",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3064",
        "ok": "3064",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3064",
        "ok": "3064",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-ec201": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105082",
path: "Animal-api sync for customer NL_105082",
pathFormatted: "req_animal-api-sync-ec201",
stats: {
    "name": "Animal-api sync for customer NL_105082",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2520",
        "ok": "2520",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2520",
        "ok": "2520",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2520",
        "ok": "2520",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2520",
        "ok": "2520",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2520",
        "ok": "2520",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2520",
        "ok": "2520",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2520",
        "ok": "2520",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c9dab": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_122464",
path: "Animal-api sync for customer NL_122464",
pathFormatted: "req_animal-api-sync-c9dab",
stats: {
    "name": "Animal-api sync for customer NL_122464",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2828",
        "ok": "2828",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2828",
        "ok": "2828",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2828",
        "ok": "2828",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2828",
        "ok": "2828",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2828",
        "ok": "2828",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2828",
        "ok": "2828",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2828",
        "ok": "2828",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-2092d": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_150518",
path: "Animal-api sync for customer BE_150518",
pathFormatted: "req_animal-api-sync-2092d",
stats: {
    "name": "Animal-api sync for customer BE_150518",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2648",
        "ok": "2648",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2648",
        "ok": "2648",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2648",
        "ok": "2648",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2648",
        "ok": "2648",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2648",
        "ok": "2648",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2648",
        "ok": "2648",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2648",
        "ok": "2648",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-cdf93": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_118194",
path: "Animal-api sync for customer NL_118194",
pathFormatted: "req_animal-api-sync-cdf93",
stats: {
    "name": "Animal-api sync for customer NL_118194",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2163",
        "ok": "2163",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2163",
        "ok": "2163",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2163",
        "ok": "2163",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2163",
        "ok": "2163",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2163",
        "ok": "2163",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2163",
        "ok": "2163",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2163",
        "ok": "2163",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-ac6d4": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_121952",
path: "Animal-api sync for customer NL_121952",
pathFormatted: "req_animal-api-sync-ac6d4",
stats: {
    "name": "Animal-api sync for customer NL_121952",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3556",
        "ok": "3556",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3556",
        "ok": "3556",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3556",
        "ok": "3556",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3556",
        "ok": "3556",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3556",
        "ok": "3556",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3556",
        "ok": "3556",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3556",
        "ok": "3556",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-0ec02": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_114079",
path: "Animal-api sync for customer NL_114079",
pathFormatted: "req_animal-api-sync-0ec02",
stats: {
    "name": "Animal-api sync for customer NL_114079",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1198",
        "ok": "1198",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1198",
        "ok": "1198",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1198",
        "ok": "1198",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1198",
        "ok": "1198",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1198",
        "ok": "1198",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1198",
        "ok": "1198",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1198",
        "ok": "1198",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-2901a": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_136544",
path: "Animal-api sync for customer NL_136544",
pathFormatted: "req_animal-api-sync-2901a",
stats: {
    "name": "Animal-api sync for customer NL_136544",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2071",
        "ok": "2071",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2071",
        "ok": "2071",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2071",
        "ok": "2071",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2071",
        "ok": "2071",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2071",
        "ok": "2071",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2071",
        "ok": "2071",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2071",
        "ok": "2071",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-1a27a": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_120522",
path: "Animal-api sync for customer NL_120522",
pathFormatted: "req_animal-api-sync-1a27a",
stats: {
    "name": "Animal-api sync for customer NL_120522",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2087",
        "ok": "2087",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2087",
        "ok": "2087",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2087",
        "ok": "2087",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2087",
        "ok": "2087",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2087",
        "ok": "2087",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2087",
        "ok": "2087",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2087",
        "ok": "2087",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-317c3": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_132801",
path: "Animal-api sync for customer NL_132801",
pathFormatted: "req_animal-api-sync-317c3",
stats: {
    "name": "Animal-api sync for customer NL_132801",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1820",
        "ok": "1820",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1820",
        "ok": "1820",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1820",
        "ok": "1820",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1820",
        "ok": "1820",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1820",
        "ok": "1820",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1820",
        "ok": "1820",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1820",
        "ok": "1820",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-83baa": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_114916",
path: "Animal-api sync for customer NL_114916",
pathFormatted: "req_animal-api-sync-83baa",
stats: {
    "name": "Animal-api sync for customer NL_114916",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2789",
        "ok": "2789",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2789",
        "ok": "2789",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2789",
        "ok": "2789",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2789",
        "ok": "2789",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2789",
        "ok": "2789",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2789",
        "ok": "2789",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2789",
        "ok": "2789",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-3eaed": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_108223",
path: "Animal-api sync for customer NL_108223",
pathFormatted: "req_animal-api-sync-3eaed",
stats: {
    "name": "Animal-api sync for customer NL_108223",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3128",
        "ok": "3128",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3128",
        "ok": "3128",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3128",
        "ok": "3128",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3128",
        "ok": "3128",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3128",
        "ok": "3128",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3128",
        "ok": "3128",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3128",
        "ok": "3128",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-6ad60": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_124372",
path: "Animal-api sync for customer NL_124372",
pathFormatted: "req_animal-api-sync-6ad60",
stats: {
    "name": "Animal-api sync for customer NL_124372",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1603",
        "ok": "1603",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1603",
        "ok": "1603",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1603",
        "ok": "1603",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1603",
        "ok": "1603",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1603",
        "ok": "1603",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1603",
        "ok": "1603",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1603",
        "ok": "1603",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-967fa": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_113311",
path: "Animal-api sync for customer NL_113311",
pathFormatted: "req_animal-api-sync-967fa",
stats: {
    "name": "Animal-api sync for customer NL_113311",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2087",
        "ok": "2087",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2087",
        "ok": "2087",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2087",
        "ok": "2087",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2087",
        "ok": "2087",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2087",
        "ok": "2087",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2087",
        "ok": "2087",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2087",
        "ok": "2087",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-3736b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_114775",
path: "Animal-api sync for customer NL_114775",
pathFormatted: "req_animal-api-sync-3736b",
stats: {
    "name": "Animal-api sync for customer NL_114775",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "10430",
        "ok": "10430",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "10430",
        "ok": "10430",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "10430",
        "ok": "10430",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "10430",
        "ok": "10430",
        "ko": "-"
    },
    "percentiles2": {
        "total": "10430",
        "ok": "10430",
        "ko": "-"
    },
    "percentiles3": {
        "total": "10430",
        "ok": "10430",
        "ko": "-"
    },
    "percentiles4": {
        "total": "10430",
        "ok": "10430",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-2e3f2": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_124578",
path: "Animal-api sync for customer NL_124578",
pathFormatted: "req_animal-api-sync-2e3f2",
stats: {
    "name": "Animal-api sync for customer NL_124578",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2365",
        "ok": "2365",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2365",
        "ok": "2365",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2365",
        "ok": "2365",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2365",
        "ok": "2365",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2365",
        "ok": "2365",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2365",
        "ok": "2365",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2365",
        "ok": "2365",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-f6bd1": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_118770",
path: "Animal-api sync for customer NL_118770",
pathFormatted: "req_animal-api-sync-f6bd1",
stats: {
    "name": "Animal-api sync for customer NL_118770",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2360",
        "ok": "2360",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2360",
        "ok": "2360",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2360",
        "ok": "2360",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2360",
        "ok": "2360",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2360",
        "ok": "2360",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2360",
        "ok": "2360",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2360",
        "ok": "2360",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-cfdfb": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111312",
path: "Animal-api sync for customer NL_111312",
pathFormatted: "req_animal-api-sync-cfdfb",
stats: {
    "name": "Animal-api sync for customer NL_111312",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2539",
        "ok": "2539",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2539",
        "ok": "2539",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2539",
        "ok": "2539",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2539",
        "ok": "2539",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2539",
        "ok": "2539",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2539",
        "ok": "2539",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2539",
        "ok": "2539",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c9778": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_119273",
path: "Animal-api sync for customer NL_119273",
pathFormatted: "req_animal-api-sync-c9778",
stats: {
    "name": "Animal-api sync for customer NL_119273",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2361",
        "ok": "2361",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2361",
        "ok": "2361",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2361",
        "ok": "2361",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2361",
        "ok": "2361",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2361",
        "ok": "2361",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2361",
        "ok": "2361",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2361",
        "ok": "2361",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-39d31": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_118474",
path: "Animal-api sync for customer NL_118474",
pathFormatted: "req_animal-api-sync-39d31",
stats: {
    "name": "Animal-api sync for customer NL_118474",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2407",
        "ok": "2407",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2407",
        "ok": "2407",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2407",
        "ok": "2407",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2407",
        "ok": "2407",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2407",
        "ok": "2407",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2407",
        "ok": "2407",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2407",
        "ok": "2407",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-53fa3": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_108741",
path: "Animal-api sync for customer NL_108741",
pathFormatted: "req_animal-api-sync-53fa3",
stats: {
    "name": "Animal-api sync for customer NL_108741",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2157",
        "ok": "2157",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2157",
        "ok": "2157",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2157",
        "ok": "2157",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2157",
        "ok": "2157",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2157",
        "ok": "2157",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2157",
        "ok": "2157",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2157",
        "ok": "2157",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-99401": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111178",
path: "Animal-api sync for customer NL_111178",
pathFormatted: "req_animal-api-sync-99401",
stats: {
    "name": "Animal-api sync for customer NL_111178",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2573",
        "ok": "2573",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2573",
        "ok": "2573",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2573",
        "ok": "2573",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2573",
        "ok": "2573",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2573",
        "ok": "2573",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2573",
        "ok": "2573",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2573",
        "ok": "2573",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-09c2e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_135864",
path: "Animal-api sync for customer NL_135864",
pathFormatted: "req_animal-api-sync-09c2e",
stats: {
    "name": "Animal-api sync for customer NL_135864",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2142",
        "ok": "2142",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2142",
        "ok": "2142",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2142",
        "ok": "2142",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2142",
        "ok": "2142",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2142",
        "ok": "2142",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2142",
        "ok": "2142",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2142",
        "ok": "2142",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-236d8": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_124049",
path: "Animal-api sync for customer NL_124049",
pathFormatted: "req_animal-api-sync-236d8",
stats: {
    "name": "Animal-api sync for customer NL_124049",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2108",
        "ok": "2108",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2108",
        "ok": "2108",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2108",
        "ok": "2108",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2108",
        "ok": "2108",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2108",
        "ok": "2108",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2108",
        "ok": "2108",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2108",
        "ok": "2108",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-882c2": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_128097",
path: "Animal-api sync for customer NL_128097",
pathFormatted: "req_animal-api-sync-882c2",
stats: {
    "name": "Animal-api sync for customer NL_128097",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1382",
        "ok": "1382",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1382",
        "ok": "1382",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1382",
        "ok": "1382",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1382",
        "ok": "1382",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1382",
        "ok": "1382",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1382",
        "ok": "1382",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1382",
        "ok": "1382",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e7614": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_159766",
path: "Animal-api sync for customer BE_159766",
pathFormatted: "req_animal-api-sync-e7614",
stats: {
    "name": "Animal-api sync for customer BE_159766",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5315",
        "ok": "5315",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5315",
        "ok": "5315",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5315",
        "ok": "5315",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5315",
        "ok": "5315",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5315",
        "ok": "5315",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5315",
        "ok": "5315",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5315",
        "ok": "5315",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-52a69": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_212921",
path: "Animal-api sync for customer BE_212921",
pathFormatted: "req_animal-api-sync-52a69",
stats: {
    "name": "Animal-api sync for customer BE_212921",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2196",
        "ok": "2196",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2196",
        "ok": "2196",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2196",
        "ok": "2196",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2196",
        "ok": "2196",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2196",
        "ok": "2196",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2196",
        "ok": "2196",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2196",
        "ok": "2196",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-53e2f": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_127894",
path: "Animal-api sync for customer NL_127894",
pathFormatted: "req_animal-api-sync-53e2f",
stats: {
    "name": "Animal-api sync for customer NL_127894",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2368",
        "ok": "2368",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2368",
        "ok": "2368",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2368",
        "ok": "2368",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2368",
        "ok": "2368",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2368",
        "ok": "2368",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2368",
        "ok": "2368",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2368",
        "ok": "2368",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b4a99": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104247",
path: "Animal-api sync for customer NL_104247",
pathFormatted: "req_animal-api-sync-b4a99",
stats: {
    "name": "Animal-api sync for customer NL_104247",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3055",
        "ok": "3055",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3055",
        "ok": "3055",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3055",
        "ok": "3055",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3055",
        "ok": "3055",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3055",
        "ok": "3055",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3055",
        "ok": "3055",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3055",
        "ok": "3055",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c4c5c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_122906",
path: "Animal-api sync for customer NL_122906",
pathFormatted: "req_animal-api-sync-c4c5c",
stats: {
    "name": "Animal-api sync for customer NL_122906",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2240",
        "ok": "2240",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2240",
        "ok": "2240",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2240",
        "ok": "2240",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2240",
        "ok": "2240",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2240",
        "ok": "2240",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2240",
        "ok": "2240",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2240",
        "ok": "2240",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e42dd": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_108296",
path: "Animal-api sync for customer NL_108296",
pathFormatted: "req_animal-api-sync-e42dd",
stats: {
    "name": "Animal-api sync for customer NL_108296",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1315",
        "ok": "1315",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1315",
        "ok": "1315",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1315",
        "ok": "1315",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1315",
        "ok": "1315",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1315",
        "ok": "1315",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1315",
        "ok": "1315",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1315",
        "ok": "1315",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b8f6b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_120259",
path: "Animal-api sync for customer NL_120259",
pathFormatted: "req_animal-api-sync-b8f6b",
stats: {
    "name": "Animal-api sync for customer NL_120259",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2881",
        "ok": "2881",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2881",
        "ok": "2881",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2881",
        "ok": "2881",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2881",
        "ok": "2881",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2881",
        "ok": "2881",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2881",
        "ok": "2881",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2881",
        "ok": "2881",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-10dc5": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_113215",
path: "Animal-api sync for customer NL_113215",
pathFormatted: "req_animal-api-sync-10dc5",
stats: {
    "name": "Animal-api sync for customer NL_113215",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2817",
        "ok": "2817",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2817",
        "ok": "2817",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2817",
        "ok": "2817",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2817",
        "ok": "2817",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2817",
        "ok": "2817",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2817",
        "ok": "2817",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2817",
        "ok": "2817",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-ee676": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_119868",
path: "Animal-api sync for customer NL_119868",
pathFormatted: "req_animal-api-sync-ee676",
stats: {
    "name": "Animal-api sync for customer NL_119868",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4711",
        "ok": "4711",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4711",
        "ok": "4711",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4711",
        "ok": "4711",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4711",
        "ok": "4711",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4711",
        "ok": "4711",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4711",
        "ok": "4711",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4711",
        "ok": "4711",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-12c8b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_107507",
path: "Animal-api sync for customer NL_107507",
pathFormatted: "req_animal-api-sync-12c8b",
stats: {
    "name": "Animal-api sync for customer NL_107507",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2464",
        "ok": "2464",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2464",
        "ok": "2464",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2464",
        "ok": "2464",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2464",
        "ok": "2464",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2464",
        "ok": "2464",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2464",
        "ok": "2464",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2464",
        "ok": "2464",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-70c3b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_108064",
path: "Animal-api sync for customer NL_108064",
pathFormatted: "req_animal-api-sync-70c3b",
stats: {
    "name": "Animal-api sync for customer NL_108064",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4008",
        "ok": "4008",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4008",
        "ok": "4008",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4008",
        "ok": "4008",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4008",
        "ok": "4008",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4008",
        "ok": "4008",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4008",
        "ok": "4008",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4008",
        "ok": "4008",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d5bee": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_114423",
path: "Animal-api sync for customer NL_114423",
pathFormatted: "req_animal-api-sync-d5bee",
stats: {
    "name": "Animal-api sync for customer NL_114423",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1201",
        "ok": "1201",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1201",
        "ok": "1201",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1201",
        "ok": "1201",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1201",
        "ok": "1201",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1201",
        "ok": "1201",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1201",
        "ok": "1201",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1201",
        "ok": "1201",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-f94cb": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_107241",
path: "Animal-api sync for customer NL_107241",
pathFormatted: "req_animal-api-sync-f94cb",
stats: {
    "name": "Animal-api sync for customer NL_107241",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2577",
        "ok": "2577",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2577",
        "ok": "2577",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2577",
        "ok": "2577",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2577",
        "ok": "2577",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2577",
        "ok": "2577",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2577",
        "ok": "2577",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2577",
        "ok": "2577",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-4b1f5": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_129590",
path: "Animal-api sync for customer NL_129590",
pathFormatted: "req_animal-api-sync-4b1f5",
stats: {
    "name": "Animal-api sync for customer NL_129590",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2987",
        "ok": "2987",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2987",
        "ok": "2987",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2987",
        "ok": "2987",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2987",
        "ok": "2987",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2987",
        "ok": "2987",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2987",
        "ok": "2987",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2987",
        "ok": "2987",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-25bc2": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111740",
path: "Animal-api sync for customer NL_111740",
pathFormatted: "req_animal-api-sync-25bc2",
stats: {
    "name": "Animal-api sync for customer NL_111740",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1830",
        "ok": "1830",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1830",
        "ok": "1830",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1830",
        "ok": "1830",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1830",
        "ok": "1830",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1830",
        "ok": "1830",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1830",
        "ok": "1830",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1830",
        "ok": "1830",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-2efa4": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117125",
path: "Animal-api sync for customer NL_117125",
pathFormatted: "req_animal-api-sync-2efa4",
stats: {
    "name": "Animal-api sync for customer NL_117125",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3308",
        "ok": "3308",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3308",
        "ok": "3308",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3308",
        "ok": "3308",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3308",
        "ok": "3308",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3308",
        "ok": "3308",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3308",
        "ok": "3308",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3308",
        "ok": "3308",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c4fc3": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_216527",
path: "Animal-api sync for customer NL_216527",
pathFormatted: "req_animal-api-sync-c4fc3",
stats: {
    "name": "Animal-api sync for customer NL_216527",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2392",
        "ok": "2392",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2392",
        "ok": "2392",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2392",
        "ok": "2392",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2392",
        "ok": "2392",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2392",
        "ok": "2392",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2392",
        "ok": "2392",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2392",
        "ok": "2392",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-41435": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_119693",
path: "Animal-api sync for customer NL_119693",
pathFormatted: "req_animal-api-sync-41435",
stats: {
    "name": "Animal-api sync for customer NL_119693",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2750",
        "ok": "2750",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2750",
        "ok": "2750",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2750",
        "ok": "2750",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2750",
        "ok": "2750",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2750",
        "ok": "2750",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2750",
        "ok": "2750",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2750",
        "ok": "2750",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e3e3d": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_126029",
path: "Animal-api sync for customer NL_126029",
pathFormatted: "req_animal-api-sync-e3e3d",
stats: {
    "name": "Animal-api sync for customer NL_126029",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3187",
        "ok": "3187",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3187",
        "ok": "3187",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3187",
        "ok": "3187",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3187",
        "ok": "3187",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3187",
        "ok": "3187",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3187",
        "ok": "3187",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3187",
        "ok": "3187",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d8bef": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_114961",
path: "Animal-api sync for customer NL_114961",
pathFormatted: "req_animal-api-sync-d8bef",
stats: {
    "name": "Animal-api sync for customer NL_114961",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2441",
        "ok": "2441",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2441",
        "ok": "2441",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2441",
        "ok": "2441",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2441",
        "ok": "2441",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2441",
        "ok": "2441",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2441",
        "ok": "2441",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2441",
        "ok": "2441",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-40763": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_127944",
path: "Animal-api sync for customer NL_127944",
pathFormatted: "req_animal-api-sync-40763",
stats: {
    "name": "Animal-api sync for customer NL_127944",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1849",
        "ok": "1849",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1849",
        "ok": "1849",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1849",
        "ok": "1849",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1849",
        "ok": "1849",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1849",
        "ok": "1849",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1849",
        "ok": "1849",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1849",
        "ok": "1849",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-9f4a5": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_108715",
path: "Animal-api sync for customer NL_108715",
pathFormatted: "req_animal-api-sync-9f4a5",
stats: {
    "name": "Animal-api sync for customer NL_108715",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1738",
        "ok": "1738",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1738",
        "ok": "1738",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1738",
        "ok": "1738",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1738",
        "ok": "1738",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1738",
        "ok": "1738",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1738",
        "ok": "1738",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1738",
        "ok": "1738",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-0355c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_133793",
path: "Animal-api sync for customer NL_133793",
pathFormatted: "req_animal-api-sync-0355c",
stats: {
    "name": "Animal-api sync for customer NL_133793",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1943",
        "ok": "1943",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1943",
        "ok": "1943",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1943",
        "ok": "1943",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1943",
        "ok": "1943",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1943",
        "ok": "1943",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1943",
        "ok": "1943",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1943",
        "ok": "1943",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-6bc35": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_127470",
path: "Animal-api sync for customer NL_127470",
pathFormatted: "req_animal-api-sync-6bc35",
stats: {
    "name": "Animal-api sync for customer NL_127470",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1997",
        "ok": "1997",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1997",
        "ok": "1997",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1997",
        "ok": "1997",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1997",
        "ok": "1997",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1997",
        "ok": "1997",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1997",
        "ok": "1997",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1997",
        "ok": "1997",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-7eb9d": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104934",
path: "Animal-api sync for customer NL_104934",
pathFormatted: "req_animal-api-sync-7eb9d",
stats: {
    "name": "Animal-api sync for customer NL_104934",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5601",
        "ok": "5601",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5601",
        "ok": "5601",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5601",
        "ok": "5601",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5601",
        "ok": "5601",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5601",
        "ok": "5601",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5601",
        "ok": "5601",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5601",
        "ok": "5601",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-3a032": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_122396",
path: "Animal-api sync for customer NL_122396",
pathFormatted: "req_animal-api-sync-3a032",
stats: {
    "name": "Animal-api sync for customer NL_122396",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1592",
        "ok": "1592",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1592",
        "ok": "1592",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1592",
        "ok": "1592",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1592",
        "ok": "1592",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1592",
        "ok": "1592",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1592",
        "ok": "1592",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1592",
        "ok": "1592",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-1cf92": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105173",
path: "Animal-api sync for customer NL_105173",
pathFormatted: "req_animal-api-sync-1cf92",
stats: {
    "name": "Animal-api sync for customer NL_105173",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2458",
        "ok": "2458",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2458",
        "ok": "2458",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2458",
        "ok": "2458",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2458",
        "ok": "2458",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2458",
        "ok": "2458",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2458",
        "ok": "2458",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2458",
        "ok": "2458",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b22b6": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_131590",
path: "Animal-api sync for customer NL_131590",
pathFormatted: "req_animal-api-sync-b22b6",
stats: {
    "name": "Animal-api sync for customer NL_131590",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1482",
        "ok": "1482",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1482",
        "ok": "1482",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1482",
        "ok": "1482",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1482",
        "ok": "1482",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1482",
        "ok": "1482",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1482",
        "ok": "1482",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1482",
        "ok": "1482",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-1080b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110702",
path: "Animal-api sync for customer NL_110702",
pathFormatted: "req_animal-api-sync-1080b",
stats: {
    "name": "Animal-api sync for customer NL_110702",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1584",
        "ok": "1584",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1584",
        "ok": "1584",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1584",
        "ok": "1584",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1584",
        "ok": "1584",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1584",
        "ok": "1584",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1584",
        "ok": "1584",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1584",
        "ok": "1584",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-8580c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_119732",
path: "Animal-api sync for customer NL_119732",
pathFormatted: "req_animal-api-sync-8580c",
stats: {
    "name": "Animal-api sync for customer NL_119732",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1261",
        "ok": "1261",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1261",
        "ok": "1261",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1261",
        "ok": "1261",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1261",
        "ok": "1261",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1261",
        "ok": "1261",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1261",
        "ok": "1261",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1261",
        "ok": "1261",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-8dfa9": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_116373",
path: "Animal-api sync for customer NL_116373",
pathFormatted: "req_animal-api-sync-8dfa9",
stats: {
    "name": "Animal-api sync for customer NL_116373",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2332",
        "ok": "2332",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2332",
        "ok": "2332",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2332",
        "ok": "2332",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2332",
        "ok": "2332",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2332",
        "ok": "2332",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2332",
        "ok": "2332",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2332",
        "ok": "2332",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c298e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_124623",
path: "Animal-api sync for customer NL_124623",
pathFormatted: "req_animal-api-sync-c298e",
stats: {
    "name": "Animal-api sync for customer NL_124623",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1019",
        "ok": "1019",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1019",
        "ok": "1019",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1019",
        "ok": "1019",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1019",
        "ok": "1019",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1019",
        "ok": "1019",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1019",
        "ok": "1019",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1019",
        "ok": "1019",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-18c50": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_124684",
path: "Animal-api sync for customer NL_124684",
pathFormatted: "req_animal-api-sync-18c50",
stats: {
    "name": "Animal-api sync for customer NL_124684",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1513",
        "ok": "1513",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1513",
        "ok": "1513",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1513",
        "ok": "1513",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1513",
        "ok": "1513",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1513",
        "ok": "1513",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1513",
        "ok": "1513",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1513",
        "ok": "1513",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-11d7b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111681",
path: "Animal-api sync for customer NL_111681",
pathFormatted: "req_animal-api-sync-11d7b",
stats: {
    "name": "Animal-api sync for customer NL_111681",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2105",
        "ok": "2105",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2105",
        "ok": "2105",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2105",
        "ok": "2105",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2105",
        "ok": "2105",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2105",
        "ok": "2105",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2105",
        "ok": "2105",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2105",
        "ok": "2105",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-f0b42": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_107121",
path: "Animal-api sync for customer NL_107121",
pathFormatted: "req_animal-api-sync-f0b42",
stats: {
    "name": "Animal-api sync for customer NL_107121",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4964",
        "ok": "4964",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4964",
        "ok": "4964",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4964",
        "ok": "4964",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4964",
        "ok": "4964",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4964",
        "ok": "4964",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4964",
        "ok": "4964",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4964",
        "ok": "4964",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-de92c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_109851",
path: "Animal-api sync for customer NL_109851",
pathFormatted: "req_animal-api-sync-de92c",
stats: {
    "name": "Animal-api sync for customer NL_109851",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4268",
        "ok": "4268",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4268",
        "ok": "4268",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4268",
        "ok": "4268",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4268",
        "ok": "4268",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4268",
        "ok": "4268",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4268",
        "ok": "4268",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4268",
        "ok": "4268",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-31610": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_109147",
path: "Animal-api sync for customer NL_109147",
pathFormatted: "req_animal-api-sync-31610",
stats: {
    "name": "Animal-api sync for customer NL_109147",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1515",
        "ok": "1515",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1515",
        "ok": "1515",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1515",
        "ok": "1515",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1515",
        "ok": "1515",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1515",
        "ok": "1515",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1515",
        "ok": "1515",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1515",
        "ok": "1515",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a6474": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_217194",
path: "Animal-api sync for customer NL_217194",
pathFormatted: "req_animal-api-sync-a6474",
stats: {
    "name": "Animal-api sync for customer NL_217194",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2746",
        "ok": "2746",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2746",
        "ok": "2746",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2746",
        "ok": "2746",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2746",
        "ok": "2746",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2746",
        "ok": "2746",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2746",
        "ok": "2746",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2746",
        "ok": "2746",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-735e5": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_162624",
path: "Animal-api sync for customer BE_162624",
pathFormatted: "req_animal-api-sync-735e5",
stats: {
    "name": "Animal-api sync for customer BE_162624",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4666",
        "ok": "4666",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4666",
        "ok": "4666",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4666",
        "ok": "4666",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4666",
        "ok": "4666",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4666",
        "ok": "4666",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4666",
        "ok": "4666",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4666",
        "ok": "4666",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-da049": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111349",
path: "Animal-api sync for customer NL_111349",
pathFormatted: "req_animal-api-sync-da049",
stats: {
    "name": "Animal-api sync for customer NL_111349",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2457",
        "ok": "2457",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2457",
        "ok": "2457",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2457",
        "ok": "2457",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2457",
        "ok": "2457",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2457",
        "ok": "2457",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2457",
        "ok": "2457",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2457",
        "ok": "2457",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-0945e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_120830",
path: "Animal-api sync for customer NL_120830",
pathFormatted: "req_animal-api-sync-0945e",
stats: {
    "name": "Animal-api sync for customer NL_120830",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3259",
        "ok": "3259",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3259",
        "ok": "3259",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3259",
        "ok": "3259",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3259",
        "ok": "3259",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3259",
        "ok": "3259",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3259",
        "ok": "3259",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3259",
        "ok": "3259",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d1db6": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_154426",
path: "Animal-api sync for customer BE_154426",
pathFormatted: "req_animal-api-sync-d1db6",
stats: {
    "name": "Animal-api sync for customer BE_154426",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5671",
        "ok": "5671",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5671",
        "ok": "5671",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5671",
        "ok": "5671",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5671",
        "ok": "5671",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5671",
        "ok": "5671",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5671",
        "ok": "5671",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5671",
        "ok": "5671",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-3b46a": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_160734",
path: "Animal-api sync for customer NL_160734",
pathFormatted: "req_animal-api-sync-3b46a",
stats: {
    "name": "Animal-api sync for customer NL_160734",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2552",
        "ok": "2552",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2552",
        "ok": "2552",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2552",
        "ok": "2552",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2552",
        "ok": "2552",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2552",
        "ok": "2552",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2552",
        "ok": "2552",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2552",
        "ok": "2552",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-4b701": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_122804",
path: "Animal-api sync for customer NL_122804",
pathFormatted: "req_animal-api-sync-4b701",
stats: {
    "name": "Animal-api sync for customer NL_122804",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2726",
        "ok": "2726",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2726",
        "ok": "2726",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2726",
        "ok": "2726",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2726",
        "ok": "2726",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2726",
        "ok": "2726",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2726",
        "ok": "2726",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2726",
        "ok": "2726",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-3f46c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110510",
path: "Animal-api sync for customer NL_110510",
pathFormatted: "req_animal-api-sync-3f46c",
stats: {
    "name": "Animal-api sync for customer NL_110510",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2462",
        "ok": "2462",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2462",
        "ok": "2462",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2462",
        "ok": "2462",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2462",
        "ok": "2462",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2462",
        "ok": "2462",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2462",
        "ok": "2462",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2462",
        "ok": "2462",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e1650": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_189253",
path: "Animal-api sync for customer BE_189253",
pathFormatted: "req_animal-api-sync-e1650",
stats: {
    "name": "Animal-api sync for customer BE_189253",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2983",
        "ok": "2983",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2983",
        "ok": "2983",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2983",
        "ok": "2983",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2983",
        "ok": "2983",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2983",
        "ok": "2983",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2983",
        "ok": "2983",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2983",
        "ok": "2983",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-5a990": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_196831",
path: "Animal-api sync for customer BE_196831",
pathFormatted: "req_animal-api-sync-5a990",
stats: {
    "name": "Animal-api sync for customer BE_196831",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3068",
        "ok": "3068",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3068",
        "ok": "3068",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3068",
        "ok": "3068",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3068",
        "ok": "3068",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3068",
        "ok": "3068",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3068",
        "ok": "3068",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3068",
        "ok": "3068",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-8b170": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_128376",
path: "Animal-api sync for customer NL_128376",
pathFormatted: "req_animal-api-sync-8b170",
stats: {
    "name": "Animal-api sync for customer NL_128376",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2930",
        "ok": "2930",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2930",
        "ok": "2930",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2930",
        "ok": "2930",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2930",
        "ok": "2930",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2930",
        "ok": "2930",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2930",
        "ok": "2930",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2930",
        "ok": "2930",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-9e2d7": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_127839",
path: "Animal-api sync for customer NL_127839",
pathFormatted: "req_animal-api-sync-9e2d7",
stats: {
    "name": "Animal-api sync for customer NL_127839",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2109",
        "ok": "2109",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2109",
        "ok": "2109",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2109",
        "ok": "2109",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2109",
        "ok": "2109",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2109",
        "ok": "2109",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2109",
        "ok": "2109",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2109",
        "ok": "2109",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-cb3f0": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_109354",
path: "Animal-api sync for customer NL_109354",
pathFormatted: "req_animal-api-sync-cb3f0",
stats: {
    "name": "Animal-api sync for customer NL_109354",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1716",
        "ok": "1716",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1716",
        "ok": "1716",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1716",
        "ok": "1716",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1716",
        "ok": "1716",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1716",
        "ok": "1716",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1716",
        "ok": "1716",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1716",
        "ok": "1716",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-77e7f": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_116276",
path: "Animal-api sync for customer NL_116276",
pathFormatted: "req_animal-api-sync-77e7f",
stats: {
    "name": "Animal-api sync for customer NL_116276",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2470",
        "ok": "2470",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2470",
        "ok": "2470",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2470",
        "ok": "2470",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2470",
        "ok": "2470",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2470",
        "ok": "2470",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2470",
        "ok": "2470",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2470",
        "ok": "2470",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e91bf": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111232",
path: "Animal-api sync for customer NL_111232",
pathFormatted: "req_animal-api-sync-e91bf",
stats: {
    "name": "Animal-api sync for customer NL_111232",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2062",
        "ok": "2062",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2062",
        "ok": "2062",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2062",
        "ok": "2062",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2062",
        "ok": "2062",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2062",
        "ok": "2062",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2062",
        "ok": "2062",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2062",
        "ok": "2062",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-df608": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_106231",
path: "Animal-api sync for customer NL_106231",
pathFormatted: "req_animal-api-sync-df608",
stats: {
    "name": "Animal-api sync for customer NL_106231",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4107",
        "ok": "4107",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4107",
        "ok": "4107",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4107",
        "ok": "4107",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4107",
        "ok": "4107",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4107",
        "ok": "4107",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4107",
        "ok": "4107",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4107",
        "ok": "4107",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-96235": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_119086",
path: "Animal-api sync for customer NL_119086",
pathFormatted: "req_animal-api-sync-96235",
stats: {
    "name": "Animal-api sync for customer NL_119086",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5099",
        "ok": "5099",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5099",
        "ok": "5099",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5099",
        "ok": "5099",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5099",
        "ok": "5099",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5099",
        "ok": "5099",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5099",
        "ok": "5099",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5099",
        "ok": "5099",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b66f8": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110851",
path: "Animal-api sync for customer NL_110851",
pathFormatted: "req_animal-api-sync-b66f8",
stats: {
    "name": "Animal-api sync for customer NL_110851",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4026",
        "ok": "4026",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4026",
        "ok": "4026",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4026",
        "ok": "4026",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4026",
        "ok": "4026",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4026",
        "ok": "4026",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4026",
        "ok": "4026",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4026",
        "ok": "4026",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-fc706": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_108712",
path: "Animal-api sync for customer NL_108712",
pathFormatted: "req_animal-api-sync-fc706",
stats: {
    "name": "Animal-api sync for customer NL_108712",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2599",
        "ok": "2599",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2599",
        "ok": "2599",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2599",
        "ok": "2599",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2599",
        "ok": "2599",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2599",
        "ok": "2599",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2599",
        "ok": "2599",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2599",
        "ok": "2599",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-77463": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_142162",
path: "Animal-api sync for customer NL_142162",
pathFormatted: "req_animal-api-sync-77463",
stats: {
    "name": "Animal-api sync for customer NL_142162",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3489",
        "ok": "3489",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3489",
        "ok": "3489",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3489",
        "ok": "3489",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3489",
        "ok": "3489",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3489",
        "ok": "3489",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3489",
        "ok": "3489",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3489",
        "ok": "3489",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-3e0f1": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_119459",
path: "Animal-api sync for customer NL_119459",
pathFormatted: "req_animal-api-sync-3e0f1",
stats: {
    "name": "Animal-api sync for customer NL_119459",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2448",
        "ok": "2448",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2448",
        "ok": "2448",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2448",
        "ok": "2448",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2448",
        "ok": "2448",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2448",
        "ok": "2448",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2448",
        "ok": "2448",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2448",
        "ok": "2448",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-f9107": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_121807",
path: "Animal-api sync for customer NL_121807",
pathFormatted: "req_animal-api-sync-f9107",
stats: {
    "name": "Animal-api sync for customer NL_121807",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3217",
        "ok": "3217",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3217",
        "ok": "3217",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3217",
        "ok": "3217",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3217",
        "ok": "3217",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3217",
        "ok": "3217",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3217",
        "ok": "3217",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3217",
        "ok": "3217",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-82f9e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110685",
path: "Animal-api sync for customer NL_110685",
pathFormatted: "req_animal-api-sync-82f9e",
stats: {
    "name": "Animal-api sync for customer NL_110685",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2651",
        "ok": "2651",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2651",
        "ok": "2651",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2651",
        "ok": "2651",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2651",
        "ok": "2651",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2651",
        "ok": "2651",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2651",
        "ok": "2651",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2651",
        "ok": "2651",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-39816": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_145536",
path: "Animal-api sync for customer BE_145536",
pathFormatted: "req_animal-api-sync-39816",
stats: {
    "name": "Animal-api sync for customer BE_145536",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2638",
        "ok": "2638",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2638",
        "ok": "2638",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2638",
        "ok": "2638",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2638",
        "ok": "2638",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2638",
        "ok": "2638",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2638",
        "ok": "2638",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2638",
        "ok": "2638",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-66198": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_107001",
path: "Animal-api sync for customer NL_107001",
pathFormatted: "req_animal-api-sync-66198",
stats: {
    "name": "Animal-api sync for customer NL_107001",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2802",
        "ok": "2802",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2802",
        "ok": "2802",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2802",
        "ok": "2802",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2802",
        "ok": "2802",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2802",
        "ok": "2802",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2802",
        "ok": "2802",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2802",
        "ok": "2802",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-299ce": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_119621",
path: "Animal-api sync for customer NL_119621",
pathFormatted: "req_animal-api-sync-299ce",
stats: {
    "name": "Animal-api sync for customer NL_119621",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2207",
        "ok": "2207",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2207",
        "ok": "2207",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2207",
        "ok": "2207",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2207",
        "ok": "2207",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2207",
        "ok": "2207",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2207",
        "ok": "2207",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2207",
        "ok": "2207",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-cb563": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_195750",
path: "Animal-api sync for customer BE_195750",
pathFormatted: "req_animal-api-sync-cb563",
stats: {
    "name": "Animal-api sync for customer BE_195750",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1918",
        "ok": "1918",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1918",
        "ok": "1918",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1918",
        "ok": "1918",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1918",
        "ok": "1918",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1918",
        "ok": "1918",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1918",
        "ok": "1918",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1918",
        "ok": "1918",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-86128": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110109",
path: "Animal-api sync for customer NL_110109",
pathFormatted: "req_animal-api-sync-86128",
stats: {
    "name": "Animal-api sync for customer NL_110109",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2269",
        "ok": "2269",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2269",
        "ok": "2269",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2269",
        "ok": "2269",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2269",
        "ok": "2269",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2269",
        "ok": "2269",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2269",
        "ok": "2269",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2269",
        "ok": "2269",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-2a7a0": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_119620",
path: "Animal-api sync for customer NL_119620",
pathFormatted: "req_animal-api-sync-2a7a0",
stats: {
    "name": "Animal-api sync for customer NL_119620",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2639",
        "ok": "2639",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2639",
        "ok": "2639",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2639",
        "ok": "2639",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2639",
        "ok": "2639",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2639",
        "ok": "2639",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2639",
        "ok": "2639",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2639",
        "ok": "2639",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-f1bc5": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_106522",
path: "Animal-api sync for customer NL_106522",
pathFormatted: "req_animal-api-sync-f1bc5",
stats: {
    "name": "Animal-api sync for customer NL_106522",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2035",
        "ok": "2035",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2035",
        "ok": "2035",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2035",
        "ok": "2035",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2035",
        "ok": "2035",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2035",
        "ok": "2035",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2035",
        "ok": "2035",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2035",
        "ok": "2035",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-fa8fb": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110675",
path: "Animal-api sync for customer NL_110675",
pathFormatted: "req_animal-api-sync-fa8fb",
stats: {
    "name": "Animal-api sync for customer NL_110675",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1537",
        "ok": "1537",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1537",
        "ok": "1537",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1537",
        "ok": "1537",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1537",
        "ok": "1537",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1537",
        "ok": "1537",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1537",
        "ok": "1537",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1537",
        "ok": "1537",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-9a989": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105564",
path: "Animal-api sync for customer NL_105564",
pathFormatted: "req_animal-api-sync-9a989",
stats: {
    "name": "Animal-api sync for customer NL_105564",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2182",
        "ok": "2182",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2182",
        "ok": "2182",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2182",
        "ok": "2182",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2182",
        "ok": "2182",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2182",
        "ok": "2182",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2182",
        "ok": "2182",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2182",
        "ok": "2182",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-1d50e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_107593",
path: "Animal-api sync for customer NL_107593",
pathFormatted: "req_animal-api-sync-1d50e",
stats: {
    "name": "Animal-api sync for customer NL_107593",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2202",
        "ok": "2202",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2202",
        "ok": "2202",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2202",
        "ok": "2202",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2202",
        "ok": "2202",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2202",
        "ok": "2202",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2202",
        "ok": "2202",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2202",
        "ok": "2202",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-3fad8": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_119768",
path: "Animal-api sync for customer NL_119768",
pathFormatted: "req_animal-api-sync-3fad8",
stats: {
    "name": "Animal-api sync for customer NL_119768",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1853",
        "ok": "1853",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1853",
        "ok": "1853",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1853",
        "ok": "1853",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1853",
        "ok": "1853",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1853",
        "ok": "1853",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1853",
        "ok": "1853",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1853",
        "ok": "1853",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-1cdda": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_106961",
path: "Animal-api sync for customer NL_106961",
pathFormatted: "req_animal-api-sync-1cdda",
stats: {
    "name": "Animal-api sync for customer NL_106961",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1504",
        "ok": "1504",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1504",
        "ok": "1504",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1504",
        "ok": "1504",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1504",
        "ok": "1504",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1504",
        "ok": "1504",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1504",
        "ok": "1504",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1504",
        "ok": "1504",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-028d0": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_139052",
path: "Animal-api sync for customer NL_139052",
pathFormatted: "req_animal-api-sync-028d0",
stats: {
    "name": "Animal-api sync for customer NL_139052",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1118",
        "ok": "1118",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1118",
        "ok": "1118",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1118",
        "ok": "1118",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1118",
        "ok": "1118",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1118",
        "ok": "1118",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1118",
        "ok": "1118",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1118",
        "ok": "1118",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-3df0e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_136411",
path: "Animal-api sync for customer NL_136411",
pathFormatted: "req_animal-api-sync-3df0e",
stats: {
    "name": "Animal-api sync for customer NL_136411",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4116",
        "ok": "4116",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4116",
        "ok": "4116",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4116",
        "ok": "4116",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4116",
        "ok": "4116",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4116",
        "ok": "4116",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4116",
        "ok": "4116",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4116",
        "ok": "4116",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-34074": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104611",
path: "Animal-api sync for customer NL_104611",
pathFormatted: "req_animal-api-sync-34074",
stats: {
    "name": "Animal-api sync for customer NL_104611",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "7044",
        "ok": "7044",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "7044",
        "ok": "7044",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "7044",
        "ok": "7044",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "7044",
        "ok": "7044",
        "ko": "-"
    },
    "percentiles2": {
        "total": "7044",
        "ok": "7044",
        "ko": "-"
    },
    "percentiles3": {
        "total": "7044",
        "ok": "7044",
        "ko": "-"
    },
    "percentiles4": {
        "total": "7044",
        "ok": "7044",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-4e4dd": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_132571",
path: "Animal-api sync for customer NL_132571",
pathFormatted: "req_animal-api-sync-4e4dd",
stats: {
    "name": "Animal-api sync for customer NL_132571",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1730",
        "ok": "1730",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1730",
        "ok": "1730",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1730",
        "ok": "1730",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1730",
        "ok": "1730",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1730",
        "ok": "1730",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1730",
        "ok": "1730",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1730",
        "ok": "1730",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b76bc": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_121619",
path: "Animal-api sync for customer NL_121619",
pathFormatted: "req_animal-api-sync-b76bc",
stats: {
    "name": "Animal-api sync for customer NL_121619",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3626",
        "ok": "3626",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3626",
        "ok": "3626",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3626",
        "ok": "3626",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3626",
        "ok": "3626",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3626",
        "ok": "3626",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3626",
        "ok": "3626",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3626",
        "ok": "3626",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-67b51": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_208384",
path: "Animal-api sync for customer NL_208384",
pathFormatted: "req_animal-api-sync-67b51",
stats: {
    "name": "Animal-api sync for customer NL_208384",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2823",
        "ok": "2823",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2823",
        "ok": "2823",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2823",
        "ok": "2823",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2823",
        "ok": "2823",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2823",
        "ok": "2823",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2823",
        "ok": "2823",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2823",
        "ok": "2823",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-f5467": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_164463",
path: "Animal-api sync for customer NL_164463",
pathFormatted: "req_animal-api-sync-f5467",
stats: {
    "name": "Animal-api sync for customer NL_164463",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2928",
        "ok": "2928",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2928",
        "ok": "2928",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2928",
        "ok": "2928",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2928",
        "ok": "2928",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2928",
        "ok": "2928",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2928",
        "ok": "2928",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2928",
        "ok": "2928",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a921a": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_158726",
path: "Animal-api sync for customer BE_158726",
pathFormatted: "req_animal-api-sync-a921a",
stats: {
    "name": "Animal-api sync for customer BE_158726",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2407",
        "ok": "2407",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2407",
        "ok": "2407",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2407",
        "ok": "2407",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2407",
        "ok": "2407",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2407",
        "ok": "2407",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2407",
        "ok": "2407",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2407",
        "ok": "2407",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-43d17": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_159367",
path: "Animal-api sync for customer NL_159367",
pathFormatted: "req_animal-api-sync-43d17",
stats: {
    "name": "Animal-api sync for customer NL_159367",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1614",
        "ok": "1614",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1614",
        "ok": "1614",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1614",
        "ok": "1614",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1614",
        "ok": "1614",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1614",
        "ok": "1614",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1614",
        "ok": "1614",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1614",
        "ok": "1614",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-7b4fd": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_207082",
path: "Animal-api sync for customer BE_207082",
pathFormatted: "req_animal-api-sync-7b4fd",
stats: {
    "name": "Animal-api sync for customer BE_207082",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3103",
        "ok": "3103",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3103",
        "ok": "3103",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3103",
        "ok": "3103",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3103",
        "ok": "3103",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3103",
        "ok": "3103",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3103",
        "ok": "3103",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3103",
        "ok": "3103",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-774bc": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105254",
path: "Animal-api sync for customer NL_105254",
pathFormatted: "req_animal-api-sync-774bc",
stats: {
    "name": "Animal-api sync for customer NL_105254",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2536",
        "ok": "2536",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2536",
        "ok": "2536",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2536",
        "ok": "2536",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2536",
        "ok": "2536",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2536",
        "ok": "2536",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2536",
        "ok": "2536",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2536",
        "ok": "2536",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-72b5a": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_129227",
path: "Animal-api sync for customer NL_129227",
pathFormatted: "req_animal-api-sync-72b5a",
stats: {
    "name": "Animal-api sync for customer NL_129227",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2061",
        "ok": "2061",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2061",
        "ok": "2061",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2061",
        "ok": "2061",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2061",
        "ok": "2061",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2061",
        "ok": "2061",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2061",
        "ok": "2061",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2061",
        "ok": "2061",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e7477": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_120288",
path: "Animal-api sync for customer NL_120288",
pathFormatted: "req_animal-api-sync-e7477",
stats: {
    "name": "Animal-api sync for customer NL_120288",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2199",
        "ok": "2199",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2199",
        "ok": "2199",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2199",
        "ok": "2199",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2199",
        "ok": "2199",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2199",
        "ok": "2199",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2199",
        "ok": "2199",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2199",
        "ok": "2199",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b6fe1": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_112227",
path: "Animal-api sync for customer NL_112227",
pathFormatted: "req_animal-api-sync-b6fe1",
stats: {
    "name": "Animal-api sync for customer NL_112227",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2191",
        "ok": "2191",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2191",
        "ok": "2191",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2191",
        "ok": "2191",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2191",
        "ok": "2191",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2191",
        "ok": "2191",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2191",
        "ok": "2191",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2191",
        "ok": "2191",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-6891c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111812",
path: "Animal-api sync for customer NL_111812",
pathFormatted: "req_animal-api-sync-6891c",
stats: {
    "name": "Animal-api sync for customer NL_111812",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2846",
        "ok": "2846",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2846",
        "ok": "2846",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2846",
        "ok": "2846",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2846",
        "ok": "2846",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2846",
        "ok": "2846",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2846",
        "ok": "2846",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2846",
        "ok": "2846",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-5324e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_112685",
path: "Animal-api sync for customer NL_112685",
pathFormatted: "req_animal-api-sync-5324e",
stats: {
    "name": "Animal-api sync for customer NL_112685",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1976",
        "ok": "1976",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1976",
        "ok": "1976",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1976",
        "ok": "1976",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1976",
        "ok": "1976",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1976",
        "ok": "1976",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1976",
        "ok": "1976",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1976",
        "ok": "1976",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-39487": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_153606",
path: "Animal-api sync for customer BE_153606",
pathFormatted: "req_animal-api-sync-39487",
stats: {
    "name": "Animal-api sync for customer BE_153606",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2901",
        "ok": "2901",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2901",
        "ok": "2901",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2901",
        "ok": "2901",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2901",
        "ok": "2901",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2901",
        "ok": "2901",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2901",
        "ok": "2901",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2901",
        "ok": "2901",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-115a1": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_106548",
path: "Animal-api sync for customer NL_106548",
pathFormatted: "req_animal-api-sync-115a1",
stats: {
    "name": "Animal-api sync for customer NL_106548",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2458",
        "ok": "2458",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2458",
        "ok": "2458",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2458",
        "ok": "2458",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2458",
        "ok": "2458",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2458",
        "ok": "2458",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2458",
        "ok": "2458",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2458",
        "ok": "2458",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-dfd7c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_139331",
path: "Animal-api sync for customer NL_139331",
pathFormatted: "req_animal-api-sync-dfd7c",
stats: {
    "name": "Animal-api sync for customer NL_139331",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2494",
        "ok": "2494",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2494",
        "ok": "2494",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2494",
        "ok": "2494",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2494",
        "ok": "2494",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2494",
        "ok": "2494",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2494",
        "ok": "2494",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2494",
        "ok": "2494",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c6932": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_133968",
path: "Animal-api sync for customer NL_133968",
pathFormatted: "req_animal-api-sync-c6932",
stats: {
    "name": "Animal-api sync for customer NL_133968",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1646",
        "ok": "1646",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1646",
        "ok": "1646",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1646",
        "ok": "1646",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1646",
        "ok": "1646",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1646",
        "ok": "1646",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1646",
        "ok": "1646",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1646",
        "ok": "1646",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-4cd78": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111414",
path: "Animal-api sync for customer NL_111414",
pathFormatted: "req_animal-api-sync-4cd78",
stats: {
    "name": "Animal-api sync for customer NL_111414",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2488",
        "ok": "2488",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2488",
        "ok": "2488",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2488",
        "ok": "2488",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2488",
        "ok": "2488",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2488",
        "ok": "2488",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2488",
        "ok": "2488",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2488",
        "ok": "2488",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-0fed7": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_109760",
path: "Animal-api sync for customer NL_109760",
pathFormatted: "req_animal-api-sync-0fed7",
stats: {
    "name": "Animal-api sync for customer NL_109760",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1836",
        "ok": "1836",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1836",
        "ok": "1836",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1836",
        "ok": "1836",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1836",
        "ok": "1836",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1836",
        "ok": "1836",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1836",
        "ok": "1836",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1836",
        "ok": "1836",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-f47e6": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_120627",
path: "Animal-api sync for customer NL_120627",
pathFormatted: "req_animal-api-sync-f47e6",
stats: {
    "name": "Animal-api sync for customer NL_120627",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4238",
        "ok": "4238",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4238",
        "ok": "4238",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4238",
        "ok": "4238",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4238",
        "ok": "4238",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4238",
        "ok": "4238",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4238",
        "ok": "4238",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4238",
        "ok": "4238",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-66c16": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_112114",
path: "Animal-api sync for customer NL_112114",
pathFormatted: "req_animal-api-sync-66c16",
stats: {
    "name": "Animal-api sync for customer NL_112114",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1458",
        "ok": "1458",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1458",
        "ok": "1458",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1458",
        "ok": "1458",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1458",
        "ok": "1458",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1458",
        "ok": "1458",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1458",
        "ok": "1458",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1458",
        "ok": "1458",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-79b8e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_116848",
path: "Animal-api sync for customer NL_116848",
pathFormatted: "req_animal-api-sync-79b8e",
stats: {
    "name": "Animal-api sync for customer NL_116848",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2693",
        "ok": "2693",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2693",
        "ok": "2693",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2693",
        "ok": "2693",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2693",
        "ok": "2693",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2693",
        "ok": "2693",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2693",
        "ok": "2693",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2693",
        "ok": "2693",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-5420f": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110280",
path: "Animal-api sync for customer NL_110280",
pathFormatted: "req_animal-api-sync-5420f",
stats: {
    "name": "Animal-api sync for customer NL_110280",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2094",
        "ok": "2094",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2094",
        "ok": "2094",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2094",
        "ok": "2094",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2094",
        "ok": "2094",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2094",
        "ok": "2094",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2094",
        "ok": "2094",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2094",
        "ok": "2094",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-63b47": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111966",
path: "Animal-api sync for customer NL_111966",
pathFormatted: "req_animal-api-sync-63b47",
stats: {
    "name": "Animal-api sync for customer NL_111966",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2286",
        "ok": "2286",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2286",
        "ok": "2286",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2286",
        "ok": "2286",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2286",
        "ok": "2286",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2286",
        "ok": "2286",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2286",
        "ok": "2286",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2286",
        "ok": "2286",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-fbcf4": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_128711",
path: "Animal-api sync for customer NL_128711",
pathFormatted: "req_animal-api-sync-fbcf4",
stats: {
    "name": "Animal-api sync for customer NL_128711",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1985",
        "ok": "1985",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1985",
        "ok": "1985",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1985",
        "ok": "1985",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1985",
        "ok": "1985",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1985",
        "ok": "1985",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1985",
        "ok": "1985",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1985",
        "ok": "1985",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d05fc": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_131083",
path: "Animal-api sync for customer NL_131083",
pathFormatted: "req_animal-api-sync-d05fc",
stats: {
    "name": "Animal-api sync for customer NL_131083",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2228",
        "ok": "2228",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2228",
        "ok": "2228",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2228",
        "ok": "2228",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2228",
        "ok": "2228",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2228",
        "ok": "2228",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2228",
        "ok": "2228",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2228",
        "ok": "2228",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-ed470": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_115514",
path: "Animal-api sync for customer NL_115514",
pathFormatted: "req_animal-api-sync-ed470",
stats: {
    "name": "Animal-api sync for customer NL_115514",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2133",
        "ok": "2133",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2133",
        "ok": "2133",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2133",
        "ok": "2133",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2133",
        "ok": "2133",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2133",
        "ok": "2133",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2133",
        "ok": "2133",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2133",
        "ok": "2133",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-29375": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_121682",
path: "Animal-api sync for customer NL_121682",
pathFormatted: "req_animal-api-sync-29375",
stats: {
    "name": "Animal-api sync for customer NL_121682",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1471",
        "ok": "1471",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1471",
        "ok": "1471",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1471",
        "ok": "1471",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1471",
        "ok": "1471",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1471",
        "ok": "1471",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1471",
        "ok": "1471",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1471",
        "ok": "1471",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-4e713": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111266",
path: "Animal-api sync for customer NL_111266",
pathFormatted: "req_animal-api-sync-4e713",
stats: {
    "name": "Animal-api sync for customer NL_111266",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1922",
        "ok": "1922",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1922",
        "ok": "1922",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1922",
        "ok": "1922",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1922",
        "ok": "1922",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1922",
        "ok": "1922",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1922",
        "ok": "1922",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1922",
        "ok": "1922",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-5df96": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_114543",
path: "Animal-api sync for customer NL_114543",
pathFormatted: "req_animal-api-sync-5df96",
stats: {
    "name": "Animal-api sync for customer NL_114543",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1745",
        "ok": "1745",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1745",
        "ok": "1745",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1745",
        "ok": "1745",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1745",
        "ok": "1745",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1745",
        "ok": "1745",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1745",
        "ok": "1745",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1745",
        "ok": "1745",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-38246": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_118506",
path: "Animal-api sync for customer NL_118506",
pathFormatted: "req_animal-api-sync-38246",
stats: {
    "name": "Animal-api sync for customer NL_118506",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2127",
        "ok": "2127",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2127",
        "ok": "2127",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2127",
        "ok": "2127",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2127",
        "ok": "2127",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2127",
        "ok": "2127",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2127",
        "ok": "2127",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2127",
        "ok": "2127",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-3d430": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_134950",
path: "Animal-api sync for customer NL_134950",
pathFormatted: "req_animal-api-sync-3d430",
stats: {
    "name": "Animal-api sync for customer NL_134950",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1791",
        "ok": "1791",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1791",
        "ok": "1791",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1791",
        "ok": "1791",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1791",
        "ok": "1791",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1791",
        "ok": "1791",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1791",
        "ok": "1791",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1791",
        "ok": "1791",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a667b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_103980",
path: "Animal-api sync for customer NL_103980",
pathFormatted: "req_animal-api-sync-a667b",
stats: {
    "name": "Animal-api sync for customer NL_103980",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "8278",
        "ok": "8278",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "8278",
        "ok": "8278",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "8278",
        "ok": "8278",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "8278",
        "ok": "8278",
        "ko": "-"
    },
    "percentiles2": {
        "total": "8278",
        "ok": "8278",
        "ko": "-"
    },
    "percentiles3": {
        "total": "8278",
        "ok": "8278",
        "ko": "-"
    },
    "percentiles4": {
        "total": "8278",
        "ok": "8278",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-92bed": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105164",
path: "Animal-api sync for customer NL_105164",
pathFormatted: "req_animal-api-sync-92bed",
stats: {
    "name": "Animal-api sync for customer NL_105164",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2201",
        "ok": "2201",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2201",
        "ok": "2201",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2201",
        "ok": "2201",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2201",
        "ok": "2201",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2201",
        "ok": "2201",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2201",
        "ok": "2201",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2201",
        "ok": "2201",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-74e33": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_189313",
path: "Animal-api sync for customer BE_189313",
pathFormatted: "req_animal-api-sync-74e33",
stats: {
    "name": "Animal-api sync for customer BE_189313",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2075",
        "ok": "2075",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2075",
        "ok": "2075",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2075",
        "ok": "2075",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2075",
        "ok": "2075",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2075",
        "ok": "2075",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2075",
        "ok": "2075",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2075",
        "ok": "2075",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-aaf86": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_130803",
path: "Animal-api sync for customer NL_130803",
pathFormatted: "req_animal-api-sync-aaf86",
stats: {
    "name": "Animal-api sync for customer NL_130803",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2933",
        "ok": "2933",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2933",
        "ok": "2933",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2933",
        "ok": "2933",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2933",
        "ok": "2933",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2933",
        "ok": "2933",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2933",
        "ok": "2933",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2933",
        "ok": "2933",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-390c0": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_113006",
path: "Animal-api sync for customer NL_113006",
pathFormatted: "req_animal-api-sync-390c0",
stats: {
    "name": "Animal-api sync for customer NL_113006",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1792",
        "ok": "1792",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1792",
        "ok": "1792",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1792",
        "ok": "1792",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1792",
        "ok": "1792",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1792",
        "ok": "1792",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1792",
        "ok": "1792",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1792",
        "ok": "1792",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-363df": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_120238",
path: "Animal-api sync for customer NL_120238",
pathFormatted: "req_animal-api-sync-363df",
stats: {
    "name": "Animal-api sync for customer NL_120238",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1627",
        "ok": "1627",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1627",
        "ok": "1627",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1627",
        "ok": "1627",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1627",
        "ok": "1627",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1627",
        "ok": "1627",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1627",
        "ok": "1627",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1627",
        "ok": "1627",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e70df": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_129794",
path: "Animal-api sync for customer NL_129794",
pathFormatted: "req_animal-api-sync-e70df",
stats: {
    "name": "Animal-api sync for customer NL_129794",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "936",
        "ok": "936",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "936",
        "ok": "936",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "936",
        "ok": "936",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "936",
        "ok": "936",
        "ko": "-"
    },
    "percentiles2": {
        "total": "936",
        "ok": "936",
        "ko": "-"
    },
    "percentiles3": {
        "total": "936",
        "ok": "936",
        "ko": "-"
    },
    "percentiles4": {
        "total": "936",
        "ok": "936",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a9219": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_103449",
path: "Animal-api sync for customer NL_103449",
pathFormatted: "req_animal-api-sync-a9219",
stats: {
    "name": "Animal-api sync for customer NL_103449",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2596",
        "ok": "2596",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2596",
        "ok": "2596",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2596",
        "ok": "2596",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2596",
        "ok": "2596",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2596",
        "ok": "2596",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2596",
        "ok": "2596",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2596",
        "ok": "2596",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-30320": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_128704",
path: "Animal-api sync for customer NL_128704",
pathFormatted: "req_animal-api-sync-30320",
stats: {
    "name": "Animal-api sync for customer NL_128704",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1710",
        "ok": "1710",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1710",
        "ok": "1710",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1710",
        "ok": "1710",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1710",
        "ok": "1710",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1710",
        "ok": "1710",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1710",
        "ok": "1710",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1710",
        "ok": "1710",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c50f5": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111928",
path: "Animal-api sync for customer NL_111928",
pathFormatted: "req_animal-api-sync-c50f5",
stats: {
    "name": "Animal-api sync for customer NL_111928",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2442",
        "ok": "2442",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2442",
        "ok": "2442",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2442",
        "ok": "2442",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2442",
        "ok": "2442",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2442",
        "ok": "2442",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2442",
        "ok": "2442",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2442",
        "ok": "2442",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-294ca": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111076",
path: "Animal-api sync for customer NL_111076",
pathFormatted: "req_animal-api-sync-294ca",
stats: {
    "name": "Animal-api sync for customer NL_111076",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1834",
        "ok": "1834",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1834",
        "ok": "1834",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1834",
        "ok": "1834",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1834",
        "ok": "1834",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1834",
        "ok": "1834",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1834",
        "ok": "1834",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1834",
        "ok": "1834",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-080dc": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_131881",
path: "Animal-api sync for customer NL_131881",
pathFormatted: "req_animal-api-sync-080dc",
stats: {
    "name": "Animal-api sync for customer NL_131881",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1772",
        "ok": "1772",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1772",
        "ok": "1772",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1772",
        "ok": "1772",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1772",
        "ok": "1772",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1772",
        "ok": "1772",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1772",
        "ok": "1772",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1772",
        "ok": "1772",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-9f153": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_113848",
path: "Animal-api sync for customer NL_113848",
pathFormatted: "req_animal-api-sync-9f153",
stats: {
    "name": "Animal-api sync for customer NL_113848",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1974",
        "ok": "1974",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1974",
        "ok": "1974",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1974",
        "ok": "1974",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1974",
        "ok": "1974",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1974",
        "ok": "1974",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1974",
        "ok": "1974",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1974",
        "ok": "1974",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-084e7": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_108590",
path: "Animal-api sync for customer NL_108590",
pathFormatted: "req_animal-api-sync-084e7",
stats: {
    "name": "Animal-api sync for customer NL_108590",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2793",
        "ok": "2793",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2793",
        "ok": "2793",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2793",
        "ok": "2793",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2793",
        "ok": "2793",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2793",
        "ok": "2793",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2793",
        "ok": "2793",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2793",
        "ok": "2793",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-93bcd": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_107077",
path: "Animal-api sync for customer NL_107077",
pathFormatted: "req_animal-api-sync-93bcd",
stats: {
    "name": "Animal-api sync for customer NL_107077",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1280",
        "ok": "1280",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1280",
        "ok": "1280",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1280",
        "ok": "1280",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1280",
        "ok": "1280",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1280",
        "ok": "1280",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1280",
        "ok": "1280",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1280",
        "ok": "1280",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-43480": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_211973",
path: "Animal-api sync for customer BE_211973",
pathFormatted: "req_animal-api-sync-43480",
stats: {
    "name": "Animal-api sync for customer BE_211973",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4062",
        "ok": "4062",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4062",
        "ok": "4062",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4062",
        "ok": "4062",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4062",
        "ok": "4062",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4062",
        "ok": "4062",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4062",
        "ok": "4062",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4062",
        "ok": "4062",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a5109": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_124277",
path: "Animal-api sync for customer NL_124277",
pathFormatted: "req_animal-api-sync-a5109",
stats: {
    "name": "Animal-api sync for customer NL_124277",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2239",
        "ok": "2239",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2239",
        "ok": "2239",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2239",
        "ok": "2239",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2239",
        "ok": "2239",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2239",
        "ok": "2239",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2239",
        "ok": "2239",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2239",
        "ok": "2239",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-53304": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_108829",
path: "Animal-api sync for customer NL_108829",
pathFormatted: "req_animal-api-sync-53304",
stats: {
    "name": "Animal-api sync for customer NL_108829",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2939",
        "ok": "2939",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2939",
        "ok": "2939",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2939",
        "ok": "2939",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2939",
        "ok": "2939",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2939",
        "ok": "2939",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2939",
        "ok": "2939",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2939",
        "ok": "2939",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-84fc1": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_131210",
path: "Animal-api sync for customer NL_131210",
pathFormatted: "req_animal-api-sync-84fc1",
stats: {
    "name": "Animal-api sync for customer NL_131210",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1514",
        "ok": "1514",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1514",
        "ok": "1514",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1514",
        "ok": "1514",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1514",
        "ok": "1514",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1514",
        "ok": "1514",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1514",
        "ok": "1514",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1514",
        "ok": "1514",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-fc8bc": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_119277",
path: "Animal-api sync for customer NL_119277",
pathFormatted: "req_animal-api-sync-fc8bc",
stats: {
    "name": "Animal-api sync for customer NL_119277",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2285",
        "ok": "2285",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2285",
        "ok": "2285",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2285",
        "ok": "2285",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2285",
        "ok": "2285",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2285",
        "ok": "2285",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2285",
        "ok": "2285",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2285",
        "ok": "2285",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b0637": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_109813",
path: "Animal-api sync for customer NL_109813",
pathFormatted: "req_animal-api-sync-b0637",
stats: {
    "name": "Animal-api sync for customer NL_109813",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1886",
        "ok": "1886",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1886",
        "ok": "1886",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1886",
        "ok": "1886",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1886",
        "ok": "1886",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1886",
        "ok": "1886",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1886",
        "ok": "1886",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1886",
        "ok": "1886",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d7914": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_189320",
path: "Animal-api sync for customer NL_189320",
pathFormatted: "req_animal-api-sync-d7914",
stats: {
    "name": "Animal-api sync for customer NL_189320",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2563",
        "ok": "2563",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2563",
        "ok": "2563",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2563",
        "ok": "2563",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2563",
        "ok": "2563",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2563",
        "ok": "2563",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2563",
        "ok": "2563",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2563",
        "ok": "2563",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-acdb8": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_113754",
path: "Animal-api sync for customer NL_113754",
pathFormatted: "req_animal-api-sync-acdb8",
stats: {
    "name": "Animal-api sync for customer NL_113754",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2458",
        "ok": "2458",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2458",
        "ok": "2458",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2458",
        "ok": "2458",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2458",
        "ok": "2458",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2458",
        "ok": "2458",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2458",
        "ok": "2458",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2458",
        "ok": "2458",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-36730": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_135526",
path: "Animal-api sync for customer NL_135526",
pathFormatted: "req_animal-api-sync-36730",
stats: {
    "name": "Animal-api sync for customer NL_135526",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2193",
        "ok": "2193",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2193",
        "ok": "2193",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2193",
        "ok": "2193",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2193",
        "ok": "2193",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2193",
        "ok": "2193",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2193",
        "ok": "2193",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2193",
        "ok": "2193",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-1e909": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_154872",
path: "Animal-api sync for customer BE_154872",
pathFormatted: "req_animal-api-sync-1e909",
stats: {
    "name": "Animal-api sync for customer BE_154872",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2383",
        "ok": "2383",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2383",
        "ok": "2383",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2383",
        "ok": "2383",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2383",
        "ok": "2383",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2383",
        "ok": "2383",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2383",
        "ok": "2383",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2383",
        "ok": "2383",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-f54d2": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_125689",
path: "Animal-api sync for customer NL_125689",
pathFormatted: "req_animal-api-sync-f54d2",
stats: {
    "name": "Animal-api sync for customer NL_125689",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2927",
        "ok": "2927",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2927",
        "ok": "2927",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2927",
        "ok": "2927",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2927",
        "ok": "2927",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2927",
        "ok": "2927",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2927",
        "ok": "2927",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2927",
        "ok": "2927",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-baa35": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110539",
path: "Animal-api sync for customer NL_110539",
pathFormatted: "req_animal-api-sync-baa35",
stats: {
    "name": "Animal-api sync for customer NL_110539",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2440",
        "ok": "2440",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2440",
        "ok": "2440",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2440",
        "ok": "2440",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2440",
        "ok": "2440",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2440",
        "ok": "2440",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2440",
        "ok": "2440",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2440",
        "ok": "2440",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-14454": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104271",
path: "Animal-api sync for customer NL_104271",
pathFormatted: "req_animal-api-sync-14454",
stats: {
    "name": "Animal-api sync for customer NL_104271",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4188",
        "ok": "4188",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4188",
        "ok": "4188",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4188",
        "ok": "4188",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4188",
        "ok": "4188",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4188",
        "ok": "4188",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4188",
        "ok": "4188",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4188",
        "ok": "4188",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b6907": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_123943",
path: "Animal-api sync for customer NL_123943",
pathFormatted: "req_animal-api-sync-b6907",
stats: {
    "name": "Animal-api sync for customer NL_123943",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2149",
        "ok": "2149",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2149",
        "ok": "2149",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2149",
        "ok": "2149",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2149",
        "ok": "2149",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2149",
        "ok": "2149",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2149",
        "ok": "2149",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2149",
        "ok": "2149",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c4646": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110070",
path: "Animal-api sync for customer NL_110070",
pathFormatted: "req_animal-api-sync-c4646",
stats: {
    "name": "Animal-api sync for customer NL_110070",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2465",
        "ok": "2465",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2465",
        "ok": "2465",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2465",
        "ok": "2465",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2465",
        "ok": "2465",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2465",
        "ok": "2465",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2465",
        "ok": "2465",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2465",
        "ok": "2465",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-0eeba": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_114348",
path: "Animal-api sync for customer NL_114348",
pathFormatted: "req_animal-api-sync-0eeba",
stats: {
    "name": "Animal-api sync for customer NL_114348",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2534",
        "ok": "2534",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2534",
        "ok": "2534",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2534",
        "ok": "2534",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2534",
        "ok": "2534",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2534",
        "ok": "2534",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2534",
        "ok": "2534",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2534",
        "ok": "2534",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-5c35b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_114376",
path: "Animal-api sync for customer NL_114376",
pathFormatted: "req_animal-api-sync-5c35b",
stats: {
    "name": "Animal-api sync for customer NL_114376",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1517",
        "ok": "1517",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1517",
        "ok": "1517",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1517",
        "ok": "1517",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1517",
        "ok": "1517",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1517",
        "ok": "1517",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1517",
        "ok": "1517",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1517",
        "ok": "1517",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-451e8": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_121268",
path: "Animal-api sync for customer NL_121268",
pathFormatted: "req_animal-api-sync-451e8",
stats: {
    "name": "Animal-api sync for customer NL_121268",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1662",
        "ok": "1662",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1662",
        "ok": "1662",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1662",
        "ok": "1662",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1662",
        "ok": "1662",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1662",
        "ok": "1662",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1662",
        "ok": "1662",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1662",
        "ok": "1662",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e42e2": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_128581",
path: "Animal-api sync for customer NL_128581",
pathFormatted: "req_animal-api-sync-e42e2",
stats: {
    "name": "Animal-api sync for customer NL_128581",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1018",
        "ok": "1018",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1018",
        "ok": "1018",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1018",
        "ok": "1018",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1018",
        "ok": "1018",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1018",
        "ok": "1018",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1018",
        "ok": "1018",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1018",
        "ok": "1018",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b164c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_120434",
path: "Animal-api sync for customer NL_120434",
pathFormatted: "req_animal-api-sync-b164c",
stats: {
    "name": "Animal-api sync for customer NL_120434",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2939",
        "ok": "2939",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2939",
        "ok": "2939",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2939",
        "ok": "2939",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2939",
        "ok": "2939",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2939",
        "ok": "2939",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2939",
        "ok": "2939",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2939",
        "ok": "2939",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-7461a": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_120398",
path: "Animal-api sync for customer NL_120398",
pathFormatted: "req_animal-api-sync-7461a",
stats: {
    "name": "Animal-api sync for customer NL_120398",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2552",
        "ok": "2552",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2552",
        "ok": "2552",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2552",
        "ok": "2552",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2552",
        "ok": "2552",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2552",
        "ok": "2552",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2552",
        "ok": "2552",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2552",
        "ok": "2552",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-07a69": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_103947",
path: "Animal-api sync for customer NL_103947",
pathFormatted: "req_animal-api-sync-07a69",
stats: {
    "name": "Animal-api sync for customer NL_103947",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3906",
        "ok": "3906",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3906",
        "ok": "3906",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3906",
        "ok": "3906",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3906",
        "ok": "3906",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3906",
        "ok": "3906",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3906",
        "ok": "3906",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3906",
        "ok": "3906",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c6405": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104467",
path: "Animal-api sync for customer NL_104467",
pathFormatted: "req_animal-api-sync-c6405",
stats: {
    "name": "Animal-api sync for customer NL_104467",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2398",
        "ok": "2398",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2398",
        "ok": "2398",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2398",
        "ok": "2398",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2398",
        "ok": "2398",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2398",
        "ok": "2398",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2398",
        "ok": "2398",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2398",
        "ok": "2398",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-23f13": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_125273",
path: "Animal-api sync for customer NL_125273",
pathFormatted: "req_animal-api-sync-23f13",
stats: {
    "name": "Animal-api sync for customer NL_125273",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2523",
        "ok": "2523",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2523",
        "ok": "2523",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2523",
        "ok": "2523",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2523",
        "ok": "2523",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2523",
        "ok": "2523",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2523",
        "ok": "2523",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2523",
        "ok": "2523",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-96127": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_161584",
path: "Animal-api sync for customer BE_161584",
pathFormatted: "req_animal-api-sync-96127",
stats: {
    "name": "Animal-api sync for customer BE_161584",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2196",
        "ok": "2196",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2196",
        "ok": "2196",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2196",
        "ok": "2196",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2196",
        "ok": "2196",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2196",
        "ok": "2196",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2196",
        "ok": "2196",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2196",
        "ok": "2196",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-614ac": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104734",
path: "Animal-api sync for customer NL_104734",
pathFormatted: "req_animal-api-sync-614ac",
stats: {
    "name": "Animal-api sync for customer NL_104734",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2251",
        "ok": "2251",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2251",
        "ok": "2251",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2251",
        "ok": "2251",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2251",
        "ok": "2251",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2251",
        "ok": "2251",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2251",
        "ok": "2251",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2251",
        "ok": "2251",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-2d56c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110590",
path: "Animal-api sync for customer NL_110590",
pathFormatted: "req_animal-api-sync-2d56c",
stats: {
    "name": "Animal-api sync for customer NL_110590",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2143",
        "ok": "2143",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2143",
        "ok": "2143",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2143",
        "ok": "2143",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2143",
        "ok": "2143",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2143",
        "ok": "2143",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2143",
        "ok": "2143",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2143",
        "ok": "2143",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e31c7": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_210459",
path: "Animal-api sync for customer NL_210459",
pathFormatted: "req_animal-api-sync-e31c7",
stats: {
    "name": "Animal-api sync for customer NL_210459",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2229",
        "ok": "2229",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2229",
        "ok": "2229",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2229",
        "ok": "2229",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2229",
        "ok": "2229",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2229",
        "ok": "2229",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2229",
        "ok": "2229",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2229",
        "ok": "2229",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-7fca2": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_158798",
path: "Animal-api sync for customer NL_158798",
pathFormatted: "req_animal-api-sync-7fca2",
stats: {
    "name": "Animal-api sync for customer NL_158798",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2951",
        "ok": "2951",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2951",
        "ok": "2951",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2951",
        "ok": "2951",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2951",
        "ok": "2951",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2951",
        "ok": "2951",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2951",
        "ok": "2951",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2951",
        "ok": "2951",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b8f29": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_153558",
path: "Animal-api sync for customer BE_153558",
pathFormatted: "req_animal-api-sync-b8f29",
stats: {
    "name": "Animal-api sync for customer BE_153558",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1532",
        "ok": "1532",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1532",
        "ok": "1532",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1532",
        "ok": "1532",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1532",
        "ok": "1532",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1532",
        "ok": "1532",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1532",
        "ok": "1532",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1532",
        "ok": "1532",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-aa22e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_137034",
path: "Animal-api sync for customer NL_137034",
pathFormatted: "req_animal-api-sync-aa22e",
stats: {
    "name": "Animal-api sync for customer NL_137034",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1064",
        "ok": "1064",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1064",
        "ok": "1064",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1064",
        "ok": "1064",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1064",
        "ok": "1064",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1064",
        "ok": "1064",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1064",
        "ok": "1064",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1064",
        "ok": "1064",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b5283": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_112270",
path: "Animal-api sync for customer NL_112270",
pathFormatted: "req_animal-api-sync-b5283",
stats: {
    "name": "Animal-api sync for customer NL_112270",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3430",
        "ok": "3430",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3430",
        "ok": "3430",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3430",
        "ok": "3430",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3430",
        "ok": "3430",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3430",
        "ok": "3430",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3430",
        "ok": "3430",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3430",
        "ok": "3430",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-10e14": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_113903",
path: "Animal-api sync for customer NL_113903",
pathFormatted: "req_animal-api-sync-10e14",
stats: {
    "name": "Animal-api sync for customer NL_113903",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "7058",
        "ok": "7058",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "7058",
        "ok": "7058",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "7058",
        "ok": "7058",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "7058",
        "ok": "7058",
        "ko": "-"
    },
    "percentiles2": {
        "total": "7058",
        "ok": "7058",
        "ko": "-"
    },
    "percentiles3": {
        "total": "7058",
        "ok": "7058",
        "ko": "-"
    },
    "percentiles4": {
        "total": "7058",
        "ok": "7058",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-3dd2c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105439",
path: "Animal-api sync for customer NL_105439",
pathFormatted: "req_animal-api-sync-3dd2c",
stats: {
    "name": "Animal-api sync for customer NL_105439",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2260",
        "ok": "2260",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2260",
        "ok": "2260",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2260",
        "ok": "2260",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2260",
        "ok": "2260",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2260",
        "ok": "2260",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2260",
        "ok": "2260",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2260",
        "ok": "2260",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-8785d": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_207024",
path: "Animal-api sync for customer BE_207024",
pathFormatted: "req_animal-api-sync-8785d",
stats: {
    "name": "Animal-api sync for customer BE_207024",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2084",
        "ok": "2084",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2084",
        "ok": "2084",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2084",
        "ok": "2084",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2084",
        "ok": "2084",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2084",
        "ok": "2084",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2084",
        "ok": "2084",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2084",
        "ok": "2084",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-010fe": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_129945",
path: "Animal-api sync for customer NL_129945",
pathFormatted: "req_animal-api-sync-010fe",
stats: {
    "name": "Animal-api sync for customer NL_129945",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1915",
        "ok": "1915",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1915",
        "ok": "1915",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1915",
        "ok": "1915",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1915",
        "ok": "1915",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1915",
        "ok": "1915",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1915",
        "ok": "1915",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1915",
        "ok": "1915",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-58bfa": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_131425",
path: "Animal-api sync for customer NL_131425",
pathFormatted: "req_animal-api-sync-58bfa",
stats: {
    "name": "Animal-api sync for customer NL_131425",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2248",
        "ok": "2248",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2248",
        "ok": "2248",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2248",
        "ok": "2248",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2248",
        "ok": "2248",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2248",
        "ok": "2248",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2248",
        "ok": "2248",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2248",
        "ok": "2248",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-9a226": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_113819",
path: "Animal-api sync for customer NL_113819",
pathFormatted: "req_animal-api-sync-9a226",
stats: {
    "name": "Animal-api sync for customer NL_113819",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1584",
        "ok": "1584",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1584",
        "ok": "1584",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1584",
        "ok": "1584",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1584",
        "ok": "1584",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1584",
        "ok": "1584",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1584",
        "ok": "1584",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1584",
        "ok": "1584",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-44a53": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_121469",
path: "Animal-api sync for customer NL_121469",
pathFormatted: "req_animal-api-sync-44a53",
stats: {
    "name": "Animal-api sync for customer NL_121469",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2986",
        "ok": "2986",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2986",
        "ok": "2986",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2986",
        "ok": "2986",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2986",
        "ok": "2986",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2986",
        "ok": "2986",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2986",
        "ok": "2986",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2986",
        "ok": "2986",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c3fd5": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104563",
path: "Animal-api sync for customer NL_104563",
pathFormatted: "req_animal-api-sync-c3fd5",
stats: {
    "name": "Animal-api sync for customer NL_104563",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2702",
        "ok": "2702",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2702",
        "ok": "2702",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2702",
        "ok": "2702",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2702",
        "ok": "2702",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2702",
        "ok": "2702",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2702",
        "ok": "2702",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2702",
        "ok": "2702",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-2f86f": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_122640",
path: "Animal-api sync for customer NL_122640",
pathFormatted: "req_animal-api-sync-2f86f",
stats: {
    "name": "Animal-api sync for customer NL_122640",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "6595",
        "ok": "6595",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "6595",
        "ok": "6595",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "6595",
        "ok": "6595",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "6595",
        "ok": "6595",
        "ko": "-"
    },
    "percentiles2": {
        "total": "6595",
        "ok": "6595",
        "ko": "-"
    },
    "percentiles3": {
        "total": "6595",
        "ok": "6595",
        "ko": "-"
    },
    "percentiles4": {
        "total": "6595",
        "ok": "6595",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-447ce": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_136064",
path: "Animal-api sync for customer NL_136064",
pathFormatted: "req_animal-api-sync-447ce",
stats: {
    "name": "Animal-api sync for customer NL_136064",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1520",
        "ok": "1520",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1520",
        "ok": "1520",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1520",
        "ok": "1520",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1520",
        "ok": "1520",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1520",
        "ok": "1520",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1520",
        "ok": "1520",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1520",
        "ok": "1520",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-02723": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_106633",
path: "Animal-api sync for customer NL_106633",
pathFormatted: "req_animal-api-sync-02723",
stats: {
    "name": "Animal-api sync for customer NL_106633",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2567",
        "ok": "2567",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2567",
        "ok": "2567",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2567",
        "ok": "2567",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2567",
        "ok": "2567",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2567",
        "ok": "2567",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2567",
        "ok": "2567",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2567",
        "ok": "2567",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-19174": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117339",
path: "Animal-api sync for customer NL_117339",
pathFormatted: "req_animal-api-sync-19174",
stats: {
    "name": "Animal-api sync for customer NL_117339",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1691",
        "ok": "1691",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1691",
        "ok": "1691",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1691",
        "ok": "1691",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1691",
        "ok": "1691",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1691",
        "ok": "1691",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1691",
        "ok": "1691",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1691",
        "ok": "1691",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-9a47c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_115141",
path: "Animal-api sync for customer NL_115141",
pathFormatted: "req_animal-api-sync-9a47c",
stats: {
    "name": "Animal-api sync for customer NL_115141",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2473",
        "ok": "2473",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2473",
        "ok": "2473",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2473",
        "ok": "2473",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2473",
        "ok": "2473",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2473",
        "ok": "2473",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2473",
        "ok": "2473",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2473",
        "ok": "2473",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-0a9fa": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_125735",
path: "Animal-api sync for customer NL_125735",
pathFormatted: "req_animal-api-sync-0a9fa",
stats: {
    "name": "Animal-api sync for customer NL_125735",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2288",
        "ok": "2288",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2288",
        "ok": "2288",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2288",
        "ok": "2288",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2288",
        "ok": "2288",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2288",
        "ok": "2288",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2288",
        "ok": "2288",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2288",
        "ok": "2288",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-56dad": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_112805",
path: "Animal-api sync for customer NL_112805",
pathFormatted: "req_animal-api-sync-56dad",
stats: {
    "name": "Animal-api sync for customer NL_112805",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1982",
        "ok": "1982",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1982",
        "ok": "1982",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1982",
        "ok": "1982",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1982",
        "ok": "1982",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1982",
        "ok": "1982",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1982",
        "ok": "1982",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1982",
        "ok": "1982",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-4c3a5": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110894",
path: "Animal-api sync for customer NL_110894",
pathFormatted: "req_animal-api-sync-4c3a5",
stats: {
    "name": "Animal-api sync for customer NL_110894",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2018",
        "ok": "2018",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2018",
        "ok": "2018",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2018",
        "ok": "2018",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2018",
        "ok": "2018",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2018",
        "ok": "2018",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2018",
        "ok": "2018",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2018",
        "ok": "2018",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-86c49": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_145826",
path: "Animal-api sync for customer BE_145826",
pathFormatted: "req_animal-api-sync-86c49",
stats: {
    "name": "Animal-api sync for customer BE_145826",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2219",
        "ok": "2219",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2219",
        "ok": "2219",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2219",
        "ok": "2219",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2219",
        "ok": "2219",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2219",
        "ok": "2219",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2219",
        "ok": "2219",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2219",
        "ok": "2219",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-9e7ff": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110428",
path: "Animal-api sync for customer NL_110428",
pathFormatted: "req_animal-api-sync-9e7ff",
stats: {
    "name": "Animal-api sync for customer NL_110428",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1799",
        "ok": "1799",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1799",
        "ok": "1799",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1799",
        "ok": "1799",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1799",
        "ok": "1799",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1799",
        "ok": "1799",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1799",
        "ok": "1799",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1799",
        "ok": "1799",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-6c072": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_115279",
path: "Animal-api sync for customer NL_115279",
pathFormatted: "req_animal-api-sync-6c072",
stats: {
    "name": "Animal-api sync for customer NL_115279",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2025",
        "ok": "2025",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2025",
        "ok": "2025",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2025",
        "ok": "2025",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2025",
        "ok": "2025",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2025",
        "ok": "2025",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2025",
        "ok": "2025",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2025",
        "ok": "2025",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-2de80": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_129343",
path: "Animal-api sync for customer NL_129343",
pathFormatted: "req_animal-api-sync-2de80",
stats: {
    "name": "Animal-api sync for customer NL_129343",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1877",
        "ok": "1877",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1877",
        "ok": "1877",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1877",
        "ok": "1877",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1877",
        "ok": "1877",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1877",
        "ok": "1877",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1877",
        "ok": "1877",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1877",
        "ok": "1877",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-925cc": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_120686",
path: "Animal-api sync for customer NL_120686",
pathFormatted: "req_animal-api-sync-925cc",
stats: {
    "name": "Animal-api sync for customer NL_120686",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3168",
        "ok": "3168",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3168",
        "ok": "3168",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3168",
        "ok": "3168",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3168",
        "ok": "3168",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3168",
        "ok": "3168",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3168",
        "ok": "3168",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3168",
        "ok": "3168",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-f6a28": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_129516",
path: "Animal-api sync for customer NL_129516",
pathFormatted: "req_animal-api-sync-f6a28",
stats: {
    "name": "Animal-api sync for customer NL_129516",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "8027",
        "ok": "8027",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "8027",
        "ok": "8027",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "8027",
        "ok": "8027",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "8027",
        "ok": "8027",
        "ko": "-"
    },
    "percentiles2": {
        "total": "8027",
        "ok": "8027",
        "ko": "-"
    },
    "percentiles3": {
        "total": "8027",
        "ok": "8027",
        "ko": "-"
    },
    "percentiles4": {
        "total": "8027",
        "ok": "8027",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-10907": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_155615",
path: "Animal-api sync for customer NL_155615",
pathFormatted: "req_animal-api-sync-10907",
stats: {
    "name": "Animal-api sync for customer NL_155615",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2317",
        "ok": "2317",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2317",
        "ok": "2317",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2317",
        "ok": "2317",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2317",
        "ok": "2317",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2317",
        "ok": "2317",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2317",
        "ok": "2317",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2317",
        "ok": "2317",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-83fad": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_161486",
path: "Animal-api sync for customer NL_161486",
pathFormatted: "req_animal-api-sync-83fad",
stats: {
    "name": "Animal-api sync for customer NL_161486",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2834",
        "ok": "2834",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2834",
        "ok": "2834",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2834",
        "ok": "2834",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2834",
        "ok": "2834",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2834",
        "ok": "2834",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2834",
        "ok": "2834",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2834",
        "ok": "2834",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-6eeed": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_136560",
path: "Animal-api sync for customer NL_136560",
pathFormatted: "req_animal-api-sync-6eeed",
stats: {
    "name": "Animal-api sync for customer NL_136560",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2206",
        "ok": "2206",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2206",
        "ok": "2206",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2206",
        "ok": "2206",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2206",
        "ok": "2206",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2206",
        "ok": "2206",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2206",
        "ok": "2206",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2206",
        "ok": "2206",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-678da": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_116304",
path: "Animal-api sync for customer NL_116304",
pathFormatted: "req_animal-api-sync-678da",
stats: {
    "name": "Animal-api sync for customer NL_116304",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3208",
        "ok": "3208",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3208",
        "ok": "3208",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3208",
        "ok": "3208",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3208",
        "ok": "3208",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3208",
        "ok": "3208",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3208",
        "ok": "3208",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3208",
        "ok": "3208",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-23e9e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_136624",
path: "Animal-api sync for customer NL_136624",
pathFormatted: "req_animal-api-sync-23e9e",
stats: {
    "name": "Animal-api sync for customer NL_136624",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1518",
        "ok": "1518",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1518",
        "ok": "1518",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1518",
        "ok": "1518",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1518",
        "ok": "1518",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1518",
        "ok": "1518",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1518",
        "ok": "1518",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1518",
        "ok": "1518",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b45fa": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_160578",
path: "Animal-api sync for customer BE_160578",
pathFormatted: "req_animal-api-sync-b45fa",
stats: {
    "name": "Animal-api sync for customer BE_160578",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4258",
        "ok": "4258",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4258",
        "ok": "4258",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4258",
        "ok": "4258",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4258",
        "ok": "4258",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4258",
        "ok": "4258",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4258",
        "ok": "4258",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4258",
        "ok": "4258",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-ed8de": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117329",
path: "Animal-api sync for customer NL_117329",
pathFormatted: "req_animal-api-sync-ed8de",
stats: {
    "name": "Animal-api sync for customer NL_117329",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3002",
        "ok": "3002",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3002",
        "ok": "3002",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3002",
        "ok": "3002",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3002",
        "ok": "3002",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3002",
        "ok": "3002",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3002",
        "ok": "3002",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3002",
        "ok": "3002",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-7699b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_115653",
path: "Animal-api sync for customer NL_115653",
pathFormatted: "req_animal-api-sync-7699b",
stats: {
    "name": "Animal-api sync for customer NL_115653",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1335",
        "ok": "1335",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1335",
        "ok": "1335",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1335",
        "ok": "1335",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1335",
        "ok": "1335",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1335",
        "ok": "1335",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1335",
        "ok": "1335",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1335",
        "ok": "1335",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-f6750": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_145660",
path: "Animal-api sync for customer BE_145660",
pathFormatted: "req_animal-api-sync-f6750",
stats: {
    "name": "Animal-api sync for customer BE_145660",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2351",
        "ok": "2351",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2351",
        "ok": "2351",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2351",
        "ok": "2351",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2351",
        "ok": "2351",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2351",
        "ok": "2351",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2351",
        "ok": "2351",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2351",
        "ok": "2351",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-de4e1": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_196422",
path: "Animal-api sync for customer BE_196422",
pathFormatted: "req_animal-api-sync-de4e1",
stats: {
    "name": "Animal-api sync for customer BE_196422",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1559",
        "ok": "1559",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1559",
        "ok": "1559",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1559",
        "ok": "1559",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1559",
        "ok": "1559",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1559",
        "ok": "1559",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1559",
        "ok": "1559",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1559",
        "ok": "1559",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-94458": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111031",
path: "Animal-api sync for customer NL_111031",
pathFormatted: "req_animal-api-sync-94458",
stats: {
    "name": "Animal-api sync for customer NL_111031",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2025",
        "ok": "2025",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2025",
        "ok": "2025",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2025",
        "ok": "2025",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2025",
        "ok": "2025",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2025",
        "ok": "2025",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2025",
        "ok": "2025",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2025",
        "ok": "2025",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-1114b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105737",
path: "Animal-api sync for customer NL_105737",
pathFormatted: "req_animal-api-sync-1114b",
stats: {
    "name": "Animal-api sync for customer NL_105737",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1996",
        "ok": "1996",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1996",
        "ok": "1996",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1996",
        "ok": "1996",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1996",
        "ok": "1996",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1996",
        "ok": "1996",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1996",
        "ok": "1996",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1996",
        "ok": "1996",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-34261": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_112642",
path: "Animal-api sync for customer NL_112642",
pathFormatted: "req_animal-api-sync-34261",
stats: {
    "name": "Animal-api sync for customer NL_112642",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2798",
        "ok": "2798",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2798",
        "ok": "2798",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2798",
        "ok": "2798",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2798",
        "ok": "2798",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2798",
        "ok": "2798",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2798",
        "ok": "2798",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2798",
        "ok": "2798",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-88106": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_112559",
path: "Animal-api sync for customer NL_112559",
pathFormatted: "req_animal-api-sync-88106",
stats: {
    "name": "Animal-api sync for customer NL_112559",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1867",
        "ok": "1867",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1867",
        "ok": "1867",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1867",
        "ok": "1867",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1867",
        "ok": "1867",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1867",
        "ok": "1867",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1867",
        "ok": "1867",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1867",
        "ok": "1867",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-12cf4": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_103806",
path: "Animal-api sync for customer NL_103806",
pathFormatted: "req_animal-api-sync-12cf4",
stats: {
    "name": "Animal-api sync for customer NL_103806",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3404",
        "ok": "3404",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3404",
        "ok": "3404",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3404",
        "ok": "3404",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3404",
        "ok": "3404",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3404",
        "ok": "3404",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3404",
        "ok": "3404",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3404",
        "ok": "3404",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-0c8bf": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_135056",
path: "Animal-api sync for customer NL_135056",
pathFormatted: "req_animal-api-sync-0c8bf",
stats: {
    "name": "Animal-api sync for customer NL_135056",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2461",
        "ok": "2461",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2461",
        "ok": "2461",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2461",
        "ok": "2461",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2461",
        "ok": "2461",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2461",
        "ok": "2461",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2461",
        "ok": "2461",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2461",
        "ok": "2461",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-cd315": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_115640",
path: "Animal-api sync for customer NL_115640",
pathFormatted: "req_animal-api-sync-cd315",
stats: {
    "name": "Animal-api sync for customer NL_115640",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1609",
        "ok": "1609",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1609",
        "ok": "1609",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1609",
        "ok": "1609",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1609",
        "ok": "1609",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1609",
        "ok": "1609",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1609",
        "ok": "1609",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1609",
        "ok": "1609",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-708ff": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111792",
path: "Animal-api sync for customer NL_111792",
pathFormatted: "req_animal-api-sync-708ff",
stats: {
    "name": "Animal-api sync for customer NL_111792",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2142",
        "ok": "2142",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2142",
        "ok": "2142",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2142",
        "ok": "2142",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2142",
        "ok": "2142",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2142",
        "ok": "2142",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2142",
        "ok": "2142",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2142",
        "ok": "2142",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c74f5": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110479",
path: "Animal-api sync for customer NL_110479",
pathFormatted: "req_animal-api-sync-c74f5",
stats: {
    "name": "Animal-api sync for customer NL_110479",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3136",
        "ok": "3136",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3136",
        "ok": "3136",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3136",
        "ok": "3136",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3136",
        "ok": "3136",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3136",
        "ok": "3136",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3136",
        "ok": "3136",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3136",
        "ok": "3136",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b4519": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110383",
path: "Animal-api sync for customer NL_110383",
pathFormatted: "req_animal-api-sync-b4519",
stats: {
    "name": "Animal-api sync for customer NL_110383",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2012",
        "ok": "2012",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2012",
        "ok": "2012",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2012",
        "ok": "2012",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2012",
        "ok": "2012",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2012",
        "ok": "2012",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2012",
        "ok": "2012",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2012",
        "ok": "2012",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-ccec3": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_159045",
path: "Animal-api sync for customer BE_159045",
pathFormatted: "req_animal-api-sync-ccec3",
stats: {
    "name": "Animal-api sync for customer BE_159045",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2414",
        "ok": "2414",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2414",
        "ok": "2414",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2414",
        "ok": "2414",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2414",
        "ok": "2414",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2414",
        "ok": "2414",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2414",
        "ok": "2414",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2414",
        "ok": "2414",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-f73d7": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_112171",
path: "Animal-api sync for customer NL_112171",
pathFormatted: "req_animal-api-sync-f73d7",
stats: {
    "name": "Animal-api sync for customer NL_112171",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2580",
        "ok": "2580",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2580",
        "ok": "2580",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2580",
        "ok": "2580",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2580",
        "ok": "2580",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2580",
        "ok": "2580",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2580",
        "ok": "2580",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2580",
        "ok": "2580",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c61a4": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_158630",
path: "Animal-api sync for customer NL_158630",
pathFormatted: "req_animal-api-sync-c61a4",
stats: {
    "name": "Animal-api sync for customer NL_158630",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2531",
        "ok": "2531",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2531",
        "ok": "2531",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2531",
        "ok": "2531",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2531",
        "ok": "2531",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2531",
        "ok": "2531",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2531",
        "ok": "2531",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2531",
        "ok": "2531",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-3df21": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111890",
path: "Animal-api sync for customer NL_111890",
pathFormatted: "req_animal-api-sync-3df21",
stats: {
    "name": "Animal-api sync for customer NL_111890",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2040",
        "ok": "2040",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2040",
        "ok": "2040",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2040",
        "ok": "2040",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2040",
        "ok": "2040",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2040",
        "ok": "2040",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2040",
        "ok": "2040",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2040",
        "ok": "2040",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-11d0e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_119978",
path: "Animal-api sync for customer NL_119978",
pathFormatted: "req_animal-api-sync-11d0e",
stats: {
    "name": "Animal-api sync for customer NL_119978",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2505",
        "ok": "2505",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2505",
        "ok": "2505",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2505",
        "ok": "2505",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2505",
        "ok": "2505",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2505",
        "ok": "2505",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2505",
        "ok": "2505",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2505",
        "ok": "2505",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-cd0f8": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117391",
path: "Animal-api sync for customer NL_117391",
pathFormatted: "req_animal-api-sync-cd0f8",
stats: {
    "name": "Animal-api sync for customer NL_117391",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1847",
        "ok": "1847",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1847",
        "ok": "1847",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1847",
        "ok": "1847",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1847",
        "ok": "1847",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1847",
        "ok": "1847",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1847",
        "ok": "1847",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1847",
        "ok": "1847",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-997ea": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111277",
path: "Animal-api sync for customer NL_111277",
pathFormatted: "req_animal-api-sync-997ea",
stats: {
    "name": "Animal-api sync for customer NL_111277",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2972",
        "ok": "2972",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2972",
        "ok": "2972",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2972",
        "ok": "2972",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2972",
        "ok": "2972",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2972",
        "ok": "2972",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2972",
        "ok": "2972",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2972",
        "ok": "2972",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-998da": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_120046",
path: "Animal-api sync for customer NL_120046",
pathFormatted: "req_animal-api-sync-998da",
stats: {
    "name": "Animal-api sync for customer NL_120046",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1281",
        "ok": "1281",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1281",
        "ok": "1281",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1281",
        "ok": "1281",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1281",
        "ok": "1281",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1281",
        "ok": "1281",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1281",
        "ok": "1281",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1281",
        "ok": "1281",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-08b09": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_122753",
path: "Animal-api sync for customer NL_122753",
pathFormatted: "req_animal-api-sync-08b09",
stats: {
    "name": "Animal-api sync for customer NL_122753",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2792",
        "ok": "2792",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2792",
        "ok": "2792",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2792",
        "ok": "2792",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2792",
        "ok": "2792",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2792",
        "ok": "2792",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2792",
        "ok": "2792",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2792",
        "ok": "2792",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a5635": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_193658",
path: "Animal-api sync for customer BE_193658",
pathFormatted: "req_animal-api-sync-a5635",
stats: {
    "name": "Animal-api sync for customer BE_193658",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2634",
        "ok": "2634",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2634",
        "ok": "2634",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2634",
        "ok": "2634",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2634",
        "ok": "2634",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2634",
        "ok": "2634",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2634",
        "ok": "2634",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2634",
        "ok": "2634",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-f89f8": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_121091",
path: "Animal-api sync for customer NL_121091",
pathFormatted: "req_animal-api-sync-f89f8",
stats: {
    "name": "Animal-api sync for customer NL_121091",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1529",
        "ok": "1529",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1529",
        "ok": "1529",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1529",
        "ok": "1529",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1529",
        "ok": "1529",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1529",
        "ok": "1529",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1529",
        "ok": "1529",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1529",
        "ok": "1529",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-6c82d": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_143962",
path: "Animal-api sync for customer NL_143962",
pathFormatted: "req_animal-api-sync-6c82d",
stats: {
    "name": "Animal-api sync for customer NL_143962",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2025",
        "ok": "2025",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2025",
        "ok": "2025",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2025",
        "ok": "2025",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2025",
        "ok": "2025",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2025",
        "ok": "2025",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2025",
        "ok": "2025",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2025",
        "ok": "2025",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-afe85": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104819",
path: "Animal-api sync for customer NL_104819",
pathFormatted: "req_animal-api-sync-afe85",
stats: {
    "name": "Animal-api sync for customer NL_104819",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2783",
        "ok": "2783",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2783",
        "ok": "2783",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2783",
        "ok": "2783",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2783",
        "ok": "2783",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2783",
        "ok": "2783",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2783",
        "ok": "2783",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2783",
        "ok": "2783",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d9f9d": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_156439",
path: "Animal-api sync for customer BE_156439",
pathFormatted: "req_animal-api-sync-d9f9d",
stats: {
    "name": "Animal-api sync for customer BE_156439",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2626",
        "ok": "2626",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2626",
        "ok": "2626",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2626",
        "ok": "2626",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2626",
        "ok": "2626",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2626",
        "ok": "2626",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2626",
        "ok": "2626",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2626",
        "ok": "2626",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-038e6": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117080",
path: "Animal-api sync for customer NL_117080",
pathFormatted: "req_animal-api-sync-038e6",
stats: {
    "name": "Animal-api sync for customer NL_117080",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2800",
        "ok": "2800",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2800",
        "ok": "2800",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2800",
        "ok": "2800",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2800",
        "ok": "2800",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2800",
        "ok": "2800",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2800",
        "ok": "2800",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2800",
        "ok": "2800",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-9a2c1": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_142403",
path: "Animal-api sync for customer NL_142403",
pathFormatted: "req_animal-api-sync-9a2c1",
stats: {
    "name": "Animal-api sync for customer NL_142403",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2312",
        "ok": "2312",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2312",
        "ok": "2312",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2312",
        "ok": "2312",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2312",
        "ok": "2312",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2312",
        "ok": "2312",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2312",
        "ok": "2312",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2312",
        "ok": "2312",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e1749": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_121066",
path: "Animal-api sync for customer NL_121066",
pathFormatted: "req_animal-api-sync-e1749",
stats: {
    "name": "Animal-api sync for customer NL_121066",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2284",
        "ok": "2284",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2284",
        "ok": "2284",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2284",
        "ok": "2284",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2284",
        "ok": "2284",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2284",
        "ok": "2284",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2284",
        "ok": "2284",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2284",
        "ok": "2284",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c09d9": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_126693",
path: "Animal-api sync for customer NL_126693",
pathFormatted: "req_animal-api-sync-c09d9",
stats: {
    "name": "Animal-api sync for customer NL_126693",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2003",
        "ok": "2003",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2003",
        "ok": "2003",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2003",
        "ok": "2003",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2003",
        "ok": "2003",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2003",
        "ok": "2003",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2003",
        "ok": "2003",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2003",
        "ok": "2003",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-cdc3c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_125737",
path: "Animal-api sync for customer NL_125737",
pathFormatted: "req_animal-api-sync-cdc3c",
stats: {
    "name": "Animal-api sync for customer NL_125737",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2427",
        "ok": "2427",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2427",
        "ok": "2427",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2427",
        "ok": "2427",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2427",
        "ok": "2427",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2427",
        "ok": "2427",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2427",
        "ok": "2427",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2427",
        "ok": "2427",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-339e7": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_115945",
path: "Animal-api sync for customer NL_115945",
pathFormatted: "req_animal-api-sync-339e7",
stats: {
    "name": "Animal-api sync for customer NL_115945",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1439",
        "ok": "1439",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1439",
        "ok": "1439",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1439",
        "ok": "1439",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1439",
        "ok": "1439",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1439",
        "ok": "1439",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1439",
        "ok": "1439",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1439",
        "ok": "1439",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c1773": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105858",
path: "Animal-api sync for customer NL_105858",
pathFormatted: "req_animal-api-sync-c1773",
stats: {
    "name": "Animal-api sync for customer NL_105858",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2471",
        "ok": "2471",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2471",
        "ok": "2471",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2471",
        "ok": "2471",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2471",
        "ok": "2471",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2471",
        "ok": "2471",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2471",
        "ok": "2471",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2471",
        "ok": "2471",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c41a4": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_118516",
path: "Animal-api sync for customer NL_118516",
pathFormatted: "req_animal-api-sync-c41a4",
stats: {
    "name": "Animal-api sync for customer NL_118516",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1896",
        "ok": "1896",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1896",
        "ok": "1896",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1896",
        "ok": "1896",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1896",
        "ok": "1896",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1896",
        "ok": "1896",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1896",
        "ok": "1896",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1896",
        "ok": "1896",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e8c3a": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_154725",
path: "Animal-api sync for customer BE_154725",
pathFormatted: "req_animal-api-sync-e8c3a",
stats: {
    "name": "Animal-api sync for customer BE_154725",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2128",
        "ok": "2128",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2128",
        "ok": "2128",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2128",
        "ok": "2128",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2128",
        "ok": "2128",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2128",
        "ok": "2128",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2128",
        "ok": "2128",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2128",
        "ok": "2128",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-fc13a": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_154710",
path: "Animal-api sync for customer BE_154710",
pathFormatted: "req_animal-api-sync-fc13a",
stats: {
    "name": "Animal-api sync for customer BE_154710",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2116",
        "ok": "2116",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2116",
        "ok": "2116",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2116",
        "ok": "2116",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2116",
        "ok": "2116",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2116",
        "ok": "2116",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2116",
        "ok": "2116",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2116",
        "ok": "2116",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d0d7d": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_147835",
path: "Animal-api sync for customer BE_147835",
pathFormatted: "req_animal-api-sync-d0d7d",
stats: {
    "name": "Animal-api sync for customer BE_147835",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2682",
        "ok": "2682",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2682",
        "ok": "2682",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2682",
        "ok": "2682",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2682",
        "ok": "2682",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2682",
        "ok": "2682",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2682",
        "ok": "2682",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2682",
        "ok": "2682",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-f852c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_129269",
path: "Animal-api sync for customer NL_129269",
pathFormatted: "req_animal-api-sync-f852c",
stats: {
    "name": "Animal-api sync for customer NL_129269",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2501",
        "ok": "2501",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2501",
        "ok": "2501",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2501",
        "ok": "2501",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2501",
        "ok": "2501",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2501",
        "ok": "2501",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2501",
        "ok": "2501",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2501",
        "ok": "2501",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
