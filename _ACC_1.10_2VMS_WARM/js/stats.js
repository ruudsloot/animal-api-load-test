var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "495",
        "ok": "495",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1004",
        "ok": "1004",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "13743",
        "ok": "13743",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3384",
        "ok": "3384",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "1448",
        "ok": "1448",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3042",
        "ok": "3042",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3885",
        "ok": "3885",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5949",
        "ok": "5949",
        "ko": "-"
    },
    "percentiles4": {
        "total": "8701",
        "ok": "8701",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 495,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.228",
        "ok": "1.228",
        "ko": "-"
    }
},
contents: {
"req_animal-api-sync-9e54f": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_112168",
path: "Animal-api sync for customer NL_112168",
pathFormatted: "req_animal-api-sync-9e54f",
stats: {
    "name": "Animal-api sync for customer NL_112168",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3014",
        "ok": "3014",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3014",
        "ok": "3014",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3014",
        "ok": "3014",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3014",
        "ok": "3014",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3014",
        "ok": "3014",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3014",
        "ok": "3014",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3014",
        "ok": "3014",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-f5a2a": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_125092",
path: "Animal-api sync for customer NL_125092",
pathFormatted: "req_animal-api-sync-f5a2a",
stats: {
    "name": "Animal-api sync for customer NL_125092",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2879",
        "ok": "2879",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2879",
        "ok": "2879",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2879",
        "ok": "2879",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2879",
        "ok": "2879",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2879",
        "ok": "2879",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2879",
        "ok": "2879",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2879",
        "ok": "2879",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-0f1c3": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_128277",
path: "Animal-api sync for customer NL_128277",
pathFormatted: "req_animal-api-sync-0f1c3",
stats: {
    "name": "Animal-api sync for customer NL_128277",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3437",
        "ok": "3437",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3437",
        "ok": "3437",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3437",
        "ok": "3437",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3437",
        "ok": "3437",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3437",
        "ok": "3437",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3437",
        "ok": "3437",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3437",
        "ok": "3437",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-1d77d": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_109531",
path: "Animal-api sync for customer NL_109531",
pathFormatted: "req_animal-api-sync-1d77d",
stats: {
    "name": "Animal-api sync for customer NL_109531",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2485",
        "ok": "2485",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2485",
        "ok": "2485",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2485",
        "ok": "2485",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2485",
        "ok": "2485",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2485",
        "ok": "2485",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2485",
        "ok": "2485",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2485",
        "ok": "2485",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e9ad2": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_121119",
path: "Animal-api sync for customer NL_121119",
pathFormatted: "req_animal-api-sync-e9ad2",
stats: {
    "name": "Animal-api sync for customer NL_121119",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3041",
        "ok": "3041",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3041",
        "ok": "3041",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3041",
        "ok": "3041",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3041",
        "ok": "3041",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3041",
        "ok": "3041",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3041",
        "ok": "3041",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3041",
        "ok": "3041",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-86f14": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_121285",
path: "Animal-api sync for customer NL_121285",
pathFormatted: "req_animal-api-sync-86f14",
stats: {
    "name": "Animal-api sync for customer NL_121285",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2193",
        "ok": "2193",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2193",
        "ok": "2193",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2193",
        "ok": "2193",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2193",
        "ok": "2193",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2193",
        "ok": "2193",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2193",
        "ok": "2193",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2193",
        "ok": "2193",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d5299": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104311",
path: "Animal-api sync for customer NL_104311",
pathFormatted: "req_animal-api-sync-d5299",
stats: {
    "name": "Animal-api sync for customer NL_104311",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2305",
        "ok": "2305",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2305",
        "ok": "2305",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2305",
        "ok": "2305",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2305",
        "ok": "2305",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2305",
        "ok": "2305",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2305",
        "ok": "2305",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2305",
        "ok": "2305",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-51e6e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117693",
path: "Animal-api sync for customer NL_117693",
pathFormatted: "req_animal-api-sync-51e6e",
stats: {
    "name": "Animal-api sync for customer NL_117693",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3092",
        "ok": "3092",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3092",
        "ok": "3092",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3092",
        "ok": "3092",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3092",
        "ok": "3092",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3092",
        "ok": "3092",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3092",
        "ok": "3092",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3092",
        "ok": "3092",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-44547": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117813",
path: "Animal-api sync for customer NL_117813",
pathFormatted: "req_animal-api-sync-44547",
stats: {
    "name": "Animal-api sync for customer NL_117813",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2859",
        "ok": "2859",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2859",
        "ok": "2859",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2859",
        "ok": "2859",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2859",
        "ok": "2859",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2859",
        "ok": "2859",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2859",
        "ok": "2859",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2859",
        "ok": "2859",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-76899": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_121301",
path: "Animal-api sync for customer NL_121301",
pathFormatted: "req_animal-api-sync-76899",
stats: {
    "name": "Animal-api sync for customer NL_121301",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2410",
        "ok": "2410",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2410",
        "ok": "2410",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2410",
        "ok": "2410",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2410",
        "ok": "2410",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2410",
        "ok": "2410",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2410",
        "ok": "2410",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2410",
        "ok": "2410",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-857a0": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_127307",
path: "Animal-api sync for customer NL_127307",
pathFormatted: "req_animal-api-sync-857a0",
stats: {
    "name": "Animal-api sync for customer NL_127307",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1344",
        "ok": "1344",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1344",
        "ok": "1344",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1344",
        "ok": "1344",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1344",
        "ok": "1344",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1344",
        "ok": "1344",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1344",
        "ok": "1344",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1344",
        "ok": "1344",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-8b245": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_120606",
path: "Animal-api sync for customer NL_120606",
pathFormatted: "req_animal-api-sync-8b245",
stats: {
    "name": "Animal-api sync for customer NL_120606",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2782",
        "ok": "2782",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2782",
        "ok": "2782",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2782",
        "ok": "2782",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2782",
        "ok": "2782",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2782",
        "ok": "2782",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2782",
        "ok": "2782",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2782",
        "ok": "2782",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-1ba07": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105320",
path: "Animal-api sync for customer NL_105320",
pathFormatted: "req_animal-api-sync-1ba07",
stats: {
    "name": "Animal-api sync for customer NL_105320",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2786",
        "ok": "2786",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2786",
        "ok": "2786",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2786",
        "ok": "2786",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2786",
        "ok": "2786",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2786",
        "ok": "2786",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2786",
        "ok": "2786",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2786",
        "ok": "2786",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-07d8c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_115607",
path: "Animal-api sync for customer NL_115607",
pathFormatted: "req_animal-api-sync-07d8c",
stats: {
    "name": "Animal-api sync for customer NL_115607",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3981",
        "ok": "3981",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3981",
        "ok": "3981",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3981",
        "ok": "3981",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3981",
        "ok": "3981",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3981",
        "ok": "3981",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3981",
        "ok": "3981",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3981",
        "ok": "3981",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d643b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_129420",
path: "Animal-api sync for customer NL_129420",
pathFormatted: "req_animal-api-sync-d643b",
stats: {
    "name": "Animal-api sync for customer NL_129420",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1196",
        "ok": "1196",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1196",
        "ok": "1196",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1196",
        "ok": "1196",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1196",
        "ok": "1196",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1196",
        "ok": "1196",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1196",
        "ok": "1196",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1196",
        "ok": "1196",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-5b7d7": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_113415",
path: "Animal-api sync for customer NL_113415",
pathFormatted: "req_animal-api-sync-5b7d7",
stats: {
    "name": "Animal-api sync for customer NL_113415",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4266",
        "ok": "4266",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4266",
        "ok": "4266",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4266",
        "ok": "4266",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4266",
        "ok": "4266",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4266",
        "ok": "4266",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4266",
        "ok": "4266",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4266",
        "ok": "4266",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-05129": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117717",
path: "Animal-api sync for customer NL_117717",
pathFormatted: "req_animal-api-sync-05129",
stats: {
    "name": "Animal-api sync for customer NL_117717",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2303",
        "ok": "2303",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2303",
        "ok": "2303",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2303",
        "ok": "2303",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2303",
        "ok": "2303",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2303",
        "ok": "2303",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2303",
        "ok": "2303",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2303",
        "ok": "2303",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a175e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_207835",
path: "Animal-api sync for customer NL_207835",
pathFormatted: "req_animal-api-sync-a175e",
stats: {
    "name": "Animal-api sync for customer NL_207835",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1463",
        "ok": "1463",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1463",
        "ok": "1463",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1463",
        "ok": "1463",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1463",
        "ok": "1463",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1463",
        "ok": "1463",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1463",
        "ok": "1463",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1463",
        "ok": "1463",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-3bf26": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_108699",
path: "Animal-api sync for customer NL_108699",
pathFormatted: "req_animal-api-sync-3bf26",
stats: {
    "name": "Animal-api sync for customer NL_108699",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1198",
        "ok": "1198",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1198",
        "ok": "1198",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1198",
        "ok": "1198",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1198",
        "ok": "1198",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1198",
        "ok": "1198",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1198",
        "ok": "1198",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1198",
        "ok": "1198",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-2b97a": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_161984",
path: "Animal-api sync for customer NL_161984",
pathFormatted: "req_animal-api-sync-2b97a",
stats: {
    "name": "Animal-api sync for customer NL_161984",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2711",
        "ok": "2711",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2711",
        "ok": "2711",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2711",
        "ok": "2711",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2711",
        "ok": "2711",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2711",
        "ok": "2711",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2711",
        "ok": "2711",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2711",
        "ok": "2711",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-5243f": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111939",
path: "Animal-api sync for customer NL_111939",
pathFormatted: "req_animal-api-sync-5243f",
stats: {
    "name": "Animal-api sync for customer NL_111939",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3944",
        "ok": "3944",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3944",
        "ok": "3944",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3944",
        "ok": "3944",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3944",
        "ok": "3944",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3944",
        "ok": "3944",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3944",
        "ok": "3944",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3944",
        "ok": "3944",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b6219": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_144583",
path: "Animal-api sync for customer NL_144583",
pathFormatted: "req_animal-api-sync-b6219",
stats: {
    "name": "Animal-api sync for customer NL_144583",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2420",
        "ok": "2420",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2420",
        "ok": "2420",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2420",
        "ok": "2420",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2420",
        "ok": "2420",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2420",
        "ok": "2420",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2420",
        "ok": "2420",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2420",
        "ok": "2420",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-43dcc": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_112405",
path: "Animal-api sync for customer NL_112405",
pathFormatted: "req_animal-api-sync-43dcc",
stats: {
    "name": "Animal-api sync for customer NL_112405",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3314",
        "ok": "3314",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3314",
        "ok": "3314",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3314",
        "ok": "3314",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3314",
        "ok": "3314",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3314",
        "ok": "3314",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3314",
        "ok": "3314",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3314",
        "ok": "3314",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d545c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_131064",
path: "Animal-api sync for customer NL_131064",
pathFormatted: "req_animal-api-sync-d545c",
stats: {
    "name": "Animal-api sync for customer NL_131064",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3841",
        "ok": "3841",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3841",
        "ok": "3841",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3841",
        "ok": "3841",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3841",
        "ok": "3841",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3841",
        "ok": "3841",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3841",
        "ok": "3841",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3841",
        "ok": "3841",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-93205": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_120176",
path: "Animal-api sync for customer NL_120176",
pathFormatted: "req_animal-api-sync-93205",
stats: {
    "name": "Animal-api sync for customer NL_120176",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3774",
        "ok": "3774",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3774",
        "ok": "3774",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3774",
        "ok": "3774",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3774",
        "ok": "3774",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3774",
        "ok": "3774",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3774",
        "ok": "3774",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3774",
        "ok": "3774",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-bf978": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_118830",
path: "Animal-api sync for customer NL_118830",
pathFormatted: "req_animal-api-sync-bf978",
stats: {
    "name": "Animal-api sync for customer NL_118830",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3285",
        "ok": "3285",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3285",
        "ok": "3285",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3285",
        "ok": "3285",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3285",
        "ok": "3285",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3285",
        "ok": "3285",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3285",
        "ok": "3285",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3285",
        "ok": "3285",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-28852": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_116904",
path: "Animal-api sync for customer NL_116904",
pathFormatted: "req_animal-api-sync-28852",
stats: {
    "name": "Animal-api sync for customer NL_116904",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3950",
        "ok": "3950",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3950",
        "ok": "3950",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3950",
        "ok": "3950",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3950",
        "ok": "3950",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3950",
        "ok": "3950",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3950",
        "ok": "3950",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3950",
        "ok": "3950",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-3566a": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_107884",
path: "Animal-api sync for customer NL_107884",
pathFormatted: "req_animal-api-sync-3566a",
stats: {
    "name": "Animal-api sync for customer NL_107884",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3535",
        "ok": "3535",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3535",
        "ok": "3535",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3535",
        "ok": "3535",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3535",
        "ok": "3535",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3535",
        "ok": "3535",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3535",
        "ok": "3535",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3535",
        "ok": "3535",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c24b5": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_122429",
path: "Animal-api sync for customer NL_122429",
pathFormatted: "req_animal-api-sync-c24b5",
stats: {
    "name": "Animal-api sync for customer NL_122429",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3039",
        "ok": "3039",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3039",
        "ok": "3039",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3039",
        "ok": "3039",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3039",
        "ok": "3039",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3039",
        "ok": "3039",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3039",
        "ok": "3039",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3039",
        "ok": "3039",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-31f54": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_122305",
path: "Animal-api sync for customer NL_122305",
pathFormatted: "req_animal-api-sync-31f54",
stats: {
    "name": "Animal-api sync for customer NL_122305",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3115",
        "ok": "3115",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3115",
        "ok": "3115",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3115",
        "ok": "3115",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3115",
        "ok": "3115",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3115",
        "ok": "3115",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3115",
        "ok": "3115",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3115",
        "ok": "3115",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-8ebc3": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_212864",
path: "Animal-api sync for customer BE_212864",
pathFormatted: "req_animal-api-sync-8ebc3",
stats: {
    "name": "Animal-api sync for customer BE_212864",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3328",
        "ok": "3328",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3328",
        "ok": "3328",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3328",
        "ok": "3328",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3328",
        "ok": "3328",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3328",
        "ok": "3328",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3328",
        "ok": "3328",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3328",
        "ok": "3328",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a5160": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_156494",
path: "Animal-api sync for customer NL_156494",
pathFormatted: "req_animal-api-sync-a5160",
stats: {
    "name": "Animal-api sync for customer NL_156494",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5910",
        "ok": "5910",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5910",
        "ok": "5910",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5910",
        "ok": "5910",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5910",
        "ok": "5910",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5910",
        "ok": "5910",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5910",
        "ok": "5910",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5910",
        "ok": "5910",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-34ba1": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_165365",
path: "Animal-api sync for customer BE_165365",
pathFormatted: "req_animal-api-sync-34ba1",
stats: {
    "name": "Animal-api sync for customer BE_165365",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4216",
        "ok": "4216",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4216",
        "ok": "4216",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4216",
        "ok": "4216",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4216",
        "ok": "4216",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4216",
        "ok": "4216",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4216",
        "ok": "4216",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4216",
        "ok": "4216",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-9ec36": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_124747",
path: "Animal-api sync for customer NL_124747",
pathFormatted: "req_animal-api-sync-9ec36",
stats: {
    "name": "Animal-api sync for customer NL_124747",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3982",
        "ok": "3982",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3982",
        "ok": "3982",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3982",
        "ok": "3982",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3982",
        "ok": "3982",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3982",
        "ok": "3982",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3982",
        "ok": "3982",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3982",
        "ok": "3982",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b9862": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_161770",
path: "Animal-api sync for customer NL_161770",
pathFormatted: "req_animal-api-sync-b9862",
stats: {
    "name": "Animal-api sync for customer NL_161770",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3930",
        "ok": "3930",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3930",
        "ok": "3930",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3930",
        "ok": "3930",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3930",
        "ok": "3930",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3930",
        "ok": "3930",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3930",
        "ok": "3930",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3930",
        "ok": "3930",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b2272": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105223",
path: "Animal-api sync for customer NL_105223",
pathFormatted: "req_animal-api-sync-b2272",
stats: {
    "name": "Animal-api sync for customer NL_105223",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2950",
        "ok": "2950",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2950",
        "ok": "2950",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2950",
        "ok": "2950",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2950",
        "ok": "2950",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2950",
        "ok": "2950",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2950",
        "ok": "2950",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2950",
        "ok": "2950",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-49a96": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_128642",
path: "Animal-api sync for customer NL_128642",
pathFormatted: "req_animal-api-sync-49a96",
stats: {
    "name": "Animal-api sync for customer NL_128642",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2405",
        "ok": "2405",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2405",
        "ok": "2405",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2405",
        "ok": "2405",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2405",
        "ok": "2405",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2405",
        "ok": "2405",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2405",
        "ok": "2405",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2405",
        "ok": "2405",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-5c4eb": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_123093",
path: "Animal-api sync for customer NL_123093",
pathFormatted: "req_animal-api-sync-5c4eb",
stats: {
    "name": "Animal-api sync for customer NL_123093",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2488",
        "ok": "2488",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2488",
        "ok": "2488",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2488",
        "ok": "2488",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2488",
        "ok": "2488",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2488",
        "ok": "2488",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2488",
        "ok": "2488",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2488",
        "ok": "2488",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-90ee6": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105389",
path: "Animal-api sync for customer NL_105389",
pathFormatted: "req_animal-api-sync-90ee6",
stats: {
    "name": "Animal-api sync for customer NL_105389",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4304",
        "ok": "4304",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4304",
        "ok": "4304",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4304",
        "ok": "4304",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4304",
        "ok": "4304",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4304",
        "ok": "4304",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4304",
        "ok": "4304",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4304",
        "ok": "4304",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-0908d": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105707",
path: "Animal-api sync for customer NL_105707",
pathFormatted: "req_animal-api-sync-0908d",
stats: {
    "name": "Animal-api sync for customer NL_105707",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3136",
        "ok": "3136",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3136",
        "ok": "3136",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3136",
        "ok": "3136",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3136",
        "ok": "3136",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3136",
        "ok": "3136",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3136",
        "ok": "3136",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3136",
        "ok": "3136",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-0a82c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_124910",
path: "Animal-api sync for customer NL_124910",
pathFormatted: "req_animal-api-sync-0a82c",
stats: {
    "name": "Animal-api sync for customer NL_124910",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1222",
        "ok": "1222",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1222",
        "ok": "1222",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1222",
        "ok": "1222",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1222",
        "ok": "1222",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1222",
        "ok": "1222",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1222",
        "ok": "1222",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1222",
        "ok": "1222",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d54bb": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_108024",
path: "Animal-api sync for customer NL_108024",
pathFormatted: "req_animal-api-sync-d54bb",
stats: {
    "name": "Animal-api sync for customer NL_108024",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4665",
        "ok": "4665",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4665",
        "ok": "4665",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4665",
        "ok": "4665",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4665",
        "ok": "4665",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4665",
        "ok": "4665",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4665",
        "ok": "4665",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4665",
        "ok": "4665",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c0ea2": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_126187",
path: "Animal-api sync for customer NL_126187",
pathFormatted: "req_animal-api-sync-c0ea2",
stats: {
    "name": "Animal-api sync for customer NL_126187",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1949",
        "ok": "1949",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1949",
        "ok": "1949",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1949",
        "ok": "1949",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1949",
        "ok": "1949",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1949",
        "ok": "1949",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1949",
        "ok": "1949",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1949",
        "ok": "1949",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-7a1f1": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_151229",
path: "Animal-api sync for customer BE_151229",
pathFormatted: "req_animal-api-sync-7a1f1",
stats: {
    "name": "Animal-api sync for customer BE_151229",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1184",
        "ok": "1184",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1184",
        "ok": "1184",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1184",
        "ok": "1184",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1184",
        "ok": "1184",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1184",
        "ok": "1184",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1184",
        "ok": "1184",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1184",
        "ok": "1184",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-72904": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_103783",
path: "Animal-api sync for customer NL_103783",
pathFormatted: "req_animal-api-sync-72904",
stats: {
    "name": "Animal-api sync for customer NL_103783",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3722",
        "ok": "3722",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3722",
        "ok": "3722",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3722",
        "ok": "3722",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3722",
        "ok": "3722",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3722",
        "ok": "3722",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3722",
        "ok": "3722",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3722",
        "ok": "3722",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-54314": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_103881",
path: "Animal-api sync for customer NL_103881",
pathFormatted: "req_animal-api-sync-54314",
stats: {
    "name": "Animal-api sync for customer NL_103881",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3619",
        "ok": "3619",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3619",
        "ok": "3619",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3619",
        "ok": "3619",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3619",
        "ok": "3619",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3619",
        "ok": "3619",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3619",
        "ok": "3619",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3619",
        "ok": "3619",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-fb3ec": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_115893",
path: "Animal-api sync for customer NL_115893",
pathFormatted: "req_animal-api-sync-fb3ec",
stats: {
    "name": "Animal-api sync for customer NL_115893",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3025",
        "ok": "3025",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3025",
        "ok": "3025",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3025",
        "ok": "3025",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3025",
        "ok": "3025",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3025",
        "ok": "3025",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3025",
        "ok": "3025",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3025",
        "ok": "3025",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d90a9": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_119649",
path: "Animal-api sync for customer NL_119649",
pathFormatted: "req_animal-api-sync-d90a9",
stats: {
    "name": "Animal-api sync for customer NL_119649",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1831",
        "ok": "1831",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1831",
        "ok": "1831",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1831",
        "ok": "1831",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1831",
        "ok": "1831",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1831",
        "ok": "1831",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1831",
        "ok": "1831",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1831",
        "ok": "1831",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c4ec4": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104910",
path: "Animal-api sync for customer NL_104910",
pathFormatted: "req_animal-api-sync-c4ec4",
stats: {
    "name": "Animal-api sync for customer NL_104910",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2962",
        "ok": "2962",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2962",
        "ok": "2962",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2962",
        "ok": "2962",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2962",
        "ok": "2962",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2962",
        "ok": "2962",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2962",
        "ok": "2962",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2962",
        "ok": "2962",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e498b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110382",
path: "Animal-api sync for customer NL_110382",
pathFormatted: "req_animal-api-sync-e498b",
stats: {
    "name": "Animal-api sync for customer NL_110382",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3048",
        "ok": "3048",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3048",
        "ok": "3048",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3048",
        "ok": "3048",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3048",
        "ok": "3048",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3048",
        "ok": "3048",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3048",
        "ok": "3048",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3048",
        "ok": "3048",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-2a4dd": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_125297",
path: "Animal-api sync for customer NL_125297",
pathFormatted: "req_animal-api-sync-2a4dd",
stats: {
    "name": "Animal-api sync for customer NL_125297",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2616",
        "ok": "2616",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2616",
        "ok": "2616",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2616",
        "ok": "2616",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2616",
        "ok": "2616",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2616",
        "ok": "2616",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2616",
        "ok": "2616",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2616",
        "ok": "2616",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c1c6a": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_135207",
path: "Animal-api sync for customer NL_135207",
pathFormatted: "req_animal-api-sync-c1c6a",
stats: {
    "name": "Animal-api sync for customer NL_135207",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3512",
        "ok": "3512",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3512",
        "ok": "3512",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3512",
        "ok": "3512",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3512",
        "ok": "3512",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3512",
        "ok": "3512",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3512",
        "ok": "3512",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3512",
        "ok": "3512",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c170f": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_109428",
path: "Animal-api sync for customer NL_109428",
pathFormatted: "req_animal-api-sync-c170f",
stats: {
    "name": "Animal-api sync for customer NL_109428",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3536",
        "ok": "3536",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3536",
        "ok": "3536",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3536",
        "ok": "3536",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3536",
        "ok": "3536",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3536",
        "ok": "3536",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3536",
        "ok": "3536",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3536",
        "ok": "3536",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-690f7": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_129681",
path: "Animal-api sync for customer NL_129681",
pathFormatted: "req_animal-api-sync-690f7",
stats: {
    "name": "Animal-api sync for customer NL_129681",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2554",
        "ok": "2554",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2554",
        "ok": "2554",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2554",
        "ok": "2554",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2554",
        "ok": "2554",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2554",
        "ok": "2554",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2554",
        "ok": "2554",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2554",
        "ok": "2554",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-de0b1": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_164131",
path: "Animal-api sync for customer NL_164131",
pathFormatted: "req_animal-api-sync-de0b1",
stats: {
    "name": "Animal-api sync for customer NL_164131",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2797",
        "ok": "2797",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2797",
        "ok": "2797",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2797",
        "ok": "2797",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2797",
        "ok": "2797",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2797",
        "ok": "2797",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2797",
        "ok": "2797",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2797",
        "ok": "2797",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-3e31b": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_149431",
path: "Animal-api sync for customer BE_149431",
pathFormatted: "req_animal-api-sync-3e31b",
stats: {
    "name": "Animal-api sync for customer BE_149431",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2709",
        "ok": "2709",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2709",
        "ok": "2709",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2709",
        "ok": "2709",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2709",
        "ok": "2709",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2709",
        "ok": "2709",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2709",
        "ok": "2709",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2709",
        "ok": "2709",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-cb71c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_120709",
path: "Animal-api sync for customer NL_120709",
pathFormatted: "req_animal-api-sync-cb71c",
stats: {
    "name": "Animal-api sync for customer NL_120709",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4011",
        "ok": "4011",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4011",
        "ok": "4011",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4011",
        "ok": "4011",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4011",
        "ok": "4011",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4011",
        "ok": "4011",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4011",
        "ok": "4011",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4011",
        "ok": "4011",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-5a7e6": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105010",
path: "Animal-api sync for customer NL_105010",
pathFormatted: "req_animal-api-sync-5a7e6",
stats: {
    "name": "Animal-api sync for customer NL_105010",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4542",
        "ok": "4542",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4542",
        "ok": "4542",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4542",
        "ok": "4542",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4542",
        "ok": "4542",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4542",
        "ok": "4542",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4542",
        "ok": "4542",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4542",
        "ok": "4542",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-0eddf": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110820",
path: "Animal-api sync for customer NL_110820",
pathFormatted: "req_animal-api-sync-0eddf",
stats: {
    "name": "Animal-api sync for customer NL_110820",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4007",
        "ok": "4007",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4007",
        "ok": "4007",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4007",
        "ok": "4007",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4007",
        "ok": "4007",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4007",
        "ok": "4007",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4007",
        "ok": "4007",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4007",
        "ok": "4007",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-558a2": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_116270",
path: "Animal-api sync for customer NL_116270",
pathFormatted: "req_animal-api-sync-558a2",
stats: {
    "name": "Animal-api sync for customer NL_116270",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2542",
        "ok": "2542",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2542",
        "ok": "2542",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2542",
        "ok": "2542",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2542",
        "ok": "2542",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2542",
        "ok": "2542",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2542",
        "ok": "2542",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2542",
        "ok": "2542",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-3833c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_124824",
path: "Animal-api sync for customer NL_124824",
pathFormatted: "req_animal-api-sync-3833c",
stats: {
    "name": "Animal-api sync for customer NL_124824",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2163",
        "ok": "2163",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2163",
        "ok": "2163",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2163",
        "ok": "2163",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2163",
        "ok": "2163",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2163",
        "ok": "2163",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2163",
        "ok": "2163",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2163",
        "ok": "2163",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a3967": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117770",
path: "Animal-api sync for customer NL_117770",
pathFormatted: "req_animal-api-sync-a3967",
stats: {
    "name": "Animal-api sync for customer NL_117770",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3801",
        "ok": "3801",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3801",
        "ok": "3801",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3801",
        "ok": "3801",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3801",
        "ok": "3801",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3801",
        "ok": "3801",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3801",
        "ok": "3801",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3801",
        "ok": "3801",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-5a613": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_194583",
path: "Animal-api sync for customer NL_194583",
pathFormatted: "req_animal-api-sync-5a613",
stats: {
    "name": "Animal-api sync for customer NL_194583",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3206",
        "ok": "3206",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3206",
        "ok": "3206",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3206",
        "ok": "3206",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3206",
        "ok": "3206",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3206",
        "ok": "3206",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3206",
        "ok": "3206",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3206",
        "ok": "3206",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c7c5d": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_129379",
path: "Animal-api sync for customer NL_129379",
pathFormatted: "req_animal-api-sync-c7c5d",
stats: {
    "name": "Animal-api sync for customer NL_129379",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2870",
        "ok": "2870",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2870",
        "ok": "2870",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2870",
        "ok": "2870",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2870",
        "ok": "2870",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2870",
        "ok": "2870",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2870",
        "ok": "2870",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2870",
        "ok": "2870",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-91ebb": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_120778",
path: "Animal-api sync for customer NL_120778",
pathFormatted: "req_animal-api-sync-91ebb",
stats: {
    "name": "Animal-api sync for customer NL_120778",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2206",
        "ok": "2206",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2206",
        "ok": "2206",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2206",
        "ok": "2206",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2206",
        "ok": "2206",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2206",
        "ok": "2206",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2206",
        "ok": "2206",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2206",
        "ok": "2206",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-6d45c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_123012",
path: "Animal-api sync for customer NL_123012",
pathFormatted: "req_animal-api-sync-6d45c",
stats: {
    "name": "Animal-api sync for customer NL_123012",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1871",
        "ok": "1871",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1871",
        "ok": "1871",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1871",
        "ok": "1871",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1871",
        "ok": "1871",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1871",
        "ok": "1871",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1871",
        "ok": "1871",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1871",
        "ok": "1871",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-3ff93": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111287",
path: "Animal-api sync for customer NL_111287",
pathFormatted: "req_animal-api-sync-3ff93",
stats: {
    "name": "Animal-api sync for customer NL_111287",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2738",
        "ok": "2738",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2738",
        "ok": "2738",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2738",
        "ok": "2738",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2738",
        "ok": "2738",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2738",
        "ok": "2738",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2738",
        "ok": "2738",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2738",
        "ok": "2738",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-968ea": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_152278",
path: "Animal-api sync for customer BE_152278",
pathFormatted: "req_animal-api-sync-968ea",
stats: {
    "name": "Animal-api sync for customer BE_152278",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3764",
        "ok": "3764",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3764",
        "ok": "3764",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3764",
        "ok": "3764",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3764",
        "ok": "3764",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3764",
        "ok": "3764",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3764",
        "ok": "3764",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3764",
        "ok": "3764",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b979d": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_134839",
path: "Animal-api sync for customer NL_134839",
pathFormatted: "req_animal-api-sync-b979d",
stats: {
    "name": "Animal-api sync for customer NL_134839",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3269",
        "ok": "3269",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3269",
        "ok": "3269",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3269",
        "ok": "3269",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3269",
        "ok": "3269",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3269",
        "ok": "3269",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3269",
        "ok": "3269",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3269",
        "ok": "3269",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-4a2f0": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_119479",
path: "Animal-api sync for customer NL_119479",
pathFormatted: "req_animal-api-sync-4a2f0",
stats: {
    "name": "Animal-api sync for customer NL_119479",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3913",
        "ok": "3913",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3913",
        "ok": "3913",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3913",
        "ok": "3913",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3913",
        "ok": "3913",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3913",
        "ok": "3913",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3913",
        "ok": "3913",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3913",
        "ok": "3913",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-04f19": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_142633",
path: "Animal-api sync for customer NL_142633",
pathFormatted: "req_animal-api-sync-04f19",
stats: {
    "name": "Animal-api sync for customer NL_142633",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3963",
        "ok": "3963",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3963",
        "ok": "3963",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3963",
        "ok": "3963",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3963",
        "ok": "3963",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3963",
        "ok": "3963",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3963",
        "ok": "3963",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3963",
        "ok": "3963",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c6934": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_144844",
path: "Animal-api sync for customer BE_144844",
pathFormatted: "req_animal-api-sync-c6934",
stats: {
    "name": "Animal-api sync for customer BE_144844",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4240",
        "ok": "4240",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4240",
        "ok": "4240",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4240",
        "ok": "4240",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4240",
        "ok": "4240",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4240",
        "ok": "4240",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4240",
        "ok": "4240",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4240",
        "ok": "4240",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-ee05d": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_106934",
path: "Animal-api sync for customer NL_106934",
pathFormatted: "req_animal-api-sync-ee05d",
stats: {
    "name": "Animal-api sync for customer NL_106934",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4419",
        "ok": "4419",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4419",
        "ok": "4419",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4419",
        "ok": "4419",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4419",
        "ok": "4419",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4419",
        "ok": "4419",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4419",
        "ok": "4419",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4419",
        "ok": "4419",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-26bf9": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110058",
path: "Animal-api sync for customer NL_110058",
pathFormatted: "req_animal-api-sync-26bf9",
stats: {
    "name": "Animal-api sync for customer NL_110058",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3934",
        "ok": "3934",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3934",
        "ok": "3934",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3934",
        "ok": "3934",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3934",
        "ok": "3934",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3934",
        "ok": "3934",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3934",
        "ok": "3934",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3934",
        "ok": "3934",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a42d0": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105360",
path: "Animal-api sync for customer NL_105360",
pathFormatted: "req_animal-api-sync-a42d0",
stats: {
    "name": "Animal-api sync for customer NL_105360",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4263",
        "ok": "4263",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4263",
        "ok": "4263",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4263",
        "ok": "4263",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4263",
        "ok": "4263",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4263",
        "ok": "4263",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4263",
        "ok": "4263",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4263",
        "ok": "4263",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-35753": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_118795",
path: "Animal-api sync for customer NL_118795",
pathFormatted: "req_animal-api-sync-35753",
stats: {
    "name": "Animal-api sync for customer NL_118795",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2491",
        "ok": "2491",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2491",
        "ok": "2491",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2491",
        "ok": "2491",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2491",
        "ok": "2491",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2491",
        "ok": "2491",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2491",
        "ok": "2491",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2491",
        "ok": "2491",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-09a8d": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117444",
path: "Animal-api sync for customer NL_117444",
pathFormatted: "req_animal-api-sync-09a8d",
stats: {
    "name": "Animal-api sync for customer NL_117444",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3090",
        "ok": "3090",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3090",
        "ok": "3090",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3090",
        "ok": "3090",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3090",
        "ok": "3090",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3090",
        "ok": "3090",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3090",
        "ok": "3090",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3090",
        "ok": "3090",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-7c5cb": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_118794",
path: "Animal-api sync for customer NL_118794",
pathFormatted: "req_animal-api-sync-7c5cb",
stats: {
    "name": "Animal-api sync for customer NL_118794",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3120",
        "ok": "3120",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3120",
        "ok": "3120",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3120",
        "ok": "3120",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3120",
        "ok": "3120",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3120",
        "ok": "3120",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3120",
        "ok": "3120",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3120",
        "ok": "3120",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e202d": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_112066",
path: "Animal-api sync for customer NL_112066",
pathFormatted: "req_animal-api-sync-e202d",
stats: {
    "name": "Animal-api sync for customer NL_112066",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2637",
        "ok": "2637",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2637",
        "ok": "2637",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2637",
        "ok": "2637",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2637",
        "ok": "2637",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2637",
        "ok": "2637",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2637",
        "ok": "2637",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2637",
        "ok": "2637",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-be37d": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_106236",
path: "Animal-api sync for customer NL_106236",
pathFormatted: "req_animal-api-sync-be37d",
stats: {
    "name": "Animal-api sync for customer NL_106236",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2381",
        "ok": "2381",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2381",
        "ok": "2381",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2381",
        "ok": "2381",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2381",
        "ok": "2381",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2381",
        "ok": "2381",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2381",
        "ok": "2381",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2381",
        "ok": "2381",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-ca7e3": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_134297",
path: "Animal-api sync for customer NL_134297",
pathFormatted: "req_animal-api-sync-ca7e3",
stats: {
    "name": "Animal-api sync for customer NL_134297",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2446",
        "ok": "2446",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2446",
        "ok": "2446",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2446",
        "ok": "2446",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2446",
        "ok": "2446",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2446",
        "ok": "2446",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2446",
        "ok": "2446",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2446",
        "ok": "2446",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-993d0": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_160647",
path: "Animal-api sync for customer NL_160647",
pathFormatted: "req_animal-api-sync-993d0",
stats: {
    "name": "Animal-api sync for customer NL_160647",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5175",
        "ok": "5175",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5175",
        "ok": "5175",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5175",
        "ok": "5175",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5175",
        "ok": "5175",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5175",
        "ok": "5175",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5175",
        "ok": "5175",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5175",
        "ok": "5175",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-1e171": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111917",
path: "Animal-api sync for customer NL_111917",
pathFormatted: "req_animal-api-sync-1e171",
stats: {
    "name": "Animal-api sync for customer NL_111917",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2709",
        "ok": "2709",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2709",
        "ok": "2709",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2709",
        "ok": "2709",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2709",
        "ok": "2709",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2709",
        "ok": "2709",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2709",
        "ok": "2709",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2709",
        "ok": "2709",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-5e0b3": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_106214",
path: "Animal-api sync for customer NL_106214",
pathFormatted: "req_animal-api-sync-5e0b3",
stats: {
    "name": "Animal-api sync for customer NL_106214",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2991",
        "ok": "2991",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2991",
        "ok": "2991",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2991",
        "ok": "2991",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2991",
        "ok": "2991",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2991",
        "ok": "2991",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2991",
        "ok": "2991",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2991",
        "ok": "2991",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a7313": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_123953",
path: "Animal-api sync for customer NL_123953",
pathFormatted: "req_animal-api-sync-a7313",
stats: {
    "name": "Animal-api sync for customer NL_123953",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3603",
        "ok": "3603",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3603",
        "ok": "3603",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3603",
        "ok": "3603",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3603",
        "ok": "3603",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3603",
        "ok": "3603",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3603",
        "ok": "3603",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3603",
        "ok": "3603",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-7cf01": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104284",
path: "Animal-api sync for customer NL_104284",
pathFormatted: "req_animal-api-sync-7cf01",
stats: {
    "name": "Animal-api sync for customer NL_104284",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2383",
        "ok": "2383",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2383",
        "ok": "2383",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2383",
        "ok": "2383",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2383",
        "ok": "2383",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2383",
        "ok": "2383",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2383",
        "ok": "2383",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2383",
        "ok": "2383",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-588aa": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_214938",
path: "Animal-api sync for customer NL_214938",
pathFormatted: "req_animal-api-sync-588aa",
stats: {
    "name": "Animal-api sync for customer NL_214938",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3013",
        "ok": "3013",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3013",
        "ok": "3013",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3013",
        "ok": "3013",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3013",
        "ok": "3013",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3013",
        "ok": "3013",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3013",
        "ok": "3013",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3013",
        "ok": "3013",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-6ebe1": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104335",
path: "Animal-api sync for customer NL_104335",
pathFormatted: "req_animal-api-sync-6ebe1",
stats: {
    "name": "Animal-api sync for customer NL_104335",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3634",
        "ok": "3634",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3634",
        "ok": "3634",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3634",
        "ok": "3634",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3634",
        "ok": "3634",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3634",
        "ok": "3634",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3634",
        "ok": "3634",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3634",
        "ok": "3634",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d86cb": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105153",
path: "Animal-api sync for customer NL_105153",
pathFormatted: "req_animal-api-sync-d86cb",
stats: {
    "name": "Animal-api sync for customer NL_105153",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3043",
        "ok": "3043",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3043",
        "ok": "3043",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3043",
        "ok": "3043",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3043",
        "ok": "3043",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3043",
        "ok": "3043",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3043",
        "ok": "3043",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3043",
        "ok": "3043",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b6645": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_126020",
path: "Animal-api sync for customer NL_126020",
pathFormatted: "req_animal-api-sync-b6645",
stats: {
    "name": "Animal-api sync for customer NL_126020",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2758",
        "ok": "2758",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2758",
        "ok": "2758",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2758",
        "ok": "2758",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2758",
        "ok": "2758",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2758",
        "ok": "2758",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2758",
        "ok": "2758",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2758",
        "ok": "2758",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-7ae4d": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_137252",
path: "Animal-api sync for customer NL_137252",
pathFormatted: "req_animal-api-sync-7ae4d",
stats: {
    "name": "Animal-api sync for customer NL_137252",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2408",
        "ok": "2408",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2408",
        "ok": "2408",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2408",
        "ok": "2408",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2408",
        "ok": "2408",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2408",
        "ok": "2408",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2408",
        "ok": "2408",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2408",
        "ok": "2408",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-f2327": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_107409",
path: "Animal-api sync for customer NL_107409",
pathFormatted: "req_animal-api-sync-f2327",
stats: {
    "name": "Animal-api sync for customer NL_107409",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2431",
        "ok": "2431",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2431",
        "ok": "2431",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2431",
        "ok": "2431",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2431",
        "ok": "2431",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2431",
        "ok": "2431",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2431",
        "ok": "2431",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2431",
        "ok": "2431",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-9b90a": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_114569",
path: "Animal-api sync for customer NL_114569",
pathFormatted: "req_animal-api-sync-9b90a",
stats: {
    "name": "Animal-api sync for customer NL_114569",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2293",
        "ok": "2293",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2293",
        "ok": "2293",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2293",
        "ok": "2293",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2293",
        "ok": "2293",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2293",
        "ok": "2293",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2293",
        "ok": "2293",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2293",
        "ok": "2293",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-56e97": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_130716",
path: "Animal-api sync for customer NL_130716",
pathFormatted: "req_animal-api-sync-56e97",
stats: {
    "name": "Animal-api sync for customer NL_130716",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2207",
        "ok": "2207",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2207",
        "ok": "2207",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2207",
        "ok": "2207",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2207",
        "ok": "2207",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2207",
        "ok": "2207",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2207",
        "ok": "2207",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2207",
        "ok": "2207",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-4b1c0": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_164635",
path: "Animal-api sync for customer BE_164635",
pathFormatted: "req_animal-api-sync-4b1c0",
stats: {
    "name": "Animal-api sync for customer BE_164635",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2973",
        "ok": "2973",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2973",
        "ok": "2973",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2973",
        "ok": "2973",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2973",
        "ok": "2973",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2973",
        "ok": "2973",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2973",
        "ok": "2973",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2973",
        "ok": "2973",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-0be59": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_137530",
path: "Animal-api sync for customer NL_137530",
pathFormatted: "req_animal-api-sync-0be59",
stats: {
    "name": "Animal-api sync for customer NL_137530",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2382",
        "ok": "2382",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2382",
        "ok": "2382",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2382",
        "ok": "2382",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2382",
        "ok": "2382",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2382",
        "ok": "2382",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2382",
        "ok": "2382",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2382",
        "ok": "2382",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e4170": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_205597",
path: "Animal-api sync for customer BE_205597",
pathFormatted: "req_animal-api-sync-e4170",
stats: {
    "name": "Animal-api sync for customer BE_205597",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3700",
        "ok": "3700",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3700",
        "ok": "3700",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3700",
        "ok": "3700",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3700",
        "ok": "3700",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3700",
        "ok": "3700",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3700",
        "ok": "3700",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3700",
        "ok": "3700",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-5e066": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_118716",
path: "Animal-api sync for customer NL_118716",
pathFormatted: "req_animal-api-sync-5e066",
stats: {
    "name": "Animal-api sync for customer NL_118716",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2966",
        "ok": "2966",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2966",
        "ok": "2966",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2966",
        "ok": "2966",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2966",
        "ok": "2966",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2966",
        "ok": "2966",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2966",
        "ok": "2966",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2966",
        "ok": "2966",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-3598c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_107584",
path: "Animal-api sync for customer NL_107584",
pathFormatted: "req_animal-api-sync-3598c",
stats: {
    "name": "Animal-api sync for customer NL_107584",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2482",
        "ok": "2482",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2482",
        "ok": "2482",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2482",
        "ok": "2482",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2482",
        "ok": "2482",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2482",
        "ok": "2482",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2482",
        "ok": "2482",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2482",
        "ok": "2482",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-6b068": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111950",
path: "Animal-api sync for customer NL_111950",
pathFormatted: "req_animal-api-sync-6b068",
stats: {
    "name": "Animal-api sync for customer NL_111950",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3879",
        "ok": "3879",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3879",
        "ok": "3879",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3879",
        "ok": "3879",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3879",
        "ok": "3879",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3879",
        "ok": "3879",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3879",
        "ok": "3879",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3879",
        "ok": "3879",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-34e6f": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_129959",
path: "Animal-api sync for customer NL_129959",
pathFormatted: "req_animal-api-sync-34e6f",
stats: {
    "name": "Animal-api sync for customer NL_129959",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2879",
        "ok": "2879",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2879",
        "ok": "2879",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2879",
        "ok": "2879",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2879",
        "ok": "2879",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2879",
        "ok": "2879",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2879",
        "ok": "2879",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2879",
        "ok": "2879",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-21b03": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_109721",
path: "Animal-api sync for customer NL_109721",
pathFormatted: "req_animal-api-sync-21b03",
stats: {
    "name": "Animal-api sync for customer NL_109721",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4002",
        "ok": "4002",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4002",
        "ok": "4002",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4002",
        "ok": "4002",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4002",
        "ok": "4002",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4002",
        "ok": "4002",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4002",
        "ok": "4002",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4002",
        "ok": "4002",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-24d15": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117331",
path: "Animal-api sync for customer NL_117331",
pathFormatted: "req_animal-api-sync-24d15",
stats: {
    "name": "Animal-api sync for customer NL_117331",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3120",
        "ok": "3120",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3120",
        "ok": "3120",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3120",
        "ok": "3120",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3120",
        "ok": "3120",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3120",
        "ok": "3120",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3120",
        "ok": "3120",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3120",
        "ok": "3120",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-4479f": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_134641",
path: "Animal-api sync for customer NL_134641",
pathFormatted: "req_animal-api-sync-4479f",
stats: {
    "name": "Animal-api sync for customer NL_134641",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1472",
        "ok": "1472",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1472",
        "ok": "1472",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1472",
        "ok": "1472",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1472",
        "ok": "1472",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1472",
        "ok": "1472",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1472",
        "ok": "1472",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1472",
        "ok": "1472",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-2ac98": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_119983",
path: "Animal-api sync for customer NL_119983",
pathFormatted: "req_animal-api-sync-2ac98",
stats: {
    "name": "Animal-api sync for customer NL_119983",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3335",
        "ok": "3335",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3335",
        "ok": "3335",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3335",
        "ok": "3335",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3335",
        "ok": "3335",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3335",
        "ok": "3335",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3335",
        "ok": "3335",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3335",
        "ok": "3335",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-53e53": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_122724",
path: "Animal-api sync for customer NL_122724",
pathFormatted: "req_animal-api-sync-53e53",
stats: {
    "name": "Animal-api sync for customer NL_122724",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3011",
        "ok": "3011",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3011",
        "ok": "3011",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3011",
        "ok": "3011",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3011",
        "ok": "3011",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3011",
        "ok": "3011",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3011",
        "ok": "3011",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3011",
        "ok": "3011",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-97824": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_129903",
path: "Animal-api sync for customer NL_129903",
pathFormatted: "req_animal-api-sync-97824",
stats: {
    "name": "Animal-api sync for customer NL_129903",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2527",
        "ok": "2527",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2527",
        "ok": "2527",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2527",
        "ok": "2527",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2527",
        "ok": "2527",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2527",
        "ok": "2527",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2527",
        "ok": "2527",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2527",
        "ok": "2527",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-9db7a": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_125745",
path: "Animal-api sync for customer NL_125745",
pathFormatted: "req_animal-api-sync-9db7a",
stats: {
    "name": "Animal-api sync for customer NL_125745",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3234",
        "ok": "3234",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3234",
        "ok": "3234",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3234",
        "ok": "3234",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3234",
        "ok": "3234",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3234",
        "ok": "3234",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3234",
        "ok": "3234",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3234",
        "ok": "3234",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-62afe": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_128808",
path: "Animal-api sync for customer NL_128808",
pathFormatted: "req_animal-api-sync-62afe",
stats: {
    "name": "Animal-api sync for customer NL_128808",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3003",
        "ok": "3003",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3003",
        "ok": "3003",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3003",
        "ok": "3003",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3003",
        "ok": "3003",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3003",
        "ok": "3003",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3003",
        "ok": "3003",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3003",
        "ok": "3003",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d8b1c": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_214192",
path: "Animal-api sync for customer BE_214192",
pathFormatted: "req_animal-api-sync-d8b1c",
stats: {
    "name": "Animal-api sync for customer BE_214192",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1228",
        "ok": "1228",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1228",
        "ok": "1228",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1228",
        "ok": "1228",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1228",
        "ok": "1228",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1228",
        "ok": "1228",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1228",
        "ok": "1228",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1228",
        "ok": "1228",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-22fcc": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110755",
path: "Animal-api sync for customer NL_110755",
pathFormatted: "req_animal-api-sync-22fcc",
stats: {
    "name": "Animal-api sync for customer NL_110755",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3041",
        "ok": "3041",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3041",
        "ok": "3041",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3041",
        "ok": "3041",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3041",
        "ok": "3041",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3041",
        "ok": "3041",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3041",
        "ok": "3041",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3041",
        "ok": "3041",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-1330d": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_145902",
path: "Animal-api sync for customer BE_145902",
pathFormatted: "req_animal-api-sync-1330d",
stats: {
    "name": "Animal-api sync for customer BE_145902",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2671",
        "ok": "2671",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2671",
        "ok": "2671",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2671",
        "ok": "2671",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2671",
        "ok": "2671",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2671",
        "ok": "2671",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2671",
        "ok": "2671",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2671",
        "ok": "2671",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-2cece": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110572",
path: "Animal-api sync for customer NL_110572",
pathFormatted: "req_animal-api-sync-2cece",
stats: {
    "name": "Animal-api sync for customer NL_110572",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2507",
        "ok": "2507",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2507",
        "ok": "2507",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2507",
        "ok": "2507",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2507",
        "ok": "2507",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2507",
        "ok": "2507",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2507",
        "ok": "2507",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2507",
        "ok": "2507",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-184df": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110881",
path: "Animal-api sync for customer NL_110881",
pathFormatted: "req_animal-api-sync-184df",
stats: {
    "name": "Animal-api sync for customer NL_110881",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2562",
        "ok": "2562",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2562",
        "ok": "2562",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2562",
        "ok": "2562",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2562",
        "ok": "2562",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2562",
        "ok": "2562",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2562",
        "ok": "2562",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2562",
        "ok": "2562",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-61fa0": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_214412",
path: "Animal-api sync for customer NL_214412",
pathFormatted: "req_animal-api-sync-61fa0",
stats: {
    "name": "Animal-api sync for customer NL_214412",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2512",
        "ok": "2512",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2512",
        "ok": "2512",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2512",
        "ok": "2512",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2512",
        "ok": "2512",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2512",
        "ok": "2512",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2512",
        "ok": "2512",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2512",
        "ok": "2512",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-2f673": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_112286",
path: "Animal-api sync for customer NL_112286",
pathFormatted: "req_animal-api-sync-2f673",
stats: {
    "name": "Animal-api sync for customer NL_112286",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1044",
        "ok": "1044",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1044",
        "ok": "1044",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1044",
        "ok": "1044",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1044",
        "ok": "1044",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1044",
        "ok": "1044",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1044",
        "ok": "1044",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1044",
        "ok": "1044",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-7a391": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_125864",
path: "Animal-api sync for customer NL_125864",
pathFormatted: "req_animal-api-sync-7a391",
stats: {
    "name": "Animal-api sync for customer NL_125864",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2842",
        "ok": "2842",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2842",
        "ok": "2842",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2842",
        "ok": "2842",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2842",
        "ok": "2842",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2842",
        "ok": "2842",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2842",
        "ok": "2842",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2842",
        "ok": "2842",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-844f0": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_128832",
path: "Animal-api sync for customer NL_128832",
pathFormatted: "req_animal-api-sync-844f0",
stats: {
    "name": "Animal-api sync for customer NL_128832",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2433",
        "ok": "2433",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2433",
        "ok": "2433",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2433",
        "ok": "2433",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2433",
        "ok": "2433",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2433",
        "ok": "2433",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2433",
        "ok": "2433",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2433",
        "ok": "2433",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-4e25d": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104043",
path: "Animal-api sync for customer NL_104043",
pathFormatted: "req_animal-api-sync-4e25d",
stats: {
    "name": "Animal-api sync for customer NL_104043",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3008",
        "ok": "3008",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3008",
        "ok": "3008",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3008",
        "ok": "3008",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3008",
        "ok": "3008",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3008",
        "ok": "3008",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3008",
        "ok": "3008",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3008",
        "ok": "3008",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-25426": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_114351",
path: "Animal-api sync for customer NL_114351",
pathFormatted: "req_animal-api-sync-25426",
stats: {
    "name": "Animal-api sync for customer NL_114351",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2008",
        "ok": "2008",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2008",
        "ok": "2008",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2008",
        "ok": "2008",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2008",
        "ok": "2008",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2008",
        "ok": "2008",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2008",
        "ok": "2008",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2008",
        "ok": "2008",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-dfb65": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_108668",
path: "Animal-api sync for customer NL_108668",
pathFormatted: "req_animal-api-sync-dfb65",
stats: {
    "name": "Animal-api sync for customer NL_108668",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2516",
        "ok": "2516",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2516",
        "ok": "2516",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2516",
        "ok": "2516",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2516",
        "ok": "2516",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2516",
        "ok": "2516",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2516",
        "ok": "2516",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2516",
        "ok": "2516",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-997d2": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111823",
path: "Animal-api sync for customer NL_111823",
pathFormatted: "req_animal-api-sync-997d2",
stats: {
    "name": "Animal-api sync for customer NL_111823",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3012",
        "ok": "3012",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3012",
        "ok": "3012",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3012",
        "ok": "3012",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3012",
        "ok": "3012",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3012",
        "ok": "3012",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3012",
        "ok": "3012",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3012",
        "ok": "3012",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d5cc8": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_103579",
path: "Animal-api sync for customer NL_103579",
pathFormatted: "req_animal-api-sync-d5cc8",
stats: {
    "name": "Animal-api sync for customer NL_103579",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2703",
        "ok": "2703",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2703",
        "ok": "2703",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2703",
        "ok": "2703",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2703",
        "ok": "2703",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2703",
        "ok": "2703",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2703",
        "ok": "2703",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2703",
        "ok": "2703",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-96467": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_134577",
path: "Animal-api sync for customer NL_134577",
pathFormatted: "req_animal-api-sync-96467",
stats: {
    "name": "Animal-api sync for customer NL_134577",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2356",
        "ok": "2356",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2356",
        "ok": "2356",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2356",
        "ok": "2356",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2356",
        "ok": "2356",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2356",
        "ok": "2356",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2356",
        "ok": "2356",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2356",
        "ok": "2356",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-4c254": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105650",
path: "Animal-api sync for customer NL_105650",
pathFormatted: "req_animal-api-sync-4c254",
stats: {
    "name": "Animal-api sync for customer NL_105650",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2300",
        "ok": "2300",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2300",
        "ok": "2300",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2300",
        "ok": "2300",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2300",
        "ok": "2300",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2300",
        "ok": "2300",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2300",
        "ok": "2300",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2300",
        "ok": "2300",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-5ea6b": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_194847",
path: "Animal-api sync for customer BE_194847",
pathFormatted: "req_animal-api-sync-5ea6b",
stats: {
    "name": "Animal-api sync for customer BE_194847",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4068",
        "ok": "4068",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4068",
        "ok": "4068",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4068",
        "ok": "4068",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4068",
        "ok": "4068",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4068",
        "ok": "4068",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4068",
        "ok": "4068",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4068",
        "ok": "4068",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a4924": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117341",
path: "Animal-api sync for customer NL_117341",
pathFormatted: "req_animal-api-sync-a4924",
stats: {
    "name": "Animal-api sync for customer NL_117341",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2510",
        "ok": "2510",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2510",
        "ok": "2510",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2510",
        "ok": "2510",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2510",
        "ok": "2510",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2510",
        "ok": "2510",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2510",
        "ok": "2510",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2510",
        "ok": "2510",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-12331": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_113115",
path: "Animal-api sync for customer NL_113115",
pathFormatted: "req_animal-api-sync-12331",
stats: {
    "name": "Animal-api sync for customer NL_113115",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3503",
        "ok": "3503",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3503",
        "ok": "3503",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3503",
        "ok": "3503",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3503",
        "ok": "3503",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3503",
        "ok": "3503",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3503",
        "ok": "3503",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3503",
        "ok": "3503",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-13f62": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_149314",
path: "Animal-api sync for customer BE_149314",
pathFormatted: "req_animal-api-sync-13f62",
stats: {
    "name": "Animal-api sync for customer BE_149314",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2448",
        "ok": "2448",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2448",
        "ok": "2448",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2448",
        "ok": "2448",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2448",
        "ok": "2448",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2448",
        "ok": "2448",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2448",
        "ok": "2448",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2448",
        "ok": "2448",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-500bc": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117068",
path: "Animal-api sync for customer NL_117068",
pathFormatted: "req_animal-api-sync-500bc",
stats: {
    "name": "Animal-api sync for customer NL_117068",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1781",
        "ok": "1781",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1781",
        "ok": "1781",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1781",
        "ok": "1781",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1781",
        "ok": "1781",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1781",
        "ok": "1781",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1781",
        "ok": "1781",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1781",
        "ok": "1781",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-6d4c5": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_122660",
path: "Animal-api sync for customer NL_122660",
pathFormatted: "req_animal-api-sync-6d4c5",
stats: {
    "name": "Animal-api sync for customer NL_122660",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1004",
        "ok": "1004",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1004",
        "ok": "1004",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1004",
        "ok": "1004",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1004",
        "ok": "1004",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1004",
        "ok": "1004",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1004",
        "ok": "1004",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1004",
        "ok": "1004",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-cb570": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104777",
path: "Animal-api sync for customer NL_104777",
pathFormatted: "req_animal-api-sync-cb570",
stats: {
    "name": "Animal-api sync for customer NL_104777",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3538",
        "ok": "3538",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3538",
        "ok": "3538",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3538",
        "ok": "3538",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3538",
        "ok": "3538",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3538",
        "ok": "3538",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3538",
        "ok": "3538",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3538",
        "ok": "3538",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-49e6e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_122219",
path: "Animal-api sync for customer NL_122219",
pathFormatted: "req_animal-api-sync-49e6e",
stats: {
    "name": "Animal-api sync for customer NL_122219",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2535",
        "ok": "2535",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2535",
        "ok": "2535",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2535",
        "ok": "2535",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2535",
        "ok": "2535",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2535",
        "ok": "2535",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2535",
        "ok": "2535",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2535",
        "ok": "2535",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b0552": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_122008",
path: "Animal-api sync for customer NL_122008",
pathFormatted: "req_animal-api-sync-b0552",
stats: {
    "name": "Animal-api sync for customer NL_122008",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4930",
        "ok": "4930",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4930",
        "ok": "4930",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4930",
        "ok": "4930",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4930",
        "ok": "4930",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4930",
        "ok": "4930",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4930",
        "ok": "4930",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4930",
        "ok": "4930",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-7d526": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105576",
path: "Animal-api sync for customer NL_105576",
pathFormatted: "req_animal-api-sync-7d526",
stats: {
    "name": "Animal-api sync for customer NL_105576",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3011",
        "ok": "3011",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3011",
        "ok": "3011",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3011",
        "ok": "3011",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3011",
        "ok": "3011",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3011",
        "ok": "3011",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3011",
        "ok": "3011",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3011",
        "ok": "3011",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a6cf9": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104168",
path: "Animal-api sync for customer NL_104168",
pathFormatted: "req_animal-api-sync-a6cf9",
stats: {
    "name": "Animal-api sync for customer NL_104168",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3737",
        "ok": "3737",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3737",
        "ok": "3737",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3737",
        "ok": "3737",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3737",
        "ok": "3737",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3737",
        "ok": "3737",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3737",
        "ok": "3737",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3737",
        "ok": "3737",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-6809e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117264",
path: "Animal-api sync for customer NL_117264",
pathFormatted: "req_animal-api-sync-6809e",
stats: {
    "name": "Animal-api sync for customer NL_117264",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3538",
        "ok": "3538",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3538",
        "ok": "3538",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3538",
        "ok": "3538",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3538",
        "ok": "3538",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3538",
        "ok": "3538",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3538",
        "ok": "3538",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3538",
        "ok": "3538",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-cb15a": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_114531",
path: "Animal-api sync for customer NL_114531",
pathFormatted: "req_animal-api-sync-cb15a",
stats: {
    "name": "Animal-api sync for customer NL_114531",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2575",
        "ok": "2575",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2575",
        "ok": "2575",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2575",
        "ok": "2575",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2575",
        "ok": "2575",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2575",
        "ok": "2575",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2575",
        "ok": "2575",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2575",
        "ok": "2575",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-aa469": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_158913",
path: "Animal-api sync for customer BE_158913",
pathFormatted: "req_animal-api-sync-aa469",
stats: {
    "name": "Animal-api sync for customer BE_158913",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3671",
        "ok": "3671",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3671",
        "ok": "3671",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3671",
        "ok": "3671",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3671",
        "ok": "3671",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3671",
        "ok": "3671",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3671",
        "ok": "3671",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3671",
        "ok": "3671",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-fc480": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_121609",
path: "Animal-api sync for customer NL_121609",
pathFormatted: "req_animal-api-sync-fc480",
stats: {
    "name": "Animal-api sync for customer NL_121609",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2747",
        "ok": "2747",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2747",
        "ok": "2747",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2747",
        "ok": "2747",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2747",
        "ok": "2747",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2747",
        "ok": "2747",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2747",
        "ok": "2747",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2747",
        "ok": "2747",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-2fa8f": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_103719",
path: "Animal-api sync for customer NL_103719",
pathFormatted: "req_animal-api-sync-2fa8f",
stats: {
    "name": "Animal-api sync for customer NL_103719",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2885",
        "ok": "2885",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2885",
        "ok": "2885",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2885",
        "ok": "2885",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2885",
        "ok": "2885",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2885",
        "ok": "2885",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2885",
        "ok": "2885",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2885",
        "ok": "2885",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c7578": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105839",
path: "Animal-api sync for customer NL_105839",
pathFormatted: "req_animal-api-sync-c7578",
stats: {
    "name": "Animal-api sync for customer NL_105839",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3652",
        "ok": "3652",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3652",
        "ok": "3652",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3652",
        "ok": "3652",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3652",
        "ok": "3652",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3652",
        "ok": "3652",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3652",
        "ok": "3652",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3652",
        "ok": "3652",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-ca9a4": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_123159",
path: "Animal-api sync for customer NL_123159",
pathFormatted: "req_animal-api-sync-ca9a4",
stats: {
    "name": "Animal-api sync for customer NL_123159",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1710",
        "ok": "1710",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1710",
        "ok": "1710",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1710",
        "ok": "1710",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1710",
        "ok": "1710",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1710",
        "ok": "1710",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1710",
        "ok": "1710",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1710",
        "ok": "1710",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d58c5": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_115162",
path: "Animal-api sync for customer NL_115162",
pathFormatted: "req_animal-api-sync-d58c5",
stats: {
    "name": "Animal-api sync for customer NL_115162",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1377",
        "ok": "1377",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1377",
        "ok": "1377",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1377",
        "ok": "1377",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1377",
        "ok": "1377",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1377",
        "ok": "1377",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1377",
        "ok": "1377",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1377",
        "ok": "1377",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d43d8": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_112453",
path: "Animal-api sync for customer NL_112453",
pathFormatted: "req_animal-api-sync-d43d8",
stats: {
    "name": "Animal-api sync for customer NL_112453",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2019",
        "ok": "2019",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2019",
        "ok": "2019",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2019",
        "ok": "2019",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2019",
        "ok": "2019",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2019",
        "ok": "2019",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2019",
        "ok": "2019",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2019",
        "ok": "2019",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-fa50f": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_194938",
path: "Animal-api sync for customer NL_194938",
pathFormatted: "req_animal-api-sync-fa50f",
stats: {
    "name": "Animal-api sync for customer NL_194938",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3683",
        "ok": "3683",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3683",
        "ok": "3683",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3683",
        "ok": "3683",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3683",
        "ok": "3683",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3683",
        "ok": "3683",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3683",
        "ok": "3683",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3683",
        "ok": "3683",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d3cf7": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_123704",
path: "Animal-api sync for customer NL_123704",
pathFormatted: "req_animal-api-sync-d3cf7",
stats: {
    "name": "Animal-api sync for customer NL_123704",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2396",
        "ok": "2396",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2396",
        "ok": "2396",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2396",
        "ok": "2396",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2396",
        "ok": "2396",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2396",
        "ok": "2396",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2396",
        "ok": "2396",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2396",
        "ok": "2396",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e5ad2": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_108858",
path: "Animal-api sync for customer NL_108858",
pathFormatted: "req_animal-api-sync-e5ad2",
stats: {
    "name": "Animal-api sync for customer NL_108858",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3415",
        "ok": "3415",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3415",
        "ok": "3415",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3415",
        "ok": "3415",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3415",
        "ok": "3415",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3415",
        "ok": "3415",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3415",
        "ok": "3415",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3415",
        "ok": "3415",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-23adc": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110836",
path: "Animal-api sync for customer NL_110836",
pathFormatted: "req_animal-api-sync-23adc",
stats: {
    "name": "Animal-api sync for customer NL_110836",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2671",
        "ok": "2671",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2671",
        "ok": "2671",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2671",
        "ok": "2671",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2671",
        "ok": "2671",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2671",
        "ok": "2671",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2671",
        "ok": "2671",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2671",
        "ok": "2671",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-8d897": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_133100",
path: "Animal-api sync for customer NL_133100",
pathFormatted: "req_animal-api-sync-8d897",
stats: {
    "name": "Animal-api sync for customer NL_133100",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3184",
        "ok": "3184",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3184",
        "ok": "3184",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3184",
        "ok": "3184",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3184",
        "ok": "3184",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3184",
        "ok": "3184",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3184",
        "ok": "3184",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3184",
        "ok": "3184",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e921e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_122446",
path: "Animal-api sync for customer NL_122446",
pathFormatted: "req_animal-api-sync-e921e",
stats: {
    "name": "Animal-api sync for customer NL_122446",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2763",
        "ok": "2763",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2763",
        "ok": "2763",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2763",
        "ok": "2763",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2763",
        "ok": "2763",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2763",
        "ok": "2763",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2763",
        "ok": "2763",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2763",
        "ok": "2763",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-fd312": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117690",
path: "Animal-api sync for customer NL_117690",
pathFormatted: "req_animal-api-sync-fd312",
stats: {
    "name": "Animal-api sync for customer NL_117690",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3011",
        "ok": "3011",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3011",
        "ok": "3011",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3011",
        "ok": "3011",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3011",
        "ok": "3011",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3011",
        "ok": "3011",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3011",
        "ok": "3011",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3011",
        "ok": "3011",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c6f3a": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_119414",
path: "Animal-api sync for customer NL_119414",
pathFormatted: "req_animal-api-sync-c6f3a",
stats: {
    "name": "Animal-api sync for customer NL_119414",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2813",
        "ok": "2813",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2813",
        "ok": "2813",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2813",
        "ok": "2813",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2813",
        "ok": "2813",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2813",
        "ok": "2813",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2813",
        "ok": "2813",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2813",
        "ok": "2813",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a8229": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_129350",
path: "Animal-api sync for customer NL_129350",
pathFormatted: "req_animal-api-sync-a8229",
stats: {
    "name": "Animal-api sync for customer NL_129350",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "6416",
        "ok": "6416",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "6416",
        "ok": "6416",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "6416",
        "ok": "6416",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "6416",
        "ok": "6416",
        "ko": "-"
    },
    "percentiles2": {
        "total": "6416",
        "ok": "6416",
        "ko": "-"
    },
    "percentiles3": {
        "total": "6416",
        "ok": "6416",
        "ko": "-"
    },
    "percentiles4": {
        "total": "6416",
        "ok": "6416",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e7e4c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105106",
path: "Animal-api sync for customer NL_105106",
pathFormatted: "req_animal-api-sync-e7e4c",
stats: {
    "name": "Animal-api sync for customer NL_105106",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3010",
        "ok": "3010",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3010",
        "ok": "3010",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3010",
        "ok": "3010",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3010",
        "ok": "3010",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3010",
        "ok": "3010",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3010",
        "ok": "3010",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3010",
        "ok": "3010",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c3423": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_112704",
path: "Animal-api sync for customer NL_112704",
pathFormatted: "req_animal-api-sync-c3423",
stats: {
    "name": "Animal-api sync for customer NL_112704",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4626",
        "ok": "4626",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4626",
        "ok": "4626",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4626",
        "ok": "4626",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4626",
        "ok": "4626",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4626",
        "ok": "4626",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4626",
        "ok": "4626",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4626",
        "ok": "4626",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c2f39": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_157878",
path: "Animal-api sync for customer BE_157878",
pathFormatted: "req_animal-api-sync-c2f39",
stats: {
    "name": "Animal-api sync for customer BE_157878",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4263",
        "ok": "4263",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4263",
        "ok": "4263",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4263",
        "ok": "4263",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4263",
        "ok": "4263",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4263",
        "ok": "4263",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4263",
        "ok": "4263",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4263",
        "ok": "4263",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-fee59": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117006",
path: "Animal-api sync for customer NL_117006",
pathFormatted: "req_animal-api-sync-fee59",
stats: {
    "name": "Animal-api sync for customer NL_117006",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2347",
        "ok": "2347",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2347",
        "ok": "2347",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2347",
        "ok": "2347",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2347",
        "ok": "2347",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2347",
        "ok": "2347",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2347",
        "ok": "2347",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2347",
        "ok": "2347",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-bab68": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_134874",
path: "Animal-api sync for customer NL_134874",
pathFormatted: "req_animal-api-sync-bab68",
stats: {
    "name": "Animal-api sync for customer NL_134874",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3718",
        "ok": "3718",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3718",
        "ok": "3718",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3718",
        "ok": "3718",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3718",
        "ok": "3718",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3718",
        "ok": "3718",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3718",
        "ok": "3718",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3718",
        "ok": "3718",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-3e99c": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_214193",
path: "Animal-api sync for customer BE_214193",
pathFormatted: "req_animal-api-sync-3e99c",
stats: {
    "name": "Animal-api sync for customer BE_214193",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3750",
        "ok": "3750",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3750",
        "ok": "3750",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3750",
        "ok": "3750",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3750",
        "ok": "3750",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3750",
        "ok": "3750",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3750",
        "ok": "3750",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3750",
        "ok": "3750",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b2bc0": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_162281",
path: "Animal-api sync for customer BE_162281",
pathFormatted: "req_animal-api-sync-b2bc0",
stats: {
    "name": "Animal-api sync for customer BE_162281",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5321",
        "ok": "5321",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5321",
        "ok": "5321",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5321",
        "ok": "5321",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5321",
        "ok": "5321",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5321",
        "ok": "5321",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5321",
        "ok": "5321",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5321",
        "ok": "5321",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-046eb": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_127329",
path: "Animal-api sync for customer NL_127329",
pathFormatted: "req_animal-api-sync-046eb",
stats: {
    "name": "Animal-api sync for customer NL_127329",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1631",
        "ok": "1631",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1631",
        "ok": "1631",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1631",
        "ok": "1631",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1631",
        "ok": "1631",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1631",
        "ok": "1631",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1631",
        "ok": "1631",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1631",
        "ok": "1631",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-7cc26": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_131009",
path: "Animal-api sync for customer NL_131009",
pathFormatted: "req_animal-api-sync-7cc26",
stats: {
    "name": "Animal-api sync for customer NL_131009",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3446",
        "ok": "3446",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3446",
        "ok": "3446",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3446",
        "ok": "3446",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3446",
        "ok": "3446",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3446",
        "ok": "3446",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3446",
        "ok": "3446",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3446",
        "ok": "3446",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-52d91": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_163987",
path: "Animal-api sync for customer BE_163987",
pathFormatted: "req_animal-api-sync-52d91",
stats: {
    "name": "Animal-api sync for customer BE_163987",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3369",
        "ok": "3369",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3369",
        "ok": "3369",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3369",
        "ok": "3369",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3369",
        "ok": "3369",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3369",
        "ok": "3369",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3369",
        "ok": "3369",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3369",
        "ok": "3369",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-9b768": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_115732",
path: "Animal-api sync for customer NL_115732",
pathFormatted: "req_animal-api-sync-9b768",
stats: {
    "name": "Animal-api sync for customer NL_115732",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2657",
        "ok": "2657",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2657",
        "ok": "2657",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2657",
        "ok": "2657",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2657",
        "ok": "2657",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2657",
        "ok": "2657",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2657",
        "ok": "2657",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2657",
        "ok": "2657",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c1337": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104350",
path: "Animal-api sync for customer NL_104350",
pathFormatted: "req_animal-api-sync-c1337",
stats: {
    "name": "Animal-api sync for customer NL_104350",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4159",
        "ok": "4159",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4159",
        "ok": "4159",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4159",
        "ok": "4159",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4159",
        "ok": "4159",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4159",
        "ok": "4159",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4159",
        "ok": "4159",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4159",
        "ok": "4159",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-de861": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_138595",
path: "Animal-api sync for customer NL_138595",
pathFormatted: "req_animal-api-sync-de861",
stats: {
    "name": "Animal-api sync for customer NL_138595",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2273",
        "ok": "2273",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2273",
        "ok": "2273",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2273",
        "ok": "2273",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2273",
        "ok": "2273",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2273",
        "ok": "2273",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2273",
        "ok": "2273",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2273",
        "ok": "2273",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-cc8e5": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_123944",
path: "Animal-api sync for customer NL_123944",
pathFormatted: "req_animal-api-sync-cc8e5",
stats: {
    "name": "Animal-api sync for customer NL_123944",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2238",
        "ok": "2238",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2238",
        "ok": "2238",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2238",
        "ok": "2238",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2238",
        "ok": "2238",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2238",
        "ok": "2238",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2238",
        "ok": "2238",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2238",
        "ok": "2238",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-033e7": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_113983",
path: "Animal-api sync for customer NL_113983",
pathFormatted: "req_animal-api-sync-033e7",
stats: {
    "name": "Animal-api sync for customer NL_113983",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1612",
        "ok": "1612",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1612",
        "ok": "1612",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1612",
        "ok": "1612",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1612",
        "ok": "1612",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1612",
        "ok": "1612",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1612",
        "ok": "1612",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1612",
        "ok": "1612",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-cb74a": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_131236",
path: "Animal-api sync for customer NL_131236",
pathFormatted: "req_animal-api-sync-cb74a",
stats: {
    "name": "Animal-api sync for customer NL_131236",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2423",
        "ok": "2423",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2423",
        "ok": "2423",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2423",
        "ok": "2423",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2423",
        "ok": "2423",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2423",
        "ok": "2423",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2423",
        "ok": "2423",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2423",
        "ok": "2423",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-4c33d": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_131701",
path: "Animal-api sync for customer NL_131701",
pathFormatted: "req_animal-api-sync-4c33d",
stats: {
    "name": "Animal-api sync for customer NL_131701",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2067",
        "ok": "2067",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2067",
        "ok": "2067",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2067",
        "ok": "2067",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2067",
        "ok": "2067",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2067",
        "ok": "2067",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2067",
        "ok": "2067",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2067",
        "ok": "2067",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-98e1b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_129146",
path: "Animal-api sync for customer NL_129146",
pathFormatted: "req_animal-api-sync-98e1b",
stats: {
    "name": "Animal-api sync for customer NL_129146",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4536",
        "ok": "4536",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4536",
        "ok": "4536",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4536",
        "ok": "4536",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4536",
        "ok": "4536",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4536",
        "ok": "4536",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4536",
        "ok": "4536",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4536",
        "ok": "4536",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e8633": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117217",
path: "Animal-api sync for customer NL_117217",
pathFormatted: "req_animal-api-sync-e8633",
stats: {
    "name": "Animal-api sync for customer NL_117217",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2154",
        "ok": "2154",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2154",
        "ok": "2154",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2154",
        "ok": "2154",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2154",
        "ok": "2154",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2154",
        "ok": "2154",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2154",
        "ok": "2154",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2154",
        "ok": "2154",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-748ee": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_118193",
path: "Animal-api sync for customer NL_118193",
pathFormatted: "req_animal-api-sync-748ee",
stats: {
    "name": "Animal-api sync for customer NL_118193",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2198",
        "ok": "2198",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2198",
        "ok": "2198",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2198",
        "ok": "2198",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2198",
        "ok": "2198",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2198",
        "ok": "2198",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2198",
        "ok": "2198",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2198",
        "ok": "2198",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-ac942": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_162293",
path: "Animal-api sync for customer BE_162293",
pathFormatted: "req_animal-api-sync-ac942",
stats: {
    "name": "Animal-api sync for customer BE_162293",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3916",
        "ok": "3916",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3916",
        "ok": "3916",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3916",
        "ok": "3916",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3916",
        "ok": "3916",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3916",
        "ok": "3916",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3916",
        "ok": "3916",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3916",
        "ok": "3916",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-18df0": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_132381",
path: "Animal-api sync for customer NL_132381",
pathFormatted: "req_animal-api-sync-18df0",
stats: {
    "name": "Animal-api sync for customer NL_132381",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2138",
        "ok": "2138",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2138",
        "ok": "2138",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2138",
        "ok": "2138",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2138",
        "ok": "2138",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2138",
        "ok": "2138",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2138",
        "ok": "2138",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2138",
        "ok": "2138",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-7e345": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111388",
path: "Animal-api sync for customer NL_111388",
pathFormatted: "req_animal-api-sync-7e345",
stats: {
    "name": "Animal-api sync for customer NL_111388",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3359",
        "ok": "3359",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3359",
        "ok": "3359",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3359",
        "ok": "3359",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3359",
        "ok": "3359",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3359",
        "ok": "3359",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3359",
        "ok": "3359",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3359",
        "ok": "3359",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d3a0e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_118547",
path: "Animal-api sync for customer NL_118547",
pathFormatted: "req_animal-api-sync-d3a0e",
stats: {
    "name": "Animal-api sync for customer NL_118547",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2122",
        "ok": "2122",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2122",
        "ok": "2122",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2122",
        "ok": "2122",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2122",
        "ok": "2122",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2122",
        "ok": "2122",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2122",
        "ok": "2122",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2122",
        "ok": "2122",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-3631b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105143",
path: "Animal-api sync for customer NL_105143",
pathFormatted: "req_animal-api-sync-3631b",
stats: {
    "name": "Animal-api sync for customer NL_105143",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3847",
        "ok": "3847",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3847",
        "ok": "3847",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3847",
        "ok": "3847",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3847",
        "ok": "3847",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3847",
        "ok": "3847",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3847",
        "ok": "3847",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3847",
        "ok": "3847",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-f99c5": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_131859",
path: "Animal-api sync for customer NL_131859",
pathFormatted: "req_animal-api-sync-f99c5",
stats: {
    "name": "Animal-api sync for customer NL_131859",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1485",
        "ok": "1485",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1485",
        "ok": "1485",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1485",
        "ok": "1485",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1485",
        "ok": "1485",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1485",
        "ok": "1485",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1485",
        "ok": "1485",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1485",
        "ok": "1485",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-ba235": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_109254",
path: "Animal-api sync for customer NL_109254",
pathFormatted: "req_animal-api-sync-ba235",
stats: {
    "name": "Animal-api sync for customer NL_109254",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2278",
        "ok": "2278",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2278",
        "ok": "2278",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2278",
        "ok": "2278",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2278",
        "ok": "2278",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2278",
        "ok": "2278",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2278",
        "ok": "2278",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2278",
        "ok": "2278",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e87b4": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_118457",
path: "Animal-api sync for customer NL_118457",
pathFormatted: "req_animal-api-sync-e87b4",
stats: {
    "name": "Animal-api sync for customer NL_118457",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3241",
        "ok": "3241",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3241",
        "ok": "3241",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3241",
        "ok": "3241",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3241",
        "ok": "3241",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3241",
        "ok": "3241",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3241",
        "ok": "3241",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3241",
        "ok": "3241",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-84066": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_136422",
path: "Animal-api sync for customer NL_136422",
pathFormatted: "req_animal-api-sync-84066",
stats: {
    "name": "Animal-api sync for customer NL_136422",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2088",
        "ok": "2088",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2088",
        "ok": "2088",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2088",
        "ok": "2088",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2088",
        "ok": "2088",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2088",
        "ok": "2088",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2088",
        "ok": "2088",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2088",
        "ok": "2088",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-f4249": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_138300",
path: "Animal-api sync for customer NL_138300",
pathFormatted: "req_animal-api-sync-f4249",
stats: {
    "name": "Animal-api sync for customer NL_138300",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3022",
        "ok": "3022",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3022",
        "ok": "3022",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3022",
        "ok": "3022",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3022",
        "ok": "3022",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3022",
        "ok": "3022",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3022",
        "ok": "3022",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3022",
        "ok": "3022",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-32bc3": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_103585",
path: "Animal-api sync for customer NL_103585",
pathFormatted: "req_animal-api-sync-32bc3",
stats: {
    "name": "Animal-api sync for customer NL_103585",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2405",
        "ok": "2405",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2405",
        "ok": "2405",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2405",
        "ok": "2405",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2405",
        "ok": "2405",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2405",
        "ok": "2405",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2405",
        "ok": "2405",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2405",
        "ok": "2405",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-787cb": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_143564",
path: "Animal-api sync for customer NL_143564",
pathFormatted: "req_animal-api-sync-787cb",
stats: {
    "name": "Animal-api sync for customer NL_143564",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1731",
        "ok": "1731",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1731",
        "ok": "1731",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1731",
        "ok": "1731",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1731",
        "ok": "1731",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1731",
        "ok": "1731",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1731",
        "ok": "1731",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1731",
        "ok": "1731",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-0f436": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_107376",
path: "Animal-api sync for customer NL_107376",
pathFormatted: "req_animal-api-sync-0f436",
stats: {
    "name": "Animal-api sync for customer NL_107376",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3089",
        "ok": "3089",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3089",
        "ok": "3089",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3089",
        "ok": "3089",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3089",
        "ok": "3089",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3089",
        "ok": "3089",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3089",
        "ok": "3089",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3089",
        "ok": "3089",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-20cd3": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110661",
path: "Animal-api sync for customer NL_110661",
pathFormatted: "req_animal-api-sync-20cd3",
stats: {
    "name": "Animal-api sync for customer NL_110661",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3092",
        "ok": "3092",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3092",
        "ok": "3092",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3092",
        "ok": "3092",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3092",
        "ok": "3092",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3092",
        "ok": "3092",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3092",
        "ok": "3092",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3092",
        "ok": "3092",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-eb26b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_118799",
path: "Animal-api sync for customer NL_118799",
pathFormatted: "req_animal-api-sync-eb26b",
stats: {
    "name": "Animal-api sync for customer NL_118799",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2606",
        "ok": "2606",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2606",
        "ok": "2606",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2606",
        "ok": "2606",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2606",
        "ok": "2606",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2606",
        "ok": "2606",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2606",
        "ok": "2606",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2606",
        "ok": "2606",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c1388": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_122597",
path: "Animal-api sync for customer NL_122597",
pathFormatted: "req_animal-api-sync-c1388",
stats: {
    "name": "Animal-api sync for customer NL_122597",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4318",
        "ok": "4318",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4318",
        "ok": "4318",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4318",
        "ok": "4318",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4318",
        "ok": "4318",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4318",
        "ok": "4318",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4318",
        "ok": "4318",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4318",
        "ok": "4318",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-28731": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_136181",
path: "Animal-api sync for customer NL_136181",
pathFormatted: "req_animal-api-sync-28731",
stats: {
    "name": "Animal-api sync for customer NL_136181",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2493",
        "ok": "2493",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2493",
        "ok": "2493",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2493",
        "ok": "2493",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2493",
        "ok": "2493",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2493",
        "ok": "2493",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2493",
        "ok": "2493",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2493",
        "ok": "2493",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-da9fa": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_212118",
path: "Animal-api sync for customer NL_212118",
pathFormatted: "req_animal-api-sync-da9fa",
stats: {
    "name": "Animal-api sync for customer NL_212118",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2147",
        "ok": "2147",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2147",
        "ok": "2147",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2147",
        "ok": "2147",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2147",
        "ok": "2147",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2147",
        "ok": "2147",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2147",
        "ok": "2147",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2147",
        "ok": "2147",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-2b764": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111875",
path: "Animal-api sync for customer NL_111875",
pathFormatted: "req_animal-api-sync-2b764",
stats: {
    "name": "Animal-api sync for customer NL_111875",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2731",
        "ok": "2731",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2731",
        "ok": "2731",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2731",
        "ok": "2731",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2731",
        "ok": "2731",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2731",
        "ok": "2731",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2731",
        "ok": "2731",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2731",
        "ok": "2731",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-56095": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_125359",
path: "Animal-api sync for customer NL_125359",
pathFormatted: "req_animal-api-sync-56095",
stats: {
    "name": "Animal-api sync for customer NL_125359",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2194",
        "ok": "2194",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2194",
        "ok": "2194",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2194",
        "ok": "2194",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2194",
        "ok": "2194",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2194",
        "ok": "2194",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2194",
        "ok": "2194",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2194",
        "ok": "2194",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-7c52f": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_112020",
path: "Animal-api sync for customer NL_112020",
pathFormatted: "req_animal-api-sync-7c52f",
stats: {
    "name": "Animal-api sync for customer NL_112020",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3755",
        "ok": "3755",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3755",
        "ok": "3755",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3755",
        "ok": "3755",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3755",
        "ok": "3755",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3755",
        "ok": "3755",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3755",
        "ok": "3755",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3755",
        "ok": "3755",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-1586b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_113408",
path: "Animal-api sync for customer NL_113408",
pathFormatted: "req_animal-api-sync-1586b",
stats: {
    "name": "Animal-api sync for customer NL_113408",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5872",
        "ok": "5872",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5872",
        "ok": "5872",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5872",
        "ok": "5872",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5872",
        "ok": "5872",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5872",
        "ok": "5872",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5872",
        "ok": "5872",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5872",
        "ok": "5872",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-bca62": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_112986",
path: "Animal-api sync for customer NL_112986",
pathFormatted: "req_animal-api-sync-bca62",
stats: {
    "name": "Animal-api sync for customer NL_112986",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4610",
        "ok": "4610",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4610",
        "ok": "4610",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4610",
        "ok": "4610",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4610",
        "ok": "4610",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4610",
        "ok": "4610",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4610",
        "ok": "4610",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4610",
        "ok": "4610",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d8a50": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_109324",
path: "Animal-api sync for customer NL_109324",
pathFormatted: "req_animal-api-sync-d8a50",
stats: {
    "name": "Animal-api sync for customer NL_109324",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4578",
        "ok": "4578",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4578",
        "ok": "4578",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4578",
        "ok": "4578",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4578",
        "ok": "4578",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4578",
        "ok": "4578",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4578",
        "ok": "4578",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4578",
        "ok": "4578",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d4b4d": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_123556",
path: "Animal-api sync for customer NL_123556",
pathFormatted: "req_animal-api-sync-d4b4d",
stats: {
    "name": "Animal-api sync for customer NL_123556",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3270",
        "ok": "3270",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3270",
        "ok": "3270",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3270",
        "ok": "3270",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3270",
        "ok": "3270",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3270",
        "ok": "3270",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3270",
        "ok": "3270",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3270",
        "ok": "3270",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-bc7d0": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_151016",
path: "Animal-api sync for customer BE_151016",
pathFormatted: "req_animal-api-sync-bc7d0",
stats: {
    "name": "Animal-api sync for customer BE_151016",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3415",
        "ok": "3415",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3415",
        "ok": "3415",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3415",
        "ok": "3415",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3415",
        "ok": "3415",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3415",
        "ok": "3415",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3415",
        "ok": "3415",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3415",
        "ok": "3415",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a0b51": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_118379",
path: "Animal-api sync for customer NL_118379",
pathFormatted: "req_animal-api-sync-a0b51",
stats: {
    "name": "Animal-api sync for customer NL_118379",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3574",
        "ok": "3574",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3574",
        "ok": "3574",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3574",
        "ok": "3574",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3574",
        "ok": "3574",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3574",
        "ok": "3574",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3574",
        "ok": "3574",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3574",
        "ok": "3574",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b6f25": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_119444",
path: "Animal-api sync for customer NL_119444",
pathFormatted: "req_animal-api-sync-b6f25",
stats: {
    "name": "Animal-api sync for customer NL_119444",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3134",
        "ok": "3134",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3134",
        "ok": "3134",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3134",
        "ok": "3134",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3134",
        "ok": "3134",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3134",
        "ok": "3134",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3134",
        "ok": "3134",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3134",
        "ok": "3134",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b9666": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_207943",
path: "Animal-api sync for customer BE_207943",
pathFormatted: "req_animal-api-sync-b9666",
stats: {
    "name": "Animal-api sync for customer BE_207943",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3843",
        "ok": "3843",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3843",
        "ok": "3843",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3843",
        "ok": "3843",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3843",
        "ok": "3843",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3843",
        "ok": "3843",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3843",
        "ok": "3843",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3843",
        "ok": "3843",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-662d1": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_211608",
path: "Animal-api sync for customer BE_211608",
pathFormatted: "req_animal-api-sync-662d1",
stats: {
    "name": "Animal-api sync for customer BE_211608",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3103",
        "ok": "3103",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3103",
        "ok": "3103",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3103",
        "ok": "3103",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3103",
        "ok": "3103",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3103",
        "ok": "3103",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3103",
        "ok": "3103",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3103",
        "ok": "3103",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-58eb2": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_125881",
path: "Animal-api sync for customer NL_125881",
pathFormatted: "req_animal-api-sync-58eb2",
stats: {
    "name": "Animal-api sync for customer NL_125881",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2407",
        "ok": "2407",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2407",
        "ok": "2407",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2407",
        "ok": "2407",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2407",
        "ok": "2407",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2407",
        "ok": "2407",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2407",
        "ok": "2407",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2407",
        "ok": "2407",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-cd728": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117817",
path: "Animal-api sync for customer NL_117817",
pathFormatted: "req_animal-api-sync-cd728",
stats: {
    "name": "Animal-api sync for customer NL_117817",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2375",
        "ok": "2375",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2375",
        "ok": "2375",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2375",
        "ok": "2375",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2375",
        "ok": "2375",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2375",
        "ok": "2375",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2375",
        "ok": "2375",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2375",
        "ok": "2375",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d0381": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_126231",
path: "Animal-api sync for customer NL_126231",
pathFormatted: "req_animal-api-sync-d0381",
stats: {
    "name": "Animal-api sync for customer NL_126231",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3520",
        "ok": "3520",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3520",
        "ok": "3520",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3520",
        "ok": "3520",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3520",
        "ok": "3520",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3520",
        "ok": "3520",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3520",
        "ok": "3520",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3520",
        "ok": "3520",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-1164c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104369",
path: "Animal-api sync for customer NL_104369",
pathFormatted: "req_animal-api-sync-1164c",
stats: {
    "name": "Animal-api sync for customer NL_104369",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2834",
        "ok": "2834",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2834",
        "ok": "2834",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2834",
        "ok": "2834",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2834",
        "ok": "2834",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2834",
        "ok": "2834",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2834",
        "ok": "2834",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2834",
        "ok": "2834",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c2774": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_130077",
path: "Animal-api sync for customer NL_130077",
pathFormatted: "req_animal-api-sync-c2774",
stats: {
    "name": "Animal-api sync for customer NL_130077",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3642",
        "ok": "3642",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3642",
        "ok": "3642",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3642",
        "ok": "3642",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3642",
        "ok": "3642",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3642",
        "ok": "3642",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3642",
        "ok": "3642",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3642",
        "ok": "3642",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-da2a1": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105775",
path: "Animal-api sync for customer NL_105775",
pathFormatted: "req_animal-api-sync-da2a1",
stats: {
    "name": "Animal-api sync for customer NL_105775",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3240",
        "ok": "3240",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3240",
        "ok": "3240",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3240",
        "ok": "3240",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3240",
        "ok": "3240",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3240",
        "ok": "3240",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3240",
        "ok": "3240",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3240",
        "ok": "3240",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-12657": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_212029",
path: "Animal-api sync for customer BE_212029",
pathFormatted: "req_animal-api-sync-12657",
stats: {
    "name": "Animal-api sync for customer BE_212029",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4097",
        "ok": "4097",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4097",
        "ok": "4097",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4097",
        "ok": "4097",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4097",
        "ok": "4097",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4097",
        "ok": "4097",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4097",
        "ok": "4097",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4097",
        "ok": "4097",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a231f": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_119073",
path: "Animal-api sync for customer NL_119073",
pathFormatted: "req_animal-api-sync-a231f",
stats: {
    "name": "Animal-api sync for customer NL_119073",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2926",
        "ok": "2926",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2926",
        "ok": "2926",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2926",
        "ok": "2926",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2926",
        "ok": "2926",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2926",
        "ok": "2926",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2926",
        "ok": "2926",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2926",
        "ok": "2926",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a5dcb": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_113563",
path: "Animal-api sync for customer NL_113563",
pathFormatted: "req_animal-api-sync-a5dcb",
stats: {
    "name": "Animal-api sync for customer NL_113563",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "9404",
        "ok": "9404",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "9404",
        "ok": "9404",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "9404",
        "ok": "9404",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "9404",
        "ok": "9404",
        "ko": "-"
    },
    "percentiles2": {
        "total": "9404",
        "ok": "9404",
        "ko": "-"
    },
    "percentiles3": {
        "total": "9404",
        "ok": "9404",
        "ko": "-"
    },
    "percentiles4": {
        "total": "9404",
        "ok": "9404",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-0d921": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_124334",
path: "Animal-api sync for customer NL_124334",
pathFormatted: "req_animal-api-sync-0d921",
stats: {
    "name": "Animal-api sync for customer NL_124334",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2822",
        "ok": "2822",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2822",
        "ok": "2822",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2822",
        "ok": "2822",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2822",
        "ok": "2822",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2822",
        "ok": "2822",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2822",
        "ok": "2822",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2822",
        "ok": "2822",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a5308": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_129853",
path: "Animal-api sync for customer NL_129853",
pathFormatted: "req_animal-api-sync-a5308",
stats: {
    "name": "Animal-api sync for customer NL_129853",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2563",
        "ok": "2563",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2563",
        "ok": "2563",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2563",
        "ok": "2563",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2563",
        "ok": "2563",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2563",
        "ok": "2563",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2563",
        "ok": "2563",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2563",
        "ok": "2563",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a8587": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_112236",
path: "Animal-api sync for customer NL_112236",
pathFormatted: "req_animal-api-sync-a8587",
stats: {
    "name": "Animal-api sync for customer NL_112236",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2388",
        "ok": "2388",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2388",
        "ok": "2388",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2388",
        "ok": "2388",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2388",
        "ok": "2388",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2388",
        "ok": "2388",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2388",
        "ok": "2388",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2388",
        "ok": "2388",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c1a9b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105397",
path: "Animal-api sync for customer NL_105397",
pathFormatted: "req_animal-api-sync-c1a9b",
stats: {
    "name": "Animal-api sync for customer NL_105397",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4335",
        "ok": "4335",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4335",
        "ok": "4335",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4335",
        "ok": "4335",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4335",
        "ok": "4335",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4335",
        "ok": "4335",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4335",
        "ok": "4335",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4335",
        "ok": "4335",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-f47d9": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_162268",
path: "Animal-api sync for customer NL_162268",
pathFormatted: "req_animal-api-sync-f47d9",
stats: {
    "name": "Animal-api sync for customer NL_162268",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5398",
        "ok": "5398",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5398",
        "ok": "5398",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5398",
        "ok": "5398",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5398",
        "ok": "5398",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5398",
        "ok": "5398",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5398",
        "ok": "5398",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5398",
        "ok": "5398",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-7141e": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_195066",
path: "Animal-api sync for customer BE_195066",
pathFormatted: "req_animal-api-sync-7141e",
stats: {
    "name": "Animal-api sync for customer BE_195066",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "6100",
        "ok": "6100",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "6100",
        "ok": "6100",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "6100",
        "ok": "6100",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "6100",
        "ok": "6100",
        "ko": "-"
    },
    "percentiles2": {
        "total": "6100",
        "ok": "6100",
        "ko": "-"
    },
    "percentiles3": {
        "total": "6100",
        "ok": "6100",
        "ko": "-"
    },
    "percentiles4": {
        "total": "6100",
        "ok": "6100",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-9fb14": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_115364",
path: "Animal-api sync for customer NL_115364",
pathFormatted: "req_animal-api-sync-9fb14",
stats: {
    "name": "Animal-api sync for customer NL_115364",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4153",
        "ok": "4153",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4153",
        "ok": "4153",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4153",
        "ok": "4153",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4153",
        "ok": "4153",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4153",
        "ok": "4153",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4153",
        "ok": "4153",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4153",
        "ok": "4153",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-29ae3": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_107774",
path: "Animal-api sync for customer NL_107774",
pathFormatted: "req_animal-api-sync-29ae3",
stats: {
    "name": "Animal-api sync for customer NL_107774",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "6513",
        "ok": "6513",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "6513",
        "ok": "6513",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "6513",
        "ok": "6513",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "6513",
        "ok": "6513",
        "ko": "-"
    },
    "percentiles2": {
        "total": "6513",
        "ok": "6513",
        "ko": "-"
    },
    "percentiles3": {
        "total": "6513",
        "ok": "6513",
        "ko": "-"
    },
    "percentiles4": {
        "total": "6513",
        "ok": "6513",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e24f9": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_125084",
path: "Animal-api sync for customer NL_125084",
pathFormatted: "req_animal-api-sync-e24f9",
stats: {
    "name": "Animal-api sync for customer NL_125084",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3939",
        "ok": "3939",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3939",
        "ok": "3939",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3939",
        "ok": "3939",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3939",
        "ok": "3939",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3939",
        "ok": "3939",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3939",
        "ok": "3939",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3939",
        "ok": "3939",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-6bcf1": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_130692",
path: "Animal-api sync for customer NL_130692",
pathFormatted: "req_animal-api-sync-6bcf1",
stats: {
    "name": "Animal-api sync for customer NL_130692",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4538",
        "ok": "4538",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4538",
        "ok": "4538",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4538",
        "ok": "4538",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4538",
        "ok": "4538",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4538",
        "ok": "4538",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4538",
        "ok": "4538",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4538",
        "ok": "4538",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-42cac": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117138",
path: "Animal-api sync for customer NL_117138",
pathFormatted: "req_animal-api-sync-42cac",
stats: {
    "name": "Animal-api sync for customer NL_117138",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4539",
        "ok": "4539",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4539",
        "ok": "4539",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4539",
        "ok": "4539",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4539",
        "ok": "4539",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4539",
        "ok": "4539",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4539",
        "ok": "4539",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4539",
        "ok": "4539",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-7ddaa": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_127532",
path: "Animal-api sync for customer NL_127532",
pathFormatted: "req_animal-api-sync-7ddaa",
stats: {
    "name": "Animal-api sync for customer NL_127532",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3660",
        "ok": "3660",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3660",
        "ok": "3660",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3660",
        "ok": "3660",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3660",
        "ok": "3660",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3660",
        "ok": "3660",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3660",
        "ok": "3660",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3660",
        "ok": "3660",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-35067": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_145405",
path: "Animal-api sync for customer BE_145405",
pathFormatted: "req_animal-api-sync-35067",
stats: {
    "name": "Animal-api sync for customer BE_145405",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5402",
        "ok": "5402",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5402",
        "ok": "5402",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5402",
        "ok": "5402",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5402",
        "ok": "5402",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5402",
        "ok": "5402",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5402",
        "ok": "5402",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5402",
        "ok": "5402",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-2e40c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_131445",
path: "Animal-api sync for customer NL_131445",
pathFormatted: "req_animal-api-sync-2e40c",
stats: {
    "name": "Animal-api sync for customer NL_131445",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3132",
        "ok": "3132",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3132",
        "ok": "3132",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3132",
        "ok": "3132",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3132",
        "ok": "3132",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3132",
        "ok": "3132",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3132",
        "ok": "3132",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3132",
        "ok": "3132",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-3eb24": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_103773",
path: "Animal-api sync for customer NL_103773",
pathFormatted: "req_animal-api-sync-3eb24",
stats: {
    "name": "Animal-api sync for customer NL_103773",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4620",
        "ok": "4620",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4620",
        "ok": "4620",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4620",
        "ok": "4620",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4620",
        "ok": "4620",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4620",
        "ok": "4620",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4620",
        "ok": "4620",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4620",
        "ok": "4620",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-db524": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104670",
path: "Animal-api sync for customer NL_104670",
pathFormatted: "req_animal-api-sync-db524",
stats: {
    "name": "Animal-api sync for customer NL_104670",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3980",
        "ok": "3980",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3980",
        "ok": "3980",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3980",
        "ok": "3980",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3980",
        "ok": "3980",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3980",
        "ok": "3980",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3980",
        "ok": "3980",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3980",
        "ok": "3980",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-8824d": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_129197",
path: "Animal-api sync for customer NL_129197",
pathFormatted: "req_animal-api-sync-8824d",
stats: {
    "name": "Animal-api sync for customer NL_129197",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5632",
        "ok": "5632",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5632",
        "ok": "5632",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5632",
        "ok": "5632",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5632",
        "ok": "5632",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5632",
        "ok": "5632",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5632",
        "ok": "5632",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5632",
        "ok": "5632",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d2007": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_214023",
path: "Animal-api sync for customer BE_214023",
pathFormatted: "req_animal-api-sync-d2007",
stats: {
    "name": "Animal-api sync for customer BE_214023",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3588",
        "ok": "3588",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3588",
        "ok": "3588",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3588",
        "ok": "3588",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3588",
        "ok": "3588",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3588",
        "ok": "3588",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3588",
        "ok": "3588",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3588",
        "ok": "3588",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e79fc": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_114849",
path: "Animal-api sync for customer NL_114849",
pathFormatted: "req_animal-api-sync-e79fc",
stats: {
    "name": "Animal-api sync for customer NL_114849",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3864",
        "ok": "3864",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3864",
        "ok": "3864",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3864",
        "ok": "3864",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3864",
        "ok": "3864",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3864",
        "ok": "3864",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3864",
        "ok": "3864",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3864",
        "ok": "3864",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-6b259": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_118526",
path: "Animal-api sync for customer NL_118526",
pathFormatted: "req_animal-api-sync-6b259",
stats: {
    "name": "Animal-api sync for customer NL_118526",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2411",
        "ok": "2411",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2411",
        "ok": "2411",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2411",
        "ok": "2411",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2411",
        "ok": "2411",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2411",
        "ok": "2411",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2411",
        "ok": "2411",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2411",
        "ok": "2411",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-1f585": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_123243",
path: "Animal-api sync for customer NL_123243",
pathFormatted: "req_animal-api-sync-1f585",
stats: {
    "name": "Animal-api sync for customer NL_123243",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3268",
        "ok": "3268",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3268",
        "ok": "3268",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3268",
        "ok": "3268",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3268",
        "ok": "3268",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3268",
        "ok": "3268",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3268",
        "ok": "3268",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3268",
        "ok": "3268",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-360a2": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_120728",
path: "Animal-api sync for customer NL_120728",
pathFormatted: "req_animal-api-sync-360a2",
stats: {
    "name": "Animal-api sync for customer NL_120728",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3375",
        "ok": "3375",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3375",
        "ok": "3375",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3375",
        "ok": "3375",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3375",
        "ok": "3375",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3375",
        "ok": "3375",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3375",
        "ok": "3375",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3375",
        "ok": "3375",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-964ea": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_131647",
path: "Animal-api sync for customer NL_131647",
pathFormatted: "req_animal-api-sync-964ea",
stats: {
    "name": "Animal-api sync for customer NL_131647",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4316",
        "ok": "4316",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4316",
        "ok": "4316",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4316",
        "ok": "4316",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4316",
        "ok": "4316",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4316",
        "ok": "4316",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4316",
        "ok": "4316",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4316",
        "ok": "4316",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-90a7a": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_158870",
path: "Animal-api sync for customer NL_158870",
pathFormatted: "req_animal-api-sync-90a7a",
stats: {
    "name": "Animal-api sync for customer NL_158870",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "8325",
        "ok": "8325",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "8325",
        "ok": "8325",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "8325",
        "ok": "8325",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "8325",
        "ok": "8325",
        "ko": "-"
    },
    "percentiles2": {
        "total": "8325",
        "ok": "8325",
        "ko": "-"
    },
    "percentiles3": {
        "total": "8325",
        "ok": "8325",
        "ko": "-"
    },
    "percentiles4": {
        "total": "8325",
        "ok": "8325",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-ee74d": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_149842",
path: "Animal-api sync for customer BE_149842",
pathFormatted: "req_animal-api-sync-ee74d",
stats: {
    "name": "Animal-api sync for customer BE_149842",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3458",
        "ok": "3458",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3458",
        "ok": "3458",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3458",
        "ok": "3458",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3458",
        "ok": "3458",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3458",
        "ok": "3458",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3458",
        "ok": "3458",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3458",
        "ok": "3458",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-edbd5": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_156716",
path: "Animal-api sync for customer NL_156716",
pathFormatted: "req_animal-api-sync-edbd5",
stats: {
    "name": "Animal-api sync for customer NL_156716",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2963",
        "ok": "2963",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2963",
        "ok": "2963",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2963",
        "ok": "2963",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2963",
        "ok": "2963",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2963",
        "ok": "2963",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2963",
        "ok": "2963",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2963",
        "ok": "2963",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b6490": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_128966",
path: "Animal-api sync for customer NL_128966",
pathFormatted: "req_animal-api-sync-b6490",
stats: {
    "name": "Animal-api sync for customer NL_128966",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3414",
        "ok": "3414",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3414",
        "ok": "3414",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3414",
        "ok": "3414",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3414",
        "ok": "3414",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3414",
        "ok": "3414",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3414",
        "ok": "3414",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3414",
        "ok": "3414",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-5eb0d": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_129758",
path: "Animal-api sync for customer NL_129758",
pathFormatted: "req_animal-api-sync-5eb0d",
stats: {
    "name": "Animal-api sync for customer NL_129758",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2304",
        "ok": "2304",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2304",
        "ok": "2304",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2304",
        "ok": "2304",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2304",
        "ok": "2304",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2304",
        "ok": "2304",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2304",
        "ok": "2304",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2304",
        "ok": "2304",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-7665a": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_107608",
path: "Animal-api sync for customer NL_107608",
pathFormatted: "req_animal-api-sync-7665a",
stats: {
    "name": "Animal-api sync for customer NL_107608",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4290",
        "ok": "4290",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4290",
        "ok": "4290",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4290",
        "ok": "4290",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4290",
        "ok": "4290",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4290",
        "ok": "4290",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4290",
        "ok": "4290",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4290",
        "ok": "4290",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-352f1": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_120975",
path: "Animal-api sync for customer NL_120975",
pathFormatted: "req_animal-api-sync-352f1",
stats: {
    "name": "Animal-api sync for customer NL_120975",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3162",
        "ok": "3162",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3162",
        "ok": "3162",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3162",
        "ok": "3162",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3162",
        "ok": "3162",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3162",
        "ok": "3162",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3162",
        "ok": "3162",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3162",
        "ok": "3162",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a1b1c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105501",
path: "Animal-api sync for customer NL_105501",
pathFormatted: "req_animal-api-sync-a1b1c",
stats: {
    "name": "Animal-api sync for customer NL_105501",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3535",
        "ok": "3535",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3535",
        "ok": "3535",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3535",
        "ok": "3535",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3535",
        "ok": "3535",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3535",
        "ok": "3535",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3535",
        "ok": "3535",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3535",
        "ok": "3535",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-558ec": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_115428",
path: "Animal-api sync for customer NL_115428",
pathFormatted: "req_animal-api-sync-558ec",
stats: {
    "name": "Animal-api sync for customer NL_115428",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1961",
        "ok": "1961",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1961",
        "ok": "1961",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1961",
        "ok": "1961",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1961",
        "ok": "1961",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1961",
        "ok": "1961",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1961",
        "ok": "1961",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1961",
        "ok": "1961",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e201e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110096",
path: "Animal-api sync for customer NL_110096",
pathFormatted: "req_animal-api-sync-e201e",
stats: {
    "name": "Animal-api sync for customer NL_110096",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2822",
        "ok": "2822",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2822",
        "ok": "2822",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2822",
        "ok": "2822",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2822",
        "ok": "2822",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2822",
        "ok": "2822",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2822",
        "ok": "2822",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2822",
        "ok": "2822",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-66a67": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_115297",
path: "Animal-api sync for customer NL_115297",
pathFormatted: "req_animal-api-sync-66a67",
stats: {
    "name": "Animal-api sync for customer NL_115297",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3712",
        "ok": "3712",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3712",
        "ok": "3712",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3712",
        "ok": "3712",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3712",
        "ok": "3712",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3712",
        "ok": "3712",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3712",
        "ok": "3712",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3712",
        "ok": "3712",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e614b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_114481",
path: "Animal-api sync for customer NL_114481",
pathFormatted: "req_animal-api-sync-e614b",
stats: {
    "name": "Animal-api sync for customer NL_114481",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2924",
        "ok": "2924",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2924",
        "ok": "2924",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2924",
        "ok": "2924",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2924",
        "ok": "2924",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2924",
        "ok": "2924",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2924",
        "ok": "2924",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2924",
        "ok": "2924",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a326b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_109292",
path: "Animal-api sync for customer NL_109292",
pathFormatted: "req_animal-api-sync-a326b",
stats: {
    "name": "Animal-api sync for customer NL_109292",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4538",
        "ok": "4538",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4538",
        "ok": "4538",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4538",
        "ok": "4538",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4538",
        "ok": "4538",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4538",
        "ok": "4538",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4538",
        "ok": "4538",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4538",
        "ok": "4538",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-ec201": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105082",
path: "Animal-api sync for customer NL_105082",
pathFormatted: "req_animal-api-sync-ec201",
stats: {
    "name": "Animal-api sync for customer NL_105082",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3466",
        "ok": "3466",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3466",
        "ok": "3466",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3466",
        "ok": "3466",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3466",
        "ok": "3466",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3466",
        "ok": "3466",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3466",
        "ok": "3466",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3466",
        "ok": "3466",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c9dab": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_122464",
path: "Animal-api sync for customer NL_122464",
pathFormatted: "req_animal-api-sync-c9dab",
stats: {
    "name": "Animal-api sync for customer NL_122464",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4665",
        "ok": "4665",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4665",
        "ok": "4665",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4665",
        "ok": "4665",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4665",
        "ok": "4665",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4665",
        "ok": "4665",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4665",
        "ok": "4665",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4665",
        "ok": "4665",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-2092d": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_150518",
path: "Animal-api sync for customer BE_150518",
pathFormatted: "req_animal-api-sync-2092d",
stats: {
    "name": "Animal-api sync for customer BE_150518",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3191",
        "ok": "3191",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3191",
        "ok": "3191",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3191",
        "ok": "3191",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3191",
        "ok": "3191",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3191",
        "ok": "3191",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3191",
        "ok": "3191",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3191",
        "ok": "3191",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-cdf93": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_118194",
path: "Animal-api sync for customer NL_118194",
pathFormatted: "req_animal-api-sync-cdf93",
stats: {
    "name": "Animal-api sync for customer NL_118194",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3401",
        "ok": "3401",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3401",
        "ok": "3401",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3401",
        "ok": "3401",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3401",
        "ok": "3401",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3401",
        "ok": "3401",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3401",
        "ok": "3401",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3401",
        "ok": "3401",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-ac6d4": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_121952",
path: "Animal-api sync for customer NL_121952",
pathFormatted: "req_animal-api-sync-ac6d4",
stats: {
    "name": "Animal-api sync for customer NL_121952",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4095",
        "ok": "4095",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4095",
        "ok": "4095",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4095",
        "ok": "4095",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4095",
        "ok": "4095",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4095",
        "ok": "4095",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4095",
        "ok": "4095",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4095",
        "ok": "4095",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-0ec02": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_114079",
path: "Animal-api sync for customer NL_114079",
pathFormatted: "req_animal-api-sync-0ec02",
stats: {
    "name": "Animal-api sync for customer NL_114079",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2263",
        "ok": "2263",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2263",
        "ok": "2263",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2263",
        "ok": "2263",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2263",
        "ok": "2263",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2263",
        "ok": "2263",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2263",
        "ok": "2263",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2263",
        "ok": "2263",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-2901a": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_136544",
path: "Animal-api sync for customer NL_136544",
pathFormatted: "req_animal-api-sync-2901a",
stats: {
    "name": "Animal-api sync for customer NL_136544",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2442",
        "ok": "2442",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2442",
        "ok": "2442",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2442",
        "ok": "2442",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2442",
        "ok": "2442",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2442",
        "ok": "2442",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2442",
        "ok": "2442",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2442",
        "ok": "2442",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-1a27a": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_120522",
path: "Animal-api sync for customer NL_120522",
pathFormatted: "req_animal-api-sync-1a27a",
stats: {
    "name": "Animal-api sync for customer NL_120522",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3406",
        "ok": "3406",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3406",
        "ok": "3406",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3406",
        "ok": "3406",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3406",
        "ok": "3406",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3406",
        "ok": "3406",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3406",
        "ok": "3406",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3406",
        "ok": "3406",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-317c3": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_132801",
path: "Animal-api sync for customer NL_132801",
pathFormatted: "req_animal-api-sync-317c3",
stats: {
    "name": "Animal-api sync for customer NL_132801",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2619",
        "ok": "2619",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2619",
        "ok": "2619",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2619",
        "ok": "2619",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2619",
        "ok": "2619",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2619",
        "ok": "2619",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2619",
        "ok": "2619",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2619",
        "ok": "2619",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-83baa": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_114916",
path: "Animal-api sync for customer NL_114916",
pathFormatted: "req_animal-api-sync-83baa",
stats: {
    "name": "Animal-api sync for customer NL_114916",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3808",
        "ok": "3808",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3808",
        "ok": "3808",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3808",
        "ok": "3808",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3808",
        "ok": "3808",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3808",
        "ok": "3808",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3808",
        "ok": "3808",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3808",
        "ok": "3808",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-3eaed": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_108223",
path: "Animal-api sync for customer NL_108223",
pathFormatted: "req_animal-api-sync-3eaed",
stats: {
    "name": "Animal-api sync for customer NL_108223",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3598",
        "ok": "3598",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3598",
        "ok": "3598",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3598",
        "ok": "3598",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3598",
        "ok": "3598",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3598",
        "ok": "3598",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3598",
        "ok": "3598",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3598",
        "ok": "3598",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-6ad60": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_124372",
path: "Animal-api sync for customer NL_124372",
pathFormatted: "req_animal-api-sync-6ad60",
stats: {
    "name": "Animal-api sync for customer NL_124372",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2948",
        "ok": "2948",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2948",
        "ok": "2948",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2948",
        "ok": "2948",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2948",
        "ok": "2948",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2948",
        "ok": "2948",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2948",
        "ok": "2948",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2948",
        "ok": "2948",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-967fa": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_113311",
path: "Animal-api sync for customer NL_113311",
pathFormatted: "req_animal-api-sync-967fa",
stats: {
    "name": "Animal-api sync for customer NL_113311",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2573",
        "ok": "2573",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2573",
        "ok": "2573",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2573",
        "ok": "2573",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2573",
        "ok": "2573",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2573",
        "ok": "2573",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2573",
        "ok": "2573",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2573",
        "ok": "2573",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-3736b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_114775",
path: "Animal-api sync for customer NL_114775",
pathFormatted: "req_animal-api-sync-3736b",
stats: {
    "name": "Animal-api sync for customer NL_114775",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "13743",
        "ok": "13743",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "13743",
        "ok": "13743",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "13743",
        "ok": "13743",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "13743",
        "ok": "13743",
        "ko": "-"
    },
    "percentiles2": {
        "total": "13743",
        "ok": "13743",
        "ko": "-"
    },
    "percentiles3": {
        "total": "13743",
        "ok": "13743",
        "ko": "-"
    },
    "percentiles4": {
        "total": "13743",
        "ok": "13743",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-2e3f2": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_124578",
path: "Animal-api sync for customer NL_124578",
pathFormatted: "req_animal-api-sync-2e3f2",
stats: {
    "name": "Animal-api sync for customer NL_124578",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2682",
        "ok": "2682",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2682",
        "ok": "2682",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2682",
        "ok": "2682",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2682",
        "ok": "2682",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2682",
        "ok": "2682",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2682",
        "ok": "2682",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2682",
        "ok": "2682",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-f6bd1": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_118770",
path: "Animal-api sync for customer NL_118770",
pathFormatted: "req_animal-api-sync-f6bd1",
stats: {
    "name": "Animal-api sync for customer NL_118770",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3466",
        "ok": "3466",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3466",
        "ok": "3466",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3466",
        "ok": "3466",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3466",
        "ok": "3466",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3466",
        "ok": "3466",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3466",
        "ok": "3466",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3466",
        "ok": "3466",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-cfdfb": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111312",
path: "Animal-api sync for customer NL_111312",
pathFormatted: "req_animal-api-sync-cfdfb",
stats: {
    "name": "Animal-api sync for customer NL_111312",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3213",
        "ok": "3213",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3213",
        "ok": "3213",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3213",
        "ok": "3213",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3213",
        "ok": "3213",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3213",
        "ok": "3213",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3213",
        "ok": "3213",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3213",
        "ok": "3213",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c9778": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_119273",
path: "Animal-api sync for customer NL_119273",
pathFormatted: "req_animal-api-sync-c9778",
stats: {
    "name": "Animal-api sync for customer NL_119273",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2830",
        "ok": "2830",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2830",
        "ok": "2830",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2830",
        "ok": "2830",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2830",
        "ok": "2830",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2830",
        "ok": "2830",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2830",
        "ok": "2830",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2830",
        "ok": "2830",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-39d31": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_118474",
path: "Animal-api sync for customer NL_118474",
pathFormatted: "req_animal-api-sync-39d31",
stats: {
    "name": "Animal-api sync for customer NL_118474",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3600",
        "ok": "3600",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3600",
        "ok": "3600",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3600",
        "ok": "3600",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3600",
        "ok": "3600",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3600",
        "ok": "3600",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3600",
        "ok": "3600",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3600",
        "ok": "3600",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-53fa3": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_108741",
path: "Animal-api sync for customer NL_108741",
pathFormatted: "req_animal-api-sync-53fa3",
stats: {
    "name": "Animal-api sync for customer NL_108741",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3858",
        "ok": "3858",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3858",
        "ok": "3858",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3858",
        "ok": "3858",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3858",
        "ok": "3858",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3858",
        "ok": "3858",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3858",
        "ok": "3858",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3858",
        "ok": "3858",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-99401": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111178",
path: "Animal-api sync for customer NL_111178",
pathFormatted: "req_animal-api-sync-99401",
stats: {
    "name": "Animal-api sync for customer NL_111178",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4192",
        "ok": "4192",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4192",
        "ok": "4192",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4192",
        "ok": "4192",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4192",
        "ok": "4192",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4192",
        "ok": "4192",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4192",
        "ok": "4192",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4192",
        "ok": "4192",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-09c2e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_135864",
path: "Animal-api sync for customer NL_135864",
pathFormatted: "req_animal-api-sync-09c2e",
stats: {
    "name": "Animal-api sync for customer NL_135864",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3686",
        "ok": "3686",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3686",
        "ok": "3686",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3686",
        "ok": "3686",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3686",
        "ok": "3686",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3686",
        "ok": "3686",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3686",
        "ok": "3686",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3686",
        "ok": "3686",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-236d8": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_124049",
path: "Animal-api sync for customer NL_124049",
pathFormatted: "req_animal-api-sync-236d8",
stats: {
    "name": "Animal-api sync for customer NL_124049",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3302",
        "ok": "3302",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3302",
        "ok": "3302",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3302",
        "ok": "3302",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3302",
        "ok": "3302",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3302",
        "ok": "3302",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3302",
        "ok": "3302",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3302",
        "ok": "3302",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-882c2": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_128097",
path: "Animal-api sync for customer NL_128097",
pathFormatted: "req_animal-api-sync-882c2",
stats: {
    "name": "Animal-api sync for customer NL_128097",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2513",
        "ok": "2513",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2513",
        "ok": "2513",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2513",
        "ok": "2513",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2513",
        "ok": "2513",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2513",
        "ok": "2513",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2513",
        "ok": "2513",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2513",
        "ok": "2513",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e7614": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_159766",
path: "Animal-api sync for customer BE_159766",
pathFormatted: "req_animal-api-sync-e7614",
stats: {
    "name": "Animal-api sync for customer BE_159766",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "8220",
        "ok": "8220",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "8220",
        "ok": "8220",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "8220",
        "ok": "8220",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "8220",
        "ok": "8220",
        "ko": "-"
    },
    "percentiles2": {
        "total": "8220",
        "ok": "8220",
        "ko": "-"
    },
    "percentiles3": {
        "total": "8220",
        "ok": "8220",
        "ko": "-"
    },
    "percentiles4": {
        "total": "8220",
        "ok": "8220",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-52a69": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_212921",
path: "Animal-api sync for customer BE_212921",
pathFormatted: "req_animal-api-sync-52a69",
stats: {
    "name": "Animal-api sync for customer BE_212921",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2989",
        "ok": "2989",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2989",
        "ok": "2989",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2989",
        "ok": "2989",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2989",
        "ok": "2989",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2989",
        "ok": "2989",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2989",
        "ok": "2989",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2989",
        "ok": "2989",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-53e2f": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_127894",
path: "Animal-api sync for customer NL_127894",
pathFormatted: "req_animal-api-sync-53e2f",
stats: {
    "name": "Animal-api sync for customer NL_127894",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2869",
        "ok": "2869",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2869",
        "ok": "2869",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2869",
        "ok": "2869",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2869",
        "ok": "2869",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2869",
        "ok": "2869",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2869",
        "ok": "2869",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2869",
        "ok": "2869",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b4a99": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104247",
path: "Animal-api sync for customer NL_104247",
pathFormatted: "req_animal-api-sync-b4a99",
stats: {
    "name": "Animal-api sync for customer NL_104247",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4074",
        "ok": "4074",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4074",
        "ok": "4074",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4074",
        "ok": "4074",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4074",
        "ok": "4074",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4074",
        "ok": "4074",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4074",
        "ok": "4074",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4074",
        "ok": "4074",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c4c5c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_122906",
path: "Animal-api sync for customer NL_122906",
pathFormatted: "req_animal-api-sync-c4c5c",
stats: {
    "name": "Animal-api sync for customer NL_122906",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3880",
        "ok": "3880",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3880",
        "ok": "3880",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3880",
        "ok": "3880",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3880",
        "ok": "3880",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3880",
        "ok": "3880",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3880",
        "ok": "3880",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3880",
        "ok": "3880",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e42dd": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_108296",
path: "Animal-api sync for customer NL_108296",
pathFormatted: "req_animal-api-sync-e42dd",
stats: {
    "name": "Animal-api sync for customer NL_108296",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1692",
        "ok": "1692",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1692",
        "ok": "1692",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1692",
        "ok": "1692",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1692",
        "ok": "1692",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1692",
        "ok": "1692",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1692",
        "ok": "1692",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1692",
        "ok": "1692",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b8f6b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_120259",
path: "Animal-api sync for customer NL_120259",
pathFormatted: "req_animal-api-sync-b8f6b",
stats: {
    "name": "Animal-api sync for customer NL_120259",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4057",
        "ok": "4057",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4057",
        "ok": "4057",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4057",
        "ok": "4057",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4057",
        "ok": "4057",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4057",
        "ok": "4057",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4057",
        "ok": "4057",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4057",
        "ok": "4057",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-10dc5": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_113215",
path: "Animal-api sync for customer NL_113215",
pathFormatted: "req_animal-api-sync-10dc5",
stats: {
    "name": "Animal-api sync for customer NL_113215",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3650",
        "ok": "3650",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3650",
        "ok": "3650",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3650",
        "ok": "3650",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3650",
        "ok": "3650",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3650",
        "ok": "3650",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3650",
        "ok": "3650",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3650",
        "ok": "3650",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-ee676": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_119868",
path: "Animal-api sync for customer NL_119868",
pathFormatted: "req_animal-api-sync-ee676",
stats: {
    "name": "Animal-api sync for customer NL_119868",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "8182",
        "ok": "8182",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "8182",
        "ok": "8182",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "8182",
        "ok": "8182",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "8182",
        "ok": "8182",
        "ko": "-"
    },
    "percentiles2": {
        "total": "8182",
        "ok": "8182",
        "ko": "-"
    },
    "percentiles3": {
        "total": "8182",
        "ok": "8182",
        "ko": "-"
    },
    "percentiles4": {
        "total": "8182",
        "ok": "8182",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-12c8b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_107507",
path: "Animal-api sync for customer NL_107507",
pathFormatted: "req_animal-api-sync-12c8b",
stats: {
    "name": "Animal-api sync for customer NL_107507",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3665",
        "ok": "3665",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3665",
        "ok": "3665",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3665",
        "ok": "3665",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3665",
        "ok": "3665",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3665",
        "ok": "3665",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3665",
        "ok": "3665",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3665",
        "ok": "3665",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-70c3b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_108064",
path: "Animal-api sync for customer NL_108064",
pathFormatted: "req_animal-api-sync-70c3b",
stats: {
    "name": "Animal-api sync for customer NL_108064",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5274",
        "ok": "5274",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5274",
        "ok": "5274",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5274",
        "ok": "5274",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5274",
        "ok": "5274",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5274",
        "ok": "5274",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5274",
        "ok": "5274",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5274",
        "ok": "5274",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d5bee": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_114423",
path: "Animal-api sync for customer NL_114423",
pathFormatted: "req_animal-api-sync-d5bee",
stats: {
    "name": "Animal-api sync for customer NL_114423",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2244",
        "ok": "2244",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2244",
        "ok": "2244",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2244",
        "ok": "2244",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2244",
        "ok": "2244",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2244",
        "ok": "2244",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2244",
        "ok": "2244",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2244",
        "ok": "2244",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-f94cb": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_107241",
path: "Animal-api sync for customer NL_107241",
pathFormatted: "req_animal-api-sync-f94cb",
stats: {
    "name": "Animal-api sync for customer NL_107241",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4513",
        "ok": "4513",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4513",
        "ok": "4513",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4513",
        "ok": "4513",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4513",
        "ok": "4513",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4513",
        "ok": "4513",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4513",
        "ok": "4513",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4513",
        "ok": "4513",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-4b1f5": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_129590",
path: "Animal-api sync for customer NL_129590",
pathFormatted: "req_animal-api-sync-4b1f5",
stats: {
    "name": "Animal-api sync for customer NL_129590",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5846",
        "ok": "5846",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5846",
        "ok": "5846",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5846",
        "ok": "5846",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5846",
        "ok": "5846",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5846",
        "ok": "5846",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5846",
        "ok": "5846",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5846",
        "ok": "5846",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-25bc2": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111740",
path: "Animal-api sync for customer NL_111740",
pathFormatted: "req_animal-api-sync-25bc2",
stats: {
    "name": "Animal-api sync for customer NL_111740",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1998",
        "ok": "1998",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1998",
        "ok": "1998",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1998",
        "ok": "1998",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1998",
        "ok": "1998",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1998",
        "ok": "1998",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1998",
        "ok": "1998",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1998",
        "ok": "1998",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-2efa4": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117125",
path: "Animal-api sync for customer NL_117125",
pathFormatted: "req_animal-api-sync-2efa4",
stats: {
    "name": "Animal-api sync for customer NL_117125",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "6974",
        "ok": "6974",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "6974",
        "ok": "6974",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "6974",
        "ok": "6974",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "6974",
        "ok": "6974",
        "ko": "-"
    },
    "percentiles2": {
        "total": "6974",
        "ok": "6974",
        "ko": "-"
    },
    "percentiles3": {
        "total": "6974",
        "ok": "6974",
        "ko": "-"
    },
    "percentiles4": {
        "total": "6974",
        "ok": "6974",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c4fc3": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_216527",
path: "Animal-api sync for customer NL_216527",
pathFormatted: "req_animal-api-sync-c4fc3",
stats: {
    "name": "Animal-api sync for customer NL_216527",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3219",
        "ok": "3219",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3219",
        "ok": "3219",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3219",
        "ok": "3219",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3219",
        "ok": "3219",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3219",
        "ok": "3219",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3219",
        "ok": "3219",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3219",
        "ok": "3219",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-41435": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_119693",
path: "Animal-api sync for customer NL_119693",
pathFormatted: "req_animal-api-sync-41435",
stats: {
    "name": "Animal-api sync for customer NL_119693",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "6091",
        "ok": "6091",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "6091",
        "ok": "6091",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "6091",
        "ok": "6091",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "6091",
        "ok": "6091",
        "ko": "-"
    },
    "percentiles2": {
        "total": "6091",
        "ok": "6091",
        "ko": "-"
    },
    "percentiles3": {
        "total": "6091",
        "ok": "6091",
        "ko": "-"
    },
    "percentiles4": {
        "total": "6091",
        "ok": "6091",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e3e3d": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_126029",
path: "Animal-api sync for customer NL_126029",
pathFormatted: "req_animal-api-sync-e3e3d",
stats: {
    "name": "Animal-api sync for customer NL_126029",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3960",
        "ok": "3960",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3960",
        "ok": "3960",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3960",
        "ok": "3960",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3960",
        "ok": "3960",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3960",
        "ok": "3960",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3960",
        "ok": "3960",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3960",
        "ok": "3960",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d8bef": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_114961",
path: "Animal-api sync for customer NL_114961",
pathFormatted: "req_animal-api-sync-d8bef",
stats: {
    "name": "Animal-api sync for customer NL_114961",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4887",
        "ok": "4887",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4887",
        "ok": "4887",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4887",
        "ok": "4887",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4887",
        "ok": "4887",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4887",
        "ok": "4887",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4887",
        "ok": "4887",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4887",
        "ok": "4887",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-40763": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_127944",
path: "Animal-api sync for customer NL_127944",
pathFormatted: "req_animal-api-sync-40763",
stats: {
    "name": "Animal-api sync for customer NL_127944",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2996",
        "ok": "2996",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2996",
        "ok": "2996",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2996",
        "ok": "2996",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2996",
        "ok": "2996",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2996",
        "ok": "2996",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2996",
        "ok": "2996",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2996",
        "ok": "2996",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-9f4a5": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_108715",
path: "Animal-api sync for customer NL_108715",
pathFormatted: "req_animal-api-sync-9f4a5",
stats: {
    "name": "Animal-api sync for customer NL_108715",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4804",
        "ok": "4804",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4804",
        "ok": "4804",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4804",
        "ok": "4804",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4804",
        "ok": "4804",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4804",
        "ok": "4804",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4804",
        "ok": "4804",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4804",
        "ok": "4804",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-0355c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_133793",
path: "Animal-api sync for customer NL_133793",
pathFormatted: "req_animal-api-sync-0355c",
stats: {
    "name": "Animal-api sync for customer NL_133793",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2394",
        "ok": "2394",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2394",
        "ok": "2394",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2394",
        "ok": "2394",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2394",
        "ok": "2394",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2394",
        "ok": "2394",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2394",
        "ok": "2394",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2394",
        "ok": "2394",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-6bc35": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_127470",
path: "Animal-api sync for customer NL_127470",
pathFormatted: "req_animal-api-sync-6bc35",
stats: {
    "name": "Animal-api sync for customer NL_127470",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5502",
        "ok": "5502",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5502",
        "ok": "5502",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5502",
        "ok": "5502",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5502",
        "ok": "5502",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5502",
        "ok": "5502",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5502",
        "ok": "5502",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5502",
        "ok": "5502",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-7eb9d": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104934",
path: "Animal-api sync for customer NL_104934",
pathFormatted: "req_animal-api-sync-7eb9d",
stats: {
    "name": "Animal-api sync for customer NL_104934",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "6808",
        "ok": "6808",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "6808",
        "ok": "6808",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "6808",
        "ok": "6808",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "6808",
        "ok": "6808",
        "ko": "-"
    },
    "percentiles2": {
        "total": "6808",
        "ok": "6808",
        "ko": "-"
    },
    "percentiles3": {
        "total": "6808",
        "ok": "6808",
        "ko": "-"
    },
    "percentiles4": {
        "total": "6808",
        "ok": "6808",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-3a032": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_122396",
path: "Animal-api sync for customer NL_122396",
pathFormatted: "req_animal-api-sync-3a032",
stats: {
    "name": "Animal-api sync for customer NL_122396",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5164",
        "ok": "5164",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5164",
        "ok": "5164",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5164",
        "ok": "5164",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5164",
        "ok": "5164",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5164",
        "ok": "5164",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5164",
        "ok": "5164",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5164",
        "ok": "5164",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-1cf92": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105173",
path: "Animal-api sync for customer NL_105173",
pathFormatted: "req_animal-api-sync-1cf92",
stats: {
    "name": "Animal-api sync for customer NL_105173",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3666",
        "ok": "3666",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3666",
        "ok": "3666",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3666",
        "ok": "3666",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3666",
        "ok": "3666",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3666",
        "ok": "3666",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3666",
        "ok": "3666",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3666",
        "ok": "3666",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b22b6": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_131590",
path: "Animal-api sync for customer NL_131590",
pathFormatted: "req_animal-api-sync-b22b6",
stats: {
    "name": "Animal-api sync for customer NL_131590",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5256",
        "ok": "5256",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5256",
        "ok": "5256",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5256",
        "ok": "5256",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5256",
        "ok": "5256",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5256",
        "ok": "5256",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5256",
        "ok": "5256",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5256",
        "ok": "5256",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-1080b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110702",
path: "Animal-api sync for customer NL_110702",
pathFormatted: "req_animal-api-sync-1080b",
stats: {
    "name": "Animal-api sync for customer NL_110702",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2923",
        "ok": "2923",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2923",
        "ok": "2923",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2923",
        "ok": "2923",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2923",
        "ok": "2923",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2923",
        "ok": "2923",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2923",
        "ok": "2923",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2923",
        "ok": "2923",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-8580c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_119732",
path: "Animal-api sync for customer NL_119732",
pathFormatted: "req_animal-api-sync-8580c",
stats: {
    "name": "Animal-api sync for customer NL_119732",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4234",
        "ok": "4234",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4234",
        "ok": "4234",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4234",
        "ok": "4234",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4234",
        "ok": "4234",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4234",
        "ok": "4234",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4234",
        "ok": "4234",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4234",
        "ok": "4234",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-8dfa9": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_116373",
path: "Animal-api sync for customer NL_116373",
pathFormatted: "req_animal-api-sync-8dfa9",
stats: {
    "name": "Animal-api sync for customer NL_116373",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3604",
        "ok": "3604",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3604",
        "ok": "3604",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3604",
        "ok": "3604",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3604",
        "ok": "3604",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3604",
        "ok": "3604",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3604",
        "ok": "3604",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3604",
        "ok": "3604",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c298e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_124623",
path: "Animal-api sync for customer NL_124623",
pathFormatted: "req_animal-api-sync-c298e",
stats: {
    "name": "Animal-api sync for customer NL_124623",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3406",
        "ok": "3406",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3406",
        "ok": "3406",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3406",
        "ok": "3406",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3406",
        "ok": "3406",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3406",
        "ok": "3406",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3406",
        "ok": "3406",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3406",
        "ok": "3406",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-18c50": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_124684",
path: "Animal-api sync for customer NL_124684",
pathFormatted: "req_animal-api-sync-18c50",
stats: {
    "name": "Animal-api sync for customer NL_124684",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3789",
        "ok": "3789",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3789",
        "ok": "3789",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3789",
        "ok": "3789",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3789",
        "ok": "3789",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3789",
        "ok": "3789",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3789",
        "ok": "3789",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3789",
        "ok": "3789",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-11d7b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111681",
path: "Animal-api sync for customer NL_111681",
pathFormatted: "req_animal-api-sync-11d7b",
stats: {
    "name": "Animal-api sync for customer NL_111681",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3903",
        "ok": "3903",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3903",
        "ok": "3903",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3903",
        "ok": "3903",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3903",
        "ok": "3903",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3903",
        "ok": "3903",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3903",
        "ok": "3903",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3903",
        "ok": "3903",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-f0b42": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_107121",
path: "Animal-api sync for customer NL_107121",
pathFormatted: "req_animal-api-sync-f0b42",
stats: {
    "name": "Animal-api sync for customer NL_107121",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "8656",
        "ok": "8656",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "8656",
        "ok": "8656",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "8656",
        "ok": "8656",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "8656",
        "ok": "8656",
        "ko": "-"
    },
    "percentiles2": {
        "total": "8656",
        "ok": "8656",
        "ko": "-"
    },
    "percentiles3": {
        "total": "8656",
        "ok": "8656",
        "ko": "-"
    },
    "percentiles4": {
        "total": "8656",
        "ok": "8656",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-de92c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_109851",
path: "Animal-api sync for customer NL_109851",
pathFormatted: "req_animal-api-sync-de92c",
stats: {
    "name": "Animal-api sync for customer NL_109851",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5634",
        "ok": "5634",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5634",
        "ok": "5634",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5634",
        "ok": "5634",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5634",
        "ok": "5634",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5634",
        "ok": "5634",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5634",
        "ok": "5634",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5634",
        "ok": "5634",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-31610": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_109147",
path: "Animal-api sync for customer NL_109147",
pathFormatted: "req_animal-api-sync-31610",
stats: {
    "name": "Animal-api sync for customer NL_109147",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2789",
        "ok": "2789",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2789",
        "ok": "2789",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2789",
        "ok": "2789",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2789",
        "ok": "2789",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2789",
        "ok": "2789",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2789",
        "ok": "2789",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2789",
        "ok": "2789",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a6474": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_217194",
path: "Animal-api sync for customer NL_217194",
pathFormatted: "req_animal-api-sync-a6474",
stats: {
    "name": "Animal-api sync for customer NL_217194",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4521",
        "ok": "4521",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4521",
        "ok": "4521",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4521",
        "ok": "4521",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4521",
        "ok": "4521",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4521",
        "ok": "4521",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4521",
        "ok": "4521",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4521",
        "ok": "4521",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-735e5": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_162624",
path: "Animal-api sync for customer BE_162624",
pathFormatted: "req_animal-api-sync-735e5",
stats: {
    "name": "Animal-api sync for customer BE_162624",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "6640",
        "ok": "6640",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "6640",
        "ok": "6640",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "6640",
        "ok": "6640",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "6640",
        "ok": "6640",
        "ko": "-"
    },
    "percentiles2": {
        "total": "6640",
        "ok": "6640",
        "ko": "-"
    },
    "percentiles3": {
        "total": "6640",
        "ok": "6640",
        "ko": "-"
    },
    "percentiles4": {
        "total": "6640",
        "ok": "6640",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-da049": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111349",
path: "Animal-api sync for customer NL_111349",
pathFormatted: "req_animal-api-sync-da049",
stats: {
    "name": "Animal-api sync for customer NL_111349",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3542",
        "ok": "3542",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3542",
        "ok": "3542",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3542",
        "ok": "3542",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3542",
        "ok": "3542",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3542",
        "ok": "3542",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3542",
        "ok": "3542",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3542",
        "ok": "3542",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-0945e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_120830",
path: "Animal-api sync for customer NL_120830",
pathFormatted: "req_animal-api-sync-0945e",
stats: {
    "name": "Animal-api sync for customer NL_120830",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "6827",
        "ok": "6827",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "6827",
        "ok": "6827",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "6827",
        "ok": "6827",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "6827",
        "ok": "6827",
        "ko": "-"
    },
    "percentiles2": {
        "total": "6827",
        "ok": "6827",
        "ko": "-"
    },
    "percentiles3": {
        "total": "6827",
        "ok": "6827",
        "ko": "-"
    },
    "percentiles4": {
        "total": "6827",
        "ok": "6827",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d1db6": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_154426",
path: "Animal-api sync for customer BE_154426",
pathFormatted: "req_animal-api-sync-d1db6",
stats: {
    "name": "Animal-api sync for customer BE_154426",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "8000",
        "ok": "8000",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "8000",
        "ok": "8000",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "8000",
        "ok": "8000",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "8000",
        "ok": "8000",
        "ko": "-"
    },
    "percentiles2": {
        "total": "8000",
        "ok": "8000",
        "ko": "-"
    },
    "percentiles3": {
        "total": "8000",
        "ok": "8000",
        "ko": "-"
    },
    "percentiles4": {
        "total": "8000",
        "ok": "8000",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-3b46a": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_160734",
path: "Animal-api sync for customer NL_160734",
pathFormatted: "req_animal-api-sync-3b46a",
stats: {
    "name": "Animal-api sync for customer NL_160734",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4196",
        "ok": "4196",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4196",
        "ok": "4196",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4196",
        "ok": "4196",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4196",
        "ok": "4196",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4196",
        "ok": "4196",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4196",
        "ok": "4196",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4196",
        "ok": "4196",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-4b701": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_122804",
path: "Animal-api sync for customer NL_122804",
pathFormatted: "req_animal-api-sync-4b701",
stats: {
    "name": "Animal-api sync for customer NL_122804",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3270",
        "ok": "3270",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3270",
        "ok": "3270",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3270",
        "ok": "3270",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3270",
        "ok": "3270",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3270",
        "ok": "3270",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3270",
        "ok": "3270",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3270",
        "ok": "3270",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-3f46c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110510",
path: "Animal-api sync for customer NL_110510",
pathFormatted: "req_animal-api-sync-3f46c",
stats: {
    "name": "Animal-api sync for customer NL_110510",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4319",
        "ok": "4319",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4319",
        "ok": "4319",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4319",
        "ok": "4319",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4319",
        "ok": "4319",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4319",
        "ok": "4319",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4319",
        "ok": "4319",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4319",
        "ok": "4319",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e1650": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_189253",
path: "Animal-api sync for customer BE_189253",
pathFormatted: "req_animal-api-sync-e1650",
stats: {
    "name": "Animal-api sync for customer BE_189253",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5394",
        "ok": "5394",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5394",
        "ok": "5394",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5394",
        "ok": "5394",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5394",
        "ok": "5394",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5394",
        "ok": "5394",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5394",
        "ok": "5394",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5394",
        "ok": "5394",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-5a990": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_196831",
path: "Animal-api sync for customer BE_196831",
pathFormatted: "req_animal-api-sync-5a990",
stats: {
    "name": "Animal-api sync for customer BE_196831",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5163",
        "ok": "5163",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5163",
        "ok": "5163",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5163",
        "ok": "5163",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5163",
        "ok": "5163",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5163",
        "ok": "5163",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5163",
        "ok": "5163",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5163",
        "ok": "5163",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-8b170": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_128376",
path: "Animal-api sync for customer NL_128376",
pathFormatted: "req_animal-api-sync-8b170",
stats: {
    "name": "Animal-api sync for customer NL_128376",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4844",
        "ok": "4844",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4844",
        "ok": "4844",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4844",
        "ok": "4844",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4844",
        "ok": "4844",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4844",
        "ok": "4844",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4844",
        "ok": "4844",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4844",
        "ok": "4844",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-9e2d7": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_127839",
path: "Animal-api sync for customer NL_127839",
pathFormatted: "req_animal-api-sync-9e2d7",
stats: {
    "name": "Animal-api sync for customer NL_127839",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4512",
        "ok": "4512",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4512",
        "ok": "4512",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4512",
        "ok": "4512",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4512",
        "ok": "4512",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4512",
        "ok": "4512",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4512",
        "ok": "4512",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4512",
        "ok": "4512",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-cb3f0": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_109354",
path: "Animal-api sync for customer NL_109354",
pathFormatted: "req_animal-api-sync-cb3f0",
stats: {
    "name": "Animal-api sync for customer NL_109354",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3436",
        "ok": "3436",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3436",
        "ok": "3436",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3436",
        "ok": "3436",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3436",
        "ok": "3436",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3436",
        "ok": "3436",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3436",
        "ok": "3436",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3436",
        "ok": "3436",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-77e7f": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_116276",
path: "Animal-api sync for customer NL_116276",
pathFormatted: "req_animal-api-sync-77e7f",
stats: {
    "name": "Animal-api sync for customer NL_116276",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "6534",
        "ok": "6534",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "6534",
        "ok": "6534",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "6534",
        "ok": "6534",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "6534",
        "ok": "6534",
        "ko": "-"
    },
    "percentiles2": {
        "total": "6534",
        "ok": "6534",
        "ko": "-"
    },
    "percentiles3": {
        "total": "6534",
        "ok": "6534",
        "ko": "-"
    },
    "percentiles4": {
        "total": "6534",
        "ok": "6534",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e91bf": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111232",
path: "Animal-api sync for customer NL_111232",
pathFormatted: "req_animal-api-sync-e91bf",
stats: {
    "name": "Animal-api sync for customer NL_111232",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5193",
        "ok": "5193",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5193",
        "ok": "5193",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5193",
        "ok": "5193",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5193",
        "ok": "5193",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5193",
        "ok": "5193",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5193",
        "ok": "5193",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5193",
        "ok": "5193",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-df608": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_106231",
path: "Animal-api sync for customer NL_106231",
pathFormatted: "req_animal-api-sync-df608",
stats: {
    "name": "Animal-api sync for customer NL_106231",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4688",
        "ok": "4688",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4688",
        "ok": "4688",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4688",
        "ok": "4688",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4688",
        "ok": "4688",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4688",
        "ok": "4688",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4688",
        "ok": "4688",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4688",
        "ok": "4688",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-96235": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_119086",
path: "Animal-api sync for customer NL_119086",
pathFormatted: "req_animal-api-sync-96235",
stats: {
    "name": "Animal-api sync for customer NL_119086",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "11471",
        "ok": "11471",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "11471",
        "ok": "11471",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "11471",
        "ok": "11471",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "11471",
        "ok": "11471",
        "ko": "-"
    },
    "percentiles2": {
        "total": "11471",
        "ok": "11471",
        "ko": "-"
    },
    "percentiles3": {
        "total": "11471",
        "ok": "11471",
        "ko": "-"
    },
    "percentiles4": {
        "total": "11471",
        "ok": "11471",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b66f8": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110851",
path: "Animal-api sync for customer NL_110851",
pathFormatted: "req_animal-api-sync-b66f8",
stats: {
    "name": "Animal-api sync for customer NL_110851",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5346",
        "ok": "5346",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5346",
        "ok": "5346",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5346",
        "ok": "5346",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5346",
        "ok": "5346",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5346",
        "ok": "5346",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5346",
        "ok": "5346",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5346",
        "ok": "5346",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-fc706": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_108712",
path: "Animal-api sync for customer NL_108712",
pathFormatted: "req_animal-api-sync-fc706",
stats: {
    "name": "Animal-api sync for customer NL_108712",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5389",
        "ok": "5389",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5389",
        "ok": "5389",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5389",
        "ok": "5389",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5389",
        "ok": "5389",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5389",
        "ok": "5389",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5389",
        "ok": "5389",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5389",
        "ok": "5389",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-77463": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_142162",
path: "Animal-api sync for customer NL_142162",
pathFormatted: "req_animal-api-sync-77463",
stats: {
    "name": "Animal-api sync for customer NL_142162",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "6289",
        "ok": "6289",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "6289",
        "ok": "6289",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "6289",
        "ok": "6289",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "6289",
        "ok": "6289",
        "ko": "-"
    },
    "percentiles2": {
        "total": "6289",
        "ok": "6289",
        "ko": "-"
    },
    "percentiles3": {
        "total": "6289",
        "ok": "6289",
        "ko": "-"
    },
    "percentiles4": {
        "total": "6289",
        "ok": "6289",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-3e0f1": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_119459",
path: "Animal-api sync for customer NL_119459",
pathFormatted: "req_animal-api-sync-3e0f1",
stats: {
    "name": "Animal-api sync for customer NL_119459",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4833",
        "ok": "4833",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4833",
        "ok": "4833",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4833",
        "ok": "4833",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4833",
        "ok": "4833",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4833",
        "ok": "4833",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4833",
        "ok": "4833",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4833",
        "ok": "4833",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-f9107": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_121807",
path: "Animal-api sync for customer NL_121807",
pathFormatted: "req_animal-api-sync-f9107",
stats: {
    "name": "Animal-api sync for customer NL_121807",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4195",
        "ok": "4195",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4195",
        "ok": "4195",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4195",
        "ok": "4195",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4195",
        "ok": "4195",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4195",
        "ok": "4195",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4195",
        "ok": "4195",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4195",
        "ok": "4195",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-82f9e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110685",
path: "Animal-api sync for customer NL_110685",
pathFormatted: "req_animal-api-sync-82f9e",
stats: {
    "name": "Animal-api sync for customer NL_110685",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "6797",
        "ok": "6797",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "6797",
        "ok": "6797",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "6797",
        "ok": "6797",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "6797",
        "ok": "6797",
        "ko": "-"
    },
    "percentiles2": {
        "total": "6797",
        "ok": "6797",
        "ko": "-"
    },
    "percentiles3": {
        "total": "6797",
        "ok": "6797",
        "ko": "-"
    },
    "percentiles4": {
        "total": "6797",
        "ok": "6797",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-39816": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_145536",
path: "Animal-api sync for customer BE_145536",
pathFormatted: "req_animal-api-sync-39816",
stats: {
    "name": "Animal-api sync for customer BE_145536",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4846",
        "ok": "4846",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4846",
        "ok": "4846",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4846",
        "ok": "4846",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4846",
        "ok": "4846",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4846",
        "ok": "4846",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4846",
        "ok": "4846",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4846",
        "ok": "4846",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-66198": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_107001",
path: "Animal-api sync for customer NL_107001",
pathFormatted: "req_animal-api-sync-66198",
stats: {
    "name": "Animal-api sync for customer NL_107001",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5844",
        "ok": "5844",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5844",
        "ok": "5844",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5844",
        "ok": "5844",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5844",
        "ok": "5844",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5844",
        "ok": "5844",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5844",
        "ok": "5844",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5844",
        "ok": "5844",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-299ce": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_119621",
path: "Animal-api sync for customer NL_119621",
pathFormatted: "req_animal-api-sync-299ce",
stats: {
    "name": "Animal-api sync for customer NL_119621",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3901",
        "ok": "3901",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3901",
        "ok": "3901",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3901",
        "ok": "3901",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3901",
        "ok": "3901",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3901",
        "ok": "3901",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3901",
        "ok": "3901",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3901",
        "ok": "3901",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-cb563": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_195750",
path: "Animal-api sync for customer BE_195750",
pathFormatted: "req_animal-api-sync-cb563",
stats: {
    "name": "Animal-api sync for customer BE_195750",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "6039",
        "ok": "6039",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "6039",
        "ok": "6039",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "6039",
        "ok": "6039",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "6039",
        "ok": "6039",
        "ko": "-"
    },
    "percentiles2": {
        "total": "6039",
        "ok": "6039",
        "ko": "-"
    },
    "percentiles3": {
        "total": "6039",
        "ok": "6039",
        "ko": "-"
    },
    "percentiles4": {
        "total": "6039",
        "ok": "6039",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-86128": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110109",
path: "Animal-api sync for customer NL_110109",
pathFormatted: "req_animal-api-sync-86128",
stats: {
    "name": "Animal-api sync for customer NL_110109",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4278",
        "ok": "4278",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4278",
        "ok": "4278",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4278",
        "ok": "4278",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4278",
        "ok": "4278",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4278",
        "ok": "4278",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4278",
        "ok": "4278",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4278",
        "ok": "4278",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-2a7a0": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_119620",
path: "Animal-api sync for customer NL_119620",
pathFormatted: "req_animal-api-sync-2a7a0",
stats: {
    "name": "Animal-api sync for customer NL_119620",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "6168",
        "ok": "6168",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "6168",
        "ok": "6168",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "6168",
        "ok": "6168",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "6168",
        "ok": "6168",
        "ko": "-"
    },
    "percentiles2": {
        "total": "6168",
        "ok": "6168",
        "ko": "-"
    },
    "percentiles3": {
        "total": "6168",
        "ok": "6168",
        "ko": "-"
    },
    "percentiles4": {
        "total": "6168",
        "ok": "6168",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-f1bc5": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_106522",
path: "Animal-api sync for customer NL_106522",
pathFormatted: "req_animal-api-sync-f1bc5",
stats: {
    "name": "Animal-api sync for customer NL_106522",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3716",
        "ok": "3716",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3716",
        "ok": "3716",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3716",
        "ok": "3716",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3716",
        "ok": "3716",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3716",
        "ok": "3716",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3716",
        "ok": "3716",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3716",
        "ok": "3716",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-fa8fb": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110675",
path: "Animal-api sync for customer NL_110675",
pathFormatted: "req_animal-api-sync-fa8fb",
stats: {
    "name": "Animal-api sync for customer NL_110675",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4606",
        "ok": "4606",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4606",
        "ok": "4606",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4606",
        "ok": "4606",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4606",
        "ok": "4606",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4606",
        "ok": "4606",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4606",
        "ok": "4606",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4606",
        "ok": "4606",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-9a989": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105564",
path: "Animal-api sync for customer NL_105564",
pathFormatted: "req_animal-api-sync-9a989",
stats: {
    "name": "Animal-api sync for customer NL_105564",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4110",
        "ok": "4110",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4110",
        "ok": "4110",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4110",
        "ok": "4110",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4110",
        "ok": "4110",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4110",
        "ok": "4110",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4110",
        "ok": "4110",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4110",
        "ok": "4110",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-1d50e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_107593",
path: "Animal-api sync for customer NL_107593",
pathFormatted: "req_animal-api-sync-1d50e",
stats: {
    "name": "Animal-api sync for customer NL_107593",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5315",
        "ok": "5315",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5315",
        "ok": "5315",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5315",
        "ok": "5315",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5315",
        "ok": "5315",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5315",
        "ok": "5315",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5315",
        "ok": "5315",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5315",
        "ok": "5315",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-3fad8": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_119768",
path: "Animal-api sync for customer NL_119768",
pathFormatted: "req_animal-api-sync-3fad8",
stats: {
    "name": "Animal-api sync for customer NL_119768",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2605",
        "ok": "2605",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2605",
        "ok": "2605",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2605",
        "ok": "2605",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2605",
        "ok": "2605",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2605",
        "ok": "2605",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2605",
        "ok": "2605",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2605",
        "ok": "2605",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-1cdda": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_106961",
path: "Animal-api sync for customer NL_106961",
pathFormatted: "req_animal-api-sync-1cdda",
stats: {
    "name": "Animal-api sync for customer NL_106961",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3891",
        "ok": "3891",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3891",
        "ok": "3891",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3891",
        "ok": "3891",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3891",
        "ok": "3891",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3891",
        "ok": "3891",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3891",
        "ok": "3891",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3891",
        "ok": "3891",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-028d0": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_139052",
path: "Animal-api sync for customer NL_139052",
pathFormatted: "req_animal-api-sync-028d0",
stats: {
    "name": "Animal-api sync for customer NL_139052",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1797",
        "ok": "1797",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1797",
        "ok": "1797",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1797",
        "ok": "1797",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1797",
        "ok": "1797",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1797",
        "ok": "1797",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1797",
        "ok": "1797",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1797",
        "ok": "1797",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-3df0e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_136411",
path: "Animal-api sync for customer NL_136411",
pathFormatted: "req_animal-api-sync-3df0e",
stats: {
    "name": "Animal-api sync for customer NL_136411",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5198",
        "ok": "5198",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5198",
        "ok": "5198",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5198",
        "ok": "5198",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5198",
        "ok": "5198",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5198",
        "ok": "5198",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5198",
        "ok": "5198",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5198",
        "ok": "5198",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-34074": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104611",
path: "Animal-api sync for customer NL_104611",
pathFormatted: "req_animal-api-sync-34074",
stats: {
    "name": "Animal-api sync for customer NL_104611",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "7142",
        "ok": "7142",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "7142",
        "ok": "7142",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "7142",
        "ok": "7142",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "7142",
        "ok": "7142",
        "ko": "-"
    },
    "percentiles2": {
        "total": "7142",
        "ok": "7142",
        "ko": "-"
    },
    "percentiles3": {
        "total": "7142",
        "ok": "7142",
        "ko": "-"
    },
    "percentiles4": {
        "total": "7142",
        "ok": "7142",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-4e4dd": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_132571",
path: "Animal-api sync for customer NL_132571",
pathFormatted: "req_animal-api-sync-4e4dd",
stats: {
    "name": "Animal-api sync for customer NL_132571",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2575",
        "ok": "2575",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2575",
        "ok": "2575",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2575",
        "ok": "2575",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2575",
        "ok": "2575",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2575",
        "ok": "2575",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2575",
        "ok": "2575",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2575",
        "ok": "2575",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b76bc": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_121619",
path: "Animal-api sync for customer NL_121619",
pathFormatted: "req_animal-api-sync-b76bc",
stats: {
    "name": "Animal-api sync for customer NL_121619",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5789",
        "ok": "5789",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5789",
        "ok": "5789",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5789",
        "ok": "5789",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5789",
        "ok": "5789",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5789",
        "ok": "5789",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5789",
        "ok": "5789",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5789",
        "ok": "5789",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-67b51": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_208384",
path: "Animal-api sync for customer NL_208384",
pathFormatted: "req_animal-api-sync-67b51",
stats: {
    "name": "Animal-api sync for customer NL_208384",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4005",
        "ok": "4005",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4005",
        "ok": "4005",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4005",
        "ok": "4005",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4005",
        "ok": "4005",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4005",
        "ok": "4005",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4005",
        "ok": "4005",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4005",
        "ok": "4005",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-f5467": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_164463",
path: "Animal-api sync for customer NL_164463",
pathFormatted: "req_animal-api-sync-f5467",
stats: {
    "name": "Animal-api sync for customer NL_164463",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4174",
        "ok": "4174",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4174",
        "ok": "4174",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4174",
        "ok": "4174",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4174",
        "ok": "4174",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4174",
        "ok": "4174",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4174",
        "ok": "4174",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4174",
        "ok": "4174",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a921a": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_158726",
path: "Animal-api sync for customer BE_158726",
pathFormatted: "req_animal-api-sync-a921a",
stats: {
    "name": "Animal-api sync for customer BE_158726",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3662",
        "ok": "3662",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3662",
        "ok": "3662",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3662",
        "ok": "3662",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3662",
        "ok": "3662",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3662",
        "ok": "3662",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3662",
        "ok": "3662",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3662",
        "ok": "3662",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-43d17": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_159367",
path: "Animal-api sync for customer NL_159367",
pathFormatted: "req_animal-api-sync-43d17",
stats: {
    "name": "Animal-api sync for customer NL_159367",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2766",
        "ok": "2766",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2766",
        "ok": "2766",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2766",
        "ok": "2766",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2766",
        "ok": "2766",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2766",
        "ok": "2766",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2766",
        "ok": "2766",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2766",
        "ok": "2766",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-7b4fd": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_207082",
path: "Animal-api sync for customer BE_207082",
pathFormatted: "req_animal-api-sync-7b4fd",
stats: {
    "name": "Animal-api sync for customer BE_207082",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4526",
        "ok": "4526",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4526",
        "ok": "4526",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4526",
        "ok": "4526",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4526",
        "ok": "4526",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4526",
        "ok": "4526",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4526",
        "ok": "4526",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4526",
        "ok": "4526",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-774bc": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105254",
path: "Animal-api sync for customer NL_105254",
pathFormatted: "req_animal-api-sync-774bc",
stats: {
    "name": "Animal-api sync for customer NL_105254",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3805",
        "ok": "3805",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3805",
        "ok": "3805",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3805",
        "ok": "3805",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3805",
        "ok": "3805",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3805",
        "ok": "3805",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3805",
        "ok": "3805",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3805",
        "ok": "3805",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-72b5a": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_129227",
path: "Animal-api sync for customer NL_129227",
pathFormatted: "req_animal-api-sync-72b5a",
stats: {
    "name": "Animal-api sync for customer NL_129227",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3449",
        "ok": "3449",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3449",
        "ok": "3449",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3449",
        "ok": "3449",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3449",
        "ok": "3449",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3449",
        "ok": "3449",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3449",
        "ok": "3449",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3449",
        "ok": "3449",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e7477": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_120288",
path: "Animal-api sync for customer NL_120288",
pathFormatted: "req_animal-api-sync-e7477",
stats: {
    "name": "Animal-api sync for customer NL_120288",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3868",
        "ok": "3868",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3868",
        "ok": "3868",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3868",
        "ok": "3868",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3868",
        "ok": "3868",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3868",
        "ok": "3868",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3868",
        "ok": "3868",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3868",
        "ok": "3868",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b6fe1": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_112227",
path: "Animal-api sync for customer NL_112227",
pathFormatted: "req_animal-api-sync-b6fe1",
stats: {
    "name": "Animal-api sync for customer NL_112227",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3185",
        "ok": "3185",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3185",
        "ok": "3185",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3185",
        "ok": "3185",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3185",
        "ok": "3185",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3185",
        "ok": "3185",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3185",
        "ok": "3185",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3185",
        "ok": "3185",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-6891c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111812",
path: "Animal-api sync for customer NL_111812",
pathFormatted: "req_animal-api-sync-6891c",
stats: {
    "name": "Animal-api sync for customer NL_111812",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3481",
        "ok": "3481",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3481",
        "ok": "3481",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3481",
        "ok": "3481",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3481",
        "ok": "3481",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3481",
        "ok": "3481",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3481",
        "ok": "3481",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3481",
        "ok": "3481",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-5324e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_112685",
path: "Animal-api sync for customer NL_112685",
pathFormatted: "req_animal-api-sync-5324e",
stats: {
    "name": "Animal-api sync for customer NL_112685",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3098",
        "ok": "3098",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3098",
        "ok": "3098",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3098",
        "ok": "3098",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3098",
        "ok": "3098",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3098",
        "ok": "3098",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3098",
        "ok": "3098",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3098",
        "ok": "3098",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-39487": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_153606",
path: "Animal-api sync for customer BE_153606",
pathFormatted: "req_animal-api-sync-39487",
stats: {
    "name": "Animal-api sync for customer BE_153606",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4198",
        "ok": "4198",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4198",
        "ok": "4198",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4198",
        "ok": "4198",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4198",
        "ok": "4198",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4198",
        "ok": "4198",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4198",
        "ok": "4198",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4198",
        "ok": "4198",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-115a1": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_106548",
path: "Animal-api sync for customer NL_106548",
pathFormatted: "req_animal-api-sync-115a1",
stats: {
    "name": "Animal-api sync for customer NL_106548",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2801",
        "ok": "2801",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2801",
        "ok": "2801",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2801",
        "ok": "2801",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2801",
        "ok": "2801",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2801",
        "ok": "2801",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2801",
        "ok": "2801",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2801",
        "ok": "2801",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-dfd7c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_139331",
path: "Animal-api sync for customer NL_139331",
pathFormatted: "req_animal-api-sync-dfd7c",
stats: {
    "name": "Animal-api sync for customer NL_139331",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3030",
        "ok": "3030",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3030",
        "ok": "3030",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3030",
        "ok": "3030",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3030",
        "ok": "3030",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3030",
        "ok": "3030",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3030",
        "ok": "3030",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3030",
        "ok": "3030",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c6932": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_133968",
path: "Animal-api sync for customer NL_133968",
pathFormatted: "req_animal-api-sync-c6932",
stats: {
    "name": "Animal-api sync for customer NL_133968",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2574",
        "ok": "2574",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2574",
        "ok": "2574",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2574",
        "ok": "2574",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2574",
        "ok": "2574",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2574",
        "ok": "2574",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2574",
        "ok": "2574",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2574",
        "ok": "2574",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-4cd78": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111414",
path: "Animal-api sync for customer NL_111414",
pathFormatted: "req_animal-api-sync-4cd78",
stats: {
    "name": "Animal-api sync for customer NL_111414",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2600",
        "ok": "2600",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2600",
        "ok": "2600",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2600",
        "ok": "2600",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2600",
        "ok": "2600",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2600",
        "ok": "2600",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2600",
        "ok": "2600",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2600",
        "ok": "2600",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-0fed7": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_109760",
path: "Animal-api sync for customer NL_109760",
pathFormatted: "req_animal-api-sync-0fed7",
stats: {
    "name": "Animal-api sync for customer NL_109760",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2545",
        "ok": "2545",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2545",
        "ok": "2545",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2545",
        "ok": "2545",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2545",
        "ok": "2545",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2545",
        "ok": "2545",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2545",
        "ok": "2545",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2545",
        "ok": "2545",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-f47e6": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_120627",
path: "Animal-api sync for customer NL_120627",
pathFormatted: "req_animal-api-sync-f47e6",
stats: {
    "name": "Animal-api sync for customer NL_120627",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5014",
        "ok": "5014",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5014",
        "ok": "5014",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5014",
        "ok": "5014",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5014",
        "ok": "5014",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5014",
        "ok": "5014",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5014",
        "ok": "5014",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5014",
        "ok": "5014",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-66c16": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_112114",
path: "Animal-api sync for customer NL_112114",
pathFormatted: "req_animal-api-sync-66c16",
stats: {
    "name": "Animal-api sync for customer NL_112114",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2065",
        "ok": "2065",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2065",
        "ok": "2065",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2065",
        "ok": "2065",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2065",
        "ok": "2065",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2065",
        "ok": "2065",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2065",
        "ok": "2065",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2065",
        "ok": "2065",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-79b8e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_116848",
path: "Animal-api sync for customer NL_116848",
pathFormatted: "req_animal-api-sync-79b8e",
stats: {
    "name": "Animal-api sync for customer NL_116848",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3512",
        "ok": "3512",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3512",
        "ok": "3512",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3512",
        "ok": "3512",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3512",
        "ok": "3512",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3512",
        "ok": "3512",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3512",
        "ok": "3512",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3512",
        "ok": "3512",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-5420f": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110280",
path: "Animal-api sync for customer NL_110280",
pathFormatted: "req_animal-api-sync-5420f",
stats: {
    "name": "Animal-api sync for customer NL_110280",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3715",
        "ok": "3715",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3715",
        "ok": "3715",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3715",
        "ok": "3715",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3715",
        "ok": "3715",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3715",
        "ok": "3715",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3715",
        "ok": "3715",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3715",
        "ok": "3715",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-63b47": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111966",
path: "Animal-api sync for customer NL_111966",
pathFormatted: "req_animal-api-sync-63b47",
stats: {
    "name": "Animal-api sync for customer NL_111966",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2991",
        "ok": "2991",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2991",
        "ok": "2991",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2991",
        "ok": "2991",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2991",
        "ok": "2991",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2991",
        "ok": "2991",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2991",
        "ok": "2991",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2991",
        "ok": "2991",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-fbcf4": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_128711",
path: "Animal-api sync for customer NL_128711",
pathFormatted: "req_animal-api-sync-fbcf4",
stats: {
    "name": "Animal-api sync for customer NL_128711",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2744",
        "ok": "2744",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2744",
        "ok": "2744",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2744",
        "ok": "2744",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2744",
        "ok": "2744",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2744",
        "ok": "2744",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2744",
        "ok": "2744",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2744",
        "ok": "2744",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d05fc": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_131083",
path: "Animal-api sync for customer NL_131083",
pathFormatted: "req_animal-api-sync-d05fc",
stats: {
    "name": "Animal-api sync for customer NL_131083",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2938",
        "ok": "2938",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2938",
        "ok": "2938",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2938",
        "ok": "2938",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2938",
        "ok": "2938",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2938",
        "ok": "2938",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2938",
        "ok": "2938",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2938",
        "ok": "2938",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-ed470": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_115514",
path: "Animal-api sync for customer NL_115514",
pathFormatted: "req_animal-api-sync-ed470",
stats: {
    "name": "Animal-api sync for customer NL_115514",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2671",
        "ok": "2671",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2671",
        "ok": "2671",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2671",
        "ok": "2671",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2671",
        "ok": "2671",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2671",
        "ok": "2671",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2671",
        "ok": "2671",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2671",
        "ok": "2671",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-29375": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_121682",
path: "Animal-api sync for customer NL_121682",
pathFormatted: "req_animal-api-sync-29375",
stats: {
    "name": "Animal-api sync for customer NL_121682",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1902",
        "ok": "1902",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1902",
        "ok": "1902",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1902",
        "ok": "1902",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1902",
        "ok": "1902",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1902",
        "ok": "1902",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1902",
        "ok": "1902",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1902",
        "ok": "1902",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-4e713": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111266",
path: "Animal-api sync for customer NL_111266",
pathFormatted: "req_animal-api-sync-4e713",
stats: {
    "name": "Animal-api sync for customer NL_111266",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2255",
        "ok": "2255",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2255",
        "ok": "2255",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2255",
        "ok": "2255",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2255",
        "ok": "2255",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2255",
        "ok": "2255",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2255",
        "ok": "2255",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2255",
        "ok": "2255",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-5df96": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_114543",
path: "Animal-api sync for customer NL_114543",
pathFormatted: "req_animal-api-sync-5df96",
stats: {
    "name": "Animal-api sync for customer NL_114543",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1998",
        "ok": "1998",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1998",
        "ok": "1998",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1998",
        "ok": "1998",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1998",
        "ok": "1998",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1998",
        "ok": "1998",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1998",
        "ok": "1998",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1998",
        "ok": "1998",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-38246": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_118506",
path: "Animal-api sync for customer NL_118506",
pathFormatted: "req_animal-api-sync-38246",
stats: {
    "name": "Animal-api sync for customer NL_118506",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2349",
        "ok": "2349",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2349",
        "ok": "2349",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2349",
        "ok": "2349",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2349",
        "ok": "2349",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2349",
        "ok": "2349",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2349",
        "ok": "2349",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2349",
        "ok": "2349",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-3d430": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_134950",
path: "Animal-api sync for customer NL_134950",
pathFormatted: "req_animal-api-sync-3d430",
stats: {
    "name": "Animal-api sync for customer NL_134950",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1658",
        "ok": "1658",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1658",
        "ok": "1658",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1658",
        "ok": "1658",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1658",
        "ok": "1658",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1658",
        "ok": "1658",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1658",
        "ok": "1658",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1658",
        "ok": "1658",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a667b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_103980",
path: "Animal-api sync for customer NL_103980",
pathFormatted: "req_animal-api-sync-a667b",
stats: {
    "name": "Animal-api sync for customer NL_103980",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "10095",
        "ok": "10095",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "10095",
        "ok": "10095",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "10095",
        "ok": "10095",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "10095",
        "ok": "10095",
        "ko": "-"
    },
    "percentiles2": {
        "total": "10095",
        "ok": "10095",
        "ko": "-"
    },
    "percentiles3": {
        "total": "10095",
        "ok": "10095",
        "ko": "-"
    },
    "percentiles4": {
        "total": "10095",
        "ok": "10095",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-92bed": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105164",
path: "Animal-api sync for customer NL_105164",
pathFormatted: "req_animal-api-sync-92bed",
stats: {
    "name": "Animal-api sync for customer NL_105164",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3093",
        "ok": "3093",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3093",
        "ok": "3093",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3093",
        "ok": "3093",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3093",
        "ok": "3093",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3093",
        "ok": "3093",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3093",
        "ok": "3093",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3093",
        "ok": "3093",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-74e33": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_189313",
path: "Animal-api sync for customer BE_189313",
pathFormatted: "req_animal-api-sync-74e33",
stats: {
    "name": "Animal-api sync for customer BE_189313",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2770",
        "ok": "2770",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2770",
        "ok": "2770",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2770",
        "ok": "2770",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2770",
        "ok": "2770",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2770",
        "ok": "2770",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2770",
        "ok": "2770",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2770",
        "ok": "2770",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-aaf86": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_130803",
path: "Animal-api sync for customer NL_130803",
pathFormatted: "req_animal-api-sync-aaf86",
stats: {
    "name": "Animal-api sync for customer NL_130803",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3166",
        "ok": "3166",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3166",
        "ok": "3166",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3166",
        "ok": "3166",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3166",
        "ok": "3166",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3166",
        "ok": "3166",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3166",
        "ok": "3166",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3166",
        "ok": "3166",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-390c0": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_113006",
path: "Animal-api sync for customer NL_113006",
pathFormatted: "req_animal-api-sync-390c0",
stats: {
    "name": "Animal-api sync for customer NL_113006",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2290",
        "ok": "2290",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2290",
        "ok": "2290",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2290",
        "ok": "2290",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2290",
        "ok": "2290",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2290",
        "ok": "2290",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2290",
        "ok": "2290",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2290",
        "ok": "2290",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-363df": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_120238",
path: "Animal-api sync for customer NL_120238",
pathFormatted: "req_animal-api-sync-363df",
stats: {
    "name": "Animal-api sync for customer NL_120238",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2163",
        "ok": "2163",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2163",
        "ok": "2163",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2163",
        "ok": "2163",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2163",
        "ok": "2163",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2163",
        "ok": "2163",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2163",
        "ok": "2163",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2163",
        "ok": "2163",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e70df": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_129794",
path: "Animal-api sync for customer NL_129794",
pathFormatted: "req_animal-api-sync-e70df",
stats: {
    "name": "Animal-api sync for customer NL_129794",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1618",
        "ok": "1618",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1618",
        "ok": "1618",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1618",
        "ok": "1618",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1618",
        "ok": "1618",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1618",
        "ok": "1618",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1618",
        "ok": "1618",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1618",
        "ok": "1618",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a9219": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_103449",
path: "Animal-api sync for customer NL_103449",
pathFormatted: "req_animal-api-sync-a9219",
stats: {
    "name": "Animal-api sync for customer NL_103449",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2960",
        "ok": "2960",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2960",
        "ok": "2960",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2960",
        "ok": "2960",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2960",
        "ok": "2960",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2960",
        "ok": "2960",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2960",
        "ok": "2960",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2960",
        "ok": "2960",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-30320": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_128704",
path: "Animal-api sync for customer NL_128704",
pathFormatted: "req_animal-api-sync-30320",
stats: {
    "name": "Animal-api sync for customer NL_128704",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2056",
        "ok": "2056",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2056",
        "ok": "2056",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2056",
        "ok": "2056",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2056",
        "ok": "2056",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2056",
        "ok": "2056",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2056",
        "ok": "2056",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2056",
        "ok": "2056",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c50f5": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111928",
path: "Animal-api sync for customer NL_111928",
pathFormatted: "req_animal-api-sync-c50f5",
stats: {
    "name": "Animal-api sync for customer NL_111928",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3259",
        "ok": "3259",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3259",
        "ok": "3259",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3259",
        "ok": "3259",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3259",
        "ok": "3259",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3259",
        "ok": "3259",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3259",
        "ok": "3259",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3259",
        "ok": "3259",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-294ca": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111076",
path: "Animal-api sync for customer NL_111076",
pathFormatted: "req_animal-api-sync-294ca",
stats: {
    "name": "Animal-api sync for customer NL_111076",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2846",
        "ok": "2846",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2846",
        "ok": "2846",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2846",
        "ok": "2846",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2846",
        "ok": "2846",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2846",
        "ok": "2846",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2846",
        "ok": "2846",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2846",
        "ok": "2846",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-080dc": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_131881",
path: "Animal-api sync for customer NL_131881",
pathFormatted: "req_animal-api-sync-080dc",
stats: {
    "name": "Animal-api sync for customer NL_131881",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1923",
        "ok": "1923",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1923",
        "ok": "1923",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1923",
        "ok": "1923",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1923",
        "ok": "1923",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1923",
        "ok": "1923",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1923",
        "ok": "1923",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1923",
        "ok": "1923",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-9f153": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_113848",
path: "Animal-api sync for customer NL_113848",
pathFormatted: "req_animal-api-sync-9f153",
stats: {
    "name": "Animal-api sync for customer NL_113848",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2193",
        "ok": "2193",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2193",
        "ok": "2193",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2193",
        "ok": "2193",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2193",
        "ok": "2193",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2193",
        "ok": "2193",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2193",
        "ok": "2193",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2193",
        "ok": "2193",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-084e7": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_108590",
path: "Animal-api sync for customer NL_108590",
pathFormatted: "req_animal-api-sync-084e7",
stats: {
    "name": "Animal-api sync for customer NL_108590",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2647",
        "ok": "2647",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2647",
        "ok": "2647",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2647",
        "ok": "2647",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2647",
        "ok": "2647",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2647",
        "ok": "2647",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2647",
        "ok": "2647",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2647",
        "ok": "2647",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-93bcd": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_107077",
path: "Animal-api sync for customer NL_107077",
pathFormatted: "req_animal-api-sync-93bcd",
stats: {
    "name": "Animal-api sync for customer NL_107077",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1461",
        "ok": "1461",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1461",
        "ok": "1461",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1461",
        "ok": "1461",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1461",
        "ok": "1461",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1461",
        "ok": "1461",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1461",
        "ok": "1461",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1461",
        "ok": "1461",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-43480": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_211973",
path: "Animal-api sync for customer BE_211973",
pathFormatted: "req_animal-api-sync-43480",
stats: {
    "name": "Animal-api sync for customer BE_211973",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3995",
        "ok": "3995",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3995",
        "ok": "3995",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3995",
        "ok": "3995",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3995",
        "ok": "3995",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3995",
        "ok": "3995",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3995",
        "ok": "3995",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3995",
        "ok": "3995",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a5109": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_124277",
path: "Animal-api sync for customer NL_124277",
pathFormatted: "req_animal-api-sync-a5109",
stats: {
    "name": "Animal-api sync for customer NL_124277",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2886",
        "ok": "2886",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2886",
        "ok": "2886",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2886",
        "ok": "2886",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2886",
        "ok": "2886",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2886",
        "ok": "2886",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2886",
        "ok": "2886",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2886",
        "ok": "2886",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-53304": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_108829",
path: "Animal-api sync for customer NL_108829",
pathFormatted: "req_animal-api-sync-53304",
stats: {
    "name": "Animal-api sync for customer NL_108829",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3173",
        "ok": "3173",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3173",
        "ok": "3173",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3173",
        "ok": "3173",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3173",
        "ok": "3173",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3173",
        "ok": "3173",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3173",
        "ok": "3173",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3173",
        "ok": "3173",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-84fc1": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_131210",
path: "Animal-api sync for customer NL_131210",
pathFormatted: "req_animal-api-sync-84fc1",
stats: {
    "name": "Animal-api sync for customer NL_131210",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2063",
        "ok": "2063",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2063",
        "ok": "2063",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2063",
        "ok": "2063",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2063",
        "ok": "2063",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2063",
        "ok": "2063",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2063",
        "ok": "2063",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2063",
        "ok": "2063",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-fc8bc": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_119277",
path: "Animal-api sync for customer NL_119277",
pathFormatted: "req_animal-api-sync-fc8bc",
stats: {
    "name": "Animal-api sync for customer NL_119277",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2547",
        "ok": "2547",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2547",
        "ok": "2547",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2547",
        "ok": "2547",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2547",
        "ok": "2547",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2547",
        "ok": "2547",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2547",
        "ok": "2547",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2547",
        "ok": "2547",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b0637": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_109813",
path: "Animal-api sync for customer NL_109813",
pathFormatted: "req_animal-api-sync-b0637",
stats: {
    "name": "Animal-api sync for customer NL_109813",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2777",
        "ok": "2777",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2777",
        "ok": "2777",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2777",
        "ok": "2777",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2777",
        "ok": "2777",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2777",
        "ok": "2777",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2777",
        "ok": "2777",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2777",
        "ok": "2777",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d7914": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_189320",
path: "Animal-api sync for customer NL_189320",
pathFormatted: "req_animal-api-sync-d7914",
stats: {
    "name": "Animal-api sync for customer NL_189320",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2988",
        "ok": "2988",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2988",
        "ok": "2988",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2988",
        "ok": "2988",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2988",
        "ok": "2988",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2988",
        "ok": "2988",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2988",
        "ok": "2988",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2988",
        "ok": "2988",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-acdb8": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_113754",
path: "Animal-api sync for customer NL_113754",
pathFormatted: "req_animal-api-sync-acdb8",
stats: {
    "name": "Animal-api sync for customer NL_113754",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3292",
        "ok": "3292",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3292",
        "ok": "3292",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3292",
        "ok": "3292",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3292",
        "ok": "3292",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3292",
        "ok": "3292",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3292",
        "ok": "3292",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3292",
        "ok": "3292",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-36730": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_135526",
path: "Animal-api sync for customer NL_135526",
pathFormatted: "req_animal-api-sync-36730",
stats: {
    "name": "Animal-api sync for customer NL_135526",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2311",
        "ok": "2311",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2311",
        "ok": "2311",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2311",
        "ok": "2311",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2311",
        "ok": "2311",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2311",
        "ok": "2311",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2311",
        "ok": "2311",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2311",
        "ok": "2311",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-1e909": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_154872",
path: "Animal-api sync for customer BE_154872",
pathFormatted: "req_animal-api-sync-1e909",
stats: {
    "name": "Animal-api sync for customer BE_154872",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2768",
        "ok": "2768",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2768",
        "ok": "2768",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2768",
        "ok": "2768",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2768",
        "ok": "2768",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2768",
        "ok": "2768",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2768",
        "ok": "2768",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2768",
        "ok": "2768",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-f54d2": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_125689",
path: "Animal-api sync for customer NL_125689",
pathFormatted: "req_animal-api-sync-f54d2",
stats: {
    "name": "Animal-api sync for customer NL_125689",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3613",
        "ok": "3613",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3613",
        "ok": "3613",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3613",
        "ok": "3613",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3613",
        "ok": "3613",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3613",
        "ok": "3613",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3613",
        "ok": "3613",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3613",
        "ok": "3613",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-baa35": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110539",
path: "Animal-api sync for customer NL_110539",
pathFormatted: "req_animal-api-sync-baa35",
stats: {
    "name": "Animal-api sync for customer NL_110539",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3013",
        "ok": "3013",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3013",
        "ok": "3013",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3013",
        "ok": "3013",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3013",
        "ok": "3013",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3013",
        "ok": "3013",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3013",
        "ok": "3013",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3013",
        "ok": "3013",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-14454": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104271",
path: "Animal-api sync for customer NL_104271",
pathFormatted: "req_animal-api-sync-14454",
stats: {
    "name": "Animal-api sync for customer NL_104271",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4930",
        "ok": "4930",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4930",
        "ok": "4930",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4930",
        "ok": "4930",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4930",
        "ok": "4930",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4930",
        "ok": "4930",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4930",
        "ok": "4930",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4930",
        "ok": "4930",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b6907": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_123943",
path: "Animal-api sync for customer NL_123943",
pathFormatted: "req_animal-api-sync-b6907",
stats: {
    "name": "Animal-api sync for customer NL_123943",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2871",
        "ok": "2871",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2871",
        "ok": "2871",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2871",
        "ok": "2871",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2871",
        "ok": "2871",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2871",
        "ok": "2871",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2871",
        "ok": "2871",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2871",
        "ok": "2871",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c4646": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110070",
path: "Animal-api sync for customer NL_110070",
pathFormatted: "req_animal-api-sync-c4646",
stats: {
    "name": "Animal-api sync for customer NL_110070",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3295",
        "ok": "3295",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3295",
        "ok": "3295",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3295",
        "ok": "3295",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3295",
        "ok": "3295",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3295",
        "ok": "3295",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3295",
        "ok": "3295",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3295",
        "ok": "3295",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-0eeba": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_114348",
path: "Animal-api sync for customer NL_114348",
pathFormatted: "req_animal-api-sync-0eeba",
stats: {
    "name": "Animal-api sync for customer NL_114348",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2869",
        "ok": "2869",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2869",
        "ok": "2869",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2869",
        "ok": "2869",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2869",
        "ok": "2869",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2869",
        "ok": "2869",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2869",
        "ok": "2869",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2869",
        "ok": "2869",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-5c35b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_114376",
path: "Animal-api sync for customer NL_114376",
pathFormatted: "req_animal-api-sync-5c35b",
stats: {
    "name": "Animal-api sync for customer NL_114376",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1239",
        "ok": "1239",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1239",
        "ok": "1239",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1239",
        "ok": "1239",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1239",
        "ok": "1239",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1239",
        "ok": "1239",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1239",
        "ok": "1239",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1239",
        "ok": "1239",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-451e8": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_121268",
path: "Animal-api sync for customer NL_121268",
pathFormatted: "req_animal-api-sync-451e8",
stats: {
    "name": "Animal-api sync for customer NL_121268",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2554",
        "ok": "2554",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2554",
        "ok": "2554",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2554",
        "ok": "2554",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2554",
        "ok": "2554",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2554",
        "ok": "2554",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2554",
        "ok": "2554",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2554",
        "ok": "2554",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e42e2": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_128581",
path: "Animal-api sync for customer NL_128581",
pathFormatted: "req_animal-api-sync-e42e2",
stats: {
    "name": "Animal-api sync for customer NL_128581",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2009",
        "ok": "2009",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2009",
        "ok": "2009",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2009",
        "ok": "2009",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2009",
        "ok": "2009",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2009",
        "ok": "2009",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2009",
        "ok": "2009",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2009",
        "ok": "2009",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b164c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_120434",
path: "Animal-api sync for customer NL_120434",
pathFormatted: "req_animal-api-sync-b164c",
stats: {
    "name": "Animal-api sync for customer NL_120434",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4316",
        "ok": "4316",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4316",
        "ok": "4316",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4316",
        "ok": "4316",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4316",
        "ok": "4316",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4316",
        "ok": "4316",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4316",
        "ok": "4316",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4316",
        "ok": "4316",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-7461a": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_120398",
path: "Animal-api sync for customer NL_120398",
pathFormatted: "req_animal-api-sync-7461a",
stats: {
    "name": "Animal-api sync for customer NL_120398",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3012",
        "ok": "3012",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3012",
        "ok": "3012",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3012",
        "ok": "3012",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3012",
        "ok": "3012",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3012",
        "ok": "3012",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3012",
        "ok": "3012",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3012",
        "ok": "3012",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-07a69": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_103947",
path: "Animal-api sync for customer NL_103947",
pathFormatted: "req_animal-api-sync-07a69",
stats: {
    "name": "Animal-api sync for customer NL_103947",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5802",
        "ok": "5802",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5802",
        "ok": "5802",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5802",
        "ok": "5802",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5802",
        "ok": "5802",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5802",
        "ok": "5802",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5802",
        "ok": "5802",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5802",
        "ok": "5802",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c6405": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104467",
path: "Animal-api sync for customer NL_104467",
pathFormatted: "req_animal-api-sync-c6405",
stats: {
    "name": "Animal-api sync for customer NL_104467",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3110",
        "ok": "3110",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3110",
        "ok": "3110",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3110",
        "ok": "3110",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3110",
        "ok": "3110",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3110",
        "ok": "3110",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3110",
        "ok": "3110",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3110",
        "ok": "3110",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-23f13": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_125273",
path: "Animal-api sync for customer NL_125273",
pathFormatted: "req_animal-api-sync-23f13",
stats: {
    "name": "Animal-api sync for customer NL_125273",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3043",
        "ok": "3043",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3043",
        "ok": "3043",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3043",
        "ok": "3043",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3043",
        "ok": "3043",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3043",
        "ok": "3043",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3043",
        "ok": "3043",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3043",
        "ok": "3043",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-96127": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_161584",
path: "Animal-api sync for customer BE_161584",
pathFormatted: "req_animal-api-sync-96127",
stats: {
    "name": "Animal-api sync for customer BE_161584",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2822",
        "ok": "2822",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2822",
        "ok": "2822",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2822",
        "ok": "2822",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2822",
        "ok": "2822",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2822",
        "ok": "2822",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2822",
        "ok": "2822",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2822",
        "ok": "2822",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-614ac": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104734",
path: "Animal-api sync for customer NL_104734",
pathFormatted: "req_animal-api-sync-614ac",
stats: {
    "name": "Animal-api sync for customer NL_104734",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3045",
        "ok": "3045",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3045",
        "ok": "3045",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3045",
        "ok": "3045",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3045",
        "ok": "3045",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3045",
        "ok": "3045",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3045",
        "ok": "3045",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3045",
        "ok": "3045",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-2d56c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110590",
path: "Animal-api sync for customer NL_110590",
pathFormatted: "req_animal-api-sync-2d56c",
stats: {
    "name": "Animal-api sync for customer NL_110590",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2866",
        "ok": "2866",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2866",
        "ok": "2866",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2866",
        "ok": "2866",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2866",
        "ok": "2866",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2866",
        "ok": "2866",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2866",
        "ok": "2866",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2866",
        "ok": "2866",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e31c7": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_210459",
path: "Animal-api sync for customer NL_210459",
pathFormatted: "req_animal-api-sync-e31c7",
stats: {
    "name": "Animal-api sync for customer NL_210459",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2823",
        "ok": "2823",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2823",
        "ok": "2823",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2823",
        "ok": "2823",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2823",
        "ok": "2823",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2823",
        "ok": "2823",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2823",
        "ok": "2823",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2823",
        "ok": "2823",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-7fca2": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_158798",
path: "Animal-api sync for customer NL_158798",
pathFormatted: "req_animal-api-sync-7fca2",
stats: {
    "name": "Animal-api sync for customer NL_158798",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3229",
        "ok": "3229",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3229",
        "ok": "3229",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3229",
        "ok": "3229",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3229",
        "ok": "3229",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3229",
        "ok": "3229",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3229",
        "ok": "3229",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3229",
        "ok": "3229",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b8f29": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_153558",
path: "Animal-api sync for customer BE_153558",
pathFormatted: "req_animal-api-sync-b8f29",
stats: {
    "name": "Animal-api sync for customer BE_153558",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2120",
        "ok": "2120",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2120",
        "ok": "2120",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2120",
        "ok": "2120",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2120",
        "ok": "2120",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2120",
        "ok": "2120",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2120",
        "ok": "2120",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2120",
        "ok": "2120",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-aa22e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_137034",
path: "Animal-api sync for customer NL_137034",
pathFormatted: "req_animal-api-sync-aa22e",
stats: {
    "name": "Animal-api sync for customer NL_137034",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1213",
        "ok": "1213",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1213",
        "ok": "1213",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1213",
        "ok": "1213",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1213",
        "ok": "1213",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1213",
        "ok": "1213",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1213",
        "ok": "1213",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1213",
        "ok": "1213",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b5283": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_112270",
path: "Animal-api sync for customer NL_112270",
pathFormatted: "req_animal-api-sync-b5283",
stats: {
    "name": "Animal-api sync for customer NL_112270",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4011",
        "ok": "4011",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4011",
        "ok": "4011",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4011",
        "ok": "4011",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4011",
        "ok": "4011",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4011",
        "ok": "4011",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4011",
        "ok": "4011",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4011",
        "ok": "4011",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-10e14": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_113903",
path: "Animal-api sync for customer NL_113903",
pathFormatted: "req_animal-api-sync-10e14",
stats: {
    "name": "Animal-api sync for customer NL_113903",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "6269",
        "ok": "6269",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "6269",
        "ok": "6269",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "6269",
        "ok": "6269",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "6269",
        "ok": "6269",
        "ko": "-"
    },
    "percentiles2": {
        "total": "6269",
        "ok": "6269",
        "ko": "-"
    },
    "percentiles3": {
        "total": "6269",
        "ok": "6269",
        "ko": "-"
    },
    "percentiles4": {
        "total": "6269",
        "ok": "6269",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-3dd2c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105439",
path: "Animal-api sync for customer NL_105439",
pathFormatted: "req_animal-api-sync-3dd2c",
stats: {
    "name": "Animal-api sync for customer NL_105439",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2833",
        "ok": "2833",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2833",
        "ok": "2833",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2833",
        "ok": "2833",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2833",
        "ok": "2833",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2833",
        "ok": "2833",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2833",
        "ok": "2833",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2833",
        "ok": "2833",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-8785d": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_207024",
path: "Animal-api sync for customer BE_207024",
pathFormatted: "req_animal-api-sync-8785d",
stats: {
    "name": "Animal-api sync for customer BE_207024",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2280",
        "ok": "2280",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2280",
        "ok": "2280",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2280",
        "ok": "2280",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2280",
        "ok": "2280",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2280",
        "ok": "2280",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2280",
        "ok": "2280",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2280",
        "ok": "2280",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-010fe": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_129945",
path: "Animal-api sync for customer NL_129945",
pathFormatted: "req_animal-api-sync-010fe",
stats: {
    "name": "Animal-api sync for customer NL_129945",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2097",
        "ok": "2097",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2097",
        "ok": "2097",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2097",
        "ok": "2097",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2097",
        "ok": "2097",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2097",
        "ok": "2097",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2097",
        "ok": "2097",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2097",
        "ok": "2097",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-58bfa": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_131425",
path: "Animal-api sync for customer NL_131425",
pathFormatted: "req_animal-api-sync-58bfa",
stats: {
    "name": "Animal-api sync for customer NL_131425",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2290",
        "ok": "2290",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2290",
        "ok": "2290",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2290",
        "ok": "2290",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2290",
        "ok": "2290",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2290",
        "ok": "2290",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2290",
        "ok": "2290",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2290",
        "ok": "2290",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-9a226": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_113819",
path: "Animal-api sync for customer NL_113819",
pathFormatted: "req_animal-api-sync-9a226",
stats: {
    "name": "Animal-api sync for customer NL_113819",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2099",
        "ok": "2099",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2099",
        "ok": "2099",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2099",
        "ok": "2099",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2099",
        "ok": "2099",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2099",
        "ok": "2099",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2099",
        "ok": "2099",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2099",
        "ok": "2099",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-44a53": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_121469",
path: "Animal-api sync for customer NL_121469",
pathFormatted: "req_animal-api-sync-44a53",
stats: {
    "name": "Animal-api sync for customer NL_121469",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3142",
        "ok": "3142",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3142",
        "ok": "3142",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3142",
        "ok": "3142",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3142",
        "ok": "3142",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3142",
        "ok": "3142",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3142",
        "ok": "3142",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3142",
        "ok": "3142",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c3fd5": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104563",
path: "Animal-api sync for customer NL_104563",
pathFormatted: "req_animal-api-sync-c3fd5",
stats: {
    "name": "Animal-api sync for customer NL_104563",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2669",
        "ok": "2669",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2669",
        "ok": "2669",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2669",
        "ok": "2669",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2669",
        "ok": "2669",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2669",
        "ok": "2669",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2669",
        "ok": "2669",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2669",
        "ok": "2669",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-2f86f": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_122640",
path: "Animal-api sync for customer NL_122640",
pathFormatted: "req_animal-api-sync-2f86f",
stats: {
    "name": "Animal-api sync for customer NL_122640",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5325",
        "ok": "5325",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5325",
        "ok": "5325",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5325",
        "ok": "5325",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5325",
        "ok": "5325",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5325",
        "ok": "5325",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5325",
        "ok": "5325",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5325",
        "ok": "5325",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-447ce": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_136064",
path: "Animal-api sync for customer NL_136064",
pathFormatted: "req_animal-api-sync-447ce",
stats: {
    "name": "Animal-api sync for customer NL_136064",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2412",
        "ok": "2412",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2412",
        "ok": "2412",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2412",
        "ok": "2412",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2412",
        "ok": "2412",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2412",
        "ok": "2412",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2412",
        "ok": "2412",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2412",
        "ok": "2412",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-02723": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_106633",
path: "Animal-api sync for customer NL_106633",
pathFormatted: "req_animal-api-sync-02723",
stats: {
    "name": "Animal-api sync for customer NL_106633",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2638",
        "ok": "2638",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2638",
        "ok": "2638",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2638",
        "ok": "2638",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2638",
        "ok": "2638",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2638",
        "ok": "2638",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2638",
        "ok": "2638",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2638",
        "ok": "2638",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-19174": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117339",
path: "Animal-api sync for customer NL_117339",
pathFormatted: "req_animal-api-sync-19174",
stats: {
    "name": "Animal-api sync for customer NL_117339",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2175",
        "ok": "2175",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2175",
        "ok": "2175",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2175",
        "ok": "2175",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2175",
        "ok": "2175",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2175",
        "ok": "2175",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2175",
        "ok": "2175",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2175",
        "ok": "2175",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-9a47c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_115141",
path: "Animal-api sync for customer NL_115141",
pathFormatted: "req_animal-api-sync-9a47c",
stats: {
    "name": "Animal-api sync for customer NL_115141",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2220",
        "ok": "2220",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2220",
        "ok": "2220",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2220",
        "ok": "2220",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2220",
        "ok": "2220",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2220",
        "ok": "2220",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2220",
        "ok": "2220",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2220",
        "ok": "2220",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-0a9fa": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_125735",
path: "Animal-api sync for customer NL_125735",
pathFormatted: "req_animal-api-sync-0a9fa",
stats: {
    "name": "Animal-api sync for customer NL_125735",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2436",
        "ok": "2436",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2436",
        "ok": "2436",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2436",
        "ok": "2436",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2436",
        "ok": "2436",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2436",
        "ok": "2436",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2436",
        "ok": "2436",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2436",
        "ok": "2436",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-56dad": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_112805",
path: "Animal-api sync for customer NL_112805",
pathFormatted: "req_animal-api-sync-56dad",
stats: {
    "name": "Animal-api sync for customer NL_112805",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2100",
        "ok": "2100",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2100",
        "ok": "2100",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2100",
        "ok": "2100",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2100",
        "ok": "2100",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2100",
        "ok": "2100",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2100",
        "ok": "2100",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2100",
        "ok": "2100",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-4c3a5": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110894",
path: "Animal-api sync for customer NL_110894",
pathFormatted: "req_animal-api-sync-4c3a5",
stats: {
    "name": "Animal-api sync for customer NL_110894",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2555",
        "ok": "2555",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2555",
        "ok": "2555",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2555",
        "ok": "2555",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2555",
        "ok": "2555",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2555",
        "ok": "2555",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2555",
        "ok": "2555",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2555",
        "ok": "2555",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-86c49": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_145826",
path: "Animal-api sync for customer BE_145826",
pathFormatted: "req_animal-api-sync-86c49",
stats: {
    "name": "Animal-api sync for customer BE_145826",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2016",
        "ok": "2016",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2016",
        "ok": "2016",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2016",
        "ok": "2016",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2016",
        "ok": "2016",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2016",
        "ok": "2016",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2016",
        "ok": "2016",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2016",
        "ok": "2016",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-9e7ff": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110428",
path: "Animal-api sync for customer NL_110428",
pathFormatted: "req_animal-api-sync-9e7ff",
stats: {
    "name": "Animal-api sync for customer NL_110428",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2288",
        "ok": "2288",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2288",
        "ok": "2288",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2288",
        "ok": "2288",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2288",
        "ok": "2288",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2288",
        "ok": "2288",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2288",
        "ok": "2288",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2288",
        "ok": "2288",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-6c072": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_115279",
path: "Animal-api sync for customer NL_115279",
pathFormatted: "req_animal-api-sync-6c072",
stats: {
    "name": "Animal-api sync for customer NL_115279",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2313",
        "ok": "2313",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2313",
        "ok": "2313",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2313",
        "ok": "2313",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2313",
        "ok": "2313",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2313",
        "ok": "2313",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2313",
        "ok": "2313",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2313",
        "ok": "2313",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-2de80": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_129343",
path: "Animal-api sync for customer NL_129343",
pathFormatted: "req_animal-api-sync-2de80",
stats: {
    "name": "Animal-api sync for customer NL_129343",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2549",
        "ok": "2549",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2549",
        "ok": "2549",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2549",
        "ok": "2549",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2549",
        "ok": "2549",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2549",
        "ok": "2549",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2549",
        "ok": "2549",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2549",
        "ok": "2549",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-925cc": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_120686",
path: "Animal-api sync for customer NL_120686",
pathFormatted: "req_animal-api-sync-925cc",
stats: {
    "name": "Animal-api sync for customer NL_120686",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2651",
        "ok": "2651",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2651",
        "ok": "2651",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2651",
        "ok": "2651",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2651",
        "ok": "2651",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2651",
        "ok": "2651",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2651",
        "ok": "2651",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2651",
        "ok": "2651",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-f6a28": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_129516",
path: "Animal-api sync for customer NL_129516",
pathFormatted: "req_animal-api-sync-f6a28",
stats: {
    "name": "Animal-api sync for customer NL_129516",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "10650",
        "ok": "10650",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "10650",
        "ok": "10650",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "10650",
        "ok": "10650",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "10650",
        "ok": "10650",
        "ko": "-"
    },
    "percentiles2": {
        "total": "10650",
        "ok": "10650",
        "ko": "-"
    },
    "percentiles3": {
        "total": "10650",
        "ok": "10650",
        "ko": "-"
    },
    "percentiles4": {
        "total": "10650",
        "ok": "10650",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-10907": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_155615",
path: "Animal-api sync for customer NL_155615",
pathFormatted: "req_animal-api-sync-10907",
stats: {
    "name": "Animal-api sync for customer NL_155615",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2816",
        "ok": "2816",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2816",
        "ok": "2816",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2816",
        "ok": "2816",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2816",
        "ok": "2816",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2816",
        "ok": "2816",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2816",
        "ok": "2816",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2816",
        "ok": "2816",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-83fad": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_161486",
path: "Animal-api sync for customer NL_161486",
pathFormatted: "req_animal-api-sync-83fad",
stats: {
    "name": "Animal-api sync for customer NL_161486",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3347",
        "ok": "3347",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3347",
        "ok": "3347",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3347",
        "ok": "3347",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3347",
        "ok": "3347",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3347",
        "ok": "3347",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3347",
        "ok": "3347",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3347",
        "ok": "3347",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-6eeed": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_136560",
path: "Animal-api sync for customer NL_136560",
pathFormatted: "req_animal-api-sync-6eeed",
stats: {
    "name": "Animal-api sync for customer NL_136560",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1923",
        "ok": "1923",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1923",
        "ok": "1923",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1923",
        "ok": "1923",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1923",
        "ok": "1923",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1923",
        "ok": "1923",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1923",
        "ok": "1923",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1923",
        "ok": "1923",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-678da": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_116304",
path: "Animal-api sync for customer NL_116304",
pathFormatted: "req_animal-api-sync-678da",
stats: {
    "name": "Animal-api sync for customer NL_116304",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3595",
        "ok": "3595",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3595",
        "ok": "3595",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3595",
        "ok": "3595",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3595",
        "ok": "3595",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3595",
        "ok": "3595",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3595",
        "ok": "3595",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3595",
        "ok": "3595",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-23e9e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_136624",
path: "Animal-api sync for customer NL_136624",
pathFormatted: "req_animal-api-sync-23e9e",
stats: {
    "name": "Animal-api sync for customer NL_136624",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1896",
        "ok": "1896",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1896",
        "ok": "1896",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1896",
        "ok": "1896",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1896",
        "ok": "1896",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1896",
        "ok": "1896",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1896",
        "ok": "1896",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1896",
        "ok": "1896",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b45fa": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_160578",
path: "Animal-api sync for customer BE_160578",
pathFormatted: "req_animal-api-sync-b45fa",
stats: {
    "name": "Animal-api sync for customer BE_160578",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4687",
        "ok": "4687",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4687",
        "ok": "4687",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4687",
        "ok": "4687",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4687",
        "ok": "4687",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4687",
        "ok": "4687",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4687",
        "ok": "4687",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4687",
        "ok": "4687",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-ed8de": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117329",
path: "Animal-api sync for customer NL_117329",
pathFormatted: "req_animal-api-sync-ed8de",
stats: {
    "name": "Animal-api sync for customer NL_117329",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2969",
        "ok": "2969",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2969",
        "ok": "2969",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2969",
        "ok": "2969",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2969",
        "ok": "2969",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2969",
        "ok": "2969",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2969",
        "ok": "2969",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2969",
        "ok": "2969",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-7699b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_115653",
path: "Animal-api sync for customer NL_115653",
pathFormatted: "req_animal-api-sync-7699b",
stats: {
    "name": "Animal-api sync for customer NL_115653",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2387",
        "ok": "2387",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2387",
        "ok": "2387",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2387",
        "ok": "2387",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2387",
        "ok": "2387",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2387",
        "ok": "2387",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2387",
        "ok": "2387",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2387",
        "ok": "2387",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-f6750": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_145660",
path: "Animal-api sync for customer BE_145660",
pathFormatted: "req_animal-api-sync-f6750",
stats: {
    "name": "Animal-api sync for customer BE_145660",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3198",
        "ok": "3198",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3198",
        "ok": "3198",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3198",
        "ok": "3198",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3198",
        "ok": "3198",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3198",
        "ok": "3198",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3198",
        "ok": "3198",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3198",
        "ok": "3198",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-de4e1": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_196422",
path: "Animal-api sync for customer BE_196422",
pathFormatted: "req_animal-api-sync-de4e1",
stats: {
    "name": "Animal-api sync for customer BE_196422",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2073",
        "ok": "2073",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2073",
        "ok": "2073",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2073",
        "ok": "2073",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2073",
        "ok": "2073",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2073",
        "ok": "2073",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2073",
        "ok": "2073",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2073",
        "ok": "2073",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-94458": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111031",
path: "Animal-api sync for customer NL_111031",
pathFormatted: "req_animal-api-sync-94458",
stats: {
    "name": "Animal-api sync for customer NL_111031",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2272",
        "ok": "2272",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2272",
        "ok": "2272",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2272",
        "ok": "2272",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2272",
        "ok": "2272",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2272",
        "ok": "2272",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2272",
        "ok": "2272",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2272",
        "ok": "2272",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-1114b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105737",
path: "Animal-api sync for customer NL_105737",
pathFormatted: "req_animal-api-sync-1114b",
stats: {
    "name": "Animal-api sync for customer NL_105737",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2482",
        "ok": "2482",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2482",
        "ok": "2482",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2482",
        "ok": "2482",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2482",
        "ok": "2482",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2482",
        "ok": "2482",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2482",
        "ok": "2482",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2482",
        "ok": "2482",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-34261": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_112642",
path: "Animal-api sync for customer NL_112642",
pathFormatted: "req_animal-api-sync-34261",
stats: {
    "name": "Animal-api sync for customer NL_112642",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3687",
        "ok": "3687",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3687",
        "ok": "3687",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3687",
        "ok": "3687",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3687",
        "ok": "3687",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3687",
        "ok": "3687",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3687",
        "ok": "3687",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3687",
        "ok": "3687",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-88106": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_112559",
path: "Animal-api sync for customer NL_112559",
pathFormatted: "req_animal-api-sync-88106",
stats: {
    "name": "Animal-api sync for customer NL_112559",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2068",
        "ok": "2068",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2068",
        "ok": "2068",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2068",
        "ok": "2068",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2068",
        "ok": "2068",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2068",
        "ok": "2068",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2068",
        "ok": "2068",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2068",
        "ok": "2068",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-12cf4": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_103806",
path: "Animal-api sync for customer NL_103806",
pathFormatted: "req_animal-api-sync-12cf4",
stats: {
    "name": "Animal-api sync for customer NL_103806",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3468",
        "ok": "3468",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3468",
        "ok": "3468",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3468",
        "ok": "3468",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3468",
        "ok": "3468",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3468",
        "ok": "3468",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3468",
        "ok": "3468",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3468",
        "ok": "3468",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-0c8bf": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_135056",
path: "Animal-api sync for customer NL_135056",
pathFormatted: "req_animal-api-sync-0c8bf",
stats: {
    "name": "Animal-api sync for customer NL_135056",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3277",
        "ok": "3277",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3277",
        "ok": "3277",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3277",
        "ok": "3277",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3277",
        "ok": "3277",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3277",
        "ok": "3277",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3277",
        "ok": "3277",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3277",
        "ok": "3277",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-cd315": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_115640",
path: "Animal-api sync for customer NL_115640",
pathFormatted: "req_animal-api-sync-cd315",
stats: {
    "name": "Animal-api sync for customer NL_115640",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2179",
        "ok": "2179",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2179",
        "ok": "2179",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2179",
        "ok": "2179",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2179",
        "ok": "2179",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2179",
        "ok": "2179",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2179",
        "ok": "2179",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2179",
        "ok": "2179",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-708ff": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111792",
path: "Animal-api sync for customer NL_111792",
pathFormatted: "req_animal-api-sync-708ff",
stats: {
    "name": "Animal-api sync for customer NL_111792",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2526",
        "ok": "2526",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2526",
        "ok": "2526",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2526",
        "ok": "2526",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2526",
        "ok": "2526",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2526",
        "ok": "2526",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2526",
        "ok": "2526",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2526",
        "ok": "2526",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c74f5": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110479",
path: "Animal-api sync for customer NL_110479",
pathFormatted: "req_animal-api-sync-c74f5",
stats: {
    "name": "Animal-api sync for customer NL_110479",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3451",
        "ok": "3451",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3451",
        "ok": "3451",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3451",
        "ok": "3451",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3451",
        "ok": "3451",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3451",
        "ok": "3451",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3451",
        "ok": "3451",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3451",
        "ok": "3451",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b4519": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110383",
path: "Animal-api sync for customer NL_110383",
pathFormatted: "req_animal-api-sync-b4519",
stats: {
    "name": "Animal-api sync for customer NL_110383",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2377",
        "ok": "2377",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2377",
        "ok": "2377",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2377",
        "ok": "2377",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2377",
        "ok": "2377",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2377",
        "ok": "2377",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2377",
        "ok": "2377",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2377",
        "ok": "2377",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-ccec3": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_159045",
path: "Animal-api sync for customer BE_159045",
pathFormatted: "req_animal-api-sync-ccec3",
stats: {
    "name": "Animal-api sync for customer BE_159045",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2663",
        "ok": "2663",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2663",
        "ok": "2663",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2663",
        "ok": "2663",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2663",
        "ok": "2663",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2663",
        "ok": "2663",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2663",
        "ok": "2663",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2663",
        "ok": "2663",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-f73d7": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_112171",
path: "Animal-api sync for customer NL_112171",
pathFormatted: "req_animal-api-sync-f73d7",
stats: {
    "name": "Animal-api sync for customer NL_112171",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3245",
        "ok": "3245",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3245",
        "ok": "3245",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3245",
        "ok": "3245",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3245",
        "ok": "3245",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3245",
        "ok": "3245",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3245",
        "ok": "3245",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3245",
        "ok": "3245",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c61a4": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_158630",
path: "Animal-api sync for customer NL_158630",
pathFormatted: "req_animal-api-sync-c61a4",
stats: {
    "name": "Animal-api sync for customer NL_158630",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2632",
        "ok": "2632",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2632",
        "ok": "2632",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2632",
        "ok": "2632",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2632",
        "ok": "2632",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2632",
        "ok": "2632",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2632",
        "ok": "2632",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2632",
        "ok": "2632",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-3df21": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111890",
path: "Animal-api sync for customer NL_111890",
pathFormatted: "req_animal-api-sync-3df21",
stats: {
    "name": "Animal-api sync for customer NL_111890",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2765",
        "ok": "2765",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2765",
        "ok": "2765",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2765",
        "ok": "2765",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2765",
        "ok": "2765",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2765",
        "ok": "2765",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2765",
        "ok": "2765",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2765",
        "ok": "2765",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-11d0e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_119978",
path: "Animal-api sync for customer NL_119978",
pathFormatted: "req_animal-api-sync-11d0e",
stats: {
    "name": "Animal-api sync for customer NL_119978",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2932",
        "ok": "2932",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2932",
        "ok": "2932",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2932",
        "ok": "2932",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2932",
        "ok": "2932",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2932",
        "ok": "2932",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2932",
        "ok": "2932",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2932",
        "ok": "2932",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-cd0f8": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117391",
path: "Animal-api sync for customer NL_117391",
pathFormatted: "req_animal-api-sync-cd0f8",
stats: {
    "name": "Animal-api sync for customer NL_117391",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2874",
        "ok": "2874",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2874",
        "ok": "2874",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2874",
        "ok": "2874",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2874",
        "ok": "2874",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2874",
        "ok": "2874",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2874",
        "ok": "2874",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2874",
        "ok": "2874",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-997ea": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111277",
path: "Animal-api sync for customer NL_111277",
pathFormatted: "req_animal-api-sync-997ea",
stats: {
    "name": "Animal-api sync for customer NL_111277",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3060",
        "ok": "3060",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3060",
        "ok": "3060",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3060",
        "ok": "3060",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3060",
        "ok": "3060",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3060",
        "ok": "3060",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3060",
        "ok": "3060",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3060",
        "ok": "3060",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-998da": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_120046",
path: "Animal-api sync for customer NL_120046",
pathFormatted: "req_animal-api-sync-998da",
stats: {
    "name": "Animal-api sync for customer NL_120046",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1937",
        "ok": "1937",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1937",
        "ok": "1937",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1937",
        "ok": "1937",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1937",
        "ok": "1937",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1937",
        "ok": "1937",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1937",
        "ok": "1937",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1937",
        "ok": "1937",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-08b09": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_122753",
path: "Animal-api sync for customer NL_122753",
pathFormatted: "req_animal-api-sync-08b09",
stats: {
    "name": "Animal-api sync for customer NL_122753",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3631",
        "ok": "3631",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3631",
        "ok": "3631",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3631",
        "ok": "3631",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3631",
        "ok": "3631",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3631",
        "ok": "3631",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3631",
        "ok": "3631",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3631",
        "ok": "3631",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a5635": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_193658",
path: "Animal-api sync for customer BE_193658",
pathFormatted: "req_animal-api-sync-a5635",
stats: {
    "name": "Animal-api sync for customer BE_193658",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2786",
        "ok": "2786",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2786",
        "ok": "2786",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2786",
        "ok": "2786",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2786",
        "ok": "2786",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2786",
        "ok": "2786",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2786",
        "ok": "2786",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2786",
        "ok": "2786",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-f89f8": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_121091",
path: "Animal-api sync for customer NL_121091",
pathFormatted: "req_animal-api-sync-f89f8",
stats: {
    "name": "Animal-api sync for customer NL_121091",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1224",
        "ok": "1224",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1224",
        "ok": "1224",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1224",
        "ok": "1224",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1224",
        "ok": "1224",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1224",
        "ok": "1224",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1224",
        "ok": "1224",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1224",
        "ok": "1224",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-6c82d": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_143962",
path: "Animal-api sync for customer NL_143962",
pathFormatted: "req_animal-api-sync-6c82d",
stats: {
    "name": "Animal-api sync for customer NL_143962",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2037",
        "ok": "2037",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2037",
        "ok": "2037",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2037",
        "ok": "2037",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2037",
        "ok": "2037",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2037",
        "ok": "2037",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2037",
        "ok": "2037",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2037",
        "ok": "2037",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-afe85": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104819",
path: "Animal-api sync for customer NL_104819",
pathFormatted: "req_animal-api-sync-afe85",
stats: {
    "name": "Animal-api sync for customer NL_104819",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2875",
        "ok": "2875",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2875",
        "ok": "2875",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2875",
        "ok": "2875",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2875",
        "ok": "2875",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2875",
        "ok": "2875",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2875",
        "ok": "2875",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2875",
        "ok": "2875",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d9f9d": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_156439",
path: "Animal-api sync for customer BE_156439",
pathFormatted: "req_animal-api-sync-d9f9d",
stats: {
    "name": "Animal-api sync for customer BE_156439",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2901",
        "ok": "2901",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2901",
        "ok": "2901",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2901",
        "ok": "2901",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2901",
        "ok": "2901",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2901",
        "ok": "2901",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2901",
        "ok": "2901",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2901",
        "ok": "2901",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-038e6": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117080",
path: "Animal-api sync for customer NL_117080",
pathFormatted: "req_animal-api-sync-038e6",
stats: {
    "name": "Animal-api sync for customer NL_117080",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2617",
        "ok": "2617",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2617",
        "ok": "2617",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2617",
        "ok": "2617",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2617",
        "ok": "2617",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2617",
        "ok": "2617",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2617",
        "ok": "2617",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2617",
        "ok": "2617",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-9a2c1": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_142403",
path: "Animal-api sync for customer NL_142403",
pathFormatted: "req_animal-api-sync-9a2c1",
stats: {
    "name": "Animal-api sync for customer NL_142403",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2543",
        "ok": "2543",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2543",
        "ok": "2543",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2543",
        "ok": "2543",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2543",
        "ok": "2543",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2543",
        "ok": "2543",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2543",
        "ok": "2543",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2543",
        "ok": "2543",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e1749": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_121066",
path: "Animal-api sync for customer NL_121066",
pathFormatted: "req_animal-api-sync-e1749",
stats: {
    "name": "Animal-api sync for customer NL_121066",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2234",
        "ok": "2234",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2234",
        "ok": "2234",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2234",
        "ok": "2234",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2234",
        "ok": "2234",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2234",
        "ok": "2234",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2234",
        "ok": "2234",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2234",
        "ok": "2234",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c09d9": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_126693",
path: "Animal-api sync for customer NL_126693",
pathFormatted: "req_animal-api-sync-c09d9",
stats: {
    "name": "Animal-api sync for customer NL_126693",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2006",
        "ok": "2006",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2006",
        "ok": "2006",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2006",
        "ok": "2006",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2006",
        "ok": "2006",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2006",
        "ok": "2006",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2006",
        "ok": "2006",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2006",
        "ok": "2006",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-cdc3c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_125737",
path: "Animal-api sync for customer NL_125737",
pathFormatted: "req_animal-api-sync-cdc3c",
stats: {
    "name": "Animal-api sync for customer NL_125737",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3009",
        "ok": "3009",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3009",
        "ok": "3009",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3009",
        "ok": "3009",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3009",
        "ok": "3009",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3009",
        "ok": "3009",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3009",
        "ok": "3009",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3009",
        "ok": "3009",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-339e7": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_115945",
path: "Animal-api sync for customer NL_115945",
pathFormatted: "req_animal-api-sync-339e7",
stats: {
    "name": "Animal-api sync for customer NL_115945",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1610",
        "ok": "1610",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1610",
        "ok": "1610",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1610",
        "ok": "1610",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1610",
        "ok": "1610",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1610",
        "ok": "1610",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1610",
        "ok": "1610",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1610",
        "ok": "1610",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c1773": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105858",
path: "Animal-api sync for customer NL_105858",
pathFormatted: "req_animal-api-sync-c1773",
stats: {
    "name": "Animal-api sync for customer NL_105858",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2763",
        "ok": "2763",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2763",
        "ok": "2763",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2763",
        "ok": "2763",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2763",
        "ok": "2763",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2763",
        "ok": "2763",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2763",
        "ok": "2763",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2763",
        "ok": "2763",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c41a4": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_118516",
path: "Animal-api sync for customer NL_118516",
pathFormatted: "req_animal-api-sync-c41a4",
stats: {
    "name": "Animal-api sync for customer NL_118516",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2043",
        "ok": "2043",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2043",
        "ok": "2043",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2043",
        "ok": "2043",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2043",
        "ok": "2043",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2043",
        "ok": "2043",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2043",
        "ok": "2043",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2043",
        "ok": "2043",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e8c3a": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_154725",
path: "Animal-api sync for customer BE_154725",
pathFormatted: "req_animal-api-sync-e8c3a",
stats: {
    "name": "Animal-api sync for customer BE_154725",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2419",
        "ok": "2419",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2419",
        "ok": "2419",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2419",
        "ok": "2419",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2419",
        "ok": "2419",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2419",
        "ok": "2419",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2419",
        "ok": "2419",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2419",
        "ok": "2419",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-fc13a": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_154710",
path: "Animal-api sync for customer BE_154710",
pathFormatted: "req_animal-api-sync-fc13a",
stats: {
    "name": "Animal-api sync for customer BE_154710",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2921",
        "ok": "2921",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2921",
        "ok": "2921",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2921",
        "ok": "2921",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2921",
        "ok": "2921",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2921",
        "ok": "2921",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2921",
        "ok": "2921",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2921",
        "ok": "2921",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d0d7d": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_147835",
path: "Animal-api sync for customer BE_147835",
pathFormatted: "req_animal-api-sync-d0d7d",
stats: {
    "name": "Animal-api sync for customer BE_147835",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2959",
        "ok": "2959",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2959",
        "ok": "2959",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2959",
        "ok": "2959",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2959",
        "ok": "2959",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2959",
        "ok": "2959",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2959",
        "ok": "2959",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2959",
        "ok": "2959",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-f852c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_129269",
path: "Animal-api sync for customer NL_129269",
pathFormatted: "req_animal-api-sync-f852c",
stats: {
    "name": "Animal-api sync for customer NL_129269",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3493",
        "ok": "3493",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3493",
        "ok": "3493",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3493",
        "ok": "3493",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3493",
        "ok": "3493",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3493",
        "ok": "3493",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3493",
        "ok": "3493",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3493",
        "ok": "3493",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
