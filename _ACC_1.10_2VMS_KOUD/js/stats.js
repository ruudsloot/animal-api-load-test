var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "495",
        "ok": "417",
        "ko": "78"
    },
    "minResponseTime": {
        "total": "958",
        "ok": "958",
        "ko": "60000"
    },
    "maxResponseTime": {
        "total": "60010",
        "ok": "59485",
        "ko": "60010"
    },
    "meanResponseTime": {
        "total": "24278",
        "ok": "17595",
        "ko": "60004"
    },
    "standardDeviation": {
        "total": "21770",
        "ok": "16709",
        "ko": "3"
    },
    "percentiles1": {
        "total": "13622",
        "ok": "10007",
        "ko": "60003"
    },
    "percentiles2": {
        "total": "46634",
        "ok": "25374",
        "ko": "60005"
    },
    "percentiles3": {
        "total": "60005",
        "ok": "54645",
        "ko": "60009"
    },
    "percentiles4": {
        "total": "60009",
        "ok": "58134",
        "ko": "60010"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 1,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 416,
    "percentage": 84
},
    "group4": {
    "name": "failed",
    "count": 78,
    "percentage": 16
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.095",
        "ok": "0.923",
        "ko": "0.173"
    }
},
contents: {
"req_animal-api-sync-9e54f": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_112168",
path: "Animal-api sync for customer NL_112168",
pathFormatted: "req_animal-api-sync-9e54f",
stats: {
    "name": "Animal-api sync for customer NL_112168",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "7019",
        "ok": "7019",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "7019",
        "ok": "7019",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "7019",
        "ok": "7019",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "7019",
        "ok": "7019",
        "ko": "-"
    },
    "percentiles2": {
        "total": "7019",
        "ok": "7019",
        "ko": "-"
    },
    "percentiles3": {
        "total": "7019",
        "ok": "7019",
        "ko": "-"
    },
    "percentiles4": {
        "total": "7019",
        "ok": "7019",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-f5a2a": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_125092",
path: "Animal-api sync for customer NL_125092",
pathFormatted: "req_animal-api-sync-f5a2a",
stats: {
    "name": "Animal-api sync for customer NL_125092",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "7671",
        "ok": "7671",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "7671",
        "ok": "7671",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "7671",
        "ok": "7671",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "7671",
        "ok": "7671",
        "ko": "-"
    },
    "percentiles2": {
        "total": "7671",
        "ok": "7671",
        "ko": "-"
    },
    "percentiles3": {
        "total": "7671",
        "ok": "7671",
        "ko": "-"
    },
    "percentiles4": {
        "total": "7671",
        "ok": "7671",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-0f1c3": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_128277",
path: "Animal-api sync for customer NL_128277",
pathFormatted: "req_animal-api-sync-0f1c3",
stats: {
    "name": "Animal-api sync for customer NL_128277",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "24490",
        "ok": "24490",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "24490",
        "ok": "24490",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "24490",
        "ok": "24490",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "24490",
        "ok": "24490",
        "ko": "-"
    },
    "percentiles2": {
        "total": "24490",
        "ok": "24490",
        "ko": "-"
    },
    "percentiles3": {
        "total": "24490",
        "ok": "24490",
        "ko": "-"
    },
    "percentiles4": {
        "total": "24490",
        "ok": "24490",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-1d77d": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_109531",
path: "Animal-api sync for customer NL_109531",
pathFormatted: "req_animal-api-sync-1d77d",
stats: {
    "name": "Animal-api sync for customer NL_109531",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "6786",
        "ok": "6786",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "6786",
        "ok": "6786",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "6786",
        "ok": "6786",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "6786",
        "ok": "6786",
        "ko": "-"
    },
    "percentiles2": {
        "total": "6786",
        "ok": "6786",
        "ko": "-"
    },
    "percentiles3": {
        "total": "6786",
        "ok": "6786",
        "ko": "-"
    },
    "percentiles4": {
        "total": "6786",
        "ok": "6786",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e9ad2": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_121119",
path: "Animal-api sync for customer NL_121119",
pathFormatted: "req_animal-api-sync-e9ad2",
stats: {
    "name": "Animal-api sync for customer NL_121119",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "6618",
        "ok": "6618",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "6618",
        "ok": "6618",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "6618",
        "ok": "6618",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "6618",
        "ok": "6618",
        "ko": "-"
    },
    "percentiles2": {
        "total": "6618",
        "ok": "6618",
        "ko": "-"
    },
    "percentiles3": {
        "total": "6618",
        "ok": "6618",
        "ko": "-"
    },
    "percentiles4": {
        "total": "6618",
        "ok": "6618",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-86f14": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_121285",
path: "Animal-api sync for customer NL_121285",
pathFormatted: "req_animal-api-sync-86f14",
stats: {
    "name": "Animal-api sync for customer NL_121285",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "13458",
        "ok": "13458",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "13458",
        "ok": "13458",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "13458",
        "ok": "13458",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "13458",
        "ok": "13458",
        "ko": "-"
    },
    "percentiles2": {
        "total": "13458",
        "ok": "13458",
        "ko": "-"
    },
    "percentiles3": {
        "total": "13458",
        "ok": "13458",
        "ko": "-"
    },
    "percentiles4": {
        "total": "13458",
        "ok": "13458",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d5299": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104311",
path: "Animal-api sync for customer NL_104311",
pathFormatted: "req_animal-api-sync-d5299",
stats: {
    "name": "Animal-api sync for customer NL_104311",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4918",
        "ok": "4918",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4918",
        "ok": "4918",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4918",
        "ok": "4918",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4918",
        "ok": "4918",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4918",
        "ok": "4918",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4918",
        "ok": "4918",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4918",
        "ok": "4918",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-51e6e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117693",
path: "Animal-api sync for customer NL_117693",
pathFormatted: "req_animal-api-sync-51e6e",
stats: {
    "name": "Animal-api sync for customer NL_117693",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "11188",
        "ok": "11188",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "11188",
        "ok": "11188",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "11188",
        "ok": "11188",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "11188",
        "ok": "11188",
        "ko": "-"
    },
    "percentiles2": {
        "total": "11188",
        "ok": "11188",
        "ko": "-"
    },
    "percentiles3": {
        "total": "11188",
        "ok": "11188",
        "ko": "-"
    },
    "percentiles4": {
        "total": "11188",
        "ok": "11188",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-44547": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117813",
path: "Animal-api sync for customer NL_117813",
pathFormatted: "req_animal-api-sync-44547",
stats: {
    "name": "Animal-api sync for customer NL_117813",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "24442",
        "ok": "24442",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "24442",
        "ok": "24442",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "24442",
        "ok": "24442",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "24442",
        "ok": "24442",
        "ko": "-"
    },
    "percentiles2": {
        "total": "24442",
        "ok": "24442",
        "ko": "-"
    },
    "percentiles3": {
        "total": "24442",
        "ok": "24442",
        "ko": "-"
    },
    "percentiles4": {
        "total": "24442",
        "ok": "24442",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-76899": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_121301",
path: "Animal-api sync for customer NL_121301",
pathFormatted: "req_animal-api-sync-76899",
stats: {
    "name": "Animal-api sync for customer NL_121301",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "28981",
        "ok": "28981",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "28981",
        "ok": "28981",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "28981",
        "ok": "28981",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "28981",
        "ok": "28981",
        "ko": "-"
    },
    "percentiles2": {
        "total": "28981",
        "ok": "28981",
        "ko": "-"
    },
    "percentiles3": {
        "total": "28981",
        "ok": "28981",
        "ko": "-"
    },
    "percentiles4": {
        "total": "28981",
        "ok": "28981",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-857a0": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_127307",
path: "Animal-api sync for customer NL_127307",
pathFormatted: "req_animal-api-sync-857a0",
stats: {
    "name": "Animal-api sync for customer NL_127307",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1917",
        "ok": "1917",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1917",
        "ok": "1917",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1917",
        "ok": "1917",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1917",
        "ok": "1917",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1917",
        "ok": "1917",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1917",
        "ok": "1917",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1917",
        "ok": "1917",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-8b245": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_120606",
path: "Animal-api sync for customer NL_120606",
pathFormatted: "req_animal-api-sync-8b245",
stats: {
    "name": "Animal-api sync for customer NL_120606",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "8532",
        "ok": "8532",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "8532",
        "ok": "8532",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "8532",
        "ok": "8532",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "8532",
        "ok": "8532",
        "ko": "-"
    },
    "percentiles2": {
        "total": "8532",
        "ok": "8532",
        "ko": "-"
    },
    "percentiles3": {
        "total": "8532",
        "ok": "8532",
        "ko": "-"
    },
    "percentiles4": {
        "total": "8532",
        "ok": "8532",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-1ba07": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105320",
path: "Animal-api sync for customer NL_105320",
pathFormatted: "req_animal-api-sync-1ba07",
stats: {
    "name": "Animal-api sync for customer NL_105320",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3696",
        "ok": "3696",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3696",
        "ok": "3696",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3696",
        "ok": "3696",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3696",
        "ok": "3696",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3696",
        "ok": "3696",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3696",
        "ok": "3696",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3696",
        "ok": "3696",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-07d8c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_115607",
path: "Animal-api sync for customer NL_115607",
pathFormatted: "req_animal-api-sync-07d8c",
stats: {
    "name": "Animal-api sync for customer NL_115607",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "8796",
        "ok": "8796",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "8796",
        "ok": "8796",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "8796",
        "ok": "8796",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "8796",
        "ok": "8796",
        "ko": "-"
    },
    "percentiles2": {
        "total": "8796",
        "ok": "8796",
        "ko": "-"
    },
    "percentiles3": {
        "total": "8796",
        "ok": "8796",
        "ko": "-"
    },
    "percentiles4": {
        "total": "8796",
        "ok": "8796",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d643b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_129420",
path: "Animal-api sync for customer NL_129420",
pathFormatted: "req_animal-api-sync-d643b",
stats: {
    "name": "Animal-api sync for customer NL_129420",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1249",
        "ok": "1249",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1249",
        "ok": "1249",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1249",
        "ok": "1249",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1249",
        "ok": "1249",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1249",
        "ok": "1249",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1249",
        "ok": "1249",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1249",
        "ok": "1249",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-5b7d7": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_113415",
path: "Animal-api sync for customer NL_113415",
pathFormatted: "req_animal-api-sync-5b7d7",
stats: {
    "name": "Animal-api sync for customer NL_113415",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "6532",
        "ok": "6532",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "6532",
        "ok": "6532",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "6532",
        "ok": "6532",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "6532",
        "ok": "6532",
        "ko": "-"
    },
    "percentiles2": {
        "total": "6532",
        "ok": "6532",
        "ko": "-"
    },
    "percentiles3": {
        "total": "6532",
        "ok": "6532",
        "ko": "-"
    },
    "percentiles4": {
        "total": "6532",
        "ok": "6532",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-05129": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117717",
path: "Animal-api sync for customer NL_117717",
pathFormatted: "req_animal-api-sync-05129",
stats: {
    "name": "Animal-api sync for customer NL_117717",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4951",
        "ok": "4951",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4951",
        "ok": "4951",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4951",
        "ok": "4951",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4951",
        "ok": "4951",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4951",
        "ok": "4951",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4951",
        "ok": "4951",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4951",
        "ok": "4951",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a175e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_207835",
path: "Animal-api sync for customer NL_207835",
pathFormatted: "req_animal-api-sync-a175e",
stats: {
    "name": "Animal-api sync for customer NL_207835",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1293",
        "ok": "1293",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1293",
        "ok": "1293",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1293",
        "ok": "1293",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1293",
        "ok": "1293",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1293",
        "ok": "1293",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1293",
        "ok": "1293",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1293",
        "ok": "1293",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-3bf26": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_108699",
path: "Animal-api sync for customer NL_108699",
pathFormatted: "req_animal-api-sync-3bf26",
stats: {
    "name": "Animal-api sync for customer NL_108699",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1280",
        "ok": "1280",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1280",
        "ok": "1280",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1280",
        "ok": "1280",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1280",
        "ok": "1280",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1280",
        "ok": "1280",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1280",
        "ok": "1280",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1280",
        "ok": "1280",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-2b97a": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_161984",
path: "Animal-api sync for customer NL_161984",
pathFormatted: "req_animal-api-sync-2b97a",
stats: {
    "name": "Animal-api sync for customer NL_161984",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4819",
        "ok": "4819",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4819",
        "ok": "4819",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4819",
        "ok": "4819",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4819",
        "ok": "4819",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4819",
        "ok": "4819",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4819",
        "ok": "4819",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4819",
        "ok": "4819",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-5243f": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111939",
path: "Animal-api sync for customer NL_111939",
pathFormatted: "req_animal-api-sync-5243f",
stats: {
    "name": "Animal-api sync for customer NL_111939",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "30846",
        "ok": "30846",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "30846",
        "ok": "30846",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "30846",
        "ok": "30846",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "30846",
        "ok": "30846",
        "ko": "-"
    },
    "percentiles2": {
        "total": "30846",
        "ok": "30846",
        "ko": "-"
    },
    "percentiles3": {
        "total": "30846",
        "ok": "30846",
        "ko": "-"
    },
    "percentiles4": {
        "total": "30846",
        "ok": "30846",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b6219": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_144583",
path: "Animal-api sync for customer NL_144583",
pathFormatted: "req_animal-api-sync-b6219",
stats: {
    "name": "Animal-api sync for customer NL_144583",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "10777",
        "ok": "10777",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "10777",
        "ok": "10777",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "10777",
        "ok": "10777",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "10777",
        "ok": "10777",
        "ko": "-"
    },
    "percentiles2": {
        "total": "10777",
        "ok": "10777",
        "ko": "-"
    },
    "percentiles3": {
        "total": "10777",
        "ok": "10777",
        "ko": "-"
    },
    "percentiles4": {
        "total": "10777",
        "ok": "10777",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-43dcc": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_112405",
path: "Animal-api sync for customer NL_112405",
pathFormatted: "req_animal-api-sync-43dcc",
stats: {
    "name": "Animal-api sync for customer NL_112405",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "18036",
        "ok": "18036",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "18036",
        "ok": "18036",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "18036",
        "ok": "18036",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "18036",
        "ok": "18036",
        "ko": "-"
    },
    "percentiles2": {
        "total": "18036",
        "ok": "18036",
        "ko": "-"
    },
    "percentiles3": {
        "total": "18036",
        "ok": "18036",
        "ko": "-"
    },
    "percentiles4": {
        "total": "18036",
        "ok": "18036",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d545c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_131064",
path: "Animal-api sync for customer NL_131064",
pathFormatted: "req_animal-api-sync-d545c",
stats: {
    "name": "Animal-api sync for customer NL_131064",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5471",
        "ok": "5471",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5471",
        "ok": "5471",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5471",
        "ok": "5471",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5471",
        "ok": "5471",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5471",
        "ok": "5471",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5471",
        "ok": "5471",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5471",
        "ok": "5471",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-93205": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_120176",
path: "Animal-api sync for customer NL_120176",
pathFormatted: "req_animal-api-sync-93205",
stats: {
    "name": "Animal-api sync for customer NL_120176",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "6214",
        "ok": "6214",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "6214",
        "ok": "6214",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "6214",
        "ok": "6214",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "6214",
        "ok": "6214",
        "ko": "-"
    },
    "percentiles2": {
        "total": "6214",
        "ok": "6214",
        "ko": "-"
    },
    "percentiles3": {
        "total": "6214",
        "ok": "6214",
        "ko": "-"
    },
    "percentiles4": {
        "total": "6214",
        "ok": "6214",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-bf978": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_118830",
path: "Animal-api sync for customer NL_118830",
pathFormatted: "req_animal-api-sync-bf978",
stats: {
    "name": "Animal-api sync for customer NL_118830",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5654",
        "ok": "5654",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5654",
        "ok": "5654",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5654",
        "ok": "5654",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5654",
        "ok": "5654",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5654",
        "ok": "5654",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5654",
        "ok": "5654",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5654",
        "ok": "5654",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-28852": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_116904",
path: "Animal-api sync for customer NL_116904",
pathFormatted: "req_animal-api-sync-28852",
stats: {
    "name": "Animal-api sync for customer NL_116904",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "27725",
        "ok": "27725",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "27725",
        "ok": "27725",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "27725",
        "ok": "27725",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "27725",
        "ok": "27725",
        "ko": "-"
    },
    "percentiles2": {
        "total": "27725",
        "ok": "27725",
        "ko": "-"
    },
    "percentiles3": {
        "total": "27725",
        "ok": "27725",
        "ko": "-"
    },
    "percentiles4": {
        "total": "27725",
        "ok": "27725",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-3566a": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_107884",
path: "Animal-api sync for customer NL_107884",
pathFormatted: "req_animal-api-sync-3566a",
stats: {
    "name": "Animal-api sync for customer NL_107884",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "19759",
        "ok": "19759",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "19759",
        "ok": "19759",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "19759",
        "ok": "19759",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "19759",
        "ok": "19759",
        "ko": "-"
    },
    "percentiles2": {
        "total": "19759",
        "ok": "19759",
        "ko": "-"
    },
    "percentiles3": {
        "total": "19759",
        "ok": "19759",
        "ko": "-"
    },
    "percentiles4": {
        "total": "19759",
        "ok": "19759",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c24b5": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_122429",
path: "Animal-api sync for customer NL_122429",
pathFormatted: "req_animal-api-sync-c24b5",
stats: {
    "name": "Animal-api sync for customer NL_122429",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "18778",
        "ok": "18778",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "18778",
        "ok": "18778",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "18778",
        "ok": "18778",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "18778",
        "ok": "18778",
        "ko": "-"
    },
    "percentiles2": {
        "total": "18778",
        "ok": "18778",
        "ko": "-"
    },
    "percentiles3": {
        "total": "18778",
        "ok": "18778",
        "ko": "-"
    },
    "percentiles4": {
        "total": "18778",
        "ok": "18778",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-31f54": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_122305",
path: "Animal-api sync for customer NL_122305",
pathFormatted: "req_animal-api-sync-31f54",
stats: {
    "name": "Animal-api sync for customer NL_122305",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "7081",
        "ok": "7081",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "7081",
        "ok": "7081",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "7081",
        "ok": "7081",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "7081",
        "ok": "7081",
        "ko": "-"
    },
    "percentiles2": {
        "total": "7081",
        "ok": "7081",
        "ko": "-"
    },
    "percentiles3": {
        "total": "7081",
        "ok": "7081",
        "ko": "-"
    },
    "percentiles4": {
        "total": "7081",
        "ok": "7081",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-8ebc3": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_212864",
path: "Animal-api sync for customer BE_212864",
pathFormatted: "req_animal-api-sync-8ebc3",
stats: {
    "name": "Animal-api sync for customer BE_212864",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "11209",
        "ok": "11209",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "11209",
        "ok": "11209",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "11209",
        "ok": "11209",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "11209",
        "ok": "11209",
        "ko": "-"
    },
    "percentiles2": {
        "total": "11209",
        "ok": "11209",
        "ko": "-"
    },
    "percentiles3": {
        "total": "11209",
        "ok": "11209",
        "ko": "-"
    },
    "percentiles4": {
        "total": "11209",
        "ok": "11209",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a5160": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_156494",
path: "Animal-api sync for customer NL_156494",
pathFormatted: "req_animal-api-sync-a5160",
stats: {
    "name": "Animal-api sync for customer NL_156494",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "25113",
        "ok": "25113",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "25113",
        "ok": "25113",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "25113",
        "ok": "25113",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "25113",
        "ok": "25113",
        "ko": "-"
    },
    "percentiles2": {
        "total": "25113",
        "ok": "25113",
        "ko": "-"
    },
    "percentiles3": {
        "total": "25113",
        "ok": "25113",
        "ko": "-"
    },
    "percentiles4": {
        "total": "25113",
        "ok": "25113",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-34ba1": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_165365",
path: "Animal-api sync for customer BE_165365",
pathFormatted: "req_animal-api-sync-34ba1",
stats: {
    "name": "Animal-api sync for customer BE_165365",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "8011",
        "ok": "8011",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "8011",
        "ok": "8011",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "8011",
        "ok": "8011",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "8011",
        "ok": "8011",
        "ko": "-"
    },
    "percentiles2": {
        "total": "8011",
        "ok": "8011",
        "ko": "-"
    },
    "percentiles3": {
        "total": "8011",
        "ok": "8011",
        "ko": "-"
    },
    "percentiles4": {
        "total": "8011",
        "ok": "8011",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-9ec36": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_124747",
path: "Animal-api sync for customer NL_124747",
pathFormatted: "req_animal-api-sync-9ec36",
stats: {
    "name": "Animal-api sync for customer NL_124747",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "7029",
        "ok": "7029",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "7029",
        "ok": "7029",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "7029",
        "ok": "7029",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "7029",
        "ok": "7029",
        "ko": "-"
    },
    "percentiles2": {
        "total": "7029",
        "ok": "7029",
        "ko": "-"
    },
    "percentiles3": {
        "total": "7029",
        "ok": "7029",
        "ko": "-"
    },
    "percentiles4": {
        "total": "7029",
        "ok": "7029",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b9862": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_161770",
path: "Animal-api sync for customer NL_161770",
pathFormatted: "req_animal-api-sync-b9862",
stats: {
    "name": "Animal-api sync for customer NL_161770",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "28393",
        "ok": "28393",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "28393",
        "ok": "28393",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "28393",
        "ok": "28393",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "28393",
        "ok": "28393",
        "ko": "-"
    },
    "percentiles2": {
        "total": "28393",
        "ok": "28393",
        "ko": "-"
    },
    "percentiles3": {
        "total": "28393",
        "ok": "28393",
        "ko": "-"
    },
    "percentiles4": {
        "total": "28393",
        "ok": "28393",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b2272": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105223",
path: "Animal-api sync for customer NL_105223",
pathFormatted: "req_animal-api-sync-b2272",
stats: {
    "name": "Animal-api sync for customer NL_105223",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "6523",
        "ok": "6523",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "6523",
        "ok": "6523",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "6523",
        "ok": "6523",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "6523",
        "ok": "6523",
        "ko": "-"
    },
    "percentiles2": {
        "total": "6523",
        "ok": "6523",
        "ko": "-"
    },
    "percentiles3": {
        "total": "6523",
        "ok": "6523",
        "ko": "-"
    },
    "percentiles4": {
        "total": "6523",
        "ok": "6523",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-49a96": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_128642",
path: "Animal-api sync for customer NL_128642",
pathFormatted: "req_animal-api-sync-49a96",
stats: {
    "name": "Animal-api sync for customer NL_128642",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "15047",
        "ok": "15047",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "15047",
        "ok": "15047",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "15047",
        "ok": "15047",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "15047",
        "ok": "15047",
        "ko": "-"
    },
    "percentiles2": {
        "total": "15047",
        "ok": "15047",
        "ko": "-"
    },
    "percentiles3": {
        "total": "15047",
        "ok": "15047",
        "ko": "-"
    },
    "percentiles4": {
        "total": "15047",
        "ok": "15047",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-5c4eb": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_123093",
path: "Animal-api sync for customer NL_123093",
pathFormatted: "req_animal-api-sync-5c4eb",
stats: {
    "name": "Animal-api sync for customer NL_123093",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2037",
        "ok": "2037",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2037",
        "ok": "2037",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2037",
        "ok": "2037",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2037",
        "ok": "2037",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2037",
        "ok": "2037",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2037",
        "ok": "2037",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2037",
        "ok": "2037",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-90ee6": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105389",
path: "Animal-api sync for customer NL_105389",
pathFormatted: "req_animal-api-sync-90ee6",
stats: {
    "name": "Animal-api sync for customer NL_105389",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "12737",
        "ok": "12737",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "12737",
        "ok": "12737",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "12737",
        "ok": "12737",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "12737",
        "ok": "12737",
        "ko": "-"
    },
    "percentiles2": {
        "total": "12737",
        "ok": "12737",
        "ko": "-"
    },
    "percentiles3": {
        "total": "12737",
        "ok": "12737",
        "ko": "-"
    },
    "percentiles4": {
        "total": "12737",
        "ok": "12737",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-0908d": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105707",
path: "Animal-api sync for customer NL_105707",
pathFormatted: "req_animal-api-sync-0908d",
stats: {
    "name": "Animal-api sync for customer NL_105707",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5778",
        "ok": "5778",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5778",
        "ok": "5778",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5778",
        "ok": "5778",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5778",
        "ok": "5778",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5778",
        "ok": "5778",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5778",
        "ok": "5778",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5778",
        "ok": "5778",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-0a82c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_124910",
path: "Animal-api sync for customer NL_124910",
pathFormatted: "req_animal-api-sync-0a82c",
stats: {
    "name": "Animal-api sync for customer NL_124910",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1444",
        "ok": "1444",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1444",
        "ok": "1444",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1444",
        "ok": "1444",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1444",
        "ok": "1444",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1444",
        "ok": "1444",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1444",
        "ok": "1444",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1444",
        "ok": "1444",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d54bb": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_108024",
path: "Animal-api sync for customer NL_108024",
pathFormatted: "req_animal-api-sync-d54bb",
stats: {
    "name": "Animal-api sync for customer NL_108024",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "30419",
        "ok": "30419",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "30419",
        "ok": "30419",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "30419",
        "ok": "30419",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "30419",
        "ok": "30419",
        "ko": "-"
    },
    "percentiles2": {
        "total": "30419",
        "ok": "30419",
        "ko": "-"
    },
    "percentiles3": {
        "total": "30419",
        "ok": "30419",
        "ko": "-"
    },
    "percentiles4": {
        "total": "30419",
        "ok": "30419",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c0ea2": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_126187",
path: "Animal-api sync for customer NL_126187",
pathFormatted: "req_animal-api-sync-c0ea2",
stats: {
    "name": "Animal-api sync for customer NL_126187",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3942",
        "ok": "3942",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3942",
        "ok": "3942",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3942",
        "ok": "3942",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3942",
        "ok": "3942",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3942",
        "ok": "3942",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3942",
        "ok": "3942",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3942",
        "ok": "3942",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-7a1f1": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_151229",
path: "Animal-api sync for customer BE_151229",
pathFormatted: "req_animal-api-sync-7a1f1",
stats: {
    "name": "Animal-api sync for customer BE_151229",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1401",
        "ok": "1401",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1401",
        "ok": "1401",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1401",
        "ok": "1401",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1401",
        "ok": "1401",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1401",
        "ok": "1401",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1401",
        "ok": "1401",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1401",
        "ok": "1401",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-72904": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_103783",
path: "Animal-api sync for customer NL_103783",
pathFormatted: "req_animal-api-sync-72904",
stats: {
    "name": "Animal-api sync for customer NL_103783",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "24863",
        "ok": "24863",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "24863",
        "ok": "24863",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "24863",
        "ok": "24863",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "24863",
        "ok": "24863",
        "ko": "-"
    },
    "percentiles2": {
        "total": "24863",
        "ok": "24863",
        "ko": "-"
    },
    "percentiles3": {
        "total": "24863",
        "ok": "24863",
        "ko": "-"
    },
    "percentiles4": {
        "total": "24863",
        "ok": "24863",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-54314": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_103881",
path: "Animal-api sync for customer NL_103881",
pathFormatted: "req_animal-api-sync-54314",
stats: {
    "name": "Animal-api sync for customer NL_103881",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "11405",
        "ok": "11405",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "11405",
        "ok": "11405",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "11405",
        "ok": "11405",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "11405",
        "ok": "11405",
        "ko": "-"
    },
    "percentiles2": {
        "total": "11405",
        "ok": "11405",
        "ko": "-"
    },
    "percentiles3": {
        "total": "11405",
        "ok": "11405",
        "ko": "-"
    },
    "percentiles4": {
        "total": "11405",
        "ok": "11405",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-fb3ec": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_115893",
path: "Animal-api sync for customer NL_115893",
pathFormatted: "req_animal-api-sync-fb3ec",
stats: {
    "name": "Animal-api sync for customer NL_115893",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "10039",
        "ok": "10039",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "10039",
        "ok": "10039",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "10039",
        "ok": "10039",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "10039",
        "ok": "10039",
        "ko": "-"
    },
    "percentiles2": {
        "total": "10039",
        "ok": "10039",
        "ko": "-"
    },
    "percentiles3": {
        "total": "10039",
        "ok": "10039",
        "ko": "-"
    },
    "percentiles4": {
        "total": "10039",
        "ok": "10039",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d90a9": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_119649",
path: "Animal-api sync for customer NL_119649",
pathFormatted: "req_animal-api-sync-d90a9",
stats: {
    "name": "Animal-api sync for customer NL_119649",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2740",
        "ok": "2740",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2740",
        "ok": "2740",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2740",
        "ok": "2740",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2740",
        "ok": "2740",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2740",
        "ok": "2740",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2740",
        "ok": "2740",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2740",
        "ok": "2740",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c4ec4": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104910",
path: "Animal-api sync for customer NL_104910",
pathFormatted: "req_animal-api-sync-c4ec4",
stats: {
    "name": "Animal-api sync for customer NL_104910",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3917",
        "ok": "3917",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3917",
        "ok": "3917",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3917",
        "ok": "3917",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3917",
        "ok": "3917",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3917",
        "ok": "3917",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3917",
        "ok": "3917",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3917",
        "ok": "3917",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e498b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110382",
path: "Animal-api sync for customer NL_110382",
pathFormatted: "req_animal-api-sync-e498b",
stats: {
    "name": "Animal-api sync for customer NL_110382",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5798",
        "ok": "5798",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5798",
        "ok": "5798",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5798",
        "ok": "5798",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5798",
        "ok": "5798",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5798",
        "ok": "5798",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5798",
        "ok": "5798",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5798",
        "ok": "5798",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-2a4dd": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_125297",
path: "Animal-api sync for customer NL_125297",
pathFormatted: "req_animal-api-sync-2a4dd",
stats: {
    "name": "Animal-api sync for customer NL_125297",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4056",
        "ok": "4056",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4056",
        "ok": "4056",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4056",
        "ok": "4056",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4056",
        "ok": "4056",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4056",
        "ok": "4056",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4056",
        "ok": "4056",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4056",
        "ok": "4056",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c1c6a": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_135207",
path: "Animal-api sync for customer NL_135207",
pathFormatted: "req_animal-api-sync-c1c6a",
stats: {
    "name": "Animal-api sync for customer NL_135207",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4414",
        "ok": "4414",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4414",
        "ok": "4414",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4414",
        "ok": "4414",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4414",
        "ok": "4414",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4414",
        "ok": "4414",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4414",
        "ok": "4414",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4414",
        "ok": "4414",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c170f": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_109428",
path: "Animal-api sync for customer NL_109428",
pathFormatted: "req_animal-api-sync-c170f",
stats: {
    "name": "Animal-api sync for customer NL_109428",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "17214",
        "ok": "17214",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "17214",
        "ok": "17214",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "17214",
        "ok": "17214",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "17214",
        "ok": "17214",
        "ko": "-"
    },
    "percentiles2": {
        "total": "17214",
        "ok": "17214",
        "ko": "-"
    },
    "percentiles3": {
        "total": "17214",
        "ok": "17214",
        "ko": "-"
    },
    "percentiles4": {
        "total": "17214",
        "ok": "17214",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-690f7": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_129681",
path: "Animal-api sync for customer NL_129681",
pathFormatted: "req_animal-api-sync-690f7",
stats: {
    "name": "Animal-api sync for customer NL_129681",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2906",
        "ok": "2906",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2906",
        "ok": "2906",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2906",
        "ok": "2906",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2906",
        "ok": "2906",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2906",
        "ok": "2906",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2906",
        "ok": "2906",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2906",
        "ok": "2906",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-de0b1": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_164131",
path: "Animal-api sync for customer NL_164131",
pathFormatted: "req_animal-api-sync-de0b1",
stats: {
    "name": "Animal-api sync for customer NL_164131",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "17954",
        "ok": "17954",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "17954",
        "ok": "17954",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "17954",
        "ok": "17954",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "17954",
        "ok": "17954",
        "ko": "-"
    },
    "percentiles2": {
        "total": "17954",
        "ok": "17954",
        "ko": "-"
    },
    "percentiles3": {
        "total": "17954",
        "ok": "17954",
        "ko": "-"
    },
    "percentiles4": {
        "total": "17954",
        "ok": "17954",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-3e31b": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_149431",
path: "Animal-api sync for customer BE_149431",
pathFormatted: "req_animal-api-sync-3e31b",
stats: {
    "name": "Animal-api sync for customer BE_149431",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2082",
        "ok": "2082",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2082",
        "ok": "2082",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2082",
        "ok": "2082",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2082",
        "ok": "2082",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2082",
        "ok": "2082",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2082",
        "ok": "2082",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2082",
        "ok": "2082",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-cb71c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_120709",
path: "Animal-api sync for customer NL_120709",
pathFormatted: "req_animal-api-sync-cb71c",
stats: {
    "name": "Animal-api sync for customer NL_120709",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "23256",
        "ok": "23256",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "23256",
        "ok": "23256",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "23256",
        "ok": "23256",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "23256",
        "ok": "23256",
        "ko": "-"
    },
    "percentiles2": {
        "total": "23256",
        "ok": "23256",
        "ko": "-"
    },
    "percentiles3": {
        "total": "23256",
        "ok": "23256",
        "ko": "-"
    },
    "percentiles4": {
        "total": "23256",
        "ok": "23256",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-5a7e6": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105010",
path: "Animal-api sync for customer NL_105010",
pathFormatted: "req_animal-api-sync-5a7e6",
stats: {
    "name": "Animal-api sync for customer NL_105010",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "9052",
        "ok": "9052",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "9052",
        "ok": "9052",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "9052",
        "ok": "9052",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "9052",
        "ok": "9052",
        "ko": "-"
    },
    "percentiles2": {
        "total": "9052",
        "ok": "9052",
        "ko": "-"
    },
    "percentiles3": {
        "total": "9052",
        "ok": "9052",
        "ko": "-"
    },
    "percentiles4": {
        "total": "9052",
        "ok": "9052",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-0eddf": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110820",
path: "Animal-api sync for customer NL_110820",
pathFormatted: "req_animal-api-sync-0eddf",
stats: {
    "name": "Animal-api sync for customer NL_110820",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "10068",
        "ok": "10068",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "10068",
        "ok": "10068",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "10068",
        "ok": "10068",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "10068",
        "ok": "10068",
        "ko": "-"
    },
    "percentiles2": {
        "total": "10068",
        "ok": "10068",
        "ko": "-"
    },
    "percentiles3": {
        "total": "10068",
        "ok": "10068",
        "ko": "-"
    },
    "percentiles4": {
        "total": "10068",
        "ok": "10068",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-558a2": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_116270",
path: "Animal-api sync for customer NL_116270",
pathFormatted: "req_animal-api-sync-558a2",
stats: {
    "name": "Animal-api sync for customer NL_116270",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4249",
        "ok": "4249",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4249",
        "ok": "4249",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4249",
        "ok": "4249",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4249",
        "ok": "4249",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4249",
        "ok": "4249",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4249",
        "ok": "4249",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4249",
        "ok": "4249",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-3833c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_124824",
path: "Animal-api sync for customer NL_124824",
pathFormatted: "req_animal-api-sync-3833c",
stats: {
    "name": "Animal-api sync for customer NL_124824",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2947",
        "ok": "2947",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2947",
        "ok": "2947",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2947",
        "ok": "2947",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2947",
        "ok": "2947",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2947",
        "ok": "2947",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2947",
        "ok": "2947",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2947",
        "ok": "2947",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a3967": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117770",
path: "Animal-api sync for customer NL_117770",
pathFormatted: "req_animal-api-sync-a3967",
stats: {
    "name": "Animal-api sync for customer NL_117770",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "6043",
        "ok": "6043",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "6043",
        "ok": "6043",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "6043",
        "ok": "6043",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "6043",
        "ok": "6043",
        "ko": "-"
    },
    "percentiles2": {
        "total": "6043",
        "ok": "6043",
        "ko": "-"
    },
    "percentiles3": {
        "total": "6043",
        "ok": "6043",
        "ko": "-"
    },
    "percentiles4": {
        "total": "6043",
        "ok": "6043",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-5a613": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_194583",
path: "Animal-api sync for customer NL_194583",
pathFormatted: "req_animal-api-sync-5a613",
stats: {
    "name": "Animal-api sync for customer NL_194583",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "6911",
        "ok": "6911",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "6911",
        "ok": "6911",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "6911",
        "ok": "6911",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "6911",
        "ok": "6911",
        "ko": "-"
    },
    "percentiles2": {
        "total": "6911",
        "ok": "6911",
        "ko": "-"
    },
    "percentiles3": {
        "total": "6911",
        "ok": "6911",
        "ko": "-"
    },
    "percentiles4": {
        "total": "6911",
        "ok": "6911",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c7c5d": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_129379",
path: "Animal-api sync for customer NL_129379",
pathFormatted: "req_animal-api-sync-c7c5d",
stats: {
    "name": "Animal-api sync for customer NL_129379",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "6592",
        "ok": "6592",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "6592",
        "ok": "6592",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "6592",
        "ok": "6592",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "6592",
        "ok": "6592",
        "ko": "-"
    },
    "percentiles2": {
        "total": "6592",
        "ok": "6592",
        "ko": "-"
    },
    "percentiles3": {
        "total": "6592",
        "ok": "6592",
        "ko": "-"
    },
    "percentiles4": {
        "total": "6592",
        "ok": "6592",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-91ebb": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_120778",
path: "Animal-api sync for customer NL_120778",
pathFormatted: "req_animal-api-sync-91ebb",
stats: {
    "name": "Animal-api sync for customer NL_120778",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2239",
        "ok": "2239",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2239",
        "ok": "2239",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2239",
        "ok": "2239",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2239",
        "ok": "2239",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2239",
        "ok": "2239",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2239",
        "ok": "2239",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2239",
        "ok": "2239",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-6d45c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_123012",
path: "Animal-api sync for customer NL_123012",
pathFormatted: "req_animal-api-sync-6d45c",
stats: {
    "name": "Animal-api sync for customer NL_123012",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3090",
        "ok": "3090",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3090",
        "ok": "3090",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3090",
        "ok": "3090",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3090",
        "ok": "3090",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3090",
        "ok": "3090",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3090",
        "ok": "3090",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3090",
        "ok": "3090",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-3ff93": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111287",
path: "Animal-api sync for customer NL_111287",
pathFormatted: "req_animal-api-sync-3ff93",
stats: {
    "name": "Animal-api sync for customer NL_111287",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "8314",
        "ok": "8314",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "8314",
        "ok": "8314",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "8314",
        "ok": "8314",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "8314",
        "ok": "8314",
        "ko": "-"
    },
    "percentiles2": {
        "total": "8314",
        "ok": "8314",
        "ko": "-"
    },
    "percentiles3": {
        "total": "8314",
        "ok": "8314",
        "ko": "-"
    },
    "percentiles4": {
        "total": "8314",
        "ok": "8314",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-968ea": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_152278",
path: "Animal-api sync for customer BE_152278",
pathFormatted: "req_animal-api-sync-968ea",
stats: {
    "name": "Animal-api sync for customer BE_152278",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3565",
        "ok": "3565",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3565",
        "ok": "3565",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3565",
        "ok": "3565",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3565",
        "ok": "3565",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3565",
        "ok": "3565",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3565",
        "ok": "3565",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3565",
        "ok": "3565",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b979d": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_134839",
path: "Animal-api sync for customer NL_134839",
pathFormatted: "req_animal-api-sync-b979d",
stats: {
    "name": "Animal-api sync for customer NL_134839",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3298",
        "ok": "3298",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3298",
        "ok": "3298",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3298",
        "ok": "3298",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3298",
        "ok": "3298",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3298",
        "ok": "3298",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3298",
        "ok": "3298",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3298",
        "ok": "3298",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-4a2f0": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_119479",
path: "Animal-api sync for customer NL_119479",
pathFormatted: "req_animal-api-sync-4a2f0",
stats: {
    "name": "Animal-api sync for customer NL_119479",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3460",
        "ok": "3460",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3460",
        "ok": "3460",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3460",
        "ok": "3460",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3460",
        "ok": "3460",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3460",
        "ok": "3460",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3460",
        "ok": "3460",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3460",
        "ok": "3460",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-04f19": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_142633",
path: "Animal-api sync for customer NL_142633",
pathFormatted: "req_animal-api-sync-04f19",
stats: {
    "name": "Animal-api sync for customer NL_142633",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "23185",
        "ok": "23185",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "23185",
        "ok": "23185",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "23185",
        "ok": "23185",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "23185",
        "ok": "23185",
        "ko": "-"
    },
    "percentiles2": {
        "total": "23185",
        "ok": "23185",
        "ko": "-"
    },
    "percentiles3": {
        "total": "23185",
        "ok": "23185",
        "ko": "-"
    },
    "percentiles4": {
        "total": "23185",
        "ok": "23185",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c6934": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_144844",
path: "Animal-api sync for customer BE_144844",
pathFormatted: "req_animal-api-sync-c6934",
stats: {
    "name": "Animal-api sync for customer BE_144844",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5101",
        "ok": "5101",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5101",
        "ok": "5101",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5101",
        "ok": "5101",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5101",
        "ok": "5101",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5101",
        "ok": "5101",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5101",
        "ok": "5101",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5101",
        "ok": "5101",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-ee05d": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_106934",
path: "Animal-api sync for customer NL_106934",
pathFormatted: "req_animal-api-sync-ee05d",
stats: {
    "name": "Animal-api sync for customer NL_106934",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5423",
        "ok": "5423",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5423",
        "ok": "5423",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5423",
        "ok": "5423",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5423",
        "ok": "5423",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5423",
        "ok": "5423",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5423",
        "ok": "5423",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5423",
        "ok": "5423",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-26bf9": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110058",
path: "Animal-api sync for customer NL_110058",
pathFormatted: "req_animal-api-sync-26bf9",
stats: {
    "name": "Animal-api sync for customer NL_110058",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5167",
        "ok": "5167",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5167",
        "ok": "5167",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5167",
        "ok": "5167",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5167",
        "ok": "5167",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5167",
        "ok": "5167",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5167",
        "ok": "5167",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5167",
        "ok": "5167",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a42d0": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105360",
path: "Animal-api sync for customer NL_105360",
pathFormatted: "req_animal-api-sync-a42d0",
stats: {
    "name": "Animal-api sync for customer NL_105360",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "14876",
        "ok": "14876",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "14876",
        "ok": "14876",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "14876",
        "ok": "14876",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "14876",
        "ok": "14876",
        "ko": "-"
    },
    "percentiles2": {
        "total": "14876",
        "ok": "14876",
        "ko": "-"
    },
    "percentiles3": {
        "total": "14876",
        "ok": "14876",
        "ko": "-"
    },
    "percentiles4": {
        "total": "14876",
        "ok": "14876",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-35753": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_118795",
path: "Animal-api sync for customer NL_118795",
pathFormatted: "req_animal-api-sync-35753",
stats: {
    "name": "Animal-api sync for customer NL_118795",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "6760",
        "ok": "6760",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "6760",
        "ok": "6760",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "6760",
        "ok": "6760",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "6760",
        "ok": "6760",
        "ko": "-"
    },
    "percentiles2": {
        "total": "6760",
        "ok": "6760",
        "ko": "-"
    },
    "percentiles3": {
        "total": "6760",
        "ok": "6760",
        "ko": "-"
    },
    "percentiles4": {
        "total": "6760",
        "ok": "6760",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-09a8d": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117444",
path: "Animal-api sync for customer NL_117444",
pathFormatted: "req_animal-api-sync-09a8d",
stats: {
    "name": "Animal-api sync for customer NL_117444",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3112",
        "ok": "3112",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3112",
        "ok": "3112",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3112",
        "ok": "3112",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3112",
        "ok": "3112",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3112",
        "ok": "3112",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3112",
        "ok": "3112",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3112",
        "ok": "3112",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-7c5cb": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_118794",
path: "Animal-api sync for customer NL_118794",
pathFormatted: "req_animal-api-sync-7c5cb",
stats: {
    "name": "Animal-api sync for customer NL_118794",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3427",
        "ok": "3427",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3427",
        "ok": "3427",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3427",
        "ok": "3427",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3427",
        "ok": "3427",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3427",
        "ok": "3427",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3427",
        "ok": "3427",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3427",
        "ok": "3427",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e202d": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_112066",
path: "Animal-api sync for customer NL_112066",
pathFormatted: "req_animal-api-sync-e202d",
stats: {
    "name": "Animal-api sync for customer NL_112066",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4257",
        "ok": "4257",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4257",
        "ok": "4257",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4257",
        "ok": "4257",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4257",
        "ok": "4257",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4257",
        "ok": "4257",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4257",
        "ok": "4257",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4257",
        "ok": "4257",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-be37d": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_106236",
path: "Animal-api sync for customer NL_106236",
pathFormatted: "req_animal-api-sync-be37d",
stats: {
    "name": "Animal-api sync for customer NL_106236",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5027",
        "ok": "5027",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5027",
        "ok": "5027",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5027",
        "ok": "5027",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5027",
        "ok": "5027",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5027",
        "ok": "5027",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5027",
        "ok": "5027",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5027",
        "ok": "5027",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-ca7e3": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_134297",
path: "Animal-api sync for customer NL_134297",
pathFormatted: "req_animal-api-sync-ca7e3",
stats: {
    "name": "Animal-api sync for customer NL_134297",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4768",
        "ok": "4768",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4768",
        "ok": "4768",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4768",
        "ok": "4768",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4768",
        "ok": "4768",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4768",
        "ok": "4768",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4768",
        "ok": "4768",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4768",
        "ok": "4768",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-993d0": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_160647",
path: "Animal-api sync for customer NL_160647",
pathFormatted: "req_animal-api-sync-993d0",
stats: {
    "name": "Animal-api sync for customer NL_160647",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21863",
        "ok": "21863",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "21863",
        "ok": "21863",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "21863",
        "ok": "21863",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "21863",
        "ok": "21863",
        "ko": "-"
    },
    "percentiles2": {
        "total": "21863",
        "ok": "21863",
        "ko": "-"
    },
    "percentiles3": {
        "total": "21863",
        "ok": "21863",
        "ko": "-"
    },
    "percentiles4": {
        "total": "21863",
        "ok": "21863",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-1e171": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111917",
path: "Animal-api sync for customer NL_111917",
pathFormatted: "req_animal-api-sync-1e171",
stats: {
    "name": "Animal-api sync for customer NL_111917",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "18884",
        "ok": "18884",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "18884",
        "ok": "18884",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "18884",
        "ok": "18884",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "18884",
        "ok": "18884",
        "ko": "-"
    },
    "percentiles2": {
        "total": "18884",
        "ok": "18884",
        "ko": "-"
    },
    "percentiles3": {
        "total": "18884",
        "ok": "18884",
        "ko": "-"
    },
    "percentiles4": {
        "total": "18884",
        "ok": "18884",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-5e0b3": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_106214",
path: "Animal-api sync for customer NL_106214",
pathFormatted: "req_animal-api-sync-5e0b3",
stats: {
    "name": "Animal-api sync for customer NL_106214",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3068",
        "ok": "3068",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3068",
        "ok": "3068",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3068",
        "ok": "3068",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3068",
        "ok": "3068",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3068",
        "ok": "3068",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3068",
        "ok": "3068",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3068",
        "ok": "3068",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a7313": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_123953",
path: "Animal-api sync for customer NL_123953",
pathFormatted: "req_animal-api-sync-a7313",
stats: {
    "name": "Animal-api sync for customer NL_123953",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "28499",
        "ok": "28499",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "28499",
        "ok": "28499",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "28499",
        "ok": "28499",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "28499",
        "ok": "28499",
        "ko": "-"
    },
    "percentiles2": {
        "total": "28499",
        "ok": "28499",
        "ko": "-"
    },
    "percentiles3": {
        "total": "28499",
        "ok": "28499",
        "ko": "-"
    },
    "percentiles4": {
        "total": "28499",
        "ok": "28499",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-7cf01": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104284",
path: "Animal-api sync for customer NL_104284",
pathFormatted: "req_animal-api-sync-7cf01",
stats: {
    "name": "Animal-api sync for customer NL_104284",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3453",
        "ok": "3453",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3453",
        "ok": "3453",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3453",
        "ok": "3453",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3453",
        "ok": "3453",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3453",
        "ok": "3453",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3453",
        "ok": "3453",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3453",
        "ok": "3453",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-588aa": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_214938",
path: "Animal-api sync for customer NL_214938",
pathFormatted: "req_animal-api-sync-588aa",
stats: {
    "name": "Animal-api sync for customer NL_214938",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "6416",
        "ok": "6416",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "6416",
        "ok": "6416",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "6416",
        "ok": "6416",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "6416",
        "ok": "6416",
        "ko": "-"
    },
    "percentiles2": {
        "total": "6416",
        "ok": "6416",
        "ko": "-"
    },
    "percentiles3": {
        "total": "6416",
        "ok": "6416",
        "ko": "-"
    },
    "percentiles4": {
        "total": "6416",
        "ok": "6416",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-6ebe1": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104335",
path: "Animal-api sync for customer NL_104335",
pathFormatted: "req_animal-api-sync-6ebe1",
stats: {
    "name": "Animal-api sync for customer NL_104335",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "23062",
        "ok": "23062",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "23062",
        "ok": "23062",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "23062",
        "ok": "23062",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "23062",
        "ok": "23062",
        "ko": "-"
    },
    "percentiles2": {
        "total": "23062",
        "ok": "23062",
        "ko": "-"
    },
    "percentiles3": {
        "total": "23062",
        "ok": "23062",
        "ko": "-"
    },
    "percentiles4": {
        "total": "23062",
        "ok": "23062",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d86cb": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105153",
path: "Animal-api sync for customer NL_105153",
pathFormatted: "req_animal-api-sync-d86cb",
stats: {
    "name": "Animal-api sync for customer NL_105153",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4090",
        "ok": "4090",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4090",
        "ok": "4090",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4090",
        "ok": "4090",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4090",
        "ok": "4090",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4090",
        "ok": "4090",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4090",
        "ok": "4090",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4090",
        "ok": "4090",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b6645": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_126020",
path: "Animal-api sync for customer NL_126020",
pathFormatted: "req_animal-api-sync-b6645",
stats: {
    "name": "Animal-api sync for customer NL_126020",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "10880",
        "ok": "10880",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "10880",
        "ok": "10880",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "10880",
        "ok": "10880",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "10880",
        "ok": "10880",
        "ko": "-"
    },
    "percentiles2": {
        "total": "10880",
        "ok": "10880",
        "ko": "-"
    },
    "percentiles3": {
        "total": "10880",
        "ok": "10880",
        "ko": "-"
    },
    "percentiles4": {
        "total": "10880",
        "ok": "10880",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-7ae4d": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_137252",
path: "Animal-api sync for customer NL_137252",
pathFormatted: "req_animal-api-sync-7ae4d",
stats: {
    "name": "Animal-api sync for customer NL_137252",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3426",
        "ok": "3426",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3426",
        "ok": "3426",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3426",
        "ok": "3426",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3426",
        "ok": "3426",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3426",
        "ok": "3426",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3426",
        "ok": "3426",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3426",
        "ok": "3426",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-f2327": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_107409",
path: "Animal-api sync for customer NL_107409",
pathFormatted: "req_animal-api-sync-f2327",
stats: {
    "name": "Animal-api sync for customer NL_107409",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "12172",
        "ok": "12172",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "12172",
        "ok": "12172",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "12172",
        "ok": "12172",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "12172",
        "ok": "12172",
        "ko": "-"
    },
    "percentiles2": {
        "total": "12172",
        "ok": "12172",
        "ko": "-"
    },
    "percentiles3": {
        "total": "12172",
        "ok": "12172",
        "ko": "-"
    },
    "percentiles4": {
        "total": "12172",
        "ok": "12172",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-9b90a": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_114569",
path: "Animal-api sync for customer NL_114569",
pathFormatted: "req_animal-api-sync-9b90a",
stats: {
    "name": "Animal-api sync for customer NL_114569",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3343",
        "ok": "3343",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3343",
        "ok": "3343",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3343",
        "ok": "3343",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3343",
        "ok": "3343",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3343",
        "ok": "3343",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3343",
        "ok": "3343",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3343",
        "ok": "3343",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-56e97": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_130716",
path: "Animal-api sync for customer NL_130716",
pathFormatted: "req_animal-api-sync-56e97",
stats: {
    "name": "Animal-api sync for customer NL_130716",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4376",
        "ok": "4376",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4376",
        "ok": "4376",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4376",
        "ok": "4376",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4376",
        "ok": "4376",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4376",
        "ok": "4376",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4376",
        "ok": "4376",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4376",
        "ok": "4376",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-4b1c0": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_164635",
path: "Animal-api sync for customer BE_164635",
pathFormatted: "req_animal-api-sync-4b1c0",
stats: {
    "name": "Animal-api sync for customer BE_164635",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3085",
        "ok": "3085",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3085",
        "ok": "3085",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3085",
        "ok": "3085",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3085",
        "ok": "3085",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3085",
        "ok": "3085",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3085",
        "ok": "3085",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3085",
        "ok": "3085",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-0be59": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_137530",
path: "Animal-api sync for customer NL_137530",
pathFormatted: "req_animal-api-sync-0be59",
stats: {
    "name": "Animal-api sync for customer NL_137530",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3089",
        "ok": "3089",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3089",
        "ok": "3089",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3089",
        "ok": "3089",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3089",
        "ok": "3089",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3089",
        "ok": "3089",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3089",
        "ok": "3089",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3089",
        "ok": "3089",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e4170": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_205597",
path: "Animal-api sync for customer BE_205597",
pathFormatted: "req_animal-api-sync-e4170",
stats: {
    "name": "Animal-api sync for customer BE_205597",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4598",
        "ok": "4598",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4598",
        "ok": "4598",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4598",
        "ok": "4598",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4598",
        "ok": "4598",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4598",
        "ok": "4598",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4598",
        "ok": "4598",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4598",
        "ok": "4598",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-5e066": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_118716",
path: "Animal-api sync for customer NL_118716",
pathFormatted: "req_animal-api-sync-5e066",
stats: {
    "name": "Animal-api sync for customer NL_118716",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4235",
        "ok": "4235",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4235",
        "ok": "4235",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4235",
        "ok": "4235",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4235",
        "ok": "4235",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4235",
        "ok": "4235",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4235",
        "ok": "4235",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4235",
        "ok": "4235",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-3598c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_107584",
path: "Animal-api sync for customer NL_107584",
pathFormatted: "req_animal-api-sync-3598c",
stats: {
    "name": "Animal-api sync for customer NL_107584",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5212",
        "ok": "5212",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5212",
        "ok": "5212",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5212",
        "ok": "5212",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5212",
        "ok": "5212",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5212",
        "ok": "5212",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5212",
        "ok": "5212",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5212",
        "ok": "5212",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-6b068": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111950",
path: "Animal-api sync for customer NL_111950",
pathFormatted: "req_animal-api-sync-6b068",
stats: {
    "name": "Animal-api sync for customer NL_111950",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5175",
        "ok": "5175",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5175",
        "ok": "5175",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5175",
        "ok": "5175",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5175",
        "ok": "5175",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5175",
        "ok": "5175",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5175",
        "ok": "5175",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5175",
        "ok": "5175",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-34e6f": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_129959",
path: "Animal-api sync for customer NL_129959",
pathFormatted: "req_animal-api-sync-34e6f",
stats: {
    "name": "Animal-api sync for customer NL_129959",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4754",
        "ok": "4754",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4754",
        "ok": "4754",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4754",
        "ok": "4754",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4754",
        "ok": "4754",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4754",
        "ok": "4754",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4754",
        "ok": "4754",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4754",
        "ok": "4754",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-21b03": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_109721",
path: "Animal-api sync for customer NL_109721",
pathFormatted: "req_animal-api-sync-21b03",
stats: {
    "name": "Animal-api sync for customer NL_109721",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "8687",
        "ok": "8687",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "8687",
        "ok": "8687",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "8687",
        "ok": "8687",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "8687",
        "ok": "8687",
        "ko": "-"
    },
    "percentiles2": {
        "total": "8687",
        "ok": "8687",
        "ko": "-"
    },
    "percentiles3": {
        "total": "8687",
        "ok": "8687",
        "ko": "-"
    },
    "percentiles4": {
        "total": "8687",
        "ok": "8687",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-24d15": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117331",
path: "Animal-api sync for customer NL_117331",
pathFormatted: "req_animal-api-sync-24d15",
stats: {
    "name": "Animal-api sync for customer NL_117331",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5114",
        "ok": "5114",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5114",
        "ok": "5114",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5114",
        "ok": "5114",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5114",
        "ok": "5114",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5114",
        "ok": "5114",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5114",
        "ok": "5114",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5114",
        "ok": "5114",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-4479f": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_134641",
path: "Animal-api sync for customer NL_134641",
pathFormatted: "req_animal-api-sync-4479f",
stats: {
    "name": "Animal-api sync for customer NL_134641",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1326",
        "ok": "1326",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1326",
        "ok": "1326",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1326",
        "ok": "1326",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1326",
        "ok": "1326",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1326",
        "ok": "1326",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1326",
        "ok": "1326",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1326",
        "ok": "1326",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-2ac98": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_119983",
path: "Animal-api sync for customer NL_119983",
pathFormatted: "req_animal-api-sync-2ac98",
stats: {
    "name": "Animal-api sync for customer NL_119983",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "24909",
        "ok": "24909",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "24909",
        "ok": "24909",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "24909",
        "ok": "24909",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "24909",
        "ok": "24909",
        "ko": "-"
    },
    "percentiles2": {
        "total": "24909",
        "ok": "24909",
        "ko": "-"
    },
    "percentiles3": {
        "total": "24909",
        "ok": "24909",
        "ko": "-"
    },
    "percentiles4": {
        "total": "24909",
        "ok": "24909",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-53e53": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_122724",
path: "Animal-api sync for customer NL_122724",
pathFormatted: "req_animal-api-sync-53e53",
stats: {
    "name": "Animal-api sync for customer NL_122724",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "6177",
        "ok": "6177",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "6177",
        "ok": "6177",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "6177",
        "ok": "6177",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "6177",
        "ok": "6177",
        "ko": "-"
    },
    "percentiles2": {
        "total": "6177",
        "ok": "6177",
        "ko": "-"
    },
    "percentiles3": {
        "total": "6177",
        "ok": "6177",
        "ko": "-"
    },
    "percentiles4": {
        "total": "6177",
        "ok": "6177",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-97824": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_129903",
path: "Animal-api sync for customer NL_129903",
pathFormatted: "req_animal-api-sync-97824",
stats: {
    "name": "Animal-api sync for customer NL_129903",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5623",
        "ok": "5623",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5623",
        "ok": "5623",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5623",
        "ok": "5623",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5623",
        "ok": "5623",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5623",
        "ok": "5623",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5623",
        "ok": "5623",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5623",
        "ok": "5623",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-9db7a": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_125745",
path: "Animal-api sync for customer NL_125745",
pathFormatted: "req_animal-api-sync-9db7a",
stats: {
    "name": "Animal-api sync for customer NL_125745",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "13037",
        "ok": "13037",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "13037",
        "ok": "13037",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "13037",
        "ok": "13037",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "13037",
        "ok": "13037",
        "ko": "-"
    },
    "percentiles2": {
        "total": "13037",
        "ok": "13037",
        "ko": "-"
    },
    "percentiles3": {
        "total": "13037",
        "ok": "13037",
        "ko": "-"
    },
    "percentiles4": {
        "total": "13037",
        "ok": "13037",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-62afe": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_128808",
path: "Animal-api sync for customer NL_128808",
pathFormatted: "req_animal-api-sync-62afe",
stats: {
    "name": "Animal-api sync for customer NL_128808",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4140",
        "ok": "4140",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4140",
        "ok": "4140",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4140",
        "ok": "4140",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4140",
        "ok": "4140",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4140",
        "ok": "4140",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4140",
        "ok": "4140",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4140",
        "ok": "4140",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d8b1c": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_214192",
path: "Animal-api sync for customer BE_214192",
pathFormatted: "req_animal-api-sync-d8b1c",
stats: {
    "name": "Animal-api sync for customer BE_214192",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1579",
        "ok": "1579",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1579",
        "ok": "1579",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1579",
        "ok": "1579",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1579",
        "ok": "1579",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1579",
        "ok": "1579",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1579",
        "ok": "1579",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1579",
        "ok": "1579",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-22fcc": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110755",
path: "Animal-api sync for customer NL_110755",
pathFormatted: "req_animal-api-sync-22fcc",
stats: {
    "name": "Animal-api sync for customer NL_110755",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4670",
        "ok": "4670",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4670",
        "ok": "4670",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4670",
        "ok": "4670",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4670",
        "ok": "4670",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4670",
        "ok": "4670",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4670",
        "ok": "4670",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4670",
        "ok": "4670",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-1330d": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_145902",
path: "Animal-api sync for customer BE_145902",
pathFormatted: "req_animal-api-sync-1330d",
stats: {
    "name": "Animal-api sync for customer BE_145902",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2972",
        "ok": "2972",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2972",
        "ok": "2972",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2972",
        "ok": "2972",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2972",
        "ok": "2972",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2972",
        "ok": "2972",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2972",
        "ok": "2972",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2972",
        "ok": "2972",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-2cece": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110572",
path: "Animal-api sync for customer NL_110572",
pathFormatted: "req_animal-api-sync-2cece",
stats: {
    "name": "Animal-api sync for customer NL_110572",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4412",
        "ok": "4412",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4412",
        "ok": "4412",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4412",
        "ok": "4412",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4412",
        "ok": "4412",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4412",
        "ok": "4412",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4412",
        "ok": "4412",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4412",
        "ok": "4412",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-184df": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110881",
path: "Animal-api sync for customer NL_110881",
pathFormatted: "req_animal-api-sync-184df",
stats: {
    "name": "Animal-api sync for customer NL_110881",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3911",
        "ok": "3911",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3911",
        "ok": "3911",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3911",
        "ok": "3911",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3911",
        "ok": "3911",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3911",
        "ok": "3911",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3911",
        "ok": "3911",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3911",
        "ok": "3911",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-61fa0": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_214412",
path: "Animal-api sync for customer NL_214412",
pathFormatted: "req_animal-api-sync-61fa0",
stats: {
    "name": "Animal-api sync for customer NL_214412",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3543",
        "ok": "3543",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3543",
        "ok": "3543",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3543",
        "ok": "3543",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3543",
        "ok": "3543",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3543",
        "ok": "3543",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3543",
        "ok": "3543",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3543",
        "ok": "3543",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-2f673": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_112286",
path: "Animal-api sync for customer NL_112286",
pathFormatted: "req_animal-api-sync-2f673",
stats: {
    "name": "Animal-api sync for customer NL_112286",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "958",
        "ok": "958",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "958",
        "ok": "958",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "958",
        "ok": "958",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "958",
        "ok": "958",
        "ko": "-"
    },
    "percentiles2": {
        "total": "958",
        "ok": "958",
        "ko": "-"
    },
    "percentiles3": {
        "total": "958",
        "ok": "958",
        "ko": "-"
    },
    "percentiles4": {
        "total": "958",
        "ok": "958",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-7a391": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_125864",
path: "Animal-api sync for customer NL_125864",
pathFormatted: "req_animal-api-sync-7a391",
stats: {
    "name": "Animal-api sync for customer NL_125864",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4629",
        "ok": "4629",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4629",
        "ok": "4629",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4629",
        "ok": "4629",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4629",
        "ok": "4629",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4629",
        "ok": "4629",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4629",
        "ok": "4629",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4629",
        "ok": "4629",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-844f0": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_128832",
path: "Animal-api sync for customer NL_128832",
pathFormatted: "req_animal-api-sync-844f0",
stats: {
    "name": "Animal-api sync for customer NL_128832",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "9274",
        "ok": "9274",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "9274",
        "ok": "9274",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "9274",
        "ok": "9274",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "9274",
        "ok": "9274",
        "ko": "-"
    },
    "percentiles2": {
        "total": "9274",
        "ok": "9274",
        "ko": "-"
    },
    "percentiles3": {
        "total": "9274",
        "ok": "9274",
        "ko": "-"
    },
    "percentiles4": {
        "total": "9274",
        "ok": "9274",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-4e25d": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104043",
path: "Animal-api sync for customer NL_104043",
pathFormatted: "req_animal-api-sync-4e25d",
stats: {
    "name": "Animal-api sync for customer NL_104043",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "6234",
        "ok": "6234",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "6234",
        "ok": "6234",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "6234",
        "ok": "6234",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "6234",
        "ok": "6234",
        "ko": "-"
    },
    "percentiles2": {
        "total": "6234",
        "ok": "6234",
        "ko": "-"
    },
    "percentiles3": {
        "total": "6234",
        "ok": "6234",
        "ko": "-"
    },
    "percentiles4": {
        "total": "6234",
        "ok": "6234",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-25426": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_114351",
path: "Animal-api sync for customer NL_114351",
pathFormatted: "req_animal-api-sync-25426",
stats: {
    "name": "Animal-api sync for customer NL_114351",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2604",
        "ok": "2604",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2604",
        "ok": "2604",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2604",
        "ok": "2604",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2604",
        "ok": "2604",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2604",
        "ok": "2604",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2604",
        "ok": "2604",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2604",
        "ok": "2604",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-dfb65": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_108668",
path: "Animal-api sync for customer NL_108668",
pathFormatted: "req_animal-api-sync-dfb65",
stats: {
    "name": "Animal-api sync for customer NL_108668",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5563",
        "ok": "5563",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5563",
        "ok": "5563",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5563",
        "ok": "5563",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5563",
        "ok": "5563",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5563",
        "ok": "5563",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5563",
        "ok": "5563",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5563",
        "ok": "5563",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-997d2": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111823",
path: "Animal-api sync for customer NL_111823",
pathFormatted: "req_animal-api-sync-997d2",
stats: {
    "name": "Animal-api sync for customer NL_111823",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "16373",
        "ok": "16373",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "16373",
        "ok": "16373",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "16373",
        "ok": "16373",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "16373",
        "ok": "16373",
        "ko": "-"
    },
    "percentiles2": {
        "total": "16373",
        "ok": "16373",
        "ko": "-"
    },
    "percentiles3": {
        "total": "16373",
        "ok": "16373",
        "ko": "-"
    },
    "percentiles4": {
        "total": "16373",
        "ok": "16373",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d5cc8": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_103579",
path: "Animal-api sync for customer NL_103579",
pathFormatted: "req_animal-api-sync-d5cc8",
stats: {
    "name": "Animal-api sync for customer NL_103579",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4038",
        "ok": "4038",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4038",
        "ok": "4038",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4038",
        "ok": "4038",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4038",
        "ok": "4038",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4038",
        "ok": "4038",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4038",
        "ok": "4038",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4038",
        "ok": "4038",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-96467": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_134577",
path: "Animal-api sync for customer NL_134577",
pathFormatted: "req_animal-api-sync-96467",
stats: {
    "name": "Animal-api sync for customer NL_134577",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3836",
        "ok": "3836",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3836",
        "ok": "3836",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3836",
        "ok": "3836",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3836",
        "ok": "3836",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3836",
        "ok": "3836",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3836",
        "ok": "3836",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3836",
        "ok": "3836",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-4c254": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105650",
path: "Animal-api sync for customer NL_105650",
pathFormatted: "req_animal-api-sync-4c254",
stats: {
    "name": "Animal-api sync for customer NL_105650",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "14352",
        "ok": "14352",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "14352",
        "ok": "14352",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "14352",
        "ok": "14352",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "14352",
        "ok": "14352",
        "ko": "-"
    },
    "percentiles2": {
        "total": "14352",
        "ok": "14352",
        "ko": "-"
    },
    "percentiles3": {
        "total": "14352",
        "ok": "14352",
        "ko": "-"
    },
    "percentiles4": {
        "total": "14352",
        "ok": "14352",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-5ea6b": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_194847",
path: "Animal-api sync for customer BE_194847",
pathFormatted: "req_animal-api-sync-5ea6b",
stats: {
    "name": "Animal-api sync for customer BE_194847",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "6288",
        "ok": "6288",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "6288",
        "ok": "6288",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "6288",
        "ok": "6288",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "6288",
        "ok": "6288",
        "ko": "-"
    },
    "percentiles2": {
        "total": "6288",
        "ok": "6288",
        "ko": "-"
    },
    "percentiles3": {
        "total": "6288",
        "ok": "6288",
        "ko": "-"
    },
    "percentiles4": {
        "total": "6288",
        "ok": "6288",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a4924": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117341",
path: "Animal-api sync for customer NL_117341",
pathFormatted: "req_animal-api-sync-a4924",
stats: {
    "name": "Animal-api sync for customer NL_117341",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3035",
        "ok": "3035",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3035",
        "ok": "3035",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3035",
        "ok": "3035",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3035",
        "ok": "3035",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3035",
        "ok": "3035",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3035",
        "ok": "3035",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3035",
        "ok": "3035",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-12331": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_113115",
path: "Animal-api sync for customer NL_113115",
pathFormatted: "req_animal-api-sync-12331",
stats: {
    "name": "Animal-api sync for customer NL_113115",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5604",
        "ok": "5604",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5604",
        "ok": "5604",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5604",
        "ok": "5604",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5604",
        "ok": "5604",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5604",
        "ok": "5604",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5604",
        "ok": "5604",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5604",
        "ok": "5604",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-13f62": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_149314",
path: "Animal-api sync for customer BE_149314",
pathFormatted: "req_animal-api-sync-13f62",
stats: {
    "name": "Animal-api sync for customer BE_149314",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3279",
        "ok": "3279",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3279",
        "ok": "3279",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3279",
        "ok": "3279",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3279",
        "ok": "3279",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3279",
        "ok": "3279",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3279",
        "ok": "3279",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3279",
        "ok": "3279",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-500bc": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117068",
path: "Animal-api sync for customer NL_117068",
pathFormatted: "req_animal-api-sync-500bc",
stats: {
    "name": "Animal-api sync for customer NL_117068",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2476",
        "ok": "2476",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2476",
        "ok": "2476",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2476",
        "ok": "2476",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2476",
        "ok": "2476",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2476",
        "ok": "2476",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2476",
        "ok": "2476",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2476",
        "ok": "2476",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-6d4c5": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_122660",
path: "Animal-api sync for customer NL_122660",
pathFormatted: "req_animal-api-sync-6d4c5",
stats: {
    "name": "Animal-api sync for customer NL_122660",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1403",
        "ok": "1403",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1403",
        "ok": "1403",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1403",
        "ok": "1403",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1403",
        "ok": "1403",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1403",
        "ok": "1403",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1403",
        "ok": "1403",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1403",
        "ok": "1403",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-cb570": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104777",
path: "Animal-api sync for customer NL_104777",
pathFormatted: "req_animal-api-sync-cb570",
stats: {
    "name": "Animal-api sync for customer NL_104777",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "28739",
        "ok": "28739",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "28739",
        "ok": "28739",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "28739",
        "ok": "28739",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "28739",
        "ok": "28739",
        "ko": "-"
    },
    "percentiles2": {
        "total": "28739",
        "ok": "28739",
        "ko": "-"
    },
    "percentiles3": {
        "total": "28739",
        "ok": "28739",
        "ko": "-"
    },
    "percentiles4": {
        "total": "28739",
        "ok": "28739",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-49e6e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_122219",
path: "Animal-api sync for customer NL_122219",
pathFormatted: "req_animal-api-sync-49e6e",
stats: {
    "name": "Animal-api sync for customer NL_122219",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3555",
        "ok": "3555",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3555",
        "ok": "3555",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3555",
        "ok": "3555",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3555",
        "ok": "3555",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3555",
        "ok": "3555",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3555",
        "ok": "3555",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3555",
        "ok": "3555",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b0552": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_122008",
path: "Animal-api sync for customer NL_122008",
pathFormatted: "req_animal-api-sync-b0552",
stats: {
    "name": "Animal-api sync for customer NL_122008",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "11607",
        "ok": "11607",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "11607",
        "ok": "11607",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "11607",
        "ok": "11607",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "11607",
        "ok": "11607",
        "ko": "-"
    },
    "percentiles2": {
        "total": "11607",
        "ok": "11607",
        "ko": "-"
    },
    "percentiles3": {
        "total": "11607",
        "ok": "11607",
        "ko": "-"
    },
    "percentiles4": {
        "total": "11607",
        "ok": "11607",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-7d526": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105576",
path: "Animal-api sync for customer NL_105576",
pathFormatted: "req_animal-api-sync-7d526",
stats: {
    "name": "Animal-api sync for customer NL_105576",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4315",
        "ok": "4315",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4315",
        "ok": "4315",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4315",
        "ok": "4315",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4315",
        "ok": "4315",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4315",
        "ok": "4315",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4315",
        "ok": "4315",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4315",
        "ok": "4315",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a6cf9": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104168",
path: "Animal-api sync for customer NL_104168",
pathFormatted: "req_animal-api-sync-a6cf9",
stats: {
    "name": "Animal-api sync for customer NL_104168",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "8370",
        "ok": "8370",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "8370",
        "ok": "8370",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "8370",
        "ok": "8370",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "8370",
        "ok": "8370",
        "ko": "-"
    },
    "percentiles2": {
        "total": "8370",
        "ok": "8370",
        "ko": "-"
    },
    "percentiles3": {
        "total": "8370",
        "ok": "8370",
        "ko": "-"
    },
    "percentiles4": {
        "total": "8370",
        "ok": "8370",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-6809e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117264",
path: "Animal-api sync for customer NL_117264",
pathFormatted: "req_animal-api-sync-6809e",
stats: {
    "name": "Animal-api sync for customer NL_117264",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "23728",
        "ok": "23728",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "23728",
        "ok": "23728",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "23728",
        "ok": "23728",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "23728",
        "ok": "23728",
        "ko": "-"
    },
    "percentiles2": {
        "total": "23728",
        "ok": "23728",
        "ko": "-"
    },
    "percentiles3": {
        "total": "23728",
        "ok": "23728",
        "ko": "-"
    },
    "percentiles4": {
        "total": "23728",
        "ok": "23728",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-cb15a": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_114531",
path: "Animal-api sync for customer NL_114531",
pathFormatted: "req_animal-api-sync-cb15a",
stats: {
    "name": "Animal-api sync for customer NL_114531",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5151",
        "ok": "5151",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5151",
        "ok": "5151",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5151",
        "ok": "5151",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5151",
        "ok": "5151",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5151",
        "ok": "5151",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5151",
        "ok": "5151",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5151",
        "ok": "5151",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-aa469": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_158913",
path: "Animal-api sync for customer BE_158913",
pathFormatted: "req_animal-api-sync-aa469",
stats: {
    "name": "Animal-api sync for customer BE_158913",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5758",
        "ok": "5758",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5758",
        "ok": "5758",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5758",
        "ok": "5758",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5758",
        "ok": "5758",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5758",
        "ok": "5758",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5758",
        "ok": "5758",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5758",
        "ok": "5758",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-fc480": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_121609",
path: "Animal-api sync for customer NL_121609",
pathFormatted: "req_animal-api-sync-fc480",
stats: {
    "name": "Animal-api sync for customer NL_121609",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "10505",
        "ok": "10505",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "10505",
        "ok": "10505",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "10505",
        "ok": "10505",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "10505",
        "ok": "10505",
        "ko": "-"
    },
    "percentiles2": {
        "total": "10505",
        "ok": "10505",
        "ko": "-"
    },
    "percentiles3": {
        "total": "10505",
        "ok": "10505",
        "ko": "-"
    },
    "percentiles4": {
        "total": "10505",
        "ok": "10505",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-2fa8f": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_103719",
path: "Animal-api sync for customer NL_103719",
pathFormatted: "req_animal-api-sync-2fa8f",
stats: {
    "name": "Animal-api sync for customer NL_103719",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "20947",
        "ok": "20947",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "20947",
        "ok": "20947",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "20947",
        "ok": "20947",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "20947",
        "ok": "20947",
        "ko": "-"
    },
    "percentiles2": {
        "total": "20947",
        "ok": "20947",
        "ko": "-"
    },
    "percentiles3": {
        "total": "20947",
        "ok": "20947",
        "ko": "-"
    },
    "percentiles4": {
        "total": "20947",
        "ok": "20947",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c7578": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105839",
path: "Animal-api sync for customer NL_105839",
pathFormatted: "req_animal-api-sync-c7578",
stats: {
    "name": "Animal-api sync for customer NL_105839",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "24115",
        "ok": "24115",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "24115",
        "ok": "24115",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "24115",
        "ok": "24115",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "24115",
        "ok": "24115",
        "ko": "-"
    },
    "percentiles2": {
        "total": "24115",
        "ok": "24115",
        "ko": "-"
    },
    "percentiles3": {
        "total": "24115",
        "ok": "24115",
        "ko": "-"
    },
    "percentiles4": {
        "total": "24115",
        "ok": "24115",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-ca9a4": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_123159",
path: "Animal-api sync for customer NL_123159",
pathFormatted: "req_animal-api-sync-ca9a4",
stats: {
    "name": "Animal-api sync for customer NL_123159",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3033",
        "ok": "3033",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3033",
        "ok": "3033",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3033",
        "ok": "3033",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3033",
        "ok": "3033",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3033",
        "ok": "3033",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3033",
        "ok": "3033",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3033",
        "ok": "3033",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d58c5": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_115162",
path: "Animal-api sync for customer NL_115162",
pathFormatted: "req_animal-api-sync-d58c5",
stats: {
    "name": "Animal-api sync for customer NL_115162",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1553",
        "ok": "1553",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1553",
        "ok": "1553",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1553",
        "ok": "1553",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1553",
        "ok": "1553",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1553",
        "ok": "1553",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1553",
        "ok": "1553",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1553",
        "ok": "1553",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d43d8": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_112453",
path: "Animal-api sync for customer NL_112453",
pathFormatted: "req_animal-api-sync-d43d8",
stats: {
    "name": "Animal-api sync for customer NL_112453",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3987",
        "ok": "3987",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3987",
        "ok": "3987",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3987",
        "ok": "3987",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3987",
        "ok": "3987",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3987",
        "ok": "3987",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3987",
        "ok": "3987",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3987",
        "ok": "3987",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-fa50f": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_194938",
path: "Animal-api sync for customer NL_194938",
pathFormatted: "req_animal-api-sync-fa50f",
stats: {
    "name": "Animal-api sync for customer NL_194938",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "10750",
        "ok": "10750",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "10750",
        "ok": "10750",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "10750",
        "ok": "10750",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "10750",
        "ok": "10750",
        "ko": "-"
    },
    "percentiles2": {
        "total": "10750",
        "ok": "10750",
        "ko": "-"
    },
    "percentiles3": {
        "total": "10750",
        "ok": "10750",
        "ko": "-"
    },
    "percentiles4": {
        "total": "10750",
        "ok": "10750",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d3cf7": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_123704",
path: "Animal-api sync for customer NL_123704",
pathFormatted: "req_animal-api-sync-d3cf7",
stats: {
    "name": "Animal-api sync for customer NL_123704",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2574",
        "ok": "2574",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2574",
        "ok": "2574",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2574",
        "ok": "2574",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2574",
        "ok": "2574",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2574",
        "ok": "2574",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2574",
        "ok": "2574",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2574",
        "ok": "2574",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e5ad2": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_108858",
path: "Animal-api sync for customer NL_108858",
pathFormatted: "req_animal-api-sync-e5ad2",
stats: {
    "name": "Animal-api sync for customer NL_108858",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5235",
        "ok": "5235",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5235",
        "ok": "5235",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5235",
        "ok": "5235",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5235",
        "ok": "5235",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5235",
        "ok": "5235",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5235",
        "ok": "5235",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5235",
        "ok": "5235",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-23adc": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110836",
path: "Animal-api sync for customer NL_110836",
pathFormatted: "req_animal-api-sync-23adc",
stats: {
    "name": "Animal-api sync for customer NL_110836",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4526",
        "ok": "4526",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4526",
        "ok": "4526",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4526",
        "ok": "4526",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4526",
        "ok": "4526",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4526",
        "ok": "4526",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4526",
        "ok": "4526",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4526",
        "ok": "4526",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-8d897": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_133100",
path: "Animal-api sync for customer NL_133100",
pathFormatted: "req_animal-api-sync-8d897",
stats: {
    "name": "Animal-api sync for customer NL_133100",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "18804",
        "ok": "18804",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "18804",
        "ok": "18804",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "18804",
        "ok": "18804",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "18804",
        "ok": "18804",
        "ko": "-"
    },
    "percentiles2": {
        "total": "18804",
        "ok": "18804",
        "ko": "-"
    },
    "percentiles3": {
        "total": "18804",
        "ok": "18804",
        "ko": "-"
    },
    "percentiles4": {
        "total": "18804",
        "ok": "18804",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e921e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_122446",
path: "Animal-api sync for customer NL_122446",
pathFormatted: "req_animal-api-sync-e921e",
stats: {
    "name": "Animal-api sync for customer NL_122446",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "8101",
        "ok": "8101",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "8101",
        "ok": "8101",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "8101",
        "ok": "8101",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "8101",
        "ok": "8101",
        "ko": "-"
    },
    "percentiles2": {
        "total": "8101",
        "ok": "8101",
        "ko": "-"
    },
    "percentiles3": {
        "total": "8101",
        "ok": "8101",
        "ko": "-"
    },
    "percentiles4": {
        "total": "8101",
        "ok": "8101",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-fd312": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117690",
path: "Animal-api sync for customer NL_117690",
pathFormatted: "req_animal-api-sync-fd312",
stats: {
    "name": "Animal-api sync for customer NL_117690",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "6221",
        "ok": "6221",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "6221",
        "ok": "6221",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "6221",
        "ok": "6221",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "6221",
        "ok": "6221",
        "ko": "-"
    },
    "percentiles2": {
        "total": "6221",
        "ok": "6221",
        "ko": "-"
    },
    "percentiles3": {
        "total": "6221",
        "ok": "6221",
        "ko": "-"
    },
    "percentiles4": {
        "total": "6221",
        "ok": "6221",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c6f3a": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_119414",
path: "Animal-api sync for customer NL_119414",
pathFormatted: "req_animal-api-sync-c6f3a",
stats: {
    "name": "Animal-api sync for customer NL_119414",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "7360",
        "ok": "7360",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "7360",
        "ok": "7360",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "7360",
        "ok": "7360",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "7360",
        "ok": "7360",
        "ko": "-"
    },
    "percentiles2": {
        "total": "7360",
        "ok": "7360",
        "ko": "-"
    },
    "percentiles3": {
        "total": "7360",
        "ok": "7360",
        "ko": "-"
    },
    "percentiles4": {
        "total": "7360",
        "ok": "7360",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a8229": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_129350",
path: "Animal-api sync for customer NL_129350",
pathFormatted: "req_animal-api-sync-a8229",
stats: {
    "name": "Animal-api sync for customer NL_129350",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "14809",
        "ok": "14809",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "14809",
        "ok": "14809",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "14809",
        "ok": "14809",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "14809",
        "ok": "14809",
        "ko": "-"
    },
    "percentiles2": {
        "total": "14809",
        "ok": "14809",
        "ko": "-"
    },
    "percentiles3": {
        "total": "14809",
        "ok": "14809",
        "ko": "-"
    },
    "percentiles4": {
        "total": "14809",
        "ok": "14809",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e7e4c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105106",
path: "Animal-api sync for customer NL_105106",
pathFormatted: "req_animal-api-sync-e7e4c",
stats: {
    "name": "Animal-api sync for customer NL_105106",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5758",
        "ok": "5758",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5758",
        "ok": "5758",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5758",
        "ok": "5758",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5758",
        "ok": "5758",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5758",
        "ok": "5758",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5758",
        "ok": "5758",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5758",
        "ok": "5758",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c3423": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_112704",
path: "Animal-api sync for customer NL_112704",
pathFormatted: "req_animal-api-sync-c3423",
stats: {
    "name": "Animal-api sync for customer NL_112704",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "11017",
        "ok": "11017",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "11017",
        "ok": "11017",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "11017",
        "ok": "11017",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "11017",
        "ok": "11017",
        "ko": "-"
    },
    "percentiles2": {
        "total": "11017",
        "ok": "11017",
        "ko": "-"
    },
    "percentiles3": {
        "total": "11017",
        "ok": "11017",
        "ko": "-"
    },
    "percentiles4": {
        "total": "11017",
        "ok": "11017",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c2f39": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_157878",
path: "Animal-api sync for customer BE_157878",
pathFormatted: "req_animal-api-sync-c2f39",
stats: {
    "name": "Animal-api sync for customer BE_157878",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4952",
        "ok": "4952",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4952",
        "ok": "4952",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4952",
        "ok": "4952",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4952",
        "ok": "4952",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4952",
        "ok": "4952",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4952",
        "ok": "4952",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4952",
        "ok": "4952",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-fee59": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117006",
path: "Animal-api sync for customer NL_117006",
pathFormatted: "req_animal-api-sync-fee59",
stats: {
    "name": "Animal-api sync for customer NL_117006",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2336",
        "ok": "2336",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2336",
        "ok": "2336",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2336",
        "ok": "2336",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2336",
        "ok": "2336",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2336",
        "ok": "2336",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2336",
        "ok": "2336",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2336",
        "ok": "2336",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-bab68": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_134874",
path: "Animal-api sync for customer NL_134874",
pathFormatted: "req_animal-api-sync-bab68",
stats: {
    "name": "Animal-api sync for customer NL_134874",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "6219",
        "ok": "6219",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "6219",
        "ok": "6219",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "6219",
        "ok": "6219",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "6219",
        "ok": "6219",
        "ko": "-"
    },
    "percentiles2": {
        "total": "6219",
        "ok": "6219",
        "ko": "-"
    },
    "percentiles3": {
        "total": "6219",
        "ok": "6219",
        "ko": "-"
    },
    "percentiles4": {
        "total": "6219",
        "ok": "6219",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-3e99c": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_214193",
path: "Animal-api sync for customer BE_214193",
pathFormatted: "req_animal-api-sync-3e99c",
stats: {
    "name": "Animal-api sync for customer BE_214193",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "7185",
        "ok": "7185",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "7185",
        "ok": "7185",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "7185",
        "ok": "7185",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "7185",
        "ok": "7185",
        "ko": "-"
    },
    "percentiles2": {
        "total": "7185",
        "ok": "7185",
        "ko": "-"
    },
    "percentiles3": {
        "total": "7185",
        "ok": "7185",
        "ko": "-"
    },
    "percentiles4": {
        "total": "7185",
        "ok": "7185",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b2bc0": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_162281",
path: "Animal-api sync for customer BE_162281",
pathFormatted: "req_animal-api-sync-b2bc0",
stats: {
    "name": "Animal-api sync for customer BE_162281",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "10743",
        "ok": "10743",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "10743",
        "ok": "10743",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "10743",
        "ok": "10743",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "10743",
        "ok": "10743",
        "ko": "-"
    },
    "percentiles2": {
        "total": "10743",
        "ok": "10743",
        "ko": "-"
    },
    "percentiles3": {
        "total": "10743",
        "ok": "10743",
        "ko": "-"
    },
    "percentiles4": {
        "total": "10743",
        "ok": "10743",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-046eb": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_127329",
path: "Animal-api sync for customer NL_127329",
pathFormatted: "req_animal-api-sync-046eb",
stats: {
    "name": "Animal-api sync for customer NL_127329",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1882",
        "ok": "1882",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1882",
        "ok": "1882",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1882",
        "ok": "1882",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1882",
        "ok": "1882",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1882",
        "ok": "1882",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1882",
        "ok": "1882",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1882",
        "ok": "1882",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-7cc26": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_131009",
path: "Animal-api sync for customer NL_131009",
pathFormatted: "req_animal-api-sync-7cc26",
stats: {
    "name": "Animal-api sync for customer NL_131009",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "9103",
        "ok": "9103",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "9103",
        "ok": "9103",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "9103",
        "ok": "9103",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "9103",
        "ok": "9103",
        "ko": "-"
    },
    "percentiles2": {
        "total": "9103",
        "ok": "9103",
        "ko": "-"
    },
    "percentiles3": {
        "total": "9103",
        "ok": "9103",
        "ko": "-"
    },
    "percentiles4": {
        "total": "9103",
        "ok": "9103",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-52d91": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_163987",
path: "Animal-api sync for customer BE_163987",
pathFormatted: "req_animal-api-sync-52d91",
stats: {
    "name": "Animal-api sync for customer BE_163987",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2811",
        "ok": "2811",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2811",
        "ok": "2811",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2811",
        "ok": "2811",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2811",
        "ok": "2811",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2811",
        "ok": "2811",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2811",
        "ok": "2811",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2811",
        "ok": "2811",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-9b768": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_115732",
path: "Animal-api sync for customer NL_115732",
pathFormatted: "req_animal-api-sync-9b768",
stats: {
    "name": "Animal-api sync for customer NL_115732",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "6426",
        "ok": "6426",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "6426",
        "ok": "6426",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "6426",
        "ok": "6426",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "6426",
        "ok": "6426",
        "ko": "-"
    },
    "percentiles2": {
        "total": "6426",
        "ok": "6426",
        "ko": "-"
    },
    "percentiles3": {
        "total": "6426",
        "ok": "6426",
        "ko": "-"
    },
    "percentiles4": {
        "total": "6426",
        "ok": "6426",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c1337": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104350",
path: "Animal-api sync for customer NL_104350",
pathFormatted: "req_animal-api-sync-c1337",
stats: {
    "name": "Animal-api sync for customer NL_104350",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5596",
        "ok": "5596",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5596",
        "ok": "5596",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5596",
        "ok": "5596",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5596",
        "ok": "5596",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5596",
        "ok": "5596",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5596",
        "ok": "5596",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5596",
        "ok": "5596",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-de861": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_138595",
path: "Animal-api sync for customer NL_138595",
pathFormatted: "req_animal-api-sync-de861",
stats: {
    "name": "Animal-api sync for customer NL_138595",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2243",
        "ok": "2243",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2243",
        "ok": "2243",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2243",
        "ok": "2243",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2243",
        "ok": "2243",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2243",
        "ok": "2243",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2243",
        "ok": "2243",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2243",
        "ok": "2243",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-cc8e5": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_123944",
path: "Animal-api sync for customer NL_123944",
pathFormatted: "req_animal-api-sync-cc8e5",
stats: {
    "name": "Animal-api sync for customer NL_123944",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2337",
        "ok": "2337",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2337",
        "ok": "2337",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2337",
        "ok": "2337",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2337",
        "ok": "2337",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2337",
        "ok": "2337",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2337",
        "ok": "2337",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2337",
        "ok": "2337",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-033e7": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_113983",
path: "Animal-api sync for customer NL_113983",
pathFormatted: "req_animal-api-sync-033e7",
stats: {
    "name": "Animal-api sync for customer NL_113983",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2604",
        "ok": "2604",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2604",
        "ok": "2604",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2604",
        "ok": "2604",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2604",
        "ok": "2604",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2604",
        "ok": "2604",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2604",
        "ok": "2604",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2604",
        "ok": "2604",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-cb74a": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_131236",
path: "Animal-api sync for customer NL_131236",
pathFormatted: "req_animal-api-sync-cb74a",
stats: {
    "name": "Animal-api sync for customer NL_131236",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4909",
        "ok": "4909",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4909",
        "ok": "4909",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4909",
        "ok": "4909",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4909",
        "ok": "4909",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4909",
        "ok": "4909",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4909",
        "ok": "4909",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4909",
        "ok": "4909",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-4c33d": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_131701",
path: "Animal-api sync for customer NL_131701",
pathFormatted: "req_animal-api-sync-4c33d",
stats: {
    "name": "Animal-api sync for customer NL_131701",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4552",
        "ok": "4552",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4552",
        "ok": "4552",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4552",
        "ok": "4552",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4552",
        "ok": "4552",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4552",
        "ok": "4552",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4552",
        "ok": "4552",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4552",
        "ok": "4552",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-98e1b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_129146",
path: "Animal-api sync for customer NL_129146",
pathFormatted: "req_animal-api-sync-98e1b",
stats: {
    "name": "Animal-api sync for customer NL_129146",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "12564",
        "ok": "12564",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "12564",
        "ok": "12564",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "12564",
        "ok": "12564",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "12564",
        "ok": "12564",
        "ko": "-"
    },
    "percentiles2": {
        "total": "12564",
        "ok": "12564",
        "ko": "-"
    },
    "percentiles3": {
        "total": "12564",
        "ok": "12564",
        "ko": "-"
    },
    "percentiles4": {
        "total": "12564",
        "ok": "12564",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e8633": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117217",
path: "Animal-api sync for customer NL_117217",
pathFormatted: "req_animal-api-sync-e8633",
stats: {
    "name": "Animal-api sync for customer NL_117217",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4150",
        "ok": "4150",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4150",
        "ok": "4150",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4150",
        "ok": "4150",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4150",
        "ok": "4150",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4150",
        "ok": "4150",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4150",
        "ok": "4150",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4150",
        "ok": "4150",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-748ee": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_118193",
path: "Animal-api sync for customer NL_118193",
pathFormatted: "req_animal-api-sync-748ee",
stats: {
    "name": "Animal-api sync for customer NL_118193",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "8913",
        "ok": "8913",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "8913",
        "ok": "8913",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "8913",
        "ok": "8913",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "8913",
        "ok": "8913",
        "ko": "-"
    },
    "percentiles2": {
        "total": "8913",
        "ok": "8913",
        "ko": "-"
    },
    "percentiles3": {
        "total": "8913",
        "ok": "8913",
        "ko": "-"
    },
    "percentiles4": {
        "total": "8913",
        "ok": "8913",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-ac942": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_162293",
path: "Animal-api sync for customer BE_162293",
pathFormatted: "req_animal-api-sync-ac942",
stats: {
    "name": "Animal-api sync for customer BE_162293",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "8004",
        "ok": "8004",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "8004",
        "ok": "8004",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "8004",
        "ok": "8004",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "8004",
        "ok": "8004",
        "ko": "-"
    },
    "percentiles2": {
        "total": "8004",
        "ok": "8004",
        "ko": "-"
    },
    "percentiles3": {
        "total": "8004",
        "ok": "8004",
        "ko": "-"
    },
    "percentiles4": {
        "total": "8004",
        "ok": "8004",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-18df0": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_132381",
path: "Animal-api sync for customer NL_132381",
pathFormatted: "req_animal-api-sync-18df0",
stats: {
    "name": "Animal-api sync for customer NL_132381",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "12371",
        "ok": "12371",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "12371",
        "ok": "12371",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "12371",
        "ok": "12371",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "12371",
        "ok": "12371",
        "ko": "-"
    },
    "percentiles2": {
        "total": "12371",
        "ok": "12371",
        "ko": "-"
    },
    "percentiles3": {
        "total": "12371",
        "ok": "12371",
        "ko": "-"
    },
    "percentiles4": {
        "total": "12371",
        "ok": "12371",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-7e345": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111388",
path: "Animal-api sync for customer NL_111388",
pathFormatted: "req_animal-api-sync-7e345",
stats: {
    "name": "Animal-api sync for customer NL_111388",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5379",
        "ok": "5379",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5379",
        "ok": "5379",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5379",
        "ok": "5379",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5379",
        "ok": "5379",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5379",
        "ok": "5379",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5379",
        "ok": "5379",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5379",
        "ok": "5379",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d3a0e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_118547",
path: "Animal-api sync for customer NL_118547",
pathFormatted: "req_animal-api-sync-d3a0e",
stats: {
    "name": "Animal-api sync for customer NL_118547",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3028",
        "ok": "3028",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3028",
        "ok": "3028",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3028",
        "ok": "3028",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3028",
        "ok": "3028",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3028",
        "ok": "3028",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3028",
        "ok": "3028",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3028",
        "ok": "3028",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-3631b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105143",
path: "Animal-api sync for customer NL_105143",
pathFormatted: "req_animal-api-sync-3631b",
stats: {
    "name": "Animal-api sync for customer NL_105143",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "24175",
        "ok": "24175",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "24175",
        "ok": "24175",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "24175",
        "ok": "24175",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "24175",
        "ok": "24175",
        "ko": "-"
    },
    "percentiles2": {
        "total": "24175",
        "ok": "24175",
        "ko": "-"
    },
    "percentiles3": {
        "total": "24175",
        "ok": "24175",
        "ko": "-"
    },
    "percentiles4": {
        "total": "24175",
        "ok": "24175",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-f99c5": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_131859",
path: "Animal-api sync for customer NL_131859",
pathFormatted: "req_animal-api-sync-f99c5",
stats: {
    "name": "Animal-api sync for customer NL_131859",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1664",
        "ok": "1664",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1664",
        "ok": "1664",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1664",
        "ok": "1664",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1664",
        "ok": "1664",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1664",
        "ok": "1664",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1664",
        "ok": "1664",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1664",
        "ok": "1664",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-ba235": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_109254",
path: "Animal-api sync for customer NL_109254",
pathFormatted: "req_animal-api-sync-ba235",
stats: {
    "name": "Animal-api sync for customer NL_109254",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2829",
        "ok": "2829",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2829",
        "ok": "2829",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2829",
        "ok": "2829",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2829",
        "ok": "2829",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2829",
        "ok": "2829",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2829",
        "ok": "2829",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2829",
        "ok": "2829",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e87b4": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_118457",
path: "Animal-api sync for customer NL_118457",
pathFormatted: "req_animal-api-sync-e87b4",
stats: {
    "name": "Animal-api sync for customer NL_118457",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4790",
        "ok": "4790",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4790",
        "ok": "4790",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4790",
        "ok": "4790",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4790",
        "ok": "4790",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4790",
        "ok": "4790",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4790",
        "ok": "4790",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4790",
        "ok": "4790",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-84066": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_136422",
path: "Animal-api sync for customer NL_136422",
pathFormatted: "req_animal-api-sync-84066",
stats: {
    "name": "Animal-api sync for customer NL_136422",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1832",
        "ok": "1832",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1832",
        "ok": "1832",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1832",
        "ok": "1832",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1832",
        "ok": "1832",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1832",
        "ok": "1832",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1832",
        "ok": "1832",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1832",
        "ok": "1832",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-f4249": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_138300",
path: "Animal-api sync for customer NL_138300",
pathFormatted: "req_animal-api-sync-f4249",
stats: {
    "name": "Animal-api sync for customer NL_138300",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "8097",
        "ok": "8097",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "8097",
        "ok": "8097",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "8097",
        "ok": "8097",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "8097",
        "ok": "8097",
        "ko": "-"
    },
    "percentiles2": {
        "total": "8097",
        "ok": "8097",
        "ko": "-"
    },
    "percentiles3": {
        "total": "8097",
        "ok": "8097",
        "ko": "-"
    },
    "percentiles4": {
        "total": "8097",
        "ok": "8097",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-32bc3": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_103585",
path: "Animal-api sync for customer NL_103585",
pathFormatted: "req_animal-api-sync-32bc3",
stats: {
    "name": "Animal-api sync for customer NL_103585",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2855",
        "ok": "2855",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2855",
        "ok": "2855",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2855",
        "ok": "2855",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2855",
        "ok": "2855",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2855",
        "ok": "2855",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2855",
        "ok": "2855",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2855",
        "ok": "2855",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-787cb": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_143564",
path: "Animal-api sync for customer NL_143564",
pathFormatted: "req_animal-api-sync-787cb",
stats: {
    "name": "Animal-api sync for customer NL_143564",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1399",
        "ok": "1399",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1399",
        "ok": "1399",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1399",
        "ok": "1399",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1399",
        "ok": "1399",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1399",
        "ok": "1399",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1399",
        "ok": "1399",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1399",
        "ok": "1399",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-0f436": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_107376",
path: "Animal-api sync for customer NL_107376",
pathFormatted: "req_animal-api-sync-0f436",
stats: {
    "name": "Animal-api sync for customer NL_107376",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4420",
        "ok": "4420",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4420",
        "ok": "4420",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4420",
        "ok": "4420",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4420",
        "ok": "4420",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4420",
        "ok": "4420",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4420",
        "ok": "4420",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4420",
        "ok": "4420",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-20cd3": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110661",
path: "Animal-api sync for customer NL_110661",
pathFormatted: "req_animal-api-sync-20cd3",
stats: {
    "name": "Animal-api sync for customer NL_110661",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "24048",
        "ok": "24048",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "24048",
        "ok": "24048",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "24048",
        "ok": "24048",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "24048",
        "ok": "24048",
        "ko": "-"
    },
    "percentiles2": {
        "total": "24048",
        "ok": "24048",
        "ko": "-"
    },
    "percentiles3": {
        "total": "24048",
        "ok": "24048",
        "ko": "-"
    },
    "percentiles4": {
        "total": "24048",
        "ok": "24048",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-eb26b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_118799",
path: "Animal-api sync for customer NL_118799",
pathFormatted: "req_animal-api-sync-eb26b",
stats: {
    "name": "Animal-api sync for customer NL_118799",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5100",
        "ok": "5100",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5100",
        "ok": "5100",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5100",
        "ok": "5100",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5100",
        "ok": "5100",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5100",
        "ok": "5100",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5100",
        "ok": "5100",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5100",
        "ok": "5100",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c1388": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_122597",
path: "Animal-api sync for customer NL_122597",
pathFormatted: "req_animal-api-sync-c1388",
stats: {
    "name": "Animal-api sync for customer NL_122597",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "10323",
        "ok": "10323",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "10323",
        "ok": "10323",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "10323",
        "ok": "10323",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "10323",
        "ok": "10323",
        "ko": "-"
    },
    "percentiles2": {
        "total": "10323",
        "ok": "10323",
        "ko": "-"
    },
    "percentiles3": {
        "total": "10323",
        "ok": "10323",
        "ko": "-"
    },
    "percentiles4": {
        "total": "10323",
        "ok": "10323",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-28731": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_136181",
path: "Animal-api sync for customer NL_136181",
pathFormatted: "req_animal-api-sync-28731",
stats: {
    "name": "Animal-api sync for customer NL_136181",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "9585",
        "ok": "9585",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "9585",
        "ok": "9585",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "9585",
        "ok": "9585",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "9585",
        "ok": "9585",
        "ko": "-"
    },
    "percentiles2": {
        "total": "9585",
        "ok": "9585",
        "ko": "-"
    },
    "percentiles3": {
        "total": "9585",
        "ok": "9585",
        "ko": "-"
    },
    "percentiles4": {
        "total": "9585",
        "ok": "9585",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-da9fa": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_212118",
path: "Animal-api sync for customer NL_212118",
pathFormatted: "req_animal-api-sync-da9fa",
stats: {
    "name": "Animal-api sync for customer NL_212118",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2325",
        "ok": "2325",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2325",
        "ok": "2325",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2325",
        "ok": "2325",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2325",
        "ok": "2325",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2325",
        "ok": "2325",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2325",
        "ok": "2325",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2325",
        "ok": "2325",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-2b764": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111875",
path: "Animal-api sync for customer NL_111875",
pathFormatted: "req_animal-api-sync-2b764",
stats: {
    "name": "Animal-api sync for customer NL_111875",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "18639",
        "ok": "18639",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "18639",
        "ok": "18639",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "18639",
        "ok": "18639",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "18639",
        "ok": "18639",
        "ko": "-"
    },
    "percentiles2": {
        "total": "18639",
        "ok": "18639",
        "ko": "-"
    },
    "percentiles3": {
        "total": "18639",
        "ok": "18639",
        "ko": "-"
    },
    "percentiles4": {
        "total": "18639",
        "ok": "18639",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-56095": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_125359",
path: "Animal-api sync for customer NL_125359",
pathFormatted: "req_animal-api-sync-56095",
stats: {
    "name": "Animal-api sync for customer NL_125359",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3645",
        "ok": "3645",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3645",
        "ok": "3645",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3645",
        "ok": "3645",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3645",
        "ok": "3645",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3645",
        "ok": "3645",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3645",
        "ok": "3645",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3645",
        "ok": "3645",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-7c52f": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_112020",
path: "Animal-api sync for customer NL_112020",
pathFormatted: "req_animal-api-sync-7c52f",
stats: {
    "name": "Animal-api sync for customer NL_112020",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "7885",
        "ok": "7885",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "7885",
        "ok": "7885",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "7885",
        "ok": "7885",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "7885",
        "ok": "7885",
        "ko": "-"
    },
    "percentiles2": {
        "total": "7885",
        "ok": "7885",
        "ko": "-"
    },
    "percentiles3": {
        "total": "7885",
        "ok": "7885",
        "ko": "-"
    },
    "percentiles4": {
        "total": "7885",
        "ok": "7885",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-1586b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_113408",
path: "Animal-api sync for customer NL_113408",
pathFormatted: "req_animal-api-sync-1586b",
stats: {
    "name": "Animal-api sync for customer NL_113408",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "15251",
        "ok": "15251",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "15251",
        "ok": "15251",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "15251",
        "ok": "15251",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "15251",
        "ok": "15251",
        "ko": "-"
    },
    "percentiles2": {
        "total": "15251",
        "ok": "15251",
        "ko": "-"
    },
    "percentiles3": {
        "total": "15251",
        "ok": "15251",
        "ko": "-"
    },
    "percentiles4": {
        "total": "15251",
        "ok": "15251",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-bca62": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_112986",
path: "Animal-api sync for customer NL_112986",
pathFormatted: "req_animal-api-sync-bca62",
stats: {
    "name": "Animal-api sync for customer NL_112986",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "33117",
        "ok": "33117",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "33117",
        "ok": "33117",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "33117",
        "ok": "33117",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "33117",
        "ok": "33117",
        "ko": "-"
    },
    "percentiles2": {
        "total": "33117",
        "ok": "33117",
        "ko": "-"
    },
    "percentiles3": {
        "total": "33117",
        "ok": "33117",
        "ko": "-"
    },
    "percentiles4": {
        "total": "33117",
        "ok": "33117",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d8a50": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_109324",
path: "Animal-api sync for customer NL_109324",
pathFormatted: "req_animal-api-sync-d8a50",
stats: {
    "name": "Animal-api sync for customer NL_109324",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "11909",
        "ok": "11909",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "11909",
        "ok": "11909",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "11909",
        "ok": "11909",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "11909",
        "ok": "11909",
        "ko": "-"
    },
    "percentiles2": {
        "total": "11909",
        "ok": "11909",
        "ko": "-"
    },
    "percentiles3": {
        "total": "11909",
        "ok": "11909",
        "ko": "-"
    },
    "percentiles4": {
        "total": "11909",
        "ok": "11909",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d4b4d": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_123556",
path: "Animal-api sync for customer NL_123556",
pathFormatted: "req_animal-api-sync-d4b4d",
stats: {
    "name": "Animal-api sync for customer NL_123556",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "28097",
        "ok": "28097",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "28097",
        "ok": "28097",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "28097",
        "ok": "28097",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "28097",
        "ok": "28097",
        "ko": "-"
    },
    "percentiles2": {
        "total": "28097",
        "ok": "28097",
        "ko": "-"
    },
    "percentiles3": {
        "total": "28097",
        "ok": "28097",
        "ko": "-"
    },
    "percentiles4": {
        "total": "28097",
        "ok": "28097",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-bc7d0": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_151016",
path: "Animal-api sync for customer BE_151016",
pathFormatted: "req_animal-api-sync-bc7d0",
stats: {
    "name": "Animal-api sync for customer BE_151016",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3871",
        "ok": "3871",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3871",
        "ok": "3871",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3871",
        "ok": "3871",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3871",
        "ok": "3871",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3871",
        "ok": "3871",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3871",
        "ok": "3871",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3871",
        "ok": "3871",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a0b51": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_118379",
path: "Animal-api sync for customer NL_118379",
pathFormatted: "req_animal-api-sync-a0b51",
stats: {
    "name": "Animal-api sync for customer NL_118379",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "32724",
        "ok": "32724",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "32724",
        "ok": "32724",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "32724",
        "ok": "32724",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "32724",
        "ok": "32724",
        "ko": "-"
    },
    "percentiles2": {
        "total": "32724",
        "ok": "32724",
        "ko": "-"
    },
    "percentiles3": {
        "total": "32724",
        "ok": "32724",
        "ko": "-"
    },
    "percentiles4": {
        "total": "32724",
        "ok": "32724",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b6f25": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_119444",
path: "Animal-api sync for customer NL_119444",
pathFormatted: "req_animal-api-sync-b6f25",
stats: {
    "name": "Animal-api sync for customer NL_119444",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5537",
        "ok": "5537",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5537",
        "ok": "5537",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5537",
        "ok": "5537",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5537",
        "ok": "5537",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5537",
        "ok": "5537",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5537",
        "ok": "5537",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5537",
        "ok": "5537",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b9666": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_207943",
path: "Animal-api sync for customer BE_207943",
pathFormatted: "req_animal-api-sync-b9666",
stats: {
    "name": "Animal-api sync for customer BE_207943",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "39781",
        "ok": "39781",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "39781",
        "ok": "39781",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "39781",
        "ok": "39781",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "39781",
        "ok": "39781",
        "ko": "-"
    },
    "percentiles2": {
        "total": "39781",
        "ok": "39781",
        "ko": "-"
    },
    "percentiles3": {
        "total": "39781",
        "ok": "39781",
        "ko": "-"
    },
    "percentiles4": {
        "total": "39781",
        "ok": "39781",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-662d1": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_211608",
path: "Animal-api sync for customer BE_211608",
pathFormatted: "req_animal-api-sync-662d1",
stats: {
    "name": "Animal-api sync for customer BE_211608",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5018",
        "ok": "5018",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5018",
        "ok": "5018",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5018",
        "ok": "5018",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5018",
        "ok": "5018",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5018",
        "ok": "5018",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5018",
        "ok": "5018",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5018",
        "ok": "5018",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-58eb2": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_125881",
path: "Animal-api sync for customer NL_125881",
pathFormatted: "req_animal-api-sync-58eb2",
stats: {
    "name": "Animal-api sync for customer NL_125881",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2676",
        "ok": "2676",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2676",
        "ok": "2676",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2676",
        "ok": "2676",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2676",
        "ok": "2676",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2676",
        "ok": "2676",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2676",
        "ok": "2676",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2676",
        "ok": "2676",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-cd728": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117817",
path: "Animal-api sync for customer NL_117817",
pathFormatted: "req_animal-api-sync-cd728",
stats: {
    "name": "Animal-api sync for customer NL_117817",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5133",
        "ok": "5133",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5133",
        "ok": "5133",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5133",
        "ok": "5133",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5133",
        "ok": "5133",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5133",
        "ok": "5133",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5133",
        "ok": "5133",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5133",
        "ok": "5133",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d0381": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_126231",
path: "Animal-api sync for customer NL_126231",
pathFormatted: "req_animal-api-sync-d0381",
stats: {
    "name": "Animal-api sync for customer NL_126231",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "6039",
        "ok": "6039",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "6039",
        "ok": "6039",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "6039",
        "ok": "6039",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "6039",
        "ok": "6039",
        "ko": "-"
    },
    "percentiles2": {
        "total": "6039",
        "ok": "6039",
        "ko": "-"
    },
    "percentiles3": {
        "total": "6039",
        "ok": "6039",
        "ko": "-"
    },
    "percentiles4": {
        "total": "6039",
        "ok": "6039",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-1164c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104369",
path: "Animal-api sync for customer NL_104369",
pathFormatted: "req_animal-api-sync-1164c",
stats: {
    "name": "Animal-api sync for customer NL_104369",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "7262",
        "ok": "7262",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "7262",
        "ok": "7262",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "7262",
        "ok": "7262",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "7262",
        "ok": "7262",
        "ko": "-"
    },
    "percentiles2": {
        "total": "7262",
        "ok": "7262",
        "ko": "-"
    },
    "percentiles3": {
        "total": "7262",
        "ok": "7262",
        "ko": "-"
    },
    "percentiles4": {
        "total": "7262",
        "ok": "7262",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c2774": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_130077",
path: "Animal-api sync for customer NL_130077",
pathFormatted: "req_animal-api-sync-c2774",
stats: {
    "name": "Animal-api sync for customer NL_130077",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "10007",
        "ok": "10007",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "10007",
        "ok": "10007",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "10007",
        "ok": "10007",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "10007",
        "ok": "10007",
        "ko": "-"
    },
    "percentiles2": {
        "total": "10007",
        "ok": "10007",
        "ko": "-"
    },
    "percentiles3": {
        "total": "10007",
        "ok": "10007",
        "ko": "-"
    },
    "percentiles4": {
        "total": "10007",
        "ok": "10007",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-da2a1": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105775",
path: "Animal-api sync for customer NL_105775",
pathFormatted: "req_animal-api-sync-da2a1",
stats: {
    "name": "Animal-api sync for customer NL_105775",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "26933",
        "ok": "26933",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "26933",
        "ok": "26933",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "26933",
        "ok": "26933",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "26933",
        "ok": "26933",
        "ko": "-"
    },
    "percentiles2": {
        "total": "26933",
        "ok": "26933",
        "ko": "-"
    },
    "percentiles3": {
        "total": "26933",
        "ok": "26933",
        "ko": "-"
    },
    "percentiles4": {
        "total": "26933",
        "ok": "26933",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-12657": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_212029",
path: "Animal-api sync for customer BE_212029",
pathFormatted: "req_animal-api-sync-12657",
stats: {
    "name": "Animal-api sync for customer BE_212029",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "10922",
        "ok": "10922",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "10922",
        "ok": "10922",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "10922",
        "ok": "10922",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "10922",
        "ok": "10922",
        "ko": "-"
    },
    "percentiles2": {
        "total": "10922",
        "ok": "10922",
        "ko": "-"
    },
    "percentiles3": {
        "total": "10922",
        "ok": "10922",
        "ko": "-"
    },
    "percentiles4": {
        "total": "10922",
        "ok": "10922",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a231f": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_119073",
path: "Animal-api sync for customer NL_119073",
pathFormatted: "req_animal-api-sync-a231f",
stats: {
    "name": "Animal-api sync for customer NL_119073",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "7293",
        "ok": "7293",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "7293",
        "ok": "7293",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "7293",
        "ok": "7293",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "7293",
        "ok": "7293",
        "ko": "-"
    },
    "percentiles2": {
        "total": "7293",
        "ok": "7293",
        "ko": "-"
    },
    "percentiles3": {
        "total": "7293",
        "ok": "7293",
        "ko": "-"
    },
    "percentiles4": {
        "total": "7293",
        "ok": "7293",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a5dcb": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_113563",
path: "Animal-api sync for customer NL_113563",
pathFormatted: "req_animal-api-sync-a5dcb",
stats: {
    "name": "Animal-api sync for customer NL_113563",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "23357",
        "ok": "23357",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "23357",
        "ok": "23357",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "23357",
        "ok": "23357",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "23357",
        "ok": "23357",
        "ko": "-"
    },
    "percentiles2": {
        "total": "23357",
        "ok": "23357",
        "ko": "-"
    },
    "percentiles3": {
        "total": "23357",
        "ok": "23357",
        "ko": "-"
    },
    "percentiles4": {
        "total": "23357",
        "ok": "23357",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-0d921": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_124334",
path: "Animal-api sync for customer NL_124334",
pathFormatted: "req_animal-api-sync-0d921",
stats: {
    "name": "Animal-api sync for customer NL_124334",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "25650",
        "ok": "25650",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "25650",
        "ok": "25650",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "25650",
        "ok": "25650",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "25650",
        "ok": "25650",
        "ko": "-"
    },
    "percentiles2": {
        "total": "25650",
        "ok": "25650",
        "ko": "-"
    },
    "percentiles3": {
        "total": "25650",
        "ok": "25650",
        "ko": "-"
    },
    "percentiles4": {
        "total": "25650",
        "ok": "25650",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a5308": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_129853",
path: "Animal-api sync for customer NL_129853",
pathFormatted: "req_animal-api-sync-a5308",
stats: {
    "name": "Animal-api sync for customer NL_129853",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4545",
        "ok": "4545",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4545",
        "ok": "4545",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4545",
        "ok": "4545",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4545",
        "ok": "4545",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4545",
        "ok": "4545",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4545",
        "ok": "4545",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4545",
        "ok": "4545",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a8587": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_112236",
path: "Animal-api sync for customer NL_112236",
pathFormatted: "req_animal-api-sync-a8587",
stats: {
    "name": "Animal-api sync for customer NL_112236",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "16572",
        "ok": "16572",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "16572",
        "ok": "16572",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "16572",
        "ok": "16572",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "16572",
        "ok": "16572",
        "ko": "-"
    },
    "percentiles2": {
        "total": "16572",
        "ok": "16572",
        "ko": "-"
    },
    "percentiles3": {
        "total": "16572",
        "ok": "16572",
        "ko": "-"
    },
    "percentiles4": {
        "total": "16572",
        "ok": "16572",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c1a9b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105397",
path: "Animal-api sync for customer NL_105397",
pathFormatted: "req_animal-api-sync-c1a9b",
stats: {
    "name": "Animal-api sync for customer NL_105397",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "18519",
        "ok": "18519",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "18519",
        "ok": "18519",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "18519",
        "ok": "18519",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "18519",
        "ok": "18519",
        "ko": "-"
    },
    "percentiles2": {
        "total": "18519",
        "ok": "18519",
        "ko": "-"
    },
    "percentiles3": {
        "total": "18519",
        "ok": "18519",
        "ko": "-"
    },
    "percentiles4": {
        "total": "18519",
        "ok": "18519",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-f47d9": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_162268",
path: "Animal-api sync for customer NL_162268",
pathFormatted: "req_animal-api-sync-f47d9",
stats: {
    "name": "Animal-api sync for customer NL_162268",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21886",
        "ok": "21886",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "21886",
        "ok": "21886",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "21886",
        "ok": "21886",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "21886",
        "ok": "21886",
        "ko": "-"
    },
    "percentiles2": {
        "total": "21886",
        "ok": "21886",
        "ko": "-"
    },
    "percentiles3": {
        "total": "21886",
        "ok": "21886",
        "ko": "-"
    },
    "percentiles4": {
        "total": "21886",
        "ok": "21886",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-7141e": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_195066",
path: "Animal-api sync for customer BE_195066",
pathFormatted: "req_animal-api-sync-7141e",
stats: {
    "name": "Animal-api sync for customer BE_195066",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "36321",
        "ok": "36321",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "36321",
        "ok": "36321",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "36321",
        "ok": "36321",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "36321",
        "ok": "36321",
        "ko": "-"
    },
    "percentiles2": {
        "total": "36321",
        "ok": "36321",
        "ko": "-"
    },
    "percentiles3": {
        "total": "36321",
        "ok": "36321",
        "ko": "-"
    },
    "percentiles4": {
        "total": "36321",
        "ok": "36321",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-9fb14": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_115364",
path: "Animal-api sync for customer NL_115364",
pathFormatted: "req_animal-api-sync-9fb14",
stats: {
    "name": "Animal-api sync for customer NL_115364",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "22322",
        "ok": "22322",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "22322",
        "ok": "22322",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "22322",
        "ok": "22322",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "22322",
        "ok": "22322",
        "ko": "-"
    },
    "percentiles2": {
        "total": "22322",
        "ok": "22322",
        "ko": "-"
    },
    "percentiles3": {
        "total": "22322",
        "ok": "22322",
        "ko": "-"
    },
    "percentiles4": {
        "total": "22322",
        "ok": "22322",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-29ae3": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_107774",
path: "Animal-api sync for customer NL_107774",
pathFormatted: "req_animal-api-sync-29ae3",
stats: {
    "name": "Animal-api sync for customer NL_107774",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "14364",
        "ok": "14364",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "14364",
        "ok": "14364",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "14364",
        "ok": "14364",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "14364",
        "ok": "14364",
        "ko": "-"
    },
    "percentiles2": {
        "total": "14364",
        "ok": "14364",
        "ko": "-"
    },
    "percentiles3": {
        "total": "14364",
        "ok": "14364",
        "ko": "-"
    },
    "percentiles4": {
        "total": "14364",
        "ok": "14364",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e24f9": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_125084",
path: "Animal-api sync for customer NL_125084",
pathFormatted: "req_animal-api-sync-e24f9",
stats: {
    "name": "Animal-api sync for customer NL_125084",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5101",
        "ok": "5101",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5101",
        "ok": "5101",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5101",
        "ok": "5101",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5101",
        "ok": "5101",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5101",
        "ok": "5101",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5101",
        "ok": "5101",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5101",
        "ok": "5101",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-6bcf1": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_130692",
path: "Animal-api sync for customer NL_130692",
pathFormatted: "req_animal-api-sync-6bcf1",
stats: {
    "name": "Animal-api sync for customer NL_130692",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "6282",
        "ok": "6282",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "6282",
        "ok": "6282",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "6282",
        "ok": "6282",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "6282",
        "ok": "6282",
        "ko": "-"
    },
    "percentiles2": {
        "total": "6282",
        "ok": "6282",
        "ko": "-"
    },
    "percentiles3": {
        "total": "6282",
        "ok": "6282",
        "ko": "-"
    },
    "percentiles4": {
        "total": "6282",
        "ok": "6282",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-42cac": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117138",
path: "Animal-api sync for customer NL_117138",
pathFormatted: "req_animal-api-sync-42cac",
stats: {
    "name": "Animal-api sync for customer NL_117138",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "8102",
        "ok": "8102",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "8102",
        "ok": "8102",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "8102",
        "ok": "8102",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "8102",
        "ok": "8102",
        "ko": "-"
    },
    "percentiles2": {
        "total": "8102",
        "ok": "8102",
        "ko": "-"
    },
    "percentiles3": {
        "total": "8102",
        "ok": "8102",
        "ko": "-"
    },
    "percentiles4": {
        "total": "8102",
        "ok": "8102",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-7ddaa": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_127532",
path: "Animal-api sync for customer NL_127532",
pathFormatted: "req_animal-api-sync-7ddaa",
stats: {
    "name": "Animal-api sync for customer NL_127532",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "8379",
        "ok": "8379",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "8379",
        "ok": "8379",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "8379",
        "ok": "8379",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "8379",
        "ok": "8379",
        "ko": "-"
    },
    "percentiles2": {
        "total": "8379",
        "ok": "8379",
        "ko": "-"
    },
    "percentiles3": {
        "total": "8379",
        "ok": "8379",
        "ko": "-"
    },
    "percentiles4": {
        "total": "8379",
        "ok": "8379",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-35067": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_145405",
path: "Animal-api sync for customer BE_145405",
pathFormatted: "req_animal-api-sync-35067",
stats: {
    "name": "Animal-api sync for customer BE_145405",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "12416",
        "ok": "12416",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "12416",
        "ok": "12416",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "12416",
        "ok": "12416",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "12416",
        "ok": "12416",
        "ko": "-"
    },
    "percentiles2": {
        "total": "12416",
        "ok": "12416",
        "ko": "-"
    },
    "percentiles3": {
        "total": "12416",
        "ok": "12416",
        "ko": "-"
    },
    "percentiles4": {
        "total": "12416",
        "ok": "12416",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-2e40c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_131445",
path: "Animal-api sync for customer NL_131445",
pathFormatted: "req_animal-api-sync-2e40c",
stats: {
    "name": "Animal-api sync for customer NL_131445",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "7867",
        "ok": "7867",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "7867",
        "ok": "7867",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "7867",
        "ok": "7867",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "7867",
        "ok": "7867",
        "ko": "-"
    },
    "percentiles2": {
        "total": "7867",
        "ok": "7867",
        "ko": "-"
    },
    "percentiles3": {
        "total": "7867",
        "ok": "7867",
        "ko": "-"
    },
    "percentiles4": {
        "total": "7867",
        "ok": "7867",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-3eb24": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_103773",
path: "Animal-api sync for customer NL_103773",
pathFormatted: "req_animal-api-sync-3eb24",
stats: {
    "name": "Animal-api sync for customer NL_103773",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "15325",
        "ok": "15325",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "15325",
        "ok": "15325",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "15325",
        "ok": "15325",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "15325",
        "ok": "15325",
        "ko": "-"
    },
    "percentiles2": {
        "total": "15325",
        "ok": "15325",
        "ko": "-"
    },
    "percentiles3": {
        "total": "15325",
        "ok": "15325",
        "ko": "-"
    },
    "percentiles4": {
        "total": "15325",
        "ok": "15325",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-db524": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104670",
path: "Animal-api sync for customer NL_104670",
pathFormatted: "req_animal-api-sync-db524",
stats: {
    "name": "Animal-api sync for customer NL_104670",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "maxResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "meanResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles2": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles3": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles4": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-8824d": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_129197",
path: "Animal-api sync for customer NL_129197",
pathFormatted: "req_animal-api-sync-8824d",
stats: {
    "name": "Animal-api sync for customer NL_129197",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "maxResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "meanResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles2": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles3": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles4": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-d2007": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_214023",
path: "Animal-api sync for customer BE_214023",
pathFormatted: "req_animal-api-sync-d2007",
stats: {
    "name": "Animal-api sync for customer BE_214023",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "30520",
        "ok": "30520",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "30520",
        "ok": "30520",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "30520",
        "ok": "30520",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "30520",
        "ok": "30520",
        "ko": "-"
    },
    "percentiles2": {
        "total": "30520",
        "ok": "30520",
        "ko": "-"
    },
    "percentiles3": {
        "total": "30520",
        "ok": "30520",
        "ko": "-"
    },
    "percentiles4": {
        "total": "30520",
        "ok": "30520",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e79fc": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_114849",
path: "Animal-api sync for customer NL_114849",
pathFormatted: "req_animal-api-sync-e79fc",
stats: {
    "name": "Animal-api sync for customer NL_114849",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "6971",
        "ok": "6971",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "6971",
        "ok": "6971",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "6971",
        "ok": "6971",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "6971",
        "ok": "6971",
        "ko": "-"
    },
    "percentiles2": {
        "total": "6971",
        "ok": "6971",
        "ko": "-"
    },
    "percentiles3": {
        "total": "6971",
        "ok": "6971",
        "ko": "-"
    },
    "percentiles4": {
        "total": "6971",
        "ok": "6971",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-6b259": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_118526",
path: "Animal-api sync for customer NL_118526",
pathFormatted: "req_animal-api-sync-6b259",
stats: {
    "name": "Animal-api sync for customer NL_118526",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2599",
        "ok": "2599",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2599",
        "ok": "2599",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2599",
        "ok": "2599",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2599",
        "ok": "2599",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2599",
        "ok": "2599",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2599",
        "ok": "2599",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2599",
        "ok": "2599",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-1f585": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_123243",
path: "Animal-api sync for customer NL_123243",
pathFormatted: "req_animal-api-sync-1f585",
stats: {
    "name": "Animal-api sync for customer NL_123243",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5575",
        "ok": "5575",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5575",
        "ok": "5575",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5575",
        "ok": "5575",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5575",
        "ok": "5575",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5575",
        "ok": "5575",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5575",
        "ok": "5575",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5575",
        "ok": "5575",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-360a2": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_120728",
path: "Animal-api sync for customer NL_120728",
pathFormatted: "req_animal-api-sync-360a2",
stats: {
    "name": "Animal-api sync for customer NL_120728",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "maxResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "meanResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles2": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles3": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles4": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-964ea": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_131647",
path: "Animal-api sync for customer NL_131647",
pathFormatted: "req_animal-api-sync-964ea",
stats: {
    "name": "Animal-api sync for customer NL_131647",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "7088",
        "ok": "7088",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "7088",
        "ok": "7088",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "7088",
        "ok": "7088",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "7088",
        "ok": "7088",
        "ko": "-"
    },
    "percentiles2": {
        "total": "7088",
        "ok": "7088",
        "ko": "-"
    },
    "percentiles3": {
        "total": "7088",
        "ok": "7088",
        "ko": "-"
    },
    "percentiles4": {
        "total": "7088",
        "ok": "7088",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-90a7a": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_158870",
path: "Animal-api sync for customer NL_158870",
pathFormatted: "req_animal-api-sync-90a7a",
stats: {
    "name": "Animal-api sync for customer NL_158870",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "maxResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "meanResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles4": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-ee74d": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_149842",
path: "Animal-api sync for customer BE_149842",
pathFormatted: "req_animal-api-sync-ee74d",
stats: {
    "name": "Animal-api sync for customer BE_149842",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "9431",
        "ok": "9431",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "9431",
        "ok": "9431",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "9431",
        "ok": "9431",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "9431",
        "ok": "9431",
        "ko": "-"
    },
    "percentiles2": {
        "total": "9431",
        "ok": "9431",
        "ko": "-"
    },
    "percentiles3": {
        "total": "9431",
        "ok": "9431",
        "ko": "-"
    },
    "percentiles4": {
        "total": "9431",
        "ok": "9431",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-edbd5": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_156716",
path: "Animal-api sync for customer NL_156716",
pathFormatted: "req_animal-api-sync-edbd5",
stats: {
    "name": "Animal-api sync for customer NL_156716",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "maxResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "meanResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles2": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles3": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles4": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-b6490": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_128966",
path: "Animal-api sync for customer NL_128966",
pathFormatted: "req_animal-api-sync-b6490",
stats: {
    "name": "Animal-api sync for customer NL_128966",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "7978",
        "ok": "7978",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "7978",
        "ok": "7978",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "7978",
        "ok": "7978",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "7978",
        "ok": "7978",
        "ko": "-"
    },
    "percentiles2": {
        "total": "7978",
        "ok": "7978",
        "ko": "-"
    },
    "percentiles3": {
        "total": "7978",
        "ok": "7978",
        "ko": "-"
    },
    "percentiles4": {
        "total": "7978",
        "ok": "7978",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-5eb0d": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_129758",
path: "Animal-api sync for customer NL_129758",
pathFormatted: "req_animal-api-sync-5eb0d",
stats: {
    "name": "Animal-api sync for customer NL_129758",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "15410",
        "ok": "15410",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "15410",
        "ok": "15410",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "15410",
        "ok": "15410",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "15410",
        "ok": "15410",
        "ko": "-"
    },
    "percentiles2": {
        "total": "15410",
        "ok": "15410",
        "ko": "-"
    },
    "percentiles3": {
        "total": "15410",
        "ok": "15410",
        "ko": "-"
    },
    "percentiles4": {
        "total": "15410",
        "ok": "15410",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-7665a": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_107608",
path: "Animal-api sync for customer NL_107608",
pathFormatted: "req_animal-api-sync-7665a",
stats: {
    "name": "Animal-api sync for customer NL_107608",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "7580",
        "ok": "7580",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "7580",
        "ok": "7580",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "7580",
        "ok": "7580",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "7580",
        "ok": "7580",
        "ko": "-"
    },
    "percentiles2": {
        "total": "7580",
        "ok": "7580",
        "ko": "-"
    },
    "percentiles3": {
        "total": "7580",
        "ok": "7580",
        "ko": "-"
    },
    "percentiles4": {
        "total": "7580",
        "ok": "7580",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-352f1": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_120975",
path: "Animal-api sync for customer NL_120975",
pathFormatted: "req_animal-api-sync-352f1",
stats: {
    "name": "Animal-api sync for customer NL_120975",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "maxResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "meanResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles2": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles3": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles4": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-a1b1c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105501",
path: "Animal-api sync for customer NL_105501",
pathFormatted: "req_animal-api-sync-a1b1c",
stats: {
    "name": "Animal-api sync for customer NL_105501",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "9795",
        "ok": "9795",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "9795",
        "ok": "9795",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "9795",
        "ok": "9795",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "9795",
        "ok": "9795",
        "ko": "-"
    },
    "percentiles2": {
        "total": "9795",
        "ok": "9795",
        "ko": "-"
    },
    "percentiles3": {
        "total": "9795",
        "ok": "9795",
        "ko": "-"
    },
    "percentiles4": {
        "total": "9795",
        "ok": "9795",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-558ec": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_115428",
path: "Animal-api sync for customer NL_115428",
pathFormatted: "req_animal-api-sync-558ec",
stats: {
    "name": "Animal-api sync for customer NL_115428",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1905",
        "ok": "1905",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1905",
        "ok": "1905",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1905",
        "ok": "1905",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1905",
        "ok": "1905",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1905",
        "ok": "1905",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1905",
        "ok": "1905",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1905",
        "ok": "1905",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e201e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110096",
path: "Animal-api sync for customer NL_110096",
pathFormatted: "req_animal-api-sync-e201e",
stats: {
    "name": "Animal-api sync for customer NL_110096",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "25374",
        "ok": "25374",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "25374",
        "ok": "25374",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "25374",
        "ok": "25374",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "25374",
        "ok": "25374",
        "ko": "-"
    },
    "percentiles2": {
        "total": "25374",
        "ok": "25374",
        "ko": "-"
    },
    "percentiles3": {
        "total": "25374",
        "ok": "25374",
        "ko": "-"
    },
    "percentiles4": {
        "total": "25374",
        "ok": "25374",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-66a67": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_115297",
path: "Animal-api sync for customer NL_115297",
pathFormatted: "req_animal-api-sync-66a67",
stats: {
    "name": "Animal-api sync for customer NL_115297",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "9621",
        "ok": "9621",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "9621",
        "ok": "9621",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "9621",
        "ok": "9621",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "9621",
        "ok": "9621",
        "ko": "-"
    },
    "percentiles2": {
        "total": "9621",
        "ok": "9621",
        "ko": "-"
    },
    "percentiles3": {
        "total": "9621",
        "ok": "9621",
        "ko": "-"
    },
    "percentiles4": {
        "total": "9621",
        "ok": "9621",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e614b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_114481",
path: "Animal-api sync for customer NL_114481",
pathFormatted: "req_animal-api-sync-e614b",
stats: {
    "name": "Animal-api sync for customer NL_114481",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "7225",
        "ok": "7225",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "7225",
        "ok": "7225",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "7225",
        "ok": "7225",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "7225",
        "ok": "7225",
        "ko": "-"
    },
    "percentiles2": {
        "total": "7225",
        "ok": "7225",
        "ko": "-"
    },
    "percentiles3": {
        "total": "7225",
        "ok": "7225",
        "ko": "-"
    },
    "percentiles4": {
        "total": "7225",
        "ok": "7225",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a326b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_109292",
path: "Animal-api sync for customer NL_109292",
pathFormatted: "req_animal-api-sync-a326b",
stats: {
    "name": "Animal-api sync for customer NL_109292",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "11520",
        "ok": "11520",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "11520",
        "ok": "11520",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "11520",
        "ok": "11520",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "11520",
        "ok": "11520",
        "ko": "-"
    },
    "percentiles2": {
        "total": "11520",
        "ok": "11520",
        "ko": "-"
    },
    "percentiles3": {
        "total": "11520",
        "ok": "11520",
        "ko": "-"
    },
    "percentiles4": {
        "total": "11520",
        "ok": "11520",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-ec201": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105082",
path: "Animal-api sync for customer NL_105082",
pathFormatted: "req_animal-api-sync-ec201",
stats: {
    "name": "Animal-api sync for customer NL_105082",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "maxResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "meanResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles2": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles3": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles4": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-c9dab": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_122464",
path: "Animal-api sync for customer NL_122464",
pathFormatted: "req_animal-api-sync-c9dab",
stats: {
    "name": "Animal-api sync for customer NL_122464",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "34585",
        "ok": "34585",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "34585",
        "ok": "34585",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "34585",
        "ok": "34585",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "34585",
        "ok": "34585",
        "ko": "-"
    },
    "percentiles2": {
        "total": "34585",
        "ok": "34585",
        "ko": "-"
    },
    "percentiles3": {
        "total": "34585",
        "ok": "34585",
        "ko": "-"
    },
    "percentiles4": {
        "total": "34585",
        "ok": "34585",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-2092d": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_150518",
path: "Animal-api sync for customer BE_150518",
pathFormatted: "req_animal-api-sync-2092d",
stats: {
    "name": "Animal-api sync for customer BE_150518",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "7148",
        "ok": "7148",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "7148",
        "ok": "7148",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "7148",
        "ok": "7148",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "7148",
        "ok": "7148",
        "ko": "-"
    },
    "percentiles2": {
        "total": "7148",
        "ok": "7148",
        "ko": "-"
    },
    "percentiles3": {
        "total": "7148",
        "ok": "7148",
        "ko": "-"
    },
    "percentiles4": {
        "total": "7148",
        "ok": "7148",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-cdf93": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_118194",
path: "Animal-api sync for customer NL_118194",
pathFormatted: "req_animal-api-sync-cdf93",
stats: {
    "name": "Animal-api sync for customer NL_118194",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "27809",
        "ok": "27809",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "27809",
        "ok": "27809",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "27809",
        "ok": "27809",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "27809",
        "ok": "27809",
        "ko": "-"
    },
    "percentiles2": {
        "total": "27809",
        "ok": "27809",
        "ko": "-"
    },
    "percentiles3": {
        "total": "27809",
        "ok": "27809",
        "ko": "-"
    },
    "percentiles4": {
        "total": "27809",
        "ok": "27809",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-ac6d4": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_121952",
path: "Animal-api sync for customer NL_121952",
pathFormatted: "req_animal-api-sync-ac6d4",
stats: {
    "name": "Animal-api sync for customer NL_121952",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "27137",
        "ok": "27137",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "27137",
        "ok": "27137",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "27137",
        "ok": "27137",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "27137",
        "ok": "27137",
        "ko": "-"
    },
    "percentiles2": {
        "total": "27137",
        "ok": "27137",
        "ko": "-"
    },
    "percentiles3": {
        "total": "27137",
        "ok": "27137",
        "ko": "-"
    },
    "percentiles4": {
        "total": "27137",
        "ok": "27137",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-0ec02": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_114079",
path: "Animal-api sync for customer NL_114079",
pathFormatted: "req_animal-api-sync-0ec02",
stats: {
    "name": "Animal-api sync for customer NL_114079",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2758",
        "ok": "2758",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2758",
        "ok": "2758",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2758",
        "ok": "2758",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2758",
        "ok": "2758",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2758",
        "ok": "2758",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2758",
        "ok": "2758",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2758",
        "ok": "2758",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-2901a": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_136544",
path: "Animal-api sync for customer NL_136544",
pathFormatted: "req_animal-api-sync-2901a",
stats: {
    "name": "Animal-api sync for customer NL_136544",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "6190",
        "ok": "6190",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "6190",
        "ok": "6190",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "6190",
        "ok": "6190",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "6190",
        "ok": "6190",
        "ko": "-"
    },
    "percentiles2": {
        "total": "6190",
        "ok": "6190",
        "ko": "-"
    },
    "percentiles3": {
        "total": "6190",
        "ok": "6190",
        "ko": "-"
    },
    "percentiles4": {
        "total": "6190",
        "ok": "6190",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-1a27a": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_120522",
path: "Animal-api sync for customer NL_120522",
pathFormatted: "req_animal-api-sync-1a27a",
stats: {
    "name": "Animal-api sync for customer NL_120522",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "10430",
        "ok": "10430",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "10430",
        "ok": "10430",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "10430",
        "ok": "10430",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "10430",
        "ok": "10430",
        "ko": "-"
    },
    "percentiles2": {
        "total": "10430",
        "ok": "10430",
        "ko": "-"
    },
    "percentiles3": {
        "total": "10430",
        "ok": "10430",
        "ko": "-"
    },
    "percentiles4": {
        "total": "10430",
        "ok": "10430",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-317c3": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_132801",
path: "Animal-api sync for customer NL_132801",
pathFormatted: "req_animal-api-sync-317c3",
stats: {
    "name": "Animal-api sync for customer NL_132801",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4847",
        "ok": "4847",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4847",
        "ok": "4847",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4847",
        "ok": "4847",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4847",
        "ok": "4847",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4847",
        "ok": "4847",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4847",
        "ok": "4847",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4847",
        "ok": "4847",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-83baa": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_114916",
path: "Animal-api sync for customer NL_114916",
pathFormatted: "req_animal-api-sync-83baa",
stats: {
    "name": "Animal-api sync for customer NL_114916",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "maxResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "meanResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles2": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles3": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles4": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-3eaed": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_108223",
path: "Animal-api sync for customer NL_108223",
pathFormatted: "req_animal-api-sync-3eaed",
stats: {
    "name": "Animal-api sync for customer NL_108223",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "maxResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "meanResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles2": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles3": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles4": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-6ad60": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_124372",
path: "Animal-api sync for customer NL_124372",
pathFormatted: "req_animal-api-sync-6ad60",
stats: {
    "name": "Animal-api sync for customer NL_124372",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5093",
        "ok": "5093",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5093",
        "ok": "5093",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5093",
        "ok": "5093",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5093",
        "ok": "5093",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5093",
        "ok": "5093",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5093",
        "ok": "5093",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5093",
        "ok": "5093",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-967fa": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_113311",
path: "Animal-api sync for customer NL_113311",
pathFormatted: "req_animal-api-sync-967fa",
stats: {
    "name": "Animal-api sync for customer NL_113311",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "8196",
        "ok": "8196",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "8196",
        "ok": "8196",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "8196",
        "ok": "8196",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "8196",
        "ok": "8196",
        "ko": "-"
    },
    "percentiles2": {
        "total": "8196",
        "ok": "8196",
        "ko": "-"
    },
    "percentiles3": {
        "total": "8196",
        "ok": "8196",
        "ko": "-"
    },
    "percentiles4": {
        "total": "8196",
        "ok": "8196",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-3736b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_114775",
path: "Animal-api sync for customer NL_114775",
pathFormatted: "req_animal-api-sync-3736b",
stats: {
    "name": "Animal-api sync for customer NL_114775",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "44687",
        "ok": "44687",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "44687",
        "ok": "44687",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "44687",
        "ok": "44687",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "44687",
        "ok": "44687",
        "ko": "-"
    },
    "percentiles2": {
        "total": "44687",
        "ok": "44687",
        "ko": "-"
    },
    "percentiles3": {
        "total": "44687",
        "ok": "44687",
        "ko": "-"
    },
    "percentiles4": {
        "total": "44687",
        "ok": "44687",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-2e3f2": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_124578",
path: "Animal-api sync for customer NL_124578",
pathFormatted: "req_animal-api-sync-2e3f2",
stats: {
    "name": "Animal-api sync for customer NL_124578",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "7180",
        "ok": "7180",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "7180",
        "ok": "7180",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "7180",
        "ok": "7180",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "7180",
        "ok": "7180",
        "ko": "-"
    },
    "percentiles2": {
        "total": "7180",
        "ok": "7180",
        "ko": "-"
    },
    "percentiles3": {
        "total": "7180",
        "ok": "7180",
        "ko": "-"
    },
    "percentiles4": {
        "total": "7180",
        "ok": "7180",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-f6bd1": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_118770",
path: "Animal-api sync for customer NL_118770",
pathFormatted: "req_animal-api-sync-f6bd1",
stats: {
    "name": "Animal-api sync for customer NL_118770",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "35156",
        "ok": "35156",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "35156",
        "ok": "35156",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "35156",
        "ok": "35156",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "35156",
        "ok": "35156",
        "ko": "-"
    },
    "percentiles2": {
        "total": "35156",
        "ok": "35156",
        "ko": "-"
    },
    "percentiles3": {
        "total": "35156",
        "ok": "35156",
        "ko": "-"
    },
    "percentiles4": {
        "total": "35156",
        "ok": "35156",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-cfdfb": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111312",
path: "Animal-api sync for customer NL_111312",
pathFormatted: "req_animal-api-sync-cfdfb",
stats: {
    "name": "Animal-api sync for customer NL_111312",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "12804",
        "ok": "12804",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "12804",
        "ok": "12804",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "12804",
        "ok": "12804",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "12804",
        "ok": "12804",
        "ko": "-"
    },
    "percentiles2": {
        "total": "12804",
        "ok": "12804",
        "ko": "-"
    },
    "percentiles3": {
        "total": "12804",
        "ok": "12804",
        "ko": "-"
    },
    "percentiles4": {
        "total": "12804",
        "ok": "12804",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c9778": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_119273",
path: "Animal-api sync for customer NL_119273",
pathFormatted: "req_animal-api-sync-c9778",
stats: {
    "name": "Animal-api sync for customer NL_119273",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "9703",
        "ok": "9703",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "9703",
        "ok": "9703",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "9703",
        "ok": "9703",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "9703",
        "ok": "9703",
        "ko": "-"
    },
    "percentiles2": {
        "total": "9703",
        "ok": "9703",
        "ko": "-"
    },
    "percentiles3": {
        "total": "9703",
        "ok": "9703",
        "ko": "-"
    },
    "percentiles4": {
        "total": "9703",
        "ok": "9703",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-39d31": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_118474",
path: "Animal-api sync for customer NL_118474",
pathFormatted: "req_animal-api-sync-39d31",
stats: {
    "name": "Animal-api sync for customer NL_118474",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "10080",
        "ok": "10080",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "10080",
        "ok": "10080",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "10080",
        "ok": "10080",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "10080",
        "ok": "10080",
        "ko": "-"
    },
    "percentiles2": {
        "total": "10080",
        "ok": "10080",
        "ko": "-"
    },
    "percentiles3": {
        "total": "10080",
        "ok": "10080",
        "ko": "-"
    },
    "percentiles4": {
        "total": "10080",
        "ok": "10080",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-53fa3": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_108741",
path: "Animal-api sync for customer NL_108741",
pathFormatted: "req_animal-api-sync-53fa3",
stats: {
    "name": "Animal-api sync for customer NL_108741",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "48500",
        "ok": "48500",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "48500",
        "ok": "48500",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "48500",
        "ok": "48500",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "48500",
        "ok": "48500",
        "ko": "-"
    },
    "percentiles2": {
        "total": "48500",
        "ok": "48500",
        "ko": "-"
    },
    "percentiles3": {
        "total": "48500",
        "ok": "48500",
        "ko": "-"
    },
    "percentiles4": {
        "total": "48500",
        "ok": "48500",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-99401": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111178",
path: "Animal-api sync for customer NL_111178",
pathFormatted: "req_animal-api-sync-99401",
stats: {
    "name": "Animal-api sync for customer NL_111178",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "maxResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "meanResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles2": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles3": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles4": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-09c2e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_135864",
path: "Animal-api sync for customer NL_135864",
pathFormatted: "req_animal-api-sync-09c2e",
stats: {
    "name": "Animal-api sync for customer NL_135864",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "34537",
        "ok": "34537",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "34537",
        "ok": "34537",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "34537",
        "ok": "34537",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "34537",
        "ok": "34537",
        "ko": "-"
    },
    "percentiles2": {
        "total": "34537",
        "ok": "34537",
        "ko": "-"
    },
    "percentiles3": {
        "total": "34537",
        "ok": "34537",
        "ko": "-"
    },
    "percentiles4": {
        "total": "34537",
        "ok": "34537",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-236d8": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_124049",
path: "Animal-api sync for customer NL_124049",
pathFormatted: "req_animal-api-sync-236d8",
stats: {
    "name": "Animal-api sync for customer NL_124049",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "17729",
        "ok": "17729",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "17729",
        "ok": "17729",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "17729",
        "ok": "17729",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "17729",
        "ok": "17729",
        "ko": "-"
    },
    "percentiles2": {
        "total": "17729",
        "ok": "17729",
        "ko": "-"
    },
    "percentiles3": {
        "total": "17729",
        "ok": "17729",
        "ko": "-"
    },
    "percentiles4": {
        "total": "17729",
        "ok": "17729",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-882c2": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_128097",
path: "Animal-api sync for customer NL_128097",
pathFormatted: "req_animal-api-sync-882c2",
stats: {
    "name": "Animal-api sync for customer NL_128097",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4378",
        "ok": "4378",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4378",
        "ok": "4378",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4378",
        "ok": "4378",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4378",
        "ok": "4378",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4378",
        "ok": "4378",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4378",
        "ok": "4378",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4378",
        "ok": "4378",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e7614": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_159766",
path: "Animal-api sync for customer BE_159766",
pathFormatted: "req_animal-api-sync-e7614",
stats: {
    "name": "Animal-api sync for customer BE_159766",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "maxResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "meanResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles2": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles3": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles4": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-52a69": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_212921",
path: "Animal-api sync for customer BE_212921",
pathFormatted: "req_animal-api-sync-52a69",
stats: {
    "name": "Animal-api sync for customer BE_212921",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "7896",
        "ok": "7896",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "7896",
        "ok": "7896",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "7896",
        "ok": "7896",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "7896",
        "ok": "7896",
        "ko": "-"
    },
    "percentiles2": {
        "total": "7896",
        "ok": "7896",
        "ko": "-"
    },
    "percentiles3": {
        "total": "7896",
        "ok": "7896",
        "ko": "-"
    },
    "percentiles4": {
        "total": "7896",
        "ok": "7896",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-53e2f": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_127894",
path: "Animal-api sync for customer NL_127894",
pathFormatted: "req_animal-api-sync-53e2f",
stats: {
    "name": "Animal-api sync for customer NL_127894",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "11846",
        "ok": "11846",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "11846",
        "ok": "11846",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "11846",
        "ok": "11846",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "11846",
        "ok": "11846",
        "ko": "-"
    },
    "percentiles2": {
        "total": "11846",
        "ok": "11846",
        "ko": "-"
    },
    "percentiles3": {
        "total": "11846",
        "ok": "11846",
        "ko": "-"
    },
    "percentiles4": {
        "total": "11846",
        "ok": "11846",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b4a99": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104247",
path: "Animal-api sync for customer NL_104247",
pathFormatted: "req_animal-api-sync-b4a99",
stats: {
    "name": "Animal-api sync for customer NL_104247",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "14263",
        "ok": "14263",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "14263",
        "ok": "14263",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "14263",
        "ok": "14263",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "14263",
        "ok": "14263",
        "ko": "-"
    },
    "percentiles2": {
        "total": "14263",
        "ok": "14263",
        "ko": "-"
    },
    "percentiles3": {
        "total": "14263",
        "ok": "14263",
        "ko": "-"
    },
    "percentiles4": {
        "total": "14263",
        "ok": "14263",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c4c5c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_122906",
path: "Animal-api sync for customer NL_122906",
pathFormatted: "req_animal-api-sync-c4c5c",
stats: {
    "name": "Animal-api sync for customer NL_122906",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "12813",
        "ok": "12813",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "12813",
        "ok": "12813",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "12813",
        "ok": "12813",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "12813",
        "ok": "12813",
        "ko": "-"
    },
    "percentiles2": {
        "total": "12813",
        "ok": "12813",
        "ko": "-"
    },
    "percentiles3": {
        "total": "12813",
        "ok": "12813",
        "ko": "-"
    },
    "percentiles4": {
        "total": "12813",
        "ok": "12813",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e42dd": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_108296",
path: "Animal-api sync for customer NL_108296",
pathFormatted: "req_animal-api-sync-e42dd",
stats: {
    "name": "Animal-api sync for customer NL_108296",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2528",
        "ok": "2528",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2528",
        "ok": "2528",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2528",
        "ok": "2528",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2528",
        "ok": "2528",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2528",
        "ok": "2528",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2528",
        "ok": "2528",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2528",
        "ok": "2528",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b8f6b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_120259",
path: "Animal-api sync for customer NL_120259",
pathFormatted: "req_animal-api-sync-b8f6b",
stats: {
    "name": "Animal-api sync for customer NL_120259",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "10068",
        "ok": "10068",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "10068",
        "ok": "10068",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "10068",
        "ok": "10068",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "10068",
        "ok": "10068",
        "ko": "-"
    },
    "percentiles2": {
        "total": "10068",
        "ok": "10068",
        "ko": "-"
    },
    "percentiles3": {
        "total": "10068",
        "ok": "10068",
        "ko": "-"
    },
    "percentiles4": {
        "total": "10068",
        "ok": "10068",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-10dc5": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_113215",
path: "Animal-api sync for customer NL_113215",
pathFormatted: "req_animal-api-sync-10dc5",
stats: {
    "name": "Animal-api sync for customer NL_113215",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "maxResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "meanResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles2": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles3": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles4": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-ee676": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_119868",
path: "Animal-api sync for customer NL_119868",
pathFormatted: "req_animal-api-sync-ee676",
stats: {
    "name": "Animal-api sync for customer NL_119868",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "32885",
        "ok": "32885",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "32885",
        "ok": "32885",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "32885",
        "ok": "32885",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "32885",
        "ok": "32885",
        "ko": "-"
    },
    "percentiles2": {
        "total": "32885",
        "ok": "32885",
        "ko": "-"
    },
    "percentiles3": {
        "total": "32885",
        "ok": "32885",
        "ko": "-"
    },
    "percentiles4": {
        "total": "32885",
        "ok": "32885",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-12c8b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_107507",
path: "Animal-api sync for customer NL_107507",
pathFormatted: "req_animal-api-sync-12c8b",
stats: {
    "name": "Animal-api sync for customer NL_107507",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "maxResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "meanResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles4": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-70c3b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_108064",
path: "Animal-api sync for customer NL_108064",
pathFormatted: "req_animal-api-sync-70c3b",
stats: {
    "name": "Animal-api sync for customer NL_108064",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "32657",
        "ok": "32657",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "32657",
        "ok": "32657",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "32657",
        "ok": "32657",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "32657",
        "ok": "32657",
        "ko": "-"
    },
    "percentiles2": {
        "total": "32657",
        "ok": "32657",
        "ko": "-"
    },
    "percentiles3": {
        "total": "32657",
        "ok": "32657",
        "ko": "-"
    },
    "percentiles4": {
        "total": "32657",
        "ok": "32657",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d5bee": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_114423",
path: "Animal-api sync for customer NL_114423",
pathFormatted: "req_animal-api-sync-d5bee",
stats: {
    "name": "Animal-api sync for customer NL_114423",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2710",
        "ok": "2710",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2710",
        "ok": "2710",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2710",
        "ok": "2710",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2710",
        "ok": "2710",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2710",
        "ok": "2710",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2710",
        "ok": "2710",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2710",
        "ok": "2710",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-f94cb": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_107241",
path: "Animal-api sync for customer NL_107241",
pathFormatted: "req_animal-api-sync-f94cb",
stats: {
    "name": "Animal-api sync for customer NL_107241",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21229",
        "ok": "21229",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "21229",
        "ok": "21229",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "21229",
        "ok": "21229",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "21229",
        "ok": "21229",
        "ko": "-"
    },
    "percentiles2": {
        "total": "21229",
        "ok": "21229",
        "ko": "-"
    },
    "percentiles3": {
        "total": "21229",
        "ok": "21229",
        "ko": "-"
    },
    "percentiles4": {
        "total": "21229",
        "ok": "21229",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-4b1f5": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_129590",
path: "Animal-api sync for customer NL_129590",
pathFormatted: "req_animal-api-sync-4b1f5",
stats: {
    "name": "Animal-api sync for customer NL_129590",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "51576",
        "ok": "51576",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "51576",
        "ok": "51576",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "51576",
        "ok": "51576",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "51576",
        "ok": "51576",
        "ko": "-"
    },
    "percentiles2": {
        "total": "51576",
        "ok": "51576",
        "ko": "-"
    },
    "percentiles3": {
        "total": "51576",
        "ok": "51576",
        "ko": "-"
    },
    "percentiles4": {
        "total": "51576",
        "ok": "51576",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-25bc2": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111740",
path: "Animal-api sync for customer NL_111740",
pathFormatted: "req_animal-api-sync-25bc2",
stats: {
    "name": "Animal-api sync for customer NL_111740",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1955",
        "ok": "1955",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1955",
        "ok": "1955",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1955",
        "ok": "1955",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1955",
        "ok": "1955",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1955",
        "ok": "1955",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1955",
        "ok": "1955",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1955",
        "ok": "1955",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-2efa4": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117125",
path: "Animal-api sync for customer NL_117125",
pathFormatted: "req_animal-api-sync-2efa4",
stats: {
    "name": "Animal-api sync for customer NL_117125",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "24266",
        "ok": "24266",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "24266",
        "ok": "24266",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "24266",
        "ok": "24266",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "24266",
        "ok": "24266",
        "ko": "-"
    },
    "percentiles2": {
        "total": "24266",
        "ok": "24266",
        "ko": "-"
    },
    "percentiles3": {
        "total": "24266",
        "ok": "24266",
        "ko": "-"
    },
    "percentiles4": {
        "total": "24266",
        "ok": "24266",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c4fc3": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_216527",
path: "Animal-api sync for customer NL_216527",
pathFormatted: "req_animal-api-sync-c4fc3",
stats: {
    "name": "Animal-api sync for customer NL_216527",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "20933",
        "ok": "20933",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "20933",
        "ok": "20933",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "20933",
        "ok": "20933",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "20933",
        "ok": "20933",
        "ko": "-"
    },
    "percentiles2": {
        "total": "20933",
        "ok": "20933",
        "ko": "-"
    },
    "percentiles3": {
        "total": "20933",
        "ok": "20933",
        "ko": "-"
    },
    "percentiles4": {
        "total": "20933",
        "ok": "20933",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-41435": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_119693",
path: "Animal-api sync for customer NL_119693",
pathFormatted: "req_animal-api-sync-41435",
stats: {
    "name": "Animal-api sync for customer NL_119693",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "13283",
        "ok": "13283",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "13283",
        "ok": "13283",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "13283",
        "ok": "13283",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "13283",
        "ok": "13283",
        "ko": "-"
    },
    "percentiles2": {
        "total": "13283",
        "ok": "13283",
        "ko": "-"
    },
    "percentiles3": {
        "total": "13283",
        "ok": "13283",
        "ko": "-"
    },
    "percentiles4": {
        "total": "13283",
        "ok": "13283",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e3e3d": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_126029",
path: "Animal-api sync for customer NL_126029",
pathFormatted: "req_animal-api-sync-e3e3d",
stats: {
    "name": "Animal-api sync for customer NL_126029",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "maxResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "meanResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles2": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles3": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles4": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-d8bef": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_114961",
path: "Animal-api sync for customer NL_114961",
pathFormatted: "req_animal-api-sync-d8bef",
stats: {
    "name": "Animal-api sync for customer NL_114961",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "15670",
        "ok": "15670",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "15670",
        "ok": "15670",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "15670",
        "ok": "15670",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "15670",
        "ok": "15670",
        "ko": "-"
    },
    "percentiles2": {
        "total": "15670",
        "ok": "15670",
        "ko": "-"
    },
    "percentiles3": {
        "total": "15670",
        "ok": "15670",
        "ko": "-"
    },
    "percentiles4": {
        "total": "15670",
        "ok": "15670",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-40763": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_127944",
path: "Animal-api sync for customer NL_127944",
pathFormatted: "req_animal-api-sync-40763",
stats: {
    "name": "Animal-api sync for customer NL_127944",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3122",
        "ok": "3122",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3122",
        "ok": "3122",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3122",
        "ok": "3122",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3122",
        "ok": "3122",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3122",
        "ok": "3122",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3122",
        "ok": "3122",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3122",
        "ok": "3122",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-9f4a5": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_108715",
path: "Animal-api sync for customer NL_108715",
pathFormatted: "req_animal-api-sync-9f4a5",
stats: {
    "name": "Animal-api sync for customer NL_108715",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "41304",
        "ok": "41304",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "41304",
        "ok": "41304",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "41304",
        "ok": "41304",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "41304",
        "ok": "41304",
        "ko": "-"
    },
    "percentiles2": {
        "total": "41304",
        "ok": "41304",
        "ko": "-"
    },
    "percentiles3": {
        "total": "41304",
        "ok": "41304",
        "ko": "-"
    },
    "percentiles4": {
        "total": "41304",
        "ok": "41304",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-0355c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_133793",
path: "Animal-api sync for customer NL_133793",
pathFormatted: "req_animal-api-sync-0355c",
stats: {
    "name": "Animal-api sync for customer NL_133793",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "25883",
        "ok": "25883",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "25883",
        "ok": "25883",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "25883",
        "ok": "25883",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "25883",
        "ok": "25883",
        "ko": "-"
    },
    "percentiles2": {
        "total": "25883",
        "ok": "25883",
        "ko": "-"
    },
    "percentiles3": {
        "total": "25883",
        "ok": "25883",
        "ko": "-"
    },
    "percentiles4": {
        "total": "25883",
        "ok": "25883",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-6bc35": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_127470",
path: "Animal-api sync for customer NL_127470",
pathFormatted: "req_animal-api-sync-6bc35",
stats: {
    "name": "Animal-api sync for customer NL_127470",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "10563",
        "ok": "10563",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "10563",
        "ok": "10563",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "10563",
        "ok": "10563",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "10563",
        "ok": "10563",
        "ko": "-"
    },
    "percentiles2": {
        "total": "10563",
        "ok": "10563",
        "ko": "-"
    },
    "percentiles3": {
        "total": "10563",
        "ok": "10563",
        "ko": "-"
    },
    "percentiles4": {
        "total": "10563",
        "ok": "10563",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-7eb9d": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104934",
path: "Animal-api sync for customer NL_104934",
pathFormatted: "req_animal-api-sync-7eb9d",
stats: {
    "name": "Animal-api sync for customer NL_104934",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "maxResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "meanResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles2": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles3": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles4": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-3a032": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_122396",
path: "Animal-api sync for customer NL_122396",
pathFormatted: "req_animal-api-sync-3a032",
stats: {
    "name": "Animal-api sync for customer NL_122396",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "9710",
        "ok": "9710",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "9710",
        "ok": "9710",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "9710",
        "ok": "9710",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "9710",
        "ok": "9710",
        "ko": "-"
    },
    "percentiles2": {
        "total": "9710",
        "ok": "9710",
        "ko": "-"
    },
    "percentiles3": {
        "total": "9710",
        "ok": "9710",
        "ko": "-"
    },
    "percentiles4": {
        "total": "9710",
        "ok": "9710",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-1cf92": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105173",
path: "Animal-api sync for customer NL_105173",
pathFormatted: "req_animal-api-sync-1cf92",
stats: {
    "name": "Animal-api sync for customer NL_105173",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "42409",
        "ok": "42409",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "42409",
        "ok": "42409",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "42409",
        "ok": "42409",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "42409",
        "ok": "42409",
        "ko": "-"
    },
    "percentiles2": {
        "total": "42409",
        "ok": "42409",
        "ko": "-"
    },
    "percentiles3": {
        "total": "42409",
        "ok": "42409",
        "ko": "-"
    },
    "percentiles4": {
        "total": "42409",
        "ok": "42409",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b22b6": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_131590",
path: "Animal-api sync for customer NL_131590",
pathFormatted: "req_animal-api-sync-b22b6",
stats: {
    "name": "Animal-api sync for customer NL_131590",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "6293",
        "ok": "6293",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "6293",
        "ok": "6293",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "6293",
        "ok": "6293",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "6293",
        "ok": "6293",
        "ko": "-"
    },
    "percentiles2": {
        "total": "6293",
        "ok": "6293",
        "ko": "-"
    },
    "percentiles3": {
        "total": "6293",
        "ok": "6293",
        "ko": "-"
    },
    "percentiles4": {
        "total": "6293",
        "ok": "6293",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-1080b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110702",
path: "Animal-api sync for customer NL_110702",
pathFormatted: "req_animal-api-sync-1080b",
stats: {
    "name": "Animal-api sync for customer NL_110702",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "9334",
        "ok": "9334",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "9334",
        "ok": "9334",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "9334",
        "ok": "9334",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "9334",
        "ok": "9334",
        "ko": "-"
    },
    "percentiles2": {
        "total": "9334",
        "ok": "9334",
        "ko": "-"
    },
    "percentiles3": {
        "total": "9334",
        "ok": "9334",
        "ko": "-"
    },
    "percentiles4": {
        "total": "9334",
        "ok": "9334",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-8580c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_119732",
path: "Animal-api sync for customer NL_119732",
pathFormatted: "req_animal-api-sync-8580c",
stats: {
    "name": "Animal-api sync for customer NL_119732",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3351",
        "ok": "3351",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3351",
        "ok": "3351",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3351",
        "ok": "3351",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3351",
        "ok": "3351",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3351",
        "ok": "3351",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3351",
        "ok": "3351",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3351",
        "ok": "3351",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-8dfa9": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_116373",
path: "Animal-api sync for customer NL_116373",
pathFormatted: "req_animal-api-sync-8dfa9",
stats: {
    "name": "Animal-api sync for customer NL_116373",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "9690",
        "ok": "9690",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "9690",
        "ok": "9690",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "9690",
        "ok": "9690",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "9690",
        "ok": "9690",
        "ko": "-"
    },
    "percentiles2": {
        "total": "9690",
        "ok": "9690",
        "ko": "-"
    },
    "percentiles3": {
        "total": "9690",
        "ok": "9690",
        "ko": "-"
    },
    "percentiles4": {
        "total": "9690",
        "ok": "9690",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c298e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_124623",
path: "Animal-api sync for customer NL_124623",
pathFormatted: "req_animal-api-sync-c298e",
stats: {
    "name": "Animal-api sync for customer NL_124623",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2798",
        "ok": "2798",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2798",
        "ok": "2798",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2798",
        "ok": "2798",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2798",
        "ok": "2798",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2798",
        "ok": "2798",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2798",
        "ok": "2798",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2798",
        "ok": "2798",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-18c50": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_124684",
path: "Animal-api sync for customer NL_124684",
pathFormatted: "req_animal-api-sync-18c50",
stats: {
    "name": "Animal-api sync for customer NL_124684",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "7956",
        "ok": "7956",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "7956",
        "ok": "7956",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "7956",
        "ok": "7956",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "7956",
        "ok": "7956",
        "ko": "-"
    },
    "percentiles2": {
        "total": "7956",
        "ok": "7956",
        "ko": "-"
    },
    "percentiles3": {
        "total": "7956",
        "ok": "7956",
        "ko": "-"
    },
    "percentiles4": {
        "total": "7956",
        "ok": "7956",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-11d7b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111681",
path: "Animal-api sync for customer NL_111681",
pathFormatted: "req_animal-api-sync-11d7b",
stats: {
    "name": "Animal-api sync for customer NL_111681",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "20089",
        "ok": "20089",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "20089",
        "ok": "20089",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "20089",
        "ok": "20089",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "20089",
        "ok": "20089",
        "ko": "-"
    },
    "percentiles2": {
        "total": "20089",
        "ok": "20089",
        "ko": "-"
    },
    "percentiles3": {
        "total": "20089",
        "ok": "20089",
        "ko": "-"
    },
    "percentiles4": {
        "total": "20089",
        "ok": "20089",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-f0b42": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_107121",
path: "Animal-api sync for customer NL_107121",
pathFormatted: "req_animal-api-sync-f0b42",
stats: {
    "name": "Animal-api sync for customer NL_107121",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "24430",
        "ok": "24430",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "24430",
        "ok": "24430",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "24430",
        "ok": "24430",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "24430",
        "ok": "24430",
        "ko": "-"
    },
    "percentiles2": {
        "total": "24430",
        "ok": "24430",
        "ko": "-"
    },
    "percentiles3": {
        "total": "24430",
        "ok": "24430",
        "ko": "-"
    },
    "percentiles4": {
        "total": "24430",
        "ok": "24430",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-de92c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_109851",
path: "Animal-api sync for customer NL_109851",
pathFormatted: "req_animal-api-sync-de92c",
stats: {
    "name": "Animal-api sync for customer NL_109851",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "30365",
        "ok": "30365",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "30365",
        "ok": "30365",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "30365",
        "ok": "30365",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "30365",
        "ok": "30365",
        "ko": "-"
    },
    "percentiles2": {
        "total": "30365",
        "ok": "30365",
        "ko": "-"
    },
    "percentiles3": {
        "total": "30365",
        "ok": "30365",
        "ko": "-"
    },
    "percentiles4": {
        "total": "30365",
        "ok": "30365",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-31610": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_109147",
path: "Animal-api sync for customer NL_109147",
pathFormatted: "req_animal-api-sync-31610",
stats: {
    "name": "Animal-api sync for customer NL_109147",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2608",
        "ok": "2608",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2608",
        "ok": "2608",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2608",
        "ok": "2608",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2608",
        "ok": "2608",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2608",
        "ok": "2608",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2608",
        "ok": "2608",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2608",
        "ok": "2608",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a6474": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_217194",
path: "Animal-api sync for customer NL_217194",
pathFormatted: "req_animal-api-sync-a6474",
stats: {
    "name": "Animal-api sync for customer NL_217194",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "20866",
        "ok": "20866",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "20866",
        "ok": "20866",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "20866",
        "ok": "20866",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "20866",
        "ok": "20866",
        "ko": "-"
    },
    "percentiles2": {
        "total": "20866",
        "ok": "20866",
        "ko": "-"
    },
    "percentiles3": {
        "total": "20866",
        "ok": "20866",
        "ko": "-"
    },
    "percentiles4": {
        "total": "20866",
        "ok": "20866",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-735e5": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_162624",
path: "Animal-api sync for customer BE_162624",
pathFormatted: "req_animal-api-sync-735e5",
stats: {
    "name": "Animal-api sync for customer BE_162624",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "16255",
        "ok": "16255",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "16255",
        "ok": "16255",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "16255",
        "ok": "16255",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "16255",
        "ok": "16255",
        "ko": "-"
    },
    "percentiles2": {
        "total": "16255",
        "ok": "16255",
        "ko": "-"
    },
    "percentiles3": {
        "total": "16255",
        "ok": "16255",
        "ko": "-"
    },
    "percentiles4": {
        "total": "16255",
        "ok": "16255",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-da049": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111349",
path: "Animal-api sync for customer NL_111349",
pathFormatted: "req_animal-api-sync-da049",
stats: {
    "name": "Animal-api sync for customer NL_111349",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "14799",
        "ok": "14799",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "14799",
        "ok": "14799",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "14799",
        "ok": "14799",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "14799",
        "ok": "14799",
        "ko": "-"
    },
    "percentiles2": {
        "total": "14799",
        "ok": "14799",
        "ko": "-"
    },
    "percentiles3": {
        "total": "14799",
        "ok": "14799",
        "ko": "-"
    },
    "percentiles4": {
        "total": "14799",
        "ok": "14799",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-0945e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_120830",
path: "Animal-api sync for customer NL_120830",
pathFormatted: "req_animal-api-sync-0945e",
stats: {
    "name": "Animal-api sync for customer NL_120830",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "14757",
        "ok": "14757",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "14757",
        "ok": "14757",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "14757",
        "ok": "14757",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "14757",
        "ok": "14757",
        "ko": "-"
    },
    "percentiles2": {
        "total": "14757",
        "ok": "14757",
        "ko": "-"
    },
    "percentiles3": {
        "total": "14757",
        "ok": "14757",
        "ko": "-"
    },
    "percentiles4": {
        "total": "14757",
        "ok": "14757",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d1db6": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_154426",
path: "Animal-api sync for customer BE_154426",
pathFormatted: "req_animal-api-sync-d1db6",
stats: {
    "name": "Animal-api sync for customer BE_154426",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "maxResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "meanResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles4": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-3b46a": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_160734",
path: "Animal-api sync for customer NL_160734",
pathFormatted: "req_animal-api-sync-3b46a",
stats: {
    "name": "Animal-api sync for customer NL_160734",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "7785",
        "ok": "7785",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "7785",
        "ok": "7785",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "7785",
        "ok": "7785",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "7785",
        "ok": "7785",
        "ko": "-"
    },
    "percentiles2": {
        "total": "7785",
        "ok": "7785",
        "ko": "-"
    },
    "percentiles3": {
        "total": "7785",
        "ok": "7785",
        "ko": "-"
    },
    "percentiles4": {
        "total": "7785",
        "ok": "7785",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-4b701": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_122804",
path: "Animal-api sync for customer NL_122804",
pathFormatted: "req_animal-api-sync-4b701",
stats: {
    "name": "Animal-api sync for customer NL_122804",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "maxResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "meanResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles2": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles3": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles4": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-3f46c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110510",
path: "Animal-api sync for customer NL_110510",
pathFormatted: "req_animal-api-sync-3f46c",
stats: {
    "name": "Animal-api sync for customer NL_110510",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5049",
        "ok": "5049",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5049",
        "ok": "5049",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5049",
        "ok": "5049",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5049",
        "ok": "5049",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5049",
        "ok": "5049",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5049",
        "ok": "5049",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5049",
        "ok": "5049",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e1650": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_189253",
path: "Animal-api sync for customer BE_189253",
pathFormatted: "req_animal-api-sync-e1650",
stats: {
    "name": "Animal-api sync for customer BE_189253",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "19045",
        "ok": "19045",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "19045",
        "ok": "19045",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "19045",
        "ok": "19045",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "19045",
        "ok": "19045",
        "ko": "-"
    },
    "percentiles2": {
        "total": "19045",
        "ok": "19045",
        "ko": "-"
    },
    "percentiles3": {
        "total": "19045",
        "ok": "19045",
        "ko": "-"
    },
    "percentiles4": {
        "total": "19045",
        "ok": "19045",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-5a990": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_196831",
path: "Animal-api sync for customer BE_196831",
pathFormatted: "req_animal-api-sync-5a990",
stats: {
    "name": "Animal-api sync for customer BE_196831",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "9100",
        "ok": "9100",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "9100",
        "ok": "9100",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "9100",
        "ok": "9100",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "9100",
        "ok": "9100",
        "ko": "-"
    },
    "percentiles2": {
        "total": "9100",
        "ok": "9100",
        "ko": "-"
    },
    "percentiles3": {
        "total": "9100",
        "ok": "9100",
        "ko": "-"
    },
    "percentiles4": {
        "total": "9100",
        "ok": "9100",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-8b170": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_128376",
path: "Animal-api sync for customer NL_128376",
pathFormatted: "req_animal-api-sync-8b170",
stats: {
    "name": "Animal-api sync for customer NL_128376",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "33806",
        "ok": "33806",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "33806",
        "ok": "33806",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "33806",
        "ok": "33806",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "33806",
        "ok": "33806",
        "ko": "-"
    },
    "percentiles2": {
        "total": "33806",
        "ok": "33806",
        "ko": "-"
    },
    "percentiles3": {
        "total": "33806",
        "ok": "33806",
        "ko": "-"
    },
    "percentiles4": {
        "total": "33806",
        "ok": "33806",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-9e2d7": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_127839",
path: "Animal-api sync for customer NL_127839",
pathFormatted: "req_animal-api-sync-9e2d7",
stats: {
    "name": "Animal-api sync for customer NL_127839",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "9434",
        "ok": "9434",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "9434",
        "ok": "9434",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "9434",
        "ok": "9434",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "9434",
        "ok": "9434",
        "ko": "-"
    },
    "percentiles2": {
        "total": "9434",
        "ok": "9434",
        "ko": "-"
    },
    "percentiles3": {
        "total": "9434",
        "ok": "9434",
        "ko": "-"
    },
    "percentiles4": {
        "total": "9434",
        "ok": "9434",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-cb3f0": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_109354",
path: "Animal-api sync for customer NL_109354",
pathFormatted: "req_animal-api-sync-cb3f0",
stats: {
    "name": "Animal-api sync for customer NL_109354",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "6929",
        "ok": "6929",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "6929",
        "ok": "6929",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "6929",
        "ok": "6929",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "6929",
        "ok": "6929",
        "ko": "-"
    },
    "percentiles2": {
        "total": "6929",
        "ok": "6929",
        "ko": "-"
    },
    "percentiles3": {
        "total": "6929",
        "ok": "6929",
        "ko": "-"
    },
    "percentiles4": {
        "total": "6929",
        "ok": "6929",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-77e7f": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_116276",
path: "Animal-api sync for customer NL_116276",
pathFormatted: "req_animal-api-sync-77e7f",
stats: {
    "name": "Animal-api sync for customer NL_116276",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "13622",
        "ok": "13622",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "13622",
        "ok": "13622",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "13622",
        "ok": "13622",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "13622",
        "ok": "13622",
        "ko": "-"
    },
    "percentiles2": {
        "total": "13622",
        "ok": "13622",
        "ko": "-"
    },
    "percentiles3": {
        "total": "13622",
        "ok": "13622",
        "ko": "-"
    },
    "percentiles4": {
        "total": "13622",
        "ok": "13622",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e91bf": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111232",
path: "Animal-api sync for customer NL_111232",
pathFormatted: "req_animal-api-sync-e91bf",
stats: {
    "name": "Animal-api sync for customer NL_111232",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "7605",
        "ok": "7605",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "7605",
        "ok": "7605",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "7605",
        "ok": "7605",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "7605",
        "ok": "7605",
        "ko": "-"
    },
    "percentiles2": {
        "total": "7605",
        "ok": "7605",
        "ko": "-"
    },
    "percentiles3": {
        "total": "7605",
        "ok": "7605",
        "ko": "-"
    },
    "percentiles4": {
        "total": "7605",
        "ok": "7605",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-df608": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_106231",
path: "Animal-api sync for customer NL_106231",
pathFormatted: "req_animal-api-sync-df608",
stats: {
    "name": "Animal-api sync for customer NL_106231",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "maxResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "meanResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles4": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-96235": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_119086",
path: "Animal-api sync for customer NL_119086",
pathFormatted: "req_animal-api-sync-96235",
stats: {
    "name": "Animal-api sync for customer NL_119086",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21033",
        "ok": "21033",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "21033",
        "ok": "21033",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "21033",
        "ok": "21033",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "21033",
        "ok": "21033",
        "ko": "-"
    },
    "percentiles2": {
        "total": "21033",
        "ok": "21033",
        "ko": "-"
    },
    "percentiles3": {
        "total": "21033",
        "ok": "21033",
        "ko": "-"
    },
    "percentiles4": {
        "total": "21033",
        "ok": "21033",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b66f8": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110851",
path: "Animal-api sync for customer NL_110851",
pathFormatted: "req_animal-api-sync-b66f8",
stats: {
    "name": "Animal-api sync for customer NL_110851",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "17234",
        "ok": "17234",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "17234",
        "ok": "17234",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "17234",
        "ok": "17234",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "17234",
        "ok": "17234",
        "ko": "-"
    },
    "percentiles2": {
        "total": "17234",
        "ok": "17234",
        "ko": "-"
    },
    "percentiles3": {
        "total": "17234",
        "ok": "17234",
        "ko": "-"
    },
    "percentiles4": {
        "total": "17234",
        "ok": "17234",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-fc706": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_108712",
path: "Animal-api sync for customer NL_108712",
pathFormatted: "req_animal-api-sync-fc706",
stats: {
    "name": "Animal-api sync for customer NL_108712",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "11577",
        "ok": "11577",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "11577",
        "ok": "11577",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "11577",
        "ok": "11577",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "11577",
        "ok": "11577",
        "ko": "-"
    },
    "percentiles2": {
        "total": "11577",
        "ok": "11577",
        "ko": "-"
    },
    "percentiles3": {
        "total": "11577",
        "ok": "11577",
        "ko": "-"
    },
    "percentiles4": {
        "total": "11577",
        "ok": "11577",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-77463": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_142162",
path: "Animal-api sync for customer NL_142162",
pathFormatted: "req_animal-api-sync-77463",
stats: {
    "name": "Animal-api sync for customer NL_142162",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "31204",
        "ok": "31204",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "31204",
        "ok": "31204",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "31204",
        "ok": "31204",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "31204",
        "ok": "31204",
        "ko": "-"
    },
    "percentiles2": {
        "total": "31204",
        "ok": "31204",
        "ko": "-"
    },
    "percentiles3": {
        "total": "31204",
        "ok": "31204",
        "ko": "-"
    },
    "percentiles4": {
        "total": "31204",
        "ok": "31204",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-3e0f1": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_119459",
path: "Animal-api sync for customer NL_119459",
pathFormatted: "req_animal-api-sync-3e0f1",
stats: {
    "name": "Animal-api sync for customer NL_119459",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "8019",
        "ok": "8019",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "8019",
        "ok": "8019",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "8019",
        "ok": "8019",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "8019",
        "ok": "8019",
        "ko": "-"
    },
    "percentiles2": {
        "total": "8019",
        "ok": "8019",
        "ko": "-"
    },
    "percentiles3": {
        "total": "8019",
        "ok": "8019",
        "ko": "-"
    },
    "percentiles4": {
        "total": "8019",
        "ok": "8019",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-f9107": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_121807",
path: "Animal-api sync for customer NL_121807",
pathFormatted: "req_animal-api-sync-f9107",
stats: {
    "name": "Animal-api sync for customer NL_121807",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "maxResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "meanResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles2": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles3": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles4": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-82f9e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110685",
path: "Animal-api sync for customer NL_110685",
pathFormatted: "req_animal-api-sync-82f9e",
stats: {
    "name": "Animal-api sync for customer NL_110685",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "46613",
        "ok": "46613",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "46613",
        "ok": "46613",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "46613",
        "ok": "46613",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "46613",
        "ok": "46613",
        "ko": "-"
    },
    "percentiles2": {
        "total": "46613",
        "ok": "46613",
        "ko": "-"
    },
    "percentiles3": {
        "total": "46613",
        "ok": "46613",
        "ko": "-"
    },
    "percentiles4": {
        "total": "46613",
        "ok": "46613",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-39816": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_145536",
path: "Animal-api sync for customer BE_145536",
pathFormatted: "req_animal-api-sync-39816",
stats: {
    "name": "Animal-api sync for customer BE_145536",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "8500",
        "ok": "8500",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "8500",
        "ok": "8500",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "8500",
        "ok": "8500",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "8500",
        "ok": "8500",
        "ko": "-"
    },
    "percentiles2": {
        "total": "8500",
        "ok": "8500",
        "ko": "-"
    },
    "percentiles3": {
        "total": "8500",
        "ok": "8500",
        "ko": "-"
    },
    "percentiles4": {
        "total": "8500",
        "ok": "8500",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-66198": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_107001",
path: "Animal-api sync for customer NL_107001",
pathFormatted: "req_animal-api-sync-66198",
stats: {
    "name": "Animal-api sync for customer NL_107001",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "13206",
        "ok": "13206",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "13206",
        "ok": "13206",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "13206",
        "ok": "13206",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "13206",
        "ok": "13206",
        "ko": "-"
    },
    "percentiles2": {
        "total": "13206",
        "ok": "13206",
        "ko": "-"
    },
    "percentiles3": {
        "total": "13206",
        "ok": "13206",
        "ko": "-"
    },
    "percentiles4": {
        "total": "13206",
        "ok": "13206",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-299ce": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_119621",
path: "Animal-api sync for customer NL_119621",
pathFormatted: "req_animal-api-sync-299ce",
stats: {
    "name": "Animal-api sync for customer NL_119621",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "7931",
        "ok": "7931",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "7931",
        "ok": "7931",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "7931",
        "ok": "7931",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "7931",
        "ok": "7931",
        "ko": "-"
    },
    "percentiles2": {
        "total": "7931",
        "ok": "7931",
        "ko": "-"
    },
    "percentiles3": {
        "total": "7931",
        "ok": "7931",
        "ko": "-"
    },
    "percentiles4": {
        "total": "7931",
        "ok": "7931",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-cb563": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_195750",
path: "Animal-api sync for customer BE_195750",
pathFormatted: "req_animal-api-sync-cb563",
stats: {
    "name": "Animal-api sync for customer BE_195750",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4919",
        "ok": "4919",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4919",
        "ok": "4919",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4919",
        "ok": "4919",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4919",
        "ok": "4919",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4919",
        "ok": "4919",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4919",
        "ok": "4919",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4919",
        "ok": "4919",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-86128": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110109",
path: "Animal-api sync for customer NL_110109",
pathFormatted: "req_animal-api-sync-86128",
stats: {
    "name": "Animal-api sync for customer NL_110109",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "maxResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "meanResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles2": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles3": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles4": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-2a7a0": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_119620",
path: "Animal-api sync for customer NL_119620",
pathFormatted: "req_animal-api-sync-2a7a0",
stats: {
    "name": "Animal-api sync for customer NL_119620",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "10735",
        "ok": "10735",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "10735",
        "ok": "10735",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "10735",
        "ok": "10735",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "10735",
        "ok": "10735",
        "ko": "-"
    },
    "percentiles2": {
        "total": "10735",
        "ok": "10735",
        "ko": "-"
    },
    "percentiles3": {
        "total": "10735",
        "ok": "10735",
        "ko": "-"
    },
    "percentiles4": {
        "total": "10735",
        "ok": "10735",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-f1bc5": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_106522",
path: "Animal-api sync for customer NL_106522",
pathFormatted: "req_animal-api-sync-f1bc5",
stats: {
    "name": "Animal-api sync for customer NL_106522",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4550",
        "ok": "4550",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4550",
        "ok": "4550",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4550",
        "ok": "4550",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4550",
        "ok": "4550",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4550",
        "ok": "4550",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4550",
        "ok": "4550",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4550",
        "ok": "4550",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-fa8fb": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110675",
path: "Animal-api sync for customer NL_110675",
pathFormatted: "req_animal-api-sync-fa8fb",
stats: {
    "name": "Animal-api sync for customer NL_110675",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "42477",
        "ok": "42477",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "42477",
        "ok": "42477",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "42477",
        "ok": "42477",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "42477",
        "ok": "42477",
        "ko": "-"
    },
    "percentiles2": {
        "total": "42477",
        "ok": "42477",
        "ko": "-"
    },
    "percentiles3": {
        "total": "42477",
        "ok": "42477",
        "ko": "-"
    },
    "percentiles4": {
        "total": "42477",
        "ok": "42477",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-9a989": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105564",
path: "Animal-api sync for customer NL_105564",
pathFormatted: "req_animal-api-sync-9a989",
stats: {
    "name": "Animal-api sync for customer NL_105564",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "12291",
        "ok": "12291",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "12291",
        "ok": "12291",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "12291",
        "ok": "12291",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "12291",
        "ok": "12291",
        "ko": "-"
    },
    "percentiles2": {
        "total": "12291",
        "ok": "12291",
        "ko": "-"
    },
    "percentiles3": {
        "total": "12291",
        "ok": "12291",
        "ko": "-"
    },
    "percentiles4": {
        "total": "12291",
        "ok": "12291",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-1d50e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_107593",
path: "Animal-api sync for customer NL_107593",
pathFormatted: "req_animal-api-sync-1d50e",
stats: {
    "name": "Animal-api sync for customer NL_107593",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "11806",
        "ok": "11806",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "11806",
        "ok": "11806",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "11806",
        "ok": "11806",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "11806",
        "ok": "11806",
        "ko": "-"
    },
    "percentiles2": {
        "total": "11806",
        "ok": "11806",
        "ko": "-"
    },
    "percentiles3": {
        "total": "11806",
        "ok": "11806",
        "ko": "-"
    },
    "percentiles4": {
        "total": "11806",
        "ok": "11806",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-3fad8": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_119768",
path: "Animal-api sync for customer NL_119768",
pathFormatted: "req_animal-api-sync-3fad8",
stats: {
    "name": "Animal-api sync for customer NL_119768",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "6154",
        "ok": "6154",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "6154",
        "ok": "6154",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "6154",
        "ok": "6154",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "6154",
        "ok": "6154",
        "ko": "-"
    },
    "percentiles2": {
        "total": "6154",
        "ok": "6154",
        "ko": "-"
    },
    "percentiles3": {
        "total": "6154",
        "ok": "6154",
        "ko": "-"
    },
    "percentiles4": {
        "total": "6154",
        "ok": "6154",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-1cdda": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_106961",
path: "Animal-api sync for customer NL_106961",
pathFormatted: "req_animal-api-sync-1cdda",
stats: {
    "name": "Animal-api sync for customer NL_106961",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4075",
        "ok": "4075",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4075",
        "ok": "4075",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4075",
        "ok": "4075",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4075",
        "ok": "4075",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4075",
        "ok": "4075",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4075",
        "ok": "4075",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4075",
        "ok": "4075",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-028d0": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_139052",
path: "Animal-api sync for customer NL_139052",
pathFormatted: "req_animal-api-sync-028d0",
stats: {
    "name": "Animal-api sync for customer NL_139052",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1367",
        "ok": "1367",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1367",
        "ok": "1367",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1367",
        "ok": "1367",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1367",
        "ok": "1367",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1367",
        "ok": "1367",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1367",
        "ok": "1367",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1367",
        "ok": "1367",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-3df0e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_136411",
path: "Animal-api sync for customer NL_136411",
pathFormatted: "req_animal-api-sync-3df0e",
stats: {
    "name": "Animal-api sync for customer NL_136411",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "maxResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "meanResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles4": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-34074": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104611",
path: "Animal-api sync for customer NL_104611",
pathFormatted: "req_animal-api-sync-34074",
stats: {
    "name": "Animal-api sync for customer NL_104611",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "38588",
        "ok": "38588",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "38588",
        "ok": "38588",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "38588",
        "ok": "38588",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "38588",
        "ok": "38588",
        "ko": "-"
    },
    "percentiles2": {
        "total": "38588",
        "ok": "38588",
        "ko": "-"
    },
    "percentiles3": {
        "total": "38588",
        "ok": "38588",
        "ko": "-"
    },
    "percentiles4": {
        "total": "38588",
        "ok": "38588",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-4e4dd": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_132571",
path: "Animal-api sync for customer NL_132571",
pathFormatted: "req_animal-api-sync-4e4dd",
stats: {
    "name": "Animal-api sync for customer NL_132571",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2518",
        "ok": "2518",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2518",
        "ok": "2518",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2518",
        "ok": "2518",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2518",
        "ok": "2518",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2518",
        "ok": "2518",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2518",
        "ok": "2518",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2518",
        "ok": "2518",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b76bc": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_121619",
path: "Animal-api sync for customer NL_121619",
pathFormatted: "req_animal-api-sync-b76bc",
stats: {
    "name": "Animal-api sync for customer NL_121619",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "maxResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "meanResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles4": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-67b51": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_208384",
path: "Animal-api sync for customer NL_208384",
pathFormatted: "req_animal-api-sync-67b51",
stats: {
    "name": "Animal-api sync for customer NL_208384",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "23023",
        "ok": "23023",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "23023",
        "ok": "23023",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "23023",
        "ok": "23023",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "23023",
        "ok": "23023",
        "ko": "-"
    },
    "percentiles2": {
        "total": "23023",
        "ok": "23023",
        "ko": "-"
    },
    "percentiles3": {
        "total": "23023",
        "ok": "23023",
        "ko": "-"
    },
    "percentiles4": {
        "total": "23023",
        "ok": "23023",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-f5467": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_164463",
path: "Animal-api sync for customer NL_164463",
pathFormatted: "req_animal-api-sync-f5467",
stats: {
    "name": "Animal-api sync for customer NL_164463",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "14435",
        "ok": "14435",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "14435",
        "ok": "14435",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "14435",
        "ok": "14435",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "14435",
        "ok": "14435",
        "ko": "-"
    },
    "percentiles2": {
        "total": "14435",
        "ok": "14435",
        "ko": "-"
    },
    "percentiles3": {
        "total": "14435",
        "ok": "14435",
        "ko": "-"
    },
    "percentiles4": {
        "total": "14435",
        "ok": "14435",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a921a": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_158726",
path: "Animal-api sync for customer BE_158726",
pathFormatted: "req_animal-api-sync-a921a",
stats: {
    "name": "Animal-api sync for customer BE_158726",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "8171",
        "ok": "8171",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "8171",
        "ok": "8171",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "8171",
        "ok": "8171",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "8171",
        "ok": "8171",
        "ko": "-"
    },
    "percentiles2": {
        "total": "8171",
        "ok": "8171",
        "ko": "-"
    },
    "percentiles3": {
        "total": "8171",
        "ok": "8171",
        "ko": "-"
    },
    "percentiles4": {
        "total": "8171",
        "ok": "8171",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-43d17": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_159367",
path: "Animal-api sync for customer NL_159367",
pathFormatted: "req_animal-api-sync-43d17",
stats: {
    "name": "Animal-api sync for customer NL_159367",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2770",
        "ok": "2770",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2770",
        "ok": "2770",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2770",
        "ok": "2770",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2770",
        "ok": "2770",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2770",
        "ok": "2770",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2770",
        "ok": "2770",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2770",
        "ok": "2770",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-7b4fd": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_207082",
path: "Animal-api sync for customer BE_207082",
pathFormatted: "req_animal-api-sync-7b4fd",
stats: {
    "name": "Animal-api sync for customer BE_207082",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "maxResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "meanResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles2": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles3": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles4": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-774bc": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105254",
path: "Animal-api sync for customer NL_105254",
pathFormatted: "req_animal-api-sync-774bc",
stats: {
    "name": "Animal-api sync for customer NL_105254",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "9110",
        "ok": "9110",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "9110",
        "ok": "9110",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "9110",
        "ok": "9110",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "9110",
        "ok": "9110",
        "ko": "-"
    },
    "percentiles2": {
        "total": "9110",
        "ok": "9110",
        "ko": "-"
    },
    "percentiles3": {
        "total": "9110",
        "ok": "9110",
        "ko": "-"
    },
    "percentiles4": {
        "total": "9110",
        "ok": "9110",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-72b5a": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_129227",
path: "Animal-api sync for customer NL_129227",
pathFormatted: "req_animal-api-sync-72b5a",
stats: {
    "name": "Animal-api sync for customer NL_129227",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "12448",
        "ok": "12448",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "12448",
        "ok": "12448",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "12448",
        "ok": "12448",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "12448",
        "ok": "12448",
        "ko": "-"
    },
    "percentiles2": {
        "total": "12448",
        "ok": "12448",
        "ko": "-"
    },
    "percentiles3": {
        "total": "12448",
        "ok": "12448",
        "ko": "-"
    },
    "percentiles4": {
        "total": "12448",
        "ok": "12448",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e7477": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_120288",
path: "Animal-api sync for customer NL_120288",
pathFormatted: "req_animal-api-sync-e7477",
stats: {
    "name": "Animal-api sync for customer NL_120288",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "13259",
        "ok": "13259",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "13259",
        "ok": "13259",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "13259",
        "ok": "13259",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "13259",
        "ok": "13259",
        "ko": "-"
    },
    "percentiles2": {
        "total": "13259",
        "ok": "13259",
        "ko": "-"
    },
    "percentiles3": {
        "total": "13259",
        "ok": "13259",
        "ko": "-"
    },
    "percentiles4": {
        "total": "13259",
        "ok": "13259",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b6fe1": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_112227",
path: "Animal-api sync for customer NL_112227",
pathFormatted: "req_animal-api-sync-b6fe1",
stats: {
    "name": "Animal-api sync for customer NL_112227",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "14973",
        "ok": "14973",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "14973",
        "ok": "14973",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "14973",
        "ok": "14973",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "14973",
        "ok": "14973",
        "ko": "-"
    },
    "percentiles2": {
        "total": "14973",
        "ok": "14973",
        "ko": "-"
    },
    "percentiles3": {
        "total": "14973",
        "ok": "14973",
        "ko": "-"
    },
    "percentiles4": {
        "total": "14973",
        "ok": "14973",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-6891c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111812",
path: "Animal-api sync for customer NL_111812",
pathFormatted: "req_animal-api-sync-6891c",
stats: {
    "name": "Animal-api sync for customer NL_111812",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "22996",
        "ok": "22996",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "22996",
        "ok": "22996",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "22996",
        "ok": "22996",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "22996",
        "ok": "22996",
        "ko": "-"
    },
    "percentiles2": {
        "total": "22996",
        "ok": "22996",
        "ko": "-"
    },
    "percentiles3": {
        "total": "22996",
        "ok": "22996",
        "ko": "-"
    },
    "percentiles4": {
        "total": "22996",
        "ok": "22996",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-5324e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_112685",
path: "Animal-api sync for customer NL_112685",
pathFormatted: "req_animal-api-sync-5324e",
stats: {
    "name": "Animal-api sync for customer NL_112685",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "6486",
        "ok": "6486",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "6486",
        "ok": "6486",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "6486",
        "ok": "6486",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "6486",
        "ok": "6486",
        "ko": "-"
    },
    "percentiles2": {
        "total": "6486",
        "ok": "6486",
        "ko": "-"
    },
    "percentiles3": {
        "total": "6486",
        "ok": "6486",
        "ko": "-"
    },
    "percentiles4": {
        "total": "6486",
        "ok": "6486",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-39487": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_153606",
path: "Animal-api sync for customer BE_153606",
pathFormatted: "req_animal-api-sync-39487",
stats: {
    "name": "Animal-api sync for customer BE_153606",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "18924",
        "ok": "18924",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "18924",
        "ok": "18924",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "18924",
        "ok": "18924",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "18924",
        "ok": "18924",
        "ko": "-"
    },
    "percentiles2": {
        "total": "18924",
        "ok": "18924",
        "ko": "-"
    },
    "percentiles3": {
        "total": "18924",
        "ok": "18924",
        "ko": "-"
    },
    "percentiles4": {
        "total": "18924",
        "ok": "18924",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-115a1": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_106548",
path: "Animal-api sync for customer NL_106548",
pathFormatted: "req_animal-api-sync-115a1",
stats: {
    "name": "Animal-api sync for customer NL_106548",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21957",
        "ok": "21957",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "21957",
        "ok": "21957",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "21957",
        "ok": "21957",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "21957",
        "ok": "21957",
        "ko": "-"
    },
    "percentiles2": {
        "total": "21957",
        "ok": "21957",
        "ko": "-"
    },
    "percentiles3": {
        "total": "21957",
        "ok": "21957",
        "ko": "-"
    },
    "percentiles4": {
        "total": "21957",
        "ok": "21957",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-dfd7c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_139331",
path: "Animal-api sync for customer NL_139331",
pathFormatted: "req_animal-api-sync-dfd7c",
stats: {
    "name": "Animal-api sync for customer NL_139331",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "24367",
        "ok": "24367",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "24367",
        "ok": "24367",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "24367",
        "ok": "24367",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "24367",
        "ok": "24367",
        "ko": "-"
    },
    "percentiles2": {
        "total": "24367",
        "ok": "24367",
        "ko": "-"
    },
    "percentiles3": {
        "total": "24367",
        "ok": "24367",
        "ko": "-"
    },
    "percentiles4": {
        "total": "24367",
        "ok": "24367",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c6932": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_133968",
path: "Animal-api sync for customer NL_133968",
pathFormatted: "req_animal-api-sync-c6932",
stats: {
    "name": "Animal-api sync for customer NL_133968",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "16679",
        "ok": "16679",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "16679",
        "ok": "16679",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "16679",
        "ok": "16679",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "16679",
        "ok": "16679",
        "ko": "-"
    },
    "percentiles2": {
        "total": "16679",
        "ok": "16679",
        "ko": "-"
    },
    "percentiles3": {
        "total": "16679",
        "ok": "16679",
        "ko": "-"
    },
    "percentiles4": {
        "total": "16679",
        "ok": "16679",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-4cd78": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111414",
path: "Animal-api sync for customer NL_111414",
pathFormatted: "req_animal-api-sync-4cd78",
stats: {
    "name": "Animal-api sync for customer NL_111414",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "maxResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "meanResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles4": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-0fed7": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_109760",
path: "Animal-api sync for customer NL_109760",
pathFormatted: "req_animal-api-sync-0fed7",
stats: {
    "name": "Animal-api sync for customer NL_109760",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "16142",
        "ok": "16142",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "16142",
        "ok": "16142",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "16142",
        "ok": "16142",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "16142",
        "ok": "16142",
        "ko": "-"
    },
    "percentiles2": {
        "total": "16142",
        "ok": "16142",
        "ko": "-"
    },
    "percentiles3": {
        "total": "16142",
        "ok": "16142",
        "ko": "-"
    },
    "percentiles4": {
        "total": "16142",
        "ok": "16142",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-f47e6": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_120627",
path: "Animal-api sync for customer NL_120627",
pathFormatted: "req_animal-api-sync-f47e6",
stats: {
    "name": "Animal-api sync for customer NL_120627",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "maxResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "meanResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles2": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles3": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles4": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-66c16": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_112114",
path: "Animal-api sync for customer NL_112114",
pathFormatted: "req_animal-api-sync-66c16",
stats: {
    "name": "Animal-api sync for customer NL_112114",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "16830",
        "ok": "16830",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "16830",
        "ok": "16830",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "16830",
        "ok": "16830",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "16830",
        "ok": "16830",
        "ko": "-"
    },
    "percentiles2": {
        "total": "16830",
        "ok": "16830",
        "ko": "-"
    },
    "percentiles3": {
        "total": "16830",
        "ok": "16830",
        "ko": "-"
    },
    "percentiles4": {
        "total": "16830",
        "ok": "16830",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-79b8e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_116848",
path: "Animal-api sync for customer NL_116848",
pathFormatted: "req_animal-api-sync-79b8e",
stats: {
    "name": "Animal-api sync for customer NL_116848",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "maxResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "meanResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles2": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles3": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles4": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-5420f": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110280",
path: "Animal-api sync for customer NL_110280",
pathFormatted: "req_animal-api-sync-5420f",
stats: {
    "name": "Animal-api sync for customer NL_110280",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "maxResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "meanResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles2": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles3": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles4": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-63b47": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111966",
path: "Animal-api sync for customer NL_111966",
pathFormatted: "req_animal-api-sync-63b47",
stats: {
    "name": "Animal-api sync for customer NL_111966",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "maxResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "meanResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles2": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles3": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles4": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-fbcf4": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_128711",
path: "Animal-api sync for customer NL_128711",
pathFormatted: "req_animal-api-sync-fbcf4",
stats: {
    "name": "Animal-api sync for customer NL_128711",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "35347",
        "ok": "35347",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "35347",
        "ok": "35347",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "35347",
        "ok": "35347",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "35347",
        "ok": "35347",
        "ko": "-"
    },
    "percentiles2": {
        "total": "35347",
        "ok": "35347",
        "ko": "-"
    },
    "percentiles3": {
        "total": "35347",
        "ok": "35347",
        "ko": "-"
    },
    "percentiles4": {
        "total": "35347",
        "ok": "35347",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d05fc": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_131083",
path: "Animal-api sync for customer NL_131083",
pathFormatted: "req_animal-api-sync-d05fc",
stats: {
    "name": "Animal-api sync for customer NL_131083",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "maxResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "meanResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles2": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles3": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles4": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-ed470": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_115514",
path: "Animal-api sync for customer NL_115514",
pathFormatted: "req_animal-api-sync-ed470",
stats: {
    "name": "Animal-api sync for customer NL_115514",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "36077",
        "ok": "36077",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "36077",
        "ok": "36077",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "36077",
        "ok": "36077",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "36077",
        "ok": "36077",
        "ko": "-"
    },
    "percentiles2": {
        "total": "36077",
        "ok": "36077",
        "ko": "-"
    },
    "percentiles3": {
        "total": "36077",
        "ok": "36077",
        "ko": "-"
    },
    "percentiles4": {
        "total": "36077",
        "ok": "36077",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-29375": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_121682",
path: "Animal-api sync for customer NL_121682",
pathFormatted: "req_animal-api-sync-29375",
stats: {
    "name": "Animal-api sync for customer NL_121682",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "29811",
        "ok": "29811",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "29811",
        "ok": "29811",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "29811",
        "ok": "29811",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "29811",
        "ok": "29811",
        "ko": "-"
    },
    "percentiles2": {
        "total": "29811",
        "ok": "29811",
        "ko": "-"
    },
    "percentiles3": {
        "total": "29811",
        "ok": "29811",
        "ko": "-"
    },
    "percentiles4": {
        "total": "29811",
        "ok": "29811",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-4e713": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111266",
path: "Animal-api sync for customer NL_111266",
pathFormatted: "req_animal-api-sync-4e713",
stats: {
    "name": "Animal-api sync for customer NL_111266",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "49969",
        "ok": "49969",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "49969",
        "ok": "49969",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "49969",
        "ok": "49969",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "49969",
        "ok": "49969",
        "ko": "-"
    },
    "percentiles2": {
        "total": "49969",
        "ok": "49969",
        "ko": "-"
    },
    "percentiles3": {
        "total": "49969",
        "ok": "49969",
        "ko": "-"
    },
    "percentiles4": {
        "total": "49969",
        "ok": "49969",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-5df96": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_114543",
path: "Animal-api sync for customer NL_114543",
pathFormatted: "req_animal-api-sync-5df96",
stats: {
    "name": "Animal-api sync for customer NL_114543",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "24749",
        "ok": "24749",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "24749",
        "ok": "24749",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "24749",
        "ok": "24749",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "24749",
        "ok": "24749",
        "ko": "-"
    },
    "percentiles2": {
        "total": "24749",
        "ok": "24749",
        "ko": "-"
    },
    "percentiles3": {
        "total": "24749",
        "ok": "24749",
        "ko": "-"
    },
    "percentiles4": {
        "total": "24749",
        "ok": "24749",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-38246": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_118506",
path: "Animal-api sync for customer NL_118506",
pathFormatted: "req_animal-api-sync-38246",
stats: {
    "name": "Animal-api sync for customer NL_118506",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "39002",
        "ok": "39002",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "39002",
        "ok": "39002",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "39002",
        "ok": "39002",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "39002",
        "ok": "39002",
        "ko": "-"
    },
    "percentiles2": {
        "total": "39002",
        "ok": "39002",
        "ko": "-"
    },
    "percentiles3": {
        "total": "39002",
        "ok": "39002",
        "ko": "-"
    },
    "percentiles4": {
        "total": "39002",
        "ok": "39002",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-3d430": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_134950",
path: "Animal-api sync for customer NL_134950",
pathFormatted: "req_animal-api-sync-3d430",
stats: {
    "name": "Animal-api sync for customer NL_134950",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "19961",
        "ok": "19961",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "19961",
        "ok": "19961",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "19961",
        "ok": "19961",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "19961",
        "ok": "19961",
        "ko": "-"
    },
    "percentiles2": {
        "total": "19961",
        "ok": "19961",
        "ko": "-"
    },
    "percentiles3": {
        "total": "19961",
        "ok": "19961",
        "ko": "-"
    },
    "percentiles4": {
        "total": "19961",
        "ok": "19961",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a667b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_103980",
path: "Animal-api sync for customer NL_103980",
pathFormatted: "req_animal-api-sync-a667b",
stats: {
    "name": "Animal-api sync for customer NL_103980",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "maxResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "meanResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles4": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-92bed": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105164",
path: "Animal-api sync for customer NL_105164",
pathFormatted: "req_animal-api-sync-92bed",
stats: {
    "name": "Animal-api sync for customer NL_105164",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "56273",
        "ok": "56273",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "56273",
        "ok": "56273",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "56273",
        "ok": "56273",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "56273",
        "ok": "56273",
        "ko": "-"
    },
    "percentiles2": {
        "total": "56273",
        "ok": "56273",
        "ko": "-"
    },
    "percentiles3": {
        "total": "56273",
        "ok": "56273",
        "ko": "-"
    },
    "percentiles4": {
        "total": "56273",
        "ok": "56273",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-74e33": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_189313",
path: "Animal-api sync for customer BE_189313",
pathFormatted: "req_animal-api-sync-74e33",
stats: {
    "name": "Animal-api sync for customer BE_189313",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "maxResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "meanResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles4": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-aaf86": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_130803",
path: "Animal-api sync for customer NL_130803",
pathFormatted: "req_animal-api-sync-aaf86",
stats: {
    "name": "Animal-api sync for customer NL_130803",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "maxResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "meanResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles4": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-390c0": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_113006",
path: "Animal-api sync for customer NL_113006",
pathFormatted: "req_animal-api-sync-390c0",
stats: {
    "name": "Animal-api sync for customer NL_113006",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "35211",
        "ok": "35211",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "35211",
        "ok": "35211",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "35211",
        "ok": "35211",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "35211",
        "ok": "35211",
        "ko": "-"
    },
    "percentiles2": {
        "total": "35211",
        "ok": "35211",
        "ko": "-"
    },
    "percentiles3": {
        "total": "35211",
        "ok": "35211",
        "ko": "-"
    },
    "percentiles4": {
        "total": "35211",
        "ok": "35211",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-363df": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_120238",
path: "Animal-api sync for customer NL_120238",
pathFormatted: "req_animal-api-sync-363df",
stats: {
    "name": "Animal-api sync for customer NL_120238",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "30573",
        "ok": "30573",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "30573",
        "ok": "30573",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "30573",
        "ok": "30573",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "30573",
        "ok": "30573",
        "ko": "-"
    },
    "percentiles2": {
        "total": "30573",
        "ok": "30573",
        "ko": "-"
    },
    "percentiles3": {
        "total": "30573",
        "ok": "30573",
        "ko": "-"
    },
    "percentiles4": {
        "total": "30573",
        "ok": "30573",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e70df": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_129794",
path: "Animal-api sync for customer NL_129794",
pathFormatted: "req_animal-api-sync-e70df",
stats: {
    "name": "Animal-api sync for customer NL_129794",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "24408",
        "ok": "24408",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "24408",
        "ok": "24408",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "24408",
        "ok": "24408",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "24408",
        "ok": "24408",
        "ko": "-"
    },
    "percentiles2": {
        "total": "24408",
        "ok": "24408",
        "ko": "-"
    },
    "percentiles3": {
        "total": "24408",
        "ok": "24408",
        "ko": "-"
    },
    "percentiles4": {
        "total": "24408",
        "ok": "24408",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a9219": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_103449",
path: "Animal-api sync for customer NL_103449",
pathFormatted: "req_animal-api-sync-a9219",
stats: {
    "name": "Animal-api sync for customer NL_103449",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "56558",
        "ok": "56558",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "56558",
        "ok": "56558",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "56558",
        "ok": "56558",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "56558",
        "ok": "56558",
        "ko": "-"
    },
    "percentiles2": {
        "total": "56558",
        "ok": "56558",
        "ko": "-"
    },
    "percentiles3": {
        "total": "56558",
        "ok": "56558",
        "ko": "-"
    },
    "percentiles4": {
        "total": "56558",
        "ok": "56558",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-30320": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_128704",
path: "Animal-api sync for customer NL_128704",
pathFormatted: "req_animal-api-sync-30320",
stats: {
    "name": "Animal-api sync for customer NL_128704",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "32596",
        "ok": "32596",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "32596",
        "ok": "32596",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "32596",
        "ok": "32596",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "32596",
        "ok": "32596",
        "ko": "-"
    },
    "percentiles2": {
        "total": "32596",
        "ok": "32596",
        "ko": "-"
    },
    "percentiles3": {
        "total": "32596",
        "ok": "32596",
        "ko": "-"
    },
    "percentiles4": {
        "total": "32596",
        "ok": "32596",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c50f5": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111928",
path: "Animal-api sync for customer NL_111928",
pathFormatted: "req_animal-api-sync-c50f5",
stats: {
    "name": "Animal-api sync for customer NL_111928",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "49064",
        "ok": "49064",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "49064",
        "ok": "49064",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "49064",
        "ok": "49064",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "49064",
        "ok": "49064",
        "ko": "-"
    },
    "percentiles2": {
        "total": "49064",
        "ok": "49064",
        "ko": "-"
    },
    "percentiles3": {
        "total": "49064",
        "ok": "49064",
        "ko": "-"
    },
    "percentiles4": {
        "total": "49064",
        "ok": "49064",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-294ca": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111076",
path: "Animal-api sync for customer NL_111076",
pathFormatted: "req_animal-api-sync-294ca",
stats: {
    "name": "Animal-api sync for customer NL_111076",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "39984",
        "ok": "39984",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "39984",
        "ok": "39984",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "39984",
        "ok": "39984",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "39984",
        "ok": "39984",
        "ko": "-"
    },
    "percentiles2": {
        "total": "39984",
        "ok": "39984",
        "ko": "-"
    },
    "percentiles3": {
        "total": "39984",
        "ok": "39984",
        "ko": "-"
    },
    "percentiles4": {
        "total": "39984",
        "ok": "39984",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-080dc": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_131881",
path: "Animal-api sync for customer NL_131881",
pathFormatted: "req_animal-api-sync-080dc",
stats: {
    "name": "Animal-api sync for customer NL_131881",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "30447",
        "ok": "30447",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "30447",
        "ok": "30447",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "30447",
        "ok": "30447",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "30447",
        "ok": "30447",
        "ko": "-"
    },
    "percentiles2": {
        "total": "30447",
        "ok": "30447",
        "ko": "-"
    },
    "percentiles3": {
        "total": "30447",
        "ok": "30447",
        "ko": "-"
    },
    "percentiles4": {
        "total": "30447",
        "ok": "30447",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-9f153": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_113848",
path: "Animal-api sync for customer NL_113848",
pathFormatted: "req_animal-api-sync-9f153",
stats: {
    "name": "Animal-api sync for customer NL_113848",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "49171",
        "ok": "49171",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "49171",
        "ok": "49171",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "49171",
        "ok": "49171",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "49171",
        "ok": "49171",
        "ko": "-"
    },
    "percentiles2": {
        "total": "49171",
        "ok": "49171",
        "ko": "-"
    },
    "percentiles3": {
        "total": "49171",
        "ok": "49171",
        "ko": "-"
    },
    "percentiles4": {
        "total": "49171",
        "ok": "49171",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-084e7": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_108590",
path: "Animal-api sync for customer NL_108590",
pathFormatted: "req_animal-api-sync-084e7",
stats: {
    "name": "Animal-api sync for customer NL_108590",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "46665",
        "ok": "46665",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "46665",
        "ok": "46665",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "46665",
        "ok": "46665",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "46665",
        "ok": "46665",
        "ko": "-"
    },
    "percentiles2": {
        "total": "46665",
        "ok": "46665",
        "ko": "-"
    },
    "percentiles3": {
        "total": "46665",
        "ok": "46665",
        "ko": "-"
    },
    "percentiles4": {
        "total": "46665",
        "ok": "46665",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-93bcd": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_107077",
path: "Animal-api sync for customer NL_107077",
pathFormatted: "req_animal-api-sync-93bcd",
stats: {
    "name": "Animal-api sync for customer NL_107077",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "32776",
        "ok": "32776",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "32776",
        "ok": "32776",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "32776",
        "ok": "32776",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "32776",
        "ok": "32776",
        "ko": "-"
    },
    "percentiles2": {
        "total": "32776",
        "ok": "32776",
        "ko": "-"
    },
    "percentiles3": {
        "total": "32776",
        "ok": "32776",
        "ko": "-"
    },
    "percentiles4": {
        "total": "32776",
        "ok": "32776",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-43480": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_211973",
path: "Animal-api sync for customer BE_211973",
pathFormatted: "req_animal-api-sync-43480",
stats: {
    "name": "Animal-api sync for customer BE_211973",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "maxResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "meanResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles2": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles3": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles4": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-a5109": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_124277",
path: "Animal-api sync for customer NL_124277",
pathFormatted: "req_animal-api-sync-a5109",
stats: {
    "name": "Animal-api sync for customer NL_124277",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "maxResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "meanResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles2": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles3": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles4": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-53304": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_108829",
path: "Animal-api sync for customer NL_108829",
pathFormatted: "req_animal-api-sync-53304",
stats: {
    "name": "Animal-api sync for customer NL_108829",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "maxResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "meanResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles2": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles3": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles4": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-84fc1": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_131210",
path: "Animal-api sync for customer NL_131210",
pathFormatted: "req_animal-api-sync-84fc1",
stats: {
    "name": "Animal-api sync for customer NL_131210",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "43133",
        "ok": "43133",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "43133",
        "ok": "43133",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "43133",
        "ok": "43133",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "43133",
        "ok": "43133",
        "ko": "-"
    },
    "percentiles2": {
        "total": "43133",
        "ok": "43133",
        "ko": "-"
    },
    "percentiles3": {
        "total": "43133",
        "ok": "43133",
        "ko": "-"
    },
    "percentiles4": {
        "total": "43133",
        "ok": "43133",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-fc8bc": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_119277",
path: "Animal-api sync for customer NL_119277",
pathFormatted: "req_animal-api-sync-fc8bc",
stats: {
    "name": "Animal-api sync for customer NL_119277",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "37371",
        "ok": "37371",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "37371",
        "ok": "37371",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "37371",
        "ok": "37371",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "37371",
        "ok": "37371",
        "ko": "-"
    },
    "percentiles2": {
        "total": "37371",
        "ok": "37371",
        "ko": "-"
    },
    "percentiles3": {
        "total": "37371",
        "ok": "37371",
        "ko": "-"
    },
    "percentiles4": {
        "total": "37371",
        "ok": "37371",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b0637": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_109813",
path: "Animal-api sync for customer NL_109813",
pathFormatted: "req_animal-api-sync-b0637",
stats: {
    "name": "Animal-api sync for customer NL_109813",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "55035",
        "ok": "55035",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "55035",
        "ok": "55035",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "55035",
        "ok": "55035",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "55035",
        "ok": "55035",
        "ko": "-"
    },
    "percentiles2": {
        "total": "55035",
        "ok": "55035",
        "ko": "-"
    },
    "percentiles3": {
        "total": "55035",
        "ok": "55035",
        "ko": "-"
    },
    "percentiles4": {
        "total": "55035",
        "ok": "55035",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d7914": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_189320",
path: "Animal-api sync for customer NL_189320",
pathFormatted: "req_animal-api-sync-d7914",
stats: {
    "name": "Animal-api sync for customer NL_189320",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "54548",
        "ok": "54548",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "54548",
        "ok": "54548",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "54548",
        "ok": "54548",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "54548",
        "ok": "54548",
        "ko": "-"
    },
    "percentiles2": {
        "total": "54548",
        "ok": "54548",
        "ko": "-"
    },
    "percentiles3": {
        "total": "54548",
        "ok": "54548",
        "ko": "-"
    },
    "percentiles4": {
        "total": "54548",
        "ok": "54548",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-acdb8": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_113754",
path: "Animal-api sync for customer NL_113754",
pathFormatted: "req_animal-api-sync-acdb8",
stats: {
    "name": "Animal-api sync for customer NL_113754",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "maxResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "meanResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles2": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles3": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles4": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-36730": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_135526",
path: "Animal-api sync for customer NL_135526",
pathFormatted: "req_animal-api-sync-36730",
stats: {
    "name": "Animal-api sync for customer NL_135526",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "43954",
        "ok": "43954",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "43954",
        "ok": "43954",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "43954",
        "ok": "43954",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "43954",
        "ok": "43954",
        "ko": "-"
    },
    "percentiles2": {
        "total": "43954",
        "ok": "43954",
        "ko": "-"
    },
    "percentiles3": {
        "total": "43954",
        "ok": "43954",
        "ko": "-"
    },
    "percentiles4": {
        "total": "43954",
        "ok": "43954",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-1e909": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_154872",
path: "Animal-api sync for customer BE_154872",
pathFormatted: "req_animal-api-sync-1e909",
stats: {
    "name": "Animal-api sync for customer BE_154872",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "45200",
        "ok": "45200",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "45200",
        "ok": "45200",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "45200",
        "ok": "45200",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "45200",
        "ok": "45200",
        "ko": "-"
    },
    "percentiles2": {
        "total": "45200",
        "ok": "45200",
        "ko": "-"
    },
    "percentiles3": {
        "total": "45200",
        "ok": "45200",
        "ko": "-"
    },
    "percentiles4": {
        "total": "45200",
        "ok": "45200",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-f54d2": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_125689",
path: "Animal-api sync for customer NL_125689",
pathFormatted: "req_animal-api-sync-f54d2",
stats: {
    "name": "Animal-api sync for customer NL_125689",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "maxResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "meanResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles2": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles3": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles4": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-baa35": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110539",
path: "Animal-api sync for customer NL_110539",
pathFormatted: "req_animal-api-sync-baa35",
stats: {
    "name": "Animal-api sync for customer NL_110539",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "54073",
        "ok": "54073",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "54073",
        "ok": "54073",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "54073",
        "ok": "54073",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "54073",
        "ok": "54073",
        "ko": "-"
    },
    "percentiles2": {
        "total": "54073",
        "ok": "54073",
        "ko": "-"
    },
    "percentiles3": {
        "total": "54073",
        "ok": "54073",
        "ko": "-"
    },
    "percentiles4": {
        "total": "54073",
        "ok": "54073",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-14454": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104271",
path: "Animal-api sync for customer NL_104271",
pathFormatted: "req_animal-api-sync-14454",
stats: {
    "name": "Animal-api sync for customer NL_104271",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "maxResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "meanResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles2": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles3": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles4": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-b6907": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_123943",
path: "Animal-api sync for customer NL_123943",
pathFormatted: "req_animal-api-sync-b6907",
stats: {
    "name": "Animal-api sync for customer NL_123943",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "maxResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "meanResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles2": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles3": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles4": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-c4646": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110070",
path: "Animal-api sync for customer NL_110070",
pathFormatted: "req_animal-api-sync-c4646",
stats: {
    "name": "Animal-api sync for customer NL_110070",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "maxResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "meanResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles4": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-0eeba": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_114348",
path: "Animal-api sync for customer NL_114348",
pathFormatted: "req_animal-api-sync-0eeba",
stats: {
    "name": "Animal-api sync for customer NL_114348",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "maxResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "meanResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles2": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles3": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles4": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-5c35b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_114376",
path: "Animal-api sync for customer NL_114376",
pathFormatted: "req_animal-api-sync-5c35b",
stats: {
    "name": "Animal-api sync for customer NL_114376",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "40488",
        "ok": "40488",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "40488",
        "ok": "40488",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "40488",
        "ok": "40488",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "40488",
        "ok": "40488",
        "ko": "-"
    },
    "percentiles2": {
        "total": "40488",
        "ok": "40488",
        "ko": "-"
    },
    "percentiles3": {
        "total": "40488",
        "ok": "40488",
        "ko": "-"
    },
    "percentiles4": {
        "total": "40488",
        "ok": "40488",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-451e8": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_121268",
path: "Animal-api sync for customer NL_121268",
pathFormatted: "req_animal-api-sync-451e8",
stats: {
    "name": "Animal-api sync for customer NL_121268",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "maxResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "meanResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles2": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles3": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles4": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-e42e2": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_128581",
path: "Animal-api sync for customer NL_128581",
pathFormatted: "req_animal-api-sync-e42e2",
stats: {
    "name": "Animal-api sync for customer NL_128581",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "40656",
        "ok": "40656",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "40656",
        "ok": "40656",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "40656",
        "ok": "40656",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "40656",
        "ok": "40656",
        "ko": "-"
    },
    "percentiles2": {
        "total": "40656",
        "ok": "40656",
        "ko": "-"
    },
    "percentiles3": {
        "total": "40656",
        "ok": "40656",
        "ko": "-"
    },
    "percentiles4": {
        "total": "40656",
        "ok": "40656",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b164c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_120434",
path: "Animal-api sync for customer NL_120434",
pathFormatted: "req_animal-api-sync-b164c",
stats: {
    "name": "Animal-api sync for customer NL_120434",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60008",
        "ok": "-",
        "ko": "60008"
    },
    "maxResponseTime": {
        "total": "60008",
        "ok": "-",
        "ko": "60008"
    },
    "meanResponseTime": {
        "total": "60008",
        "ok": "-",
        "ko": "60008"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60008",
        "ok": "-",
        "ko": "60008"
    },
    "percentiles2": {
        "total": "60008",
        "ok": "-",
        "ko": "60008"
    },
    "percentiles3": {
        "total": "60008",
        "ok": "-",
        "ko": "60008"
    },
    "percentiles4": {
        "total": "60008",
        "ok": "-",
        "ko": "60008"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-7461a": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_120398",
path: "Animal-api sync for customer NL_120398",
pathFormatted: "req_animal-api-sync-7461a",
stats: {
    "name": "Animal-api sync for customer NL_120398",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "49579",
        "ok": "49579",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "49579",
        "ok": "49579",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "49579",
        "ok": "49579",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "49579",
        "ok": "49579",
        "ko": "-"
    },
    "percentiles2": {
        "total": "49579",
        "ok": "49579",
        "ko": "-"
    },
    "percentiles3": {
        "total": "49579",
        "ok": "49579",
        "ko": "-"
    },
    "percentiles4": {
        "total": "49579",
        "ok": "49579",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-07a69": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_103947",
path: "Animal-api sync for customer NL_103947",
pathFormatted: "req_animal-api-sync-07a69",
stats: {
    "name": "Animal-api sync for customer NL_103947",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60009",
        "ok": "-",
        "ko": "60009"
    },
    "maxResponseTime": {
        "total": "60009",
        "ok": "-",
        "ko": "60009"
    },
    "meanResponseTime": {
        "total": "60009",
        "ok": "-",
        "ko": "60009"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60009",
        "ok": "-",
        "ko": "60009"
    },
    "percentiles2": {
        "total": "60009",
        "ok": "-",
        "ko": "60009"
    },
    "percentiles3": {
        "total": "60009",
        "ok": "-",
        "ko": "60009"
    },
    "percentiles4": {
        "total": "60009",
        "ok": "-",
        "ko": "60009"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-c6405": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104467",
path: "Animal-api sync for customer NL_104467",
pathFormatted: "req_animal-api-sync-c6405",
stats: {
    "name": "Animal-api sync for customer NL_104467",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60008",
        "ok": "-",
        "ko": "60008"
    },
    "maxResponseTime": {
        "total": "60008",
        "ok": "-",
        "ko": "60008"
    },
    "meanResponseTime": {
        "total": "60008",
        "ok": "-",
        "ko": "60008"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60008",
        "ok": "-",
        "ko": "60008"
    },
    "percentiles2": {
        "total": "60008",
        "ok": "-",
        "ko": "60008"
    },
    "percentiles3": {
        "total": "60008",
        "ok": "-",
        "ko": "60008"
    },
    "percentiles4": {
        "total": "60008",
        "ok": "-",
        "ko": "60008"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-23f13": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_125273",
path: "Animal-api sync for customer NL_125273",
pathFormatted: "req_animal-api-sync-23f13",
stats: {
    "name": "Animal-api sync for customer NL_125273",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60008",
        "ok": "-",
        "ko": "60008"
    },
    "maxResponseTime": {
        "total": "60008",
        "ok": "-",
        "ko": "60008"
    },
    "meanResponseTime": {
        "total": "60008",
        "ok": "-",
        "ko": "60008"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60008",
        "ok": "-",
        "ko": "60008"
    },
    "percentiles2": {
        "total": "60008",
        "ok": "-",
        "ko": "60008"
    },
    "percentiles3": {
        "total": "60008",
        "ok": "-",
        "ko": "60008"
    },
    "percentiles4": {
        "total": "60008",
        "ok": "-",
        "ko": "60008"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-96127": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_161584",
path: "Animal-api sync for customer BE_161584",
pathFormatted: "req_animal-api-sync-96127",
stats: {
    "name": "Animal-api sync for customer BE_161584",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "maxResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "meanResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles2": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles3": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles4": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-614ac": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104734",
path: "Animal-api sync for customer NL_104734",
pathFormatted: "req_animal-api-sync-614ac",
stats: {
    "name": "Animal-api sync for customer NL_104734",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60009",
        "ok": "-",
        "ko": "60009"
    },
    "maxResponseTime": {
        "total": "60009",
        "ok": "-",
        "ko": "60009"
    },
    "meanResponseTime": {
        "total": "60009",
        "ok": "-",
        "ko": "60009"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60009",
        "ok": "-",
        "ko": "60009"
    },
    "percentiles2": {
        "total": "60009",
        "ok": "-",
        "ko": "60009"
    },
    "percentiles3": {
        "total": "60009",
        "ok": "-",
        "ko": "60009"
    },
    "percentiles4": {
        "total": "60009",
        "ok": "-",
        "ko": "60009"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-2d56c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110590",
path: "Animal-api sync for customer NL_110590",
pathFormatted: "req_animal-api-sync-2d56c",
stats: {
    "name": "Animal-api sync for customer NL_110590",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "47821",
        "ok": "47821",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "47821",
        "ok": "47821",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "47821",
        "ok": "47821",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "47821",
        "ok": "47821",
        "ko": "-"
    },
    "percentiles2": {
        "total": "47821",
        "ok": "47821",
        "ko": "-"
    },
    "percentiles3": {
        "total": "47821",
        "ok": "47821",
        "ko": "-"
    },
    "percentiles4": {
        "total": "47821",
        "ok": "47821",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e31c7": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_210459",
path: "Animal-api sync for customer NL_210459",
pathFormatted: "req_animal-api-sync-e31c7",
stats: {
    "name": "Animal-api sync for customer NL_210459",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "maxResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "meanResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles4": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-7fca2": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_158798",
path: "Animal-api sync for customer NL_158798",
pathFormatted: "req_animal-api-sync-7fca2",
stats: {
    "name": "Animal-api sync for customer NL_158798",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60008",
        "ok": "-",
        "ko": "60008"
    },
    "maxResponseTime": {
        "total": "60008",
        "ok": "-",
        "ko": "60008"
    },
    "meanResponseTime": {
        "total": "60008",
        "ok": "-",
        "ko": "60008"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60008",
        "ok": "-",
        "ko": "60008"
    },
    "percentiles2": {
        "total": "60008",
        "ok": "-",
        "ko": "60008"
    },
    "percentiles3": {
        "total": "60008",
        "ok": "-",
        "ko": "60008"
    },
    "percentiles4": {
        "total": "60008",
        "ok": "-",
        "ko": "60008"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-b8f29": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_153558",
path: "Animal-api sync for customer BE_153558",
pathFormatted: "req_animal-api-sync-b8f29",
stats: {
    "name": "Animal-api sync for customer BE_153558",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "44400",
        "ok": "44400",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "44400",
        "ok": "44400",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "44400",
        "ok": "44400",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "44400",
        "ok": "44400",
        "ko": "-"
    },
    "percentiles2": {
        "total": "44400",
        "ok": "44400",
        "ko": "-"
    },
    "percentiles3": {
        "total": "44400",
        "ok": "44400",
        "ko": "-"
    },
    "percentiles4": {
        "total": "44400",
        "ok": "44400",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-aa22e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_137034",
path: "Animal-api sync for customer NL_137034",
pathFormatted: "req_animal-api-sync-aa22e",
stats: {
    "name": "Animal-api sync for customer NL_137034",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "43096",
        "ok": "43096",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "43096",
        "ok": "43096",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "43096",
        "ok": "43096",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "43096",
        "ok": "43096",
        "ko": "-"
    },
    "percentiles2": {
        "total": "43096",
        "ok": "43096",
        "ko": "-"
    },
    "percentiles3": {
        "total": "43096",
        "ok": "43096",
        "ko": "-"
    },
    "percentiles4": {
        "total": "43096",
        "ok": "43096",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b5283": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_112270",
path: "Animal-api sync for customer NL_112270",
pathFormatted: "req_animal-api-sync-b5283",
stats: {
    "name": "Animal-api sync for customer NL_112270",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60010",
        "ok": "-",
        "ko": "60010"
    },
    "maxResponseTime": {
        "total": "60010",
        "ok": "-",
        "ko": "60010"
    },
    "meanResponseTime": {
        "total": "60010",
        "ok": "-",
        "ko": "60010"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60010",
        "ok": "-",
        "ko": "60010"
    },
    "percentiles2": {
        "total": "60010",
        "ok": "-",
        "ko": "60010"
    },
    "percentiles3": {
        "total": "60010",
        "ok": "-",
        "ko": "60010"
    },
    "percentiles4": {
        "total": "60010",
        "ok": "-",
        "ko": "60010"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-10e14": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_113903",
path: "Animal-api sync for customer NL_113903",
pathFormatted: "req_animal-api-sync-10e14",
stats: {
    "name": "Animal-api sync for customer NL_113903",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60010",
        "ok": "-",
        "ko": "60010"
    },
    "maxResponseTime": {
        "total": "60010",
        "ok": "-",
        "ko": "60010"
    },
    "meanResponseTime": {
        "total": "60010",
        "ok": "-",
        "ko": "60010"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60010",
        "ok": "-",
        "ko": "60010"
    },
    "percentiles2": {
        "total": "60010",
        "ok": "-",
        "ko": "60010"
    },
    "percentiles3": {
        "total": "60010",
        "ok": "-",
        "ko": "60010"
    },
    "percentiles4": {
        "total": "60010",
        "ok": "-",
        "ko": "60010"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-3dd2c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105439",
path: "Animal-api sync for customer NL_105439",
pathFormatted: "req_animal-api-sync-3dd2c",
stats: {
    "name": "Animal-api sync for customer NL_105439",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60009",
        "ok": "-",
        "ko": "60009"
    },
    "maxResponseTime": {
        "total": "60009",
        "ok": "-",
        "ko": "60009"
    },
    "meanResponseTime": {
        "total": "60009",
        "ok": "-",
        "ko": "60009"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60009",
        "ok": "-",
        "ko": "60009"
    },
    "percentiles2": {
        "total": "60009",
        "ok": "-",
        "ko": "60009"
    },
    "percentiles3": {
        "total": "60009",
        "ok": "-",
        "ko": "60009"
    },
    "percentiles4": {
        "total": "60009",
        "ok": "-",
        "ko": "60009"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-8785d": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_207024",
path: "Animal-api sync for customer BE_207024",
pathFormatted: "req_animal-api-sync-8785d",
stats: {
    "name": "Animal-api sync for customer BE_207024",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "40543",
        "ok": "40543",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "40543",
        "ok": "40543",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "40543",
        "ok": "40543",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "40543",
        "ok": "40543",
        "ko": "-"
    },
    "percentiles2": {
        "total": "40543",
        "ok": "40543",
        "ko": "-"
    },
    "percentiles3": {
        "total": "40543",
        "ok": "40543",
        "ko": "-"
    },
    "percentiles4": {
        "total": "40543",
        "ok": "40543",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-010fe": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_129945",
path: "Animal-api sync for customer NL_129945",
pathFormatted: "req_animal-api-sync-010fe",
stats: {
    "name": "Animal-api sync for customer NL_129945",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "58145",
        "ok": "58145",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "58145",
        "ok": "58145",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "58145",
        "ok": "58145",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "58145",
        "ok": "58145",
        "ko": "-"
    },
    "percentiles2": {
        "total": "58145",
        "ok": "58145",
        "ko": "-"
    },
    "percentiles3": {
        "total": "58145",
        "ok": "58145",
        "ko": "-"
    },
    "percentiles4": {
        "total": "58145",
        "ok": "58145",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-58bfa": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_131425",
path: "Animal-api sync for customer NL_131425",
pathFormatted: "req_animal-api-sync-58bfa",
stats: {
    "name": "Animal-api sync for customer NL_131425",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60008",
        "ok": "-",
        "ko": "60008"
    },
    "maxResponseTime": {
        "total": "60008",
        "ok": "-",
        "ko": "60008"
    },
    "meanResponseTime": {
        "total": "60008",
        "ok": "-",
        "ko": "60008"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60008",
        "ok": "-",
        "ko": "60008"
    },
    "percentiles2": {
        "total": "60008",
        "ok": "-",
        "ko": "60008"
    },
    "percentiles3": {
        "total": "60008",
        "ok": "-",
        "ko": "60008"
    },
    "percentiles4": {
        "total": "60008",
        "ok": "-",
        "ko": "60008"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-9a226": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_113819",
path: "Animal-api sync for customer NL_113819",
pathFormatted: "req_animal-api-sync-9a226",
stats: {
    "name": "Animal-api sync for customer NL_113819",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "36821",
        "ok": "36821",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "36821",
        "ok": "36821",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "36821",
        "ok": "36821",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "36821",
        "ok": "36821",
        "ko": "-"
    },
    "percentiles2": {
        "total": "36821",
        "ok": "36821",
        "ko": "-"
    },
    "percentiles3": {
        "total": "36821",
        "ok": "36821",
        "ko": "-"
    },
    "percentiles4": {
        "total": "36821",
        "ok": "36821",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-44a53": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_121469",
path: "Animal-api sync for customer NL_121469",
pathFormatted: "req_animal-api-sync-44a53",
stats: {
    "name": "Animal-api sync for customer NL_121469",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "maxResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "meanResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles2": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles3": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles4": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-c3fd5": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104563",
path: "Animal-api sync for customer NL_104563",
pathFormatted: "req_animal-api-sync-c3fd5",
stats: {
    "name": "Animal-api sync for customer NL_104563",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60008",
        "ok": "-",
        "ko": "60008"
    },
    "maxResponseTime": {
        "total": "60008",
        "ok": "-",
        "ko": "60008"
    },
    "meanResponseTime": {
        "total": "60008",
        "ok": "-",
        "ko": "60008"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60008",
        "ok": "-",
        "ko": "60008"
    },
    "percentiles2": {
        "total": "60008",
        "ok": "-",
        "ko": "60008"
    },
    "percentiles3": {
        "total": "60008",
        "ok": "-",
        "ko": "60008"
    },
    "percentiles4": {
        "total": "60008",
        "ok": "-",
        "ko": "60008"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-2f86f": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_122640",
path: "Animal-api sync for customer NL_122640",
pathFormatted: "req_animal-api-sync-2f86f",
stats: {
    "name": "Animal-api sync for customer NL_122640",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "49364",
        "ok": "49364",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "49364",
        "ok": "49364",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "49364",
        "ok": "49364",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "49364",
        "ok": "49364",
        "ko": "-"
    },
    "percentiles2": {
        "total": "49364",
        "ok": "49364",
        "ko": "-"
    },
    "percentiles3": {
        "total": "49364",
        "ok": "49364",
        "ko": "-"
    },
    "percentiles4": {
        "total": "49364",
        "ok": "49364",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-447ce": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_136064",
path: "Animal-api sync for customer NL_136064",
pathFormatted: "req_animal-api-sync-447ce",
stats: {
    "name": "Animal-api sync for customer NL_136064",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "44160",
        "ok": "44160",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "44160",
        "ok": "44160",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "44160",
        "ok": "44160",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "44160",
        "ok": "44160",
        "ko": "-"
    },
    "percentiles2": {
        "total": "44160",
        "ok": "44160",
        "ko": "-"
    },
    "percentiles3": {
        "total": "44160",
        "ok": "44160",
        "ko": "-"
    },
    "percentiles4": {
        "total": "44160",
        "ok": "44160",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-02723": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_106633",
path: "Animal-api sync for customer NL_106633",
pathFormatted: "req_animal-api-sync-02723",
stats: {
    "name": "Animal-api sync for customer NL_106633",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "56870",
        "ok": "56870",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "56870",
        "ok": "56870",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "56870",
        "ok": "56870",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "56870",
        "ok": "56870",
        "ko": "-"
    },
    "percentiles2": {
        "total": "56870",
        "ok": "56870",
        "ko": "-"
    },
    "percentiles3": {
        "total": "56870",
        "ok": "56870",
        "ko": "-"
    },
    "percentiles4": {
        "total": "56870",
        "ok": "56870",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-19174": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117339",
path: "Animal-api sync for customer NL_117339",
pathFormatted: "req_animal-api-sync-19174",
stats: {
    "name": "Animal-api sync for customer NL_117339",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60006",
        "ok": "-",
        "ko": "60006"
    },
    "maxResponseTime": {
        "total": "60006",
        "ok": "-",
        "ko": "60006"
    },
    "meanResponseTime": {
        "total": "60006",
        "ok": "-",
        "ko": "60006"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60006",
        "ok": "-",
        "ko": "60006"
    },
    "percentiles2": {
        "total": "60006",
        "ok": "-",
        "ko": "60006"
    },
    "percentiles3": {
        "total": "60006",
        "ok": "-",
        "ko": "60006"
    },
    "percentiles4": {
        "total": "60006",
        "ok": "-",
        "ko": "60006"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-9a47c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_115141",
path: "Animal-api sync for customer NL_115141",
pathFormatted: "req_animal-api-sync-9a47c",
stats: {
    "name": "Animal-api sync for customer NL_115141",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60006",
        "ok": "-",
        "ko": "60006"
    },
    "maxResponseTime": {
        "total": "60006",
        "ok": "-",
        "ko": "60006"
    },
    "meanResponseTime": {
        "total": "60006",
        "ok": "-",
        "ko": "60006"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60006",
        "ok": "-",
        "ko": "60006"
    },
    "percentiles2": {
        "total": "60006",
        "ok": "-",
        "ko": "60006"
    },
    "percentiles3": {
        "total": "60006",
        "ok": "-",
        "ko": "60006"
    },
    "percentiles4": {
        "total": "60006",
        "ok": "-",
        "ko": "60006"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-0a9fa": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_125735",
path: "Animal-api sync for customer NL_125735",
pathFormatted: "req_animal-api-sync-0a9fa",
stats: {
    "name": "Animal-api sync for customer NL_125735",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "52625",
        "ok": "52625",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "52625",
        "ok": "52625",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "52625",
        "ok": "52625",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "52625",
        "ok": "52625",
        "ko": "-"
    },
    "percentiles2": {
        "total": "52625",
        "ok": "52625",
        "ko": "-"
    },
    "percentiles3": {
        "total": "52625",
        "ok": "52625",
        "ko": "-"
    },
    "percentiles4": {
        "total": "52625",
        "ok": "52625",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-56dad": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_112805",
path: "Animal-api sync for customer NL_112805",
pathFormatted: "req_animal-api-sync-56dad",
stats: {
    "name": "Animal-api sync for customer NL_112805",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "51506",
        "ok": "51506",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "51506",
        "ok": "51506",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "51506",
        "ok": "51506",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "51506",
        "ok": "51506",
        "ko": "-"
    },
    "percentiles2": {
        "total": "51506",
        "ok": "51506",
        "ko": "-"
    },
    "percentiles3": {
        "total": "51506",
        "ok": "51506",
        "ko": "-"
    },
    "percentiles4": {
        "total": "51506",
        "ok": "51506",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-4c3a5": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110894",
path: "Animal-api sync for customer NL_110894",
pathFormatted: "req_animal-api-sync-4c3a5",
stats: {
    "name": "Animal-api sync for customer NL_110894",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "59266",
        "ok": "59266",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "59266",
        "ok": "59266",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "59266",
        "ok": "59266",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "59266",
        "ok": "59266",
        "ko": "-"
    },
    "percentiles2": {
        "total": "59266",
        "ok": "59266",
        "ko": "-"
    },
    "percentiles3": {
        "total": "59266",
        "ok": "59266",
        "ko": "-"
    },
    "percentiles4": {
        "total": "59266",
        "ok": "59266",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-86c49": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_145826",
path: "Animal-api sync for customer BE_145826",
pathFormatted: "req_animal-api-sync-86c49",
stats: {
    "name": "Animal-api sync for customer BE_145826",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "50238",
        "ok": "50238",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "50238",
        "ok": "50238",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "50238",
        "ok": "50238",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "50238",
        "ok": "50238",
        "ko": "-"
    },
    "percentiles2": {
        "total": "50238",
        "ok": "50238",
        "ko": "-"
    },
    "percentiles3": {
        "total": "50238",
        "ok": "50238",
        "ko": "-"
    },
    "percentiles4": {
        "total": "50238",
        "ok": "50238",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-9e7ff": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110428",
path: "Animal-api sync for customer NL_110428",
pathFormatted: "req_animal-api-sync-9e7ff",
stats: {
    "name": "Animal-api sync for customer NL_110428",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60009",
        "ok": "-",
        "ko": "60009"
    },
    "maxResponseTime": {
        "total": "60009",
        "ok": "-",
        "ko": "60009"
    },
    "meanResponseTime": {
        "total": "60009",
        "ok": "-",
        "ko": "60009"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60009",
        "ok": "-",
        "ko": "60009"
    },
    "percentiles2": {
        "total": "60009",
        "ok": "-",
        "ko": "60009"
    },
    "percentiles3": {
        "total": "60009",
        "ok": "-",
        "ko": "60009"
    },
    "percentiles4": {
        "total": "60009",
        "ok": "-",
        "ko": "60009"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-6c072": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_115279",
path: "Animal-api sync for customer NL_115279",
pathFormatted: "req_animal-api-sync-6c072",
stats: {
    "name": "Animal-api sync for customer NL_115279",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60010",
        "ok": "-",
        "ko": "60010"
    },
    "maxResponseTime": {
        "total": "60010",
        "ok": "-",
        "ko": "60010"
    },
    "meanResponseTime": {
        "total": "60010",
        "ok": "-",
        "ko": "60010"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60010",
        "ok": "-",
        "ko": "60010"
    },
    "percentiles2": {
        "total": "60010",
        "ok": "-",
        "ko": "60010"
    },
    "percentiles3": {
        "total": "60010",
        "ok": "-",
        "ko": "60010"
    },
    "percentiles4": {
        "total": "60010",
        "ok": "-",
        "ko": "60010"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-2de80": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_129343",
path: "Animal-api sync for customer NL_129343",
pathFormatted: "req_animal-api-sync-2de80",
stats: {
    "name": "Animal-api sync for customer NL_129343",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "49380",
        "ok": "49380",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "49380",
        "ok": "49380",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "49380",
        "ok": "49380",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "49380",
        "ok": "49380",
        "ko": "-"
    },
    "percentiles2": {
        "total": "49380",
        "ok": "49380",
        "ko": "-"
    },
    "percentiles3": {
        "total": "49380",
        "ok": "49380",
        "ko": "-"
    },
    "percentiles4": {
        "total": "49380",
        "ok": "49380",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-925cc": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_120686",
path: "Animal-api sync for customer NL_120686",
pathFormatted: "req_animal-api-sync-925cc",
stats: {
    "name": "Animal-api sync for customer NL_120686",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "maxResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "meanResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles4": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-f6a28": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_129516",
path: "Animal-api sync for customer NL_129516",
pathFormatted: "req_animal-api-sync-f6a28",
stats: {
    "name": "Animal-api sync for customer NL_129516",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "maxResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "meanResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles2": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles3": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles4": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-10907": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_155615",
path: "Animal-api sync for customer NL_155615",
pathFormatted: "req_animal-api-sync-10907",
stats: {
    "name": "Animal-api sync for customer NL_155615",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "maxResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "meanResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles2": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles3": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles4": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-83fad": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_161486",
path: "Animal-api sync for customer NL_161486",
pathFormatted: "req_animal-api-sync-83fad",
stats: {
    "name": "Animal-api sync for customer NL_161486",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "59485",
        "ok": "59485",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "59485",
        "ok": "59485",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "59485",
        "ok": "59485",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "59485",
        "ok": "59485",
        "ko": "-"
    },
    "percentiles2": {
        "total": "59485",
        "ok": "59485",
        "ko": "-"
    },
    "percentiles3": {
        "total": "59485",
        "ok": "59485",
        "ko": "-"
    },
    "percentiles4": {
        "total": "59485",
        "ok": "59485",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-6eeed": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_136560",
path: "Animal-api sync for customer NL_136560",
pathFormatted: "req_animal-api-sync-6eeed",
stats: {
    "name": "Animal-api sync for customer NL_136560",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "50068",
        "ok": "50068",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "50068",
        "ok": "50068",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "50068",
        "ok": "50068",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "50068",
        "ok": "50068",
        "ko": "-"
    },
    "percentiles2": {
        "total": "50068",
        "ok": "50068",
        "ko": "-"
    },
    "percentiles3": {
        "total": "50068",
        "ok": "50068",
        "ko": "-"
    },
    "percentiles4": {
        "total": "50068",
        "ok": "50068",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-678da": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_116304",
path: "Animal-api sync for customer NL_116304",
pathFormatted: "req_animal-api-sync-678da",
stats: {
    "name": "Animal-api sync for customer NL_116304",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "maxResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "meanResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles2": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles3": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles4": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-23e9e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_136624",
path: "Animal-api sync for customer NL_136624",
pathFormatted: "req_animal-api-sync-23e9e",
stats: {
    "name": "Animal-api sync for customer NL_136624",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "53206",
        "ok": "53206",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "53206",
        "ok": "53206",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "53206",
        "ok": "53206",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "53206",
        "ok": "53206",
        "ko": "-"
    },
    "percentiles2": {
        "total": "53206",
        "ok": "53206",
        "ko": "-"
    },
    "percentiles3": {
        "total": "53206",
        "ok": "53206",
        "ko": "-"
    },
    "percentiles4": {
        "total": "53206",
        "ok": "53206",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b45fa": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_160578",
path: "Animal-api sync for customer BE_160578",
pathFormatted: "req_animal-api-sync-b45fa",
stats: {
    "name": "Animal-api sync for customer BE_160578",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "maxResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "meanResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles2": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles3": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles4": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-ed8de": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117329",
path: "Animal-api sync for customer NL_117329",
pathFormatted: "req_animal-api-sync-ed8de",
stats: {
    "name": "Animal-api sync for customer NL_117329",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60010",
        "ok": "-",
        "ko": "60010"
    },
    "maxResponseTime": {
        "total": "60010",
        "ok": "-",
        "ko": "60010"
    },
    "meanResponseTime": {
        "total": "60010",
        "ok": "-",
        "ko": "60010"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60010",
        "ok": "-",
        "ko": "60010"
    },
    "percentiles2": {
        "total": "60010",
        "ok": "-",
        "ko": "60010"
    },
    "percentiles3": {
        "total": "60010",
        "ok": "-",
        "ko": "60010"
    },
    "percentiles4": {
        "total": "60010",
        "ok": "-",
        "ko": "60010"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-7699b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_115653",
path: "Animal-api sync for customer NL_115653",
pathFormatted: "req_animal-api-sync-7699b",
stats: {
    "name": "Animal-api sync for customer NL_115653",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "49710",
        "ok": "49710",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "49710",
        "ok": "49710",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "49710",
        "ok": "49710",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "49710",
        "ok": "49710",
        "ko": "-"
    },
    "percentiles2": {
        "total": "49710",
        "ok": "49710",
        "ko": "-"
    },
    "percentiles3": {
        "total": "49710",
        "ok": "49710",
        "ko": "-"
    },
    "percentiles4": {
        "total": "49710",
        "ok": "49710",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-f6750": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_145660",
path: "Animal-api sync for customer BE_145660",
pathFormatted: "req_animal-api-sync-f6750",
stats: {
    "name": "Animal-api sync for customer BE_145660",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "maxResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "meanResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles2": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles3": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles4": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-de4e1": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_196422",
path: "Animal-api sync for customer BE_196422",
pathFormatted: "req_animal-api-sync-de4e1",
stats: {
    "name": "Animal-api sync for customer BE_196422",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "47177",
        "ok": "47177",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "47177",
        "ok": "47177",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "47177",
        "ok": "47177",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "47177",
        "ok": "47177",
        "ko": "-"
    },
    "percentiles2": {
        "total": "47177",
        "ok": "47177",
        "ko": "-"
    },
    "percentiles3": {
        "total": "47177",
        "ok": "47177",
        "ko": "-"
    },
    "percentiles4": {
        "total": "47177",
        "ok": "47177",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-94458": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111031",
path: "Animal-api sync for customer NL_111031",
pathFormatted: "req_animal-api-sync-94458",
stats: {
    "name": "Animal-api sync for customer NL_111031",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "58823",
        "ok": "58823",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "58823",
        "ok": "58823",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "58823",
        "ok": "58823",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "58823",
        "ok": "58823",
        "ko": "-"
    },
    "percentiles2": {
        "total": "58823",
        "ok": "58823",
        "ko": "-"
    },
    "percentiles3": {
        "total": "58823",
        "ok": "58823",
        "ko": "-"
    },
    "percentiles4": {
        "total": "58823",
        "ok": "58823",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-1114b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105737",
path: "Animal-api sync for customer NL_105737",
pathFormatted: "req_animal-api-sync-1114b",
stats: {
    "name": "Animal-api sync for customer NL_105737",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "55179",
        "ok": "55179",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "55179",
        "ok": "55179",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "55179",
        "ok": "55179",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "55179",
        "ok": "55179",
        "ko": "-"
    },
    "percentiles2": {
        "total": "55179",
        "ok": "55179",
        "ko": "-"
    },
    "percentiles3": {
        "total": "55179",
        "ok": "55179",
        "ko": "-"
    },
    "percentiles4": {
        "total": "55179",
        "ok": "55179",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-34261": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_112642",
path: "Animal-api sync for customer NL_112642",
pathFormatted: "req_animal-api-sync-34261",
stats: {
    "name": "Animal-api sync for customer NL_112642",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "maxResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "meanResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles2": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles3": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles4": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-88106": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_112559",
path: "Animal-api sync for customer NL_112559",
pathFormatted: "req_animal-api-sync-88106",
stats: {
    "name": "Animal-api sync for customer NL_112559",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "49298",
        "ok": "49298",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "49298",
        "ok": "49298",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "49298",
        "ok": "49298",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "49298",
        "ok": "49298",
        "ko": "-"
    },
    "percentiles2": {
        "total": "49298",
        "ok": "49298",
        "ko": "-"
    },
    "percentiles3": {
        "total": "49298",
        "ok": "49298",
        "ko": "-"
    },
    "percentiles4": {
        "total": "49298",
        "ok": "49298",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-12cf4": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_103806",
path: "Animal-api sync for customer NL_103806",
pathFormatted: "req_animal-api-sync-12cf4",
stats: {
    "name": "Animal-api sync for customer NL_103806",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "58913",
        "ok": "58913",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "58913",
        "ok": "58913",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "58913",
        "ok": "58913",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "58913",
        "ok": "58913",
        "ko": "-"
    },
    "percentiles2": {
        "total": "58913",
        "ok": "58913",
        "ko": "-"
    },
    "percentiles3": {
        "total": "58913",
        "ok": "58913",
        "ko": "-"
    },
    "percentiles4": {
        "total": "58913",
        "ok": "58913",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-0c8bf": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_135056",
path: "Animal-api sync for customer NL_135056",
pathFormatted: "req_animal-api-sync-0c8bf",
stats: {
    "name": "Animal-api sync for customer NL_135056",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "56317",
        "ok": "56317",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "56317",
        "ok": "56317",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "56317",
        "ok": "56317",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "56317",
        "ok": "56317",
        "ko": "-"
    },
    "percentiles2": {
        "total": "56317",
        "ok": "56317",
        "ko": "-"
    },
    "percentiles3": {
        "total": "56317",
        "ok": "56317",
        "ko": "-"
    },
    "percentiles4": {
        "total": "56317",
        "ok": "56317",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-cd315": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_115640",
path: "Animal-api sync for customer NL_115640",
pathFormatted: "req_animal-api-sync-cd315",
stats: {
    "name": "Animal-api sync for customer NL_115640",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "56690",
        "ok": "56690",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "56690",
        "ok": "56690",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "56690",
        "ok": "56690",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "56690",
        "ok": "56690",
        "ko": "-"
    },
    "percentiles2": {
        "total": "56690",
        "ok": "56690",
        "ko": "-"
    },
    "percentiles3": {
        "total": "56690",
        "ok": "56690",
        "ko": "-"
    },
    "percentiles4": {
        "total": "56690",
        "ok": "56690",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-708ff": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111792",
path: "Animal-api sync for customer NL_111792",
pathFormatted: "req_animal-api-sync-708ff",
stats: {
    "name": "Animal-api sync for customer NL_111792",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "maxResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "meanResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles2": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles3": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles4": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-c74f5": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110479",
path: "Animal-api sync for customer NL_110479",
pathFormatted: "req_animal-api-sync-c74f5",
stats: {
    "name": "Animal-api sync for customer NL_110479",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "maxResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "meanResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles4": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-b4519": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110383",
path: "Animal-api sync for customer NL_110383",
pathFormatted: "req_animal-api-sync-b4519",
stats: {
    "name": "Animal-api sync for customer NL_110383",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "56854",
        "ok": "56854",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "56854",
        "ok": "56854",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "56854",
        "ok": "56854",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "56854",
        "ok": "56854",
        "ko": "-"
    },
    "percentiles2": {
        "total": "56854",
        "ok": "56854",
        "ko": "-"
    },
    "percentiles3": {
        "total": "56854",
        "ok": "56854",
        "ko": "-"
    },
    "percentiles4": {
        "total": "56854",
        "ok": "56854",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-ccec3": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_159045",
path: "Animal-api sync for customer BE_159045",
pathFormatted: "req_animal-api-sync-ccec3",
stats: {
    "name": "Animal-api sync for customer BE_159045",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "maxResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "meanResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles2": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles3": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles4": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-f73d7": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_112171",
path: "Animal-api sync for customer NL_112171",
pathFormatted: "req_animal-api-sync-f73d7",
stats: {
    "name": "Animal-api sync for customer NL_112171",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "maxResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "meanResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles2": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles3": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles4": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-c61a4": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_158630",
path: "Animal-api sync for customer NL_158630",
pathFormatted: "req_animal-api-sync-c61a4",
stats: {
    "name": "Animal-api sync for customer NL_158630",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "maxResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "meanResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles4": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-3df21": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111890",
path: "Animal-api sync for customer NL_111890",
pathFormatted: "req_animal-api-sync-3df21",
stats: {
    "name": "Animal-api sync for customer NL_111890",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60009",
        "ok": "-",
        "ko": "60009"
    },
    "maxResponseTime": {
        "total": "60009",
        "ok": "-",
        "ko": "60009"
    },
    "meanResponseTime": {
        "total": "60009",
        "ok": "-",
        "ko": "60009"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60009",
        "ok": "-",
        "ko": "60009"
    },
    "percentiles2": {
        "total": "60009",
        "ok": "-",
        "ko": "60009"
    },
    "percentiles3": {
        "total": "60009",
        "ok": "-",
        "ko": "60009"
    },
    "percentiles4": {
        "total": "60009",
        "ok": "-",
        "ko": "60009"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-11d0e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_119978",
path: "Animal-api sync for customer NL_119978",
pathFormatted: "req_animal-api-sync-11d0e",
stats: {
    "name": "Animal-api sync for customer NL_119978",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "maxResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "meanResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles2": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles3": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles4": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-cd0f8": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117391",
path: "Animal-api sync for customer NL_117391",
pathFormatted: "req_animal-api-sync-cd0f8",
stats: {
    "name": "Animal-api sync for customer NL_117391",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "58074",
        "ok": "58074",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "58074",
        "ok": "58074",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "58074",
        "ok": "58074",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "58074",
        "ok": "58074",
        "ko": "-"
    },
    "percentiles2": {
        "total": "58074",
        "ok": "58074",
        "ko": "-"
    },
    "percentiles3": {
        "total": "58074",
        "ok": "58074",
        "ko": "-"
    },
    "percentiles4": {
        "total": "58074",
        "ok": "58074",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-997ea": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111277",
path: "Animal-api sync for customer NL_111277",
pathFormatted: "req_animal-api-sync-997ea",
stats: {
    "name": "Animal-api sync for customer NL_111277",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60007",
        "ok": "-",
        "ko": "60007"
    },
    "maxResponseTime": {
        "total": "60007",
        "ok": "-",
        "ko": "60007"
    },
    "meanResponseTime": {
        "total": "60007",
        "ok": "-",
        "ko": "60007"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60007",
        "ok": "-",
        "ko": "60007"
    },
    "percentiles2": {
        "total": "60007",
        "ok": "-",
        "ko": "60007"
    },
    "percentiles3": {
        "total": "60007",
        "ok": "-",
        "ko": "60007"
    },
    "percentiles4": {
        "total": "60007",
        "ok": "-",
        "ko": "60007"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-998da": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_120046",
path: "Animal-api sync for customer NL_120046",
pathFormatted: "req_animal-api-sync-998da",
stats: {
    "name": "Animal-api sync for customer NL_120046",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "49705",
        "ok": "49705",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "49705",
        "ok": "49705",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "49705",
        "ok": "49705",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "49705",
        "ok": "49705",
        "ko": "-"
    },
    "percentiles2": {
        "total": "49705",
        "ok": "49705",
        "ko": "-"
    },
    "percentiles3": {
        "total": "49705",
        "ok": "49705",
        "ko": "-"
    },
    "percentiles4": {
        "total": "49705",
        "ok": "49705",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-08b09": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_122753",
path: "Animal-api sync for customer NL_122753",
pathFormatted: "req_animal-api-sync-08b09",
stats: {
    "name": "Animal-api sync for customer NL_122753",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "maxResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "meanResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles2": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles3": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles4": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-a5635": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_193658",
path: "Animal-api sync for customer BE_193658",
pathFormatted: "req_animal-api-sync-a5635",
stats: {
    "name": "Animal-api sync for customer BE_193658",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "56683",
        "ok": "56683",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "56683",
        "ok": "56683",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "56683",
        "ok": "56683",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "56683",
        "ok": "56683",
        "ko": "-"
    },
    "percentiles2": {
        "total": "56683",
        "ok": "56683",
        "ko": "-"
    },
    "percentiles3": {
        "total": "56683",
        "ok": "56683",
        "ko": "-"
    },
    "percentiles4": {
        "total": "56683",
        "ok": "56683",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-f89f8": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_121091",
path: "Animal-api sync for customer NL_121091",
pathFormatted: "req_animal-api-sync-f89f8",
stats: {
    "name": "Animal-api sync for customer NL_121091",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "46613",
        "ok": "46613",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "46613",
        "ok": "46613",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "46613",
        "ok": "46613",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "46613",
        "ok": "46613",
        "ko": "-"
    },
    "percentiles2": {
        "total": "46613",
        "ok": "46613",
        "ko": "-"
    },
    "percentiles3": {
        "total": "46613",
        "ok": "46613",
        "ko": "-"
    },
    "percentiles4": {
        "total": "46613",
        "ok": "46613",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-6c82d": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_143962",
path: "Animal-api sync for customer NL_143962",
pathFormatted: "req_animal-api-sync-6c82d",
stats: {
    "name": "Animal-api sync for customer NL_143962",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "51138",
        "ok": "51138",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "51138",
        "ok": "51138",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "51138",
        "ok": "51138",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "51138",
        "ok": "51138",
        "ko": "-"
    },
    "percentiles2": {
        "total": "51138",
        "ok": "51138",
        "ko": "-"
    },
    "percentiles3": {
        "total": "51138",
        "ok": "51138",
        "ko": "-"
    },
    "percentiles4": {
        "total": "51138",
        "ok": "51138",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-afe85": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104819",
path: "Animal-api sync for customer NL_104819",
pathFormatted: "req_animal-api-sync-afe85",
stats: {
    "name": "Animal-api sync for customer NL_104819",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "56283",
        "ok": "56283",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "56283",
        "ok": "56283",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "56283",
        "ok": "56283",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "56283",
        "ok": "56283",
        "ko": "-"
    },
    "percentiles2": {
        "total": "56283",
        "ok": "56283",
        "ko": "-"
    },
    "percentiles3": {
        "total": "56283",
        "ok": "56283",
        "ko": "-"
    },
    "percentiles4": {
        "total": "56283",
        "ok": "56283",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d9f9d": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_156439",
path: "Animal-api sync for customer BE_156439",
pathFormatted: "req_animal-api-sync-d9f9d",
stats: {
    "name": "Animal-api sync for customer BE_156439",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "55073",
        "ok": "55073",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "55073",
        "ok": "55073",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "55073",
        "ok": "55073",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "55073",
        "ok": "55073",
        "ko": "-"
    },
    "percentiles2": {
        "total": "55073",
        "ok": "55073",
        "ko": "-"
    },
    "percentiles3": {
        "total": "55073",
        "ok": "55073",
        "ko": "-"
    },
    "percentiles4": {
        "total": "55073",
        "ok": "55073",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-038e6": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117080",
path: "Animal-api sync for customer NL_117080",
pathFormatted: "req_animal-api-sync-038e6",
stats: {
    "name": "Animal-api sync for customer NL_117080",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "56273",
        "ok": "56273",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "56273",
        "ok": "56273",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "56273",
        "ok": "56273",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "56273",
        "ok": "56273",
        "ko": "-"
    },
    "percentiles2": {
        "total": "56273",
        "ok": "56273",
        "ko": "-"
    },
    "percentiles3": {
        "total": "56273",
        "ok": "56273",
        "ko": "-"
    },
    "percentiles4": {
        "total": "56273",
        "ok": "56273",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-9a2c1": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_142403",
path: "Animal-api sync for customer NL_142403",
pathFormatted: "req_animal-api-sync-9a2c1",
stats: {
    "name": "Animal-api sync for customer NL_142403",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "maxResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "meanResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles4": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-e1749": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_121066",
path: "Animal-api sync for customer NL_121066",
pathFormatted: "req_animal-api-sync-e1749",
stats: {
    "name": "Animal-api sync for customer NL_121066",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "50273",
        "ok": "50273",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "50273",
        "ok": "50273",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "50273",
        "ok": "50273",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "50273",
        "ok": "50273",
        "ko": "-"
    },
    "percentiles2": {
        "total": "50273",
        "ok": "50273",
        "ko": "-"
    },
    "percentiles3": {
        "total": "50273",
        "ok": "50273",
        "ko": "-"
    },
    "percentiles4": {
        "total": "50273",
        "ok": "50273",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c09d9": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_126693",
path: "Animal-api sync for customer NL_126693",
pathFormatted: "req_animal-api-sync-c09d9",
stats: {
    "name": "Animal-api sync for customer NL_126693",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "55372",
        "ok": "55372",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "55372",
        "ok": "55372",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "55372",
        "ok": "55372",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "55372",
        "ok": "55372",
        "ko": "-"
    },
    "percentiles2": {
        "total": "55372",
        "ok": "55372",
        "ko": "-"
    },
    "percentiles3": {
        "total": "55372",
        "ok": "55372",
        "ko": "-"
    },
    "percentiles4": {
        "total": "55372",
        "ok": "55372",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-cdc3c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_125737",
path: "Animal-api sync for customer NL_125737",
pathFormatted: "req_animal-api-sync-cdc3c",
stats: {
    "name": "Animal-api sync for customer NL_125737",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "55645",
        "ok": "55645",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "55645",
        "ok": "55645",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "55645",
        "ok": "55645",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "55645",
        "ok": "55645",
        "ko": "-"
    },
    "percentiles2": {
        "total": "55645",
        "ok": "55645",
        "ko": "-"
    },
    "percentiles3": {
        "total": "55645",
        "ok": "55645",
        "ko": "-"
    },
    "percentiles4": {
        "total": "55645",
        "ok": "55645",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-339e7": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_115945",
path: "Animal-api sync for customer NL_115945",
pathFormatted: "req_animal-api-sync-339e7",
stats: {
    "name": "Animal-api sync for customer NL_115945",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "39274",
        "ok": "39274",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "39274",
        "ok": "39274",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "39274",
        "ok": "39274",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "39274",
        "ok": "39274",
        "ko": "-"
    },
    "percentiles2": {
        "total": "39274",
        "ok": "39274",
        "ko": "-"
    },
    "percentiles3": {
        "total": "39274",
        "ok": "39274",
        "ko": "-"
    },
    "percentiles4": {
        "total": "39274",
        "ok": "39274",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c1773": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105858",
path: "Animal-api sync for customer NL_105858",
pathFormatted: "req_animal-api-sync-c1773",
stats: {
    "name": "Animal-api sync for customer NL_105858",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "57740",
        "ok": "57740",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "57740",
        "ok": "57740",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "57740",
        "ok": "57740",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "57740",
        "ok": "57740",
        "ko": "-"
    },
    "percentiles2": {
        "total": "57740",
        "ok": "57740",
        "ko": "-"
    },
    "percentiles3": {
        "total": "57740",
        "ok": "57740",
        "ko": "-"
    },
    "percentiles4": {
        "total": "57740",
        "ok": "57740",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c41a4": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_118516",
path: "Animal-api sync for customer NL_118516",
pathFormatted: "req_animal-api-sync-c41a4",
stats: {
    "name": "Animal-api sync for customer NL_118516",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "43313",
        "ok": "43313",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "43313",
        "ok": "43313",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "43313",
        "ok": "43313",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "43313",
        "ok": "43313",
        "ko": "-"
    },
    "percentiles2": {
        "total": "43313",
        "ok": "43313",
        "ko": "-"
    },
    "percentiles3": {
        "total": "43313",
        "ok": "43313",
        "ko": "-"
    },
    "percentiles4": {
        "total": "43313",
        "ok": "43313",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e8c3a": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_154725",
path: "Animal-api sync for customer BE_154725",
pathFormatted: "req_animal-api-sync-e8c3a",
stats: {
    "name": "Animal-api sync for customer BE_154725",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "45119",
        "ok": "45119",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "45119",
        "ok": "45119",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "45119",
        "ok": "45119",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "45119",
        "ok": "45119",
        "ko": "-"
    },
    "percentiles2": {
        "total": "45119",
        "ok": "45119",
        "ko": "-"
    },
    "percentiles3": {
        "total": "45119",
        "ok": "45119",
        "ko": "-"
    },
    "percentiles4": {
        "total": "45119",
        "ok": "45119",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-fc13a": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_154710",
path: "Animal-api sync for customer BE_154710",
pathFormatted: "req_animal-api-sync-fc13a",
stats: {
    "name": "Animal-api sync for customer BE_154710",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "46782",
        "ok": "46782",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "46782",
        "ok": "46782",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "46782",
        "ok": "46782",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "46782",
        "ok": "46782",
        "ko": "-"
    },
    "percentiles2": {
        "total": "46782",
        "ok": "46782",
        "ko": "-"
    },
    "percentiles3": {
        "total": "46782",
        "ok": "46782",
        "ko": "-"
    },
    "percentiles4": {
        "total": "46782",
        "ok": "46782",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d0d7d": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_147835",
path: "Animal-api sync for customer BE_147835",
pathFormatted: "req_animal-api-sync-d0d7d",
stats: {
    "name": "Animal-api sync for customer BE_147835",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "43733",
        "ok": "43733",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "43733",
        "ok": "43733",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "43733",
        "ok": "43733",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "43733",
        "ok": "43733",
        "ko": "-"
    },
    "percentiles2": {
        "total": "43733",
        "ok": "43733",
        "ko": "-"
    },
    "percentiles3": {
        "total": "43733",
        "ok": "43733",
        "ko": "-"
    },
    "percentiles4": {
        "total": "43733",
        "ok": "43733",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-f852c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_129269",
path: "Animal-api sync for customer NL_129269",
pathFormatted: "req_animal-api-sync-f852c",
stats: {
    "name": "Animal-api sync for customer NL_129269",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "46533",
        "ok": "46533",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "46533",
        "ok": "46533",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "46533",
        "ok": "46533",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "46533",
        "ok": "46533",
        "ko": "-"
    },
    "percentiles2": {
        "total": "46533",
        "ok": "46533",
        "ko": "-"
    },
    "percentiles3": {
        "total": "46533",
        "ok": "46533",
        "ko": "-"
    },
    "percentiles4": {
        "total": "46533",
        "ok": "46533",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
