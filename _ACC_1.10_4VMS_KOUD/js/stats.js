var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "495",
        "ok": "485",
        "ko": "10"
    },
    "minResponseTime": {
        "total": "1044",
        "ok": "1044",
        "ko": "60000"
    },
    "maxResponseTime": {
        "total": "60010",
        "ok": "57719",
        "ko": "60010"
    },
    "meanResponseTime": {
        "total": "12469",
        "ok": "11489",
        "ko": "60004"
    },
    "standardDeviation": {
        "total": "12340",
        "ok": "10385",
        "ko": "3"
    },
    "percentiles1": {
        "total": "8077",
        "ok": "7893",
        "ko": "60004"
    },
    "percentiles2": {
        "total": "15654",
        "ok": "14965",
        "ko": "60007"
    },
    "percentiles3": {
        "total": "40300",
        "ok": "33237",
        "ko": "60009"
    },
    "percentiles4": {
        "total": "60003",
        "ok": "48137",
        "ko": "60010"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 485,
    "percentage": 98
},
    "group4": {
    "name": "failed",
    "count": 10,
    "percentage": 2
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.216",
        "ok": "1.192",
        "ko": "0.025"
    }
},
contents: {
"req_animal-api-sync-9e54f": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_112168",
path: "Animal-api sync for customer NL_112168",
pathFormatted: "req_animal-api-sync-9e54f",
stats: {
    "name": "Animal-api sync for customer NL_112168",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "6398",
        "ok": "6398",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "6398",
        "ok": "6398",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "6398",
        "ok": "6398",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "6398",
        "ok": "6398",
        "ko": "-"
    },
    "percentiles2": {
        "total": "6398",
        "ok": "6398",
        "ko": "-"
    },
    "percentiles3": {
        "total": "6398",
        "ok": "6398",
        "ko": "-"
    },
    "percentiles4": {
        "total": "6398",
        "ok": "6398",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-f5a2a": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_125092",
path: "Animal-api sync for customer NL_125092",
pathFormatted: "req_animal-api-sync-f5a2a",
stats: {
    "name": "Animal-api sync for customer NL_125092",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "7244",
        "ok": "7244",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "7244",
        "ok": "7244",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "7244",
        "ok": "7244",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "7244",
        "ok": "7244",
        "ko": "-"
    },
    "percentiles2": {
        "total": "7244",
        "ok": "7244",
        "ko": "-"
    },
    "percentiles3": {
        "total": "7244",
        "ok": "7244",
        "ko": "-"
    },
    "percentiles4": {
        "total": "7244",
        "ok": "7244",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-0f1c3": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_128277",
path: "Animal-api sync for customer NL_128277",
pathFormatted: "req_animal-api-sync-0f1c3",
stats: {
    "name": "Animal-api sync for customer NL_128277",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "20583",
        "ok": "20583",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "20583",
        "ok": "20583",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "20583",
        "ok": "20583",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "20583",
        "ok": "20583",
        "ko": "-"
    },
    "percentiles2": {
        "total": "20583",
        "ok": "20583",
        "ko": "-"
    },
    "percentiles3": {
        "total": "20583",
        "ok": "20583",
        "ko": "-"
    },
    "percentiles4": {
        "total": "20583",
        "ok": "20583",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-1d77d": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_109531",
path: "Animal-api sync for customer NL_109531",
pathFormatted: "req_animal-api-sync-1d77d",
stats: {
    "name": "Animal-api sync for customer NL_109531",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5657",
        "ok": "5657",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5657",
        "ok": "5657",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5657",
        "ok": "5657",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5657",
        "ok": "5657",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5657",
        "ok": "5657",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5657",
        "ok": "5657",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5657",
        "ok": "5657",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e9ad2": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_121119",
path: "Animal-api sync for customer NL_121119",
pathFormatted: "req_animal-api-sync-e9ad2",
stats: {
    "name": "Animal-api sync for customer NL_121119",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "6296",
        "ok": "6296",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "6296",
        "ok": "6296",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "6296",
        "ok": "6296",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "6296",
        "ok": "6296",
        "ko": "-"
    },
    "percentiles2": {
        "total": "6296",
        "ok": "6296",
        "ko": "-"
    },
    "percentiles3": {
        "total": "6296",
        "ok": "6296",
        "ko": "-"
    },
    "percentiles4": {
        "total": "6296",
        "ok": "6296",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-86f14": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_121285",
path: "Animal-api sync for customer NL_121285",
pathFormatted: "req_animal-api-sync-86f14",
stats: {
    "name": "Animal-api sync for customer NL_121285",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "11082",
        "ok": "11082",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "11082",
        "ok": "11082",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "11082",
        "ok": "11082",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "11082",
        "ok": "11082",
        "ko": "-"
    },
    "percentiles2": {
        "total": "11082",
        "ok": "11082",
        "ko": "-"
    },
    "percentiles3": {
        "total": "11082",
        "ok": "11082",
        "ko": "-"
    },
    "percentiles4": {
        "total": "11082",
        "ok": "11082",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d5299": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104311",
path: "Animal-api sync for customer NL_104311",
pathFormatted: "req_animal-api-sync-d5299",
stats: {
    "name": "Animal-api sync for customer NL_104311",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4165",
        "ok": "4165",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4165",
        "ok": "4165",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4165",
        "ok": "4165",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4165",
        "ok": "4165",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4165",
        "ok": "4165",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4165",
        "ok": "4165",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4165",
        "ok": "4165",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-51e6e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117693",
path: "Animal-api sync for customer NL_117693",
pathFormatted: "req_animal-api-sync-51e6e",
stats: {
    "name": "Animal-api sync for customer NL_117693",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "8334",
        "ok": "8334",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "8334",
        "ok": "8334",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "8334",
        "ok": "8334",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "8334",
        "ok": "8334",
        "ko": "-"
    },
    "percentiles2": {
        "total": "8334",
        "ok": "8334",
        "ko": "-"
    },
    "percentiles3": {
        "total": "8334",
        "ok": "8334",
        "ko": "-"
    },
    "percentiles4": {
        "total": "8334",
        "ok": "8334",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-44547": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117813",
path: "Animal-api sync for customer NL_117813",
pathFormatted: "req_animal-api-sync-44547",
stats: {
    "name": "Animal-api sync for customer NL_117813",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "16574",
        "ok": "16574",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "16574",
        "ok": "16574",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "16574",
        "ok": "16574",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "16574",
        "ok": "16574",
        "ko": "-"
    },
    "percentiles2": {
        "total": "16574",
        "ok": "16574",
        "ko": "-"
    },
    "percentiles3": {
        "total": "16574",
        "ok": "16574",
        "ko": "-"
    },
    "percentiles4": {
        "total": "16574",
        "ok": "16574",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-76899": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_121301",
path: "Animal-api sync for customer NL_121301",
pathFormatted: "req_animal-api-sync-76899",
stats: {
    "name": "Animal-api sync for customer NL_121301",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21867",
        "ok": "21867",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "21867",
        "ok": "21867",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "21867",
        "ok": "21867",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "21867",
        "ok": "21867",
        "ko": "-"
    },
    "percentiles2": {
        "total": "21867",
        "ok": "21867",
        "ko": "-"
    },
    "percentiles3": {
        "total": "21867",
        "ok": "21867",
        "ko": "-"
    },
    "percentiles4": {
        "total": "21867",
        "ok": "21867",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-857a0": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_127307",
path: "Animal-api sync for customer NL_127307",
pathFormatted: "req_animal-api-sync-857a0",
stats: {
    "name": "Animal-api sync for customer NL_127307",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1745",
        "ok": "1745",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1745",
        "ok": "1745",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1745",
        "ok": "1745",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1745",
        "ok": "1745",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1745",
        "ok": "1745",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1745",
        "ok": "1745",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1745",
        "ok": "1745",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-8b245": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_120606",
path: "Animal-api sync for customer NL_120606",
pathFormatted: "req_animal-api-sync-8b245",
stats: {
    "name": "Animal-api sync for customer NL_120606",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "7305",
        "ok": "7305",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "7305",
        "ok": "7305",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "7305",
        "ok": "7305",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "7305",
        "ok": "7305",
        "ko": "-"
    },
    "percentiles2": {
        "total": "7305",
        "ok": "7305",
        "ko": "-"
    },
    "percentiles3": {
        "total": "7305",
        "ok": "7305",
        "ko": "-"
    },
    "percentiles4": {
        "total": "7305",
        "ok": "7305",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-1ba07": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105320",
path: "Animal-api sync for customer NL_105320",
pathFormatted: "req_animal-api-sync-1ba07",
stats: {
    "name": "Animal-api sync for customer NL_105320",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3016",
        "ok": "3016",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3016",
        "ok": "3016",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3016",
        "ok": "3016",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3016",
        "ok": "3016",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3016",
        "ok": "3016",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3016",
        "ok": "3016",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3016",
        "ok": "3016",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-07d8c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_115607",
path: "Animal-api sync for customer NL_115607",
pathFormatted: "req_animal-api-sync-07d8c",
stats: {
    "name": "Animal-api sync for customer NL_115607",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "7723",
        "ok": "7723",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "7723",
        "ok": "7723",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "7723",
        "ok": "7723",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "7723",
        "ok": "7723",
        "ko": "-"
    },
    "percentiles2": {
        "total": "7723",
        "ok": "7723",
        "ko": "-"
    },
    "percentiles3": {
        "total": "7723",
        "ok": "7723",
        "ko": "-"
    },
    "percentiles4": {
        "total": "7723",
        "ok": "7723",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d643b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_129420",
path: "Animal-api sync for customer NL_129420",
pathFormatted: "req_animal-api-sync-d643b",
stats: {
    "name": "Animal-api sync for customer NL_129420",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1140",
        "ok": "1140",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1140",
        "ok": "1140",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1140",
        "ok": "1140",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1140",
        "ok": "1140",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1140",
        "ok": "1140",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1140",
        "ok": "1140",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1140",
        "ok": "1140",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-5b7d7": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_113415",
path: "Animal-api sync for customer NL_113415",
pathFormatted: "req_animal-api-sync-5b7d7",
stats: {
    "name": "Animal-api sync for customer NL_113415",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5821",
        "ok": "5821",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5821",
        "ok": "5821",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5821",
        "ok": "5821",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5821",
        "ok": "5821",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5821",
        "ok": "5821",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5821",
        "ok": "5821",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5821",
        "ok": "5821",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-05129": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117717",
path: "Animal-api sync for customer NL_117717",
pathFormatted: "req_animal-api-sync-05129",
stats: {
    "name": "Animal-api sync for customer NL_117717",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3512",
        "ok": "3512",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3512",
        "ok": "3512",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3512",
        "ok": "3512",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3512",
        "ok": "3512",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3512",
        "ok": "3512",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3512",
        "ok": "3512",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3512",
        "ok": "3512",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a175e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_207835",
path: "Animal-api sync for customer NL_207835",
pathFormatted: "req_animal-api-sync-a175e",
stats: {
    "name": "Animal-api sync for customer NL_207835",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1507",
        "ok": "1507",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1507",
        "ok": "1507",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1507",
        "ok": "1507",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1507",
        "ok": "1507",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1507",
        "ok": "1507",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1507",
        "ok": "1507",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1507",
        "ok": "1507",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-3bf26": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_108699",
path: "Animal-api sync for customer NL_108699",
pathFormatted: "req_animal-api-sync-3bf26",
stats: {
    "name": "Animal-api sync for customer NL_108699",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1044",
        "ok": "1044",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1044",
        "ok": "1044",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1044",
        "ok": "1044",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1044",
        "ok": "1044",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1044",
        "ok": "1044",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1044",
        "ok": "1044",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1044",
        "ok": "1044",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-2b97a": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_161984",
path: "Animal-api sync for customer NL_161984",
pathFormatted: "req_animal-api-sync-2b97a",
stats: {
    "name": "Animal-api sync for customer NL_161984",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4074",
        "ok": "4074",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4074",
        "ok": "4074",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4074",
        "ok": "4074",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4074",
        "ok": "4074",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4074",
        "ok": "4074",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4074",
        "ok": "4074",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4074",
        "ok": "4074",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-5243f": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111939",
path: "Animal-api sync for customer NL_111939",
pathFormatted: "req_animal-api-sync-5243f",
stats: {
    "name": "Animal-api sync for customer NL_111939",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "26694",
        "ok": "26694",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "26694",
        "ok": "26694",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "26694",
        "ok": "26694",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "26694",
        "ok": "26694",
        "ko": "-"
    },
    "percentiles2": {
        "total": "26694",
        "ok": "26694",
        "ko": "-"
    },
    "percentiles3": {
        "total": "26694",
        "ok": "26694",
        "ko": "-"
    },
    "percentiles4": {
        "total": "26694",
        "ok": "26694",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b6219": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_144583",
path: "Animal-api sync for customer NL_144583",
pathFormatted: "req_animal-api-sync-b6219",
stats: {
    "name": "Animal-api sync for customer NL_144583",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "7783",
        "ok": "7783",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "7783",
        "ok": "7783",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "7783",
        "ok": "7783",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "7783",
        "ok": "7783",
        "ko": "-"
    },
    "percentiles2": {
        "total": "7783",
        "ok": "7783",
        "ko": "-"
    },
    "percentiles3": {
        "total": "7783",
        "ok": "7783",
        "ko": "-"
    },
    "percentiles4": {
        "total": "7783",
        "ok": "7783",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-43dcc": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_112405",
path: "Animal-api sync for customer NL_112405",
pathFormatted: "req_animal-api-sync-43dcc",
stats: {
    "name": "Animal-api sync for customer NL_112405",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "13821",
        "ok": "13821",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "13821",
        "ok": "13821",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "13821",
        "ok": "13821",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "13821",
        "ok": "13821",
        "ko": "-"
    },
    "percentiles2": {
        "total": "13821",
        "ok": "13821",
        "ko": "-"
    },
    "percentiles3": {
        "total": "13821",
        "ok": "13821",
        "ko": "-"
    },
    "percentiles4": {
        "total": "13821",
        "ok": "13821",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d545c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_131064",
path: "Animal-api sync for customer NL_131064",
pathFormatted: "req_animal-api-sync-d545c",
stats: {
    "name": "Animal-api sync for customer NL_131064",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4650",
        "ok": "4650",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4650",
        "ok": "4650",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4650",
        "ok": "4650",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4650",
        "ok": "4650",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4650",
        "ok": "4650",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4650",
        "ok": "4650",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4650",
        "ok": "4650",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-93205": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_120176",
path: "Animal-api sync for customer NL_120176",
pathFormatted: "req_animal-api-sync-93205",
stats: {
    "name": "Animal-api sync for customer NL_120176",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5753",
        "ok": "5753",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5753",
        "ok": "5753",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5753",
        "ok": "5753",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5753",
        "ok": "5753",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5753",
        "ok": "5753",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5753",
        "ok": "5753",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5753",
        "ok": "5753",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-bf978": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_118830",
path: "Animal-api sync for customer NL_118830",
pathFormatted: "req_animal-api-sync-bf978",
stats: {
    "name": "Animal-api sync for customer NL_118830",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4443",
        "ok": "4443",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4443",
        "ok": "4443",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4443",
        "ok": "4443",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4443",
        "ok": "4443",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4443",
        "ok": "4443",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4443",
        "ok": "4443",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4443",
        "ok": "4443",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-28852": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_116904",
path: "Animal-api sync for customer NL_116904",
pathFormatted: "req_animal-api-sync-28852",
stats: {
    "name": "Animal-api sync for customer NL_116904",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "22420",
        "ok": "22420",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "22420",
        "ok": "22420",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "22420",
        "ok": "22420",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "22420",
        "ok": "22420",
        "ko": "-"
    },
    "percentiles2": {
        "total": "22420",
        "ok": "22420",
        "ko": "-"
    },
    "percentiles3": {
        "total": "22420",
        "ok": "22420",
        "ko": "-"
    },
    "percentiles4": {
        "total": "22420",
        "ok": "22420",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-3566a": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_107884",
path: "Animal-api sync for customer NL_107884",
pathFormatted: "req_animal-api-sync-3566a",
stats: {
    "name": "Animal-api sync for customer NL_107884",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "14267",
        "ok": "14267",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "14267",
        "ok": "14267",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "14267",
        "ok": "14267",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "14267",
        "ok": "14267",
        "ko": "-"
    },
    "percentiles2": {
        "total": "14267",
        "ok": "14267",
        "ko": "-"
    },
    "percentiles3": {
        "total": "14267",
        "ok": "14267",
        "ko": "-"
    },
    "percentiles4": {
        "total": "14267",
        "ok": "14267",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c24b5": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_122429",
path: "Animal-api sync for customer NL_122429",
pathFormatted: "req_animal-api-sync-c24b5",
stats: {
    "name": "Animal-api sync for customer NL_122429",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "15016",
        "ok": "15016",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "15016",
        "ok": "15016",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "15016",
        "ok": "15016",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "15016",
        "ok": "15016",
        "ko": "-"
    },
    "percentiles2": {
        "total": "15016",
        "ok": "15016",
        "ko": "-"
    },
    "percentiles3": {
        "total": "15016",
        "ok": "15016",
        "ko": "-"
    },
    "percentiles4": {
        "total": "15016",
        "ok": "15016",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-31f54": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_122305",
path: "Animal-api sync for customer NL_122305",
pathFormatted: "req_animal-api-sync-31f54",
stats: {
    "name": "Animal-api sync for customer NL_122305",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5894",
        "ok": "5894",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5894",
        "ok": "5894",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5894",
        "ok": "5894",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5894",
        "ok": "5894",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5894",
        "ok": "5894",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5894",
        "ok": "5894",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5894",
        "ok": "5894",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-8ebc3": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_212864",
path: "Animal-api sync for customer BE_212864",
pathFormatted: "req_animal-api-sync-8ebc3",
stats: {
    "name": "Animal-api sync for customer BE_212864",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "8623",
        "ok": "8623",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "8623",
        "ok": "8623",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "8623",
        "ok": "8623",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "8623",
        "ok": "8623",
        "ko": "-"
    },
    "percentiles2": {
        "total": "8623",
        "ok": "8623",
        "ko": "-"
    },
    "percentiles3": {
        "total": "8623",
        "ok": "8623",
        "ko": "-"
    },
    "percentiles4": {
        "total": "8623",
        "ok": "8623",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a5160": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_156494",
path: "Animal-api sync for customer NL_156494",
pathFormatted: "req_animal-api-sync-a5160",
stats: {
    "name": "Animal-api sync for customer NL_156494",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "19605",
        "ok": "19605",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "19605",
        "ok": "19605",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "19605",
        "ok": "19605",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "19605",
        "ok": "19605",
        "ko": "-"
    },
    "percentiles2": {
        "total": "19605",
        "ok": "19605",
        "ko": "-"
    },
    "percentiles3": {
        "total": "19605",
        "ok": "19605",
        "ko": "-"
    },
    "percentiles4": {
        "total": "19605",
        "ok": "19605",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-34ba1": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_165365",
path: "Animal-api sync for customer BE_165365",
pathFormatted: "req_animal-api-sync-34ba1",
stats: {
    "name": "Animal-api sync for customer BE_165365",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5958",
        "ok": "5958",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5958",
        "ok": "5958",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5958",
        "ok": "5958",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5958",
        "ok": "5958",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5958",
        "ok": "5958",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5958",
        "ok": "5958",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5958",
        "ok": "5958",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-9ec36": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_124747",
path: "Animal-api sync for customer NL_124747",
pathFormatted: "req_animal-api-sync-9ec36",
stats: {
    "name": "Animal-api sync for customer NL_124747",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5948",
        "ok": "5948",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5948",
        "ok": "5948",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5948",
        "ok": "5948",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5948",
        "ok": "5948",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5948",
        "ok": "5948",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5948",
        "ok": "5948",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5948",
        "ok": "5948",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b9862": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_161770",
path: "Animal-api sync for customer NL_161770",
pathFormatted: "req_animal-api-sync-b9862",
stats: {
    "name": "Animal-api sync for customer NL_161770",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "24229",
        "ok": "24229",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "24229",
        "ok": "24229",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "24229",
        "ok": "24229",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "24229",
        "ok": "24229",
        "ko": "-"
    },
    "percentiles2": {
        "total": "24229",
        "ok": "24229",
        "ko": "-"
    },
    "percentiles3": {
        "total": "24229",
        "ok": "24229",
        "ko": "-"
    },
    "percentiles4": {
        "total": "24229",
        "ok": "24229",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b2272": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105223",
path: "Animal-api sync for customer NL_105223",
pathFormatted: "req_animal-api-sync-b2272",
stats: {
    "name": "Animal-api sync for customer NL_105223",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5899",
        "ok": "5899",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5899",
        "ok": "5899",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5899",
        "ok": "5899",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5899",
        "ok": "5899",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5899",
        "ok": "5899",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5899",
        "ok": "5899",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5899",
        "ok": "5899",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-49a96": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_128642",
path: "Animal-api sync for customer NL_128642",
pathFormatted: "req_animal-api-sync-49a96",
stats: {
    "name": "Animal-api sync for customer NL_128642",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "11731",
        "ok": "11731",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "11731",
        "ok": "11731",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "11731",
        "ok": "11731",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "11731",
        "ok": "11731",
        "ko": "-"
    },
    "percentiles2": {
        "total": "11731",
        "ok": "11731",
        "ko": "-"
    },
    "percentiles3": {
        "total": "11731",
        "ok": "11731",
        "ko": "-"
    },
    "percentiles4": {
        "total": "11731",
        "ok": "11731",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-5c4eb": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_123093",
path: "Animal-api sync for customer NL_123093",
pathFormatted: "req_animal-api-sync-5c4eb",
stats: {
    "name": "Animal-api sync for customer NL_123093",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2018",
        "ok": "2018",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2018",
        "ok": "2018",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2018",
        "ok": "2018",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2018",
        "ok": "2018",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2018",
        "ok": "2018",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2018",
        "ok": "2018",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2018",
        "ok": "2018",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-90ee6": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105389",
path: "Animal-api sync for customer NL_105389",
pathFormatted: "req_animal-api-sync-90ee6",
stats: {
    "name": "Animal-api sync for customer NL_105389",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "11926",
        "ok": "11926",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "11926",
        "ok": "11926",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "11926",
        "ok": "11926",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "11926",
        "ok": "11926",
        "ko": "-"
    },
    "percentiles2": {
        "total": "11926",
        "ok": "11926",
        "ko": "-"
    },
    "percentiles3": {
        "total": "11926",
        "ok": "11926",
        "ko": "-"
    },
    "percentiles4": {
        "total": "11926",
        "ok": "11926",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-0908d": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105707",
path: "Animal-api sync for customer NL_105707",
pathFormatted: "req_animal-api-sync-0908d",
stats: {
    "name": "Animal-api sync for customer NL_105707",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4145",
        "ok": "4145",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4145",
        "ok": "4145",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4145",
        "ok": "4145",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4145",
        "ok": "4145",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4145",
        "ok": "4145",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4145",
        "ok": "4145",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4145",
        "ok": "4145",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-0a82c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_124910",
path: "Animal-api sync for customer NL_124910",
pathFormatted: "req_animal-api-sync-0a82c",
stats: {
    "name": "Animal-api sync for customer NL_124910",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1451",
        "ok": "1451",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1451",
        "ok": "1451",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1451",
        "ok": "1451",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1451",
        "ok": "1451",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1451",
        "ok": "1451",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1451",
        "ok": "1451",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1451",
        "ok": "1451",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d54bb": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_108024",
path: "Animal-api sync for customer NL_108024",
pathFormatted: "req_animal-api-sync-d54bb",
stats: {
    "name": "Animal-api sync for customer NL_108024",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "24766",
        "ok": "24766",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "24766",
        "ok": "24766",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "24766",
        "ok": "24766",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "24766",
        "ok": "24766",
        "ko": "-"
    },
    "percentiles2": {
        "total": "24766",
        "ok": "24766",
        "ko": "-"
    },
    "percentiles3": {
        "total": "24766",
        "ok": "24766",
        "ko": "-"
    },
    "percentiles4": {
        "total": "24766",
        "ok": "24766",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c0ea2": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_126187",
path: "Animal-api sync for customer NL_126187",
pathFormatted: "req_animal-api-sync-c0ea2",
stats: {
    "name": "Animal-api sync for customer NL_126187",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2791",
        "ok": "2791",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2791",
        "ok": "2791",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2791",
        "ok": "2791",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2791",
        "ok": "2791",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2791",
        "ok": "2791",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2791",
        "ok": "2791",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2791",
        "ok": "2791",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-7a1f1": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_151229",
path: "Animal-api sync for customer BE_151229",
pathFormatted: "req_animal-api-sync-7a1f1",
stats: {
    "name": "Animal-api sync for customer BE_151229",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1282",
        "ok": "1282",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1282",
        "ok": "1282",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1282",
        "ok": "1282",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1282",
        "ok": "1282",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1282",
        "ok": "1282",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1282",
        "ok": "1282",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1282",
        "ok": "1282",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-72904": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_103783",
path: "Animal-api sync for customer NL_103783",
pathFormatted: "req_animal-api-sync-72904",
stats: {
    "name": "Animal-api sync for customer NL_103783",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21245",
        "ok": "21245",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "21245",
        "ok": "21245",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "21245",
        "ok": "21245",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "21245",
        "ok": "21245",
        "ko": "-"
    },
    "percentiles2": {
        "total": "21245",
        "ok": "21245",
        "ko": "-"
    },
    "percentiles3": {
        "total": "21245",
        "ok": "21245",
        "ko": "-"
    },
    "percentiles4": {
        "total": "21245",
        "ok": "21245",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-54314": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_103881",
path: "Animal-api sync for customer NL_103881",
pathFormatted: "req_animal-api-sync-54314",
stats: {
    "name": "Animal-api sync for customer NL_103881",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "8986",
        "ok": "8986",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "8986",
        "ok": "8986",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "8986",
        "ok": "8986",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "8986",
        "ok": "8986",
        "ko": "-"
    },
    "percentiles2": {
        "total": "8986",
        "ok": "8986",
        "ko": "-"
    },
    "percentiles3": {
        "total": "8986",
        "ok": "8986",
        "ko": "-"
    },
    "percentiles4": {
        "total": "8986",
        "ok": "8986",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-fb3ec": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_115893",
path: "Animal-api sync for customer NL_115893",
pathFormatted: "req_animal-api-sync-fb3ec",
stats: {
    "name": "Animal-api sync for customer NL_115893",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "9249",
        "ok": "9249",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "9249",
        "ok": "9249",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "9249",
        "ok": "9249",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "9249",
        "ok": "9249",
        "ko": "-"
    },
    "percentiles2": {
        "total": "9249",
        "ok": "9249",
        "ko": "-"
    },
    "percentiles3": {
        "total": "9249",
        "ok": "9249",
        "ko": "-"
    },
    "percentiles4": {
        "total": "9249",
        "ok": "9249",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d90a9": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_119649",
path: "Animal-api sync for customer NL_119649",
pathFormatted: "req_animal-api-sync-d90a9",
stats: {
    "name": "Animal-api sync for customer NL_119649",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2610",
        "ok": "2610",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2610",
        "ok": "2610",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2610",
        "ok": "2610",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2610",
        "ok": "2610",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2610",
        "ok": "2610",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2610",
        "ok": "2610",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2610",
        "ok": "2610",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c4ec4": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104910",
path: "Animal-api sync for customer NL_104910",
pathFormatted: "req_animal-api-sync-c4ec4",
stats: {
    "name": "Animal-api sync for customer NL_104910",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3674",
        "ok": "3674",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3674",
        "ok": "3674",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3674",
        "ok": "3674",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3674",
        "ok": "3674",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3674",
        "ok": "3674",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3674",
        "ok": "3674",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3674",
        "ok": "3674",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e498b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110382",
path: "Animal-api sync for customer NL_110382",
pathFormatted: "req_animal-api-sync-e498b",
stats: {
    "name": "Animal-api sync for customer NL_110382",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5326",
        "ok": "5326",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5326",
        "ok": "5326",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5326",
        "ok": "5326",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5326",
        "ok": "5326",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5326",
        "ok": "5326",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5326",
        "ok": "5326",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5326",
        "ok": "5326",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-2a4dd": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_125297",
path: "Animal-api sync for customer NL_125297",
pathFormatted: "req_animal-api-sync-2a4dd",
stats: {
    "name": "Animal-api sync for customer NL_125297",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3866",
        "ok": "3866",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3866",
        "ok": "3866",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3866",
        "ok": "3866",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3866",
        "ok": "3866",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3866",
        "ok": "3866",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3866",
        "ok": "3866",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3866",
        "ok": "3866",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c1c6a": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_135207",
path: "Animal-api sync for customer NL_135207",
pathFormatted: "req_animal-api-sync-c1c6a",
stats: {
    "name": "Animal-api sync for customer NL_135207",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3789",
        "ok": "3789",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3789",
        "ok": "3789",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3789",
        "ok": "3789",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3789",
        "ok": "3789",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3789",
        "ok": "3789",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3789",
        "ok": "3789",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3789",
        "ok": "3789",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c170f": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_109428",
path: "Animal-api sync for customer NL_109428",
pathFormatted: "req_animal-api-sync-c170f",
stats: {
    "name": "Animal-api sync for customer NL_109428",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "13248",
        "ok": "13248",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "13248",
        "ok": "13248",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "13248",
        "ok": "13248",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "13248",
        "ok": "13248",
        "ko": "-"
    },
    "percentiles2": {
        "total": "13248",
        "ok": "13248",
        "ko": "-"
    },
    "percentiles3": {
        "total": "13248",
        "ok": "13248",
        "ko": "-"
    },
    "percentiles4": {
        "total": "13248",
        "ok": "13248",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-690f7": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_129681",
path: "Animal-api sync for customer NL_129681",
pathFormatted: "req_animal-api-sync-690f7",
stats: {
    "name": "Animal-api sync for customer NL_129681",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2811",
        "ok": "2811",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2811",
        "ok": "2811",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2811",
        "ok": "2811",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2811",
        "ok": "2811",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2811",
        "ok": "2811",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2811",
        "ok": "2811",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2811",
        "ok": "2811",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-de0b1": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_164131",
path: "Animal-api sync for customer NL_164131",
pathFormatted: "req_animal-api-sync-de0b1",
stats: {
    "name": "Animal-api sync for customer NL_164131",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "14741",
        "ok": "14741",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "14741",
        "ok": "14741",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "14741",
        "ok": "14741",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "14741",
        "ok": "14741",
        "ko": "-"
    },
    "percentiles2": {
        "total": "14741",
        "ok": "14741",
        "ko": "-"
    },
    "percentiles3": {
        "total": "14741",
        "ok": "14741",
        "ko": "-"
    },
    "percentiles4": {
        "total": "14741",
        "ok": "14741",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-3e31b": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_149431",
path: "Animal-api sync for customer BE_149431",
pathFormatted: "req_animal-api-sync-3e31b",
stats: {
    "name": "Animal-api sync for customer BE_149431",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2330",
        "ok": "2330",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2330",
        "ok": "2330",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2330",
        "ok": "2330",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2330",
        "ok": "2330",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2330",
        "ok": "2330",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2330",
        "ok": "2330",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2330",
        "ok": "2330",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-cb71c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_120709",
path: "Animal-api sync for customer NL_120709",
pathFormatted: "req_animal-api-sync-cb71c",
stats: {
    "name": "Animal-api sync for customer NL_120709",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "20547",
        "ok": "20547",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "20547",
        "ok": "20547",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "20547",
        "ok": "20547",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "20547",
        "ok": "20547",
        "ko": "-"
    },
    "percentiles2": {
        "total": "20547",
        "ok": "20547",
        "ko": "-"
    },
    "percentiles3": {
        "total": "20547",
        "ok": "20547",
        "ko": "-"
    },
    "percentiles4": {
        "total": "20547",
        "ok": "20547",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-5a7e6": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105010",
path: "Animal-api sync for customer NL_105010",
pathFormatted: "req_animal-api-sync-5a7e6",
stats: {
    "name": "Animal-api sync for customer NL_105010",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "7948",
        "ok": "7948",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "7948",
        "ok": "7948",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "7948",
        "ok": "7948",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "7948",
        "ok": "7948",
        "ko": "-"
    },
    "percentiles2": {
        "total": "7948",
        "ok": "7948",
        "ko": "-"
    },
    "percentiles3": {
        "total": "7948",
        "ok": "7948",
        "ko": "-"
    },
    "percentiles4": {
        "total": "7948",
        "ok": "7948",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-0eddf": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110820",
path: "Animal-api sync for customer NL_110820",
pathFormatted: "req_animal-api-sync-0eddf",
stats: {
    "name": "Animal-api sync for customer NL_110820",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "6984",
        "ok": "6984",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "6984",
        "ok": "6984",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "6984",
        "ok": "6984",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "6984",
        "ok": "6984",
        "ko": "-"
    },
    "percentiles2": {
        "total": "6984",
        "ok": "6984",
        "ko": "-"
    },
    "percentiles3": {
        "total": "6984",
        "ok": "6984",
        "ko": "-"
    },
    "percentiles4": {
        "total": "6984",
        "ok": "6984",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-558a2": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_116270",
path: "Animal-api sync for customer NL_116270",
pathFormatted: "req_animal-api-sync-558a2",
stats: {
    "name": "Animal-api sync for customer NL_116270",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4036",
        "ok": "4036",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4036",
        "ok": "4036",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4036",
        "ok": "4036",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4036",
        "ok": "4036",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4036",
        "ok": "4036",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4036",
        "ok": "4036",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4036",
        "ok": "4036",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-3833c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_124824",
path: "Animal-api sync for customer NL_124824",
pathFormatted: "req_animal-api-sync-3833c",
stats: {
    "name": "Animal-api sync for customer NL_124824",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2208",
        "ok": "2208",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2208",
        "ok": "2208",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2208",
        "ok": "2208",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2208",
        "ok": "2208",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2208",
        "ok": "2208",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2208",
        "ok": "2208",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2208",
        "ok": "2208",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a3967": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117770",
path: "Animal-api sync for customer NL_117770",
pathFormatted: "req_animal-api-sync-a3967",
stats: {
    "name": "Animal-api sync for customer NL_117770",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4773",
        "ok": "4773",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4773",
        "ok": "4773",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4773",
        "ok": "4773",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4773",
        "ok": "4773",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4773",
        "ok": "4773",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4773",
        "ok": "4773",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4773",
        "ok": "4773",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-5a613": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_194583",
path: "Animal-api sync for customer NL_194583",
pathFormatted: "req_animal-api-sync-5a613",
stats: {
    "name": "Animal-api sync for customer NL_194583",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5186",
        "ok": "5186",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5186",
        "ok": "5186",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5186",
        "ok": "5186",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5186",
        "ok": "5186",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5186",
        "ok": "5186",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5186",
        "ok": "5186",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5186",
        "ok": "5186",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c7c5d": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_129379",
path: "Animal-api sync for customer NL_129379",
pathFormatted: "req_animal-api-sync-c7c5d",
stats: {
    "name": "Animal-api sync for customer NL_129379",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4226",
        "ok": "4226",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4226",
        "ok": "4226",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4226",
        "ok": "4226",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4226",
        "ok": "4226",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4226",
        "ok": "4226",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4226",
        "ok": "4226",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4226",
        "ok": "4226",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-91ebb": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_120778",
path: "Animal-api sync for customer NL_120778",
pathFormatted: "req_animal-api-sync-91ebb",
stats: {
    "name": "Animal-api sync for customer NL_120778",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2354",
        "ok": "2354",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2354",
        "ok": "2354",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2354",
        "ok": "2354",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2354",
        "ok": "2354",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2354",
        "ok": "2354",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2354",
        "ok": "2354",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2354",
        "ok": "2354",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-6d45c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_123012",
path: "Animal-api sync for customer NL_123012",
pathFormatted: "req_animal-api-sync-6d45c",
stats: {
    "name": "Animal-api sync for customer NL_123012",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2258",
        "ok": "2258",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2258",
        "ok": "2258",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2258",
        "ok": "2258",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2258",
        "ok": "2258",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2258",
        "ok": "2258",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2258",
        "ok": "2258",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2258",
        "ok": "2258",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-3ff93": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111287",
path: "Animal-api sync for customer NL_111287",
pathFormatted: "req_animal-api-sync-3ff93",
stats: {
    "name": "Animal-api sync for customer NL_111287",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "7515",
        "ok": "7515",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "7515",
        "ok": "7515",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "7515",
        "ok": "7515",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "7515",
        "ok": "7515",
        "ko": "-"
    },
    "percentiles2": {
        "total": "7515",
        "ok": "7515",
        "ko": "-"
    },
    "percentiles3": {
        "total": "7515",
        "ok": "7515",
        "ko": "-"
    },
    "percentiles4": {
        "total": "7515",
        "ok": "7515",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-968ea": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_152278",
path: "Animal-api sync for customer BE_152278",
pathFormatted: "req_animal-api-sync-968ea",
stats: {
    "name": "Animal-api sync for customer BE_152278",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3099",
        "ok": "3099",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3099",
        "ok": "3099",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3099",
        "ok": "3099",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3099",
        "ok": "3099",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3099",
        "ok": "3099",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3099",
        "ok": "3099",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3099",
        "ok": "3099",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b979d": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_134839",
path: "Animal-api sync for customer NL_134839",
pathFormatted: "req_animal-api-sync-b979d",
stats: {
    "name": "Animal-api sync for customer NL_134839",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3026",
        "ok": "3026",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3026",
        "ok": "3026",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3026",
        "ok": "3026",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3026",
        "ok": "3026",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3026",
        "ok": "3026",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3026",
        "ok": "3026",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3026",
        "ok": "3026",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-4a2f0": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_119479",
path: "Animal-api sync for customer NL_119479",
pathFormatted: "req_animal-api-sync-4a2f0",
stats: {
    "name": "Animal-api sync for customer NL_119479",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3788",
        "ok": "3788",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3788",
        "ok": "3788",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3788",
        "ok": "3788",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3788",
        "ok": "3788",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3788",
        "ok": "3788",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3788",
        "ok": "3788",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3788",
        "ok": "3788",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-04f19": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_142633",
path: "Animal-api sync for customer NL_142633",
pathFormatted: "req_animal-api-sync-04f19",
stats: {
    "name": "Animal-api sync for customer NL_142633",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "19488",
        "ok": "19488",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "19488",
        "ok": "19488",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "19488",
        "ok": "19488",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "19488",
        "ok": "19488",
        "ko": "-"
    },
    "percentiles2": {
        "total": "19488",
        "ok": "19488",
        "ko": "-"
    },
    "percentiles3": {
        "total": "19488",
        "ok": "19488",
        "ko": "-"
    },
    "percentiles4": {
        "total": "19488",
        "ok": "19488",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c6934": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_144844",
path: "Animal-api sync for customer BE_144844",
pathFormatted: "req_animal-api-sync-c6934",
stats: {
    "name": "Animal-api sync for customer BE_144844",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4558",
        "ok": "4558",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4558",
        "ok": "4558",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4558",
        "ok": "4558",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4558",
        "ok": "4558",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4558",
        "ok": "4558",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4558",
        "ok": "4558",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4558",
        "ok": "4558",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-ee05d": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_106934",
path: "Animal-api sync for customer NL_106934",
pathFormatted: "req_animal-api-sync-ee05d",
stats: {
    "name": "Animal-api sync for customer NL_106934",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5402",
        "ok": "5402",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5402",
        "ok": "5402",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5402",
        "ok": "5402",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5402",
        "ok": "5402",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5402",
        "ok": "5402",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5402",
        "ok": "5402",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5402",
        "ok": "5402",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-26bf9": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110058",
path: "Animal-api sync for customer NL_110058",
pathFormatted: "req_animal-api-sync-26bf9",
stats: {
    "name": "Animal-api sync for customer NL_110058",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4422",
        "ok": "4422",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4422",
        "ok": "4422",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4422",
        "ok": "4422",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4422",
        "ok": "4422",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4422",
        "ok": "4422",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4422",
        "ok": "4422",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4422",
        "ok": "4422",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a42d0": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105360",
path: "Animal-api sync for customer NL_105360",
pathFormatted: "req_animal-api-sync-a42d0",
stats: {
    "name": "Animal-api sync for customer NL_105360",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "12692",
        "ok": "12692",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "12692",
        "ok": "12692",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "12692",
        "ok": "12692",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "12692",
        "ok": "12692",
        "ko": "-"
    },
    "percentiles2": {
        "total": "12692",
        "ok": "12692",
        "ko": "-"
    },
    "percentiles3": {
        "total": "12692",
        "ok": "12692",
        "ko": "-"
    },
    "percentiles4": {
        "total": "12692",
        "ok": "12692",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-35753": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_118795",
path: "Animal-api sync for customer NL_118795",
pathFormatted: "req_animal-api-sync-35753",
stats: {
    "name": "Animal-api sync for customer NL_118795",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5410",
        "ok": "5410",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5410",
        "ok": "5410",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5410",
        "ok": "5410",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5410",
        "ok": "5410",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5410",
        "ok": "5410",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5410",
        "ok": "5410",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5410",
        "ok": "5410",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-09a8d": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117444",
path: "Animal-api sync for customer NL_117444",
pathFormatted: "req_animal-api-sync-09a8d",
stats: {
    "name": "Animal-api sync for customer NL_117444",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2781",
        "ok": "2781",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2781",
        "ok": "2781",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2781",
        "ok": "2781",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2781",
        "ok": "2781",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2781",
        "ok": "2781",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2781",
        "ok": "2781",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2781",
        "ok": "2781",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-7c5cb": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_118794",
path: "Animal-api sync for customer NL_118794",
pathFormatted: "req_animal-api-sync-7c5cb",
stats: {
    "name": "Animal-api sync for customer NL_118794",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3019",
        "ok": "3019",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3019",
        "ok": "3019",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3019",
        "ok": "3019",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3019",
        "ok": "3019",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3019",
        "ok": "3019",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3019",
        "ok": "3019",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3019",
        "ok": "3019",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e202d": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_112066",
path: "Animal-api sync for customer NL_112066",
pathFormatted: "req_animal-api-sync-e202d",
stats: {
    "name": "Animal-api sync for customer NL_112066",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4477",
        "ok": "4477",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4477",
        "ok": "4477",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4477",
        "ok": "4477",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4477",
        "ok": "4477",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4477",
        "ok": "4477",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4477",
        "ok": "4477",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4477",
        "ok": "4477",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-be37d": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_106236",
path: "Animal-api sync for customer NL_106236",
pathFormatted: "req_animal-api-sync-be37d",
stats: {
    "name": "Animal-api sync for customer NL_106236",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3993",
        "ok": "3993",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3993",
        "ok": "3993",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3993",
        "ok": "3993",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3993",
        "ok": "3993",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3993",
        "ok": "3993",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3993",
        "ok": "3993",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3993",
        "ok": "3993",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-ca7e3": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_134297",
path: "Animal-api sync for customer NL_134297",
pathFormatted: "req_animal-api-sync-ca7e3",
stats: {
    "name": "Animal-api sync for customer NL_134297",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3774",
        "ok": "3774",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3774",
        "ok": "3774",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3774",
        "ok": "3774",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3774",
        "ok": "3774",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3774",
        "ok": "3774",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3774",
        "ok": "3774",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3774",
        "ok": "3774",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-993d0": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_160647",
path: "Animal-api sync for customer NL_160647",
pathFormatted: "req_animal-api-sync-993d0",
stats: {
    "name": "Animal-api sync for customer NL_160647",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21882",
        "ok": "21882",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "21882",
        "ok": "21882",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "21882",
        "ok": "21882",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "21882",
        "ok": "21882",
        "ko": "-"
    },
    "percentiles2": {
        "total": "21882",
        "ok": "21882",
        "ko": "-"
    },
    "percentiles3": {
        "total": "21882",
        "ok": "21882",
        "ko": "-"
    },
    "percentiles4": {
        "total": "21882",
        "ok": "21882",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-1e171": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111917",
path: "Animal-api sync for customer NL_111917",
pathFormatted: "req_animal-api-sync-1e171",
stats: {
    "name": "Animal-api sync for customer NL_111917",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "15520",
        "ok": "15520",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "15520",
        "ok": "15520",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "15520",
        "ok": "15520",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "15520",
        "ok": "15520",
        "ko": "-"
    },
    "percentiles2": {
        "total": "15520",
        "ok": "15520",
        "ko": "-"
    },
    "percentiles3": {
        "total": "15520",
        "ok": "15520",
        "ko": "-"
    },
    "percentiles4": {
        "total": "15520",
        "ok": "15520",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-5e0b3": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_106214",
path: "Animal-api sync for customer NL_106214",
pathFormatted: "req_animal-api-sync-5e0b3",
stats: {
    "name": "Animal-api sync for customer NL_106214",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2820",
        "ok": "2820",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2820",
        "ok": "2820",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2820",
        "ok": "2820",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2820",
        "ok": "2820",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2820",
        "ok": "2820",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2820",
        "ok": "2820",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2820",
        "ok": "2820",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a7313": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_123953",
path: "Animal-api sync for customer NL_123953",
pathFormatted: "req_animal-api-sync-a7313",
stats: {
    "name": "Animal-api sync for customer NL_123953",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "24649",
        "ok": "24649",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "24649",
        "ok": "24649",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "24649",
        "ok": "24649",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "24649",
        "ok": "24649",
        "ko": "-"
    },
    "percentiles2": {
        "total": "24649",
        "ok": "24649",
        "ko": "-"
    },
    "percentiles3": {
        "total": "24649",
        "ok": "24649",
        "ko": "-"
    },
    "percentiles4": {
        "total": "24649",
        "ok": "24649",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-7cf01": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104284",
path: "Animal-api sync for customer NL_104284",
pathFormatted: "req_animal-api-sync-7cf01",
stats: {
    "name": "Animal-api sync for customer NL_104284",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3266",
        "ok": "3266",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3266",
        "ok": "3266",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3266",
        "ok": "3266",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3266",
        "ok": "3266",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3266",
        "ok": "3266",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3266",
        "ok": "3266",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3266",
        "ok": "3266",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-588aa": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_214938",
path: "Animal-api sync for customer NL_214938",
pathFormatted: "req_animal-api-sync-588aa",
stats: {
    "name": "Animal-api sync for customer NL_214938",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "6504",
        "ok": "6504",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "6504",
        "ok": "6504",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "6504",
        "ok": "6504",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "6504",
        "ok": "6504",
        "ko": "-"
    },
    "percentiles2": {
        "total": "6504",
        "ok": "6504",
        "ko": "-"
    },
    "percentiles3": {
        "total": "6504",
        "ok": "6504",
        "ko": "-"
    },
    "percentiles4": {
        "total": "6504",
        "ok": "6504",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-6ebe1": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104335",
path: "Animal-api sync for customer NL_104335",
pathFormatted: "req_animal-api-sync-6ebe1",
stats: {
    "name": "Animal-api sync for customer NL_104335",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "20779",
        "ok": "20779",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "20779",
        "ok": "20779",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "20779",
        "ok": "20779",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "20779",
        "ok": "20779",
        "ko": "-"
    },
    "percentiles2": {
        "total": "20779",
        "ok": "20779",
        "ko": "-"
    },
    "percentiles3": {
        "total": "20779",
        "ok": "20779",
        "ko": "-"
    },
    "percentiles4": {
        "total": "20779",
        "ok": "20779",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d86cb": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105153",
path: "Animal-api sync for customer NL_105153",
pathFormatted: "req_animal-api-sync-d86cb",
stats: {
    "name": "Animal-api sync for customer NL_105153",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3293",
        "ok": "3293",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3293",
        "ok": "3293",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3293",
        "ok": "3293",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3293",
        "ok": "3293",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3293",
        "ok": "3293",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3293",
        "ok": "3293",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3293",
        "ok": "3293",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b6645": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_126020",
path: "Animal-api sync for customer NL_126020",
pathFormatted: "req_animal-api-sync-b6645",
stats: {
    "name": "Animal-api sync for customer NL_126020",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "10141",
        "ok": "10141",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "10141",
        "ok": "10141",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "10141",
        "ok": "10141",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "10141",
        "ok": "10141",
        "ko": "-"
    },
    "percentiles2": {
        "total": "10141",
        "ok": "10141",
        "ko": "-"
    },
    "percentiles3": {
        "total": "10141",
        "ok": "10141",
        "ko": "-"
    },
    "percentiles4": {
        "total": "10141",
        "ok": "10141",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-7ae4d": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_137252",
path: "Animal-api sync for customer NL_137252",
pathFormatted: "req_animal-api-sync-7ae4d",
stats: {
    "name": "Animal-api sync for customer NL_137252",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3420",
        "ok": "3420",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3420",
        "ok": "3420",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3420",
        "ok": "3420",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3420",
        "ok": "3420",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3420",
        "ok": "3420",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3420",
        "ok": "3420",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3420",
        "ok": "3420",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-f2327": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_107409",
path: "Animal-api sync for customer NL_107409",
pathFormatted: "req_animal-api-sync-f2327",
stats: {
    "name": "Animal-api sync for customer NL_107409",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "10836",
        "ok": "10836",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "10836",
        "ok": "10836",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "10836",
        "ok": "10836",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "10836",
        "ok": "10836",
        "ko": "-"
    },
    "percentiles2": {
        "total": "10836",
        "ok": "10836",
        "ko": "-"
    },
    "percentiles3": {
        "total": "10836",
        "ok": "10836",
        "ko": "-"
    },
    "percentiles4": {
        "total": "10836",
        "ok": "10836",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-9b90a": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_114569",
path: "Animal-api sync for customer NL_114569",
pathFormatted: "req_animal-api-sync-9b90a",
stats: {
    "name": "Animal-api sync for customer NL_114569",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3246",
        "ok": "3246",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3246",
        "ok": "3246",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3246",
        "ok": "3246",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3246",
        "ok": "3246",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3246",
        "ok": "3246",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3246",
        "ok": "3246",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3246",
        "ok": "3246",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-56e97": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_130716",
path: "Animal-api sync for customer NL_130716",
pathFormatted: "req_animal-api-sync-56e97",
stats: {
    "name": "Animal-api sync for customer NL_130716",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3801",
        "ok": "3801",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3801",
        "ok": "3801",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3801",
        "ok": "3801",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3801",
        "ok": "3801",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3801",
        "ok": "3801",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3801",
        "ok": "3801",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3801",
        "ok": "3801",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-4b1c0": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_164635",
path: "Animal-api sync for customer BE_164635",
pathFormatted: "req_animal-api-sync-4b1c0",
stats: {
    "name": "Animal-api sync for customer BE_164635",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2852",
        "ok": "2852",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2852",
        "ok": "2852",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2852",
        "ok": "2852",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2852",
        "ok": "2852",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2852",
        "ok": "2852",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2852",
        "ok": "2852",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2852",
        "ok": "2852",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-0be59": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_137530",
path: "Animal-api sync for customer NL_137530",
pathFormatted: "req_animal-api-sync-0be59",
stats: {
    "name": "Animal-api sync for customer NL_137530",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3319",
        "ok": "3319",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3319",
        "ok": "3319",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3319",
        "ok": "3319",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3319",
        "ok": "3319",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3319",
        "ok": "3319",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3319",
        "ok": "3319",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3319",
        "ok": "3319",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e4170": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_205597",
path: "Animal-api sync for customer BE_205597",
pathFormatted: "req_animal-api-sync-e4170",
stats: {
    "name": "Animal-api sync for customer BE_205597",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5622",
        "ok": "5622",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5622",
        "ok": "5622",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5622",
        "ok": "5622",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5622",
        "ok": "5622",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5622",
        "ok": "5622",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5622",
        "ok": "5622",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5622",
        "ok": "5622",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-5e066": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_118716",
path: "Animal-api sync for customer NL_118716",
pathFormatted: "req_animal-api-sync-5e066",
stats: {
    "name": "Animal-api sync for customer NL_118716",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4541",
        "ok": "4541",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4541",
        "ok": "4541",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4541",
        "ok": "4541",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4541",
        "ok": "4541",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4541",
        "ok": "4541",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4541",
        "ok": "4541",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4541",
        "ok": "4541",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-3598c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_107584",
path: "Animal-api sync for customer NL_107584",
pathFormatted: "req_animal-api-sync-3598c",
stats: {
    "name": "Animal-api sync for customer NL_107584",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5891",
        "ok": "5891",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5891",
        "ok": "5891",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5891",
        "ok": "5891",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5891",
        "ok": "5891",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5891",
        "ok": "5891",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5891",
        "ok": "5891",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5891",
        "ok": "5891",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-6b068": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111950",
path: "Animal-api sync for customer NL_111950",
pathFormatted: "req_animal-api-sync-6b068",
stats: {
    "name": "Animal-api sync for customer NL_111950",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5557",
        "ok": "5557",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5557",
        "ok": "5557",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5557",
        "ok": "5557",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5557",
        "ok": "5557",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5557",
        "ok": "5557",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5557",
        "ok": "5557",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5557",
        "ok": "5557",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-34e6f": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_129959",
path: "Animal-api sync for customer NL_129959",
pathFormatted: "req_animal-api-sync-34e6f",
stats: {
    "name": "Animal-api sync for customer NL_129959",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4547",
        "ok": "4547",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4547",
        "ok": "4547",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4547",
        "ok": "4547",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4547",
        "ok": "4547",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4547",
        "ok": "4547",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4547",
        "ok": "4547",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4547",
        "ok": "4547",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-21b03": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_109721",
path: "Animal-api sync for customer NL_109721",
pathFormatted: "req_animal-api-sync-21b03",
stats: {
    "name": "Animal-api sync for customer NL_109721",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "8952",
        "ok": "8952",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "8952",
        "ok": "8952",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "8952",
        "ok": "8952",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "8952",
        "ok": "8952",
        "ko": "-"
    },
    "percentiles2": {
        "total": "8952",
        "ok": "8952",
        "ko": "-"
    },
    "percentiles3": {
        "total": "8952",
        "ok": "8952",
        "ko": "-"
    },
    "percentiles4": {
        "total": "8952",
        "ok": "8952",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-24d15": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117331",
path: "Animal-api sync for customer NL_117331",
pathFormatted: "req_animal-api-sync-24d15",
stats: {
    "name": "Animal-api sync for customer NL_117331",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5237",
        "ok": "5237",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5237",
        "ok": "5237",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5237",
        "ok": "5237",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5237",
        "ok": "5237",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5237",
        "ok": "5237",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5237",
        "ok": "5237",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5237",
        "ok": "5237",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-4479f": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_134641",
path: "Animal-api sync for customer NL_134641",
pathFormatted: "req_animal-api-sync-4479f",
stats: {
    "name": "Animal-api sync for customer NL_134641",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2182",
        "ok": "2182",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2182",
        "ok": "2182",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2182",
        "ok": "2182",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2182",
        "ok": "2182",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2182",
        "ok": "2182",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2182",
        "ok": "2182",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2182",
        "ok": "2182",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-2ac98": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_119983",
path: "Animal-api sync for customer NL_119983",
pathFormatted: "req_animal-api-sync-2ac98",
stats: {
    "name": "Animal-api sync for customer NL_119983",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "23483",
        "ok": "23483",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "23483",
        "ok": "23483",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "23483",
        "ok": "23483",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "23483",
        "ok": "23483",
        "ko": "-"
    },
    "percentiles2": {
        "total": "23483",
        "ok": "23483",
        "ko": "-"
    },
    "percentiles3": {
        "total": "23483",
        "ok": "23483",
        "ko": "-"
    },
    "percentiles4": {
        "total": "23483",
        "ok": "23483",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-53e53": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_122724",
path: "Animal-api sync for customer NL_122724",
pathFormatted: "req_animal-api-sync-53e53",
stats: {
    "name": "Animal-api sync for customer NL_122724",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "6690",
        "ok": "6690",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "6690",
        "ok": "6690",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "6690",
        "ok": "6690",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "6690",
        "ok": "6690",
        "ko": "-"
    },
    "percentiles2": {
        "total": "6690",
        "ok": "6690",
        "ko": "-"
    },
    "percentiles3": {
        "total": "6690",
        "ok": "6690",
        "ko": "-"
    },
    "percentiles4": {
        "total": "6690",
        "ok": "6690",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-97824": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_129903",
path: "Animal-api sync for customer NL_129903",
pathFormatted: "req_animal-api-sync-97824",
stats: {
    "name": "Animal-api sync for customer NL_129903",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5199",
        "ok": "5199",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5199",
        "ok": "5199",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5199",
        "ok": "5199",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5199",
        "ok": "5199",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5199",
        "ok": "5199",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5199",
        "ok": "5199",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5199",
        "ok": "5199",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-9db7a": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_125745",
path: "Animal-api sync for customer NL_125745",
pathFormatted: "req_animal-api-sync-9db7a",
stats: {
    "name": "Animal-api sync for customer NL_125745",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "11797",
        "ok": "11797",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "11797",
        "ok": "11797",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "11797",
        "ok": "11797",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "11797",
        "ok": "11797",
        "ko": "-"
    },
    "percentiles2": {
        "total": "11797",
        "ok": "11797",
        "ko": "-"
    },
    "percentiles3": {
        "total": "11797",
        "ok": "11797",
        "ko": "-"
    },
    "percentiles4": {
        "total": "11797",
        "ok": "11797",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-62afe": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_128808",
path: "Animal-api sync for customer NL_128808",
pathFormatted: "req_animal-api-sync-62afe",
stats: {
    "name": "Animal-api sync for customer NL_128808",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4019",
        "ok": "4019",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4019",
        "ok": "4019",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4019",
        "ok": "4019",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4019",
        "ok": "4019",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4019",
        "ok": "4019",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4019",
        "ok": "4019",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4019",
        "ok": "4019",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d8b1c": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_214192",
path: "Animal-api sync for customer BE_214192",
pathFormatted: "req_animal-api-sync-d8b1c",
stats: {
    "name": "Animal-api sync for customer BE_214192",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1747",
        "ok": "1747",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1747",
        "ok": "1747",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1747",
        "ok": "1747",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1747",
        "ok": "1747",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1747",
        "ok": "1747",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1747",
        "ok": "1747",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1747",
        "ok": "1747",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-22fcc": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110755",
path: "Animal-api sync for customer NL_110755",
pathFormatted: "req_animal-api-sync-22fcc",
stats: {
    "name": "Animal-api sync for customer NL_110755",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4575",
        "ok": "4575",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4575",
        "ok": "4575",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4575",
        "ok": "4575",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4575",
        "ok": "4575",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4575",
        "ok": "4575",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4575",
        "ok": "4575",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4575",
        "ok": "4575",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-1330d": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_145902",
path: "Animal-api sync for customer BE_145902",
pathFormatted: "req_animal-api-sync-1330d",
stats: {
    "name": "Animal-api sync for customer BE_145902",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3307",
        "ok": "3307",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3307",
        "ok": "3307",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3307",
        "ok": "3307",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3307",
        "ok": "3307",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3307",
        "ok": "3307",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3307",
        "ok": "3307",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3307",
        "ok": "3307",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-2cece": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110572",
path: "Animal-api sync for customer NL_110572",
pathFormatted: "req_animal-api-sync-2cece",
stats: {
    "name": "Animal-api sync for customer NL_110572",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3882",
        "ok": "3882",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3882",
        "ok": "3882",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3882",
        "ok": "3882",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3882",
        "ok": "3882",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3882",
        "ok": "3882",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3882",
        "ok": "3882",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3882",
        "ok": "3882",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-184df": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110881",
path: "Animal-api sync for customer NL_110881",
pathFormatted: "req_animal-api-sync-184df",
stats: {
    "name": "Animal-api sync for customer NL_110881",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3860",
        "ok": "3860",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3860",
        "ok": "3860",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3860",
        "ok": "3860",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3860",
        "ok": "3860",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3860",
        "ok": "3860",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3860",
        "ok": "3860",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3860",
        "ok": "3860",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-61fa0": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_214412",
path: "Animal-api sync for customer NL_214412",
pathFormatted: "req_animal-api-sync-61fa0",
stats: {
    "name": "Animal-api sync for customer NL_214412",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3345",
        "ok": "3345",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3345",
        "ok": "3345",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3345",
        "ok": "3345",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3345",
        "ok": "3345",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3345",
        "ok": "3345",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3345",
        "ok": "3345",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3345",
        "ok": "3345",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-2f673": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_112286",
path: "Animal-api sync for customer NL_112286",
pathFormatted: "req_animal-api-sync-2f673",
stats: {
    "name": "Animal-api sync for customer NL_112286",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1561",
        "ok": "1561",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1561",
        "ok": "1561",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1561",
        "ok": "1561",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1561",
        "ok": "1561",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1561",
        "ok": "1561",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1561",
        "ok": "1561",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1561",
        "ok": "1561",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-7a391": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_125864",
path: "Animal-api sync for customer NL_125864",
pathFormatted: "req_animal-api-sync-7a391",
stats: {
    "name": "Animal-api sync for customer NL_125864",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4457",
        "ok": "4457",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4457",
        "ok": "4457",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4457",
        "ok": "4457",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4457",
        "ok": "4457",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4457",
        "ok": "4457",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4457",
        "ok": "4457",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4457",
        "ok": "4457",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-844f0": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_128832",
path: "Animal-api sync for customer NL_128832",
pathFormatted: "req_animal-api-sync-844f0",
stats: {
    "name": "Animal-api sync for customer NL_128832",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "8794",
        "ok": "8794",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "8794",
        "ok": "8794",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "8794",
        "ok": "8794",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "8794",
        "ok": "8794",
        "ko": "-"
    },
    "percentiles2": {
        "total": "8794",
        "ok": "8794",
        "ko": "-"
    },
    "percentiles3": {
        "total": "8794",
        "ok": "8794",
        "ko": "-"
    },
    "percentiles4": {
        "total": "8794",
        "ok": "8794",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-4e25d": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104043",
path: "Animal-api sync for customer NL_104043",
pathFormatted: "req_animal-api-sync-4e25d",
stats: {
    "name": "Animal-api sync for customer NL_104043",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5661",
        "ok": "5661",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5661",
        "ok": "5661",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5661",
        "ok": "5661",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5661",
        "ok": "5661",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5661",
        "ok": "5661",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5661",
        "ok": "5661",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5661",
        "ok": "5661",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-25426": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_114351",
path: "Animal-api sync for customer NL_114351",
pathFormatted: "req_animal-api-sync-25426",
stats: {
    "name": "Animal-api sync for customer NL_114351",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2510",
        "ok": "2510",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2510",
        "ok": "2510",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2510",
        "ok": "2510",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2510",
        "ok": "2510",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2510",
        "ok": "2510",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2510",
        "ok": "2510",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2510",
        "ok": "2510",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-dfb65": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_108668",
path: "Animal-api sync for customer NL_108668",
pathFormatted: "req_animal-api-sync-dfb65",
stats: {
    "name": "Animal-api sync for customer NL_108668",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4953",
        "ok": "4953",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4953",
        "ok": "4953",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4953",
        "ok": "4953",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4953",
        "ok": "4953",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4953",
        "ok": "4953",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4953",
        "ok": "4953",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4953",
        "ok": "4953",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-997d2": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111823",
path: "Animal-api sync for customer NL_111823",
pathFormatted: "req_animal-api-sync-997d2",
stats: {
    "name": "Animal-api sync for customer NL_111823",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "16381",
        "ok": "16381",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "16381",
        "ok": "16381",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "16381",
        "ok": "16381",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "16381",
        "ok": "16381",
        "ko": "-"
    },
    "percentiles2": {
        "total": "16381",
        "ok": "16381",
        "ko": "-"
    },
    "percentiles3": {
        "total": "16381",
        "ok": "16381",
        "ko": "-"
    },
    "percentiles4": {
        "total": "16381",
        "ok": "16381",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d5cc8": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_103579",
path: "Animal-api sync for customer NL_103579",
pathFormatted: "req_animal-api-sync-d5cc8",
stats: {
    "name": "Animal-api sync for customer NL_103579",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3953",
        "ok": "3953",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3953",
        "ok": "3953",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3953",
        "ok": "3953",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3953",
        "ok": "3953",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3953",
        "ok": "3953",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3953",
        "ok": "3953",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3953",
        "ok": "3953",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-96467": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_134577",
path: "Animal-api sync for customer NL_134577",
pathFormatted: "req_animal-api-sync-96467",
stats: {
    "name": "Animal-api sync for customer NL_134577",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3453",
        "ok": "3453",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3453",
        "ok": "3453",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3453",
        "ok": "3453",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3453",
        "ok": "3453",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3453",
        "ok": "3453",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3453",
        "ok": "3453",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3453",
        "ok": "3453",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-4c254": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105650",
path: "Animal-api sync for customer NL_105650",
pathFormatted: "req_animal-api-sync-4c254",
stats: {
    "name": "Animal-api sync for customer NL_105650",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "12970",
        "ok": "12970",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "12970",
        "ok": "12970",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "12970",
        "ok": "12970",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "12970",
        "ok": "12970",
        "ko": "-"
    },
    "percentiles2": {
        "total": "12970",
        "ok": "12970",
        "ko": "-"
    },
    "percentiles3": {
        "total": "12970",
        "ok": "12970",
        "ko": "-"
    },
    "percentiles4": {
        "total": "12970",
        "ok": "12970",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-5ea6b": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_194847",
path: "Animal-api sync for customer BE_194847",
pathFormatted: "req_animal-api-sync-5ea6b",
stats: {
    "name": "Animal-api sync for customer BE_194847",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5958",
        "ok": "5958",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5958",
        "ok": "5958",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5958",
        "ok": "5958",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5958",
        "ok": "5958",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5958",
        "ok": "5958",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5958",
        "ok": "5958",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5958",
        "ok": "5958",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a4924": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117341",
path: "Animal-api sync for customer NL_117341",
pathFormatted: "req_animal-api-sync-a4924",
stats: {
    "name": "Animal-api sync for customer NL_117341",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2681",
        "ok": "2681",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2681",
        "ok": "2681",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2681",
        "ok": "2681",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2681",
        "ok": "2681",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2681",
        "ok": "2681",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2681",
        "ok": "2681",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2681",
        "ok": "2681",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-12331": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_113115",
path: "Animal-api sync for customer NL_113115",
pathFormatted: "req_animal-api-sync-12331",
stats: {
    "name": "Animal-api sync for customer NL_113115",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5193",
        "ok": "5193",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5193",
        "ok": "5193",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5193",
        "ok": "5193",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5193",
        "ok": "5193",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5193",
        "ok": "5193",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5193",
        "ok": "5193",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5193",
        "ok": "5193",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-13f62": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_149314",
path: "Animal-api sync for customer BE_149314",
pathFormatted: "req_animal-api-sync-13f62",
stats: {
    "name": "Animal-api sync for customer BE_149314",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3294",
        "ok": "3294",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3294",
        "ok": "3294",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3294",
        "ok": "3294",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3294",
        "ok": "3294",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3294",
        "ok": "3294",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3294",
        "ok": "3294",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3294",
        "ok": "3294",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-500bc": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117068",
path: "Animal-api sync for customer NL_117068",
pathFormatted: "req_animal-api-sync-500bc",
stats: {
    "name": "Animal-api sync for customer NL_117068",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2146",
        "ok": "2146",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2146",
        "ok": "2146",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2146",
        "ok": "2146",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2146",
        "ok": "2146",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2146",
        "ok": "2146",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2146",
        "ok": "2146",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2146",
        "ok": "2146",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-6d4c5": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_122660",
path: "Animal-api sync for customer NL_122660",
pathFormatted: "req_animal-api-sync-6d4c5",
stats: {
    "name": "Animal-api sync for customer NL_122660",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1323",
        "ok": "1323",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1323",
        "ok": "1323",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1323",
        "ok": "1323",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1323",
        "ok": "1323",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1323",
        "ok": "1323",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1323",
        "ok": "1323",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1323",
        "ok": "1323",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-cb570": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104777",
path: "Animal-api sync for customer NL_104777",
pathFormatted: "req_animal-api-sync-cb570",
stats: {
    "name": "Animal-api sync for customer NL_104777",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "27461",
        "ok": "27461",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "27461",
        "ok": "27461",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "27461",
        "ok": "27461",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "27461",
        "ok": "27461",
        "ko": "-"
    },
    "percentiles2": {
        "total": "27461",
        "ok": "27461",
        "ko": "-"
    },
    "percentiles3": {
        "total": "27461",
        "ok": "27461",
        "ko": "-"
    },
    "percentiles4": {
        "total": "27461",
        "ok": "27461",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-49e6e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_122219",
path: "Animal-api sync for customer NL_122219",
pathFormatted: "req_animal-api-sync-49e6e",
stats: {
    "name": "Animal-api sync for customer NL_122219",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3559",
        "ok": "3559",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3559",
        "ok": "3559",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3559",
        "ok": "3559",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3559",
        "ok": "3559",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3559",
        "ok": "3559",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3559",
        "ok": "3559",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3559",
        "ok": "3559",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b0552": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_122008",
path: "Animal-api sync for customer NL_122008",
pathFormatted: "req_animal-api-sync-b0552",
stats: {
    "name": "Animal-api sync for customer NL_122008",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "11193",
        "ok": "11193",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "11193",
        "ok": "11193",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "11193",
        "ok": "11193",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "11193",
        "ok": "11193",
        "ko": "-"
    },
    "percentiles2": {
        "total": "11193",
        "ok": "11193",
        "ko": "-"
    },
    "percentiles3": {
        "total": "11193",
        "ok": "11193",
        "ko": "-"
    },
    "percentiles4": {
        "total": "11193",
        "ok": "11193",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-7d526": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105576",
path: "Animal-api sync for customer NL_105576",
pathFormatted: "req_animal-api-sync-7d526",
stats: {
    "name": "Animal-api sync for customer NL_105576",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4309",
        "ok": "4309",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4309",
        "ok": "4309",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4309",
        "ok": "4309",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4309",
        "ok": "4309",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4309",
        "ok": "4309",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4309",
        "ok": "4309",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4309",
        "ok": "4309",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a6cf9": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104168",
path: "Animal-api sync for customer NL_104168",
pathFormatted: "req_animal-api-sync-a6cf9",
stats: {
    "name": "Animal-api sync for customer NL_104168",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "8014",
        "ok": "8014",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "8014",
        "ok": "8014",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "8014",
        "ok": "8014",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "8014",
        "ok": "8014",
        "ko": "-"
    },
    "percentiles2": {
        "total": "8014",
        "ok": "8014",
        "ko": "-"
    },
    "percentiles3": {
        "total": "8014",
        "ok": "8014",
        "ko": "-"
    },
    "percentiles4": {
        "total": "8014",
        "ok": "8014",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-6809e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117264",
path: "Animal-api sync for customer NL_117264",
pathFormatted: "req_animal-api-sync-6809e",
stats: {
    "name": "Animal-api sync for customer NL_117264",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "22434",
        "ok": "22434",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "22434",
        "ok": "22434",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "22434",
        "ok": "22434",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "22434",
        "ok": "22434",
        "ko": "-"
    },
    "percentiles2": {
        "total": "22434",
        "ok": "22434",
        "ko": "-"
    },
    "percentiles3": {
        "total": "22434",
        "ok": "22434",
        "ko": "-"
    },
    "percentiles4": {
        "total": "22434",
        "ok": "22434",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-cb15a": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_114531",
path: "Animal-api sync for customer NL_114531",
pathFormatted: "req_animal-api-sync-cb15a",
stats: {
    "name": "Animal-api sync for customer NL_114531",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5288",
        "ok": "5288",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5288",
        "ok": "5288",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5288",
        "ok": "5288",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5288",
        "ok": "5288",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5288",
        "ok": "5288",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5288",
        "ok": "5288",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5288",
        "ok": "5288",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-aa469": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_158913",
path: "Animal-api sync for customer BE_158913",
pathFormatted: "req_animal-api-sync-aa469",
stats: {
    "name": "Animal-api sync for customer BE_158913",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5145",
        "ok": "5145",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5145",
        "ok": "5145",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5145",
        "ok": "5145",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5145",
        "ok": "5145",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5145",
        "ok": "5145",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5145",
        "ok": "5145",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5145",
        "ok": "5145",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-fc480": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_121609",
path: "Animal-api sync for customer NL_121609",
pathFormatted: "req_animal-api-sync-fc480",
stats: {
    "name": "Animal-api sync for customer NL_121609",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "12107",
        "ok": "12107",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "12107",
        "ok": "12107",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "12107",
        "ok": "12107",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "12107",
        "ok": "12107",
        "ko": "-"
    },
    "percentiles2": {
        "total": "12107",
        "ok": "12107",
        "ko": "-"
    },
    "percentiles3": {
        "total": "12107",
        "ok": "12107",
        "ko": "-"
    },
    "percentiles4": {
        "total": "12107",
        "ok": "12107",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-2fa8f": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_103719",
path: "Animal-api sync for customer NL_103719",
pathFormatted: "req_animal-api-sync-2fa8f",
stats: {
    "name": "Animal-api sync for customer NL_103719",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "19968",
        "ok": "19968",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "19968",
        "ok": "19968",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "19968",
        "ok": "19968",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "19968",
        "ok": "19968",
        "ko": "-"
    },
    "percentiles2": {
        "total": "19968",
        "ok": "19968",
        "ko": "-"
    },
    "percentiles3": {
        "total": "19968",
        "ok": "19968",
        "ko": "-"
    },
    "percentiles4": {
        "total": "19968",
        "ok": "19968",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c7578": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105839",
path: "Animal-api sync for customer NL_105839",
pathFormatted: "req_animal-api-sync-c7578",
stats: {
    "name": "Animal-api sync for customer NL_105839",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "23404",
        "ok": "23404",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "23404",
        "ok": "23404",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "23404",
        "ok": "23404",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "23404",
        "ok": "23404",
        "ko": "-"
    },
    "percentiles2": {
        "total": "23404",
        "ok": "23404",
        "ko": "-"
    },
    "percentiles3": {
        "total": "23404",
        "ok": "23404",
        "ko": "-"
    },
    "percentiles4": {
        "total": "23404",
        "ok": "23404",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-ca9a4": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_123159",
path: "Animal-api sync for customer NL_123159",
pathFormatted: "req_animal-api-sync-ca9a4",
stats: {
    "name": "Animal-api sync for customer NL_123159",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2851",
        "ok": "2851",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2851",
        "ok": "2851",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2851",
        "ok": "2851",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2851",
        "ok": "2851",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2851",
        "ok": "2851",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2851",
        "ok": "2851",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2851",
        "ok": "2851",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d58c5": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_115162",
path: "Animal-api sync for customer NL_115162",
pathFormatted: "req_animal-api-sync-d58c5",
stats: {
    "name": "Animal-api sync for customer NL_115162",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1829",
        "ok": "1829",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1829",
        "ok": "1829",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1829",
        "ok": "1829",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1829",
        "ok": "1829",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1829",
        "ok": "1829",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1829",
        "ok": "1829",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1829",
        "ok": "1829",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d43d8": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_112453",
path: "Animal-api sync for customer NL_112453",
pathFormatted: "req_animal-api-sync-d43d8",
stats: {
    "name": "Animal-api sync for customer NL_112453",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3967",
        "ok": "3967",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3967",
        "ok": "3967",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3967",
        "ok": "3967",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3967",
        "ok": "3967",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3967",
        "ok": "3967",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3967",
        "ok": "3967",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3967",
        "ok": "3967",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-fa50f": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_194938",
path: "Animal-api sync for customer NL_194938",
pathFormatted: "req_animal-api-sync-fa50f",
stats: {
    "name": "Animal-api sync for customer NL_194938",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "11690",
        "ok": "11690",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "11690",
        "ok": "11690",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "11690",
        "ok": "11690",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "11690",
        "ok": "11690",
        "ko": "-"
    },
    "percentiles2": {
        "total": "11690",
        "ok": "11690",
        "ko": "-"
    },
    "percentiles3": {
        "total": "11690",
        "ok": "11690",
        "ko": "-"
    },
    "percentiles4": {
        "total": "11690",
        "ok": "11690",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d3cf7": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_123704",
path: "Animal-api sync for customer NL_123704",
pathFormatted: "req_animal-api-sync-d3cf7",
stats: {
    "name": "Animal-api sync for customer NL_123704",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2785",
        "ok": "2785",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2785",
        "ok": "2785",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2785",
        "ok": "2785",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2785",
        "ok": "2785",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2785",
        "ok": "2785",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2785",
        "ok": "2785",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2785",
        "ok": "2785",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e5ad2": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_108858",
path: "Animal-api sync for customer NL_108858",
pathFormatted: "req_animal-api-sync-e5ad2",
stats: {
    "name": "Animal-api sync for customer NL_108858",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5620",
        "ok": "5620",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5620",
        "ok": "5620",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5620",
        "ok": "5620",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5620",
        "ok": "5620",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5620",
        "ok": "5620",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5620",
        "ok": "5620",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5620",
        "ok": "5620",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-23adc": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110836",
path: "Animal-api sync for customer NL_110836",
pathFormatted: "req_animal-api-sync-23adc",
stats: {
    "name": "Animal-api sync for customer NL_110836",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "6592",
        "ok": "6592",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "6592",
        "ok": "6592",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "6592",
        "ok": "6592",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "6592",
        "ok": "6592",
        "ko": "-"
    },
    "percentiles2": {
        "total": "6592",
        "ok": "6592",
        "ko": "-"
    },
    "percentiles3": {
        "total": "6592",
        "ok": "6592",
        "ko": "-"
    },
    "percentiles4": {
        "total": "6592",
        "ok": "6592",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-8d897": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_133100",
path: "Animal-api sync for customer NL_133100",
pathFormatted: "req_animal-api-sync-8d897",
stats: {
    "name": "Animal-api sync for customer NL_133100",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "16745",
        "ok": "16745",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "16745",
        "ok": "16745",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "16745",
        "ok": "16745",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "16745",
        "ok": "16745",
        "ko": "-"
    },
    "percentiles2": {
        "total": "16745",
        "ok": "16745",
        "ko": "-"
    },
    "percentiles3": {
        "total": "16745",
        "ok": "16745",
        "ko": "-"
    },
    "percentiles4": {
        "total": "16745",
        "ok": "16745",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e921e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_122446",
path: "Animal-api sync for customer NL_122446",
pathFormatted: "req_animal-api-sync-e921e",
stats: {
    "name": "Animal-api sync for customer NL_122446",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "8690",
        "ok": "8690",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "8690",
        "ok": "8690",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "8690",
        "ok": "8690",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "8690",
        "ok": "8690",
        "ko": "-"
    },
    "percentiles2": {
        "total": "8690",
        "ok": "8690",
        "ko": "-"
    },
    "percentiles3": {
        "total": "8690",
        "ok": "8690",
        "ko": "-"
    },
    "percentiles4": {
        "total": "8690",
        "ok": "8690",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-fd312": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117690",
path: "Animal-api sync for customer NL_117690",
pathFormatted: "req_animal-api-sync-fd312",
stats: {
    "name": "Animal-api sync for customer NL_117690",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5691",
        "ok": "5691",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5691",
        "ok": "5691",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5691",
        "ok": "5691",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5691",
        "ok": "5691",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5691",
        "ok": "5691",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5691",
        "ok": "5691",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5691",
        "ok": "5691",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c6f3a": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_119414",
path: "Animal-api sync for customer NL_119414",
pathFormatted: "req_animal-api-sync-c6f3a",
stats: {
    "name": "Animal-api sync for customer NL_119414",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "7755",
        "ok": "7755",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "7755",
        "ok": "7755",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "7755",
        "ok": "7755",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "7755",
        "ok": "7755",
        "ko": "-"
    },
    "percentiles2": {
        "total": "7755",
        "ok": "7755",
        "ko": "-"
    },
    "percentiles3": {
        "total": "7755",
        "ok": "7755",
        "ko": "-"
    },
    "percentiles4": {
        "total": "7755",
        "ok": "7755",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a8229": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_129350",
path: "Animal-api sync for customer NL_129350",
pathFormatted: "req_animal-api-sync-a8229",
stats: {
    "name": "Animal-api sync for customer NL_129350",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "14820",
        "ok": "14820",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "14820",
        "ok": "14820",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "14820",
        "ok": "14820",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "14820",
        "ok": "14820",
        "ko": "-"
    },
    "percentiles2": {
        "total": "14820",
        "ok": "14820",
        "ko": "-"
    },
    "percentiles3": {
        "total": "14820",
        "ok": "14820",
        "ko": "-"
    },
    "percentiles4": {
        "total": "14820",
        "ok": "14820",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e7e4c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105106",
path: "Animal-api sync for customer NL_105106",
pathFormatted: "req_animal-api-sync-e7e4c",
stats: {
    "name": "Animal-api sync for customer NL_105106",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5144",
        "ok": "5144",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5144",
        "ok": "5144",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5144",
        "ok": "5144",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5144",
        "ok": "5144",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5144",
        "ok": "5144",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5144",
        "ok": "5144",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5144",
        "ok": "5144",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c3423": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_112704",
path: "Animal-api sync for customer NL_112704",
pathFormatted: "req_animal-api-sync-c3423",
stats: {
    "name": "Animal-api sync for customer NL_112704",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "9611",
        "ok": "9611",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "9611",
        "ok": "9611",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "9611",
        "ok": "9611",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "9611",
        "ok": "9611",
        "ko": "-"
    },
    "percentiles2": {
        "total": "9611",
        "ok": "9611",
        "ko": "-"
    },
    "percentiles3": {
        "total": "9611",
        "ok": "9611",
        "ko": "-"
    },
    "percentiles4": {
        "total": "9611",
        "ok": "9611",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c2f39": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_157878",
path: "Animal-api sync for customer BE_157878",
pathFormatted: "req_animal-api-sync-c2f39",
stats: {
    "name": "Animal-api sync for customer BE_157878",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "6937",
        "ok": "6937",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "6937",
        "ok": "6937",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "6937",
        "ok": "6937",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "6937",
        "ok": "6937",
        "ko": "-"
    },
    "percentiles2": {
        "total": "6937",
        "ok": "6937",
        "ko": "-"
    },
    "percentiles3": {
        "total": "6937",
        "ok": "6937",
        "ko": "-"
    },
    "percentiles4": {
        "total": "6937",
        "ok": "6937",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-fee59": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117006",
path: "Animal-api sync for customer NL_117006",
pathFormatted: "req_animal-api-sync-fee59",
stats: {
    "name": "Animal-api sync for customer NL_117006",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1909",
        "ok": "1909",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1909",
        "ok": "1909",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1909",
        "ok": "1909",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1909",
        "ok": "1909",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1909",
        "ok": "1909",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1909",
        "ok": "1909",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1909",
        "ok": "1909",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-bab68": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_134874",
path: "Animal-api sync for customer NL_134874",
pathFormatted: "req_animal-api-sync-bab68",
stats: {
    "name": "Animal-api sync for customer NL_134874",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "6439",
        "ok": "6439",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "6439",
        "ok": "6439",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "6439",
        "ok": "6439",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "6439",
        "ok": "6439",
        "ko": "-"
    },
    "percentiles2": {
        "total": "6439",
        "ok": "6439",
        "ko": "-"
    },
    "percentiles3": {
        "total": "6439",
        "ok": "6439",
        "ko": "-"
    },
    "percentiles4": {
        "total": "6439",
        "ok": "6439",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-3e99c": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_214193",
path: "Animal-api sync for customer BE_214193",
pathFormatted: "req_animal-api-sync-3e99c",
stats: {
    "name": "Animal-api sync for customer BE_214193",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "8831",
        "ok": "8831",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "8831",
        "ok": "8831",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "8831",
        "ok": "8831",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "8831",
        "ok": "8831",
        "ko": "-"
    },
    "percentiles2": {
        "total": "8831",
        "ok": "8831",
        "ko": "-"
    },
    "percentiles3": {
        "total": "8831",
        "ok": "8831",
        "ko": "-"
    },
    "percentiles4": {
        "total": "8831",
        "ok": "8831",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b2bc0": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_162281",
path: "Animal-api sync for customer BE_162281",
pathFormatted: "req_animal-api-sync-b2bc0",
stats: {
    "name": "Animal-api sync for customer BE_162281",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "8881",
        "ok": "8881",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "8881",
        "ok": "8881",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "8881",
        "ok": "8881",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "8881",
        "ok": "8881",
        "ko": "-"
    },
    "percentiles2": {
        "total": "8881",
        "ok": "8881",
        "ko": "-"
    },
    "percentiles3": {
        "total": "8881",
        "ok": "8881",
        "ko": "-"
    },
    "percentiles4": {
        "total": "8881",
        "ok": "8881",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-046eb": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_127329",
path: "Animal-api sync for customer NL_127329",
pathFormatted: "req_animal-api-sync-046eb",
stats: {
    "name": "Animal-api sync for customer NL_127329",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2171",
        "ok": "2171",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2171",
        "ok": "2171",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2171",
        "ok": "2171",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2171",
        "ok": "2171",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2171",
        "ok": "2171",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2171",
        "ok": "2171",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2171",
        "ok": "2171",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-7cc26": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_131009",
path: "Animal-api sync for customer NL_131009",
pathFormatted: "req_animal-api-sync-7cc26",
stats: {
    "name": "Animal-api sync for customer NL_131009",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "7530",
        "ok": "7530",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "7530",
        "ok": "7530",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "7530",
        "ok": "7530",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "7530",
        "ok": "7530",
        "ko": "-"
    },
    "percentiles2": {
        "total": "7530",
        "ok": "7530",
        "ko": "-"
    },
    "percentiles3": {
        "total": "7530",
        "ok": "7530",
        "ko": "-"
    },
    "percentiles4": {
        "total": "7530",
        "ok": "7530",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-52d91": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_163987",
path: "Animal-api sync for customer BE_163987",
pathFormatted: "req_animal-api-sync-52d91",
stats: {
    "name": "Animal-api sync for customer BE_163987",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3320",
        "ok": "3320",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3320",
        "ok": "3320",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3320",
        "ok": "3320",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3320",
        "ok": "3320",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3320",
        "ok": "3320",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3320",
        "ok": "3320",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3320",
        "ok": "3320",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-9b768": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_115732",
path: "Animal-api sync for customer NL_115732",
pathFormatted: "req_animal-api-sync-9b768",
stats: {
    "name": "Animal-api sync for customer NL_115732",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "7232",
        "ok": "7232",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "7232",
        "ok": "7232",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "7232",
        "ok": "7232",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "7232",
        "ok": "7232",
        "ko": "-"
    },
    "percentiles2": {
        "total": "7232",
        "ok": "7232",
        "ko": "-"
    },
    "percentiles3": {
        "total": "7232",
        "ok": "7232",
        "ko": "-"
    },
    "percentiles4": {
        "total": "7232",
        "ok": "7232",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c1337": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104350",
path: "Animal-api sync for customer NL_104350",
pathFormatted: "req_animal-api-sync-c1337",
stats: {
    "name": "Animal-api sync for customer NL_104350",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5920",
        "ok": "5920",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5920",
        "ok": "5920",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5920",
        "ok": "5920",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5920",
        "ok": "5920",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5920",
        "ok": "5920",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5920",
        "ok": "5920",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5920",
        "ok": "5920",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-de861": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_138595",
path: "Animal-api sync for customer NL_138595",
pathFormatted: "req_animal-api-sync-de861",
stats: {
    "name": "Animal-api sync for customer NL_138595",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2003",
        "ok": "2003",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2003",
        "ok": "2003",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2003",
        "ok": "2003",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2003",
        "ok": "2003",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2003",
        "ok": "2003",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2003",
        "ok": "2003",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2003",
        "ok": "2003",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-cc8e5": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_123944",
path: "Animal-api sync for customer NL_123944",
pathFormatted: "req_animal-api-sync-cc8e5",
stats: {
    "name": "Animal-api sync for customer NL_123944",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3256",
        "ok": "3256",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3256",
        "ok": "3256",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3256",
        "ok": "3256",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3256",
        "ok": "3256",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3256",
        "ok": "3256",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3256",
        "ok": "3256",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3256",
        "ok": "3256",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-033e7": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_113983",
path: "Animal-api sync for customer NL_113983",
pathFormatted: "req_animal-api-sync-033e7",
stats: {
    "name": "Animal-api sync for customer NL_113983",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1704",
        "ok": "1704",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1704",
        "ok": "1704",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1704",
        "ok": "1704",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1704",
        "ok": "1704",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1704",
        "ok": "1704",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1704",
        "ok": "1704",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1704",
        "ok": "1704",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-cb74a": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_131236",
path: "Animal-api sync for customer NL_131236",
pathFormatted: "req_animal-api-sync-cb74a",
stats: {
    "name": "Animal-api sync for customer NL_131236",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4403",
        "ok": "4403",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4403",
        "ok": "4403",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4403",
        "ok": "4403",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4403",
        "ok": "4403",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4403",
        "ok": "4403",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4403",
        "ok": "4403",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4403",
        "ok": "4403",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-4c33d": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_131701",
path: "Animal-api sync for customer NL_131701",
pathFormatted: "req_animal-api-sync-4c33d",
stats: {
    "name": "Animal-api sync for customer NL_131701",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2861",
        "ok": "2861",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2861",
        "ok": "2861",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2861",
        "ok": "2861",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2861",
        "ok": "2861",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2861",
        "ok": "2861",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2861",
        "ok": "2861",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2861",
        "ok": "2861",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-98e1b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_129146",
path: "Animal-api sync for customer NL_129146",
pathFormatted: "req_animal-api-sync-98e1b",
stats: {
    "name": "Animal-api sync for customer NL_129146",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "12013",
        "ok": "12013",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "12013",
        "ok": "12013",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "12013",
        "ok": "12013",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "12013",
        "ok": "12013",
        "ko": "-"
    },
    "percentiles2": {
        "total": "12013",
        "ok": "12013",
        "ko": "-"
    },
    "percentiles3": {
        "total": "12013",
        "ok": "12013",
        "ko": "-"
    },
    "percentiles4": {
        "total": "12013",
        "ok": "12013",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e8633": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117217",
path: "Animal-api sync for customer NL_117217",
pathFormatted: "req_animal-api-sync-e8633",
stats: {
    "name": "Animal-api sync for customer NL_117217",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3319",
        "ok": "3319",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3319",
        "ok": "3319",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3319",
        "ok": "3319",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3319",
        "ok": "3319",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3319",
        "ok": "3319",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3319",
        "ok": "3319",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3319",
        "ok": "3319",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-748ee": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_118193",
path: "Animal-api sync for customer NL_118193",
pathFormatted: "req_animal-api-sync-748ee",
stats: {
    "name": "Animal-api sync for customer NL_118193",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "9094",
        "ok": "9094",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "9094",
        "ok": "9094",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "9094",
        "ok": "9094",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "9094",
        "ok": "9094",
        "ko": "-"
    },
    "percentiles2": {
        "total": "9094",
        "ok": "9094",
        "ko": "-"
    },
    "percentiles3": {
        "total": "9094",
        "ok": "9094",
        "ko": "-"
    },
    "percentiles4": {
        "total": "9094",
        "ok": "9094",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-ac942": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_162293",
path: "Animal-api sync for customer BE_162293",
pathFormatted: "req_animal-api-sync-ac942",
stats: {
    "name": "Animal-api sync for customer BE_162293",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "6498",
        "ok": "6498",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "6498",
        "ok": "6498",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "6498",
        "ok": "6498",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "6498",
        "ok": "6498",
        "ko": "-"
    },
    "percentiles2": {
        "total": "6498",
        "ok": "6498",
        "ko": "-"
    },
    "percentiles3": {
        "total": "6498",
        "ok": "6498",
        "ko": "-"
    },
    "percentiles4": {
        "total": "6498",
        "ok": "6498",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-18df0": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_132381",
path: "Animal-api sync for customer NL_132381",
pathFormatted: "req_animal-api-sync-18df0",
stats: {
    "name": "Animal-api sync for customer NL_132381",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "10726",
        "ok": "10726",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "10726",
        "ok": "10726",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "10726",
        "ok": "10726",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "10726",
        "ok": "10726",
        "ko": "-"
    },
    "percentiles2": {
        "total": "10726",
        "ok": "10726",
        "ko": "-"
    },
    "percentiles3": {
        "total": "10726",
        "ok": "10726",
        "ko": "-"
    },
    "percentiles4": {
        "total": "10726",
        "ok": "10726",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-7e345": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111388",
path: "Animal-api sync for customer NL_111388",
pathFormatted: "req_animal-api-sync-7e345",
stats: {
    "name": "Animal-api sync for customer NL_111388",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5618",
        "ok": "5618",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5618",
        "ok": "5618",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5618",
        "ok": "5618",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5618",
        "ok": "5618",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5618",
        "ok": "5618",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5618",
        "ok": "5618",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5618",
        "ok": "5618",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d3a0e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_118547",
path: "Animal-api sync for customer NL_118547",
pathFormatted: "req_animal-api-sync-d3a0e",
stats: {
    "name": "Animal-api sync for customer NL_118547",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2873",
        "ok": "2873",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2873",
        "ok": "2873",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2873",
        "ok": "2873",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2873",
        "ok": "2873",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2873",
        "ok": "2873",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2873",
        "ok": "2873",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2873",
        "ok": "2873",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-3631b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105143",
path: "Animal-api sync for customer NL_105143",
pathFormatted: "req_animal-api-sync-3631b",
stats: {
    "name": "Animal-api sync for customer NL_105143",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "18009",
        "ok": "18009",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "18009",
        "ok": "18009",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "18009",
        "ok": "18009",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "18009",
        "ok": "18009",
        "ko": "-"
    },
    "percentiles2": {
        "total": "18009",
        "ok": "18009",
        "ko": "-"
    },
    "percentiles3": {
        "total": "18009",
        "ok": "18009",
        "ko": "-"
    },
    "percentiles4": {
        "total": "18009",
        "ok": "18009",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-f99c5": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_131859",
path: "Animal-api sync for customer NL_131859",
pathFormatted: "req_animal-api-sync-f99c5",
stats: {
    "name": "Animal-api sync for customer NL_131859",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1589",
        "ok": "1589",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1589",
        "ok": "1589",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1589",
        "ok": "1589",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1589",
        "ok": "1589",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1589",
        "ok": "1589",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1589",
        "ok": "1589",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1589",
        "ok": "1589",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-ba235": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_109254",
path: "Animal-api sync for customer NL_109254",
pathFormatted: "req_animal-api-sync-ba235",
stats: {
    "name": "Animal-api sync for customer NL_109254",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2507",
        "ok": "2507",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2507",
        "ok": "2507",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2507",
        "ok": "2507",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2507",
        "ok": "2507",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2507",
        "ok": "2507",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2507",
        "ok": "2507",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2507",
        "ok": "2507",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e87b4": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_118457",
path: "Animal-api sync for customer NL_118457",
pathFormatted: "req_animal-api-sync-e87b4",
stats: {
    "name": "Animal-api sync for customer NL_118457",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5653",
        "ok": "5653",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5653",
        "ok": "5653",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5653",
        "ok": "5653",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5653",
        "ok": "5653",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5653",
        "ok": "5653",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5653",
        "ok": "5653",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5653",
        "ok": "5653",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-84066": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_136422",
path: "Animal-api sync for customer NL_136422",
pathFormatted: "req_animal-api-sync-84066",
stats: {
    "name": "Animal-api sync for customer NL_136422",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1947",
        "ok": "1947",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1947",
        "ok": "1947",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1947",
        "ok": "1947",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1947",
        "ok": "1947",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1947",
        "ok": "1947",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1947",
        "ok": "1947",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1947",
        "ok": "1947",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-f4249": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_138300",
path: "Animal-api sync for customer NL_138300",
pathFormatted: "req_animal-api-sync-f4249",
stats: {
    "name": "Animal-api sync for customer NL_138300",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "6457",
        "ok": "6457",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "6457",
        "ok": "6457",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "6457",
        "ok": "6457",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "6457",
        "ok": "6457",
        "ko": "-"
    },
    "percentiles2": {
        "total": "6457",
        "ok": "6457",
        "ko": "-"
    },
    "percentiles3": {
        "total": "6457",
        "ok": "6457",
        "ko": "-"
    },
    "percentiles4": {
        "total": "6457",
        "ok": "6457",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-32bc3": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_103585",
path: "Animal-api sync for customer NL_103585",
pathFormatted: "req_animal-api-sync-32bc3",
stats: {
    "name": "Animal-api sync for customer NL_103585",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3339",
        "ok": "3339",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3339",
        "ok": "3339",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3339",
        "ok": "3339",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3339",
        "ok": "3339",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3339",
        "ok": "3339",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3339",
        "ok": "3339",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3339",
        "ok": "3339",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-787cb": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_143564",
path: "Animal-api sync for customer NL_143564",
pathFormatted: "req_animal-api-sync-787cb",
stats: {
    "name": "Animal-api sync for customer NL_143564",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1940",
        "ok": "1940",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1940",
        "ok": "1940",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1940",
        "ok": "1940",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1940",
        "ok": "1940",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1940",
        "ok": "1940",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1940",
        "ok": "1940",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1940",
        "ok": "1940",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-0f436": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_107376",
path: "Animal-api sync for customer NL_107376",
pathFormatted: "req_animal-api-sync-0f436",
stats: {
    "name": "Animal-api sync for customer NL_107376",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4118",
        "ok": "4118",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4118",
        "ok": "4118",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4118",
        "ok": "4118",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4118",
        "ok": "4118",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4118",
        "ok": "4118",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4118",
        "ok": "4118",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4118",
        "ok": "4118",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-20cd3": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110661",
path: "Animal-api sync for customer NL_110661",
pathFormatted: "req_animal-api-sync-20cd3",
stats: {
    "name": "Animal-api sync for customer NL_110661",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "14766",
        "ok": "14766",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "14766",
        "ok": "14766",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "14766",
        "ok": "14766",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "14766",
        "ok": "14766",
        "ko": "-"
    },
    "percentiles2": {
        "total": "14766",
        "ok": "14766",
        "ko": "-"
    },
    "percentiles3": {
        "total": "14766",
        "ok": "14766",
        "ko": "-"
    },
    "percentiles4": {
        "total": "14766",
        "ok": "14766",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-eb26b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_118799",
path: "Animal-api sync for customer NL_118799",
pathFormatted: "req_animal-api-sync-eb26b",
stats: {
    "name": "Animal-api sync for customer NL_118799",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3215",
        "ok": "3215",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3215",
        "ok": "3215",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3215",
        "ok": "3215",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3215",
        "ok": "3215",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3215",
        "ok": "3215",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3215",
        "ok": "3215",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3215",
        "ok": "3215",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c1388": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_122597",
path: "Animal-api sync for customer NL_122597",
pathFormatted: "req_animal-api-sync-c1388",
stats: {
    "name": "Animal-api sync for customer NL_122597",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "8935",
        "ok": "8935",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "8935",
        "ok": "8935",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "8935",
        "ok": "8935",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "8935",
        "ok": "8935",
        "ko": "-"
    },
    "percentiles2": {
        "total": "8935",
        "ok": "8935",
        "ko": "-"
    },
    "percentiles3": {
        "total": "8935",
        "ok": "8935",
        "ko": "-"
    },
    "percentiles4": {
        "total": "8935",
        "ok": "8935",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-28731": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_136181",
path: "Animal-api sync for customer NL_136181",
pathFormatted: "req_animal-api-sync-28731",
stats: {
    "name": "Animal-api sync for customer NL_136181",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "7544",
        "ok": "7544",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "7544",
        "ok": "7544",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "7544",
        "ok": "7544",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "7544",
        "ok": "7544",
        "ko": "-"
    },
    "percentiles2": {
        "total": "7544",
        "ok": "7544",
        "ko": "-"
    },
    "percentiles3": {
        "total": "7544",
        "ok": "7544",
        "ko": "-"
    },
    "percentiles4": {
        "total": "7544",
        "ok": "7544",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-da9fa": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_212118",
path: "Animal-api sync for customer NL_212118",
pathFormatted: "req_animal-api-sync-da9fa",
stats: {
    "name": "Animal-api sync for customer NL_212118",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2591",
        "ok": "2591",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2591",
        "ok": "2591",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2591",
        "ok": "2591",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2591",
        "ok": "2591",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2591",
        "ok": "2591",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2591",
        "ok": "2591",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2591",
        "ok": "2591",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-2b764": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111875",
path: "Animal-api sync for customer NL_111875",
pathFormatted: "req_animal-api-sync-2b764",
stats: {
    "name": "Animal-api sync for customer NL_111875",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "12197",
        "ok": "12197",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "12197",
        "ok": "12197",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "12197",
        "ok": "12197",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "12197",
        "ok": "12197",
        "ko": "-"
    },
    "percentiles2": {
        "total": "12197",
        "ok": "12197",
        "ko": "-"
    },
    "percentiles3": {
        "total": "12197",
        "ok": "12197",
        "ko": "-"
    },
    "percentiles4": {
        "total": "12197",
        "ok": "12197",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-56095": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_125359",
path: "Animal-api sync for customer NL_125359",
pathFormatted: "req_animal-api-sync-56095",
stats: {
    "name": "Animal-api sync for customer NL_125359",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3165",
        "ok": "3165",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3165",
        "ok": "3165",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3165",
        "ok": "3165",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3165",
        "ok": "3165",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3165",
        "ok": "3165",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3165",
        "ok": "3165",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3165",
        "ok": "3165",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-7c52f": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_112020",
path: "Animal-api sync for customer NL_112020",
pathFormatted: "req_animal-api-sync-7c52f",
stats: {
    "name": "Animal-api sync for customer NL_112020",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "8185",
        "ok": "8185",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "8185",
        "ok": "8185",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "8185",
        "ok": "8185",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "8185",
        "ok": "8185",
        "ko": "-"
    },
    "percentiles2": {
        "total": "8185",
        "ok": "8185",
        "ko": "-"
    },
    "percentiles3": {
        "total": "8185",
        "ok": "8185",
        "ko": "-"
    },
    "percentiles4": {
        "total": "8185",
        "ok": "8185",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-1586b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_113408",
path: "Animal-api sync for customer NL_113408",
pathFormatted: "req_animal-api-sync-1586b",
stats: {
    "name": "Animal-api sync for customer NL_113408",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "11797",
        "ok": "11797",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "11797",
        "ok": "11797",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "11797",
        "ok": "11797",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "11797",
        "ok": "11797",
        "ko": "-"
    },
    "percentiles2": {
        "total": "11797",
        "ok": "11797",
        "ko": "-"
    },
    "percentiles3": {
        "total": "11797",
        "ok": "11797",
        "ko": "-"
    },
    "percentiles4": {
        "total": "11797",
        "ok": "11797",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-bca62": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_112986",
path: "Animal-api sync for customer NL_112986",
pathFormatted: "req_animal-api-sync-bca62",
stats: {
    "name": "Animal-api sync for customer NL_112986",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "29178",
        "ok": "29178",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "29178",
        "ok": "29178",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "29178",
        "ok": "29178",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "29178",
        "ok": "29178",
        "ko": "-"
    },
    "percentiles2": {
        "total": "29178",
        "ok": "29178",
        "ko": "-"
    },
    "percentiles3": {
        "total": "29178",
        "ok": "29178",
        "ko": "-"
    },
    "percentiles4": {
        "total": "29178",
        "ok": "29178",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d8a50": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_109324",
path: "Animal-api sync for customer NL_109324",
pathFormatted: "req_animal-api-sync-d8a50",
stats: {
    "name": "Animal-api sync for customer NL_109324",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "10364",
        "ok": "10364",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "10364",
        "ok": "10364",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "10364",
        "ok": "10364",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "10364",
        "ok": "10364",
        "ko": "-"
    },
    "percentiles2": {
        "total": "10364",
        "ok": "10364",
        "ko": "-"
    },
    "percentiles3": {
        "total": "10364",
        "ok": "10364",
        "ko": "-"
    },
    "percentiles4": {
        "total": "10364",
        "ok": "10364",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d4b4d": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_123556",
path: "Animal-api sync for customer NL_123556",
pathFormatted: "req_animal-api-sync-d4b4d",
stats: {
    "name": "Animal-api sync for customer NL_123556",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "16355",
        "ok": "16355",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "16355",
        "ok": "16355",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "16355",
        "ok": "16355",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "16355",
        "ok": "16355",
        "ko": "-"
    },
    "percentiles2": {
        "total": "16355",
        "ok": "16355",
        "ko": "-"
    },
    "percentiles3": {
        "total": "16355",
        "ok": "16355",
        "ko": "-"
    },
    "percentiles4": {
        "total": "16355",
        "ok": "16355",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-bc7d0": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_151016",
path: "Animal-api sync for customer BE_151016",
pathFormatted: "req_animal-api-sync-bc7d0",
stats: {
    "name": "Animal-api sync for customer BE_151016",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4038",
        "ok": "4038",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4038",
        "ok": "4038",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4038",
        "ok": "4038",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4038",
        "ok": "4038",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4038",
        "ok": "4038",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4038",
        "ok": "4038",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4038",
        "ok": "4038",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a0b51": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_118379",
path: "Animal-api sync for customer NL_118379",
pathFormatted: "req_animal-api-sync-a0b51",
stats: {
    "name": "Animal-api sync for customer NL_118379",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "24732",
        "ok": "24732",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "24732",
        "ok": "24732",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "24732",
        "ok": "24732",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "24732",
        "ok": "24732",
        "ko": "-"
    },
    "percentiles2": {
        "total": "24732",
        "ok": "24732",
        "ko": "-"
    },
    "percentiles3": {
        "total": "24732",
        "ok": "24732",
        "ko": "-"
    },
    "percentiles4": {
        "total": "24732",
        "ok": "24732",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b6f25": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_119444",
path: "Animal-api sync for customer NL_119444",
pathFormatted: "req_animal-api-sync-b6f25",
stats: {
    "name": "Animal-api sync for customer NL_119444",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5177",
        "ok": "5177",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5177",
        "ok": "5177",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5177",
        "ok": "5177",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5177",
        "ok": "5177",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5177",
        "ok": "5177",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5177",
        "ok": "5177",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5177",
        "ok": "5177",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b9666": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_207943",
path: "Animal-api sync for customer BE_207943",
pathFormatted: "req_animal-api-sync-b9666",
stats: {
    "name": "Animal-api sync for customer BE_207943",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "34381",
        "ok": "34381",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "34381",
        "ok": "34381",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "34381",
        "ok": "34381",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "34381",
        "ok": "34381",
        "ko": "-"
    },
    "percentiles2": {
        "total": "34381",
        "ok": "34381",
        "ko": "-"
    },
    "percentiles3": {
        "total": "34381",
        "ok": "34381",
        "ko": "-"
    },
    "percentiles4": {
        "total": "34381",
        "ok": "34381",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-662d1": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_211608",
path: "Animal-api sync for customer BE_211608",
pathFormatted: "req_animal-api-sync-662d1",
stats: {
    "name": "Animal-api sync for customer BE_211608",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4651",
        "ok": "4651",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4651",
        "ok": "4651",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4651",
        "ok": "4651",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4651",
        "ok": "4651",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4651",
        "ok": "4651",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4651",
        "ok": "4651",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4651",
        "ok": "4651",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-58eb2": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_125881",
path: "Animal-api sync for customer NL_125881",
pathFormatted: "req_animal-api-sync-58eb2",
stats: {
    "name": "Animal-api sync for customer NL_125881",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2177",
        "ok": "2177",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2177",
        "ok": "2177",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2177",
        "ok": "2177",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2177",
        "ok": "2177",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2177",
        "ok": "2177",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2177",
        "ok": "2177",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2177",
        "ok": "2177",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-cd728": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117817",
path: "Animal-api sync for customer NL_117817",
pathFormatted: "req_animal-api-sync-cd728",
stats: {
    "name": "Animal-api sync for customer NL_117817",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4729",
        "ok": "4729",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4729",
        "ok": "4729",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4729",
        "ok": "4729",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4729",
        "ok": "4729",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4729",
        "ok": "4729",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4729",
        "ok": "4729",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4729",
        "ok": "4729",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d0381": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_126231",
path: "Animal-api sync for customer NL_126231",
pathFormatted: "req_animal-api-sync-d0381",
stats: {
    "name": "Animal-api sync for customer NL_126231",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5047",
        "ok": "5047",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5047",
        "ok": "5047",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5047",
        "ok": "5047",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5047",
        "ok": "5047",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5047",
        "ok": "5047",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5047",
        "ok": "5047",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5047",
        "ok": "5047",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-1164c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104369",
path: "Animal-api sync for customer NL_104369",
pathFormatted: "req_animal-api-sync-1164c",
stats: {
    "name": "Animal-api sync for customer NL_104369",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5992",
        "ok": "5992",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5992",
        "ok": "5992",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5992",
        "ok": "5992",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5992",
        "ok": "5992",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5992",
        "ok": "5992",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5992",
        "ok": "5992",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5992",
        "ok": "5992",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c2774": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_130077",
path: "Animal-api sync for customer NL_130077",
pathFormatted: "req_animal-api-sync-c2774",
stats: {
    "name": "Animal-api sync for customer NL_130077",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "6469",
        "ok": "6469",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "6469",
        "ok": "6469",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "6469",
        "ok": "6469",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "6469",
        "ok": "6469",
        "ko": "-"
    },
    "percentiles2": {
        "total": "6469",
        "ok": "6469",
        "ko": "-"
    },
    "percentiles3": {
        "total": "6469",
        "ok": "6469",
        "ko": "-"
    },
    "percentiles4": {
        "total": "6469",
        "ok": "6469",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-da2a1": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105775",
path: "Animal-api sync for customer NL_105775",
pathFormatted: "req_animal-api-sync-da2a1",
stats: {
    "name": "Animal-api sync for customer NL_105775",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "24126",
        "ok": "24126",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "24126",
        "ok": "24126",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "24126",
        "ok": "24126",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "24126",
        "ok": "24126",
        "ko": "-"
    },
    "percentiles2": {
        "total": "24126",
        "ok": "24126",
        "ko": "-"
    },
    "percentiles3": {
        "total": "24126",
        "ok": "24126",
        "ko": "-"
    },
    "percentiles4": {
        "total": "24126",
        "ok": "24126",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-12657": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_212029",
path: "Animal-api sync for customer BE_212029",
pathFormatted: "req_animal-api-sync-12657",
stats: {
    "name": "Animal-api sync for customer BE_212029",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "8714",
        "ok": "8714",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "8714",
        "ok": "8714",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "8714",
        "ok": "8714",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "8714",
        "ok": "8714",
        "ko": "-"
    },
    "percentiles2": {
        "total": "8714",
        "ok": "8714",
        "ko": "-"
    },
    "percentiles3": {
        "total": "8714",
        "ok": "8714",
        "ko": "-"
    },
    "percentiles4": {
        "total": "8714",
        "ok": "8714",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a231f": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_119073",
path: "Animal-api sync for customer NL_119073",
pathFormatted: "req_animal-api-sync-a231f",
stats: {
    "name": "Animal-api sync for customer NL_119073",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "6011",
        "ok": "6011",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "6011",
        "ok": "6011",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "6011",
        "ok": "6011",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "6011",
        "ok": "6011",
        "ko": "-"
    },
    "percentiles2": {
        "total": "6011",
        "ok": "6011",
        "ko": "-"
    },
    "percentiles3": {
        "total": "6011",
        "ok": "6011",
        "ko": "-"
    },
    "percentiles4": {
        "total": "6011",
        "ok": "6011",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a5dcb": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_113563",
path: "Animal-api sync for customer NL_113563",
pathFormatted: "req_animal-api-sync-a5dcb",
stats: {
    "name": "Animal-api sync for customer NL_113563",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "9863",
        "ok": "9863",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "9863",
        "ok": "9863",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "9863",
        "ok": "9863",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "9863",
        "ok": "9863",
        "ko": "-"
    },
    "percentiles2": {
        "total": "9863",
        "ok": "9863",
        "ko": "-"
    },
    "percentiles3": {
        "total": "9863",
        "ok": "9863",
        "ko": "-"
    },
    "percentiles4": {
        "total": "9863",
        "ok": "9863",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-0d921": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_124334",
path: "Animal-api sync for customer NL_124334",
pathFormatted: "req_animal-api-sync-0d921",
stats: {
    "name": "Animal-api sync for customer NL_124334",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21959",
        "ok": "21959",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "21959",
        "ok": "21959",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "21959",
        "ok": "21959",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "21959",
        "ok": "21959",
        "ko": "-"
    },
    "percentiles2": {
        "total": "21959",
        "ok": "21959",
        "ko": "-"
    },
    "percentiles3": {
        "total": "21959",
        "ok": "21959",
        "ko": "-"
    },
    "percentiles4": {
        "total": "21959",
        "ok": "21959",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a5308": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_129853",
path: "Animal-api sync for customer NL_129853",
pathFormatted: "req_animal-api-sync-a5308",
stats: {
    "name": "Animal-api sync for customer NL_129853",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4745",
        "ok": "4745",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4745",
        "ok": "4745",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4745",
        "ok": "4745",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4745",
        "ok": "4745",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4745",
        "ok": "4745",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4745",
        "ok": "4745",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4745",
        "ok": "4745",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a8587": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_112236",
path: "Animal-api sync for customer NL_112236",
pathFormatted: "req_animal-api-sync-a8587",
stats: {
    "name": "Animal-api sync for customer NL_112236",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "15647",
        "ok": "15647",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "15647",
        "ok": "15647",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "15647",
        "ok": "15647",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "15647",
        "ok": "15647",
        "ko": "-"
    },
    "percentiles2": {
        "total": "15647",
        "ok": "15647",
        "ko": "-"
    },
    "percentiles3": {
        "total": "15647",
        "ok": "15647",
        "ko": "-"
    },
    "percentiles4": {
        "total": "15647",
        "ok": "15647",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c1a9b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105397",
path: "Animal-api sync for customer NL_105397",
pathFormatted: "req_animal-api-sync-c1a9b",
stats: {
    "name": "Animal-api sync for customer NL_105397",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "16947",
        "ok": "16947",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "16947",
        "ok": "16947",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "16947",
        "ok": "16947",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "16947",
        "ok": "16947",
        "ko": "-"
    },
    "percentiles2": {
        "total": "16947",
        "ok": "16947",
        "ko": "-"
    },
    "percentiles3": {
        "total": "16947",
        "ok": "16947",
        "ko": "-"
    },
    "percentiles4": {
        "total": "16947",
        "ok": "16947",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-f47d9": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_162268",
path: "Animal-api sync for customer NL_162268",
pathFormatted: "req_animal-api-sync-f47d9",
stats: {
    "name": "Animal-api sync for customer NL_162268",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "19569",
        "ok": "19569",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "19569",
        "ok": "19569",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "19569",
        "ok": "19569",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "19569",
        "ok": "19569",
        "ko": "-"
    },
    "percentiles2": {
        "total": "19569",
        "ok": "19569",
        "ko": "-"
    },
    "percentiles3": {
        "total": "19569",
        "ok": "19569",
        "ko": "-"
    },
    "percentiles4": {
        "total": "19569",
        "ok": "19569",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-7141e": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_195066",
path: "Animal-api sync for customer BE_195066",
pathFormatted: "req_animal-api-sync-7141e",
stats: {
    "name": "Animal-api sync for customer BE_195066",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "42672",
        "ok": "42672",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "42672",
        "ok": "42672",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "42672",
        "ok": "42672",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "42672",
        "ok": "42672",
        "ko": "-"
    },
    "percentiles2": {
        "total": "42672",
        "ok": "42672",
        "ko": "-"
    },
    "percentiles3": {
        "total": "42672",
        "ok": "42672",
        "ko": "-"
    },
    "percentiles4": {
        "total": "42672",
        "ok": "42672",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-9fb14": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_115364",
path: "Animal-api sync for customer NL_115364",
pathFormatted: "req_animal-api-sync-9fb14",
stats: {
    "name": "Animal-api sync for customer NL_115364",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "19728",
        "ok": "19728",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "19728",
        "ok": "19728",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "19728",
        "ok": "19728",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "19728",
        "ok": "19728",
        "ko": "-"
    },
    "percentiles2": {
        "total": "19728",
        "ok": "19728",
        "ko": "-"
    },
    "percentiles3": {
        "total": "19728",
        "ok": "19728",
        "ko": "-"
    },
    "percentiles4": {
        "total": "19728",
        "ok": "19728",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-29ae3": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_107774",
path: "Animal-api sync for customer NL_107774",
pathFormatted: "req_animal-api-sync-29ae3",
stats: {
    "name": "Animal-api sync for customer NL_107774",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "15547",
        "ok": "15547",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "15547",
        "ok": "15547",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "15547",
        "ok": "15547",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "15547",
        "ok": "15547",
        "ko": "-"
    },
    "percentiles2": {
        "total": "15547",
        "ok": "15547",
        "ko": "-"
    },
    "percentiles3": {
        "total": "15547",
        "ok": "15547",
        "ko": "-"
    },
    "percentiles4": {
        "total": "15547",
        "ok": "15547",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e24f9": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_125084",
path: "Animal-api sync for customer NL_125084",
pathFormatted: "req_animal-api-sync-e24f9",
stats: {
    "name": "Animal-api sync for customer NL_125084",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3929",
        "ok": "3929",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3929",
        "ok": "3929",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3929",
        "ok": "3929",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3929",
        "ok": "3929",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3929",
        "ok": "3929",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3929",
        "ok": "3929",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3929",
        "ok": "3929",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-6bcf1": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_130692",
path: "Animal-api sync for customer NL_130692",
pathFormatted: "req_animal-api-sync-6bcf1",
stats: {
    "name": "Animal-api sync for customer NL_130692",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4150",
        "ok": "4150",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4150",
        "ok": "4150",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4150",
        "ok": "4150",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4150",
        "ok": "4150",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4150",
        "ok": "4150",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4150",
        "ok": "4150",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4150",
        "ok": "4150",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-42cac": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117138",
path: "Animal-api sync for customer NL_117138",
pathFormatted: "req_animal-api-sync-42cac",
stats: {
    "name": "Animal-api sync for customer NL_117138",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "6027",
        "ok": "6027",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "6027",
        "ok": "6027",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "6027",
        "ok": "6027",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "6027",
        "ok": "6027",
        "ko": "-"
    },
    "percentiles2": {
        "total": "6027",
        "ok": "6027",
        "ko": "-"
    },
    "percentiles3": {
        "total": "6027",
        "ok": "6027",
        "ko": "-"
    },
    "percentiles4": {
        "total": "6027",
        "ok": "6027",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-7ddaa": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_127532",
path: "Animal-api sync for customer NL_127532",
pathFormatted: "req_animal-api-sync-7ddaa",
stats: {
    "name": "Animal-api sync for customer NL_127532",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "6614",
        "ok": "6614",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "6614",
        "ok": "6614",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "6614",
        "ok": "6614",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "6614",
        "ok": "6614",
        "ko": "-"
    },
    "percentiles2": {
        "total": "6614",
        "ok": "6614",
        "ko": "-"
    },
    "percentiles3": {
        "total": "6614",
        "ok": "6614",
        "ko": "-"
    },
    "percentiles4": {
        "total": "6614",
        "ok": "6614",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-35067": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_145405",
path: "Animal-api sync for customer BE_145405",
pathFormatted: "req_animal-api-sync-35067",
stats: {
    "name": "Animal-api sync for customer BE_145405",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "14965",
        "ok": "14965",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "14965",
        "ok": "14965",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "14965",
        "ok": "14965",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "14965",
        "ok": "14965",
        "ko": "-"
    },
    "percentiles2": {
        "total": "14965",
        "ok": "14965",
        "ko": "-"
    },
    "percentiles3": {
        "total": "14965",
        "ok": "14965",
        "ko": "-"
    },
    "percentiles4": {
        "total": "14965",
        "ok": "14965",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-2e40c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_131445",
path: "Animal-api sync for customer NL_131445",
pathFormatted: "req_animal-api-sync-2e40c",
stats: {
    "name": "Animal-api sync for customer NL_131445",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "6923",
        "ok": "6923",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "6923",
        "ok": "6923",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "6923",
        "ok": "6923",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "6923",
        "ok": "6923",
        "ko": "-"
    },
    "percentiles2": {
        "total": "6923",
        "ok": "6923",
        "ko": "-"
    },
    "percentiles3": {
        "total": "6923",
        "ok": "6923",
        "ko": "-"
    },
    "percentiles4": {
        "total": "6923",
        "ok": "6923",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-3eb24": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_103773",
path: "Animal-api sync for customer NL_103773",
pathFormatted: "req_animal-api-sync-3eb24",
stats: {
    "name": "Animal-api sync for customer NL_103773",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "18744",
        "ok": "18744",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "18744",
        "ok": "18744",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "18744",
        "ok": "18744",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "18744",
        "ok": "18744",
        "ko": "-"
    },
    "percentiles2": {
        "total": "18744",
        "ok": "18744",
        "ko": "-"
    },
    "percentiles3": {
        "total": "18744",
        "ok": "18744",
        "ko": "-"
    },
    "percentiles4": {
        "total": "18744",
        "ok": "18744",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-db524": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104670",
path: "Animal-api sync for customer NL_104670",
pathFormatted: "req_animal-api-sync-db524",
stats: {
    "name": "Animal-api sync for customer NL_104670",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "maxResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "meanResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles2": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles3": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles4": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-8824d": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_129197",
path: "Animal-api sync for customer NL_129197",
pathFormatted: "req_animal-api-sync-8824d",
stats: {
    "name": "Animal-api sync for customer NL_129197",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "maxResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "meanResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles2": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles3": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles4": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-d2007": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_214023",
path: "Animal-api sync for customer BE_214023",
pathFormatted: "req_animal-api-sync-d2007",
stats: {
    "name": "Animal-api sync for customer BE_214023",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "33247",
        "ok": "33247",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "33247",
        "ok": "33247",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "33247",
        "ok": "33247",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "33247",
        "ok": "33247",
        "ko": "-"
    },
    "percentiles2": {
        "total": "33247",
        "ok": "33247",
        "ko": "-"
    },
    "percentiles3": {
        "total": "33247",
        "ok": "33247",
        "ko": "-"
    },
    "percentiles4": {
        "total": "33247",
        "ok": "33247",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e79fc": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_114849",
path: "Animal-api sync for customer NL_114849",
pathFormatted: "req_animal-api-sync-e79fc",
stats: {
    "name": "Animal-api sync for customer NL_114849",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "10457",
        "ok": "10457",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "10457",
        "ok": "10457",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "10457",
        "ok": "10457",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "10457",
        "ok": "10457",
        "ko": "-"
    },
    "percentiles2": {
        "total": "10457",
        "ok": "10457",
        "ko": "-"
    },
    "percentiles3": {
        "total": "10457",
        "ok": "10457",
        "ko": "-"
    },
    "percentiles4": {
        "total": "10457",
        "ok": "10457",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-6b259": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_118526",
path: "Animal-api sync for customer NL_118526",
pathFormatted: "req_animal-api-sync-6b259",
stats: {
    "name": "Animal-api sync for customer NL_118526",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2499",
        "ok": "2499",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2499",
        "ok": "2499",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2499",
        "ok": "2499",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2499",
        "ok": "2499",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2499",
        "ok": "2499",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2499",
        "ok": "2499",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2499",
        "ok": "2499",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-1f585": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_123243",
path: "Animal-api sync for customer NL_123243",
pathFormatted: "req_animal-api-sync-1f585",
stats: {
    "name": "Animal-api sync for customer NL_123243",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "9674",
        "ok": "9674",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "9674",
        "ok": "9674",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "9674",
        "ok": "9674",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "9674",
        "ok": "9674",
        "ko": "-"
    },
    "percentiles2": {
        "total": "9674",
        "ok": "9674",
        "ko": "-"
    },
    "percentiles3": {
        "total": "9674",
        "ok": "9674",
        "ko": "-"
    },
    "percentiles4": {
        "total": "9674",
        "ok": "9674",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-360a2": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_120728",
path: "Animal-api sync for customer NL_120728",
pathFormatted: "req_animal-api-sync-360a2",
stats: {
    "name": "Animal-api sync for customer NL_120728",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "maxResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "meanResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles2": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles3": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles4": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-964ea": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_131647",
path: "Animal-api sync for customer NL_131647",
pathFormatted: "req_animal-api-sync-964ea",
stats: {
    "name": "Animal-api sync for customer NL_131647",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "9004",
        "ok": "9004",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "9004",
        "ok": "9004",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "9004",
        "ok": "9004",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "9004",
        "ok": "9004",
        "ko": "-"
    },
    "percentiles2": {
        "total": "9004",
        "ok": "9004",
        "ko": "-"
    },
    "percentiles3": {
        "total": "9004",
        "ok": "9004",
        "ko": "-"
    },
    "percentiles4": {
        "total": "9004",
        "ok": "9004",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-90a7a": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_158870",
path: "Animal-api sync for customer NL_158870",
pathFormatted: "req_animal-api-sync-90a7a",
stats: {
    "name": "Animal-api sync for customer NL_158870",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "maxResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "meanResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles2": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles3": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles4": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-ee74d": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_149842",
path: "Animal-api sync for customer BE_149842",
pathFormatted: "req_animal-api-sync-ee74d",
stats: {
    "name": "Animal-api sync for customer BE_149842",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "11802",
        "ok": "11802",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "11802",
        "ok": "11802",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "11802",
        "ok": "11802",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "11802",
        "ok": "11802",
        "ko": "-"
    },
    "percentiles2": {
        "total": "11802",
        "ok": "11802",
        "ko": "-"
    },
    "percentiles3": {
        "total": "11802",
        "ok": "11802",
        "ko": "-"
    },
    "percentiles4": {
        "total": "11802",
        "ok": "11802",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-edbd5": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_156716",
path: "Animal-api sync for customer NL_156716",
pathFormatted: "req_animal-api-sync-edbd5",
stats: {
    "name": "Animal-api sync for customer NL_156716",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "maxResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "meanResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles2": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles3": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles4": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-b6490": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_128966",
path: "Animal-api sync for customer NL_128966",
pathFormatted: "req_animal-api-sync-b6490",
stats: {
    "name": "Animal-api sync for customer NL_128966",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "10322",
        "ok": "10322",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "10322",
        "ok": "10322",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "10322",
        "ok": "10322",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "10322",
        "ok": "10322",
        "ko": "-"
    },
    "percentiles2": {
        "total": "10322",
        "ok": "10322",
        "ko": "-"
    },
    "percentiles3": {
        "total": "10322",
        "ok": "10322",
        "ko": "-"
    },
    "percentiles4": {
        "total": "10322",
        "ok": "10322",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-5eb0d": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_129758",
path: "Animal-api sync for customer NL_129758",
pathFormatted: "req_animal-api-sync-5eb0d",
stats: {
    "name": "Animal-api sync for customer NL_129758",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "17730",
        "ok": "17730",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "17730",
        "ok": "17730",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "17730",
        "ok": "17730",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "17730",
        "ok": "17730",
        "ko": "-"
    },
    "percentiles2": {
        "total": "17730",
        "ok": "17730",
        "ko": "-"
    },
    "percentiles3": {
        "total": "17730",
        "ok": "17730",
        "ko": "-"
    },
    "percentiles4": {
        "total": "17730",
        "ok": "17730",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-7665a": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_107608",
path: "Animal-api sync for customer NL_107608",
pathFormatted: "req_animal-api-sync-7665a",
stats: {
    "name": "Animal-api sync for customer NL_107608",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "10594",
        "ok": "10594",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "10594",
        "ok": "10594",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "10594",
        "ok": "10594",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "10594",
        "ok": "10594",
        "ko": "-"
    },
    "percentiles2": {
        "total": "10594",
        "ok": "10594",
        "ko": "-"
    },
    "percentiles3": {
        "total": "10594",
        "ok": "10594",
        "ko": "-"
    },
    "percentiles4": {
        "total": "10594",
        "ok": "10594",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-352f1": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_120975",
path: "Animal-api sync for customer NL_120975",
pathFormatted: "req_animal-api-sync-352f1",
stats: {
    "name": "Animal-api sync for customer NL_120975",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "56381",
        "ok": "56381",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "56381",
        "ok": "56381",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "56381",
        "ok": "56381",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "56381",
        "ok": "56381",
        "ko": "-"
    },
    "percentiles2": {
        "total": "56381",
        "ok": "56381",
        "ko": "-"
    },
    "percentiles3": {
        "total": "56381",
        "ok": "56381",
        "ko": "-"
    },
    "percentiles4": {
        "total": "56381",
        "ok": "56381",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a1b1c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105501",
path: "Animal-api sync for customer NL_105501",
pathFormatted: "req_animal-api-sync-a1b1c",
stats: {
    "name": "Animal-api sync for customer NL_105501",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "10196",
        "ok": "10196",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "10196",
        "ok": "10196",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "10196",
        "ok": "10196",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "10196",
        "ok": "10196",
        "ko": "-"
    },
    "percentiles2": {
        "total": "10196",
        "ok": "10196",
        "ko": "-"
    },
    "percentiles3": {
        "total": "10196",
        "ok": "10196",
        "ko": "-"
    },
    "percentiles4": {
        "total": "10196",
        "ok": "10196",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-558ec": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_115428",
path: "Animal-api sync for customer NL_115428",
pathFormatted: "req_animal-api-sync-558ec",
stats: {
    "name": "Animal-api sync for customer NL_115428",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2128",
        "ok": "2128",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2128",
        "ok": "2128",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2128",
        "ok": "2128",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2128",
        "ok": "2128",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2128",
        "ok": "2128",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2128",
        "ok": "2128",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2128",
        "ok": "2128",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e201e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110096",
path: "Animal-api sync for customer NL_110096",
pathFormatted: "req_animal-api-sync-e201e",
stats: {
    "name": "Animal-api sync for customer NL_110096",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "27593",
        "ok": "27593",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "27593",
        "ok": "27593",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "27593",
        "ok": "27593",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "27593",
        "ok": "27593",
        "ko": "-"
    },
    "percentiles2": {
        "total": "27593",
        "ok": "27593",
        "ko": "-"
    },
    "percentiles3": {
        "total": "27593",
        "ok": "27593",
        "ko": "-"
    },
    "percentiles4": {
        "total": "27593",
        "ok": "27593",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-66a67": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_115297",
path: "Animal-api sync for customer NL_115297",
pathFormatted: "req_animal-api-sync-66a67",
stats: {
    "name": "Animal-api sync for customer NL_115297",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "9810",
        "ok": "9810",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "9810",
        "ok": "9810",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "9810",
        "ok": "9810",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "9810",
        "ok": "9810",
        "ko": "-"
    },
    "percentiles2": {
        "total": "9810",
        "ok": "9810",
        "ko": "-"
    },
    "percentiles3": {
        "total": "9810",
        "ok": "9810",
        "ko": "-"
    },
    "percentiles4": {
        "total": "9810",
        "ok": "9810",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e614b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_114481",
path: "Animal-api sync for customer NL_114481",
pathFormatted: "req_animal-api-sync-e614b",
stats: {
    "name": "Animal-api sync for customer NL_114481",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "9286",
        "ok": "9286",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "9286",
        "ok": "9286",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "9286",
        "ok": "9286",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "9286",
        "ok": "9286",
        "ko": "-"
    },
    "percentiles2": {
        "total": "9286",
        "ok": "9286",
        "ko": "-"
    },
    "percentiles3": {
        "total": "9286",
        "ok": "9286",
        "ko": "-"
    },
    "percentiles4": {
        "total": "9286",
        "ok": "9286",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a326b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_109292",
path: "Animal-api sync for customer NL_109292",
pathFormatted: "req_animal-api-sync-a326b",
stats: {
    "name": "Animal-api sync for customer NL_109292",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "12965",
        "ok": "12965",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "12965",
        "ok": "12965",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "12965",
        "ok": "12965",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "12965",
        "ok": "12965",
        "ko": "-"
    },
    "percentiles2": {
        "total": "12965",
        "ok": "12965",
        "ko": "-"
    },
    "percentiles3": {
        "total": "12965",
        "ok": "12965",
        "ko": "-"
    },
    "percentiles4": {
        "total": "12965",
        "ok": "12965",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-ec201": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105082",
path: "Animal-api sync for customer NL_105082",
pathFormatted: "req_animal-api-sync-ec201",
stats: {
    "name": "Animal-api sync for customer NL_105082",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "maxResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "meanResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles4": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-c9dab": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_122464",
path: "Animal-api sync for customer NL_122464",
pathFormatted: "req_animal-api-sync-c9dab",
stats: {
    "name": "Animal-api sync for customer NL_122464",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "37640",
        "ok": "37640",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "37640",
        "ok": "37640",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "37640",
        "ok": "37640",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "37640",
        "ok": "37640",
        "ko": "-"
    },
    "percentiles2": {
        "total": "37640",
        "ok": "37640",
        "ko": "-"
    },
    "percentiles3": {
        "total": "37640",
        "ok": "37640",
        "ko": "-"
    },
    "percentiles4": {
        "total": "37640",
        "ok": "37640",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-2092d": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_150518",
path: "Animal-api sync for customer BE_150518",
pathFormatted: "req_animal-api-sync-2092d",
stats: {
    "name": "Animal-api sync for customer BE_150518",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "9848",
        "ok": "9848",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "9848",
        "ok": "9848",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "9848",
        "ok": "9848",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "9848",
        "ok": "9848",
        "ko": "-"
    },
    "percentiles2": {
        "total": "9848",
        "ok": "9848",
        "ko": "-"
    },
    "percentiles3": {
        "total": "9848",
        "ok": "9848",
        "ko": "-"
    },
    "percentiles4": {
        "total": "9848",
        "ok": "9848",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-cdf93": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_118194",
path: "Animal-api sync for customer NL_118194",
pathFormatted: "req_animal-api-sync-cdf93",
stats: {
    "name": "Animal-api sync for customer NL_118194",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "29740",
        "ok": "29740",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "29740",
        "ok": "29740",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "29740",
        "ok": "29740",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "29740",
        "ok": "29740",
        "ko": "-"
    },
    "percentiles2": {
        "total": "29740",
        "ok": "29740",
        "ko": "-"
    },
    "percentiles3": {
        "total": "29740",
        "ok": "29740",
        "ko": "-"
    },
    "percentiles4": {
        "total": "29740",
        "ok": "29740",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-ac6d4": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_121952",
path: "Animal-api sync for customer NL_121952",
pathFormatted: "req_animal-api-sync-ac6d4",
stats: {
    "name": "Animal-api sync for customer NL_121952",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "30389",
        "ok": "30389",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "30389",
        "ok": "30389",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "30389",
        "ok": "30389",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "30389",
        "ok": "30389",
        "ko": "-"
    },
    "percentiles2": {
        "total": "30389",
        "ok": "30389",
        "ko": "-"
    },
    "percentiles3": {
        "total": "30389",
        "ok": "30389",
        "ko": "-"
    },
    "percentiles4": {
        "total": "30389",
        "ok": "30389",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-0ec02": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_114079",
path: "Animal-api sync for customer NL_114079",
pathFormatted: "req_animal-api-sync-0ec02",
stats: {
    "name": "Animal-api sync for customer NL_114079",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4902",
        "ok": "4902",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4902",
        "ok": "4902",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4902",
        "ok": "4902",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4902",
        "ok": "4902",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4902",
        "ok": "4902",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4902",
        "ok": "4902",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4902",
        "ok": "4902",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-2901a": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_136544",
path: "Animal-api sync for customer NL_136544",
pathFormatted: "req_animal-api-sync-2901a",
stats: {
    "name": "Animal-api sync for customer NL_136544",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "8784",
        "ok": "8784",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "8784",
        "ok": "8784",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "8784",
        "ok": "8784",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "8784",
        "ok": "8784",
        "ko": "-"
    },
    "percentiles2": {
        "total": "8784",
        "ok": "8784",
        "ko": "-"
    },
    "percentiles3": {
        "total": "8784",
        "ok": "8784",
        "ko": "-"
    },
    "percentiles4": {
        "total": "8784",
        "ok": "8784",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-1a27a": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_120522",
path: "Animal-api sync for customer NL_120522",
pathFormatted: "req_animal-api-sync-1a27a",
stats: {
    "name": "Animal-api sync for customer NL_120522",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "12626",
        "ok": "12626",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "12626",
        "ok": "12626",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "12626",
        "ok": "12626",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "12626",
        "ok": "12626",
        "ko": "-"
    },
    "percentiles2": {
        "total": "12626",
        "ok": "12626",
        "ko": "-"
    },
    "percentiles3": {
        "total": "12626",
        "ok": "12626",
        "ko": "-"
    },
    "percentiles4": {
        "total": "12626",
        "ok": "12626",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-317c3": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_132801",
path: "Animal-api sync for customer NL_132801",
pathFormatted: "req_animal-api-sync-317c3",
stats: {
    "name": "Animal-api sync for customer NL_132801",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "6911",
        "ok": "6911",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "6911",
        "ok": "6911",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "6911",
        "ok": "6911",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "6911",
        "ok": "6911",
        "ko": "-"
    },
    "percentiles2": {
        "total": "6911",
        "ok": "6911",
        "ko": "-"
    },
    "percentiles3": {
        "total": "6911",
        "ok": "6911",
        "ko": "-"
    },
    "percentiles4": {
        "total": "6911",
        "ok": "6911",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-83baa": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_114916",
path: "Animal-api sync for customer NL_114916",
pathFormatted: "req_animal-api-sync-83baa",
stats: {
    "name": "Animal-api sync for customer NL_114916",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60007",
        "ok": "-",
        "ko": "60007"
    },
    "maxResponseTime": {
        "total": "60007",
        "ok": "-",
        "ko": "60007"
    },
    "meanResponseTime": {
        "total": "60007",
        "ok": "-",
        "ko": "60007"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60007",
        "ok": "-",
        "ko": "60007"
    },
    "percentiles2": {
        "total": "60007",
        "ok": "-",
        "ko": "60007"
    },
    "percentiles3": {
        "total": "60007",
        "ok": "-",
        "ko": "60007"
    },
    "percentiles4": {
        "total": "60007",
        "ok": "-",
        "ko": "60007"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-3eaed": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_108223",
path: "Animal-api sync for customer NL_108223",
pathFormatted: "req_animal-api-sync-3eaed",
stats: {
    "name": "Animal-api sync for customer NL_108223",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60007",
        "ok": "-",
        "ko": "60007"
    },
    "maxResponseTime": {
        "total": "60007",
        "ok": "-",
        "ko": "60007"
    },
    "meanResponseTime": {
        "total": "60007",
        "ok": "-",
        "ko": "60007"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60007",
        "ok": "-",
        "ko": "60007"
    },
    "percentiles2": {
        "total": "60007",
        "ok": "-",
        "ko": "60007"
    },
    "percentiles3": {
        "total": "60007",
        "ok": "-",
        "ko": "60007"
    },
    "percentiles4": {
        "total": "60007",
        "ok": "-",
        "ko": "60007"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-6ad60": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_124372",
path: "Animal-api sync for customer NL_124372",
pathFormatted: "req_animal-api-sync-6ad60",
stats: {
    "name": "Animal-api sync for customer NL_124372",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "6731",
        "ok": "6731",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "6731",
        "ok": "6731",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "6731",
        "ok": "6731",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "6731",
        "ok": "6731",
        "ko": "-"
    },
    "percentiles2": {
        "total": "6731",
        "ok": "6731",
        "ko": "-"
    },
    "percentiles3": {
        "total": "6731",
        "ok": "6731",
        "ko": "-"
    },
    "percentiles4": {
        "total": "6731",
        "ok": "6731",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-967fa": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_113311",
path: "Animal-api sync for customer NL_113311",
pathFormatted: "req_animal-api-sync-967fa",
stats: {
    "name": "Animal-api sync for customer NL_113311",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "9166",
        "ok": "9166",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "9166",
        "ok": "9166",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "9166",
        "ok": "9166",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "9166",
        "ok": "9166",
        "ko": "-"
    },
    "percentiles2": {
        "total": "9166",
        "ok": "9166",
        "ko": "-"
    },
    "percentiles3": {
        "total": "9166",
        "ok": "9166",
        "ko": "-"
    },
    "percentiles4": {
        "total": "9166",
        "ok": "9166",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-3736b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_114775",
path: "Animal-api sync for customer NL_114775",
pathFormatted: "req_animal-api-sync-3736b",
stats: {
    "name": "Animal-api sync for customer NL_114775",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "44329",
        "ok": "44329",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "44329",
        "ok": "44329",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "44329",
        "ok": "44329",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "44329",
        "ok": "44329",
        "ko": "-"
    },
    "percentiles2": {
        "total": "44329",
        "ok": "44329",
        "ko": "-"
    },
    "percentiles3": {
        "total": "44329",
        "ok": "44329",
        "ko": "-"
    },
    "percentiles4": {
        "total": "44329",
        "ok": "44329",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-2e3f2": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_124578",
path: "Animal-api sync for customer NL_124578",
pathFormatted: "req_animal-api-sync-2e3f2",
stats: {
    "name": "Animal-api sync for customer NL_124578",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "8311",
        "ok": "8311",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "8311",
        "ok": "8311",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "8311",
        "ok": "8311",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "8311",
        "ok": "8311",
        "ko": "-"
    },
    "percentiles2": {
        "total": "8311",
        "ok": "8311",
        "ko": "-"
    },
    "percentiles3": {
        "total": "8311",
        "ok": "8311",
        "ko": "-"
    },
    "percentiles4": {
        "total": "8311",
        "ok": "8311",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-f6bd1": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_118770",
path: "Animal-api sync for customer NL_118770",
pathFormatted: "req_animal-api-sync-f6bd1",
stats: {
    "name": "Animal-api sync for customer NL_118770",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "40197",
        "ok": "40197",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "40197",
        "ok": "40197",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "40197",
        "ok": "40197",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "40197",
        "ok": "40197",
        "ko": "-"
    },
    "percentiles2": {
        "total": "40197",
        "ok": "40197",
        "ko": "-"
    },
    "percentiles3": {
        "total": "40197",
        "ok": "40197",
        "ko": "-"
    },
    "percentiles4": {
        "total": "40197",
        "ok": "40197",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-cfdfb": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111312",
path: "Animal-api sync for customer NL_111312",
pathFormatted: "req_animal-api-sync-cfdfb",
stats: {
    "name": "Animal-api sync for customer NL_111312",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "13780",
        "ok": "13780",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "13780",
        "ok": "13780",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "13780",
        "ok": "13780",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "13780",
        "ok": "13780",
        "ko": "-"
    },
    "percentiles2": {
        "total": "13780",
        "ok": "13780",
        "ko": "-"
    },
    "percentiles3": {
        "total": "13780",
        "ok": "13780",
        "ko": "-"
    },
    "percentiles4": {
        "total": "13780",
        "ok": "13780",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c9778": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_119273",
path: "Animal-api sync for customer NL_119273",
pathFormatted: "req_animal-api-sync-c9778",
stats: {
    "name": "Animal-api sync for customer NL_119273",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "10354",
        "ok": "10354",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "10354",
        "ok": "10354",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "10354",
        "ok": "10354",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "10354",
        "ok": "10354",
        "ko": "-"
    },
    "percentiles2": {
        "total": "10354",
        "ok": "10354",
        "ko": "-"
    },
    "percentiles3": {
        "total": "10354",
        "ok": "10354",
        "ko": "-"
    },
    "percentiles4": {
        "total": "10354",
        "ok": "10354",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-39d31": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_118474",
path: "Animal-api sync for customer NL_118474",
pathFormatted: "req_animal-api-sync-39d31",
stats: {
    "name": "Animal-api sync for customer NL_118474",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "12156",
        "ok": "12156",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "12156",
        "ok": "12156",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "12156",
        "ok": "12156",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "12156",
        "ok": "12156",
        "ko": "-"
    },
    "percentiles2": {
        "total": "12156",
        "ok": "12156",
        "ko": "-"
    },
    "percentiles3": {
        "total": "12156",
        "ok": "12156",
        "ko": "-"
    },
    "percentiles4": {
        "total": "12156",
        "ok": "12156",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-53fa3": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_108741",
path: "Animal-api sync for customer NL_108741",
pathFormatted: "req_animal-api-sync-53fa3",
stats: {
    "name": "Animal-api sync for customer NL_108741",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "42620",
        "ok": "42620",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "42620",
        "ok": "42620",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "42620",
        "ok": "42620",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "42620",
        "ok": "42620",
        "ko": "-"
    },
    "percentiles2": {
        "total": "42620",
        "ok": "42620",
        "ko": "-"
    },
    "percentiles3": {
        "total": "42620",
        "ok": "42620",
        "ko": "-"
    },
    "percentiles4": {
        "total": "42620",
        "ok": "42620",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-99401": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111178",
path: "Animal-api sync for customer NL_111178",
pathFormatted: "req_animal-api-sync-99401",
stats: {
    "name": "Animal-api sync for customer NL_111178",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "44970",
        "ok": "44970",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "44970",
        "ok": "44970",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "44970",
        "ok": "44970",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "44970",
        "ok": "44970",
        "ko": "-"
    },
    "percentiles2": {
        "total": "44970",
        "ok": "44970",
        "ko": "-"
    },
    "percentiles3": {
        "total": "44970",
        "ok": "44970",
        "ko": "-"
    },
    "percentiles4": {
        "total": "44970",
        "ok": "44970",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-09c2e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_135864",
path: "Animal-api sync for customer NL_135864",
pathFormatted: "req_animal-api-sync-09c2e",
stats: {
    "name": "Animal-api sync for customer NL_135864",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "38341",
        "ok": "38341",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "38341",
        "ok": "38341",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "38341",
        "ok": "38341",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "38341",
        "ok": "38341",
        "ko": "-"
    },
    "percentiles2": {
        "total": "38341",
        "ok": "38341",
        "ko": "-"
    },
    "percentiles3": {
        "total": "38341",
        "ok": "38341",
        "ko": "-"
    },
    "percentiles4": {
        "total": "38341",
        "ok": "38341",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-236d8": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_124049",
path: "Animal-api sync for customer NL_124049",
pathFormatted: "req_animal-api-sync-236d8",
stats: {
    "name": "Animal-api sync for customer NL_124049",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "16150",
        "ok": "16150",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "16150",
        "ok": "16150",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "16150",
        "ok": "16150",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "16150",
        "ok": "16150",
        "ko": "-"
    },
    "percentiles2": {
        "total": "16150",
        "ok": "16150",
        "ko": "-"
    },
    "percentiles3": {
        "total": "16150",
        "ok": "16150",
        "ko": "-"
    },
    "percentiles4": {
        "total": "16150",
        "ok": "16150",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-882c2": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_128097",
path: "Animal-api sync for customer NL_128097",
pathFormatted: "req_animal-api-sync-882c2",
stats: {
    "name": "Animal-api sync for customer NL_128097",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4296",
        "ok": "4296",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4296",
        "ok": "4296",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4296",
        "ok": "4296",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4296",
        "ok": "4296",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4296",
        "ok": "4296",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4296",
        "ok": "4296",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4296",
        "ok": "4296",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e7614": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_159766",
path: "Animal-api sync for customer BE_159766",
pathFormatted: "req_animal-api-sync-e7614",
stats: {
    "name": "Animal-api sync for customer BE_159766",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "maxResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "meanResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles2": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles3": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles4": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-52a69": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_212921",
path: "Animal-api sync for customer BE_212921",
pathFormatted: "req_animal-api-sync-52a69",
stats: {
    "name": "Animal-api sync for customer BE_212921",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "8852",
        "ok": "8852",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "8852",
        "ok": "8852",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "8852",
        "ok": "8852",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "8852",
        "ok": "8852",
        "ko": "-"
    },
    "percentiles2": {
        "total": "8852",
        "ok": "8852",
        "ko": "-"
    },
    "percentiles3": {
        "total": "8852",
        "ok": "8852",
        "ko": "-"
    },
    "percentiles4": {
        "total": "8852",
        "ok": "8852",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-53e2f": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_127894",
path: "Animal-api sync for customer NL_127894",
pathFormatted: "req_animal-api-sync-53e2f",
stats: {
    "name": "Animal-api sync for customer NL_127894",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "10442",
        "ok": "10442",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "10442",
        "ok": "10442",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "10442",
        "ok": "10442",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "10442",
        "ok": "10442",
        "ko": "-"
    },
    "percentiles2": {
        "total": "10442",
        "ok": "10442",
        "ko": "-"
    },
    "percentiles3": {
        "total": "10442",
        "ok": "10442",
        "ko": "-"
    },
    "percentiles4": {
        "total": "10442",
        "ok": "10442",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b4a99": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104247",
path: "Animal-api sync for customer NL_104247",
pathFormatted: "req_animal-api-sync-b4a99",
stats: {
    "name": "Animal-api sync for customer NL_104247",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "19142",
        "ok": "19142",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "19142",
        "ok": "19142",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "19142",
        "ok": "19142",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "19142",
        "ok": "19142",
        "ko": "-"
    },
    "percentiles2": {
        "total": "19142",
        "ok": "19142",
        "ko": "-"
    },
    "percentiles3": {
        "total": "19142",
        "ok": "19142",
        "ko": "-"
    },
    "percentiles4": {
        "total": "19142",
        "ok": "19142",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c4c5c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_122906",
path: "Animal-api sync for customer NL_122906",
pathFormatted: "req_animal-api-sync-c4c5c",
stats: {
    "name": "Animal-api sync for customer NL_122906",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "13184",
        "ok": "13184",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "13184",
        "ok": "13184",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "13184",
        "ok": "13184",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "13184",
        "ok": "13184",
        "ko": "-"
    },
    "percentiles2": {
        "total": "13184",
        "ok": "13184",
        "ko": "-"
    },
    "percentiles3": {
        "total": "13184",
        "ok": "13184",
        "ko": "-"
    },
    "percentiles4": {
        "total": "13184",
        "ok": "13184",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e42dd": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_108296",
path: "Animal-api sync for customer NL_108296",
pathFormatted: "req_animal-api-sync-e42dd",
stats: {
    "name": "Animal-api sync for customer NL_108296",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2530",
        "ok": "2530",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2530",
        "ok": "2530",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2530",
        "ok": "2530",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2530",
        "ok": "2530",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2530",
        "ok": "2530",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2530",
        "ok": "2530",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2530",
        "ok": "2530",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b8f6b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_120259",
path: "Animal-api sync for customer NL_120259",
pathFormatted: "req_animal-api-sync-b8f6b",
stats: {
    "name": "Animal-api sync for customer NL_120259",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "11401",
        "ok": "11401",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "11401",
        "ok": "11401",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "11401",
        "ok": "11401",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "11401",
        "ok": "11401",
        "ko": "-"
    },
    "percentiles2": {
        "total": "11401",
        "ok": "11401",
        "ko": "-"
    },
    "percentiles3": {
        "total": "11401",
        "ok": "11401",
        "ko": "-"
    },
    "percentiles4": {
        "total": "11401",
        "ok": "11401",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-10dc5": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_113215",
path: "Animal-api sync for customer NL_113215",
pathFormatted: "req_animal-api-sync-10dc5",
stats: {
    "name": "Animal-api sync for customer NL_113215",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "47094",
        "ok": "47094",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "47094",
        "ok": "47094",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "47094",
        "ok": "47094",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "47094",
        "ok": "47094",
        "ko": "-"
    },
    "percentiles2": {
        "total": "47094",
        "ok": "47094",
        "ko": "-"
    },
    "percentiles3": {
        "total": "47094",
        "ok": "47094",
        "ko": "-"
    },
    "percentiles4": {
        "total": "47094",
        "ok": "47094",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-ee676": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_119868",
path: "Animal-api sync for customer NL_119868",
pathFormatted: "req_animal-api-sync-ee676",
stats: {
    "name": "Animal-api sync for customer NL_119868",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "31712",
        "ok": "31712",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "31712",
        "ok": "31712",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "31712",
        "ok": "31712",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "31712",
        "ok": "31712",
        "ko": "-"
    },
    "percentiles2": {
        "total": "31712",
        "ok": "31712",
        "ko": "-"
    },
    "percentiles3": {
        "total": "31712",
        "ok": "31712",
        "ko": "-"
    },
    "percentiles4": {
        "total": "31712",
        "ok": "31712",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-12c8b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_107507",
path: "Animal-api sync for customer NL_107507",
pathFormatted: "req_animal-api-sync-12c8b",
stats: {
    "name": "Animal-api sync for customer NL_107507",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "57719",
        "ok": "57719",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "57719",
        "ok": "57719",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "57719",
        "ok": "57719",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "57719",
        "ok": "57719",
        "ko": "-"
    },
    "percentiles2": {
        "total": "57719",
        "ok": "57719",
        "ko": "-"
    },
    "percentiles3": {
        "total": "57719",
        "ok": "57719",
        "ko": "-"
    },
    "percentiles4": {
        "total": "57719",
        "ok": "57719",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-70c3b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_108064",
path: "Animal-api sync for customer NL_108064",
pathFormatted: "req_animal-api-sync-70c3b",
stats: {
    "name": "Animal-api sync for customer NL_108064",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "34308",
        "ok": "34308",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "34308",
        "ok": "34308",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "34308",
        "ok": "34308",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "34308",
        "ok": "34308",
        "ko": "-"
    },
    "percentiles2": {
        "total": "34308",
        "ok": "34308",
        "ko": "-"
    },
    "percentiles3": {
        "total": "34308",
        "ok": "34308",
        "ko": "-"
    },
    "percentiles4": {
        "total": "34308",
        "ok": "34308",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d5bee": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_114423",
path: "Animal-api sync for customer NL_114423",
pathFormatted: "req_animal-api-sync-d5bee",
stats: {
    "name": "Animal-api sync for customer NL_114423",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3821",
        "ok": "3821",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3821",
        "ok": "3821",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3821",
        "ok": "3821",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3821",
        "ok": "3821",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3821",
        "ok": "3821",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3821",
        "ok": "3821",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3821",
        "ok": "3821",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-f94cb": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_107241",
path: "Animal-api sync for customer NL_107241",
pathFormatted: "req_animal-api-sync-f94cb",
stats: {
    "name": "Animal-api sync for customer NL_107241",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "22421",
        "ok": "22421",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "22421",
        "ok": "22421",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "22421",
        "ok": "22421",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "22421",
        "ok": "22421",
        "ko": "-"
    },
    "percentiles2": {
        "total": "22421",
        "ok": "22421",
        "ko": "-"
    },
    "percentiles3": {
        "total": "22421",
        "ok": "22421",
        "ko": "-"
    },
    "percentiles4": {
        "total": "22421",
        "ok": "22421",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-4b1f5": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_129590",
path: "Animal-api sync for customer NL_129590",
pathFormatted: "req_animal-api-sync-4b1f5",
stats: {
    "name": "Animal-api sync for customer NL_129590",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "40541",
        "ok": "40541",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "40541",
        "ok": "40541",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "40541",
        "ok": "40541",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "40541",
        "ok": "40541",
        "ko": "-"
    },
    "percentiles2": {
        "total": "40541",
        "ok": "40541",
        "ko": "-"
    },
    "percentiles3": {
        "total": "40541",
        "ok": "40541",
        "ko": "-"
    },
    "percentiles4": {
        "total": "40541",
        "ok": "40541",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-25bc2": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111740",
path: "Animal-api sync for customer NL_111740",
pathFormatted: "req_animal-api-sync-25bc2",
stats: {
    "name": "Animal-api sync for customer NL_111740",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2331",
        "ok": "2331",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2331",
        "ok": "2331",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2331",
        "ok": "2331",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2331",
        "ok": "2331",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2331",
        "ok": "2331",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2331",
        "ok": "2331",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2331",
        "ok": "2331",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-2efa4": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117125",
path: "Animal-api sync for customer NL_117125",
pathFormatted: "req_animal-api-sync-2efa4",
stats: {
    "name": "Animal-api sync for customer NL_117125",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "27537",
        "ok": "27537",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "27537",
        "ok": "27537",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "27537",
        "ok": "27537",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "27537",
        "ok": "27537",
        "ko": "-"
    },
    "percentiles2": {
        "total": "27537",
        "ok": "27537",
        "ko": "-"
    },
    "percentiles3": {
        "total": "27537",
        "ok": "27537",
        "ko": "-"
    },
    "percentiles4": {
        "total": "27537",
        "ok": "27537",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c4fc3": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_216527",
path: "Animal-api sync for customer NL_216527",
pathFormatted: "req_animal-api-sync-c4fc3",
stats: {
    "name": "Animal-api sync for customer NL_216527",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "30542",
        "ok": "30542",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "30542",
        "ok": "30542",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "30542",
        "ok": "30542",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "30542",
        "ok": "30542",
        "ko": "-"
    },
    "percentiles2": {
        "total": "30542",
        "ok": "30542",
        "ko": "-"
    },
    "percentiles3": {
        "total": "30542",
        "ok": "30542",
        "ko": "-"
    },
    "percentiles4": {
        "total": "30542",
        "ok": "30542",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-41435": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_119693",
path: "Animal-api sync for customer NL_119693",
pathFormatted: "req_animal-api-sync-41435",
stats: {
    "name": "Animal-api sync for customer NL_119693",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "15208",
        "ok": "15208",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "15208",
        "ok": "15208",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "15208",
        "ok": "15208",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "15208",
        "ok": "15208",
        "ko": "-"
    },
    "percentiles2": {
        "total": "15208",
        "ok": "15208",
        "ko": "-"
    },
    "percentiles3": {
        "total": "15208",
        "ok": "15208",
        "ko": "-"
    },
    "percentiles4": {
        "total": "15208",
        "ok": "15208",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e3e3d": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_126029",
path: "Animal-api sync for customer NL_126029",
pathFormatted: "req_animal-api-sync-e3e3d",
stats: {
    "name": "Animal-api sync for customer NL_126029",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "54068",
        "ok": "54068",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "54068",
        "ok": "54068",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "54068",
        "ok": "54068",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "54068",
        "ok": "54068",
        "ko": "-"
    },
    "percentiles2": {
        "total": "54068",
        "ok": "54068",
        "ko": "-"
    },
    "percentiles3": {
        "total": "54068",
        "ok": "54068",
        "ko": "-"
    },
    "percentiles4": {
        "total": "54068",
        "ok": "54068",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d8bef": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_114961",
path: "Animal-api sync for customer NL_114961",
pathFormatted: "req_animal-api-sync-d8bef",
stats: {
    "name": "Animal-api sync for customer NL_114961",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "14392",
        "ok": "14392",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "14392",
        "ok": "14392",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "14392",
        "ok": "14392",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "14392",
        "ok": "14392",
        "ko": "-"
    },
    "percentiles2": {
        "total": "14392",
        "ok": "14392",
        "ko": "-"
    },
    "percentiles3": {
        "total": "14392",
        "ok": "14392",
        "ko": "-"
    },
    "percentiles4": {
        "total": "14392",
        "ok": "14392",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-40763": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_127944",
path: "Animal-api sync for customer NL_127944",
pathFormatted: "req_animal-api-sync-40763",
stats: {
    "name": "Animal-api sync for customer NL_127944",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4352",
        "ok": "4352",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4352",
        "ok": "4352",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4352",
        "ok": "4352",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4352",
        "ok": "4352",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4352",
        "ok": "4352",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4352",
        "ok": "4352",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4352",
        "ok": "4352",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-9f4a5": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_108715",
path: "Animal-api sync for customer NL_108715",
pathFormatted: "req_animal-api-sync-9f4a5",
stats: {
    "name": "Animal-api sync for customer NL_108715",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "35153",
        "ok": "35153",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "35153",
        "ok": "35153",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "35153",
        "ok": "35153",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "35153",
        "ok": "35153",
        "ko": "-"
    },
    "percentiles2": {
        "total": "35153",
        "ok": "35153",
        "ko": "-"
    },
    "percentiles3": {
        "total": "35153",
        "ok": "35153",
        "ko": "-"
    },
    "percentiles4": {
        "total": "35153",
        "ok": "35153",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-0355c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_133793",
path: "Animal-api sync for customer NL_133793",
pathFormatted: "req_animal-api-sync-0355c",
stats: {
    "name": "Animal-api sync for customer NL_133793",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "23849",
        "ok": "23849",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "23849",
        "ok": "23849",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "23849",
        "ok": "23849",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "23849",
        "ok": "23849",
        "ko": "-"
    },
    "percentiles2": {
        "total": "23849",
        "ok": "23849",
        "ko": "-"
    },
    "percentiles3": {
        "total": "23849",
        "ok": "23849",
        "ko": "-"
    },
    "percentiles4": {
        "total": "23849",
        "ok": "23849",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-6bc35": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_127470",
path: "Animal-api sync for customer NL_127470",
pathFormatted: "req_animal-api-sync-6bc35",
stats: {
    "name": "Animal-api sync for customer NL_127470",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "11393",
        "ok": "11393",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "11393",
        "ok": "11393",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "11393",
        "ok": "11393",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "11393",
        "ok": "11393",
        "ko": "-"
    },
    "percentiles2": {
        "total": "11393",
        "ok": "11393",
        "ko": "-"
    },
    "percentiles3": {
        "total": "11393",
        "ok": "11393",
        "ko": "-"
    },
    "percentiles4": {
        "total": "11393",
        "ok": "11393",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-7eb9d": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104934",
path: "Animal-api sync for customer NL_104934",
pathFormatted: "req_animal-api-sync-7eb9d",
stats: {
    "name": "Animal-api sync for customer NL_104934",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "54886",
        "ok": "54886",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "54886",
        "ok": "54886",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "54886",
        "ok": "54886",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "54886",
        "ok": "54886",
        "ko": "-"
    },
    "percentiles2": {
        "total": "54886",
        "ok": "54886",
        "ko": "-"
    },
    "percentiles3": {
        "total": "54886",
        "ok": "54886",
        "ko": "-"
    },
    "percentiles4": {
        "total": "54886",
        "ok": "54886",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-3a032": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_122396",
path: "Animal-api sync for customer NL_122396",
pathFormatted: "req_animal-api-sync-3a032",
stats: {
    "name": "Animal-api sync for customer NL_122396",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "10033",
        "ok": "10033",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "10033",
        "ok": "10033",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "10033",
        "ok": "10033",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "10033",
        "ok": "10033",
        "ko": "-"
    },
    "percentiles2": {
        "total": "10033",
        "ok": "10033",
        "ko": "-"
    },
    "percentiles3": {
        "total": "10033",
        "ok": "10033",
        "ko": "-"
    },
    "percentiles4": {
        "total": "10033",
        "ok": "10033",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-1cf92": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105173",
path: "Animal-api sync for customer NL_105173",
pathFormatted: "req_animal-api-sync-1cf92",
stats: {
    "name": "Animal-api sync for customer NL_105173",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "33198",
        "ok": "33198",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "33198",
        "ok": "33198",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "33198",
        "ok": "33198",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "33198",
        "ok": "33198",
        "ko": "-"
    },
    "percentiles2": {
        "total": "33198",
        "ok": "33198",
        "ko": "-"
    },
    "percentiles3": {
        "total": "33198",
        "ok": "33198",
        "ko": "-"
    },
    "percentiles4": {
        "total": "33198",
        "ok": "33198",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b22b6": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_131590",
path: "Animal-api sync for customer NL_131590",
pathFormatted: "req_animal-api-sync-b22b6",
stats: {
    "name": "Animal-api sync for customer NL_131590",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5623",
        "ok": "5623",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5623",
        "ok": "5623",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5623",
        "ok": "5623",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5623",
        "ok": "5623",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5623",
        "ok": "5623",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5623",
        "ok": "5623",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5623",
        "ok": "5623",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-1080b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110702",
path: "Animal-api sync for customer NL_110702",
pathFormatted: "req_animal-api-sync-1080b",
stats: {
    "name": "Animal-api sync for customer NL_110702",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "6907",
        "ok": "6907",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "6907",
        "ok": "6907",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "6907",
        "ok": "6907",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "6907",
        "ok": "6907",
        "ko": "-"
    },
    "percentiles2": {
        "total": "6907",
        "ok": "6907",
        "ko": "-"
    },
    "percentiles3": {
        "total": "6907",
        "ok": "6907",
        "ko": "-"
    },
    "percentiles4": {
        "total": "6907",
        "ok": "6907",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-8580c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_119732",
path: "Animal-api sync for customer NL_119732",
pathFormatted: "req_animal-api-sync-8580c",
stats: {
    "name": "Animal-api sync for customer NL_119732",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4840",
        "ok": "4840",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4840",
        "ok": "4840",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4840",
        "ok": "4840",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4840",
        "ok": "4840",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4840",
        "ok": "4840",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4840",
        "ok": "4840",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4840",
        "ok": "4840",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-8dfa9": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_116373",
path: "Animal-api sync for customer NL_116373",
pathFormatted: "req_animal-api-sync-8dfa9",
stats: {
    "name": "Animal-api sync for customer NL_116373",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "8638",
        "ok": "8638",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "8638",
        "ok": "8638",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "8638",
        "ok": "8638",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "8638",
        "ok": "8638",
        "ko": "-"
    },
    "percentiles2": {
        "total": "8638",
        "ok": "8638",
        "ko": "-"
    },
    "percentiles3": {
        "total": "8638",
        "ok": "8638",
        "ko": "-"
    },
    "percentiles4": {
        "total": "8638",
        "ok": "8638",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c298e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_124623",
path: "Animal-api sync for customer NL_124623",
pathFormatted: "req_animal-api-sync-c298e",
stats: {
    "name": "Animal-api sync for customer NL_124623",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3016",
        "ok": "3016",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3016",
        "ok": "3016",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3016",
        "ok": "3016",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3016",
        "ok": "3016",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3016",
        "ok": "3016",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3016",
        "ok": "3016",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3016",
        "ok": "3016",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-18c50": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_124684",
path: "Animal-api sync for customer NL_124684",
pathFormatted: "req_animal-api-sync-18c50",
stats: {
    "name": "Animal-api sync for customer NL_124684",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5558",
        "ok": "5558",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5558",
        "ok": "5558",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5558",
        "ok": "5558",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5558",
        "ok": "5558",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5558",
        "ok": "5558",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5558",
        "ok": "5558",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5558",
        "ok": "5558",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-11d7b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111681",
path: "Animal-api sync for customer NL_111681",
pathFormatted: "req_animal-api-sync-11d7b",
stats: {
    "name": "Animal-api sync for customer NL_111681",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "26287",
        "ok": "26287",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "26287",
        "ok": "26287",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "26287",
        "ok": "26287",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "26287",
        "ok": "26287",
        "ko": "-"
    },
    "percentiles2": {
        "total": "26287",
        "ok": "26287",
        "ko": "-"
    },
    "percentiles3": {
        "total": "26287",
        "ok": "26287",
        "ko": "-"
    },
    "percentiles4": {
        "total": "26287",
        "ok": "26287",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-f0b42": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_107121",
path: "Animal-api sync for customer NL_107121",
pathFormatted: "req_animal-api-sync-f0b42",
stats: {
    "name": "Animal-api sync for customer NL_107121",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "31752",
        "ok": "31752",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "31752",
        "ok": "31752",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "31752",
        "ok": "31752",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "31752",
        "ok": "31752",
        "ko": "-"
    },
    "percentiles2": {
        "total": "31752",
        "ok": "31752",
        "ko": "-"
    },
    "percentiles3": {
        "total": "31752",
        "ok": "31752",
        "ko": "-"
    },
    "percentiles4": {
        "total": "31752",
        "ok": "31752",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-de92c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_109851",
path: "Animal-api sync for customer NL_109851",
pathFormatted: "req_animal-api-sync-de92c",
stats: {
    "name": "Animal-api sync for customer NL_109851",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "31061",
        "ok": "31061",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "31061",
        "ok": "31061",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "31061",
        "ok": "31061",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "31061",
        "ok": "31061",
        "ko": "-"
    },
    "percentiles2": {
        "total": "31061",
        "ok": "31061",
        "ko": "-"
    },
    "percentiles3": {
        "total": "31061",
        "ok": "31061",
        "ko": "-"
    },
    "percentiles4": {
        "total": "31061",
        "ok": "31061",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-31610": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_109147",
path: "Animal-api sync for customer NL_109147",
pathFormatted: "req_animal-api-sync-31610",
stats: {
    "name": "Animal-api sync for customer NL_109147",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2269",
        "ok": "2269",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2269",
        "ok": "2269",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2269",
        "ok": "2269",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2269",
        "ok": "2269",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2269",
        "ok": "2269",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2269",
        "ok": "2269",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2269",
        "ok": "2269",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a6474": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_217194",
path: "Animal-api sync for customer NL_217194",
pathFormatted: "req_animal-api-sync-a6474",
stats: {
    "name": "Animal-api sync for customer NL_217194",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "26186",
        "ok": "26186",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "26186",
        "ok": "26186",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "26186",
        "ok": "26186",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "26186",
        "ok": "26186",
        "ko": "-"
    },
    "percentiles2": {
        "total": "26186",
        "ok": "26186",
        "ko": "-"
    },
    "percentiles3": {
        "total": "26186",
        "ok": "26186",
        "ko": "-"
    },
    "percentiles4": {
        "total": "26186",
        "ok": "26186",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-735e5": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_162624",
path: "Animal-api sync for customer BE_162624",
pathFormatted: "req_animal-api-sync-735e5",
stats: {
    "name": "Animal-api sync for customer BE_162624",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "18061",
        "ok": "18061",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "18061",
        "ok": "18061",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "18061",
        "ok": "18061",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "18061",
        "ok": "18061",
        "ko": "-"
    },
    "percentiles2": {
        "total": "18061",
        "ok": "18061",
        "ko": "-"
    },
    "percentiles3": {
        "total": "18061",
        "ok": "18061",
        "ko": "-"
    },
    "percentiles4": {
        "total": "18061",
        "ok": "18061",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-da049": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111349",
path: "Animal-api sync for customer NL_111349",
pathFormatted: "req_animal-api-sync-da049",
stats: {
    "name": "Animal-api sync for customer NL_111349",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "17311",
        "ok": "17311",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "17311",
        "ok": "17311",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "17311",
        "ok": "17311",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "17311",
        "ok": "17311",
        "ko": "-"
    },
    "percentiles2": {
        "total": "17311",
        "ok": "17311",
        "ko": "-"
    },
    "percentiles3": {
        "total": "17311",
        "ok": "17311",
        "ko": "-"
    },
    "percentiles4": {
        "total": "17311",
        "ok": "17311",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-0945e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_120830",
path: "Animal-api sync for customer NL_120830",
pathFormatted: "req_animal-api-sync-0945e",
stats: {
    "name": "Animal-api sync for customer NL_120830",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "19486",
        "ok": "19486",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "19486",
        "ok": "19486",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "19486",
        "ok": "19486",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "19486",
        "ok": "19486",
        "ko": "-"
    },
    "percentiles2": {
        "total": "19486",
        "ok": "19486",
        "ko": "-"
    },
    "percentiles3": {
        "total": "19486",
        "ok": "19486",
        "ko": "-"
    },
    "percentiles4": {
        "total": "19486",
        "ok": "19486",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d1db6": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_154426",
path: "Animal-api sync for customer BE_154426",
pathFormatted: "req_animal-api-sync-d1db6",
stats: {
    "name": "Animal-api sync for customer BE_154426",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "53615",
        "ok": "53615",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "53615",
        "ok": "53615",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "53615",
        "ok": "53615",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "53615",
        "ok": "53615",
        "ko": "-"
    },
    "percentiles2": {
        "total": "53615",
        "ok": "53615",
        "ko": "-"
    },
    "percentiles3": {
        "total": "53615",
        "ok": "53615",
        "ko": "-"
    },
    "percentiles4": {
        "total": "53615",
        "ok": "53615",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-3b46a": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_160734",
path: "Animal-api sync for customer NL_160734",
pathFormatted: "req_animal-api-sync-3b46a",
stats: {
    "name": "Animal-api sync for customer NL_160734",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "9829",
        "ok": "9829",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "9829",
        "ok": "9829",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "9829",
        "ok": "9829",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "9829",
        "ok": "9829",
        "ko": "-"
    },
    "percentiles2": {
        "total": "9829",
        "ok": "9829",
        "ko": "-"
    },
    "percentiles3": {
        "total": "9829",
        "ok": "9829",
        "ko": "-"
    },
    "percentiles4": {
        "total": "9829",
        "ok": "9829",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-4b701": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_122804",
path: "Animal-api sync for customer NL_122804",
pathFormatted: "req_animal-api-sync-4b701",
stats: {
    "name": "Animal-api sync for customer NL_122804",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "43811",
        "ok": "43811",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "43811",
        "ok": "43811",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "43811",
        "ok": "43811",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "43811",
        "ok": "43811",
        "ko": "-"
    },
    "percentiles2": {
        "total": "43811",
        "ok": "43811",
        "ko": "-"
    },
    "percentiles3": {
        "total": "43811",
        "ok": "43811",
        "ko": "-"
    },
    "percentiles4": {
        "total": "43811",
        "ok": "43811",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-3f46c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110510",
path: "Animal-api sync for customer NL_110510",
pathFormatted: "req_animal-api-sync-3f46c",
stats: {
    "name": "Animal-api sync for customer NL_110510",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4738",
        "ok": "4738",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4738",
        "ok": "4738",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4738",
        "ok": "4738",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4738",
        "ok": "4738",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4738",
        "ok": "4738",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4738",
        "ok": "4738",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4738",
        "ok": "4738",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e1650": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_189253",
path: "Animal-api sync for customer BE_189253",
pathFormatted: "req_animal-api-sync-e1650",
stats: {
    "name": "Animal-api sync for customer BE_189253",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21784",
        "ok": "21784",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "21784",
        "ok": "21784",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "21784",
        "ok": "21784",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "21784",
        "ok": "21784",
        "ko": "-"
    },
    "percentiles2": {
        "total": "21784",
        "ok": "21784",
        "ko": "-"
    },
    "percentiles3": {
        "total": "21784",
        "ok": "21784",
        "ko": "-"
    },
    "percentiles4": {
        "total": "21784",
        "ok": "21784",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-5a990": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_196831",
path: "Animal-api sync for customer BE_196831",
pathFormatted: "req_animal-api-sync-5a990",
stats: {
    "name": "Animal-api sync for customer BE_196831",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "9850",
        "ok": "9850",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "9850",
        "ok": "9850",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "9850",
        "ok": "9850",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "9850",
        "ok": "9850",
        "ko": "-"
    },
    "percentiles2": {
        "total": "9850",
        "ok": "9850",
        "ko": "-"
    },
    "percentiles3": {
        "total": "9850",
        "ok": "9850",
        "ko": "-"
    },
    "percentiles4": {
        "total": "9850",
        "ok": "9850",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-8b170": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_128376",
path: "Animal-api sync for customer NL_128376",
pathFormatted: "req_animal-api-sync-8b170",
stats: {
    "name": "Animal-api sync for customer NL_128376",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "26888",
        "ok": "26888",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "26888",
        "ok": "26888",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "26888",
        "ok": "26888",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "26888",
        "ok": "26888",
        "ko": "-"
    },
    "percentiles2": {
        "total": "26888",
        "ok": "26888",
        "ko": "-"
    },
    "percentiles3": {
        "total": "26888",
        "ok": "26888",
        "ko": "-"
    },
    "percentiles4": {
        "total": "26888",
        "ok": "26888",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-9e2d7": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_127839",
path: "Animal-api sync for customer NL_127839",
pathFormatted: "req_animal-api-sync-9e2d7",
stats: {
    "name": "Animal-api sync for customer NL_127839",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "13812",
        "ok": "13812",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "13812",
        "ok": "13812",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "13812",
        "ok": "13812",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "13812",
        "ok": "13812",
        "ko": "-"
    },
    "percentiles2": {
        "total": "13812",
        "ok": "13812",
        "ko": "-"
    },
    "percentiles3": {
        "total": "13812",
        "ok": "13812",
        "ko": "-"
    },
    "percentiles4": {
        "total": "13812",
        "ok": "13812",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-cb3f0": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_109354",
path: "Animal-api sync for customer NL_109354",
pathFormatted: "req_animal-api-sync-cb3f0",
stats: {
    "name": "Animal-api sync for customer NL_109354",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "8738",
        "ok": "8738",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "8738",
        "ok": "8738",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "8738",
        "ok": "8738",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "8738",
        "ok": "8738",
        "ko": "-"
    },
    "percentiles2": {
        "total": "8738",
        "ok": "8738",
        "ko": "-"
    },
    "percentiles3": {
        "total": "8738",
        "ok": "8738",
        "ko": "-"
    },
    "percentiles4": {
        "total": "8738",
        "ok": "8738",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-77e7f": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_116276",
path: "Animal-api sync for customer NL_116276",
pathFormatted: "req_animal-api-sync-77e7f",
stats: {
    "name": "Animal-api sync for customer NL_116276",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "15793",
        "ok": "15793",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "15793",
        "ok": "15793",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "15793",
        "ok": "15793",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "15793",
        "ok": "15793",
        "ko": "-"
    },
    "percentiles2": {
        "total": "15793",
        "ok": "15793",
        "ko": "-"
    },
    "percentiles3": {
        "total": "15793",
        "ok": "15793",
        "ko": "-"
    },
    "percentiles4": {
        "total": "15793",
        "ok": "15793",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e91bf": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111232",
path: "Animal-api sync for customer NL_111232",
pathFormatted: "req_animal-api-sync-e91bf",
stats: {
    "name": "Animal-api sync for customer NL_111232",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "9939",
        "ok": "9939",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "9939",
        "ok": "9939",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "9939",
        "ok": "9939",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "9939",
        "ok": "9939",
        "ko": "-"
    },
    "percentiles2": {
        "total": "9939",
        "ok": "9939",
        "ko": "-"
    },
    "percentiles3": {
        "total": "9939",
        "ok": "9939",
        "ko": "-"
    },
    "percentiles4": {
        "total": "9939",
        "ok": "9939",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-df608": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_106231",
path: "Animal-api sync for customer NL_106231",
pathFormatted: "req_animal-api-sync-df608",
stats: {
    "name": "Animal-api sync for customer NL_106231",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "45016",
        "ok": "45016",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "45016",
        "ok": "45016",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "45016",
        "ok": "45016",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "45016",
        "ok": "45016",
        "ko": "-"
    },
    "percentiles2": {
        "total": "45016",
        "ok": "45016",
        "ko": "-"
    },
    "percentiles3": {
        "total": "45016",
        "ok": "45016",
        "ko": "-"
    },
    "percentiles4": {
        "total": "45016",
        "ok": "45016",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-96235": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_119086",
path: "Animal-api sync for customer NL_119086",
pathFormatted: "req_animal-api-sync-96235",
stats: {
    "name": "Animal-api sync for customer NL_119086",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "24373",
        "ok": "24373",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "24373",
        "ok": "24373",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "24373",
        "ok": "24373",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "24373",
        "ok": "24373",
        "ko": "-"
    },
    "percentiles2": {
        "total": "24373",
        "ok": "24373",
        "ko": "-"
    },
    "percentiles3": {
        "total": "24373",
        "ok": "24373",
        "ko": "-"
    },
    "percentiles4": {
        "total": "24373",
        "ok": "24373",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b66f8": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110851",
path: "Animal-api sync for customer NL_110851",
pathFormatted: "req_animal-api-sync-b66f8",
stats: {
    "name": "Animal-api sync for customer NL_110851",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21060",
        "ok": "21060",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "21060",
        "ok": "21060",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "21060",
        "ok": "21060",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "21060",
        "ok": "21060",
        "ko": "-"
    },
    "percentiles2": {
        "total": "21060",
        "ok": "21060",
        "ko": "-"
    },
    "percentiles3": {
        "total": "21060",
        "ok": "21060",
        "ko": "-"
    },
    "percentiles4": {
        "total": "21060",
        "ok": "21060",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-fc706": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_108712",
path: "Animal-api sync for customer NL_108712",
pathFormatted: "req_animal-api-sync-fc706",
stats: {
    "name": "Animal-api sync for customer NL_108712",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "14909",
        "ok": "14909",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "14909",
        "ok": "14909",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "14909",
        "ok": "14909",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "14909",
        "ok": "14909",
        "ko": "-"
    },
    "percentiles2": {
        "total": "14909",
        "ok": "14909",
        "ko": "-"
    },
    "percentiles3": {
        "total": "14909",
        "ok": "14909",
        "ko": "-"
    },
    "percentiles4": {
        "total": "14909",
        "ok": "14909",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-77463": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_142162",
path: "Animal-api sync for customer NL_142162",
pathFormatted: "req_animal-api-sync-77463",
stats: {
    "name": "Animal-api sync for customer NL_142162",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "26037",
        "ok": "26037",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "26037",
        "ok": "26037",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "26037",
        "ok": "26037",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "26037",
        "ok": "26037",
        "ko": "-"
    },
    "percentiles2": {
        "total": "26037",
        "ok": "26037",
        "ko": "-"
    },
    "percentiles3": {
        "total": "26037",
        "ok": "26037",
        "ko": "-"
    },
    "percentiles4": {
        "total": "26037",
        "ok": "26037",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-3e0f1": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_119459",
path: "Animal-api sync for customer NL_119459",
pathFormatted: "req_animal-api-sync-3e0f1",
stats: {
    "name": "Animal-api sync for customer NL_119459",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "11714",
        "ok": "11714",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "11714",
        "ok": "11714",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "11714",
        "ok": "11714",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "11714",
        "ok": "11714",
        "ko": "-"
    },
    "percentiles2": {
        "total": "11714",
        "ok": "11714",
        "ko": "-"
    },
    "percentiles3": {
        "total": "11714",
        "ok": "11714",
        "ko": "-"
    },
    "percentiles4": {
        "total": "11714",
        "ok": "11714",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-f9107": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_121807",
path: "Animal-api sync for customer NL_121807",
pathFormatted: "req_animal-api-sync-f9107",
stats: {
    "name": "Animal-api sync for customer NL_121807",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "43021",
        "ok": "43021",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "43021",
        "ok": "43021",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "43021",
        "ok": "43021",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "43021",
        "ok": "43021",
        "ko": "-"
    },
    "percentiles2": {
        "total": "43021",
        "ok": "43021",
        "ko": "-"
    },
    "percentiles3": {
        "total": "43021",
        "ok": "43021",
        "ko": "-"
    },
    "percentiles4": {
        "total": "43021",
        "ok": "43021",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-82f9e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110685",
path: "Animal-api sync for customer NL_110685",
pathFormatted: "req_animal-api-sync-82f9e",
stats: {
    "name": "Animal-api sync for customer NL_110685",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "29985",
        "ok": "29985",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "29985",
        "ok": "29985",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "29985",
        "ok": "29985",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "29985",
        "ok": "29985",
        "ko": "-"
    },
    "percentiles2": {
        "total": "29985",
        "ok": "29985",
        "ko": "-"
    },
    "percentiles3": {
        "total": "29985",
        "ok": "29985",
        "ko": "-"
    },
    "percentiles4": {
        "total": "29985",
        "ok": "29985",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-39816": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_145536",
path: "Animal-api sync for customer BE_145536",
pathFormatted: "req_animal-api-sync-39816",
stats: {
    "name": "Animal-api sync for customer BE_145536",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "12570",
        "ok": "12570",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "12570",
        "ok": "12570",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "12570",
        "ok": "12570",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "12570",
        "ok": "12570",
        "ko": "-"
    },
    "percentiles2": {
        "total": "12570",
        "ok": "12570",
        "ko": "-"
    },
    "percentiles3": {
        "total": "12570",
        "ok": "12570",
        "ko": "-"
    },
    "percentiles4": {
        "total": "12570",
        "ok": "12570",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-66198": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_107001",
path: "Animal-api sync for customer NL_107001",
pathFormatted: "req_animal-api-sync-66198",
stats: {
    "name": "Animal-api sync for customer NL_107001",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "19116",
        "ok": "19116",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "19116",
        "ok": "19116",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "19116",
        "ok": "19116",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "19116",
        "ok": "19116",
        "ko": "-"
    },
    "percentiles2": {
        "total": "19116",
        "ok": "19116",
        "ko": "-"
    },
    "percentiles3": {
        "total": "19116",
        "ok": "19116",
        "ko": "-"
    },
    "percentiles4": {
        "total": "19116",
        "ok": "19116",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-299ce": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_119621",
path: "Animal-api sync for customer NL_119621",
pathFormatted: "req_animal-api-sync-299ce",
stats: {
    "name": "Animal-api sync for customer NL_119621",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "9988",
        "ok": "9988",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "9988",
        "ok": "9988",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "9988",
        "ok": "9988",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "9988",
        "ok": "9988",
        "ko": "-"
    },
    "percentiles2": {
        "total": "9988",
        "ok": "9988",
        "ko": "-"
    },
    "percentiles3": {
        "total": "9988",
        "ok": "9988",
        "ko": "-"
    },
    "percentiles4": {
        "total": "9988",
        "ok": "9988",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-cb563": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_195750",
path: "Animal-api sync for customer BE_195750",
pathFormatted: "req_animal-api-sync-cb563",
stats: {
    "name": "Animal-api sync for customer BE_195750",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "8397",
        "ok": "8397",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "8397",
        "ok": "8397",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "8397",
        "ok": "8397",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "8397",
        "ok": "8397",
        "ko": "-"
    },
    "percentiles2": {
        "total": "8397",
        "ok": "8397",
        "ko": "-"
    },
    "percentiles3": {
        "total": "8397",
        "ok": "8397",
        "ko": "-"
    },
    "percentiles4": {
        "total": "8397",
        "ok": "8397",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-86128": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110109",
path: "Animal-api sync for customer NL_110109",
pathFormatted: "req_animal-api-sync-86128",
stats: {
    "name": "Animal-api sync for customer NL_110109",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "32893",
        "ok": "32893",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "32893",
        "ok": "32893",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "32893",
        "ok": "32893",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "32893",
        "ok": "32893",
        "ko": "-"
    },
    "percentiles2": {
        "total": "32893",
        "ok": "32893",
        "ko": "-"
    },
    "percentiles3": {
        "total": "32893",
        "ok": "32893",
        "ko": "-"
    },
    "percentiles4": {
        "total": "32893",
        "ok": "32893",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-2a7a0": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_119620",
path: "Animal-api sync for customer NL_119620",
pathFormatted: "req_animal-api-sync-2a7a0",
stats: {
    "name": "Animal-api sync for customer NL_119620",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "15458",
        "ok": "15458",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "15458",
        "ok": "15458",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "15458",
        "ok": "15458",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "15458",
        "ok": "15458",
        "ko": "-"
    },
    "percentiles2": {
        "total": "15458",
        "ok": "15458",
        "ko": "-"
    },
    "percentiles3": {
        "total": "15458",
        "ok": "15458",
        "ko": "-"
    },
    "percentiles4": {
        "total": "15458",
        "ok": "15458",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-f1bc5": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_106522",
path: "Animal-api sync for customer NL_106522",
pathFormatted: "req_animal-api-sync-f1bc5",
stats: {
    "name": "Animal-api sync for customer NL_106522",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "6984",
        "ok": "6984",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "6984",
        "ok": "6984",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "6984",
        "ok": "6984",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "6984",
        "ok": "6984",
        "ko": "-"
    },
    "percentiles2": {
        "total": "6984",
        "ok": "6984",
        "ko": "-"
    },
    "percentiles3": {
        "total": "6984",
        "ok": "6984",
        "ko": "-"
    },
    "percentiles4": {
        "total": "6984",
        "ok": "6984",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-fa8fb": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110675",
path: "Animal-api sync for customer NL_110675",
pathFormatted: "req_animal-api-sync-fa8fb",
stats: {
    "name": "Animal-api sync for customer NL_110675",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "23753",
        "ok": "23753",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "23753",
        "ok": "23753",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "23753",
        "ok": "23753",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "23753",
        "ok": "23753",
        "ko": "-"
    },
    "percentiles2": {
        "total": "23753",
        "ok": "23753",
        "ko": "-"
    },
    "percentiles3": {
        "total": "23753",
        "ok": "23753",
        "ko": "-"
    },
    "percentiles4": {
        "total": "23753",
        "ok": "23753",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-9a989": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105564",
path: "Animal-api sync for customer NL_105564",
pathFormatted: "req_animal-api-sync-9a989",
stats: {
    "name": "Animal-api sync for customer NL_105564",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "15661",
        "ok": "15661",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "15661",
        "ok": "15661",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "15661",
        "ok": "15661",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "15661",
        "ok": "15661",
        "ko": "-"
    },
    "percentiles2": {
        "total": "15661",
        "ok": "15661",
        "ko": "-"
    },
    "percentiles3": {
        "total": "15661",
        "ok": "15661",
        "ko": "-"
    },
    "percentiles4": {
        "total": "15661",
        "ok": "15661",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-1d50e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_107593",
path: "Animal-api sync for customer NL_107593",
pathFormatted: "req_animal-api-sync-1d50e",
stats: {
    "name": "Animal-api sync for customer NL_107593",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "16565",
        "ok": "16565",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "16565",
        "ok": "16565",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "16565",
        "ok": "16565",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "16565",
        "ok": "16565",
        "ko": "-"
    },
    "percentiles2": {
        "total": "16565",
        "ok": "16565",
        "ko": "-"
    },
    "percentiles3": {
        "total": "16565",
        "ok": "16565",
        "ko": "-"
    },
    "percentiles4": {
        "total": "16565",
        "ok": "16565",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-3fad8": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_119768",
path: "Animal-api sync for customer NL_119768",
pathFormatted: "req_animal-api-sync-3fad8",
stats: {
    "name": "Animal-api sync for customer NL_119768",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "7803",
        "ok": "7803",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "7803",
        "ok": "7803",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "7803",
        "ok": "7803",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "7803",
        "ok": "7803",
        "ko": "-"
    },
    "percentiles2": {
        "total": "7803",
        "ok": "7803",
        "ko": "-"
    },
    "percentiles3": {
        "total": "7803",
        "ok": "7803",
        "ko": "-"
    },
    "percentiles4": {
        "total": "7803",
        "ok": "7803",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-1cdda": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_106961",
path: "Animal-api sync for customer NL_106961",
pathFormatted: "req_animal-api-sync-1cdda",
stats: {
    "name": "Animal-api sync for customer NL_106961",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "6222",
        "ok": "6222",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "6222",
        "ok": "6222",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "6222",
        "ok": "6222",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "6222",
        "ok": "6222",
        "ko": "-"
    },
    "percentiles2": {
        "total": "6222",
        "ok": "6222",
        "ko": "-"
    },
    "percentiles3": {
        "total": "6222",
        "ok": "6222",
        "ko": "-"
    },
    "percentiles4": {
        "total": "6222",
        "ok": "6222",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-028d0": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_139052",
path: "Animal-api sync for customer NL_139052",
pathFormatted: "req_animal-api-sync-028d0",
stats: {
    "name": "Animal-api sync for customer NL_139052",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3113",
        "ok": "3113",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3113",
        "ok": "3113",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3113",
        "ok": "3113",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3113",
        "ok": "3113",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3113",
        "ok": "3113",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3113",
        "ok": "3113",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3113",
        "ok": "3113",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-3df0e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_136411",
path: "Animal-api sync for customer NL_136411",
pathFormatted: "req_animal-api-sync-3df0e",
stats: {
    "name": "Animal-api sync for customer NL_136411",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "32250",
        "ok": "32250",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "32250",
        "ok": "32250",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "32250",
        "ok": "32250",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "32250",
        "ok": "32250",
        "ko": "-"
    },
    "percentiles2": {
        "total": "32250",
        "ok": "32250",
        "ko": "-"
    },
    "percentiles3": {
        "total": "32250",
        "ok": "32250",
        "ko": "-"
    },
    "percentiles4": {
        "total": "32250",
        "ok": "32250",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-34074": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104611",
path: "Animal-api sync for customer NL_104611",
pathFormatted: "req_animal-api-sync-34074",
stats: {
    "name": "Animal-api sync for customer NL_104611",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "23108",
        "ok": "23108",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "23108",
        "ok": "23108",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "23108",
        "ok": "23108",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "23108",
        "ok": "23108",
        "ko": "-"
    },
    "percentiles2": {
        "total": "23108",
        "ok": "23108",
        "ko": "-"
    },
    "percentiles3": {
        "total": "23108",
        "ok": "23108",
        "ko": "-"
    },
    "percentiles4": {
        "total": "23108",
        "ok": "23108",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-4e4dd": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_132571",
path: "Animal-api sync for customer NL_132571",
pathFormatted: "req_animal-api-sync-4e4dd",
stats: {
    "name": "Animal-api sync for customer NL_132571",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4251",
        "ok": "4251",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4251",
        "ok": "4251",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4251",
        "ok": "4251",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4251",
        "ok": "4251",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4251",
        "ok": "4251",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4251",
        "ok": "4251",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4251",
        "ok": "4251",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b76bc": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_121619",
path: "Animal-api sync for customer NL_121619",
pathFormatted: "req_animal-api-sync-b76bc",
stats: {
    "name": "Animal-api sync for customer NL_121619",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "37738",
        "ok": "37738",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "37738",
        "ok": "37738",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "37738",
        "ok": "37738",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "37738",
        "ok": "37738",
        "ko": "-"
    },
    "percentiles2": {
        "total": "37738",
        "ok": "37738",
        "ko": "-"
    },
    "percentiles3": {
        "total": "37738",
        "ok": "37738",
        "ko": "-"
    },
    "percentiles4": {
        "total": "37738",
        "ok": "37738",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-67b51": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_208384",
path: "Animal-api sync for customer NL_208384",
pathFormatted: "req_animal-api-sync-67b51",
stats: {
    "name": "Animal-api sync for customer NL_208384",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "18001",
        "ok": "18001",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "18001",
        "ok": "18001",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "18001",
        "ok": "18001",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "18001",
        "ok": "18001",
        "ko": "-"
    },
    "percentiles2": {
        "total": "18001",
        "ok": "18001",
        "ko": "-"
    },
    "percentiles3": {
        "total": "18001",
        "ok": "18001",
        "ko": "-"
    },
    "percentiles4": {
        "total": "18001",
        "ok": "18001",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-f5467": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_164463",
path: "Animal-api sync for customer NL_164463",
pathFormatted: "req_animal-api-sync-f5467",
stats: {
    "name": "Animal-api sync for customer NL_164463",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "12624",
        "ok": "12624",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "12624",
        "ok": "12624",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "12624",
        "ok": "12624",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "12624",
        "ok": "12624",
        "ko": "-"
    },
    "percentiles2": {
        "total": "12624",
        "ok": "12624",
        "ko": "-"
    },
    "percentiles3": {
        "total": "12624",
        "ok": "12624",
        "ko": "-"
    },
    "percentiles4": {
        "total": "12624",
        "ok": "12624",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a921a": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_158726",
path: "Animal-api sync for customer BE_158726",
pathFormatted: "req_animal-api-sync-a921a",
stats: {
    "name": "Animal-api sync for customer BE_158726",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "11792",
        "ok": "11792",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "11792",
        "ok": "11792",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "11792",
        "ok": "11792",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "11792",
        "ok": "11792",
        "ko": "-"
    },
    "percentiles2": {
        "total": "11792",
        "ok": "11792",
        "ko": "-"
    },
    "percentiles3": {
        "total": "11792",
        "ok": "11792",
        "ko": "-"
    },
    "percentiles4": {
        "total": "11792",
        "ok": "11792",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-43d17": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_159367",
path: "Animal-api sync for customer NL_159367",
pathFormatted: "req_animal-api-sync-43d17",
stats: {
    "name": "Animal-api sync for customer NL_159367",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3356",
        "ok": "3356",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3356",
        "ok": "3356",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3356",
        "ok": "3356",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3356",
        "ok": "3356",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3356",
        "ok": "3356",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3356",
        "ok": "3356",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3356",
        "ok": "3356",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-7b4fd": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_207082",
path: "Animal-api sync for customer BE_207082",
pathFormatted: "req_animal-api-sync-7b4fd",
stats: {
    "name": "Animal-api sync for customer BE_207082",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "38123",
        "ok": "38123",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "38123",
        "ok": "38123",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "38123",
        "ok": "38123",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "38123",
        "ok": "38123",
        "ko": "-"
    },
    "percentiles2": {
        "total": "38123",
        "ok": "38123",
        "ko": "-"
    },
    "percentiles3": {
        "total": "38123",
        "ok": "38123",
        "ko": "-"
    },
    "percentiles4": {
        "total": "38123",
        "ok": "38123",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-774bc": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105254",
path: "Animal-api sync for customer NL_105254",
pathFormatted: "req_animal-api-sync-774bc",
stats: {
    "name": "Animal-api sync for customer NL_105254",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "11134",
        "ok": "11134",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "11134",
        "ok": "11134",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "11134",
        "ok": "11134",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "11134",
        "ok": "11134",
        "ko": "-"
    },
    "percentiles2": {
        "total": "11134",
        "ok": "11134",
        "ko": "-"
    },
    "percentiles3": {
        "total": "11134",
        "ok": "11134",
        "ko": "-"
    },
    "percentiles4": {
        "total": "11134",
        "ok": "11134",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-72b5a": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_129227",
path: "Animal-api sync for customer NL_129227",
pathFormatted: "req_animal-api-sync-72b5a",
stats: {
    "name": "Animal-api sync for customer NL_129227",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "11519",
        "ok": "11519",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "11519",
        "ok": "11519",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "11519",
        "ok": "11519",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "11519",
        "ok": "11519",
        "ko": "-"
    },
    "percentiles2": {
        "total": "11519",
        "ok": "11519",
        "ko": "-"
    },
    "percentiles3": {
        "total": "11519",
        "ok": "11519",
        "ko": "-"
    },
    "percentiles4": {
        "total": "11519",
        "ok": "11519",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e7477": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_120288",
path: "Animal-api sync for customer NL_120288",
pathFormatted: "req_animal-api-sync-e7477",
stats: {
    "name": "Animal-api sync for customer NL_120288",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "12550",
        "ok": "12550",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "12550",
        "ok": "12550",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "12550",
        "ok": "12550",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "12550",
        "ok": "12550",
        "ko": "-"
    },
    "percentiles2": {
        "total": "12550",
        "ok": "12550",
        "ko": "-"
    },
    "percentiles3": {
        "total": "12550",
        "ok": "12550",
        "ko": "-"
    },
    "percentiles4": {
        "total": "12550",
        "ok": "12550",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b6fe1": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_112227",
path: "Animal-api sync for customer NL_112227",
pathFormatted: "req_animal-api-sync-b6fe1",
stats: {
    "name": "Animal-api sync for customer NL_112227",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "13289",
        "ok": "13289",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "13289",
        "ok": "13289",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "13289",
        "ok": "13289",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "13289",
        "ok": "13289",
        "ko": "-"
    },
    "percentiles2": {
        "total": "13289",
        "ok": "13289",
        "ko": "-"
    },
    "percentiles3": {
        "total": "13289",
        "ok": "13289",
        "ko": "-"
    },
    "percentiles4": {
        "total": "13289",
        "ok": "13289",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-6891c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111812",
path: "Animal-api sync for customer NL_111812",
pathFormatted: "req_animal-api-sync-6891c",
stats: {
    "name": "Animal-api sync for customer NL_111812",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "16198",
        "ok": "16198",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "16198",
        "ok": "16198",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "16198",
        "ok": "16198",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "16198",
        "ok": "16198",
        "ko": "-"
    },
    "percentiles2": {
        "total": "16198",
        "ok": "16198",
        "ko": "-"
    },
    "percentiles3": {
        "total": "16198",
        "ok": "16198",
        "ko": "-"
    },
    "percentiles4": {
        "total": "16198",
        "ok": "16198",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-5324e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_112685",
path: "Animal-api sync for customer NL_112685",
pathFormatted: "req_animal-api-sync-5324e",
stats: {
    "name": "Animal-api sync for customer NL_112685",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "6798",
        "ok": "6798",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "6798",
        "ok": "6798",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "6798",
        "ok": "6798",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "6798",
        "ok": "6798",
        "ko": "-"
    },
    "percentiles2": {
        "total": "6798",
        "ok": "6798",
        "ko": "-"
    },
    "percentiles3": {
        "total": "6798",
        "ok": "6798",
        "ko": "-"
    },
    "percentiles4": {
        "total": "6798",
        "ok": "6798",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-39487": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_153606",
path: "Animal-api sync for customer BE_153606",
pathFormatted: "req_animal-api-sync-39487",
stats: {
    "name": "Animal-api sync for customer BE_153606",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "11857",
        "ok": "11857",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "11857",
        "ok": "11857",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "11857",
        "ok": "11857",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "11857",
        "ok": "11857",
        "ko": "-"
    },
    "percentiles2": {
        "total": "11857",
        "ok": "11857",
        "ko": "-"
    },
    "percentiles3": {
        "total": "11857",
        "ok": "11857",
        "ko": "-"
    },
    "percentiles4": {
        "total": "11857",
        "ok": "11857",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-115a1": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_106548",
path: "Animal-api sync for customer NL_106548",
pathFormatted: "req_animal-api-sync-115a1",
stats: {
    "name": "Animal-api sync for customer NL_106548",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "13895",
        "ok": "13895",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "13895",
        "ok": "13895",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "13895",
        "ok": "13895",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "13895",
        "ok": "13895",
        "ko": "-"
    },
    "percentiles2": {
        "total": "13895",
        "ok": "13895",
        "ko": "-"
    },
    "percentiles3": {
        "total": "13895",
        "ok": "13895",
        "ko": "-"
    },
    "percentiles4": {
        "total": "13895",
        "ok": "13895",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-dfd7c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_139331",
path: "Animal-api sync for customer NL_139331",
pathFormatted: "req_animal-api-sync-dfd7c",
stats: {
    "name": "Animal-api sync for customer NL_139331",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "12226",
        "ok": "12226",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "12226",
        "ok": "12226",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "12226",
        "ok": "12226",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "12226",
        "ok": "12226",
        "ko": "-"
    },
    "percentiles2": {
        "total": "12226",
        "ok": "12226",
        "ko": "-"
    },
    "percentiles3": {
        "total": "12226",
        "ok": "12226",
        "ko": "-"
    },
    "percentiles4": {
        "total": "12226",
        "ok": "12226",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c6932": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_133968",
path: "Animal-api sync for customer NL_133968",
pathFormatted: "req_animal-api-sync-c6932",
stats: {
    "name": "Animal-api sync for customer NL_133968",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "9983",
        "ok": "9983",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "9983",
        "ok": "9983",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "9983",
        "ok": "9983",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "9983",
        "ok": "9983",
        "ko": "-"
    },
    "percentiles2": {
        "total": "9983",
        "ok": "9983",
        "ko": "-"
    },
    "percentiles3": {
        "total": "9983",
        "ok": "9983",
        "ko": "-"
    },
    "percentiles4": {
        "total": "9983",
        "ok": "9983",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-4cd78": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111414",
path: "Animal-api sync for customer NL_111414",
pathFormatted: "req_animal-api-sync-4cd78",
stats: {
    "name": "Animal-api sync for customer NL_111414",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "27986",
        "ok": "27986",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "27986",
        "ok": "27986",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "27986",
        "ok": "27986",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "27986",
        "ok": "27986",
        "ko": "-"
    },
    "percentiles2": {
        "total": "27986",
        "ok": "27986",
        "ko": "-"
    },
    "percentiles3": {
        "total": "27986",
        "ok": "27986",
        "ko": "-"
    },
    "percentiles4": {
        "total": "27986",
        "ok": "27986",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-0fed7": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_109760",
path: "Animal-api sync for customer NL_109760",
pathFormatted: "req_animal-api-sync-0fed7",
stats: {
    "name": "Animal-api sync for customer NL_109760",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "10295",
        "ok": "10295",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "10295",
        "ok": "10295",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "10295",
        "ok": "10295",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "10295",
        "ok": "10295",
        "ko": "-"
    },
    "percentiles2": {
        "total": "10295",
        "ok": "10295",
        "ko": "-"
    },
    "percentiles3": {
        "total": "10295",
        "ok": "10295",
        "ko": "-"
    },
    "percentiles4": {
        "total": "10295",
        "ok": "10295",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-f47e6": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_120627",
path: "Animal-api sync for customer NL_120627",
pathFormatted: "req_animal-api-sync-f47e6",
stats: {
    "name": "Animal-api sync for customer NL_120627",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "22156",
        "ok": "22156",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "22156",
        "ok": "22156",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "22156",
        "ok": "22156",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "22156",
        "ok": "22156",
        "ko": "-"
    },
    "percentiles2": {
        "total": "22156",
        "ok": "22156",
        "ko": "-"
    },
    "percentiles3": {
        "total": "22156",
        "ok": "22156",
        "ko": "-"
    },
    "percentiles4": {
        "total": "22156",
        "ok": "22156",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-66c16": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_112114",
path: "Animal-api sync for customer NL_112114",
pathFormatted: "req_animal-api-sync-66c16",
stats: {
    "name": "Animal-api sync for customer NL_112114",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5186",
        "ok": "5186",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5186",
        "ok": "5186",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5186",
        "ok": "5186",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5186",
        "ok": "5186",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5186",
        "ok": "5186",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5186",
        "ok": "5186",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5186",
        "ok": "5186",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-79b8e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_116848",
path: "Animal-api sync for customer NL_116848",
pathFormatted: "req_animal-api-sync-79b8e",
stats: {
    "name": "Animal-api sync for customer NL_116848",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "27488",
        "ok": "27488",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "27488",
        "ok": "27488",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "27488",
        "ok": "27488",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "27488",
        "ok": "27488",
        "ko": "-"
    },
    "percentiles2": {
        "total": "27488",
        "ok": "27488",
        "ko": "-"
    },
    "percentiles3": {
        "total": "27488",
        "ok": "27488",
        "ko": "-"
    },
    "percentiles4": {
        "total": "27488",
        "ok": "27488",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-5420f": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110280",
path: "Animal-api sync for customer NL_110280",
pathFormatted: "req_animal-api-sync-5420f",
stats: {
    "name": "Animal-api sync for customer NL_110280",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "24442",
        "ok": "24442",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "24442",
        "ok": "24442",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "24442",
        "ok": "24442",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "24442",
        "ok": "24442",
        "ko": "-"
    },
    "percentiles2": {
        "total": "24442",
        "ok": "24442",
        "ko": "-"
    },
    "percentiles3": {
        "total": "24442",
        "ok": "24442",
        "ko": "-"
    },
    "percentiles4": {
        "total": "24442",
        "ok": "24442",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-63b47": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111966",
path: "Animal-api sync for customer NL_111966",
pathFormatted: "req_animal-api-sync-63b47",
stats: {
    "name": "Animal-api sync for customer NL_111966",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "22018",
        "ok": "22018",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "22018",
        "ok": "22018",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "22018",
        "ok": "22018",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "22018",
        "ok": "22018",
        "ko": "-"
    },
    "percentiles2": {
        "total": "22018",
        "ok": "22018",
        "ko": "-"
    },
    "percentiles3": {
        "total": "22018",
        "ok": "22018",
        "ko": "-"
    },
    "percentiles4": {
        "total": "22018",
        "ok": "22018",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-fbcf4": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_128711",
path: "Animal-api sync for customer NL_128711",
pathFormatted: "req_animal-api-sync-fbcf4",
stats: {
    "name": "Animal-api sync for customer NL_128711",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "9277",
        "ok": "9277",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "9277",
        "ok": "9277",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "9277",
        "ok": "9277",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "9277",
        "ok": "9277",
        "ko": "-"
    },
    "percentiles2": {
        "total": "9277",
        "ok": "9277",
        "ko": "-"
    },
    "percentiles3": {
        "total": "9277",
        "ok": "9277",
        "ko": "-"
    },
    "percentiles4": {
        "total": "9277",
        "ok": "9277",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d05fc": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_131083",
path: "Animal-api sync for customer NL_131083",
pathFormatted: "req_animal-api-sync-d05fc",
stats: {
    "name": "Animal-api sync for customer NL_131083",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "18368",
        "ok": "18368",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "18368",
        "ok": "18368",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "18368",
        "ok": "18368",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "18368",
        "ok": "18368",
        "ko": "-"
    },
    "percentiles2": {
        "total": "18368",
        "ok": "18368",
        "ko": "-"
    },
    "percentiles3": {
        "total": "18368",
        "ok": "18368",
        "ko": "-"
    },
    "percentiles4": {
        "total": "18368",
        "ok": "18368",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-ed470": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_115514",
path: "Animal-api sync for customer NL_115514",
pathFormatted: "req_animal-api-sync-ed470",
stats: {
    "name": "Animal-api sync for customer NL_115514",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "9697",
        "ok": "9697",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "9697",
        "ok": "9697",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "9697",
        "ok": "9697",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "9697",
        "ok": "9697",
        "ko": "-"
    },
    "percentiles2": {
        "total": "9697",
        "ok": "9697",
        "ko": "-"
    },
    "percentiles3": {
        "total": "9697",
        "ok": "9697",
        "ko": "-"
    },
    "percentiles4": {
        "total": "9697",
        "ok": "9697",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-29375": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_121682",
path: "Animal-api sync for customer NL_121682",
pathFormatted: "req_animal-api-sync-29375",
stats: {
    "name": "Animal-api sync for customer NL_121682",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1846",
        "ok": "1846",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1846",
        "ok": "1846",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1846",
        "ok": "1846",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1846",
        "ok": "1846",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1846",
        "ok": "1846",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1846",
        "ok": "1846",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1846",
        "ok": "1846",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-4e713": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111266",
path: "Animal-api sync for customer NL_111266",
pathFormatted: "req_animal-api-sync-4e713",
stats: {
    "name": "Animal-api sync for customer NL_111266",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "15185",
        "ok": "15185",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "15185",
        "ok": "15185",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "15185",
        "ok": "15185",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "15185",
        "ok": "15185",
        "ko": "-"
    },
    "percentiles2": {
        "total": "15185",
        "ok": "15185",
        "ko": "-"
    },
    "percentiles3": {
        "total": "15185",
        "ok": "15185",
        "ko": "-"
    },
    "percentiles4": {
        "total": "15185",
        "ok": "15185",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-5df96": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_114543",
path: "Animal-api sync for customer NL_114543",
pathFormatted: "req_animal-api-sync-5df96",
stats: {
    "name": "Animal-api sync for customer NL_114543",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3355",
        "ok": "3355",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3355",
        "ok": "3355",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3355",
        "ok": "3355",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3355",
        "ok": "3355",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3355",
        "ok": "3355",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3355",
        "ok": "3355",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3355",
        "ok": "3355",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-38246": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_118506",
path: "Animal-api sync for customer NL_118506",
pathFormatted: "req_animal-api-sync-38246",
stats: {
    "name": "Animal-api sync for customer NL_118506",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "6287",
        "ok": "6287",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "6287",
        "ok": "6287",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "6287",
        "ok": "6287",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "6287",
        "ok": "6287",
        "ko": "-"
    },
    "percentiles2": {
        "total": "6287",
        "ok": "6287",
        "ko": "-"
    },
    "percentiles3": {
        "total": "6287",
        "ok": "6287",
        "ko": "-"
    },
    "percentiles4": {
        "total": "6287",
        "ok": "6287",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-3d430": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_134950",
path: "Animal-api sync for customer NL_134950",
pathFormatted: "req_animal-api-sync-3d430",
stats: {
    "name": "Animal-api sync for customer NL_134950",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5124",
        "ok": "5124",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5124",
        "ok": "5124",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5124",
        "ok": "5124",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5124",
        "ok": "5124",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5124",
        "ok": "5124",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5124",
        "ok": "5124",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5124",
        "ok": "5124",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a667b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_103980",
path: "Animal-api sync for customer NL_103980",
pathFormatted: "req_animal-api-sync-a667b",
stats: {
    "name": "Animal-api sync for customer NL_103980",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60010",
        "ok": "-",
        "ko": "60010"
    },
    "maxResponseTime": {
        "total": "60010",
        "ok": "-",
        "ko": "60010"
    },
    "meanResponseTime": {
        "total": "60010",
        "ok": "-",
        "ko": "60010"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60010",
        "ok": "-",
        "ko": "60010"
    },
    "percentiles2": {
        "total": "60010",
        "ok": "-",
        "ko": "60010"
    },
    "percentiles3": {
        "total": "60010",
        "ok": "-",
        "ko": "60010"
    },
    "percentiles4": {
        "total": "60010",
        "ok": "-",
        "ko": "60010"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-92bed": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105164",
path: "Animal-api sync for customer NL_105164",
pathFormatted: "req_animal-api-sync-92bed",
stats: {
    "name": "Animal-api sync for customer NL_105164",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "7869",
        "ok": "7869",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "7869",
        "ok": "7869",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "7869",
        "ok": "7869",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "7869",
        "ok": "7869",
        "ko": "-"
    },
    "percentiles2": {
        "total": "7869",
        "ok": "7869",
        "ko": "-"
    },
    "percentiles3": {
        "total": "7869",
        "ok": "7869",
        "ko": "-"
    },
    "percentiles4": {
        "total": "7869",
        "ok": "7869",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-74e33": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_189313",
path: "Animal-api sync for customer BE_189313",
pathFormatted: "req_animal-api-sync-74e33",
stats: {
    "name": "Animal-api sync for customer BE_189313",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "7532",
        "ok": "7532",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "7532",
        "ok": "7532",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "7532",
        "ok": "7532",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "7532",
        "ok": "7532",
        "ko": "-"
    },
    "percentiles2": {
        "total": "7532",
        "ok": "7532",
        "ko": "-"
    },
    "percentiles3": {
        "total": "7532",
        "ok": "7532",
        "ko": "-"
    },
    "percentiles4": {
        "total": "7532",
        "ok": "7532",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-aaf86": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_130803",
path: "Animal-api sync for customer NL_130803",
pathFormatted: "req_animal-api-sync-aaf86",
stats: {
    "name": "Animal-api sync for customer NL_130803",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "14736",
        "ok": "14736",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "14736",
        "ok": "14736",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "14736",
        "ok": "14736",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "14736",
        "ok": "14736",
        "ko": "-"
    },
    "percentiles2": {
        "total": "14736",
        "ok": "14736",
        "ko": "-"
    },
    "percentiles3": {
        "total": "14736",
        "ok": "14736",
        "ko": "-"
    },
    "percentiles4": {
        "total": "14736",
        "ok": "14736",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-390c0": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_113006",
path: "Animal-api sync for customer NL_113006",
pathFormatted: "req_animal-api-sync-390c0",
stats: {
    "name": "Animal-api sync for customer NL_113006",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4238",
        "ok": "4238",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4238",
        "ok": "4238",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4238",
        "ok": "4238",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4238",
        "ok": "4238",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4238",
        "ok": "4238",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4238",
        "ok": "4238",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4238",
        "ok": "4238",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-363df": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_120238",
path: "Animal-api sync for customer NL_120238",
pathFormatted: "req_animal-api-sync-363df",
stats: {
    "name": "Animal-api sync for customer NL_120238",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3749",
        "ok": "3749",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3749",
        "ok": "3749",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3749",
        "ok": "3749",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3749",
        "ok": "3749",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3749",
        "ok": "3749",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3749",
        "ok": "3749",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3749",
        "ok": "3749",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e70df": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_129794",
path: "Animal-api sync for customer NL_129794",
pathFormatted: "req_animal-api-sync-e70df",
stats: {
    "name": "Animal-api sync for customer NL_129794",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1764",
        "ok": "1764",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1764",
        "ok": "1764",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1764",
        "ok": "1764",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1764",
        "ok": "1764",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1764",
        "ok": "1764",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1764",
        "ok": "1764",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1764",
        "ok": "1764",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a9219": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_103449",
path: "Animal-api sync for customer NL_103449",
pathFormatted: "req_animal-api-sync-a9219",
stats: {
    "name": "Animal-api sync for customer NL_103449",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5355",
        "ok": "5355",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5355",
        "ok": "5355",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5355",
        "ok": "5355",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5355",
        "ok": "5355",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5355",
        "ok": "5355",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5355",
        "ok": "5355",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5355",
        "ok": "5355",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-30320": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_128704",
path: "Animal-api sync for customer NL_128704",
pathFormatted: "req_animal-api-sync-30320",
stats: {
    "name": "Animal-api sync for customer NL_128704",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4340",
        "ok": "4340",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4340",
        "ok": "4340",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4340",
        "ok": "4340",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4340",
        "ok": "4340",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4340",
        "ok": "4340",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4340",
        "ok": "4340",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4340",
        "ok": "4340",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c50f5": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111928",
path: "Animal-api sync for customer NL_111928",
pathFormatted: "req_animal-api-sync-c50f5",
stats: {
    "name": "Animal-api sync for customer NL_111928",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5318",
        "ok": "5318",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5318",
        "ok": "5318",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5318",
        "ok": "5318",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5318",
        "ok": "5318",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5318",
        "ok": "5318",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5318",
        "ok": "5318",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5318",
        "ok": "5318",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-294ca": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111076",
path: "Animal-api sync for customer NL_111076",
pathFormatted: "req_animal-api-sync-294ca",
stats: {
    "name": "Animal-api sync for customer NL_111076",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4639",
        "ok": "4639",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4639",
        "ok": "4639",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4639",
        "ok": "4639",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4639",
        "ok": "4639",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4639",
        "ok": "4639",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4639",
        "ok": "4639",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4639",
        "ok": "4639",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-080dc": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_131881",
path: "Animal-api sync for customer NL_131881",
pathFormatted: "req_animal-api-sync-080dc",
stats: {
    "name": "Animal-api sync for customer NL_131881",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1779",
        "ok": "1779",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1779",
        "ok": "1779",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1779",
        "ok": "1779",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1779",
        "ok": "1779",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1779",
        "ok": "1779",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1779",
        "ok": "1779",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1779",
        "ok": "1779",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-9f153": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_113848",
path: "Animal-api sync for customer NL_113848",
pathFormatted: "req_animal-api-sync-9f153",
stats: {
    "name": "Animal-api sync for customer NL_113848",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5825",
        "ok": "5825",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5825",
        "ok": "5825",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5825",
        "ok": "5825",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5825",
        "ok": "5825",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5825",
        "ok": "5825",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5825",
        "ok": "5825",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5825",
        "ok": "5825",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-084e7": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_108590",
path: "Animal-api sync for customer NL_108590",
pathFormatted: "req_animal-api-sync-084e7",
stats: {
    "name": "Animal-api sync for customer NL_108590",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4325",
        "ok": "4325",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4325",
        "ok": "4325",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4325",
        "ok": "4325",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4325",
        "ok": "4325",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4325",
        "ok": "4325",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4325",
        "ok": "4325",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4325",
        "ok": "4325",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-93bcd": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_107077",
path: "Animal-api sync for customer NL_107077",
pathFormatted: "req_animal-api-sync-93bcd",
stats: {
    "name": "Animal-api sync for customer NL_107077",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1987",
        "ok": "1987",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1987",
        "ok": "1987",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1987",
        "ok": "1987",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1987",
        "ok": "1987",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1987",
        "ok": "1987",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1987",
        "ok": "1987",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1987",
        "ok": "1987",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-43480": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_211973",
path: "Animal-api sync for customer BE_211973",
pathFormatted: "req_animal-api-sync-43480",
stats: {
    "name": "Animal-api sync for customer BE_211973",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "11165",
        "ok": "11165",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "11165",
        "ok": "11165",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "11165",
        "ok": "11165",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "11165",
        "ok": "11165",
        "ko": "-"
    },
    "percentiles2": {
        "total": "11165",
        "ok": "11165",
        "ko": "-"
    },
    "percentiles3": {
        "total": "11165",
        "ok": "11165",
        "ko": "-"
    },
    "percentiles4": {
        "total": "11165",
        "ok": "11165",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a5109": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_124277",
path: "Animal-api sync for customer NL_124277",
pathFormatted: "req_animal-api-sync-a5109",
stats: {
    "name": "Animal-api sync for customer NL_124277",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "17991",
        "ok": "17991",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "17991",
        "ok": "17991",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "17991",
        "ok": "17991",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "17991",
        "ok": "17991",
        "ko": "-"
    },
    "percentiles2": {
        "total": "17991",
        "ok": "17991",
        "ko": "-"
    },
    "percentiles3": {
        "total": "17991",
        "ok": "17991",
        "ko": "-"
    },
    "percentiles4": {
        "total": "17991",
        "ok": "17991",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-53304": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_108829",
path: "Animal-api sync for customer NL_108829",
pathFormatted: "req_animal-api-sync-53304",
stats: {
    "name": "Animal-api sync for customer NL_108829",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "20398",
        "ok": "20398",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "20398",
        "ok": "20398",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "20398",
        "ok": "20398",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "20398",
        "ok": "20398",
        "ko": "-"
    },
    "percentiles2": {
        "total": "20398",
        "ok": "20398",
        "ko": "-"
    },
    "percentiles3": {
        "total": "20398",
        "ok": "20398",
        "ko": "-"
    },
    "percentiles4": {
        "total": "20398",
        "ok": "20398",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-84fc1": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_131210",
path: "Animal-api sync for customer NL_131210",
pathFormatted: "req_animal-api-sync-84fc1",
stats: {
    "name": "Animal-api sync for customer NL_131210",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2120",
        "ok": "2120",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2120",
        "ok": "2120",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2120",
        "ok": "2120",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2120",
        "ok": "2120",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2120",
        "ok": "2120",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2120",
        "ok": "2120",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2120",
        "ok": "2120",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-fc8bc": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_119277",
path: "Animal-api sync for customer NL_119277",
pathFormatted: "req_animal-api-sync-fc8bc",
stats: {
    "name": "Animal-api sync for customer NL_119277",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2729",
        "ok": "2729",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2729",
        "ok": "2729",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2729",
        "ok": "2729",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2729",
        "ok": "2729",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2729",
        "ok": "2729",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2729",
        "ok": "2729",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2729",
        "ok": "2729",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b0637": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_109813",
path: "Animal-api sync for customer NL_109813",
pathFormatted: "req_animal-api-sync-b0637",
stats: {
    "name": "Animal-api sync for customer NL_109813",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "7234",
        "ok": "7234",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "7234",
        "ok": "7234",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "7234",
        "ok": "7234",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "7234",
        "ok": "7234",
        "ko": "-"
    },
    "percentiles2": {
        "total": "7234",
        "ok": "7234",
        "ko": "-"
    },
    "percentiles3": {
        "total": "7234",
        "ok": "7234",
        "ko": "-"
    },
    "percentiles4": {
        "total": "7234",
        "ok": "7234",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d7914": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_189320",
path: "Animal-api sync for customer NL_189320",
pathFormatted: "req_animal-api-sync-d7914",
stats: {
    "name": "Animal-api sync for customer NL_189320",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "6250",
        "ok": "6250",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "6250",
        "ok": "6250",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "6250",
        "ok": "6250",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "6250",
        "ok": "6250",
        "ko": "-"
    },
    "percentiles2": {
        "total": "6250",
        "ok": "6250",
        "ko": "-"
    },
    "percentiles3": {
        "total": "6250",
        "ok": "6250",
        "ko": "-"
    },
    "percentiles4": {
        "total": "6250",
        "ok": "6250",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-acdb8": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_113754",
path: "Animal-api sync for customer NL_113754",
pathFormatted: "req_animal-api-sync-acdb8",
stats: {
    "name": "Animal-api sync for customer NL_113754",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "22760",
        "ok": "22760",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "22760",
        "ok": "22760",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "22760",
        "ok": "22760",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "22760",
        "ok": "22760",
        "ko": "-"
    },
    "percentiles2": {
        "total": "22760",
        "ok": "22760",
        "ko": "-"
    },
    "percentiles3": {
        "total": "22760",
        "ok": "22760",
        "ko": "-"
    },
    "percentiles4": {
        "total": "22760",
        "ok": "22760",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-36730": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_135526",
path: "Animal-api sync for customer NL_135526",
pathFormatted: "req_animal-api-sync-36730",
stats: {
    "name": "Animal-api sync for customer NL_135526",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2868",
        "ok": "2868",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2868",
        "ok": "2868",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2868",
        "ok": "2868",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2868",
        "ok": "2868",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2868",
        "ok": "2868",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2868",
        "ok": "2868",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2868",
        "ok": "2868",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-1e909": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_154872",
path: "Animal-api sync for customer BE_154872",
pathFormatted: "req_animal-api-sync-1e909",
stats: {
    "name": "Animal-api sync for customer BE_154872",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3233",
        "ok": "3233",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3233",
        "ok": "3233",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3233",
        "ok": "3233",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3233",
        "ok": "3233",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3233",
        "ok": "3233",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3233",
        "ok": "3233",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3233",
        "ok": "3233",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-f54d2": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_125689",
path: "Animal-api sync for customer NL_125689",
pathFormatted: "req_animal-api-sync-f54d2",
stats: {
    "name": "Animal-api sync for customer NL_125689",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "10758",
        "ok": "10758",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "10758",
        "ok": "10758",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "10758",
        "ok": "10758",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "10758",
        "ok": "10758",
        "ko": "-"
    },
    "percentiles2": {
        "total": "10758",
        "ok": "10758",
        "ko": "-"
    },
    "percentiles3": {
        "total": "10758",
        "ok": "10758",
        "ko": "-"
    },
    "percentiles4": {
        "total": "10758",
        "ok": "10758",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-baa35": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110539",
path: "Animal-api sync for customer NL_110539",
pathFormatted: "req_animal-api-sync-baa35",
stats: {
    "name": "Animal-api sync for customer NL_110539",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "7271",
        "ok": "7271",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "7271",
        "ok": "7271",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "7271",
        "ok": "7271",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "7271",
        "ok": "7271",
        "ko": "-"
    },
    "percentiles2": {
        "total": "7271",
        "ok": "7271",
        "ko": "-"
    },
    "percentiles3": {
        "total": "7271",
        "ok": "7271",
        "ko": "-"
    },
    "percentiles4": {
        "total": "7271",
        "ok": "7271",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-14454": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104271",
path: "Animal-api sync for customer NL_104271",
pathFormatted: "req_animal-api-sync-14454",
stats: {
    "name": "Animal-api sync for customer NL_104271",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "13508",
        "ok": "13508",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "13508",
        "ok": "13508",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "13508",
        "ok": "13508",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "13508",
        "ok": "13508",
        "ko": "-"
    },
    "percentiles2": {
        "total": "13508",
        "ok": "13508",
        "ko": "-"
    },
    "percentiles3": {
        "total": "13508",
        "ok": "13508",
        "ko": "-"
    },
    "percentiles4": {
        "total": "13508",
        "ok": "13508",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b6907": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_123943",
path: "Animal-api sync for customer NL_123943",
pathFormatted: "req_animal-api-sync-b6907",
stats: {
    "name": "Animal-api sync for customer NL_123943",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "19637",
        "ok": "19637",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "19637",
        "ok": "19637",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "19637",
        "ok": "19637",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "19637",
        "ok": "19637",
        "ko": "-"
    },
    "percentiles2": {
        "total": "19637",
        "ok": "19637",
        "ko": "-"
    },
    "percentiles3": {
        "total": "19637",
        "ok": "19637",
        "ko": "-"
    },
    "percentiles4": {
        "total": "19637",
        "ok": "19637",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c4646": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110070",
path: "Animal-api sync for customer NL_110070",
pathFormatted: "req_animal-api-sync-c4646",
stats: {
    "name": "Animal-api sync for customer NL_110070",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "20274",
        "ok": "20274",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "20274",
        "ok": "20274",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "20274",
        "ok": "20274",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "20274",
        "ok": "20274",
        "ko": "-"
    },
    "percentiles2": {
        "total": "20274",
        "ok": "20274",
        "ko": "-"
    },
    "percentiles3": {
        "total": "20274",
        "ok": "20274",
        "ko": "-"
    },
    "percentiles4": {
        "total": "20274",
        "ok": "20274",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-0eeba": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_114348",
path: "Animal-api sync for customer NL_114348",
pathFormatted: "req_animal-api-sync-0eeba",
stats: {
    "name": "Animal-api sync for customer NL_114348",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "9516",
        "ok": "9516",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "9516",
        "ok": "9516",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "9516",
        "ok": "9516",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "9516",
        "ok": "9516",
        "ko": "-"
    },
    "percentiles2": {
        "total": "9516",
        "ok": "9516",
        "ko": "-"
    },
    "percentiles3": {
        "total": "9516",
        "ok": "9516",
        "ko": "-"
    },
    "percentiles4": {
        "total": "9516",
        "ok": "9516",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-5c35b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_114376",
path: "Animal-api sync for customer NL_114376",
pathFormatted: "req_animal-api-sync-5c35b",
stats: {
    "name": "Animal-api sync for customer NL_114376",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1616",
        "ok": "1616",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1616",
        "ok": "1616",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1616",
        "ok": "1616",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1616",
        "ok": "1616",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1616",
        "ok": "1616",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1616",
        "ok": "1616",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1616",
        "ok": "1616",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-451e8": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_121268",
path: "Animal-api sync for customer NL_121268",
pathFormatted: "req_animal-api-sync-451e8",
stats: {
    "name": "Animal-api sync for customer NL_121268",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "12778",
        "ok": "12778",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "12778",
        "ok": "12778",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "12778",
        "ok": "12778",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "12778",
        "ok": "12778",
        "ko": "-"
    },
    "percentiles2": {
        "total": "12778",
        "ok": "12778",
        "ko": "-"
    },
    "percentiles3": {
        "total": "12778",
        "ok": "12778",
        "ko": "-"
    },
    "percentiles4": {
        "total": "12778",
        "ok": "12778",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e42e2": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_128581",
path: "Animal-api sync for customer NL_128581",
pathFormatted: "req_animal-api-sync-e42e2",
stats: {
    "name": "Animal-api sync for customer NL_128581",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1936",
        "ok": "1936",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1936",
        "ok": "1936",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1936",
        "ok": "1936",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1936",
        "ok": "1936",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1936",
        "ok": "1936",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1936",
        "ok": "1936",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1936",
        "ok": "1936",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b164c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_120434",
path: "Animal-api sync for customer NL_120434",
pathFormatted: "req_animal-api-sync-b164c",
stats: {
    "name": "Animal-api sync for customer NL_120434",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "40799",
        "ok": "40799",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "40799",
        "ok": "40799",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "40799",
        "ok": "40799",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "40799",
        "ok": "40799",
        "ko": "-"
    },
    "percentiles2": {
        "total": "40799",
        "ok": "40799",
        "ko": "-"
    },
    "percentiles3": {
        "total": "40799",
        "ok": "40799",
        "ko": "-"
    },
    "percentiles4": {
        "total": "40799",
        "ok": "40799",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-7461a": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_120398",
path: "Animal-api sync for customer NL_120398",
pathFormatted: "req_animal-api-sync-7461a",
stats: {
    "name": "Animal-api sync for customer NL_120398",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5076",
        "ok": "5076",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5076",
        "ok": "5076",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5076",
        "ok": "5076",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5076",
        "ok": "5076",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5076",
        "ok": "5076",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5076",
        "ok": "5076",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5076",
        "ok": "5076",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-07a69": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_103947",
path: "Animal-api sync for customer NL_103947",
pathFormatted: "req_animal-api-sync-07a69",
stats: {
    "name": "Animal-api sync for customer NL_103947",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "22039",
        "ok": "22039",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "22039",
        "ok": "22039",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "22039",
        "ok": "22039",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "22039",
        "ok": "22039",
        "ko": "-"
    },
    "percentiles2": {
        "total": "22039",
        "ok": "22039",
        "ko": "-"
    },
    "percentiles3": {
        "total": "22039",
        "ok": "22039",
        "ko": "-"
    },
    "percentiles4": {
        "total": "22039",
        "ok": "22039",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c6405": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104467",
path: "Animal-api sync for customer NL_104467",
pathFormatted: "req_animal-api-sync-c6405",
stats: {
    "name": "Animal-api sync for customer NL_104467",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "8020",
        "ok": "8020",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "8020",
        "ok": "8020",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "8020",
        "ok": "8020",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "8020",
        "ok": "8020",
        "ko": "-"
    },
    "percentiles2": {
        "total": "8020",
        "ok": "8020",
        "ko": "-"
    },
    "percentiles3": {
        "total": "8020",
        "ok": "8020",
        "ko": "-"
    },
    "percentiles4": {
        "total": "8020",
        "ok": "8020",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-23f13": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_125273",
path: "Animal-api sync for customer NL_125273",
pathFormatted: "req_animal-api-sync-23f13",
stats: {
    "name": "Animal-api sync for customer NL_125273",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "10788",
        "ok": "10788",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "10788",
        "ok": "10788",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "10788",
        "ok": "10788",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "10788",
        "ok": "10788",
        "ko": "-"
    },
    "percentiles2": {
        "total": "10788",
        "ok": "10788",
        "ko": "-"
    },
    "percentiles3": {
        "total": "10788",
        "ok": "10788",
        "ko": "-"
    },
    "percentiles4": {
        "total": "10788",
        "ok": "10788",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-96127": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_161584",
path: "Animal-api sync for customer BE_161584",
pathFormatted: "req_animal-api-sync-96127",
stats: {
    "name": "Animal-api sync for customer BE_161584",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "10273",
        "ok": "10273",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "10273",
        "ok": "10273",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "10273",
        "ok": "10273",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "10273",
        "ok": "10273",
        "ko": "-"
    },
    "percentiles2": {
        "total": "10273",
        "ok": "10273",
        "ko": "-"
    },
    "percentiles3": {
        "total": "10273",
        "ok": "10273",
        "ko": "-"
    },
    "percentiles4": {
        "total": "10273",
        "ok": "10273",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-614ac": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104734",
path: "Animal-api sync for customer NL_104734",
pathFormatted: "req_animal-api-sync-614ac",
stats: {
    "name": "Animal-api sync for customer NL_104734",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "23589",
        "ok": "23589",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "23589",
        "ok": "23589",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "23589",
        "ok": "23589",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "23589",
        "ok": "23589",
        "ko": "-"
    },
    "percentiles2": {
        "total": "23589",
        "ok": "23589",
        "ko": "-"
    },
    "percentiles3": {
        "total": "23589",
        "ok": "23589",
        "ko": "-"
    },
    "percentiles4": {
        "total": "23589",
        "ok": "23589",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-2d56c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110590",
path: "Animal-api sync for customer NL_110590",
pathFormatted: "req_animal-api-sync-2d56c",
stats: {
    "name": "Animal-api sync for customer NL_110590",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "7558",
        "ok": "7558",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "7558",
        "ok": "7558",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "7558",
        "ok": "7558",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "7558",
        "ok": "7558",
        "ko": "-"
    },
    "percentiles2": {
        "total": "7558",
        "ok": "7558",
        "ko": "-"
    },
    "percentiles3": {
        "total": "7558",
        "ok": "7558",
        "ko": "-"
    },
    "percentiles4": {
        "total": "7558",
        "ok": "7558",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e31c7": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_210459",
path: "Animal-api sync for customer NL_210459",
pathFormatted: "req_animal-api-sync-e31c7",
stats: {
    "name": "Animal-api sync for customer NL_210459",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "19258",
        "ok": "19258",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "19258",
        "ok": "19258",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "19258",
        "ok": "19258",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "19258",
        "ok": "19258",
        "ko": "-"
    },
    "percentiles2": {
        "total": "19258",
        "ok": "19258",
        "ko": "-"
    },
    "percentiles3": {
        "total": "19258",
        "ok": "19258",
        "ko": "-"
    },
    "percentiles4": {
        "total": "19258",
        "ok": "19258",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-7fca2": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_158798",
path: "Animal-api sync for customer NL_158798",
pathFormatted: "req_animal-api-sync-7fca2",
stats: {
    "name": "Animal-api sync for customer NL_158798",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "12274",
        "ok": "12274",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "12274",
        "ok": "12274",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "12274",
        "ok": "12274",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "12274",
        "ok": "12274",
        "ko": "-"
    },
    "percentiles2": {
        "total": "12274",
        "ok": "12274",
        "ko": "-"
    },
    "percentiles3": {
        "total": "12274",
        "ok": "12274",
        "ko": "-"
    },
    "percentiles4": {
        "total": "12274",
        "ok": "12274",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b8f29": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_153558",
path: "Animal-api sync for customer BE_153558",
pathFormatted: "req_animal-api-sync-b8f29",
stats: {
    "name": "Animal-api sync for customer BE_153558",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2979",
        "ok": "2979",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2979",
        "ok": "2979",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2979",
        "ok": "2979",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2979",
        "ok": "2979",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2979",
        "ok": "2979",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2979",
        "ok": "2979",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2979",
        "ok": "2979",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-aa22e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_137034",
path: "Animal-api sync for customer NL_137034",
pathFormatted: "req_animal-api-sync-aa22e",
stats: {
    "name": "Animal-api sync for customer NL_137034",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1348",
        "ok": "1348",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1348",
        "ok": "1348",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1348",
        "ok": "1348",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1348",
        "ok": "1348",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1348",
        "ok": "1348",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1348",
        "ok": "1348",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1348",
        "ok": "1348",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b5283": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_112270",
path: "Animal-api sync for customer NL_112270",
pathFormatted: "req_animal-api-sync-b5283",
stats: {
    "name": "Animal-api sync for customer NL_112270",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "37200",
        "ok": "37200",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "37200",
        "ok": "37200",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "37200",
        "ok": "37200",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "37200",
        "ok": "37200",
        "ko": "-"
    },
    "percentiles2": {
        "total": "37200",
        "ok": "37200",
        "ko": "-"
    },
    "percentiles3": {
        "total": "37200",
        "ok": "37200",
        "ko": "-"
    },
    "percentiles4": {
        "total": "37200",
        "ok": "37200",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-10e14": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_113903",
path: "Animal-api sync for customer NL_113903",
pathFormatted: "req_animal-api-sync-10e14",
stats: {
    "name": "Animal-api sync for customer NL_113903",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "15922",
        "ok": "15922",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "15922",
        "ok": "15922",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "15922",
        "ok": "15922",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "15922",
        "ok": "15922",
        "ko": "-"
    },
    "percentiles2": {
        "total": "15922",
        "ok": "15922",
        "ko": "-"
    },
    "percentiles3": {
        "total": "15922",
        "ok": "15922",
        "ko": "-"
    },
    "percentiles4": {
        "total": "15922",
        "ok": "15922",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-3dd2c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105439",
path: "Animal-api sync for customer NL_105439",
pathFormatted: "req_animal-api-sync-3dd2c",
stats: {
    "name": "Animal-api sync for customer NL_105439",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "8098",
        "ok": "8098",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "8098",
        "ok": "8098",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "8098",
        "ok": "8098",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "8098",
        "ok": "8098",
        "ko": "-"
    },
    "percentiles2": {
        "total": "8098",
        "ok": "8098",
        "ko": "-"
    },
    "percentiles3": {
        "total": "8098",
        "ok": "8098",
        "ko": "-"
    },
    "percentiles4": {
        "total": "8098",
        "ok": "8098",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-8785d": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_207024",
path: "Animal-api sync for customer BE_207024",
pathFormatted: "req_animal-api-sync-8785d",
stats: {
    "name": "Animal-api sync for customer BE_207024",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3256",
        "ok": "3256",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3256",
        "ok": "3256",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3256",
        "ok": "3256",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3256",
        "ok": "3256",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3256",
        "ok": "3256",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3256",
        "ok": "3256",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3256",
        "ok": "3256",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-010fe": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_129945",
path: "Animal-api sync for customer NL_129945",
pathFormatted: "req_animal-api-sync-010fe",
stats: {
    "name": "Animal-api sync for customer NL_129945",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "7098",
        "ok": "7098",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "7098",
        "ok": "7098",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "7098",
        "ok": "7098",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "7098",
        "ok": "7098",
        "ko": "-"
    },
    "percentiles2": {
        "total": "7098",
        "ok": "7098",
        "ko": "-"
    },
    "percentiles3": {
        "total": "7098",
        "ok": "7098",
        "ko": "-"
    },
    "percentiles4": {
        "total": "7098",
        "ok": "7098",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-58bfa": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_131425",
path: "Animal-api sync for customer NL_131425",
pathFormatted: "req_animal-api-sync-58bfa",
stats: {
    "name": "Animal-api sync for customer NL_131425",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "15802",
        "ok": "15802",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "15802",
        "ok": "15802",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "15802",
        "ok": "15802",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "15802",
        "ok": "15802",
        "ko": "-"
    },
    "percentiles2": {
        "total": "15802",
        "ok": "15802",
        "ko": "-"
    },
    "percentiles3": {
        "total": "15802",
        "ok": "15802",
        "ko": "-"
    },
    "percentiles4": {
        "total": "15802",
        "ok": "15802",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-9a226": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_113819",
path: "Animal-api sync for customer NL_113819",
pathFormatted: "req_animal-api-sync-9a226",
stats: {
    "name": "Animal-api sync for customer NL_113819",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3105",
        "ok": "3105",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3105",
        "ok": "3105",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3105",
        "ok": "3105",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3105",
        "ok": "3105",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3105",
        "ok": "3105",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3105",
        "ok": "3105",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3105",
        "ok": "3105",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-44a53": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_121469",
path: "Animal-api sync for customer NL_121469",
pathFormatted: "req_animal-api-sync-44a53",
stats: {
    "name": "Animal-api sync for customer NL_121469",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "14702",
        "ok": "14702",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "14702",
        "ok": "14702",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "14702",
        "ok": "14702",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "14702",
        "ok": "14702",
        "ko": "-"
    },
    "percentiles2": {
        "total": "14702",
        "ok": "14702",
        "ko": "-"
    },
    "percentiles3": {
        "total": "14702",
        "ok": "14702",
        "ko": "-"
    },
    "percentiles4": {
        "total": "14702",
        "ok": "14702",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c3fd5": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104563",
path: "Animal-api sync for customer NL_104563",
pathFormatted: "req_animal-api-sync-c3fd5",
stats: {
    "name": "Animal-api sync for customer NL_104563",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "9898",
        "ok": "9898",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "9898",
        "ok": "9898",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "9898",
        "ok": "9898",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "9898",
        "ok": "9898",
        "ko": "-"
    },
    "percentiles2": {
        "total": "9898",
        "ok": "9898",
        "ko": "-"
    },
    "percentiles3": {
        "total": "9898",
        "ok": "9898",
        "ko": "-"
    },
    "percentiles4": {
        "total": "9898",
        "ok": "9898",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-2f86f": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_122640",
path: "Animal-api sync for customer NL_122640",
pathFormatted: "req_animal-api-sync-2f86f",
stats: {
    "name": "Animal-api sync for customer NL_122640",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "10001",
        "ok": "10001",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "10001",
        "ok": "10001",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "10001",
        "ok": "10001",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "10001",
        "ok": "10001",
        "ko": "-"
    },
    "percentiles2": {
        "total": "10001",
        "ok": "10001",
        "ko": "-"
    },
    "percentiles3": {
        "total": "10001",
        "ok": "10001",
        "ko": "-"
    },
    "percentiles4": {
        "total": "10001",
        "ok": "10001",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-447ce": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_136064",
path: "Animal-api sync for customer NL_136064",
pathFormatted: "req_animal-api-sync-447ce",
stats: {
    "name": "Animal-api sync for customer NL_136064",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4536",
        "ok": "4536",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4536",
        "ok": "4536",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4536",
        "ok": "4536",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4536",
        "ok": "4536",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4536",
        "ok": "4536",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4536",
        "ok": "4536",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4536",
        "ok": "4536",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-02723": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_106633",
path: "Animal-api sync for customer NL_106633",
pathFormatted: "req_animal-api-sync-02723",
stats: {
    "name": "Animal-api sync for customer NL_106633",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "7017",
        "ok": "7017",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "7017",
        "ok": "7017",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "7017",
        "ok": "7017",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "7017",
        "ok": "7017",
        "ko": "-"
    },
    "percentiles2": {
        "total": "7017",
        "ok": "7017",
        "ko": "-"
    },
    "percentiles3": {
        "total": "7017",
        "ok": "7017",
        "ko": "-"
    },
    "percentiles4": {
        "total": "7017",
        "ok": "7017",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-19174": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117339",
path: "Animal-api sync for customer NL_117339",
pathFormatted: "req_animal-api-sync-19174",
stats: {
    "name": "Animal-api sync for customer NL_117339",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "10449",
        "ok": "10449",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "10449",
        "ok": "10449",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "10449",
        "ok": "10449",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "10449",
        "ok": "10449",
        "ko": "-"
    },
    "percentiles2": {
        "total": "10449",
        "ok": "10449",
        "ko": "-"
    },
    "percentiles3": {
        "total": "10449",
        "ok": "10449",
        "ko": "-"
    },
    "percentiles4": {
        "total": "10449",
        "ok": "10449",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-9a47c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_115141",
path: "Animal-api sync for customer NL_115141",
pathFormatted: "req_animal-api-sync-9a47c",
stats: {
    "name": "Animal-api sync for customer NL_115141",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "16202",
        "ok": "16202",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "16202",
        "ok": "16202",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "16202",
        "ok": "16202",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "16202",
        "ok": "16202",
        "ko": "-"
    },
    "percentiles2": {
        "total": "16202",
        "ok": "16202",
        "ko": "-"
    },
    "percentiles3": {
        "total": "16202",
        "ok": "16202",
        "ko": "-"
    },
    "percentiles4": {
        "total": "16202",
        "ok": "16202",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-0a9fa": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_125735",
path: "Animal-api sync for customer NL_125735",
pathFormatted: "req_animal-api-sync-0a9fa",
stats: {
    "name": "Animal-api sync for customer NL_125735",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "6097",
        "ok": "6097",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "6097",
        "ok": "6097",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "6097",
        "ok": "6097",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "6097",
        "ok": "6097",
        "ko": "-"
    },
    "percentiles2": {
        "total": "6097",
        "ok": "6097",
        "ko": "-"
    },
    "percentiles3": {
        "total": "6097",
        "ok": "6097",
        "ko": "-"
    },
    "percentiles4": {
        "total": "6097",
        "ok": "6097",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-56dad": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_112805",
path: "Animal-api sync for customer NL_112805",
pathFormatted: "req_animal-api-sync-56dad",
stats: {
    "name": "Animal-api sync for customer NL_112805",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3649",
        "ok": "3649",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3649",
        "ok": "3649",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3649",
        "ok": "3649",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3649",
        "ok": "3649",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3649",
        "ok": "3649",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3649",
        "ok": "3649",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3649",
        "ok": "3649",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-4c3a5": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110894",
path: "Animal-api sync for customer NL_110894",
pathFormatted: "req_animal-api-sync-4c3a5",
stats: {
    "name": "Animal-api sync for customer NL_110894",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5547",
        "ok": "5547",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5547",
        "ok": "5547",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5547",
        "ok": "5547",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5547",
        "ok": "5547",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5547",
        "ok": "5547",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5547",
        "ok": "5547",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5547",
        "ok": "5547",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-86c49": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_145826",
path: "Animal-api sync for customer BE_145826",
pathFormatted: "req_animal-api-sync-86c49",
stats: {
    "name": "Animal-api sync for customer BE_145826",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3049",
        "ok": "3049",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3049",
        "ok": "3049",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3049",
        "ok": "3049",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3049",
        "ok": "3049",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3049",
        "ok": "3049",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3049",
        "ok": "3049",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3049",
        "ok": "3049",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-9e7ff": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110428",
path: "Animal-api sync for customer NL_110428",
pathFormatted: "req_animal-api-sync-9e7ff",
stats: {
    "name": "Animal-api sync for customer NL_110428",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "11503",
        "ok": "11503",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "11503",
        "ok": "11503",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "11503",
        "ok": "11503",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "11503",
        "ok": "11503",
        "ko": "-"
    },
    "percentiles2": {
        "total": "11503",
        "ok": "11503",
        "ko": "-"
    },
    "percentiles3": {
        "total": "11503",
        "ok": "11503",
        "ko": "-"
    },
    "percentiles4": {
        "total": "11503",
        "ok": "11503",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-6c072": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_115279",
path: "Animal-api sync for customer NL_115279",
pathFormatted: "req_animal-api-sync-6c072",
stats: {
    "name": "Animal-api sync for customer NL_115279",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "12700",
        "ok": "12700",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "12700",
        "ok": "12700",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "12700",
        "ok": "12700",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "12700",
        "ok": "12700",
        "ko": "-"
    },
    "percentiles2": {
        "total": "12700",
        "ok": "12700",
        "ko": "-"
    },
    "percentiles3": {
        "total": "12700",
        "ok": "12700",
        "ko": "-"
    },
    "percentiles4": {
        "total": "12700",
        "ok": "12700",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-2de80": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_129343",
path: "Animal-api sync for customer NL_129343",
pathFormatted: "req_animal-api-sync-2de80",
stats: {
    "name": "Animal-api sync for customer NL_129343",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5552",
        "ok": "5552",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5552",
        "ok": "5552",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5552",
        "ok": "5552",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5552",
        "ok": "5552",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5552",
        "ok": "5552",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5552",
        "ok": "5552",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5552",
        "ok": "5552",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-925cc": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_120686",
path: "Animal-api sync for customer NL_120686",
pathFormatted: "req_animal-api-sync-925cc",
stats: {
    "name": "Animal-api sync for customer NL_120686",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "23364",
        "ok": "23364",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "23364",
        "ok": "23364",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "23364",
        "ok": "23364",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "23364",
        "ok": "23364",
        "ko": "-"
    },
    "percentiles2": {
        "total": "23364",
        "ok": "23364",
        "ko": "-"
    },
    "percentiles3": {
        "total": "23364",
        "ok": "23364",
        "ko": "-"
    },
    "percentiles4": {
        "total": "23364",
        "ok": "23364",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-f6a28": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_129516",
path: "Animal-api sync for customer NL_129516",
pathFormatted: "req_animal-api-sync-f6a28",
stats: {
    "name": "Animal-api sync for customer NL_129516",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "23647",
        "ok": "23647",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "23647",
        "ok": "23647",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "23647",
        "ok": "23647",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "23647",
        "ok": "23647",
        "ko": "-"
    },
    "percentiles2": {
        "total": "23647",
        "ok": "23647",
        "ko": "-"
    },
    "percentiles3": {
        "total": "23647",
        "ok": "23647",
        "ko": "-"
    },
    "percentiles4": {
        "total": "23647",
        "ok": "23647",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-10907": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_155615",
path: "Animal-api sync for customer NL_155615",
pathFormatted: "req_animal-api-sync-10907",
stats: {
    "name": "Animal-api sync for customer NL_155615",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "19099",
        "ok": "19099",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "19099",
        "ok": "19099",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "19099",
        "ok": "19099",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "19099",
        "ok": "19099",
        "ko": "-"
    },
    "percentiles2": {
        "total": "19099",
        "ok": "19099",
        "ko": "-"
    },
    "percentiles3": {
        "total": "19099",
        "ok": "19099",
        "ko": "-"
    },
    "percentiles4": {
        "total": "19099",
        "ok": "19099",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-83fad": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_161486",
path: "Animal-api sync for customer NL_161486",
pathFormatted: "req_animal-api-sync-83fad",
stats: {
    "name": "Animal-api sync for customer NL_161486",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "9498",
        "ok": "9498",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "9498",
        "ok": "9498",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "9498",
        "ok": "9498",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "9498",
        "ok": "9498",
        "ko": "-"
    },
    "percentiles2": {
        "total": "9498",
        "ok": "9498",
        "ko": "-"
    },
    "percentiles3": {
        "total": "9498",
        "ok": "9498",
        "ko": "-"
    },
    "percentiles4": {
        "total": "9498",
        "ok": "9498",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-6eeed": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_136560",
path: "Animal-api sync for customer NL_136560",
pathFormatted: "req_animal-api-sync-6eeed",
stats: {
    "name": "Animal-api sync for customer NL_136560",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5510",
        "ok": "5510",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5510",
        "ok": "5510",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5510",
        "ok": "5510",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5510",
        "ok": "5510",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5510",
        "ok": "5510",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5510",
        "ok": "5510",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5510",
        "ok": "5510",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-678da": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_116304",
path: "Animal-api sync for customer NL_116304",
pathFormatted: "req_animal-api-sync-678da",
stats: {
    "name": "Animal-api sync for customer NL_116304",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "7603",
        "ok": "7603",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "7603",
        "ok": "7603",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "7603",
        "ok": "7603",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "7603",
        "ok": "7603",
        "ko": "-"
    },
    "percentiles2": {
        "total": "7603",
        "ok": "7603",
        "ko": "-"
    },
    "percentiles3": {
        "total": "7603",
        "ok": "7603",
        "ko": "-"
    },
    "percentiles4": {
        "total": "7603",
        "ok": "7603",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-23e9e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_136624",
path: "Animal-api sync for customer NL_136624",
pathFormatted: "req_animal-api-sync-23e9e",
stats: {
    "name": "Animal-api sync for customer NL_136624",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3552",
        "ok": "3552",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3552",
        "ok": "3552",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3552",
        "ok": "3552",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3552",
        "ok": "3552",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3552",
        "ok": "3552",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3552",
        "ok": "3552",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3552",
        "ok": "3552",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b45fa": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_160578",
path: "Animal-api sync for customer BE_160578",
pathFormatted: "req_animal-api-sync-b45fa",
stats: {
    "name": "Animal-api sync for customer BE_160578",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "13950",
        "ok": "13950",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "13950",
        "ok": "13950",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "13950",
        "ok": "13950",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "13950",
        "ok": "13950",
        "ko": "-"
    },
    "percentiles2": {
        "total": "13950",
        "ok": "13950",
        "ko": "-"
    },
    "percentiles3": {
        "total": "13950",
        "ok": "13950",
        "ko": "-"
    },
    "percentiles4": {
        "total": "13950",
        "ok": "13950",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-ed8de": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117329",
path: "Animal-api sync for customer NL_117329",
pathFormatted: "req_animal-api-sync-ed8de",
stats: {
    "name": "Animal-api sync for customer NL_117329",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "17699",
        "ok": "17699",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "17699",
        "ok": "17699",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "17699",
        "ok": "17699",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "17699",
        "ok": "17699",
        "ko": "-"
    },
    "percentiles2": {
        "total": "17699",
        "ok": "17699",
        "ko": "-"
    },
    "percentiles3": {
        "total": "17699",
        "ok": "17699",
        "ko": "-"
    },
    "percentiles4": {
        "total": "17699",
        "ok": "17699",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-7699b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_115653",
path: "Animal-api sync for customer NL_115653",
pathFormatted: "req_animal-api-sync-7699b",
stats: {
    "name": "Animal-api sync for customer NL_115653",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3798",
        "ok": "3798",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3798",
        "ok": "3798",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3798",
        "ok": "3798",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3798",
        "ok": "3798",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3798",
        "ok": "3798",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3798",
        "ok": "3798",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3798",
        "ok": "3798",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-f6750": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_145660",
path: "Animal-api sync for customer BE_145660",
pathFormatted: "req_animal-api-sync-f6750",
stats: {
    "name": "Animal-api sync for customer BE_145660",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "11800",
        "ok": "11800",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "11800",
        "ok": "11800",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "11800",
        "ok": "11800",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "11800",
        "ok": "11800",
        "ko": "-"
    },
    "percentiles2": {
        "total": "11800",
        "ok": "11800",
        "ko": "-"
    },
    "percentiles3": {
        "total": "11800",
        "ok": "11800",
        "ko": "-"
    },
    "percentiles4": {
        "total": "11800",
        "ok": "11800",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-de4e1": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_196422",
path: "Animal-api sync for customer BE_196422",
pathFormatted: "req_animal-api-sync-de4e1",
stats: {
    "name": "Animal-api sync for customer BE_196422",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2348",
        "ok": "2348",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2348",
        "ok": "2348",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2348",
        "ok": "2348",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2348",
        "ok": "2348",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2348",
        "ok": "2348",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2348",
        "ok": "2348",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2348",
        "ok": "2348",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-94458": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111031",
path: "Animal-api sync for customer NL_111031",
pathFormatted: "req_animal-api-sync-94458",
stats: {
    "name": "Animal-api sync for customer NL_111031",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5602",
        "ok": "5602",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5602",
        "ok": "5602",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5602",
        "ok": "5602",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5602",
        "ok": "5602",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5602",
        "ok": "5602",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5602",
        "ok": "5602",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5602",
        "ok": "5602",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-1114b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105737",
path: "Animal-api sync for customer NL_105737",
pathFormatted: "req_animal-api-sync-1114b",
stats: {
    "name": "Animal-api sync for customer NL_105737",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "6700",
        "ok": "6700",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "6700",
        "ok": "6700",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "6700",
        "ok": "6700",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "6700",
        "ok": "6700",
        "ko": "-"
    },
    "percentiles2": {
        "total": "6700",
        "ok": "6700",
        "ko": "-"
    },
    "percentiles3": {
        "total": "6700",
        "ok": "6700",
        "ko": "-"
    },
    "percentiles4": {
        "total": "6700",
        "ok": "6700",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-34261": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_112642",
path: "Animal-api sync for customer NL_112642",
pathFormatted: "req_animal-api-sync-34261",
stats: {
    "name": "Animal-api sync for customer NL_112642",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "8102",
        "ok": "8102",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "8102",
        "ok": "8102",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "8102",
        "ok": "8102",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "8102",
        "ok": "8102",
        "ko": "-"
    },
    "percentiles2": {
        "total": "8102",
        "ok": "8102",
        "ko": "-"
    },
    "percentiles3": {
        "total": "8102",
        "ok": "8102",
        "ko": "-"
    },
    "percentiles4": {
        "total": "8102",
        "ok": "8102",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-88106": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_112559",
path: "Animal-api sync for customer NL_112559",
pathFormatted: "req_animal-api-sync-88106",
stats: {
    "name": "Animal-api sync for customer NL_112559",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5152",
        "ok": "5152",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5152",
        "ok": "5152",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5152",
        "ok": "5152",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5152",
        "ok": "5152",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5152",
        "ok": "5152",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5152",
        "ok": "5152",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5152",
        "ok": "5152",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-12cf4": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_103806",
path: "Animal-api sync for customer NL_103806",
pathFormatted: "req_animal-api-sync-12cf4",
stats: {
    "name": "Animal-api sync for customer NL_103806",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5899",
        "ok": "5899",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5899",
        "ok": "5899",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5899",
        "ok": "5899",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5899",
        "ok": "5899",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5899",
        "ok": "5899",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5899",
        "ok": "5899",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5899",
        "ok": "5899",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-0c8bf": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_135056",
path: "Animal-api sync for customer NL_135056",
pathFormatted: "req_animal-api-sync-0c8bf",
stats: {
    "name": "Animal-api sync for customer NL_135056",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4461",
        "ok": "4461",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4461",
        "ok": "4461",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4461",
        "ok": "4461",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4461",
        "ok": "4461",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4461",
        "ok": "4461",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4461",
        "ok": "4461",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4461",
        "ok": "4461",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-cd315": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_115640",
path: "Animal-api sync for customer NL_115640",
pathFormatted: "req_animal-api-sync-cd315",
stats: {
    "name": "Animal-api sync for customer NL_115640",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3699",
        "ok": "3699",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3699",
        "ok": "3699",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3699",
        "ok": "3699",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3699",
        "ok": "3699",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3699",
        "ok": "3699",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3699",
        "ok": "3699",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3699",
        "ok": "3699",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-708ff": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111792",
path: "Animal-api sync for customer NL_111792",
pathFormatted: "req_animal-api-sync-708ff",
stats: {
    "name": "Animal-api sync for customer NL_111792",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "10203",
        "ok": "10203",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "10203",
        "ok": "10203",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "10203",
        "ok": "10203",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "10203",
        "ok": "10203",
        "ko": "-"
    },
    "percentiles2": {
        "total": "10203",
        "ok": "10203",
        "ko": "-"
    },
    "percentiles3": {
        "total": "10203",
        "ok": "10203",
        "ko": "-"
    },
    "percentiles4": {
        "total": "10203",
        "ok": "10203",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c74f5": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110479",
path: "Animal-api sync for customer NL_110479",
pathFormatted: "req_animal-api-sync-c74f5",
stats: {
    "name": "Animal-api sync for customer NL_110479",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "22652",
        "ok": "22652",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "22652",
        "ok": "22652",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "22652",
        "ok": "22652",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "22652",
        "ok": "22652",
        "ko": "-"
    },
    "percentiles2": {
        "total": "22652",
        "ok": "22652",
        "ko": "-"
    },
    "percentiles3": {
        "total": "22652",
        "ok": "22652",
        "ko": "-"
    },
    "percentiles4": {
        "total": "22652",
        "ok": "22652",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b4519": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110383",
path: "Animal-api sync for customer NL_110383",
pathFormatted: "req_animal-api-sync-b4519",
stats: {
    "name": "Animal-api sync for customer NL_110383",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5299",
        "ok": "5299",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5299",
        "ok": "5299",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5299",
        "ok": "5299",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5299",
        "ok": "5299",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5299",
        "ok": "5299",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5299",
        "ok": "5299",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5299",
        "ok": "5299",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-ccec3": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_159045",
path: "Animal-api sync for customer BE_159045",
pathFormatted: "req_animal-api-sync-ccec3",
stats: {
    "name": "Animal-api sync for customer BE_159045",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5261",
        "ok": "5261",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5261",
        "ok": "5261",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5261",
        "ok": "5261",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5261",
        "ok": "5261",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5261",
        "ok": "5261",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5261",
        "ok": "5261",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5261",
        "ok": "5261",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-f73d7": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_112171",
path: "Animal-api sync for customer NL_112171",
pathFormatted: "req_animal-api-sync-f73d7",
stats: {
    "name": "Animal-api sync for customer NL_112171",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "9853",
        "ok": "9853",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "9853",
        "ok": "9853",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "9853",
        "ok": "9853",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "9853",
        "ok": "9853",
        "ko": "-"
    },
    "percentiles2": {
        "total": "9853",
        "ok": "9853",
        "ko": "-"
    },
    "percentiles3": {
        "total": "9853",
        "ok": "9853",
        "ko": "-"
    },
    "percentiles4": {
        "total": "9853",
        "ok": "9853",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c61a4": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_158630",
path: "Animal-api sync for customer NL_158630",
pathFormatted: "req_animal-api-sync-c61a4",
stats: {
    "name": "Animal-api sync for customer NL_158630",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "12603",
        "ok": "12603",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "12603",
        "ok": "12603",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "12603",
        "ok": "12603",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "12603",
        "ok": "12603",
        "ko": "-"
    },
    "percentiles2": {
        "total": "12603",
        "ok": "12603",
        "ko": "-"
    },
    "percentiles3": {
        "total": "12603",
        "ok": "12603",
        "ko": "-"
    },
    "percentiles4": {
        "total": "12603",
        "ok": "12603",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-3df21": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111890",
path: "Animal-api sync for customer NL_111890",
pathFormatted: "req_animal-api-sync-3df21",
stats: {
    "name": "Animal-api sync for customer NL_111890",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "11311",
        "ok": "11311",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "11311",
        "ok": "11311",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "11311",
        "ok": "11311",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "11311",
        "ok": "11311",
        "ko": "-"
    },
    "percentiles2": {
        "total": "11311",
        "ok": "11311",
        "ko": "-"
    },
    "percentiles3": {
        "total": "11311",
        "ok": "11311",
        "ko": "-"
    },
    "percentiles4": {
        "total": "11311",
        "ok": "11311",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-11d0e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_119978",
path: "Animal-api sync for customer NL_119978",
pathFormatted: "req_animal-api-sync-11d0e",
stats: {
    "name": "Animal-api sync for customer NL_119978",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "13449",
        "ok": "13449",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "13449",
        "ok": "13449",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "13449",
        "ok": "13449",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "13449",
        "ok": "13449",
        "ko": "-"
    },
    "percentiles2": {
        "total": "13449",
        "ok": "13449",
        "ko": "-"
    },
    "percentiles3": {
        "total": "13449",
        "ok": "13449",
        "ko": "-"
    },
    "percentiles4": {
        "total": "13449",
        "ok": "13449",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-cd0f8": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117391",
path: "Animal-api sync for customer NL_117391",
pathFormatted: "req_animal-api-sync-cd0f8",
stats: {
    "name": "Animal-api sync for customer NL_117391",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "7097",
        "ok": "7097",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "7097",
        "ok": "7097",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "7097",
        "ok": "7097",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "7097",
        "ok": "7097",
        "ko": "-"
    },
    "percentiles2": {
        "total": "7097",
        "ok": "7097",
        "ko": "-"
    },
    "percentiles3": {
        "total": "7097",
        "ok": "7097",
        "ko": "-"
    },
    "percentiles4": {
        "total": "7097",
        "ok": "7097",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-997ea": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111277",
path: "Animal-api sync for customer NL_111277",
pathFormatted: "req_animal-api-sync-997ea",
stats: {
    "name": "Animal-api sync for customer NL_111277",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "7998",
        "ok": "7998",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "7998",
        "ok": "7998",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "7998",
        "ok": "7998",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "7998",
        "ok": "7998",
        "ko": "-"
    },
    "percentiles2": {
        "total": "7998",
        "ok": "7998",
        "ko": "-"
    },
    "percentiles3": {
        "total": "7998",
        "ok": "7998",
        "ko": "-"
    },
    "percentiles4": {
        "total": "7998",
        "ok": "7998",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-998da": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_120046",
path: "Animal-api sync for customer NL_120046",
pathFormatted: "req_animal-api-sync-998da",
stats: {
    "name": "Animal-api sync for customer NL_120046",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2902",
        "ok": "2902",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2902",
        "ok": "2902",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2902",
        "ok": "2902",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2902",
        "ok": "2902",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2902",
        "ok": "2902",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2902",
        "ok": "2902",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2902",
        "ok": "2902",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-08b09": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_122753",
path: "Animal-api sync for customer NL_122753",
pathFormatted: "req_animal-api-sync-08b09",
stats: {
    "name": "Animal-api sync for customer NL_122753",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "22751",
        "ok": "22751",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "22751",
        "ok": "22751",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "22751",
        "ok": "22751",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "22751",
        "ok": "22751",
        "ko": "-"
    },
    "percentiles2": {
        "total": "22751",
        "ok": "22751",
        "ko": "-"
    },
    "percentiles3": {
        "total": "22751",
        "ok": "22751",
        "ko": "-"
    },
    "percentiles4": {
        "total": "22751",
        "ok": "22751",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a5635": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_193658",
path: "Animal-api sync for customer BE_193658",
pathFormatted: "req_animal-api-sync-a5635",
stats: {
    "name": "Animal-api sync for customer BE_193658",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5018",
        "ok": "5018",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5018",
        "ok": "5018",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5018",
        "ok": "5018",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5018",
        "ok": "5018",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5018",
        "ok": "5018",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5018",
        "ok": "5018",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5018",
        "ok": "5018",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-f89f8": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_121091",
path: "Animal-api sync for customer NL_121091",
pathFormatted: "req_animal-api-sync-f89f8",
stats: {
    "name": "Animal-api sync for customer NL_121091",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2402",
        "ok": "2402",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2402",
        "ok": "2402",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2402",
        "ok": "2402",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2402",
        "ok": "2402",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2402",
        "ok": "2402",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2402",
        "ok": "2402",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2402",
        "ok": "2402",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-6c82d": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_143962",
path: "Animal-api sync for customer NL_143962",
pathFormatted: "req_animal-api-sync-6c82d",
stats: {
    "name": "Animal-api sync for customer NL_143962",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3552",
        "ok": "3552",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3552",
        "ok": "3552",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3552",
        "ok": "3552",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3552",
        "ok": "3552",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3552",
        "ok": "3552",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3552",
        "ok": "3552",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3552",
        "ok": "3552",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-afe85": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104819",
path: "Animal-api sync for customer NL_104819",
pathFormatted: "req_animal-api-sync-afe85",
stats: {
    "name": "Animal-api sync for customer NL_104819",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "9253",
        "ok": "9253",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "9253",
        "ok": "9253",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "9253",
        "ok": "9253",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "9253",
        "ok": "9253",
        "ko": "-"
    },
    "percentiles2": {
        "total": "9253",
        "ok": "9253",
        "ko": "-"
    },
    "percentiles3": {
        "total": "9253",
        "ok": "9253",
        "ko": "-"
    },
    "percentiles4": {
        "total": "9253",
        "ok": "9253",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d9f9d": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_156439",
path: "Animal-api sync for customer BE_156439",
pathFormatted: "req_animal-api-sync-d9f9d",
stats: {
    "name": "Animal-api sync for customer BE_156439",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "7203",
        "ok": "7203",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "7203",
        "ok": "7203",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "7203",
        "ok": "7203",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "7203",
        "ok": "7203",
        "ko": "-"
    },
    "percentiles2": {
        "total": "7203",
        "ok": "7203",
        "ko": "-"
    },
    "percentiles3": {
        "total": "7203",
        "ok": "7203",
        "ko": "-"
    },
    "percentiles4": {
        "total": "7203",
        "ok": "7203",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-038e6": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117080",
path: "Animal-api sync for customer NL_117080",
pathFormatted: "req_animal-api-sync-038e6",
stats: {
    "name": "Animal-api sync for customer NL_117080",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "7800",
        "ok": "7800",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "7800",
        "ok": "7800",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "7800",
        "ok": "7800",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "7800",
        "ok": "7800",
        "ko": "-"
    },
    "percentiles2": {
        "total": "7800",
        "ok": "7800",
        "ko": "-"
    },
    "percentiles3": {
        "total": "7800",
        "ok": "7800",
        "ko": "-"
    },
    "percentiles4": {
        "total": "7800",
        "ok": "7800",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-9a2c1": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_142403",
path: "Animal-api sync for customer NL_142403",
pathFormatted: "req_animal-api-sync-9a2c1",
stats: {
    "name": "Animal-api sync for customer NL_142403",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "11701",
        "ok": "11701",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "11701",
        "ok": "11701",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "11701",
        "ok": "11701",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "11701",
        "ok": "11701",
        "ko": "-"
    },
    "percentiles2": {
        "total": "11701",
        "ok": "11701",
        "ko": "-"
    },
    "percentiles3": {
        "total": "11701",
        "ok": "11701",
        "ko": "-"
    },
    "percentiles4": {
        "total": "11701",
        "ok": "11701",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e1749": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_121066",
path: "Animal-api sync for customer NL_121066",
pathFormatted: "req_animal-api-sync-e1749",
stats: {
    "name": "Animal-api sync for customer NL_121066",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5151",
        "ok": "5151",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5151",
        "ok": "5151",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5151",
        "ok": "5151",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5151",
        "ok": "5151",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5151",
        "ok": "5151",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5151",
        "ok": "5151",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5151",
        "ok": "5151",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c09d9": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_126693",
path: "Animal-api sync for customer NL_126693",
pathFormatted: "req_animal-api-sync-c09d9",
stats: {
    "name": "Animal-api sync for customer NL_126693",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "8153",
        "ok": "8153",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "8153",
        "ok": "8153",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "8153",
        "ok": "8153",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "8153",
        "ok": "8153",
        "ko": "-"
    },
    "percentiles2": {
        "total": "8153",
        "ok": "8153",
        "ko": "-"
    },
    "percentiles3": {
        "total": "8153",
        "ok": "8153",
        "ko": "-"
    },
    "percentiles4": {
        "total": "8153",
        "ok": "8153",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-cdc3c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_125737",
path: "Animal-api sync for customer NL_125737",
pathFormatted: "req_animal-api-sync-cdc3c",
stats: {
    "name": "Animal-api sync for customer NL_125737",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "10058",
        "ok": "10058",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "10058",
        "ok": "10058",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "10058",
        "ok": "10058",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "10058",
        "ok": "10058",
        "ko": "-"
    },
    "percentiles2": {
        "total": "10058",
        "ok": "10058",
        "ko": "-"
    },
    "percentiles3": {
        "total": "10058",
        "ok": "10058",
        "ko": "-"
    },
    "percentiles4": {
        "total": "10058",
        "ok": "10058",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-339e7": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_115945",
path: "Animal-api sync for customer NL_115945",
pathFormatted: "req_animal-api-sync-339e7",
stats: {
    "name": "Animal-api sync for customer NL_115945",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2302",
        "ok": "2302",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2302",
        "ok": "2302",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2302",
        "ok": "2302",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2302",
        "ok": "2302",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2302",
        "ok": "2302",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2302",
        "ok": "2302",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2302",
        "ok": "2302",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c1773": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105858",
path: "Animal-api sync for customer NL_105858",
pathFormatted: "req_animal-api-sync-c1773",
stats: {
    "name": "Animal-api sync for customer NL_105858",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "12150",
        "ok": "12150",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "12150",
        "ok": "12150",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "12150",
        "ok": "12150",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "12150",
        "ok": "12150",
        "ko": "-"
    },
    "percentiles2": {
        "total": "12150",
        "ok": "12150",
        "ko": "-"
    },
    "percentiles3": {
        "total": "12150",
        "ok": "12150",
        "ko": "-"
    },
    "percentiles4": {
        "total": "12150",
        "ok": "12150",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c41a4": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_118516",
path: "Animal-api sync for customer NL_118516",
pathFormatted: "req_animal-api-sync-c41a4",
stats: {
    "name": "Animal-api sync for customer NL_118516",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4750",
        "ok": "4750",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4750",
        "ok": "4750",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4750",
        "ok": "4750",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4750",
        "ok": "4750",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4750",
        "ok": "4750",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4750",
        "ok": "4750",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4750",
        "ok": "4750",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e8c3a": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_154725",
path: "Animal-api sync for customer BE_154725",
pathFormatted: "req_animal-api-sync-e8c3a",
stats: {
    "name": "Animal-api sync for customer BE_154725",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4051",
        "ok": "4051",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4051",
        "ok": "4051",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4051",
        "ok": "4051",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4051",
        "ok": "4051",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4051",
        "ok": "4051",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4051",
        "ok": "4051",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4051",
        "ok": "4051",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-fc13a": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_154710",
path: "Animal-api sync for customer BE_154710",
pathFormatted: "req_animal-api-sync-fc13a",
stats: {
    "name": "Animal-api sync for customer BE_154710",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5601",
        "ok": "5601",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5601",
        "ok": "5601",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5601",
        "ok": "5601",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5601",
        "ok": "5601",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5601",
        "ok": "5601",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5601",
        "ok": "5601",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5601",
        "ok": "5601",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d0d7d": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_147835",
path: "Animal-api sync for customer BE_147835",
pathFormatted: "req_animal-api-sync-d0d7d",
stats: {
    "name": "Animal-api sync for customer BE_147835",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4310",
        "ok": "4310",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4310",
        "ok": "4310",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4310",
        "ok": "4310",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4310",
        "ok": "4310",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4310",
        "ok": "4310",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4310",
        "ok": "4310",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4310",
        "ok": "4310",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-f852c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_129269",
path: "Animal-api sync for customer NL_129269",
pathFormatted: "req_animal-api-sync-f852c",
stats: {
    "name": "Animal-api sync for customer NL_129269",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "6802",
        "ok": "6802",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "6802",
        "ok": "6802",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "6802",
        "ok": "6802",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "6802",
        "ok": "6802",
        "ko": "-"
    },
    "percentiles2": {
        "total": "6802",
        "ok": "6802",
        "ko": "-"
    },
    "percentiles3": {
        "total": "6802",
        "ok": "6802",
        "ko": "-"
    },
    "percentiles4": {
        "total": "6802",
        "ok": "6802",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
