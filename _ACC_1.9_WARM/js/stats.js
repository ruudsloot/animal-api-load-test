var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "495",
        "ok": "494",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "1041",
        "ok": "1041",
        "ko": "2747"
    },
    "maxResponseTime": {
        "total": "12262",
        "ok": "12262",
        "ko": "2747"
    },
    "meanResponseTime": {
        "total": "3211",
        "ok": "3212",
        "ko": "2747"
    },
    "standardDeviation": {
        "total": "1288",
        "ok": "1289",
        "ko": "0"
    },
    "percentiles1": {
        "total": "2974",
        "ok": "2978",
        "ko": "2747"
    },
    "percentiles2": {
        "total": "3703",
        "ok": "3704",
        "ko": "2747"
    },
    "percentiles3": {
        "total": "5627",
        "ok": "5629",
        "ko": "2747"
    },
    "percentiles4": {
        "total": "7690",
        "ok": "7699",
        "ko": "2747"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 494,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.228",
        "ok": "1.226",
        "ko": "0.002"
    }
},
contents: {
"req_animal-api-sync-9e54f": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_112168",
path: "Animal-api sync for customer NL_112168",
pathFormatted: "req_animal-api-sync-9e54f",
stats: {
    "name": "Animal-api sync for customer NL_112168",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2721",
        "ok": "2721",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2721",
        "ok": "2721",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2721",
        "ok": "2721",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2721",
        "ok": "2721",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2721",
        "ok": "2721",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2721",
        "ok": "2721",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2721",
        "ok": "2721",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-f5a2a": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_125092",
path: "Animal-api sync for customer NL_125092",
pathFormatted: "req_animal-api-sync-f5a2a",
stats: {
    "name": "Animal-api sync for customer NL_125092",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2959",
        "ok": "2959",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2959",
        "ok": "2959",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2959",
        "ok": "2959",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2959",
        "ok": "2959",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2959",
        "ok": "2959",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2959",
        "ok": "2959",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2959",
        "ok": "2959",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-0f1c3": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_128277",
path: "Animal-api sync for customer NL_128277",
pathFormatted: "req_animal-api-sync-0f1c3",
stats: {
    "name": "Animal-api sync for customer NL_128277",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3241",
        "ok": "3241",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3241",
        "ok": "3241",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3241",
        "ok": "3241",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3241",
        "ok": "3241",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3241",
        "ok": "3241",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3241",
        "ok": "3241",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3241",
        "ok": "3241",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-1d77d": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_109531",
path: "Animal-api sync for customer NL_109531",
pathFormatted: "req_animal-api-sync-1d77d",
stats: {
    "name": "Animal-api sync for customer NL_109531",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2704",
        "ok": "2704",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2704",
        "ok": "2704",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2704",
        "ok": "2704",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2704",
        "ok": "2704",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2704",
        "ok": "2704",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2704",
        "ok": "2704",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2704",
        "ok": "2704",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e9ad2": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_121119",
path: "Animal-api sync for customer NL_121119",
pathFormatted: "req_animal-api-sync-e9ad2",
stats: {
    "name": "Animal-api sync for customer NL_121119",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2981",
        "ok": "2981",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2981",
        "ok": "2981",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2981",
        "ok": "2981",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2981",
        "ok": "2981",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2981",
        "ok": "2981",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2981",
        "ok": "2981",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2981",
        "ok": "2981",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-86f14": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_121285",
path: "Animal-api sync for customer NL_121285",
pathFormatted: "req_animal-api-sync-86f14",
stats: {
    "name": "Animal-api sync for customer NL_121285",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2319",
        "ok": "2319",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2319",
        "ok": "2319",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2319",
        "ok": "2319",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2319",
        "ok": "2319",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2319",
        "ok": "2319",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2319",
        "ok": "2319",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2319",
        "ok": "2319",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d5299": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104311",
path: "Animal-api sync for customer NL_104311",
pathFormatted: "req_animal-api-sync-d5299",
stats: {
    "name": "Animal-api sync for customer NL_104311",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2413",
        "ok": "2413",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2413",
        "ok": "2413",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2413",
        "ok": "2413",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2413",
        "ok": "2413",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2413",
        "ok": "2413",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2413",
        "ok": "2413",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2413",
        "ok": "2413",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-51e6e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117693",
path: "Animal-api sync for customer NL_117693",
pathFormatted: "req_animal-api-sync-51e6e",
stats: {
    "name": "Animal-api sync for customer NL_117693",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3303",
        "ok": "3303",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3303",
        "ok": "3303",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3303",
        "ok": "3303",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3303",
        "ok": "3303",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3303",
        "ok": "3303",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3303",
        "ok": "3303",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3303",
        "ok": "3303",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-44547": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117813",
path: "Animal-api sync for customer NL_117813",
pathFormatted: "req_animal-api-sync-44547",
stats: {
    "name": "Animal-api sync for customer NL_117813",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2982",
        "ok": "2982",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2982",
        "ok": "2982",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2982",
        "ok": "2982",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2982",
        "ok": "2982",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2982",
        "ok": "2982",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2982",
        "ok": "2982",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2982",
        "ok": "2982",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-76899": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_121301",
path: "Animal-api sync for customer NL_121301",
pathFormatted: "req_animal-api-sync-76899",
stats: {
    "name": "Animal-api sync for customer NL_121301",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3222",
        "ok": "3222",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3222",
        "ok": "3222",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3222",
        "ok": "3222",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3222",
        "ok": "3222",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3222",
        "ok": "3222",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3222",
        "ok": "3222",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3222",
        "ok": "3222",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-857a0": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_127307",
path: "Animal-api sync for customer NL_127307",
pathFormatted: "req_animal-api-sync-857a0",
stats: {
    "name": "Animal-api sync for customer NL_127307",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1643",
        "ok": "1643",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1643",
        "ok": "1643",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1643",
        "ok": "1643",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1643",
        "ok": "1643",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1643",
        "ok": "1643",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1643",
        "ok": "1643",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1643",
        "ok": "1643",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-8b245": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_120606",
path: "Animal-api sync for customer NL_120606",
pathFormatted: "req_animal-api-sync-8b245",
stats: {
    "name": "Animal-api sync for customer NL_120606",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3151",
        "ok": "3151",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3151",
        "ok": "3151",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3151",
        "ok": "3151",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3151",
        "ok": "3151",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3151",
        "ok": "3151",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3151",
        "ok": "3151",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3151",
        "ok": "3151",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-1ba07": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105320",
path: "Animal-api sync for customer NL_105320",
pathFormatted: "req_animal-api-sync-1ba07",
stats: {
    "name": "Animal-api sync for customer NL_105320",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2088",
        "ok": "2088",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2088",
        "ok": "2088",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2088",
        "ok": "2088",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2088",
        "ok": "2088",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2088",
        "ok": "2088",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2088",
        "ok": "2088",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2088",
        "ok": "2088",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-07d8c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_115607",
path: "Animal-api sync for customer NL_115607",
pathFormatted: "req_animal-api-sync-07d8c",
stats: {
    "name": "Animal-api sync for customer NL_115607",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3019",
        "ok": "3019",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3019",
        "ok": "3019",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3019",
        "ok": "3019",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3019",
        "ok": "3019",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3019",
        "ok": "3019",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3019",
        "ok": "3019",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3019",
        "ok": "3019",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d643b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_129420",
path: "Animal-api sync for customer NL_129420",
pathFormatted: "req_animal-api-sync-d643b",
stats: {
    "name": "Animal-api sync for customer NL_129420",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1179",
        "ok": "1179",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1179",
        "ok": "1179",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1179",
        "ok": "1179",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1179",
        "ok": "1179",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1179",
        "ok": "1179",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1179",
        "ok": "1179",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1179",
        "ok": "1179",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-5b7d7": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_113415",
path: "Animal-api sync for customer NL_113415",
pathFormatted: "req_animal-api-sync-5b7d7",
stats: {
    "name": "Animal-api sync for customer NL_113415",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3994",
        "ok": "3994",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3994",
        "ok": "3994",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3994",
        "ok": "3994",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3994",
        "ok": "3994",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3994",
        "ok": "3994",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3994",
        "ok": "3994",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3994",
        "ok": "3994",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-05129": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117717",
path: "Animal-api sync for customer NL_117717",
pathFormatted: "req_animal-api-sync-05129",
stats: {
    "name": "Animal-api sync for customer NL_117717",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2214",
        "ok": "2214",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2214",
        "ok": "2214",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2214",
        "ok": "2214",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2214",
        "ok": "2214",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2214",
        "ok": "2214",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2214",
        "ok": "2214",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2214",
        "ok": "2214",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a175e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_207835",
path: "Animal-api sync for customer NL_207835",
pathFormatted: "req_animal-api-sync-a175e",
stats: {
    "name": "Animal-api sync for customer NL_207835",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1133",
        "ok": "1133",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1133",
        "ok": "1133",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1133",
        "ok": "1133",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1133",
        "ok": "1133",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1133",
        "ok": "1133",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1133",
        "ok": "1133",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1133",
        "ok": "1133",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-3bf26": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_108699",
path: "Animal-api sync for customer NL_108699",
pathFormatted: "req_animal-api-sync-3bf26",
stats: {
    "name": "Animal-api sync for customer NL_108699",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1203",
        "ok": "1203",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1203",
        "ok": "1203",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1203",
        "ok": "1203",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1203",
        "ok": "1203",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1203",
        "ok": "1203",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1203",
        "ok": "1203",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1203",
        "ok": "1203",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-2b97a": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_161984",
path: "Animal-api sync for customer NL_161984",
pathFormatted: "req_animal-api-sync-2b97a",
stats: {
    "name": "Animal-api sync for customer NL_161984",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2714",
        "ok": "2714",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2714",
        "ok": "2714",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2714",
        "ok": "2714",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2714",
        "ok": "2714",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2714",
        "ok": "2714",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2714",
        "ok": "2714",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2714",
        "ok": "2714",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-5243f": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111939",
path: "Animal-api sync for customer NL_111939",
pathFormatted: "req_animal-api-sync-5243f",
stats: {
    "name": "Animal-api sync for customer NL_111939",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3168",
        "ok": "3168",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3168",
        "ok": "3168",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3168",
        "ok": "3168",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3168",
        "ok": "3168",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3168",
        "ok": "3168",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3168",
        "ok": "3168",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3168",
        "ok": "3168",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b6219": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_144583",
path: "Animal-api sync for customer NL_144583",
pathFormatted: "req_animal-api-sync-b6219",
stats: {
    "name": "Animal-api sync for customer NL_144583",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2082",
        "ok": "2082",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2082",
        "ok": "2082",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2082",
        "ok": "2082",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2082",
        "ok": "2082",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2082",
        "ok": "2082",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2082",
        "ok": "2082",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2082",
        "ok": "2082",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-43dcc": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_112405",
path: "Animal-api sync for customer NL_112405",
pathFormatted: "req_animal-api-sync-43dcc",
stats: {
    "name": "Animal-api sync for customer NL_112405",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2899",
        "ok": "2899",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2899",
        "ok": "2899",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2899",
        "ok": "2899",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2899",
        "ok": "2899",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2899",
        "ok": "2899",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2899",
        "ok": "2899",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2899",
        "ok": "2899",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d545c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_131064",
path: "Animal-api sync for customer NL_131064",
pathFormatted: "req_animal-api-sync-d545c",
stats: {
    "name": "Animal-api sync for customer NL_131064",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3345",
        "ok": "3345",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3345",
        "ok": "3345",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3345",
        "ok": "3345",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3345",
        "ok": "3345",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3345",
        "ok": "3345",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3345",
        "ok": "3345",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3345",
        "ok": "3345",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-93205": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_120176",
path: "Animal-api sync for customer NL_120176",
pathFormatted: "req_animal-api-sync-93205",
stats: {
    "name": "Animal-api sync for customer NL_120176",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3142",
        "ok": "3142",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3142",
        "ok": "3142",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3142",
        "ok": "3142",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3142",
        "ok": "3142",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3142",
        "ok": "3142",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3142",
        "ok": "3142",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3142",
        "ok": "3142",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-bf978": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_118830",
path: "Animal-api sync for customer NL_118830",
pathFormatted: "req_animal-api-sync-bf978",
stats: {
    "name": "Animal-api sync for customer NL_118830",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2641",
        "ok": "2641",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2641",
        "ok": "2641",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2641",
        "ok": "2641",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2641",
        "ok": "2641",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2641",
        "ok": "2641",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2641",
        "ok": "2641",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2641",
        "ok": "2641",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-28852": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_116904",
path: "Animal-api sync for customer NL_116904",
pathFormatted: "req_animal-api-sync-28852",
stats: {
    "name": "Animal-api sync for customer NL_116904",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3082",
        "ok": "3082",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3082",
        "ok": "3082",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3082",
        "ok": "3082",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3082",
        "ok": "3082",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3082",
        "ok": "3082",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3082",
        "ok": "3082",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3082",
        "ok": "3082",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-3566a": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_107884",
path: "Animal-api sync for customer NL_107884",
pathFormatted: "req_animal-api-sync-3566a",
stats: {
    "name": "Animal-api sync for customer NL_107884",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3139",
        "ok": "3139",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3139",
        "ok": "3139",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3139",
        "ok": "3139",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3139",
        "ok": "3139",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3139",
        "ok": "3139",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3139",
        "ok": "3139",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3139",
        "ok": "3139",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c24b5": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_122429",
path: "Animal-api sync for customer NL_122429",
pathFormatted: "req_animal-api-sync-c24b5",
stats: {
    "name": "Animal-api sync for customer NL_122429",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2719",
        "ok": "2719",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2719",
        "ok": "2719",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2719",
        "ok": "2719",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2719",
        "ok": "2719",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2719",
        "ok": "2719",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2719",
        "ok": "2719",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2719",
        "ok": "2719",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-31f54": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_122305",
path: "Animal-api sync for customer NL_122305",
pathFormatted: "req_animal-api-sync-31f54",
stats: {
    "name": "Animal-api sync for customer NL_122305",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2665",
        "ok": "2665",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2665",
        "ok": "2665",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2665",
        "ok": "2665",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2665",
        "ok": "2665",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2665",
        "ok": "2665",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2665",
        "ok": "2665",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2665",
        "ok": "2665",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-8ebc3": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_212864",
path: "Animal-api sync for customer BE_212864",
pathFormatted: "req_animal-api-sync-8ebc3",
stats: {
    "name": "Animal-api sync for customer BE_212864",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3144",
        "ok": "3144",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3144",
        "ok": "3144",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3144",
        "ok": "3144",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3144",
        "ok": "3144",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3144",
        "ok": "3144",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3144",
        "ok": "3144",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3144",
        "ok": "3144",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a5160": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_156494",
path: "Animal-api sync for customer NL_156494",
pathFormatted: "req_animal-api-sync-a5160",
stats: {
    "name": "Animal-api sync for customer NL_156494",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5501",
        "ok": "5501",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5501",
        "ok": "5501",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5501",
        "ok": "5501",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5501",
        "ok": "5501",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5501",
        "ok": "5501",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5501",
        "ok": "5501",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5501",
        "ok": "5501",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-34ba1": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_165365",
path: "Animal-api sync for customer BE_165365",
pathFormatted: "req_animal-api-sync-34ba1",
stats: {
    "name": "Animal-api sync for customer BE_165365",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3990",
        "ok": "3990",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3990",
        "ok": "3990",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3990",
        "ok": "3990",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3990",
        "ok": "3990",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3990",
        "ok": "3990",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3990",
        "ok": "3990",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3990",
        "ok": "3990",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-9ec36": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_124747",
path: "Animal-api sync for customer NL_124747",
pathFormatted: "req_animal-api-sync-9ec36",
stats: {
    "name": "Animal-api sync for customer NL_124747",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3056",
        "ok": "3056",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3056",
        "ok": "3056",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3056",
        "ok": "3056",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3056",
        "ok": "3056",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3056",
        "ok": "3056",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3056",
        "ok": "3056",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3056",
        "ok": "3056",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b9862": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_161770",
path: "Animal-api sync for customer NL_161770",
pathFormatted: "req_animal-api-sync-b9862",
stats: {
    "name": "Animal-api sync for customer NL_161770",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3370",
        "ok": "3370",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3370",
        "ok": "3370",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3370",
        "ok": "3370",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3370",
        "ok": "3370",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3370",
        "ok": "3370",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3370",
        "ok": "3370",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3370",
        "ok": "3370",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b2272": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105223",
path: "Animal-api sync for customer NL_105223",
pathFormatted: "req_animal-api-sync-b2272",
stats: {
    "name": "Animal-api sync for customer NL_105223",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2764",
        "ok": "2764",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2764",
        "ok": "2764",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2764",
        "ok": "2764",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2764",
        "ok": "2764",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2764",
        "ok": "2764",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2764",
        "ok": "2764",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2764",
        "ok": "2764",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-49a96": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_128642",
path: "Animal-api sync for customer NL_128642",
pathFormatted: "req_animal-api-sync-49a96",
stats: {
    "name": "Animal-api sync for customer NL_128642",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2172",
        "ok": "2172",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2172",
        "ok": "2172",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2172",
        "ok": "2172",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2172",
        "ok": "2172",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2172",
        "ok": "2172",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2172",
        "ok": "2172",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2172",
        "ok": "2172",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-5c4eb": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_123093",
path: "Animal-api sync for customer NL_123093",
pathFormatted: "req_animal-api-sync-5c4eb",
stats: {
    "name": "Animal-api sync for customer NL_123093",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1845",
        "ok": "1845",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1845",
        "ok": "1845",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1845",
        "ok": "1845",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1845",
        "ok": "1845",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1845",
        "ok": "1845",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1845",
        "ok": "1845",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1845",
        "ok": "1845",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-90ee6": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105389",
path: "Animal-api sync for customer NL_105389",
pathFormatted: "req_animal-api-sync-90ee6",
stats: {
    "name": "Animal-api sync for customer NL_105389",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3356",
        "ok": "3356",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3356",
        "ok": "3356",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3356",
        "ok": "3356",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3356",
        "ok": "3356",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3356",
        "ok": "3356",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3356",
        "ok": "3356",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3356",
        "ok": "3356",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-0908d": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105707",
path: "Animal-api sync for customer NL_105707",
pathFormatted: "req_animal-api-sync-0908d",
stats: {
    "name": "Animal-api sync for customer NL_105707",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2606",
        "ok": "2606",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2606",
        "ok": "2606",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2606",
        "ok": "2606",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2606",
        "ok": "2606",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2606",
        "ok": "2606",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2606",
        "ok": "2606",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2606",
        "ok": "2606",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-0a82c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_124910",
path: "Animal-api sync for customer NL_124910",
pathFormatted: "req_animal-api-sync-0a82c",
stats: {
    "name": "Animal-api sync for customer NL_124910",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1233",
        "ok": "1233",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1233",
        "ok": "1233",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1233",
        "ok": "1233",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1233",
        "ok": "1233",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1233",
        "ok": "1233",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1233",
        "ok": "1233",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1233",
        "ok": "1233",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d54bb": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_108024",
path: "Animal-api sync for customer NL_108024",
pathFormatted: "req_animal-api-sync-d54bb",
stats: {
    "name": "Animal-api sync for customer NL_108024",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4156",
        "ok": "4156",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4156",
        "ok": "4156",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4156",
        "ok": "4156",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4156",
        "ok": "4156",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4156",
        "ok": "4156",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4156",
        "ok": "4156",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4156",
        "ok": "4156",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c0ea2": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_126187",
path: "Animal-api sync for customer NL_126187",
pathFormatted: "req_animal-api-sync-c0ea2",
stats: {
    "name": "Animal-api sync for customer NL_126187",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1667",
        "ok": "1667",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1667",
        "ok": "1667",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1667",
        "ok": "1667",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1667",
        "ok": "1667",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1667",
        "ok": "1667",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1667",
        "ok": "1667",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1667",
        "ok": "1667",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-7a1f1": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_151229",
path: "Animal-api sync for customer BE_151229",
pathFormatted: "req_animal-api-sync-7a1f1",
stats: {
    "name": "Animal-api sync for customer BE_151229",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1041",
        "ok": "1041",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1041",
        "ok": "1041",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1041",
        "ok": "1041",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1041",
        "ok": "1041",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1041",
        "ok": "1041",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1041",
        "ok": "1041",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1041",
        "ok": "1041",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-72904": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_103783",
path: "Animal-api sync for customer NL_103783",
pathFormatted: "req_animal-api-sync-72904",
stats: {
    "name": "Animal-api sync for customer NL_103783",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3011",
        "ok": "3011",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3011",
        "ok": "3011",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3011",
        "ok": "3011",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3011",
        "ok": "3011",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3011",
        "ok": "3011",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3011",
        "ok": "3011",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3011",
        "ok": "3011",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-54314": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_103881",
path: "Animal-api sync for customer NL_103881",
pathFormatted: "req_animal-api-sync-54314",
stats: {
    "name": "Animal-api sync for customer NL_103881",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2664",
        "ok": "2664",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2664",
        "ok": "2664",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2664",
        "ok": "2664",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2664",
        "ok": "2664",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2664",
        "ok": "2664",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2664",
        "ok": "2664",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2664",
        "ok": "2664",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-fb3ec": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_115893",
path: "Animal-api sync for customer NL_115893",
pathFormatted: "req_animal-api-sync-fb3ec",
stats: {
    "name": "Animal-api sync for customer NL_115893",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2406",
        "ok": "2406",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2406",
        "ok": "2406",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2406",
        "ok": "2406",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2406",
        "ok": "2406",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2406",
        "ok": "2406",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2406",
        "ok": "2406",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2406",
        "ok": "2406",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d90a9": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_119649",
path: "Animal-api sync for customer NL_119649",
pathFormatted: "req_animal-api-sync-d90a9",
stats: {
    "name": "Animal-api sync for customer NL_119649",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1584",
        "ok": "1584",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1584",
        "ok": "1584",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1584",
        "ok": "1584",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1584",
        "ok": "1584",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1584",
        "ok": "1584",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1584",
        "ok": "1584",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1584",
        "ok": "1584",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c4ec4": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104910",
path: "Animal-api sync for customer NL_104910",
pathFormatted: "req_animal-api-sync-c4ec4",
stats: {
    "name": "Animal-api sync for customer NL_104910",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2192",
        "ok": "2192",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2192",
        "ok": "2192",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2192",
        "ok": "2192",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2192",
        "ok": "2192",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2192",
        "ok": "2192",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2192",
        "ok": "2192",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2192",
        "ok": "2192",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e498b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110382",
path: "Animal-api sync for customer NL_110382",
pathFormatted: "req_animal-api-sync-e498b",
stats: {
    "name": "Animal-api sync for customer NL_110382",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2404",
        "ok": "2404",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2404",
        "ok": "2404",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2404",
        "ok": "2404",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2404",
        "ok": "2404",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2404",
        "ok": "2404",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2404",
        "ok": "2404",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2404",
        "ok": "2404",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-2a4dd": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_125297",
path: "Animal-api sync for customer NL_125297",
pathFormatted: "req_animal-api-sync-2a4dd",
stats: {
    "name": "Animal-api sync for customer NL_125297",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2203",
        "ok": "2203",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2203",
        "ok": "2203",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2203",
        "ok": "2203",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2203",
        "ok": "2203",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2203",
        "ok": "2203",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2203",
        "ok": "2203",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2203",
        "ok": "2203",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c1c6a": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_135207",
path: "Animal-api sync for customer NL_135207",
pathFormatted: "req_animal-api-sync-c1c6a",
stats: {
    "name": "Animal-api sync for customer NL_135207",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2280",
        "ok": "2280",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2280",
        "ok": "2280",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2280",
        "ok": "2280",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2280",
        "ok": "2280",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2280",
        "ok": "2280",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2280",
        "ok": "2280",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2280",
        "ok": "2280",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c170f": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_109428",
path: "Animal-api sync for customer NL_109428",
pathFormatted: "req_animal-api-sync-c170f",
stats: {
    "name": "Animal-api sync for customer NL_109428",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2627",
        "ok": "2627",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2627",
        "ok": "2627",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2627",
        "ok": "2627",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2627",
        "ok": "2627",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2627",
        "ok": "2627",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2627",
        "ok": "2627",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2627",
        "ok": "2627",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-690f7": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_129681",
path: "Animal-api sync for customer NL_129681",
pathFormatted: "req_animal-api-sync-690f7",
stats: {
    "name": "Animal-api sync for customer NL_129681",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2138",
        "ok": "2138",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2138",
        "ok": "2138",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2138",
        "ok": "2138",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2138",
        "ok": "2138",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2138",
        "ok": "2138",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2138",
        "ok": "2138",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2138",
        "ok": "2138",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-de0b1": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_164131",
path: "Animal-api sync for customer NL_164131",
pathFormatted: "req_animal-api-sync-de0b1",
stats: {
    "name": "Animal-api sync for customer NL_164131",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2578",
        "ok": "2578",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2578",
        "ok": "2578",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2578",
        "ok": "2578",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2578",
        "ok": "2578",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2578",
        "ok": "2578",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2578",
        "ok": "2578",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2578",
        "ok": "2578",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-3e31b": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_149431",
path: "Animal-api sync for customer BE_149431",
pathFormatted: "req_animal-api-sync-3e31b",
stats: {
    "name": "Animal-api sync for customer BE_149431",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2116",
        "ok": "2116",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2116",
        "ok": "2116",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2116",
        "ok": "2116",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2116",
        "ok": "2116",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2116",
        "ok": "2116",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2116",
        "ok": "2116",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2116",
        "ok": "2116",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-cb71c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_120709",
path: "Animal-api sync for customer NL_120709",
pathFormatted: "req_animal-api-sync-cb71c",
stats: {
    "name": "Animal-api sync for customer NL_120709",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2639",
        "ok": "2639",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2639",
        "ok": "2639",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2639",
        "ok": "2639",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2639",
        "ok": "2639",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2639",
        "ok": "2639",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2639",
        "ok": "2639",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2639",
        "ok": "2639",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-5a7e6": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105010",
path: "Animal-api sync for customer NL_105010",
pathFormatted: "req_animal-api-sync-5a7e6",
stats: {
    "name": "Animal-api sync for customer NL_105010",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4102",
        "ok": "4102",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4102",
        "ok": "4102",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4102",
        "ok": "4102",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4102",
        "ok": "4102",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4102",
        "ok": "4102",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4102",
        "ok": "4102",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4102",
        "ok": "4102",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-0eddf": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110820",
path: "Animal-api sync for customer NL_110820",
pathFormatted: "req_animal-api-sync-0eddf",
stats: {
    "name": "Animal-api sync for customer NL_110820",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3362",
        "ok": "3362",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3362",
        "ok": "3362",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3362",
        "ok": "3362",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3362",
        "ok": "3362",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3362",
        "ok": "3362",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3362",
        "ok": "3362",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3362",
        "ok": "3362",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-558a2": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_116270",
path: "Animal-api sync for customer NL_116270",
pathFormatted: "req_animal-api-sync-558a2",
stats: {
    "name": "Animal-api sync for customer NL_116270",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2344",
        "ok": "2344",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2344",
        "ok": "2344",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2344",
        "ok": "2344",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2344",
        "ok": "2344",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2344",
        "ok": "2344",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2344",
        "ok": "2344",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2344",
        "ok": "2344",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-3833c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_124824",
path: "Animal-api sync for customer NL_124824",
pathFormatted: "req_animal-api-sync-3833c",
stats: {
    "name": "Animal-api sync for customer NL_124824",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1630",
        "ok": "1630",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1630",
        "ok": "1630",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1630",
        "ok": "1630",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1630",
        "ok": "1630",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1630",
        "ok": "1630",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1630",
        "ok": "1630",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1630",
        "ok": "1630",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a3967": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117770",
path: "Animal-api sync for customer NL_117770",
pathFormatted: "req_animal-api-sync-a3967",
stats: {
    "name": "Animal-api sync for customer NL_117770",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2939",
        "ok": "2939",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2939",
        "ok": "2939",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2939",
        "ok": "2939",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2939",
        "ok": "2939",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2939",
        "ok": "2939",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2939",
        "ok": "2939",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2939",
        "ok": "2939",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-5a613": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_194583",
path: "Animal-api sync for customer NL_194583",
pathFormatted: "req_animal-api-sync-5a613",
stats: {
    "name": "Animal-api sync for customer NL_194583",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3123",
        "ok": "3123",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3123",
        "ok": "3123",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3123",
        "ok": "3123",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3123",
        "ok": "3123",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3123",
        "ok": "3123",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3123",
        "ok": "3123",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3123",
        "ok": "3123",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c7c5d": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_129379",
path: "Animal-api sync for customer NL_129379",
pathFormatted: "req_animal-api-sync-c7c5d",
stats: {
    "name": "Animal-api sync for customer NL_129379",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2624",
        "ok": "2624",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2624",
        "ok": "2624",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2624",
        "ok": "2624",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2624",
        "ok": "2624",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2624",
        "ok": "2624",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2624",
        "ok": "2624",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2624",
        "ok": "2624",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-91ebb": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_120778",
path: "Animal-api sync for customer NL_120778",
pathFormatted: "req_animal-api-sync-91ebb",
stats: {
    "name": "Animal-api sync for customer NL_120778",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1764",
        "ok": "1764",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1764",
        "ok": "1764",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1764",
        "ok": "1764",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1764",
        "ok": "1764",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1764",
        "ok": "1764",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1764",
        "ok": "1764",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1764",
        "ok": "1764",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-6d45c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_123012",
path: "Animal-api sync for customer NL_123012",
pathFormatted: "req_animal-api-sync-6d45c",
stats: {
    "name": "Animal-api sync for customer NL_123012",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2140",
        "ok": "2140",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2140",
        "ok": "2140",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2140",
        "ok": "2140",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2140",
        "ok": "2140",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2140",
        "ok": "2140",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2140",
        "ok": "2140",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2140",
        "ok": "2140",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-3ff93": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111287",
path: "Animal-api sync for customer NL_111287",
pathFormatted: "req_animal-api-sync-3ff93",
stats: {
    "name": "Animal-api sync for customer NL_111287",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2074",
        "ok": "2074",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2074",
        "ok": "2074",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2074",
        "ok": "2074",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2074",
        "ok": "2074",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2074",
        "ok": "2074",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2074",
        "ok": "2074",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2074",
        "ok": "2074",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-968ea": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_152278",
path: "Animal-api sync for customer BE_152278",
pathFormatted: "req_animal-api-sync-968ea",
stats: {
    "name": "Animal-api sync for customer BE_152278",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2639",
        "ok": "2639",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2639",
        "ok": "2639",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2639",
        "ok": "2639",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2639",
        "ok": "2639",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2639",
        "ok": "2639",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2639",
        "ok": "2639",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2639",
        "ok": "2639",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b979d": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_134839",
path: "Animal-api sync for customer NL_134839",
pathFormatted: "req_animal-api-sync-b979d",
stats: {
    "name": "Animal-api sync for customer NL_134839",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2255",
        "ok": "2255",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2255",
        "ok": "2255",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2255",
        "ok": "2255",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2255",
        "ok": "2255",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2255",
        "ok": "2255",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2255",
        "ok": "2255",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2255",
        "ok": "2255",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-4a2f0": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_119479",
path: "Animal-api sync for customer NL_119479",
pathFormatted: "req_animal-api-sync-4a2f0",
stats: {
    "name": "Animal-api sync for customer NL_119479",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1954",
        "ok": "1954",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1954",
        "ok": "1954",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1954",
        "ok": "1954",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1954",
        "ok": "1954",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1954",
        "ok": "1954",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1954",
        "ok": "1954",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1954",
        "ok": "1954",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-04f19": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_142633",
path: "Animal-api sync for customer NL_142633",
pathFormatted: "req_animal-api-sync-04f19",
stats: {
    "name": "Animal-api sync for customer NL_142633",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3660",
        "ok": "3660",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3660",
        "ok": "3660",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3660",
        "ok": "3660",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3660",
        "ok": "3660",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3660",
        "ok": "3660",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3660",
        "ok": "3660",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3660",
        "ok": "3660",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c6934": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_144844",
path: "Animal-api sync for customer BE_144844",
pathFormatted: "req_animal-api-sync-c6934",
stats: {
    "name": "Animal-api sync for customer BE_144844",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3039",
        "ok": "3039",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3039",
        "ok": "3039",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3039",
        "ok": "3039",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3039",
        "ok": "3039",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3039",
        "ok": "3039",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3039",
        "ok": "3039",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3039",
        "ok": "3039",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-ee05d": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_106934",
path: "Animal-api sync for customer NL_106934",
pathFormatted: "req_animal-api-sync-ee05d",
stats: {
    "name": "Animal-api sync for customer NL_106934",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3227",
        "ok": "3227",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3227",
        "ok": "3227",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3227",
        "ok": "3227",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3227",
        "ok": "3227",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3227",
        "ok": "3227",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3227",
        "ok": "3227",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3227",
        "ok": "3227",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-26bf9": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110058",
path: "Animal-api sync for customer NL_110058",
pathFormatted: "req_animal-api-sync-26bf9",
stats: {
    "name": "Animal-api sync for customer NL_110058",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2427",
        "ok": "2427",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2427",
        "ok": "2427",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2427",
        "ok": "2427",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2427",
        "ok": "2427",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2427",
        "ok": "2427",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2427",
        "ok": "2427",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2427",
        "ok": "2427",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a42d0": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105360",
path: "Animal-api sync for customer NL_105360",
pathFormatted: "req_animal-api-sync-a42d0",
stats: {
    "name": "Animal-api sync for customer NL_105360",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2746",
        "ok": "2746",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2746",
        "ok": "2746",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2746",
        "ok": "2746",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2746",
        "ok": "2746",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2746",
        "ok": "2746",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2746",
        "ok": "2746",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2746",
        "ok": "2746",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-35753": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_118795",
path: "Animal-api sync for customer NL_118795",
pathFormatted: "req_animal-api-sync-35753",
stats: {
    "name": "Animal-api sync for customer NL_118795",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2205",
        "ok": "2205",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2205",
        "ok": "2205",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2205",
        "ok": "2205",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2205",
        "ok": "2205",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2205",
        "ok": "2205",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2205",
        "ok": "2205",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2205",
        "ok": "2205",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-09a8d": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117444",
path: "Animal-api sync for customer NL_117444",
pathFormatted: "req_animal-api-sync-09a8d",
stats: {
    "name": "Animal-api sync for customer NL_117444",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2520",
        "ok": "2520",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2520",
        "ok": "2520",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2520",
        "ok": "2520",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2520",
        "ok": "2520",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2520",
        "ok": "2520",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2520",
        "ok": "2520",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2520",
        "ok": "2520",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-7c5cb": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_118794",
path: "Animal-api sync for customer NL_118794",
pathFormatted: "req_animal-api-sync-7c5cb",
stats: {
    "name": "Animal-api sync for customer NL_118794",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2151",
        "ok": "2151",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2151",
        "ok": "2151",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2151",
        "ok": "2151",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2151",
        "ok": "2151",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2151",
        "ok": "2151",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2151",
        "ok": "2151",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2151",
        "ok": "2151",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e202d": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_112066",
path: "Animal-api sync for customer NL_112066",
pathFormatted: "req_animal-api-sync-e202d",
stats: {
    "name": "Animal-api sync for customer NL_112066",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2647",
        "ok": "2647",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2647",
        "ok": "2647",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2647",
        "ok": "2647",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2647",
        "ok": "2647",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2647",
        "ok": "2647",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2647",
        "ok": "2647",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2647",
        "ok": "2647",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-be37d": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_106236",
path: "Animal-api sync for customer NL_106236",
pathFormatted: "req_animal-api-sync-be37d",
stats: {
    "name": "Animal-api sync for customer NL_106236",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2562",
        "ok": "2562",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2562",
        "ok": "2562",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2562",
        "ok": "2562",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2562",
        "ok": "2562",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2562",
        "ok": "2562",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2562",
        "ok": "2562",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2562",
        "ok": "2562",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-ca7e3": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_134297",
path: "Animal-api sync for customer NL_134297",
pathFormatted: "req_animal-api-sync-ca7e3",
stats: {
    "name": "Animal-api sync for customer NL_134297",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2247",
        "ok": "2247",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2247",
        "ok": "2247",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2247",
        "ok": "2247",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2247",
        "ok": "2247",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2247",
        "ok": "2247",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2247",
        "ok": "2247",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2247",
        "ok": "2247",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-993d0": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_160647",
path: "Animal-api sync for customer NL_160647",
pathFormatted: "req_animal-api-sync-993d0",
stats: {
    "name": "Animal-api sync for customer NL_160647",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5331",
        "ok": "5331",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5331",
        "ok": "5331",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5331",
        "ok": "5331",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5331",
        "ok": "5331",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5331",
        "ok": "5331",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5331",
        "ok": "5331",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5331",
        "ok": "5331",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-1e171": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111917",
path: "Animal-api sync for customer NL_111917",
pathFormatted: "req_animal-api-sync-1e171",
stats: {
    "name": "Animal-api sync for customer NL_111917",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2663",
        "ok": "2663",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2663",
        "ok": "2663",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2663",
        "ok": "2663",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2663",
        "ok": "2663",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2663",
        "ok": "2663",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2663",
        "ok": "2663",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2663",
        "ok": "2663",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-5e0b3": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_106214",
path: "Animal-api sync for customer NL_106214",
pathFormatted: "req_animal-api-sync-5e0b3",
stats: {
    "name": "Animal-api sync for customer NL_106214",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2403",
        "ok": "2403",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2403",
        "ok": "2403",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2403",
        "ok": "2403",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2403",
        "ok": "2403",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2403",
        "ok": "2403",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2403",
        "ok": "2403",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2403",
        "ok": "2403",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a7313": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_123953",
path: "Animal-api sync for customer NL_123953",
pathFormatted: "req_animal-api-sync-a7313",
stats: {
    "name": "Animal-api sync for customer NL_123953",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3754",
        "ok": "3754",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3754",
        "ok": "3754",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3754",
        "ok": "3754",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3754",
        "ok": "3754",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3754",
        "ok": "3754",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3754",
        "ok": "3754",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3754",
        "ok": "3754",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-7cf01": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104284",
path: "Animal-api sync for customer NL_104284",
pathFormatted: "req_animal-api-sync-7cf01",
stats: {
    "name": "Animal-api sync for customer NL_104284",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2230",
        "ok": "2230",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2230",
        "ok": "2230",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2230",
        "ok": "2230",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2230",
        "ok": "2230",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2230",
        "ok": "2230",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2230",
        "ok": "2230",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2230",
        "ok": "2230",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-588aa": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_214938",
path: "Animal-api sync for customer NL_214938",
pathFormatted: "req_animal-api-sync-588aa",
stats: {
    "name": "Animal-api sync for customer NL_214938",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3189",
        "ok": "3189",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3189",
        "ok": "3189",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3189",
        "ok": "3189",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3189",
        "ok": "3189",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3189",
        "ok": "3189",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3189",
        "ok": "3189",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3189",
        "ok": "3189",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-6ebe1": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104335",
path: "Animal-api sync for customer NL_104335",
pathFormatted: "req_animal-api-sync-6ebe1",
stats: {
    "name": "Animal-api sync for customer NL_104335",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3179",
        "ok": "3179",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3179",
        "ok": "3179",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3179",
        "ok": "3179",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3179",
        "ok": "3179",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3179",
        "ok": "3179",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3179",
        "ok": "3179",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3179",
        "ok": "3179",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d86cb": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105153",
path: "Animal-api sync for customer NL_105153",
pathFormatted: "req_animal-api-sync-d86cb",
stats: {
    "name": "Animal-api sync for customer NL_105153",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2834",
        "ok": "2834",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2834",
        "ok": "2834",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2834",
        "ok": "2834",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2834",
        "ok": "2834",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2834",
        "ok": "2834",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2834",
        "ok": "2834",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2834",
        "ok": "2834",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b6645": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_126020",
path: "Animal-api sync for customer NL_126020",
pathFormatted: "req_animal-api-sync-b6645",
stats: {
    "name": "Animal-api sync for customer NL_126020",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2247",
        "ok": "2247",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2247",
        "ok": "2247",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2247",
        "ok": "2247",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2247",
        "ok": "2247",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2247",
        "ok": "2247",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2247",
        "ok": "2247",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2247",
        "ok": "2247",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-7ae4d": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_137252",
path: "Animal-api sync for customer NL_137252",
pathFormatted: "req_animal-api-sync-7ae4d",
stats: {
    "name": "Animal-api sync for customer NL_137252",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2393",
        "ok": "2393",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2393",
        "ok": "2393",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2393",
        "ok": "2393",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2393",
        "ok": "2393",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2393",
        "ok": "2393",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2393",
        "ok": "2393",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2393",
        "ok": "2393",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-f2327": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_107409",
path: "Animal-api sync for customer NL_107409",
pathFormatted: "req_animal-api-sync-f2327",
stats: {
    "name": "Animal-api sync for customer NL_107409",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2326",
        "ok": "2326",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2326",
        "ok": "2326",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2326",
        "ok": "2326",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2326",
        "ok": "2326",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2326",
        "ok": "2326",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2326",
        "ok": "2326",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2326",
        "ok": "2326",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-9b90a": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_114569",
path: "Animal-api sync for customer NL_114569",
pathFormatted: "req_animal-api-sync-9b90a",
stats: {
    "name": "Animal-api sync for customer NL_114569",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2138",
        "ok": "2138",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2138",
        "ok": "2138",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2138",
        "ok": "2138",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2138",
        "ok": "2138",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2138",
        "ok": "2138",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2138",
        "ok": "2138",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2138",
        "ok": "2138",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-56e97": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_130716",
path: "Animal-api sync for customer NL_130716",
pathFormatted: "req_animal-api-sync-56e97",
stats: {
    "name": "Animal-api sync for customer NL_130716",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2130",
        "ok": "2130",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2130",
        "ok": "2130",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2130",
        "ok": "2130",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2130",
        "ok": "2130",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2130",
        "ok": "2130",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2130",
        "ok": "2130",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2130",
        "ok": "2130",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-4b1c0": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_164635",
path: "Animal-api sync for customer BE_164635",
pathFormatted: "req_animal-api-sync-4b1c0",
stats: {
    "name": "Animal-api sync for customer BE_164635",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2538",
        "ok": "2538",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2538",
        "ok": "2538",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2538",
        "ok": "2538",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2538",
        "ok": "2538",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2538",
        "ok": "2538",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2538",
        "ok": "2538",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2538",
        "ok": "2538",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-0be59": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_137530",
path: "Animal-api sync for customer NL_137530",
pathFormatted: "req_animal-api-sync-0be59",
stats: {
    "name": "Animal-api sync for customer NL_137530",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1980",
        "ok": "1980",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1980",
        "ok": "1980",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1980",
        "ok": "1980",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1980",
        "ok": "1980",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1980",
        "ok": "1980",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1980",
        "ok": "1980",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1980",
        "ok": "1980",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e4170": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_205597",
path: "Animal-api sync for customer BE_205597",
pathFormatted: "req_animal-api-sync-e4170",
stats: {
    "name": "Animal-api sync for customer BE_205597",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3110",
        "ok": "3110",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3110",
        "ok": "3110",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3110",
        "ok": "3110",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3110",
        "ok": "3110",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3110",
        "ok": "3110",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3110",
        "ok": "3110",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3110",
        "ok": "3110",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-5e066": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_118716",
path: "Animal-api sync for customer NL_118716",
pathFormatted: "req_animal-api-sync-5e066",
stats: {
    "name": "Animal-api sync for customer NL_118716",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2846",
        "ok": "2846",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2846",
        "ok": "2846",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2846",
        "ok": "2846",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2846",
        "ok": "2846",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2846",
        "ok": "2846",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2846",
        "ok": "2846",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2846",
        "ok": "2846",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-3598c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_107584",
path: "Animal-api sync for customer NL_107584",
pathFormatted: "req_animal-api-sync-3598c",
stats: {
    "name": "Animal-api sync for customer NL_107584",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2261",
        "ok": "2261",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2261",
        "ok": "2261",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2261",
        "ok": "2261",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2261",
        "ok": "2261",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2261",
        "ok": "2261",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2261",
        "ok": "2261",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2261",
        "ok": "2261",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-6b068": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111950",
path: "Animal-api sync for customer NL_111950",
pathFormatted: "req_animal-api-sync-6b068",
stats: {
    "name": "Animal-api sync for customer NL_111950",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3330",
        "ok": "3330",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3330",
        "ok": "3330",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3330",
        "ok": "3330",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3330",
        "ok": "3330",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3330",
        "ok": "3330",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3330",
        "ok": "3330",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3330",
        "ok": "3330",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-34e6f": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_129959",
path: "Animal-api sync for customer NL_129959",
pathFormatted: "req_animal-api-sync-34e6f",
stats: {
    "name": "Animal-api sync for customer NL_129959",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2733",
        "ok": "2733",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2733",
        "ok": "2733",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2733",
        "ok": "2733",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2733",
        "ok": "2733",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2733",
        "ok": "2733",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2733",
        "ok": "2733",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2733",
        "ok": "2733",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-21b03": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_109721",
path: "Animal-api sync for customer NL_109721",
pathFormatted: "req_animal-api-sync-21b03",
stats: {
    "name": "Animal-api sync for customer NL_109721",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3844",
        "ok": "3844",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3844",
        "ok": "3844",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3844",
        "ok": "3844",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3844",
        "ok": "3844",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3844",
        "ok": "3844",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3844",
        "ok": "3844",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3844",
        "ok": "3844",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-24d15": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117331",
path: "Animal-api sync for customer NL_117331",
pathFormatted: "req_animal-api-sync-24d15",
stats: {
    "name": "Animal-api sync for customer NL_117331",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3011",
        "ok": "3011",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3011",
        "ok": "3011",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3011",
        "ok": "3011",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3011",
        "ok": "3011",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3011",
        "ok": "3011",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3011",
        "ok": "3011",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3011",
        "ok": "3011",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-4479f": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_134641",
path: "Animal-api sync for customer NL_134641",
pathFormatted: "req_animal-api-sync-4479f",
stats: {
    "name": "Animal-api sync for customer NL_134641",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1402",
        "ok": "1402",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1402",
        "ok": "1402",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1402",
        "ok": "1402",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1402",
        "ok": "1402",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1402",
        "ok": "1402",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1402",
        "ok": "1402",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1402",
        "ok": "1402",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-2ac98": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_119983",
path: "Animal-api sync for customer NL_119983",
pathFormatted: "req_animal-api-sync-2ac98",
stats: {
    "name": "Animal-api sync for customer NL_119983",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3542",
        "ok": "3542",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3542",
        "ok": "3542",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3542",
        "ok": "3542",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3542",
        "ok": "3542",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3542",
        "ok": "3542",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3542",
        "ok": "3542",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3542",
        "ok": "3542",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-53e53": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_122724",
path: "Animal-api sync for customer NL_122724",
pathFormatted: "req_animal-api-sync-53e53",
stats: {
    "name": "Animal-api sync for customer NL_122724",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2586",
        "ok": "2586",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2586",
        "ok": "2586",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2586",
        "ok": "2586",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2586",
        "ok": "2586",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2586",
        "ok": "2586",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2586",
        "ok": "2586",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2586",
        "ok": "2586",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-97824": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_129903",
path: "Animal-api sync for customer NL_129903",
pathFormatted: "req_animal-api-sync-97824",
stats: {
    "name": "Animal-api sync for customer NL_129903",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3056",
        "ok": "3056",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3056",
        "ok": "3056",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3056",
        "ok": "3056",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3056",
        "ok": "3056",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3056",
        "ok": "3056",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3056",
        "ok": "3056",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3056",
        "ok": "3056",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-9db7a": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_125745",
path: "Animal-api sync for customer NL_125745",
pathFormatted: "req_animal-api-sync-9db7a",
stats: {
    "name": "Animal-api sync for customer NL_125745",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3065",
        "ok": "3065",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3065",
        "ok": "3065",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3065",
        "ok": "3065",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3065",
        "ok": "3065",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3065",
        "ok": "3065",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3065",
        "ok": "3065",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3065",
        "ok": "3065",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-62afe": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_128808",
path: "Animal-api sync for customer NL_128808",
pathFormatted: "req_animal-api-sync-62afe",
stats: {
    "name": "Animal-api sync for customer NL_128808",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3203",
        "ok": "3203",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3203",
        "ok": "3203",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3203",
        "ok": "3203",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3203",
        "ok": "3203",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3203",
        "ok": "3203",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3203",
        "ok": "3203",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3203",
        "ok": "3203",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d8b1c": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_214192",
path: "Animal-api sync for customer BE_214192",
pathFormatted: "req_animal-api-sync-d8b1c",
stats: {
    "name": "Animal-api sync for customer BE_214192",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1201",
        "ok": "1201",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1201",
        "ok": "1201",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1201",
        "ok": "1201",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1201",
        "ok": "1201",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1201",
        "ok": "1201",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1201",
        "ok": "1201",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1201",
        "ok": "1201",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-22fcc": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110755",
path: "Animal-api sync for customer NL_110755",
pathFormatted: "req_animal-api-sync-22fcc",
stats: {
    "name": "Animal-api sync for customer NL_110755",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2601",
        "ok": "2601",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2601",
        "ok": "2601",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2601",
        "ok": "2601",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2601",
        "ok": "2601",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2601",
        "ok": "2601",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2601",
        "ok": "2601",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2601",
        "ok": "2601",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-1330d": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_145902",
path: "Animal-api sync for customer BE_145902",
pathFormatted: "req_animal-api-sync-1330d",
stats: {
    "name": "Animal-api sync for customer BE_145902",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2930",
        "ok": "2930",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2930",
        "ok": "2930",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2930",
        "ok": "2930",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2930",
        "ok": "2930",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2930",
        "ok": "2930",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2930",
        "ok": "2930",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2930",
        "ok": "2930",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-2cece": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110572",
path: "Animal-api sync for customer NL_110572",
pathFormatted: "req_animal-api-sync-2cece",
stats: {
    "name": "Animal-api sync for customer NL_110572",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2448",
        "ok": "2448",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2448",
        "ok": "2448",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2448",
        "ok": "2448",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2448",
        "ok": "2448",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2448",
        "ok": "2448",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2448",
        "ok": "2448",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2448",
        "ok": "2448",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-184df": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110881",
path: "Animal-api sync for customer NL_110881",
pathFormatted: "req_animal-api-sync-184df",
stats: {
    "name": "Animal-api sync for customer NL_110881",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2538",
        "ok": "2538",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2538",
        "ok": "2538",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2538",
        "ok": "2538",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2538",
        "ok": "2538",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2538",
        "ok": "2538",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2538",
        "ok": "2538",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2538",
        "ok": "2538",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-61fa0": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_214412",
path: "Animal-api sync for customer NL_214412",
pathFormatted: "req_animal-api-sync-61fa0",
stats: {
    "name": "Animal-api sync for customer NL_214412",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2543",
        "ok": "2543",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2543",
        "ok": "2543",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2543",
        "ok": "2543",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2543",
        "ok": "2543",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2543",
        "ok": "2543",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2543",
        "ok": "2543",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2543",
        "ok": "2543",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-2f673": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_112286",
path: "Animal-api sync for customer NL_112286",
pathFormatted: "req_animal-api-sync-2f673",
stats: {
    "name": "Animal-api sync for customer NL_112286",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1263",
        "ok": "1263",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1263",
        "ok": "1263",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1263",
        "ok": "1263",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1263",
        "ok": "1263",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1263",
        "ok": "1263",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1263",
        "ok": "1263",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1263",
        "ok": "1263",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-7a391": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_125864",
path: "Animal-api sync for customer NL_125864",
pathFormatted: "req_animal-api-sync-7a391",
stats: {
    "name": "Animal-api sync for customer NL_125864",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2745",
        "ok": "2745",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2745",
        "ok": "2745",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2745",
        "ok": "2745",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2745",
        "ok": "2745",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2745",
        "ok": "2745",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2745",
        "ok": "2745",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2745",
        "ok": "2745",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-844f0": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_128832",
path: "Animal-api sync for customer NL_128832",
pathFormatted: "req_animal-api-sync-844f0",
stats: {
    "name": "Animal-api sync for customer NL_128832",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2745",
        "ok": "2745",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2745",
        "ok": "2745",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2745",
        "ok": "2745",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2745",
        "ok": "2745",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2745",
        "ok": "2745",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2745",
        "ok": "2745",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2745",
        "ok": "2745",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-4e25d": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104043",
path: "Animal-api sync for customer NL_104043",
pathFormatted: "req_animal-api-sync-4e25d",
stats: {
    "name": "Animal-api sync for customer NL_104043",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2958",
        "ok": "2958",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2958",
        "ok": "2958",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2958",
        "ok": "2958",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2958",
        "ok": "2958",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2958",
        "ok": "2958",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2958",
        "ok": "2958",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2958",
        "ok": "2958",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-25426": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_114351",
path: "Animal-api sync for customer NL_114351",
pathFormatted: "req_animal-api-sync-25426",
stats: {
    "name": "Animal-api sync for customer NL_114351",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2102",
        "ok": "2102",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2102",
        "ok": "2102",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2102",
        "ok": "2102",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2102",
        "ok": "2102",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2102",
        "ok": "2102",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2102",
        "ok": "2102",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2102",
        "ok": "2102",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-dfb65": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_108668",
path: "Animal-api sync for customer NL_108668",
pathFormatted: "req_animal-api-sync-dfb65",
stats: {
    "name": "Animal-api sync for customer NL_108668",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2547",
        "ok": "2547",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2547",
        "ok": "2547",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2547",
        "ok": "2547",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2547",
        "ok": "2547",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2547",
        "ok": "2547",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2547",
        "ok": "2547",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2547",
        "ok": "2547",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-997d2": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111823",
path: "Animal-api sync for customer NL_111823",
pathFormatted: "req_animal-api-sync-997d2",
stats: {
    "name": "Animal-api sync for customer NL_111823",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2888",
        "ok": "2888",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2888",
        "ok": "2888",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2888",
        "ok": "2888",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2888",
        "ok": "2888",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2888",
        "ok": "2888",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2888",
        "ok": "2888",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2888",
        "ok": "2888",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d5cc8": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_103579",
path: "Animal-api sync for customer NL_103579",
pathFormatted: "req_animal-api-sync-d5cc8",
stats: {
    "name": "Animal-api sync for customer NL_103579",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2326",
        "ok": "2326",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2326",
        "ok": "2326",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2326",
        "ok": "2326",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2326",
        "ok": "2326",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2326",
        "ok": "2326",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2326",
        "ok": "2326",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2326",
        "ok": "2326",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-96467": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_134577",
path: "Animal-api sync for customer NL_134577",
pathFormatted: "req_animal-api-sync-96467",
stats: {
    "name": "Animal-api sync for customer NL_134577",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2175",
        "ok": "2175",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2175",
        "ok": "2175",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2175",
        "ok": "2175",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2175",
        "ok": "2175",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2175",
        "ok": "2175",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2175",
        "ok": "2175",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2175",
        "ok": "2175",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-4c254": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105650",
path: "Animal-api sync for customer NL_105650",
pathFormatted: "req_animal-api-sync-4c254",
stats: {
    "name": "Animal-api sync for customer NL_105650",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2550",
        "ok": "2550",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2550",
        "ok": "2550",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2550",
        "ok": "2550",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2550",
        "ok": "2550",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2550",
        "ok": "2550",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2550",
        "ok": "2550",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2550",
        "ok": "2550",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-5ea6b": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_194847",
path: "Animal-api sync for customer BE_194847",
pathFormatted: "req_animal-api-sync-5ea6b",
stats: {
    "name": "Animal-api sync for customer BE_194847",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4397",
        "ok": "4397",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4397",
        "ok": "4397",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4397",
        "ok": "4397",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4397",
        "ok": "4397",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4397",
        "ok": "4397",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4397",
        "ok": "4397",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4397",
        "ok": "4397",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a4924": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117341",
path: "Animal-api sync for customer NL_117341",
pathFormatted: "req_animal-api-sync-a4924",
stats: {
    "name": "Animal-api sync for customer NL_117341",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2329",
        "ok": "2329",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2329",
        "ok": "2329",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2329",
        "ok": "2329",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2329",
        "ok": "2329",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2329",
        "ok": "2329",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2329",
        "ok": "2329",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2329",
        "ok": "2329",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-12331": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_113115",
path: "Animal-api sync for customer NL_113115",
pathFormatted: "req_animal-api-sync-12331",
stats: {
    "name": "Animal-api sync for customer NL_113115",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3284",
        "ok": "3284",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3284",
        "ok": "3284",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3284",
        "ok": "3284",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3284",
        "ok": "3284",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3284",
        "ok": "3284",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3284",
        "ok": "3284",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3284",
        "ok": "3284",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-13f62": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_149314",
path: "Animal-api sync for customer BE_149314",
pathFormatted: "req_animal-api-sync-13f62",
stats: {
    "name": "Animal-api sync for customer BE_149314",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2361",
        "ok": "2361",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2361",
        "ok": "2361",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2361",
        "ok": "2361",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2361",
        "ok": "2361",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2361",
        "ok": "2361",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2361",
        "ok": "2361",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2361",
        "ok": "2361",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-500bc": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117068",
path: "Animal-api sync for customer NL_117068",
pathFormatted: "req_animal-api-sync-500bc",
stats: {
    "name": "Animal-api sync for customer NL_117068",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1824",
        "ok": "1824",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1824",
        "ok": "1824",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1824",
        "ok": "1824",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1824",
        "ok": "1824",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1824",
        "ok": "1824",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1824",
        "ok": "1824",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1824",
        "ok": "1824",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-6d4c5": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_122660",
path: "Animal-api sync for customer NL_122660",
pathFormatted: "req_animal-api-sync-6d4c5",
stats: {
    "name": "Animal-api sync for customer NL_122660",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1502",
        "ok": "1502",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1502",
        "ok": "1502",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1502",
        "ok": "1502",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1502",
        "ok": "1502",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1502",
        "ok": "1502",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1502",
        "ok": "1502",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1502",
        "ok": "1502",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-cb570": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104777",
path: "Animal-api sync for customer NL_104777",
pathFormatted: "req_animal-api-sync-cb570",
stats: {
    "name": "Animal-api sync for customer NL_104777",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3387",
        "ok": "3387",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3387",
        "ok": "3387",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3387",
        "ok": "3387",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3387",
        "ok": "3387",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3387",
        "ok": "3387",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3387",
        "ok": "3387",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3387",
        "ok": "3387",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-49e6e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_122219",
path: "Animal-api sync for customer NL_122219",
pathFormatted: "req_animal-api-sync-49e6e",
stats: {
    "name": "Animal-api sync for customer NL_122219",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2554",
        "ok": "2554",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2554",
        "ok": "2554",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2554",
        "ok": "2554",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2554",
        "ok": "2554",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2554",
        "ok": "2554",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2554",
        "ok": "2554",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2554",
        "ok": "2554",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b0552": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_122008",
path: "Animal-api sync for customer NL_122008",
pathFormatted: "req_animal-api-sync-b0552",
stats: {
    "name": "Animal-api sync for customer NL_122008",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "6189",
        "ok": "6189",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "6189",
        "ok": "6189",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "6189",
        "ok": "6189",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "6189",
        "ok": "6189",
        "ko": "-"
    },
    "percentiles2": {
        "total": "6189",
        "ok": "6189",
        "ko": "-"
    },
    "percentiles3": {
        "total": "6189",
        "ok": "6189",
        "ko": "-"
    },
    "percentiles4": {
        "total": "6189",
        "ok": "6189",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-7d526": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105576",
path: "Animal-api sync for customer NL_105576",
pathFormatted: "req_animal-api-sync-7d526",
stats: {
    "name": "Animal-api sync for customer NL_105576",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2890",
        "ok": "2890",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2890",
        "ok": "2890",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2890",
        "ok": "2890",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2890",
        "ok": "2890",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2890",
        "ok": "2890",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2890",
        "ok": "2890",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2890",
        "ok": "2890",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a6cf9": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104168",
path: "Animal-api sync for customer NL_104168",
pathFormatted: "req_animal-api-sync-a6cf9",
stats: {
    "name": "Animal-api sync for customer NL_104168",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4526",
        "ok": "4526",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4526",
        "ok": "4526",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4526",
        "ok": "4526",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4526",
        "ok": "4526",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4526",
        "ok": "4526",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4526",
        "ok": "4526",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4526",
        "ok": "4526",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-6809e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117264",
path: "Animal-api sync for customer NL_117264",
pathFormatted: "req_animal-api-sync-6809e",
stats: {
    "name": "Animal-api sync for customer NL_117264",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3387",
        "ok": "3387",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3387",
        "ok": "3387",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3387",
        "ok": "3387",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3387",
        "ok": "3387",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3387",
        "ok": "3387",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3387",
        "ok": "3387",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3387",
        "ok": "3387",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-cb15a": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_114531",
path: "Animal-api sync for customer NL_114531",
pathFormatted: "req_animal-api-sync-cb15a",
stats: {
    "name": "Animal-api sync for customer NL_114531",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3237",
        "ok": "3237",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3237",
        "ok": "3237",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3237",
        "ok": "3237",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3237",
        "ok": "3237",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3237",
        "ok": "3237",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3237",
        "ok": "3237",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3237",
        "ok": "3237",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-aa469": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_158913",
path: "Animal-api sync for customer BE_158913",
pathFormatted: "req_animal-api-sync-aa469",
stats: {
    "name": "Animal-api sync for customer BE_158913",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3912",
        "ok": "3912",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3912",
        "ok": "3912",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3912",
        "ok": "3912",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3912",
        "ok": "3912",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3912",
        "ok": "3912",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3912",
        "ok": "3912",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3912",
        "ok": "3912",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-fc480": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_121609",
path: "Animal-api sync for customer NL_121609",
pathFormatted: "req_animal-api-sync-fc480",
stats: {
    "name": "Animal-api sync for customer NL_121609",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2955",
        "ok": "2955",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2955",
        "ok": "2955",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2955",
        "ok": "2955",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2955",
        "ok": "2955",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2955",
        "ok": "2955",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2955",
        "ok": "2955",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2955",
        "ok": "2955",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-2fa8f": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_103719",
path: "Animal-api sync for customer NL_103719",
pathFormatted: "req_animal-api-sync-2fa8f",
stats: {
    "name": "Animal-api sync for customer NL_103719",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3172",
        "ok": "3172",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3172",
        "ok": "3172",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3172",
        "ok": "3172",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3172",
        "ok": "3172",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3172",
        "ok": "3172",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3172",
        "ok": "3172",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3172",
        "ok": "3172",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c7578": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105839",
path: "Animal-api sync for customer NL_105839",
pathFormatted: "req_animal-api-sync-c7578",
stats: {
    "name": "Animal-api sync for customer NL_105839",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3795",
        "ok": "3795",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3795",
        "ok": "3795",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3795",
        "ok": "3795",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3795",
        "ok": "3795",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3795",
        "ok": "3795",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3795",
        "ok": "3795",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3795",
        "ok": "3795",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-ca9a4": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_123159",
path: "Animal-api sync for customer NL_123159",
pathFormatted: "req_animal-api-sync-ca9a4",
stats: {
    "name": "Animal-api sync for customer NL_123159",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1829",
        "ok": "1829",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1829",
        "ok": "1829",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1829",
        "ok": "1829",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1829",
        "ok": "1829",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1829",
        "ok": "1829",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1829",
        "ok": "1829",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1829",
        "ok": "1829",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d58c5": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_115162",
path: "Animal-api sync for customer NL_115162",
pathFormatted: "req_animal-api-sync-d58c5",
stats: {
    "name": "Animal-api sync for customer NL_115162",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1377",
        "ok": "1377",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1377",
        "ok": "1377",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1377",
        "ok": "1377",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1377",
        "ok": "1377",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1377",
        "ok": "1377",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1377",
        "ok": "1377",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1377",
        "ok": "1377",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d43d8": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_112453",
path: "Animal-api sync for customer NL_112453",
pathFormatted: "req_animal-api-sync-d43d8",
stats: {
    "name": "Animal-api sync for customer NL_112453",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2597",
        "ok": "2597",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2597",
        "ok": "2597",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2597",
        "ok": "2597",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2597",
        "ok": "2597",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2597",
        "ok": "2597",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2597",
        "ok": "2597",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2597",
        "ok": "2597",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-fa50f": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_194938",
path: "Animal-api sync for customer NL_194938",
pathFormatted: "req_animal-api-sync-fa50f",
stats: {
    "name": "Animal-api sync for customer NL_194938",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3420",
        "ok": "3420",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3420",
        "ok": "3420",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3420",
        "ok": "3420",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3420",
        "ok": "3420",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3420",
        "ok": "3420",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3420",
        "ok": "3420",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3420",
        "ok": "3420",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d3cf7": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_123704",
path: "Animal-api sync for customer NL_123704",
pathFormatted: "req_animal-api-sync-d3cf7",
stats: {
    "name": "Animal-api sync for customer NL_123704",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2041",
        "ok": "2041",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2041",
        "ok": "2041",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2041",
        "ok": "2041",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2041",
        "ok": "2041",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2041",
        "ok": "2041",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2041",
        "ok": "2041",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2041",
        "ok": "2041",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e5ad2": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_108858",
path: "Animal-api sync for customer NL_108858",
pathFormatted: "req_animal-api-sync-e5ad2",
stats: {
    "name": "Animal-api sync for customer NL_108858",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3271",
        "ok": "3271",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3271",
        "ok": "3271",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3271",
        "ok": "3271",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3271",
        "ok": "3271",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3271",
        "ok": "3271",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3271",
        "ok": "3271",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3271",
        "ok": "3271",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-23adc": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110836",
path: "Animal-api sync for customer NL_110836",
pathFormatted: "req_animal-api-sync-23adc",
stats: {
    "name": "Animal-api sync for customer NL_110836",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2779",
        "ok": "2779",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2779",
        "ok": "2779",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2779",
        "ok": "2779",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2779",
        "ok": "2779",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2779",
        "ok": "2779",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2779",
        "ok": "2779",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2779",
        "ok": "2779",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-8d897": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_133100",
path: "Animal-api sync for customer NL_133100",
pathFormatted: "req_animal-api-sync-8d897",
stats: {
    "name": "Animal-api sync for customer NL_133100",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2885",
        "ok": "2885",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2885",
        "ok": "2885",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2885",
        "ok": "2885",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2885",
        "ok": "2885",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2885",
        "ok": "2885",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2885",
        "ok": "2885",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2885",
        "ok": "2885",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e921e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_122446",
path: "Animal-api sync for customer NL_122446",
pathFormatted: "req_animal-api-sync-e921e",
stats: {
    "name": "Animal-api sync for customer NL_122446",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3188",
        "ok": "3188",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3188",
        "ok": "3188",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3188",
        "ok": "3188",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3188",
        "ok": "3188",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3188",
        "ok": "3188",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3188",
        "ok": "3188",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3188",
        "ok": "3188",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-fd312": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117690",
path: "Animal-api sync for customer NL_117690",
pathFormatted: "req_animal-api-sync-fd312",
stats: {
    "name": "Animal-api sync for customer NL_117690",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2784",
        "ok": "2784",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2784",
        "ok": "2784",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2784",
        "ok": "2784",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2784",
        "ok": "2784",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2784",
        "ok": "2784",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2784",
        "ok": "2784",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2784",
        "ok": "2784",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c6f3a": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_119414",
path: "Animal-api sync for customer NL_119414",
pathFormatted: "req_animal-api-sync-c6f3a",
stats: {
    "name": "Animal-api sync for customer NL_119414",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2441",
        "ok": "2441",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2441",
        "ok": "2441",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2441",
        "ok": "2441",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2441",
        "ok": "2441",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2441",
        "ok": "2441",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2441",
        "ok": "2441",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2441",
        "ok": "2441",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a8229": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_129350",
path: "Animal-api sync for customer NL_129350",
pathFormatted: "req_animal-api-sync-a8229",
stats: {
    "name": "Animal-api sync for customer NL_129350",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "6476",
        "ok": "6476",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "6476",
        "ok": "6476",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "6476",
        "ok": "6476",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "6476",
        "ok": "6476",
        "ko": "-"
    },
    "percentiles2": {
        "total": "6476",
        "ok": "6476",
        "ko": "-"
    },
    "percentiles3": {
        "total": "6476",
        "ok": "6476",
        "ko": "-"
    },
    "percentiles4": {
        "total": "6476",
        "ok": "6476",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e7e4c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105106",
path: "Animal-api sync for customer NL_105106",
pathFormatted: "req_animal-api-sync-e7e4c",
stats: {
    "name": "Animal-api sync for customer NL_105106",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2548",
        "ok": "2548",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2548",
        "ok": "2548",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2548",
        "ok": "2548",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2548",
        "ok": "2548",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2548",
        "ok": "2548",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2548",
        "ok": "2548",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2548",
        "ok": "2548",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c3423": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_112704",
path: "Animal-api sync for customer NL_112704",
pathFormatted: "req_animal-api-sync-c3423",
stats: {
    "name": "Animal-api sync for customer NL_112704",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4952",
        "ok": "4952",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4952",
        "ok": "4952",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4952",
        "ok": "4952",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4952",
        "ok": "4952",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4952",
        "ok": "4952",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4952",
        "ok": "4952",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4952",
        "ok": "4952",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c2f39": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_157878",
path: "Animal-api sync for customer BE_157878",
pathFormatted: "req_animal-api-sync-c2f39",
stats: {
    "name": "Animal-api sync for customer BE_157878",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3704",
        "ok": "3704",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3704",
        "ok": "3704",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3704",
        "ok": "3704",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3704",
        "ok": "3704",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3704",
        "ok": "3704",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3704",
        "ok": "3704",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3704",
        "ok": "3704",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-fee59": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117006",
path: "Animal-api sync for customer NL_117006",
pathFormatted: "req_animal-api-sync-fee59",
stats: {
    "name": "Animal-api sync for customer NL_117006",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2456",
        "ok": "2456",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2456",
        "ok": "2456",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2456",
        "ok": "2456",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2456",
        "ok": "2456",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2456",
        "ok": "2456",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2456",
        "ok": "2456",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2456",
        "ok": "2456",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-bab68": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_134874",
path: "Animal-api sync for customer NL_134874",
pathFormatted: "req_animal-api-sync-bab68",
stats: {
    "name": "Animal-api sync for customer NL_134874",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2904",
        "ok": "2904",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2904",
        "ok": "2904",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2904",
        "ok": "2904",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2904",
        "ok": "2904",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2904",
        "ok": "2904",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2904",
        "ok": "2904",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2904",
        "ok": "2904",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-3e99c": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_214193",
path: "Animal-api sync for customer BE_214193",
pathFormatted: "req_animal-api-sync-3e99c",
stats: {
    "name": "Animal-api sync for customer BE_214193",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3309",
        "ok": "3309",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3309",
        "ok": "3309",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3309",
        "ok": "3309",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3309",
        "ok": "3309",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3309",
        "ok": "3309",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3309",
        "ok": "3309",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3309",
        "ok": "3309",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b2bc0": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_162281",
path: "Animal-api sync for customer BE_162281",
pathFormatted: "req_animal-api-sync-b2bc0",
stats: {
    "name": "Animal-api sync for customer BE_162281",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4709",
        "ok": "4709",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4709",
        "ok": "4709",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4709",
        "ok": "4709",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4709",
        "ok": "4709",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4709",
        "ok": "4709",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4709",
        "ok": "4709",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4709",
        "ok": "4709",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-046eb": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_127329",
path: "Animal-api sync for customer NL_127329",
pathFormatted: "req_animal-api-sync-046eb",
stats: {
    "name": "Animal-api sync for customer NL_127329",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1526",
        "ok": "1526",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1526",
        "ok": "1526",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1526",
        "ok": "1526",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1526",
        "ok": "1526",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1526",
        "ok": "1526",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1526",
        "ok": "1526",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1526",
        "ok": "1526",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-7cc26": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_131009",
path: "Animal-api sync for customer NL_131009",
pathFormatted: "req_animal-api-sync-7cc26",
stats: {
    "name": "Animal-api sync for customer NL_131009",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3051",
        "ok": "3051",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3051",
        "ok": "3051",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3051",
        "ok": "3051",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3051",
        "ok": "3051",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3051",
        "ok": "3051",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3051",
        "ok": "3051",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3051",
        "ok": "3051",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-52d91": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_163987",
path: "Animal-api sync for customer BE_163987",
pathFormatted: "req_animal-api-sync-52d91",
stats: {
    "name": "Animal-api sync for customer BE_163987",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2823",
        "ok": "2823",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2823",
        "ok": "2823",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2823",
        "ok": "2823",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2823",
        "ok": "2823",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2823",
        "ok": "2823",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2823",
        "ok": "2823",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2823",
        "ok": "2823",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-9b768": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_115732",
path: "Animal-api sync for customer NL_115732",
pathFormatted: "req_animal-api-sync-9b768",
stats: {
    "name": "Animal-api sync for customer NL_115732",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2600",
        "ok": "2600",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2600",
        "ok": "2600",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2600",
        "ok": "2600",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2600",
        "ok": "2600",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2600",
        "ok": "2600",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2600",
        "ok": "2600",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2600",
        "ok": "2600",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c1337": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104350",
path: "Animal-api sync for customer NL_104350",
pathFormatted: "req_animal-api-sync-c1337",
stats: {
    "name": "Animal-api sync for customer NL_104350",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4007",
        "ok": "4007",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4007",
        "ok": "4007",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4007",
        "ok": "4007",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4007",
        "ok": "4007",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4007",
        "ok": "4007",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4007",
        "ok": "4007",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4007",
        "ok": "4007",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-de861": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_138595",
path: "Animal-api sync for customer NL_138595",
pathFormatted: "req_animal-api-sync-de861",
stats: {
    "name": "Animal-api sync for customer NL_138595",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1616",
        "ok": "1616",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1616",
        "ok": "1616",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1616",
        "ok": "1616",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1616",
        "ok": "1616",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1616",
        "ok": "1616",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1616",
        "ok": "1616",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1616",
        "ok": "1616",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-cc8e5": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_123944",
path: "Animal-api sync for customer NL_123944",
pathFormatted: "req_animal-api-sync-cc8e5",
stats: {
    "name": "Animal-api sync for customer NL_123944",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2084",
        "ok": "2084",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2084",
        "ok": "2084",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2084",
        "ok": "2084",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2084",
        "ok": "2084",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2084",
        "ok": "2084",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2084",
        "ok": "2084",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2084",
        "ok": "2084",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-033e7": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_113983",
path: "Animal-api sync for customer NL_113983",
pathFormatted: "req_animal-api-sync-033e7",
stats: {
    "name": "Animal-api sync for customer NL_113983",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1565",
        "ok": "1565",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1565",
        "ok": "1565",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1565",
        "ok": "1565",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1565",
        "ok": "1565",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1565",
        "ok": "1565",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1565",
        "ok": "1565",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1565",
        "ok": "1565",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-cb74a": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_131236",
path: "Animal-api sync for customer NL_131236",
pathFormatted: "req_animal-api-sync-cb74a",
stats: {
    "name": "Animal-api sync for customer NL_131236",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2511",
        "ok": "2511",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2511",
        "ok": "2511",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2511",
        "ok": "2511",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2511",
        "ok": "2511",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2511",
        "ok": "2511",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2511",
        "ok": "2511",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2511",
        "ok": "2511",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-4c33d": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_131701",
path: "Animal-api sync for customer NL_131701",
pathFormatted: "req_animal-api-sync-4c33d",
stats: {
    "name": "Animal-api sync for customer NL_131701",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1899",
        "ok": "1899",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1899",
        "ok": "1899",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1899",
        "ok": "1899",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1899",
        "ok": "1899",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1899",
        "ok": "1899",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1899",
        "ok": "1899",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1899",
        "ok": "1899",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-98e1b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_129146",
path: "Animal-api sync for customer NL_129146",
pathFormatted: "req_animal-api-sync-98e1b",
stats: {
    "name": "Animal-api sync for customer NL_129146",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5015",
        "ok": "5015",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5015",
        "ok": "5015",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5015",
        "ok": "5015",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5015",
        "ok": "5015",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5015",
        "ok": "5015",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5015",
        "ok": "5015",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5015",
        "ok": "5015",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e8633": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117217",
path: "Animal-api sync for customer NL_117217",
pathFormatted: "req_animal-api-sync-e8633",
stats: {
    "name": "Animal-api sync for customer NL_117217",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2364",
        "ok": "2364",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2364",
        "ok": "2364",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2364",
        "ok": "2364",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2364",
        "ok": "2364",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2364",
        "ok": "2364",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2364",
        "ok": "2364",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2364",
        "ok": "2364",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-748ee": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_118193",
path: "Animal-api sync for customer NL_118193",
pathFormatted: "req_animal-api-sync-748ee",
stats: {
    "name": "Animal-api sync for customer NL_118193",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1960",
        "ok": "1960",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1960",
        "ok": "1960",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1960",
        "ok": "1960",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1960",
        "ok": "1960",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1960",
        "ok": "1960",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1960",
        "ok": "1960",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1960",
        "ok": "1960",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-ac942": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_162293",
path: "Animal-api sync for customer BE_162293",
pathFormatted: "req_animal-api-sync-ac942",
stats: {
    "name": "Animal-api sync for customer BE_162293",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3569",
        "ok": "3569",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3569",
        "ok": "3569",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3569",
        "ok": "3569",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3569",
        "ok": "3569",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3569",
        "ok": "3569",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3569",
        "ok": "3569",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3569",
        "ok": "3569",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-18df0": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_132381",
path: "Animal-api sync for customer NL_132381",
pathFormatted: "req_animal-api-sync-18df0",
stats: {
    "name": "Animal-api sync for customer NL_132381",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2246",
        "ok": "2246",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2246",
        "ok": "2246",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2246",
        "ok": "2246",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2246",
        "ok": "2246",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2246",
        "ok": "2246",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2246",
        "ok": "2246",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2246",
        "ok": "2246",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-7e345": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111388",
path: "Animal-api sync for customer NL_111388",
pathFormatted: "req_animal-api-sync-7e345",
stats: {
    "name": "Animal-api sync for customer NL_111388",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3541",
        "ok": "3541",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3541",
        "ok": "3541",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3541",
        "ok": "3541",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3541",
        "ok": "3541",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3541",
        "ok": "3541",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3541",
        "ok": "3541",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3541",
        "ok": "3541",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d3a0e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_118547",
path: "Animal-api sync for customer NL_118547",
pathFormatted: "req_animal-api-sync-d3a0e",
stats: {
    "name": "Animal-api sync for customer NL_118547",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2078",
        "ok": "2078",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2078",
        "ok": "2078",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2078",
        "ok": "2078",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2078",
        "ok": "2078",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2078",
        "ok": "2078",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2078",
        "ok": "2078",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2078",
        "ok": "2078",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-3631b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105143",
path: "Animal-api sync for customer NL_105143",
pathFormatted: "req_animal-api-sync-3631b",
stats: {
    "name": "Animal-api sync for customer NL_105143",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3679",
        "ok": "3679",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3679",
        "ok": "3679",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3679",
        "ok": "3679",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3679",
        "ok": "3679",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3679",
        "ok": "3679",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3679",
        "ok": "3679",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3679",
        "ok": "3679",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-f99c5": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_131859",
path: "Animal-api sync for customer NL_131859",
pathFormatted: "req_animal-api-sync-f99c5",
stats: {
    "name": "Animal-api sync for customer NL_131859",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1416",
        "ok": "1416",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1416",
        "ok": "1416",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1416",
        "ok": "1416",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1416",
        "ok": "1416",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1416",
        "ok": "1416",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1416",
        "ok": "1416",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1416",
        "ok": "1416",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-ba235": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_109254",
path: "Animal-api sync for customer NL_109254",
pathFormatted: "req_animal-api-sync-ba235",
stats: {
    "name": "Animal-api sync for customer NL_109254",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2586",
        "ok": "2586",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2586",
        "ok": "2586",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2586",
        "ok": "2586",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2586",
        "ok": "2586",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2586",
        "ok": "2586",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2586",
        "ok": "2586",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2586",
        "ok": "2586",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e87b4": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_118457",
path: "Animal-api sync for customer NL_118457",
pathFormatted: "req_animal-api-sync-e87b4",
stats: {
    "name": "Animal-api sync for customer NL_118457",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3216",
        "ok": "3216",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3216",
        "ok": "3216",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3216",
        "ok": "3216",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3216",
        "ok": "3216",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3216",
        "ok": "3216",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3216",
        "ok": "3216",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3216",
        "ok": "3216",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-84066": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_136422",
path: "Animal-api sync for customer NL_136422",
pathFormatted: "req_animal-api-sync-84066",
stats: {
    "name": "Animal-api sync for customer NL_136422",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2530",
        "ok": "2530",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2530",
        "ok": "2530",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2530",
        "ok": "2530",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2530",
        "ok": "2530",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2530",
        "ok": "2530",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2530",
        "ok": "2530",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2530",
        "ok": "2530",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-f4249": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_138300",
path: "Animal-api sync for customer NL_138300",
pathFormatted: "req_animal-api-sync-f4249",
stats: {
    "name": "Animal-api sync for customer NL_138300",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3045",
        "ok": "3045",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3045",
        "ok": "3045",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3045",
        "ok": "3045",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3045",
        "ok": "3045",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3045",
        "ok": "3045",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3045",
        "ok": "3045",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3045",
        "ok": "3045",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-32bc3": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_103585",
path: "Animal-api sync for customer NL_103585",
pathFormatted: "req_animal-api-sync-32bc3",
stats: {
    "name": "Animal-api sync for customer NL_103585",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2902",
        "ok": "2902",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2902",
        "ok": "2902",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2902",
        "ok": "2902",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2902",
        "ok": "2902",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2902",
        "ok": "2902",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2902",
        "ok": "2902",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2902",
        "ok": "2902",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-787cb": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_143564",
path: "Animal-api sync for customer NL_143564",
pathFormatted: "req_animal-api-sync-787cb",
stats: {
    "name": "Animal-api sync for customer NL_143564",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1952",
        "ok": "1952",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1952",
        "ok": "1952",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1952",
        "ok": "1952",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1952",
        "ok": "1952",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1952",
        "ok": "1952",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1952",
        "ok": "1952",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1952",
        "ok": "1952",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-0f436": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_107376",
path: "Animal-api sync for customer NL_107376",
pathFormatted: "req_animal-api-sync-0f436",
stats: {
    "name": "Animal-api sync for customer NL_107376",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3756",
        "ok": "3756",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3756",
        "ok": "3756",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3756",
        "ok": "3756",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3756",
        "ok": "3756",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3756",
        "ok": "3756",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3756",
        "ok": "3756",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3756",
        "ok": "3756",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-20cd3": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110661",
path: "Animal-api sync for customer NL_110661",
pathFormatted: "req_animal-api-sync-20cd3",
stats: {
    "name": "Animal-api sync for customer NL_110661",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2846",
        "ok": "2846",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2846",
        "ok": "2846",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2846",
        "ok": "2846",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2846",
        "ok": "2846",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2846",
        "ok": "2846",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2846",
        "ok": "2846",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2846",
        "ok": "2846",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-eb26b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_118799",
path: "Animal-api sync for customer NL_118799",
pathFormatted: "req_animal-api-sync-eb26b",
stats: {
    "name": "Animal-api sync for customer NL_118799",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2664",
        "ok": "2664",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2664",
        "ok": "2664",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2664",
        "ok": "2664",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2664",
        "ok": "2664",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2664",
        "ok": "2664",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2664",
        "ok": "2664",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2664",
        "ok": "2664",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c1388": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_122597",
path: "Animal-api sync for customer NL_122597",
pathFormatted: "req_animal-api-sync-c1388",
stats: {
    "name": "Animal-api sync for customer NL_122597",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4179",
        "ok": "4179",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4179",
        "ok": "4179",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4179",
        "ok": "4179",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4179",
        "ok": "4179",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4179",
        "ok": "4179",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4179",
        "ok": "4179",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4179",
        "ok": "4179",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-28731": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_136181",
path: "Animal-api sync for customer NL_136181",
pathFormatted: "req_animal-api-sync-28731",
stats: {
    "name": "Animal-api sync for customer NL_136181",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "2747",
        "ok": "-",
        "ko": "2747"
    },
    "maxResponseTime": {
        "total": "2747",
        "ok": "-",
        "ko": "2747"
    },
    "meanResponseTime": {
        "total": "2747",
        "ok": "-",
        "ko": "2747"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "2747",
        "ok": "-",
        "ko": "2747"
    },
    "percentiles2": {
        "total": "2747",
        "ok": "-",
        "ko": "2747"
    },
    "percentiles3": {
        "total": "2747",
        "ok": "-",
        "ko": "2747"
    },
    "percentiles4": {
        "total": "2747",
        "ok": "-",
        "ko": "2747"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-da9fa": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_212118",
path: "Animal-api sync for customer NL_212118",
pathFormatted: "req_animal-api-sync-da9fa",
stats: {
    "name": "Animal-api sync for customer NL_212118",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1955",
        "ok": "1955",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1955",
        "ok": "1955",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1955",
        "ok": "1955",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1955",
        "ok": "1955",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1955",
        "ok": "1955",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1955",
        "ok": "1955",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1955",
        "ok": "1955",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-2b764": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111875",
path: "Animal-api sync for customer NL_111875",
pathFormatted: "req_animal-api-sync-2b764",
stats: {
    "name": "Animal-api sync for customer NL_111875",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3088",
        "ok": "3088",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3088",
        "ok": "3088",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3088",
        "ok": "3088",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3088",
        "ok": "3088",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3088",
        "ok": "3088",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3088",
        "ok": "3088",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3088",
        "ok": "3088",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-56095": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_125359",
path: "Animal-api sync for customer NL_125359",
pathFormatted: "req_animal-api-sync-56095",
stats: {
    "name": "Animal-api sync for customer NL_125359",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2512",
        "ok": "2512",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2512",
        "ok": "2512",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2512",
        "ok": "2512",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2512",
        "ok": "2512",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2512",
        "ok": "2512",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2512",
        "ok": "2512",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2512",
        "ok": "2512",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-7c52f": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_112020",
path: "Animal-api sync for customer NL_112020",
pathFormatted: "req_animal-api-sync-7c52f",
stats: {
    "name": "Animal-api sync for customer NL_112020",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3544",
        "ok": "3544",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3544",
        "ok": "3544",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3544",
        "ok": "3544",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3544",
        "ok": "3544",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3544",
        "ok": "3544",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3544",
        "ok": "3544",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3544",
        "ok": "3544",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-1586b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_113408",
path: "Animal-api sync for customer NL_113408",
pathFormatted: "req_animal-api-sync-1586b",
stats: {
    "name": "Animal-api sync for customer NL_113408",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5096",
        "ok": "5096",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5096",
        "ok": "5096",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5096",
        "ok": "5096",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5096",
        "ok": "5096",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5096",
        "ok": "5096",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5096",
        "ok": "5096",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5096",
        "ok": "5096",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-bca62": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_112986",
path: "Animal-api sync for customer NL_112986",
pathFormatted: "req_animal-api-sync-bca62",
stats: {
    "name": "Animal-api sync for customer NL_112986",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4311",
        "ok": "4311",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4311",
        "ok": "4311",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4311",
        "ok": "4311",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4311",
        "ok": "4311",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4311",
        "ok": "4311",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4311",
        "ok": "4311",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4311",
        "ok": "4311",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d8a50": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_109324",
path: "Animal-api sync for customer NL_109324",
pathFormatted: "req_animal-api-sync-d8a50",
stats: {
    "name": "Animal-api sync for customer NL_109324",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4578",
        "ok": "4578",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4578",
        "ok": "4578",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4578",
        "ok": "4578",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4578",
        "ok": "4578",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4578",
        "ok": "4578",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4578",
        "ok": "4578",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4578",
        "ok": "4578",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d4b4d": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_123556",
path: "Animal-api sync for customer NL_123556",
pathFormatted: "req_animal-api-sync-d4b4d",
stats: {
    "name": "Animal-api sync for customer NL_123556",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2711",
        "ok": "2711",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2711",
        "ok": "2711",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2711",
        "ok": "2711",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2711",
        "ok": "2711",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2711",
        "ok": "2711",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2711",
        "ok": "2711",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2711",
        "ok": "2711",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-bc7d0": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_151016",
path: "Animal-api sync for customer BE_151016",
pathFormatted: "req_animal-api-sync-bc7d0",
stats: {
    "name": "Animal-api sync for customer BE_151016",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3702",
        "ok": "3702",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3702",
        "ok": "3702",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3702",
        "ok": "3702",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3702",
        "ok": "3702",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3702",
        "ok": "3702",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3702",
        "ok": "3702",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3702",
        "ok": "3702",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a0b51": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_118379",
path: "Animal-api sync for customer NL_118379",
pathFormatted: "req_animal-api-sync-a0b51",
stats: {
    "name": "Animal-api sync for customer NL_118379",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3335",
        "ok": "3335",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3335",
        "ok": "3335",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3335",
        "ok": "3335",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3335",
        "ok": "3335",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3335",
        "ok": "3335",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3335",
        "ok": "3335",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3335",
        "ok": "3335",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b6f25": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_119444",
path: "Animal-api sync for customer NL_119444",
pathFormatted: "req_animal-api-sync-b6f25",
stats: {
    "name": "Animal-api sync for customer NL_119444",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3055",
        "ok": "3055",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3055",
        "ok": "3055",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3055",
        "ok": "3055",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3055",
        "ok": "3055",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3055",
        "ok": "3055",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3055",
        "ok": "3055",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3055",
        "ok": "3055",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b9666": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_207943",
path: "Animal-api sync for customer BE_207943",
pathFormatted: "req_animal-api-sync-b9666",
stats: {
    "name": "Animal-api sync for customer BE_207943",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3407",
        "ok": "3407",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3407",
        "ok": "3407",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3407",
        "ok": "3407",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3407",
        "ok": "3407",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3407",
        "ok": "3407",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3407",
        "ok": "3407",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3407",
        "ok": "3407",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-662d1": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_211608",
path: "Animal-api sync for customer BE_211608",
pathFormatted: "req_animal-api-sync-662d1",
stats: {
    "name": "Animal-api sync for customer BE_211608",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3299",
        "ok": "3299",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3299",
        "ok": "3299",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3299",
        "ok": "3299",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3299",
        "ok": "3299",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3299",
        "ok": "3299",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3299",
        "ok": "3299",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3299",
        "ok": "3299",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-58eb2": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_125881",
path: "Animal-api sync for customer NL_125881",
pathFormatted: "req_animal-api-sync-58eb2",
stats: {
    "name": "Animal-api sync for customer NL_125881",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2018",
        "ok": "2018",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2018",
        "ok": "2018",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2018",
        "ok": "2018",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2018",
        "ok": "2018",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2018",
        "ok": "2018",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2018",
        "ok": "2018",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2018",
        "ok": "2018",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-cd728": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117817",
path: "Animal-api sync for customer NL_117817",
pathFormatted: "req_animal-api-sync-cd728",
stats: {
    "name": "Animal-api sync for customer NL_117817",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3375",
        "ok": "3375",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3375",
        "ok": "3375",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3375",
        "ok": "3375",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3375",
        "ok": "3375",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3375",
        "ok": "3375",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3375",
        "ok": "3375",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3375",
        "ok": "3375",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d0381": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_126231",
path: "Animal-api sync for customer NL_126231",
pathFormatted: "req_animal-api-sync-d0381",
stats: {
    "name": "Animal-api sync for customer NL_126231",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3155",
        "ok": "3155",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3155",
        "ok": "3155",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3155",
        "ok": "3155",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3155",
        "ok": "3155",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3155",
        "ok": "3155",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3155",
        "ok": "3155",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3155",
        "ok": "3155",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-1164c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104369",
path: "Animal-api sync for customer NL_104369",
pathFormatted: "req_animal-api-sync-1164c",
stats: {
    "name": "Animal-api sync for customer NL_104369",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3495",
        "ok": "3495",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3495",
        "ok": "3495",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3495",
        "ok": "3495",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3495",
        "ok": "3495",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3495",
        "ok": "3495",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3495",
        "ok": "3495",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3495",
        "ok": "3495",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c2774": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_130077",
path: "Animal-api sync for customer NL_130077",
pathFormatted: "req_animal-api-sync-c2774",
stats: {
    "name": "Animal-api sync for customer NL_130077",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2966",
        "ok": "2966",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2966",
        "ok": "2966",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2966",
        "ok": "2966",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2966",
        "ok": "2966",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2966",
        "ok": "2966",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2966",
        "ok": "2966",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2966",
        "ok": "2966",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-da2a1": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105775",
path: "Animal-api sync for customer NL_105775",
pathFormatted: "req_animal-api-sync-da2a1",
stats: {
    "name": "Animal-api sync for customer NL_105775",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3035",
        "ok": "3035",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3035",
        "ok": "3035",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3035",
        "ok": "3035",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3035",
        "ok": "3035",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3035",
        "ok": "3035",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3035",
        "ok": "3035",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3035",
        "ok": "3035",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-12657": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_212029",
path: "Animal-api sync for customer BE_212029",
pathFormatted: "req_animal-api-sync-12657",
stats: {
    "name": "Animal-api sync for customer BE_212029",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3907",
        "ok": "3907",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3907",
        "ok": "3907",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3907",
        "ok": "3907",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3907",
        "ok": "3907",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3907",
        "ok": "3907",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3907",
        "ok": "3907",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3907",
        "ok": "3907",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a231f": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_119073",
path: "Animal-api sync for customer NL_119073",
pathFormatted: "req_animal-api-sync-a231f",
stats: {
    "name": "Animal-api sync for customer NL_119073",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2743",
        "ok": "2743",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2743",
        "ok": "2743",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2743",
        "ok": "2743",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2743",
        "ok": "2743",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2743",
        "ok": "2743",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2743",
        "ok": "2743",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2743",
        "ok": "2743",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a5dcb": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_113563",
path: "Animal-api sync for customer NL_113563",
pathFormatted: "req_animal-api-sync-a5dcb",
stats: {
    "name": "Animal-api sync for customer NL_113563",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "7360",
        "ok": "7360",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "7360",
        "ok": "7360",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "7360",
        "ok": "7360",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "7360",
        "ok": "7360",
        "ko": "-"
    },
    "percentiles2": {
        "total": "7360",
        "ok": "7360",
        "ko": "-"
    },
    "percentiles3": {
        "total": "7360",
        "ok": "7360",
        "ko": "-"
    },
    "percentiles4": {
        "total": "7360",
        "ok": "7360",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-0d921": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_124334",
path: "Animal-api sync for customer NL_124334",
pathFormatted: "req_animal-api-sync-0d921",
stats: {
    "name": "Animal-api sync for customer NL_124334",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2604",
        "ok": "2604",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2604",
        "ok": "2604",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2604",
        "ok": "2604",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2604",
        "ok": "2604",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2604",
        "ok": "2604",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2604",
        "ok": "2604",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2604",
        "ok": "2604",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a5308": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_129853",
path: "Animal-api sync for customer NL_129853",
pathFormatted: "req_animal-api-sync-a5308",
stats: {
    "name": "Animal-api sync for customer NL_129853",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2014",
        "ok": "2014",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2014",
        "ok": "2014",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2014",
        "ok": "2014",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2014",
        "ok": "2014",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2014",
        "ok": "2014",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2014",
        "ok": "2014",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2014",
        "ok": "2014",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a8587": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_112236",
path: "Animal-api sync for customer NL_112236",
pathFormatted: "req_animal-api-sync-a8587",
stats: {
    "name": "Animal-api sync for customer NL_112236",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2365",
        "ok": "2365",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2365",
        "ok": "2365",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2365",
        "ok": "2365",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2365",
        "ok": "2365",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2365",
        "ok": "2365",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2365",
        "ok": "2365",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2365",
        "ok": "2365",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c1a9b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105397",
path: "Animal-api sync for customer NL_105397",
pathFormatted: "req_animal-api-sync-c1a9b",
stats: {
    "name": "Animal-api sync for customer NL_105397",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4274",
        "ok": "4274",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4274",
        "ok": "4274",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4274",
        "ok": "4274",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4274",
        "ok": "4274",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4274",
        "ok": "4274",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4274",
        "ok": "4274",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4274",
        "ok": "4274",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-f47d9": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_162268",
path: "Animal-api sync for customer NL_162268",
pathFormatted: "req_animal-api-sync-f47d9",
stats: {
    "name": "Animal-api sync for customer NL_162268",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4677",
        "ok": "4677",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4677",
        "ok": "4677",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4677",
        "ok": "4677",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4677",
        "ok": "4677",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4677",
        "ok": "4677",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4677",
        "ok": "4677",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4677",
        "ok": "4677",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-7141e": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_195066",
path: "Animal-api sync for customer BE_195066",
pathFormatted: "req_animal-api-sync-7141e",
stats: {
    "name": "Animal-api sync for customer BE_195066",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4665",
        "ok": "4665",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4665",
        "ok": "4665",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4665",
        "ok": "4665",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4665",
        "ok": "4665",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4665",
        "ok": "4665",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4665",
        "ok": "4665",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4665",
        "ok": "4665",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-9fb14": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_115364",
path: "Animal-api sync for customer NL_115364",
pathFormatted: "req_animal-api-sync-9fb14",
stats: {
    "name": "Animal-api sync for customer NL_115364",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3427",
        "ok": "3427",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3427",
        "ok": "3427",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3427",
        "ok": "3427",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3427",
        "ok": "3427",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3427",
        "ok": "3427",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3427",
        "ok": "3427",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3427",
        "ok": "3427",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-29ae3": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_107774",
path: "Animal-api sync for customer NL_107774",
pathFormatted: "req_animal-api-sync-29ae3",
stats: {
    "name": "Animal-api sync for customer NL_107774",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5339",
        "ok": "5339",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5339",
        "ok": "5339",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5339",
        "ok": "5339",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5339",
        "ok": "5339",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5339",
        "ok": "5339",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5339",
        "ok": "5339",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5339",
        "ok": "5339",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e24f9": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_125084",
path: "Animal-api sync for customer NL_125084",
pathFormatted: "req_animal-api-sync-e24f9",
stats: {
    "name": "Animal-api sync for customer NL_125084",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3142",
        "ok": "3142",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3142",
        "ok": "3142",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3142",
        "ok": "3142",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3142",
        "ok": "3142",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3142",
        "ok": "3142",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3142",
        "ok": "3142",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3142",
        "ok": "3142",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-6bcf1": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_130692",
path: "Animal-api sync for customer NL_130692",
pathFormatted: "req_animal-api-sync-6bcf1",
stats: {
    "name": "Animal-api sync for customer NL_130692",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3244",
        "ok": "3244",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3244",
        "ok": "3244",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3244",
        "ok": "3244",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3244",
        "ok": "3244",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3244",
        "ok": "3244",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3244",
        "ok": "3244",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3244",
        "ok": "3244",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-42cac": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117138",
path: "Animal-api sync for customer NL_117138",
pathFormatted: "req_animal-api-sync-42cac",
stats: {
    "name": "Animal-api sync for customer NL_117138",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3320",
        "ok": "3320",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3320",
        "ok": "3320",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3320",
        "ok": "3320",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3320",
        "ok": "3320",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3320",
        "ok": "3320",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3320",
        "ok": "3320",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3320",
        "ok": "3320",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-7ddaa": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_127532",
path: "Animal-api sync for customer NL_127532",
pathFormatted: "req_animal-api-sync-7ddaa",
stats: {
    "name": "Animal-api sync for customer NL_127532",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3808",
        "ok": "3808",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3808",
        "ok": "3808",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3808",
        "ok": "3808",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3808",
        "ok": "3808",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3808",
        "ok": "3808",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3808",
        "ok": "3808",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3808",
        "ok": "3808",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-35067": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_145405",
path: "Animal-api sync for customer BE_145405",
pathFormatted: "req_animal-api-sync-35067",
stats: {
    "name": "Animal-api sync for customer BE_145405",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4257",
        "ok": "4257",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4257",
        "ok": "4257",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4257",
        "ok": "4257",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4257",
        "ok": "4257",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4257",
        "ok": "4257",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4257",
        "ok": "4257",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4257",
        "ok": "4257",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-2e40c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_131445",
path: "Animal-api sync for customer NL_131445",
pathFormatted: "req_animal-api-sync-2e40c",
stats: {
    "name": "Animal-api sync for customer NL_131445",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3155",
        "ok": "3155",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3155",
        "ok": "3155",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3155",
        "ok": "3155",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3155",
        "ok": "3155",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3155",
        "ok": "3155",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3155",
        "ok": "3155",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3155",
        "ok": "3155",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-3eb24": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_103773",
path: "Animal-api sync for customer NL_103773",
pathFormatted: "req_animal-api-sync-3eb24",
stats: {
    "name": "Animal-api sync for customer NL_103773",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3887",
        "ok": "3887",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3887",
        "ok": "3887",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3887",
        "ok": "3887",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3887",
        "ok": "3887",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3887",
        "ok": "3887",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3887",
        "ok": "3887",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3887",
        "ok": "3887",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-db524": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104670",
path: "Animal-api sync for customer NL_104670",
pathFormatted: "req_animal-api-sync-db524",
stats: {
    "name": "Animal-api sync for customer NL_104670",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4488",
        "ok": "4488",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4488",
        "ok": "4488",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4488",
        "ok": "4488",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4488",
        "ok": "4488",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4488",
        "ok": "4488",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4488",
        "ok": "4488",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4488",
        "ok": "4488",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-8824d": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_129197",
path: "Animal-api sync for customer NL_129197",
pathFormatted: "req_animal-api-sync-8824d",
stats: {
    "name": "Animal-api sync for customer NL_129197",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5611",
        "ok": "5611",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5611",
        "ok": "5611",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5611",
        "ok": "5611",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5611",
        "ok": "5611",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5611",
        "ok": "5611",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5611",
        "ok": "5611",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5611",
        "ok": "5611",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d2007": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_214023",
path: "Animal-api sync for customer BE_214023",
pathFormatted: "req_animal-api-sync-d2007",
stats: {
    "name": "Animal-api sync for customer BE_214023",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4502",
        "ok": "4502",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4502",
        "ok": "4502",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4502",
        "ok": "4502",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4502",
        "ok": "4502",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4502",
        "ok": "4502",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4502",
        "ok": "4502",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4502",
        "ok": "4502",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e79fc": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_114849",
path: "Animal-api sync for customer NL_114849",
pathFormatted: "req_animal-api-sync-e79fc",
stats: {
    "name": "Animal-api sync for customer NL_114849",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4544",
        "ok": "4544",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4544",
        "ok": "4544",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4544",
        "ok": "4544",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4544",
        "ok": "4544",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4544",
        "ok": "4544",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4544",
        "ok": "4544",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4544",
        "ok": "4544",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-6b259": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_118526",
path: "Animal-api sync for customer NL_118526",
pathFormatted: "req_animal-api-sync-6b259",
stats: {
    "name": "Animal-api sync for customer NL_118526",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3125",
        "ok": "3125",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3125",
        "ok": "3125",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3125",
        "ok": "3125",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3125",
        "ok": "3125",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3125",
        "ok": "3125",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3125",
        "ok": "3125",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3125",
        "ok": "3125",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-1f585": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_123243",
path: "Animal-api sync for customer NL_123243",
pathFormatted: "req_animal-api-sync-1f585",
stats: {
    "name": "Animal-api sync for customer NL_123243",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3854",
        "ok": "3854",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3854",
        "ok": "3854",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3854",
        "ok": "3854",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3854",
        "ok": "3854",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3854",
        "ok": "3854",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3854",
        "ok": "3854",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3854",
        "ok": "3854",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-360a2": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_120728",
path: "Animal-api sync for customer NL_120728",
pathFormatted: "req_animal-api-sync-360a2",
stats: {
    "name": "Animal-api sync for customer NL_120728",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5718",
        "ok": "5718",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5718",
        "ok": "5718",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5718",
        "ok": "5718",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5718",
        "ok": "5718",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5718",
        "ok": "5718",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5718",
        "ok": "5718",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5718",
        "ok": "5718",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-964ea": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_131647",
path: "Animal-api sync for customer NL_131647",
pathFormatted: "req_animal-api-sync-964ea",
stats: {
    "name": "Animal-api sync for customer NL_131647",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2890",
        "ok": "2890",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2890",
        "ok": "2890",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2890",
        "ok": "2890",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2890",
        "ok": "2890",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2890",
        "ok": "2890",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2890",
        "ok": "2890",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2890",
        "ok": "2890",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-90a7a": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_158870",
path: "Animal-api sync for customer NL_158870",
pathFormatted: "req_animal-api-sync-90a7a",
stats: {
    "name": "Animal-api sync for customer NL_158870",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "9055",
        "ok": "9055",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "9055",
        "ok": "9055",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "9055",
        "ok": "9055",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "9055",
        "ok": "9055",
        "ko": "-"
    },
    "percentiles2": {
        "total": "9055",
        "ok": "9055",
        "ko": "-"
    },
    "percentiles3": {
        "total": "9055",
        "ok": "9055",
        "ko": "-"
    },
    "percentiles4": {
        "total": "9055",
        "ok": "9055",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-ee74d": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_149842",
path: "Animal-api sync for customer BE_149842",
pathFormatted: "req_animal-api-sync-ee74d",
stats: {
    "name": "Animal-api sync for customer BE_149842",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2992",
        "ok": "2992",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2992",
        "ok": "2992",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2992",
        "ok": "2992",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2992",
        "ok": "2992",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2992",
        "ok": "2992",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2992",
        "ok": "2992",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2992",
        "ok": "2992",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-edbd5": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_156716",
path: "Animal-api sync for customer NL_156716",
pathFormatted: "req_animal-api-sync-edbd5",
stats: {
    "name": "Animal-api sync for customer NL_156716",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4570",
        "ok": "4570",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4570",
        "ok": "4570",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4570",
        "ok": "4570",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4570",
        "ok": "4570",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4570",
        "ok": "4570",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4570",
        "ok": "4570",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4570",
        "ok": "4570",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b6490": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_128966",
path: "Animal-api sync for customer NL_128966",
pathFormatted: "req_animal-api-sync-b6490",
stats: {
    "name": "Animal-api sync for customer NL_128966",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2831",
        "ok": "2831",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2831",
        "ok": "2831",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2831",
        "ok": "2831",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2831",
        "ok": "2831",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2831",
        "ok": "2831",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2831",
        "ok": "2831",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2831",
        "ok": "2831",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-5eb0d": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_129758",
path: "Animal-api sync for customer NL_129758",
pathFormatted: "req_animal-api-sync-5eb0d",
stats: {
    "name": "Animal-api sync for customer NL_129758",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3973",
        "ok": "3973",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3973",
        "ok": "3973",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3973",
        "ok": "3973",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3973",
        "ok": "3973",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3973",
        "ok": "3973",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3973",
        "ok": "3973",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3973",
        "ok": "3973",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-7665a": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_107608",
path: "Animal-api sync for customer NL_107608",
pathFormatted: "req_animal-api-sync-7665a",
stats: {
    "name": "Animal-api sync for customer NL_107608",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3201",
        "ok": "3201",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3201",
        "ok": "3201",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3201",
        "ok": "3201",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3201",
        "ok": "3201",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3201",
        "ok": "3201",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3201",
        "ok": "3201",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3201",
        "ok": "3201",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-352f1": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_120975",
path: "Animal-api sync for customer NL_120975",
pathFormatted: "req_animal-api-sync-352f1",
stats: {
    "name": "Animal-api sync for customer NL_120975",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3730",
        "ok": "3730",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3730",
        "ok": "3730",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3730",
        "ok": "3730",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3730",
        "ok": "3730",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3730",
        "ok": "3730",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3730",
        "ok": "3730",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3730",
        "ok": "3730",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a1b1c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105501",
path: "Animal-api sync for customer NL_105501",
pathFormatted: "req_animal-api-sync-a1b1c",
stats: {
    "name": "Animal-api sync for customer NL_105501",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3314",
        "ok": "3314",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3314",
        "ok": "3314",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3314",
        "ok": "3314",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3314",
        "ok": "3314",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3314",
        "ok": "3314",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3314",
        "ok": "3314",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3314",
        "ok": "3314",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-558ec": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_115428",
path: "Animal-api sync for customer NL_115428",
pathFormatted: "req_animal-api-sync-558ec",
stats: {
    "name": "Animal-api sync for customer NL_115428",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1548",
        "ok": "1548",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1548",
        "ok": "1548",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1548",
        "ok": "1548",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1548",
        "ok": "1548",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1548",
        "ok": "1548",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1548",
        "ok": "1548",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1548",
        "ok": "1548",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e201e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110096",
path: "Animal-api sync for customer NL_110096",
pathFormatted: "req_animal-api-sync-e201e",
stats: {
    "name": "Animal-api sync for customer NL_110096",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3171",
        "ok": "3171",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3171",
        "ok": "3171",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3171",
        "ok": "3171",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3171",
        "ok": "3171",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3171",
        "ok": "3171",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3171",
        "ok": "3171",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3171",
        "ok": "3171",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-66a67": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_115297",
path: "Animal-api sync for customer NL_115297",
pathFormatted: "req_animal-api-sync-66a67",
stats: {
    "name": "Animal-api sync for customer NL_115297",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3257",
        "ok": "3257",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3257",
        "ok": "3257",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3257",
        "ok": "3257",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3257",
        "ok": "3257",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3257",
        "ok": "3257",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3257",
        "ok": "3257",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3257",
        "ok": "3257",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e614b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_114481",
path: "Animal-api sync for customer NL_114481",
pathFormatted: "req_animal-api-sync-e614b",
stats: {
    "name": "Animal-api sync for customer NL_114481",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3025",
        "ok": "3025",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3025",
        "ok": "3025",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3025",
        "ok": "3025",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3025",
        "ok": "3025",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3025",
        "ok": "3025",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3025",
        "ok": "3025",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3025",
        "ok": "3025",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a326b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_109292",
path: "Animal-api sync for customer NL_109292",
pathFormatted: "req_animal-api-sync-a326b",
stats: {
    "name": "Animal-api sync for customer NL_109292",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3913",
        "ok": "3913",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3913",
        "ok": "3913",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3913",
        "ok": "3913",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3913",
        "ok": "3913",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3913",
        "ok": "3913",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3913",
        "ok": "3913",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3913",
        "ok": "3913",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-ec201": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105082",
path: "Animal-api sync for customer NL_105082",
pathFormatted: "req_animal-api-sync-ec201",
stats: {
    "name": "Animal-api sync for customer NL_105082",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4095",
        "ok": "4095",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4095",
        "ok": "4095",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4095",
        "ok": "4095",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4095",
        "ok": "4095",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4095",
        "ok": "4095",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4095",
        "ok": "4095",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4095",
        "ok": "4095",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c9dab": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_122464",
path: "Animal-api sync for customer NL_122464",
pathFormatted: "req_animal-api-sync-c9dab",
stats: {
    "name": "Animal-api sync for customer NL_122464",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4323",
        "ok": "4323",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4323",
        "ok": "4323",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4323",
        "ok": "4323",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4323",
        "ok": "4323",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4323",
        "ok": "4323",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4323",
        "ok": "4323",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4323",
        "ok": "4323",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-2092d": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_150518",
path: "Animal-api sync for customer BE_150518",
pathFormatted: "req_animal-api-sync-2092d",
stats: {
    "name": "Animal-api sync for customer BE_150518",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3513",
        "ok": "3513",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3513",
        "ok": "3513",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3513",
        "ok": "3513",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3513",
        "ok": "3513",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3513",
        "ok": "3513",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3513",
        "ok": "3513",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3513",
        "ok": "3513",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-cdf93": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_118194",
path: "Animal-api sync for customer NL_118194",
pathFormatted: "req_animal-api-sync-cdf93",
stats: {
    "name": "Animal-api sync for customer NL_118194",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3346",
        "ok": "3346",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3346",
        "ok": "3346",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3346",
        "ok": "3346",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3346",
        "ok": "3346",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3346",
        "ok": "3346",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3346",
        "ok": "3346",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3346",
        "ok": "3346",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-ac6d4": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_121952",
path: "Animal-api sync for customer NL_121952",
pathFormatted: "req_animal-api-sync-ac6d4",
stats: {
    "name": "Animal-api sync for customer NL_121952",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4660",
        "ok": "4660",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4660",
        "ok": "4660",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4660",
        "ok": "4660",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4660",
        "ok": "4660",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4660",
        "ok": "4660",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4660",
        "ok": "4660",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4660",
        "ok": "4660",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-0ec02": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_114079",
path: "Animal-api sync for customer NL_114079",
pathFormatted: "req_animal-api-sync-0ec02",
stats: {
    "name": "Animal-api sync for customer NL_114079",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2378",
        "ok": "2378",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2378",
        "ok": "2378",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2378",
        "ok": "2378",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2378",
        "ok": "2378",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2378",
        "ok": "2378",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2378",
        "ok": "2378",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2378",
        "ok": "2378",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-2901a": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_136544",
path: "Animal-api sync for customer NL_136544",
pathFormatted: "req_animal-api-sync-2901a",
stats: {
    "name": "Animal-api sync for customer NL_136544",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2687",
        "ok": "2687",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2687",
        "ok": "2687",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2687",
        "ok": "2687",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2687",
        "ok": "2687",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2687",
        "ok": "2687",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2687",
        "ok": "2687",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2687",
        "ok": "2687",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-1a27a": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_120522",
path: "Animal-api sync for customer NL_120522",
pathFormatted: "req_animal-api-sync-1a27a",
stats: {
    "name": "Animal-api sync for customer NL_120522",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3438",
        "ok": "3438",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3438",
        "ok": "3438",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3438",
        "ok": "3438",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3438",
        "ok": "3438",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3438",
        "ok": "3438",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3438",
        "ok": "3438",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3438",
        "ok": "3438",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-317c3": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_132801",
path: "Animal-api sync for customer NL_132801",
pathFormatted: "req_animal-api-sync-317c3",
stats: {
    "name": "Animal-api sync for customer NL_132801",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2584",
        "ok": "2584",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2584",
        "ok": "2584",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2584",
        "ok": "2584",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2584",
        "ok": "2584",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2584",
        "ok": "2584",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2584",
        "ok": "2584",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2584",
        "ok": "2584",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-83baa": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_114916",
path: "Animal-api sync for customer NL_114916",
pathFormatted: "req_animal-api-sync-83baa",
stats: {
    "name": "Animal-api sync for customer NL_114916",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3531",
        "ok": "3531",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3531",
        "ok": "3531",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3531",
        "ok": "3531",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3531",
        "ok": "3531",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3531",
        "ok": "3531",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3531",
        "ok": "3531",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3531",
        "ok": "3531",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-3eaed": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_108223",
path: "Animal-api sync for customer NL_108223",
pathFormatted: "req_animal-api-sync-3eaed",
stats: {
    "name": "Animal-api sync for customer NL_108223",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4042",
        "ok": "4042",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4042",
        "ok": "4042",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4042",
        "ok": "4042",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4042",
        "ok": "4042",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4042",
        "ok": "4042",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4042",
        "ok": "4042",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4042",
        "ok": "4042",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-6ad60": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_124372",
path: "Animal-api sync for customer NL_124372",
pathFormatted: "req_animal-api-sync-6ad60",
stats: {
    "name": "Animal-api sync for customer NL_124372",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2824",
        "ok": "2824",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2824",
        "ok": "2824",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2824",
        "ok": "2824",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2824",
        "ok": "2824",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2824",
        "ok": "2824",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2824",
        "ok": "2824",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2824",
        "ok": "2824",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-967fa": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_113311",
path: "Animal-api sync for customer NL_113311",
pathFormatted: "req_animal-api-sync-967fa",
stats: {
    "name": "Animal-api sync for customer NL_113311",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2862",
        "ok": "2862",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2862",
        "ok": "2862",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2862",
        "ok": "2862",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2862",
        "ok": "2862",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2862",
        "ok": "2862",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2862",
        "ok": "2862",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2862",
        "ok": "2862",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-3736b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_114775",
path: "Animal-api sync for customer NL_114775",
pathFormatted: "req_animal-api-sync-3736b",
stats: {
    "name": "Animal-api sync for customer NL_114775",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "12262",
        "ok": "12262",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "12262",
        "ok": "12262",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "12262",
        "ok": "12262",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "12262",
        "ok": "12262",
        "ko": "-"
    },
    "percentiles2": {
        "total": "12262",
        "ok": "12262",
        "ko": "-"
    },
    "percentiles3": {
        "total": "12262",
        "ok": "12262",
        "ko": "-"
    },
    "percentiles4": {
        "total": "12262",
        "ok": "12262",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-2e3f2": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_124578",
path: "Animal-api sync for customer NL_124578",
pathFormatted: "req_animal-api-sync-2e3f2",
stats: {
    "name": "Animal-api sync for customer NL_124578",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2713",
        "ok": "2713",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2713",
        "ok": "2713",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2713",
        "ok": "2713",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2713",
        "ok": "2713",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2713",
        "ok": "2713",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2713",
        "ok": "2713",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2713",
        "ok": "2713",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-f6bd1": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_118770",
path: "Animal-api sync for customer NL_118770",
pathFormatted: "req_animal-api-sync-f6bd1",
stats: {
    "name": "Animal-api sync for customer NL_118770",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3086",
        "ok": "3086",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3086",
        "ok": "3086",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3086",
        "ok": "3086",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3086",
        "ok": "3086",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3086",
        "ok": "3086",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3086",
        "ok": "3086",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3086",
        "ok": "3086",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-cfdfb": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111312",
path: "Animal-api sync for customer NL_111312",
pathFormatted: "req_animal-api-sync-cfdfb",
stats: {
    "name": "Animal-api sync for customer NL_111312",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3248",
        "ok": "3248",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3248",
        "ok": "3248",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3248",
        "ok": "3248",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3248",
        "ok": "3248",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3248",
        "ok": "3248",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3248",
        "ok": "3248",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3248",
        "ok": "3248",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c9778": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_119273",
path: "Animal-api sync for customer NL_119273",
pathFormatted: "req_animal-api-sync-c9778",
stats: {
    "name": "Animal-api sync for customer NL_119273",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4896",
        "ok": "4896",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4896",
        "ok": "4896",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4896",
        "ok": "4896",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4896",
        "ok": "4896",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4896",
        "ok": "4896",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4896",
        "ok": "4896",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4896",
        "ok": "4896",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-39d31": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_118474",
path: "Animal-api sync for customer NL_118474",
pathFormatted: "req_animal-api-sync-39d31",
stats: {
    "name": "Animal-api sync for customer NL_118474",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3838",
        "ok": "3838",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3838",
        "ok": "3838",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3838",
        "ok": "3838",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3838",
        "ok": "3838",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3838",
        "ok": "3838",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3838",
        "ok": "3838",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3838",
        "ok": "3838",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-53fa3": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_108741",
path: "Animal-api sync for customer NL_108741",
pathFormatted: "req_animal-api-sync-53fa3",
stats: {
    "name": "Animal-api sync for customer NL_108741",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4676",
        "ok": "4676",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4676",
        "ok": "4676",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4676",
        "ok": "4676",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4676",
        "ok": "4676",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4676",
        "ok": "4676",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4676",
        "ok": "4676",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4676",
        "ok": "4676",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-99401": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111178",
path: "Animal-api sync for customer NL_111178",
pathFormatted: "req_animal-api-sync-99401",
stats: {
    "name": "Animal-api sync for customer NL_111178",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3897",
        "ok": "3897",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3897",
        "ok": "3897",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3897",
        "ok": "3897",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3897",
        "ok": "3897",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3897",
        "ok": "3897",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3897",
        "ok": "3897",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3897",
        "ok": "3897",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-09c2e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_135864",
path: "Animal-api sync for customer NL_135864",
pathFormatted: "req_animal-api-sync-09c2e",
stats: {
    "name": "Animal-api sync for customer NL_135864",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4256",
        "ok": "4256",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4256",
        "ok": "4256",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4256",
        "ok": "4256",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4256",
        "ok": "4256",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4256",
        "ok": "4256",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4256",
        "ok": "4256",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4256",
        "ok": "4256",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-236d8": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_124049",
path: "Animal-api sync for customer NL_124049",
pathFormatted: "req_animal-api-sync-236d8",
stats: {
    "name": "Animal-api sync for customer NL_124049",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3255",
        "ok": "3255",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3255",
        "ok": "3255",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3255",
        "ok": "3255",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3255",
        "ok": "3255",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3255",
        "ok": "3255",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3255",
        "ok": "3255",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3255",
        "ok": "3255",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-882c2": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_128097",
path: "Animal-api sync for customer NL_128097",
pathFormatted: "req_animal-api-sync-882c2",
stats: {
    "name": "Animal-api sync for customer NL_128097",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3066",
        "ok": "3066",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3066",
        "ok": "3066",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3066",
        "ok": "3066",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3066",
        "ok": "3066",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3066",
        "ok": "3066",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3066",
        "ok": "3066",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3066",
        "ok": "3066",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e7614": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_159766",
path: "Animal-api sync for customer BE_159766",
pathFormatted: "req_animal-api-sync-e7614",
stats: {
    "name": "Animal-api sync for customer BE_159766",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "7055",
        "ok": "7055",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "7055",
        "ok": "7055",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "7055",
        "ok": "7055",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "7055",
        "ok": "7055",
        "ko": "-"
    },
    "percentiles2": {
        "total": "7055",
        "ok": "7055",
        "ko": "-"
    },
    "percentiles3": {
        "total": "7055",
        "ok": "7055",
        "ko": "-"
    },
    "percentiles4": {
        "total": "7055",
        "ok": "7055",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-52a69": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_212921",
path: "Animal-api sync for customer BE_212921",
pathFormatted: "req_animal-api-sync-52a69",
stats: {
    "name": "Animal-api sync for customer BE_212921",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5663",
        "ok": "5663",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5663",
        "ok": "5663",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5663",
        "ok": "5663",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5663",
        "ok": "5663",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5663",
        "ok": "5663",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5663",
        "ok": "5663",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5663",
        "ok": "5663",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-53e2f": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_127894",
path: "Animal-api sync for customer NL_127894",
pathFormatted: "req_animal-api-sync-53e2f",
stats: {
    "name": "Animal-api sync for customer NL_127894",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5341",
        "ok": "5341",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5341",
        "ok": "5341",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5341",
        "ok": "5341",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5341",
        "ok": "5341",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5341",
        "ok": "5341",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5341",
        "ok": "5341",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5341",
        "ok": "5341",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b4a99": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104247",
path: "Animal-api sync for customer NL_104247",
pathFormatted: "req_animal-api-sync-b4a99",
stats: {
    "name": "Animal-api sync for customer NL_104247",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5086",
        "ok": "5086",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5086",
        "ok": "5086",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5086",
        "ok": "5086",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5086",
        "ok": "5086",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5086",
        "ok": "5086",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5086",
        "ok": "5086",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5086",
        "ok": "5086",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c4c5c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_122906",
path: "Animal-api sync for customer NL_122906",
pathFormatted: "req_animal-api-sync-c4c5c",
stats: {
    "name": "Animal-api sync for customer NL_122906",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4834",
        "ok": "4834",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4834",
        "ok": "4834",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4834",
        "ok": "4834",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4834",
        "ok": "4834",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4834",
        "ok": "4834",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4834",
        "ok": "4834",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4834",
        "ok": "4834",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e42dd": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_108296",
path: "Animal-api sync for customer NL_108296",
pathFormatted: "req_animal-api-sync-e42dd",
stats: {
    "name": "Animal-api sync for customer NL_108296",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2840",
        "ok": "2840",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2840",
        "ok": "2840",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2840",
        "ok": "2840",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2840",
        "ok": "2840",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2840",
        "ok": "2840",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2840",
        "ok": "2840",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2840",
        "ok": "2840",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b8f6b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_120259",
path: "Animal-api sync for customer NL_120259",
pathFormatted: "req_animal-api-sync-b8f6b",
stats: {
    "name": "Animal-api sync for customer NL_120259",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5791",
        "ok": "5791",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5791",
        "ok": "5791",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5791",
        "ok": "5791",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5791",
        "ok": "5791",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5791",
        "ok": "5791",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5791",
        "ok": "5791",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5791",
        "ok": "5791",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-10dc5": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_113215",
path: "Animal-api sync for customer NL_113215",
pathFormatted: "req_animal-api-sync-10dc5",
stats: {
    "name": "Animal-api sync for customer NL_113215",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4012",
        "ok": "4012",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4012",
        "ok": "4012",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4012",
        "ok": "4012",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4012",
        "ok": "4012",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4012",
        "ok": "4012",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4012",
        "ok": "4012",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4012",
        "ok": "4012",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-ee676": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_119868",
path: "Animal-api sync for customer NL_119868",
pathFormatted: "req_animal-api-sync-ee676",
stats: {
    "name": "Animal-api sync for customer NL_119868",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "7406",
        "ok": "7406",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "7406",
        "ok": "7406",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "7406",
        "ok": "7406",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "7406",
        "ok": "7406",
        "ko": "-"
    },
    "percentiles2": {
        "total": "7406",
        "ok": "7406",
        "ko": "-"
    },
    "percentiles3": {
        "total": "7406",
        "ok": "7406",
        "ko": "-"
    },
    "percentiles4": {
        "total": "7406",
        "ok": "7406",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-12c8b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_107507",
path: "Animal-api sync for customer NL_107507",
pathFormatted: "req_animal-api-sync-12c8b",
stats: {
    "name": "Animal-api sync for customer NL_107507",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3867",
        "ok": "3867",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3867",
        "ok": "3867",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3867",
        "ok": "3867",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3867",
        "ok": "3867",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3867",
        "ok": "3867",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3867",
        "ok": "3867",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3867",
        "ok": "3867",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-70c3b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_108064",
path: "Animal-api sync for customer NL_108064",
pathFormatted: "req_animal-api-sync-70c3b",
stats: {
    "name": "Animal-api sync for customer NL_108064",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4916",
        "ok": "4916",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4916",
        "ok": "4916",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4916",
        "ok": "4916",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4916",
        "ok": "4916",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4916",
        "ok": "4916",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4916",
        "ok": "4916",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4916",
        "ok": "4916",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d5bee": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_114423",
path: "Animal-api sync for customer NL_114423",
pathFormatted: "req_animal-api-sync-d5bee",
stats: {
    "name": "Animal-api sync for customer NL_114423",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2404",
        "ok": "2404",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2404",
        "ok": "2404",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2404",
        "ok": "2404",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2404",
        "ok": "2404",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2404",
        "ok": "2404",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2404",
        "ok": "2404",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2404",
        "ok": "2404",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-f94cb": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_107241",
path: "Animal-api sync for customer NL_107241",
pathFormatted: "req_animal-api-sync-f94cb",
stats: {
    "name": "Animal-api sync for customer NL_107241",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5520",
        "ok": "5520",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5520",
        "ok": "5520",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5520",
        "ok": "5520",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5520",
        "ok": "5520",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5520",
        "ok": "5520",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5520",
        "ok": "5520",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5520",
        "ok": "5520",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-4b1f5": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_129590",
path: "Animal-api sync for customer NL_129590",
pathFormatted: "req_animal-api-sync-4b1f5",
stats: {
    "name": "Animal-api sync for customer NL_129590",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4700",
        "ok": "4700",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4700",
        "ok": "4700",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4700",
        "ok": "4700",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4700",
        "ok": "4700",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4700",
        "ok": "4700",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4700",
        "ok": "4700",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4700",
        "ok": "4700",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-25bc2": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111740",
path: "Animal-api sync for customer NL_111740",
pathFormatted: "req_animal-api-sync-25bc2",
stats: {
    "name": "Animal-api sync for customer NL_111740",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4578",
        "ok": "4578",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4578",
        "ok": "4578",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4578",
        "ok": "4578",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4578",
        "ok": "4578",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4578",
        "ok": "4578",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4578",
        "ok": "4578",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4578",
        "ok": "4578",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-2efa4": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117125",
path: "Animal-api sync for customer NL_117125",
pathFormatted: "req_animal-api-sync-2efa4",
stats: {
    "name": "Animal-api sync for customer NL_117125",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4638",
        "ok": "4638",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4638",
        "ok": "4638",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4638",
        "ok": "4638",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4638",
        "ok": "4638",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4638",
        "ok": "4638",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4638",
        "ok": "4638",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4638",
        "ok": "4638",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c4fc3": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_216527",
path: "Animal-api sync for customer NL_216527",
pathFormatted: "req_animal-api-sync-c4fc3",
stats: {
    "name": "Animal-api sync for customer NL_216527",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4578",
        "ok": "4578",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4578",
        "ok": "4578",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4578",
        "ok": "4578",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4578",
        "ok": "4578",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4578",
        "ok": "4578",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4578",
        "ok": "4578",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4578",
        "ok": "4578",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-41435": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_119693",
path: "Animal-api sync for customer NL_119693",
pathFormatted: "req_animal-api-sync-41435",
stats: {
    "name": "Animal-api sync for customer NL_119693",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3947",
        "ok": "3947",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3947",
        "ok": "3947",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3947",
        "ok": "3947",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3947",
        "ok": "3947",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3947",
        "ok": "3947",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3947",
        "ok": "3947",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3947",
        "ok": "3947",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e3e3d": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_126029",
path: "Animal-api sync for customer NL_126029",
pathFormatted: "req_animal-api-sync-e3e3d",
stats: {
    "name": "Animal-api sync for customer NL_126029",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4477",
        "ok": "4477",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4477",
        "ok": "4477",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4477",
        "ok": "4477",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4477",
        "ok": "4477",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4477",
        "ok": "4477",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4477",
        "ok": "4477",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4477",
        "ok": "4477",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d8bef": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_114961",
path: "Animal-api sync for customer NL_114961",
pathFormatted: "req_animal-api-sync-d8bef",
stats: {
    "name": "Animal-api sync for customer NL_114961",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4155",
        "ok": "4155",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4155",
        "ok": "4155",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4155",
        "ok": "4155",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4155",
        "ok": "4155",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4155",
        "ok": "4155",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4155",
        "ok": "4155",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4155",
        "ok": "4155",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-40763": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_127944",
path: "Animal-api sync for customer NL_127944",
pathFormatted: "req_animal-api-sync-40763",
stats: {
    "name": "Animal-api sync for customer NL_127944",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4405",
        "ok": "4405",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4405",
        "ok": "4405",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4405",
        "ok": "4405",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4405",
        "ok": "4405",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4405",
        "ok": "4405",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4405",
        "ok": "4405",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4405",
        "ok": "4405",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-9f4a5": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_108715",
path: "Animal-api sync for customer NL_108715",
pathFormatted: "req_animal-api-sync-9f4a5",
stats: {
    "name": "Animal-api sync for customer NL_108715",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2936",
        "ok": "2936",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2936",
        "ok": "2936",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2936",
        "ok": "2936",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2936",
        "ok": "2936",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2936",
        "ok": "2936",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2936",
        "ok": "2936",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2936",
        "ok": "2936",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-0355c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_133793",
path: "Animal-api sync for customer NL_133793",
pathFormatted: "req_animal-api-sync-0355c",
stats: {
    "name": "Animal-api sync for customer NL_133793",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4194",
        "ok": "4194",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4194",
        "ok": "4194",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4194",
        "ok": "4194",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4194",
        "ok": "4194",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4194",
        "ok": "4194",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4194",
        "ok": "4194",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4194",
        "ok": "4194",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-6bc35": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_127470",
path: "Animal-api sync for customer NL_127470",
pathFormatted: "req_animal-api-sync-6bc35",
stats: {
    "name": "Animal-api sync for customer NL_127470",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3506",
        "ok": "3506",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3506",
        "ok": "3506",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3506",
        "ok": "3506",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3506",
        "ok": "3506",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3506",
        "ok": "3506",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3506",
        "ok": "3506",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3506",
        "ok": "3506",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-7eb9d": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104934",
path: "Animal-api sync for customer NL_104934",
pathFormatted: "req_animal-api-sync-7eb9d",
stats: {
    "name": "Animal-api sync for customer NL_104934",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "7639",
        "ok": "7639",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "7639",
        "ok": "7639",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "7639",
        "ok": "7639",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "7639",
        "ok": "7639",
        "ko": "-"
    },
    "percentiles2": {
        "total": "7639",
        "ok": "7639",
        "ko": "-"
    },
    "percentiles3": {
        "total": "7639",
        "ok": "7639",
        "ko": "-"
    },
    "percentiles4": {
        "total": "7639",
        "ok": "7639",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-3a032": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_122396",
path: "Animal-api sync for customer NL_122396",
pathFormatted: "req_animal-api-sync-3a032",
stats: {
    "name": "Animal-api sync for customer NL_122396",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2751",
        "ok": "2751",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2751",
        "ok": "2751",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2751",
        "ok": "2751",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2751",
        "ok": "2751",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2751",
        "ok": "2751",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2751",
        "ok": "2751",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2751",
        "ok": "2751",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-1cf92": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105173",
path: "Animal-api sync for customer NL_105173",
pathFormatted: "req_animal-api-sync-1cf92",
stats: {
    "name": "Animal-api sync for customer NL_105173",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3790",
        "ok": "3790",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3790",
        "ok": "3790",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3790",
        "ok": "3790",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3790",
        "ok": "3790",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3790",
        "ok": "3790",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3790",
        "ok": "3790",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3790",
        "ok": "3790",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b22b6": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_131590",
path: "Animal-api sync for customer NL_131590",
pathFormatted: "req_animal-api-sync-b22b6",
stats: {
    "name": "Animal-api sync for customer NL_131590",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3538",
        "ok": "3538",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3538",
        "ok": "3538",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3538",
        "ok": "3538",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3538",
        "ok": "3538",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3538",
        "ok": "3538",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3538",
        "ok": "3538",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3538",
        "ok": "3538",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-1080b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110702",
path: "Animal-api sync for customer NL_110702",
pathFormatted: "req_animal-api-sync-1080b",
stats: {
    "name": "Animal-api sync for customer NL_110702",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2621",
        "ok": "2621",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2621",
        "ok": "2621",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2621",
        "ok": "2621",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2621",
        "ok": "2621",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2621",
        "ok": "2621",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2621",
        "ok": "2621",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2621",
        "ok": "2621",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-8580c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_119732",
path: "Animal-api sync for customer NL_119732",
pathFormatted: "req_animal-api-sync-8580c",
stats: {
    "name": "Animal-api sync for customer NL_119732",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3118",
        "ok": "3118",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3118",
        "ok": "3118",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3118",
        "ok": "3118",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3118",
        "ok": "3118",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3118",
        "ok": "3118",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3118",
        "ok": "3118",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3118",
        "ok": "3118",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-8dfa9": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_116373",
path: "Animal-api sync for customer NL_116373",
pathFormatted: "req_animal-api-sync-8dfa9",
stats: {
    "name": "Animal-api sync for customer NL_116373",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2984",
        "ok": "2984",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2984",
        "ok": "2984",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2984",
        "ok": "2984",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2984",
        "ok": "2984",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2984",
        "ok": "2984",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2984",
        "ok": "2984",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2984",
        "ok": "2984",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c298e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_124623",
path: "Animal-api sync for customer NL_124623",
pathFormatted: "req_animal-api-sync-c298e",
stats: {
    "name": "Animal-api sync for customer NL_124623",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2168",
        "ok": "2168",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2168",
        "ok": "2168",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2168",
        "ok": "2168",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2168",
        "ok": "2168",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2168",
        "ok": "2168",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2168",
        "ok": "2168",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2168",
        "ok": "2168",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-18c50": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_124684",
path: "Animal-api sync for customer NL_124684",
pathFormatted: "req_animal-api-sync-18c50",
stats: {
    "name": "Animal-api sync for customer NL_124684",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1951",
        "ok": "1951",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1951",
        "ok": "1951",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1951",
        "ok": "1951",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1951",
        "ok": "1951",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1951",
        "ok": "1951",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1951",
        "ok": "1951",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1951",
        "ok": "1951",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-11d7b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111681",
path: "Animal-api sync for customer NL_111681",
pathFormatted: "req_animal-api-sync-11d7b",
stats: {
    "name": "Animal-api sync for customer NL_111681",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3625",
        "ok": "3625",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3625",
        "ok": "3625",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3625",
        "ok": "3625",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3625",
        "ok": "3625",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3625",
        "ok": "3625",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3625",
        "ok": "3625",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3625",
        "ok": "3625",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-f0b42": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_107121",
path: "Animal-api sync for customer NL_107121",
pathFormatted: "req_animal-api-sync-f0b42",
stats: {
    "name": "Animal-api sync for customer NL_107121",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4467",
        "ok": "4467",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4467",
        "ok": "4467",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4467",
        "ok": "4467",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4467",
        "ok": "4467",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4467",
        "ok": "4467",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4467",
        "ok": "4467",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4467",
        "ok": "4467",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-de92c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_109851",
path: "Animal-api sync for customer NL_109851",
pathFormatted: "req_animal-api-sync-de92c",
stats: {
    "name": "Animal-api sync for customer NL_109851",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "6004",
        "ok": "6004",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "6004",
        "ok": "6004",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "6004",
        "ok": "6004",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "6004",
        "ok": "6004",
        "ko": "-"
    },
    "percentiles2": {
        "total": "6004",
        "ok": "6004",
        "ko": "-"
    },
    "percentiles3": {
        "total": "6004",
        "ok": "6004",
        "ko": "-"
    },
    "percentiles4": {
        "total": "6004",
        "ok": "6004",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-31610": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_109147",
path: "Animal-api sync for customer NL_109147",
pathFormatted: "req_animal-api-sync-31610",
stats: {
    "name": "Animal-api sync for customer NL_109147",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1935",
        "ok": "1935",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1935",
        "ok": "1935",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1935",
        "ok": "1935",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1935",
        "ok": "1935",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1935",
        "ok": "1935",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1935",
        "ok": "1935",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1935",
        "ok": "1935",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a6474": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_217194",
path: "Animal-api sync for customer NL_217194",
pathFormatted: "req_animal-api-sync-a6474",
stats: {
    "name": "Animal-api sync for customer NL_217194",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4533",
        "ok": "4533",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4533",
        "ok": "4533",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4533",
        "ok": "4533",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4533",
        "ok": "4533",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4533",
        "ok": "4533",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4533",
        "ok": "4533",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4533",
        "ok": "4533",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-735e5": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_162624",
path: "Animal-api sync for customer BE_162624",
pathFormatted: "req_animal-api-sync-735e5",
stats: {
    "name": "Animal-api sync for customer BE_162624",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5055",
        "ok": "5055",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5055",
        "ok": "5055",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5055",
        "ok": "5055",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5055",
        "ok": "5055",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5055",
        "ok": "5055",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5055",
        "ok": "5055",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5055",
        "ok": "5055",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-da049": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111349",
path: "Animal-api sync for customer NL_111349",
pathFormatted: "req_animal-api-sync-da049",
stats: {
    "name": "Animal-api sync for customer NL_111349",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4158",
        "ok": "4158",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4158",
        "ok": "4158",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4158",
        "ok": "4158",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4158",
        "ok": "4158",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4158",
        "ok": "4158",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4158",
        "ok": "4158",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4158",
        "ok": "4158",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-0945e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_120830",
path: "Animal-api sync for customer NL_120830",
pathFormatted: "req_animal-api-sync-0945e",
stats: {
    "name": "Animal-api sync for customer NL_120830",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4250",
        "ok": "4250",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4250",
        "ok": "4250",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4250",
        "ok": "4250",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4250",
        "ok": "4250",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4250",
        "ok": "4250",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4250",
        "ok": "4250",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4250",
        "ok": "4250",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d1db6": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_154426",
path: "Animal-api sync for customer BE_154426",
pathFormatted: "req_animal-api-sync-d1db6",
stats: {
    "name": "Animal-api sync for customer BE_154426",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "8496",
        "ok": "8496",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "8496",
        "ok": "8496",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "8496",
        "ok": "8496",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "8496",
        "ok": "8496",
        "ko": "-"
    },
    "percentiles2": {
        "total": "8496",
        "ok": "8496",
        "ko": "-"
    },
    "percentiles3": {
        "total": "8496",
        "ok": "8496",
        "ko": "-"
    },
    "percentiles4": {
        "total": "8496",
        "ok": "8496",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-3b46a": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_160734",
path: "Animal-api sync for customer NL_160734",
pathFormatted: "req_animal-api-sync-3b46a",
stats: {
    "name": "Animal-api sync for customer NL_160734",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3167",
        "ok": "3167",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3167",
        "ok": "3167",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3167",
        "ok": "3167",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3167",
        "ok": "3167",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3167",
        "ok": "3167",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3167",
        "ok": "3167",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3167",
        "ok": "3167",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-4b701": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_122804",
path: "Animal-api sync for customer NL_122804",
pathFormatted: "req_animal-api-sync-4b701",
stats: {
    "name": "Animal-api sync for customer NL_122804",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4054",
        "ok": "4054",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4054",
        "ok": "4054",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4054",
        "ok": "4054",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4054",
        "ok": "4054",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4054",
        "ok": "4054",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4054",
        "ok": "4054",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4054",
        "ok": "4054",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-3f46c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110510",
path: "Animal-api sync for customer NL_110510",
pathFormatted: "req_animal-api-sync-3f46c",
stats: {
    "name": "Animal-api sync for customer NL_110510",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2767",
        "ok": "2767",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2767",
        "ok": "2767",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2767",
        "ok": "2767",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2767",
        "ok": "2767",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2767",
        "ok": "2767",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2767",
        "ok": "2767",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2767",
        "ok": "2767",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e1650": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_189253",
path: "Animal-api sync for customer BE_189253",
pathFormatted: "req_animal-api-sync-e1650",
stats: {
    "name": "Animal-api sync for customer BE_189253",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5807",
        "ok": "5807",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5807",
        "ok": "5807",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5807",
        "ok": "5807",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5807",
        "ok": "5807",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5807",
        "ok": "5807",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5807",
        "ok": "5807",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5807",
        "ok": "5807",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-5a990": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_196831",
path: "Animal-api sync for customer BE_196831",
pathFormatted: "req_animal-api-sync-5a990",
stats: {
    "name": "Animal-api sync for customer BE_196831",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "6103",
        "ok": "6103",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "6103",
        "ok": "6103",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "6103",
        "ok": "6103",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "6103",
        "ok": "6103",
        "ko": "-"
    },
    "percentiles2": {
        "total": "6103",
        "ok": "6103",
        "ko": "-"
    },
    "percentiles3": {
        "total": "6103",
        "ok": "6103",
        "ko": "-"
    },
    "percentiles4": {
        "total": "6103",
        "ok": "6103",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-8b170": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_128376",
path: "Animal-api sync for customer NL_128376",
pathFormatted: "req_animal-api-sync-8b170",
stats: {
    "name": "Animal-api sync for customer NL_128376",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5141",
        "ok": "5141",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5141",
        "ok": "5141",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5141",
        "ok": "5141",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5141",
        "ok": "5141",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5141",
        "ok": "5141",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5141",
        "ok": "5141",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5141",
        "ok": "5141",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-9e2d7": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_127839",
path: "Animal-api sync for customer NL_127839",
pathFormatted: "req_animal-api-sync-9e2d7",
stats: {
    "name": "Animal-api sync for customer NL_127839",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4909",
        "ok": "4909",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4909",
        "ok": "4909",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4909",
        "ok": "4909",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4909",
        "ok": "4909",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4909",
        "ok": "4909",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4909",
        "ok": "4909",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4909",
        "ok": "4909",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-cb3f0": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_109354",
path: "Animal-api sync for customer NL_109354",
pathFormatted: "req_animal-api-sync-cb3f0",
stats: {
    "name": "Animal-api sync for customer NL_109354",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3572",
        "ok": "3572",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3572",
        "ok": "3572",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3572",
        "ok": "3572",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3572",
        "ok": "3572",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3572",
        "ok": "3572",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3572",
        "ok": "3572",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3572",
        "ok": "3572",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-77e7f": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_116276",
path: "Animal-api sync for customer NL_116276",
pathFormatted: "req_animal-api-sync-77e7f",
stats: {
    "name": "Animal-api sync for customer NL_116276",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3924",
        "ok": "3924",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3924",
        "ok": "3924",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3924",
        "ok": "3924",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3924",
        "ok": "3924",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3924",
        "ok": "3924",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3924",
        "ok": "3924",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3924",
        "ok": "3924",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e91bf": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111232",
path: "Animal-api sync for customer NL_111232",
pathFormatted: "req_animal-api-sync-e91bf",
stats: {
    "name": "Animal-api sync for customer NL_111232",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3102",
        "ok": "3102",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3102",
        "ok": "3102",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3102",
        "ok": "3102",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3102",
        "ok": "3102",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3102",
        "ok": "3102",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3102",
        "ok": "3102",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3102",
        "ok": "3102",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-df608": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_106231",
path: "Animal-api sync for customer NL_106231",
pathFormatted: "req_animal-api-sync-df608",
stats: {
    "name": "Animal-api sync for customer NL_106231",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "6293",
        "ok": "6293",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "6293",
        "ok": "6293",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "6293",
        "ok": "6293",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "6293",
        "ok": "6293",
        "ko": "-"
    },
    "percentiles2": {
        "total": "6293",
        "ok": "6293",
        "ko": "-"
    },
    "percentiles3": {
        "total": "6293",
        "ok": "6293",
        "ko": "-"
    },
    "percentiles4": {
        "total": "6293",
        "ok": "6293",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-96235": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_119086",
path: "Animal-api sync for customer NL_119086",
pathFormatted: "req_animal-api-sync-96235",
stats: {
    "name": "Animal-api sync for customer NL_119086",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "6042",
        "ok": "6042",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "6042",
        "ok": "6042",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "6042",
        "ok": "6042",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "6042",
        "ok": "6042",
        "ko": "-"
    },
    "percentiles2": {
        "total": "6042",
        "ok": "6042",
        "ko": "-"
    },
    "percentiles3": {
        "total": "6042",
        "ok": "6042",
        "ko": "-"
    },
    "percentiles4": {
        "total": "6042",
        "ok": "6042",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b66f8": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110851",
path: "Animal-api sync for customer NL_110851",
pathFormatted: "req_animal-api-sync-b66f8",
stats: {
    "name": "Animal-api sync for customer NL_110851",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5934",
        "ok": "5934",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5934",
        "ok": "5934",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5934",
        "ok": "5934",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5934",
        "ok": "5934",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5934",
        "ok": "5934",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5934",
        "ok": "5934",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5934",
        "ok": "5934",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-fc706": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_108712",
path: "Animal-api sync for customer NL_108712",
pathFormatted: "req_animal-api-sync-fc706",
stats: {
    "name": "Animal-api sync for customer NL_108712",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4187",
        "ok": "4187",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4187",
        "ok": "4187",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4187",
        "ok": "4187",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4187",
        "ok": "4187",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4187",
        "ok": "4187",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4187",
        "ok": "4187",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4187",
        "ok": "4187",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-77463": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_142162",
path: "Animal-api sync for customer NL_142162",
pathFormatted: "req_animal-api-sync-77463",
stats: {
    "name": "Animal-api sync for customer NL_142162",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "6082",
        "ok": "6082",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "6082",
        "ok": "6082",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "6082",
        "ok": "6082",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "6082",
        "ok": "6082",
        "ko": "-"
    },
    "percentiles2": {
        "total": "6082",
        "ok": "6082",
        "ko": "-"
    },
    "percentiles3": {
        "total": "6082",
        "ok": "6082",
        "ko": "-"
    },
    "percentiles4": {
        "total": "6082",
        "ok": "6082",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-3e0f1": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_119459",
path: "Animal-api sync for customer NL_119459",
pathFormatted: "req_animal-api-sync-3e0f1",
stats: {
    "name": "Animal-api sync for customer NL_119459",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3780",
        "ok": "3780",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3780",
        "ok": "3780",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3780",
        "ok": "3780",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3780",
        "ok": "3780",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3780",
        "ok": "3780",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3780",
        "ok": "3780",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3780",
        "ok": "3780",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-f9107": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_121807",
path: "Animal-api sync for customer NL_121807",
pathFormatted: "req_animal-api-sync-f9107",
stats: {
    "name": "Animal-api sync for customer NL_121807",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4577",
        "ok": "4577",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4577",
        "ok": "4577",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4577",
        "ok": "4577",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4577",
        "ok": "4577",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4577",
        "ok": "4577",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4577",
        "ok": "4577",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4577",
        "ok": "4577",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-82f9e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110685",
path: "Animal-api sync for customer NL_110685",
pathFormatted: "req_animal-api-sync-82f9e",
stats: {
    "name": "Animal-api sync for customer NL_110685",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4147",
        "ok": "4147",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4147",
        "ok": "4147",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4147",
        "ok": "4147",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4147",
        "ok": "4147",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4147",
        "ok": "4147",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4147",
        "ok": "4147",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4147",
        "ok": "4147",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-39816": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_145536",
path: "Animal-api sync for customer BE_145536",
pathFormatted: "req_animal-api-sync-39816",
stats: {
    "name": "Animal-api sync for customer BE_145536",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "6040",
        "ok": "6040",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "6040",
        "ok": "6040",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "6040",
        "ok": "6040",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "6040",
        "ok": "6040",
        "ko": "-"
    },
    "percentiles2": {
        "total": "6040",
        "ok": "6040",
        "ko": "-"
    },
    "percentiles3": {
        "total": "6040",
        "ok": "6040",
        "ko": "-"
    },
    "percentiles4": {
        "total": "6040",
        "ok": "6040",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-66198": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_107001",
path: "Animal-api sync for customer NL_107001",
pathFormatted: "req_animal-api-sync-66198",
stats: {
    "name": "Animal-api sync for customer NL_107001",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3708",
        "ok": "3708",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3708",
        "ok": "3708",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3708",
        "ok": "3708",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3708",
        "ok": "3708",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3708",
        "ok": "3708",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3708",
        "ok": "3708",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3708",
        "ok": "3708",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-299ce": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_119621",
path: "Animal-api sync for customer NL_119621",
pathFormatted: "req_animal-api-sync-299ce",
stats: {
    "name": "Animal-api sync for customer NL_119621",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4268",
        "ok": "4268",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4268",
        "ok": "4268",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4268",
        "ok": "4268",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4268",
        "ok": "4268",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4268",
        "ok": "4268",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4268",
        "ok": "4268",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4268",
        "ok": "4268",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-cb563": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_195750",
path: "Animal-api sync for customer BE_195750",
pathFormatted: "req_animal-api-sync-cb563",
stats: {
    "name": "Animal-api sync for customer BE_195750",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3106",
        "ok": "3106",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3106",
        "ok": "3106",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3106",
        "ok": "3106",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3106",
        "ok": "3106",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3106",
        "ok": "3106",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3106",
        "ok": "3106",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3106",
        "ok": "3106",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-86128": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110109",
path: "Animal-api sync for customer NL_110109",
pathFormatted: "req_animal-api-sync-86128",
stats: {
    "name": "Animal-api sync for customer NL_110109",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3824",
        "ok": "3824",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3824",
        "ok": "3824",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3824",
        "ok": "3824",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3824",
        "ok": "3824",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3824",
        "ok": "3824",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3824",
        "ok": "3824",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3824",
        "ok": "3824",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-2a7a0": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_119620",
path: "Animal-api sync for customer NL_119620",
pathFormatted: "req_animal-api-sync-2a7a0",
stats: {
    "name": "Animal-api sync for customer NL_119620",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3835",
        "ok": "3835",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3835",
        "ok": "3835",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3835",
        "ok": "3835",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3835",
        "ok": "3835",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3835",
        "ok": "3835",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3835",
        "ok": "3835",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3835",
        "ok": "3835",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-f1bc5": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_106522",
path: "Animal-api sync for customer NL_106522",
pathFormatted: "req_animal-api-sync-f1bc5",
stats: {
    "name": "Animal-api sync for customer NL_106522",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3787",
        "ok": "3787",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3787",
        "ok": "3787",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3787",
        "ok": "3787",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3787",
        "ok": "3787",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3787",
        "ok": "3787",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3787",
        "ok": "3787",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3787",
        "ok": "3787",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-fa8fb": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110675",
path: "Animal-api sync for customer NL_110675",
pathFormatted: "req_animal-api-sync-fa8fb",
stats: {
    "name": "Animal-api sync for customer NL_110675",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3164",
        "ok": "3164",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3164",
        "ok": "3164",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3164",
        "ok": "3164",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3164",
        "ok": "3164",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3164",
        "ok": "3164",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3164",
        "ok": "3164",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3164",
        "ok": "3164",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-9a989": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105564",
path: "Animal-api sync for customer NL_105564",
pathFormatted: "req_animal-api-sync-9a989",
stats: {
    "name": "Animal-api sync for customer NL_105564",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2834",
        "ok": "2834",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2834",
        "ok": "2834",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2834",
        "ok": "2834",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2834",
        "ok": "2834",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2834",
        "ok": "2834",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2834",
        "ok": "2834",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2834",
        "ok": "2834",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-1d50e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_107593",
path: "Animal-api sync for customer NL_107593",
pathFormatted: "req_animal-api-sync-1d50e",
stats: {
    "name": "Animal-api sync for customer NL_107593",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3472",
        "ok": "3472",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3472",
        "ok": "3472",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3472",
        "ok": "3472",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3472",
        "ok": "3472",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3472",
        "ok": "3472",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3472",
        "ok": "3472",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3472",
        "ok": "3472",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-3fad8": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_119768",
path: "Animal-api sync for customer NL_119768",
pathFormatted: "req_animal-api-sync-3fad8",
stats: {
    "name": "Animal-api sync for customer NL_119768",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2032",
        "ok": "2032",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2032",
        "ok": "2032",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2032",
        "ok": "2032",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2032",
        "ok": "2032",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2032",
        "ok": "2032",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2032",
        "ok": "2032",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2032",
        "ok": "2032",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-1cdda": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_106961",
path: "Animal-api sync for customer NL_106961",
pathFormatted: "req_animal-api-sync-1cdda",
stats: {
    "name": "Animal-api sync for customer NL_106961",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2898",
        "ok": "2898",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2898",
        "ok": "2898",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2898",
        "ok": "2898",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2898",
        "ok": "2898",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2898",
        "ok": "2898",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2898",
        "ok": "2898",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2898",
        "ok": "2898",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-028d0": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_139052",
path: "Animal-api sync for customer NL_139052",
pathFormatted: "req_animal-api-sync-028d0",
stats: {
    "name": "Animal-api sync for customer NL_139052",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1440",
        "ok": "1440",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1440",
        "ok": "1440",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1440",
        "ok": "1440",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1440",
        "ok": "1440",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1440",
        "ok": "1440",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1440",
        "ok": "1440",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1440",
        "ok": "1440",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-3df0e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_136411",
path: "Animal-api sync for customer NL_136411",
pathFormatted: "req_animal-api-sync-3df0e",
stats: {
    "name": "Animal-api sync for customer NL_136411",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "6492",
        "ok": "6492",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "6492",
        "ok": "6492",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "6492",
        "ok": "6492",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "6492",
        "ok": "6492",
        "ko": "-"
    },
    "percentiles2": {
        "total": "6492",
        "ok": "6492",
        "ko": "-"
    },
    "percentiles3": {
        "total": "6492",
        "ok": "6492",
        "ko": "-"
    },
    "percentiles4": {
        "total": "6492",
        "ok": "6492",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-34074": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104611",
path: "Animal-api sync for customer NL_104611",
pathFormatted: "req_animal-api-sync-34074",
stats: {
    "name": "Animal-api sync for customer NL_104611",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "6067",
        "ok": "6067",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "6067",
        "ok": "6067",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "6067",
        "ok": "6067",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "6067",
        "ok": "6067",
        "ko": "-"
    },
    "percentiles2": {
        "total": "6067",
        "ok": "6067",
        "ko": "-"
    },
    "percentiles3": {
        "total": "6067",
        "ok": "6067",
        "ko": "-"
    },
    "percentiles4": {
        "total": "6067",
        "ok": "6067",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-4e4dd": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_132571",
path: "Animal-api sync for customer NL_132571",
pathFormatted: "req_animal-api-sync-4e4dd",
stats: {
    "name": "Animal-api sync for customer NL_132571",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2463",
        "ok": "2463",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2463",
        "ok": "2463",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2463",
        "ok": "2463",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2463",
        "ok": "2463",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2463",
        "ok": "2463",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2463",
        "ok": "2463",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2463",
        "ok": "2463",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b76bc": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_121619",
path: "Animal-api sync for customer NL_121619",
pathFormatted: "req_animal-api-sync-b76bc",
stats: {
    "name": "Animal-api sync for customer NL_121619",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4404",
        "ok": "4404",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4404",
        "ok": "4404",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4404",
        "ok": "4404",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4404",
        "ok": "4404",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4404",
        "ok": "4404",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4404",
        "ok": "4404",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4404",
        "ok": "4404",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-67b51": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_208384",
path: "Animal-api sync for customer NL_208384",
pathFormatted: "req_animal-api-sync-67b51",
stats: {
    "name": "Animal-api sync for customer NL_208384",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4600",
        "ok": "4600",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4600",
        "ok": "4600",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4600",
        "ok": "4600",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4600",
        "ok": "4600",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4600",
        "ok": "4600",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4600",
        "ok": "4600",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4600",
        "ok": "4600",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-f5467": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_164463",
path: "Animal-api sync for customer NL_164463",
pathFormatted: "req_animal-api-sync-f5467",
stats: {
    "name": "Animal-api sync for customer NL_164463",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2819",
        "ok": "2819",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2819",
        "ok": "2819",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2819",
        "ok": "2819",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2819",
        "ok": "2819",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2819",
        "ok": "2819",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2819",
        "ok": "2819",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2819",
        "ok": "2819",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a921a": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_158726",
path: "Animal-api sync for customer BE_158726",
pathFormatted: "req_animal-api-sync-a921a",
stats: {
    "name": "Animal-api sync for customer BE_158726",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3560",
        "ok": "3560",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3560",
        "ok": "3560",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3560",
        "ok": "3560",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3560",
        "ok": "3560",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3560",
        "ok": "3560",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3560",
        "ok": "3560",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3560",
        "ok": "3560",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-43d17": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_159367",
path: "Animal-api sync for customer NL_159367",
pathFormatted: "req_animal-api-sync-43d17",
stats: {
    "name": "Animal-api sync for customer NL_159367",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2115",
        "ok": "2115",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2115",
        "ok": "2115",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2115",
        "ok": "2115",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2115",
        "ok": "2115",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2115",
        "ok": "2115",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2115",
        "ok": "2115",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2115",
        "ok": "2115",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-7b4fd": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_207082",
path: "Animal-api sync for customer BE_207082",
pathFormatted: "req_animal-api-sync-7b4fd",
stats: {
    "name": "Animal-api sync for customer BE_207082",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4225",
        "ok": "4225",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4225",
        "ok": "4225",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4225",
        "ok": "4225",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4225",
        "ok": "4225",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4225",
        "ok": "4225",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4225",
        "ok": "4225",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4225",
        "ok": "4225",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-774bc": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105254",
path: "Animal-api sync for customer NL_105254",
pathFormatted: "req_animal-api-sync-774bc",
stats: {
    "name": "Animal-api sync for customer NL_105254",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2810",
        "ok": "2810",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2810",
        "ok": "2810",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2810",
        "ok": "2810",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2810",
        "ok": "2810",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2810",
        "ok": "2810",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2810",
        "ok": "2810",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2810",
        "ok": "2810",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-72b5a": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_129227",
path: "Animal-api sync for customer NL_129227",
pathFormatted: "req_animal-api-sync-72b5a",
stats: {
    "name": "Animal-api sync for customer NL_129227",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3289",
        "ok": "3289",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3289",
        "ok": "3289",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3289",
        "ok": "3289",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3289",
        "ok": "3289",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3289",
        "ok": "3289",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3289",
        "ok": "3289",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3289",
        "ok": "3289",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e7477": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_120288",
path: "Animal-api sync for customer NL_120288",
pathFormatted: "req_animal-api-sync-e7477",
stats: {
    "name": "Animal-api sync for customer NL_120288",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3038",
        "ok": "3038",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3038",
        "ok": "3038",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3038",
        "ok": "3038",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3038",
        "ok": "3038",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3038",
        "ok": "3038",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3038",
        "ok": "3038",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3038",
        "ok": "3038",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b6fe1": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_112227",
path: "Animal-api sync for customer NL_112227",
pathFormatted: "req_animal-api-sync-b6fe1",
stats: {
    "name": "Animal-api sync for customer NL_112227",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3202",
        "ok": "3202",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3202",
        "ok": "3202",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3202",
        "ok": "3202",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3202",
        "ok": "3202",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3202",
        "ok": "3202",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3202",
        "ok": "3202",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3202",
        "ok": "3202",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-6891c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111812",
path: "Animal-api sync for customer NL_111812",
pathFormatted: "req_animal-api-sync-6891c",
stats: {
    "name": "Animal-api sync for customer NL_111812",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3221",
        "ok": "3221",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3221",
        "ok": "3221",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3221",
        "ok": "3221",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3221",
        "ok": "3221",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3221",
        "ok": "3221",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3221",
        "ok": "3221",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3221",
        "ok": "3221",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-5324e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_112685",
path: "Animal-api sync for customer NL_112685",
pathFormatted: "req_animal-api-sync-5324e",
stats: {
    "name": "Animal-api sync for customer NL_112685",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3690",
        "ok": "3690",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3690",
        "ok": "3690",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3690",
        "ok": "3690",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3690",
        "ok": "3690",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3690",
        "ok": "3690",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3690",
        "ok": "3690",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3690",
        "ok": "3690",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-39487": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_153606",
path: "Animal-api sync for customer BE_153606",
pathFormatted: "req_animal-api-sync-39487",
stats: {
    "name": "Animal-api sync for customer BE_153606",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3826",
        "ok": "3826",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3826",
        "ok": "3826",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3826",
        "ok": "3826",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3826",
        "ok": "3826",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3826",
        "ok": "3826",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3826",
        "ok": "3826",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3826",
        "ok": "3826",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-115a1": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_106548",
path: "Animal-api sync for customer NL_106548",
pathFormatted: "req_animal-api-sync-115a1",
stats: {
    "name": "Animal-api sync for customer NL_106548",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3496",
        "ok": "3496",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3496",
        "ok": "3496",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3496",
        "ok": "3496",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3496",
        "ok": "3496",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3496",
        "ok": "3496",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3496",
        "ok": "3496",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3496",
        "ok": "3496",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-dfd7c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_139331",
path: "Animal-api sync for customer NL_139331",
pathFormatted: "req_animal-api-sync-dfd7c",
stats: {
    "name": "Animal-api sync for customer NL_139331",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3591",
        "ok": "3591",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3591",
        "ok": "3591",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3591",
        "ok": "3591",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3591",
        "ok": "3591",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3591",
        "ok": "3591",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3591",
        "ok": "3591",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3591",
        "ok": "3591",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c6932": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_133968",
path: "Animal-api sync for customer NL_133968",
pathFormatted: "req_animal-api-sync-c6932",
stats: {
    "name": "Animal-api sync for customer NL_133968",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2806",
        "ok": "2806",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2806",
        "ok": "2806",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2806",
        "ok": "2806",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2806",
        "ok": "2806",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2806",
        "ok": "2806",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2806",
        "ok": "2806",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2806",
        "ok": "2806",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-4cd78": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111414",
path: "Animal-api sync for customer NL_111414",
pathFormatted: "req_animal-api-sync-4cd78",
stats: {
    "name": "Animal-api sync for customer NL_111414",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3509",
        "ok": "3509",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3509",
        "ok": "3509",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3509",
        "ok": "3509",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3509",
        "ok": "3509",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3509",
        "ok": "3509",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3509",
        "ok": "3509",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3509",
        "ok": "3509",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-0fed7": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_109760",
path: "Animal-api sync for customer NL_109760",
pathFormatted: "req_animal-api-sync-0fed7",
stats: {
    "name": "Animal-api sync for customer NL_109760",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2724",
        "ok": "2724",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2724",
        "ok": "2724",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2724",
        "ok": "2724",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2724",
        "ok": "2724",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2724",
        "ok": "2724",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2724",
        "ok": "2724",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2724",
        "ok": "2724",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-f47e6": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_120627",
path: "Animal-api sync for customer NL_120627",
pathFormatted: "req_animal-api-sync-f47e6",
stats: {
    "name": "Animal-api sync for customer NL_120627",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5862",
        "ok": "5862",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5862",
        "ok": "5862",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5862",
        "ok": "5862",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5862",
        "ok": "5862",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5862",
        "ok": "5862",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5862",
        "ok": "5862",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5862",
        "ok": "5862",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-66c16": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_112114",
path: "Animal-api sync for customer NL_112114",
pathFormatted: "req_animal-api-sync-66c16",
stats: {
    "name": "Animal-api sync for customer NL_112114",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1946",
        "ok": "1946",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1946",
        "ok": "1946",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1946",
        "ok": "1946",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1946",
        "ok": "1946",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1946",
        "ok": "1946",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1946",
        "ok": "1946",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1946",
        "ok": "1946",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-79b8e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_116848",
path: "Animal-api sync for customer NL_116848",
pathFormatted: "req_animal-api-sync-79b8e",
stats: {
    "name": "Animal-api sync for customer NL_116848",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4711",
        "ok": "4711",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4711",
        "ok": "4711",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4711",
        "ok": "4711",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4711",
        "ok": "4711",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4711",
        "ok": "4711",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4711",
        "ok": "4711",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4711",
        "ok": "4711",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-5420f": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110280",
path: "Animal-api sync for customer NL_110280",
pathFormatted: "req_animal-api-sync-5420f",
stats: {
    "name": "Animal-api sync for customer NL_110280",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3203",
        "ok": "3203",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3203",
        "ok": "3203",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3203",
        "ok": "3203",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3203",
        "ok": "3203",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3203",
        "ok": "3203",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3203",
        "ok": "3203",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3203",
        "ok": "3203",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-63b47": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111966",
path: "Animal-api sync for customer NL_111966",
pathFormatted: "req_animal-api-sync-63b47",
stats: {
    "name": "Animal-api sync for customer NL_111966",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4097",
        "ok": "4097",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4097",
        "ok": "4097",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4097",
        "ok": "4097",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4097",
        "ok": "4097",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4097",
        "ok": "4097",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4097",
        "ok": "4097",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4097",
        "ok": "4097",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-fbcf4": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_128711",
path: "Animal-api sync for customer NL_128711",
pathFormatted: "req_animal-api-sync-fbcf4",
stats: {
    "name": "Animal-api sync for customer NL_128711",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2477",
        "ok": "2477",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2477",
        "ok": "2477",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2477",
        "ok": "2477",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2477",
        "ok": "2477",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2477",
        "ok": "2477",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2477",
        "ok": "2477",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2477",
        "ok": "2477",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d05fc": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_131083",
path: "Animal-api sync for customer NL_131083",
pathFormatted: "req_animal-api-sync-d05fc",
stats: {
    "name": "Animal-api sync for customer NL_131083",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3838",
        "ok": "3838",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3838",
        "ok": "3838",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3838",
        "ok": "3838",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3838",
        "ok": "3838",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3838",
        "ok": "3838",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3838",
        "ok": "3838",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3838",
        "ok": "3838",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-ed470": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_115514",
path: "Animal-api sync for customer NL_115514",
pathFormatted: "req_animal-api-sync-ed470",
stats: {
    "name": "Animal-api sync for customer NL_115514",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2453",
        "ok": "2453",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2453",
        "ok": "2453",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2453",
        "ok": "2453",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2453",
        "ok": "2453",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2453",
        "ok": "2453",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2453",
        "ok": "2453",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2453",
        "ok": "2453",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-29375": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_121682",
path: "Animal-api sync for customer NL_121682",
pathFormatted: "req_animal-api-sync-29375",
stats: {
    "name": "Animal-api sync for customer NL_121682",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1409",
        "ok": "1409",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1409",
        "ok": "1409",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1409",
        "ok": "1409",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1409",
        "ok": "1409",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1409",
        "ok": "1409",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1409",
        "ok": "1409",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1409",
        "ok": "1409",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-4e713": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111266",
path: "Animal-api sync for customer NL_111266",
pathFormatted: "req_animal-api-sync-4e713",
stats: {
    "name": "Animal-api sync for customer NL_111266",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2409",
        "ok": "2409",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2409",
        "ok": "2409",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2409",
        "ok": "2409",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2409",
        "ok": "2409",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2409",
        "ok": "2409",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2409",
        "ok": "2409",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2409",
        "ok": "2409",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-5df96": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_114543",
path: "Animal-api sync for customer NL_114543",
pathFormatted: "req_animal-api-sync-5df96",
stats: {
    "name": "Animal-api sync for customer NL_114543",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1818",
        "ok": "1818",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1818",
        "ok": "1818",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1818",
        "ok": "1818",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1818",
        "ok": "1818",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1818",
        "ok": "1818",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1818",
        "ok": "1818",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1818",
        "ok": "1818",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-38246": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_118506",
path: "Animal-api sync for customer NL_118506",
pathFormatted: "req_animal-api-sync-38246",
stats: {
    "name": "Animal-api sync for customer NL_118506",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2231",
        "ok": "2231",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2231",
        "ok": "2231",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2231",
        "ok": "2231",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2231",
        "ok": "2231",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2231",
        "ok": "2231",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2231",
        "ok": "2231",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2231",
        "ok": "2231",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-3d430": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_134950",
path: "Animal-api sync for customer NL_134950",
pathFormatted: "req_animal-api-sync-3d430",
stats: {
    "name": "Animal-api sync for customer NL_134950",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1770",
        "ok": "1770",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1770",
        "ok": "1770",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1770",
        "ok": "1770",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1770",
        "ok": "1770",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1770",
        "ok": "1770",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1770",
        "ok": "1770",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1770",
        "ok": "1770",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a667b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_103980",
path: "Animal-api sync for customer NL_103980",
pathFormatted: "req_animal-api-sync-a667b",
stats: {
    "name": "Animal-api sync for customer NL_103980",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "9278",
        "ok": "9278",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "9278",
        "ok": "9278",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "9278",
        "ok": "9278",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "9278",
        "ok": "9278",
        "ko": "-"
    },
    "percentiles2": {
        "total": "9278",
        "ok": "9278",
        "ko": "-"
    },
    "percentiles3": {
        "total": "9278",
        "ok": "9278",
        "ko": "-"
    },
    "percentiles4": {
        "total": "9278",
        "ok": "9278",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-92bed": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105164",
path: "Animal-api sync for customer NL_105164",
pathFormatted: "req_animal-api-sync-92bed",
stats: {
    "name": "Animal-api sync for customer NL_105164",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3082",
        "ok": "3082",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3082",
        "ok": "3082",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3082",
        "ok": "3082",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3082",
        "ok": "3082",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3082",
        "ok": "3082",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3082",
        "ok": "3082",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3082",
        "ok": "3082",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-74e33": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_189313",
path: "Animal-api sync for customer BE_189313",
pathFormatted: "req_animal-api-sync-74e33",
stats: {
    "name": "Animal-api sync for customer BE_189313",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3589",
        "ok": "3589",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3589",
        "ok": "3589",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3589",
        "ok": "3589",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3589",
        "ok": "3589",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3589",
        "ok": "3589",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3589",
        "ok": "3589",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3589",
        "ok": "3589",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-aaf86": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_130803",
path: "Animal-api sync for customer NL_130803",
pathFormatted: "req_animal-api-sync-aaf86",
stats: {
    "name": "Animal-api sync for customer NL_130803",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3706",
        "ok": "3706",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3706",
        "ok": "3706",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3706",
        "ok": "3706",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3706",
        "ok": "3706",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3706",
        "ok": "3706",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3706",
        "ok": "3706",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3706",
        "ok": "3706",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-390c0": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_113006",
path: "Animal-api sync for customer NL_113006",
pathFormatted: "req_animal-api-sync-390c0",
stats: {
    "name": "Animal-api sync for customer NL_113006",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2223",
        "ok": "2223",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2223",
        "ok": "2223",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2223",
        "ok": "2223",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2223",
        "ok": "2223",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2223",
        "ok": "2223",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2223",
        "ok": "2223",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2223",
        "ok": "2223",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-363df": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_120238",
path: "Animal-api sync for customer NL_120238",
pathFormatted: "req_animal-api-sync-363df",
stats: {
    "name": "Animal-api sync for customer NL_120238",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2628",
        "ok": "2628",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2628",
        "ok": "2628",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2628",
        "ok": "2628",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2628",
        "ok": "2628",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2628",
        "ok": "2628",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2628",
        "ok": "2628",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2628",
        "ok": "2628",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e70df": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_129794",
path: "Animal-api sync for customer NL_129794",
pathFormatted: "req_animal-api-sync-e70df",
stats: {
    "name": "Animal-api sync for customer NL_129794",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1646",
        "ok": "1646",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1646",
        "ok": "1646",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1646",
        "ok": "1646",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1646",
        "ok": "1646",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1646",
        "ok": "1646",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1646",
        "ok": "1646",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1646",
        "ok": "1646",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a9219": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_103449",
path: "Animal-api sync for customer NL_103449",
pathFormatted: "req_animal-api-sync-a9219",
stats: {
    "name": "Animal-api sync for customer NL_103449",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3131",
        "ok": "3131",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3131",
        "ok": "3131",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3131",
        "ok": "3131",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3131",
        "ok": "3131",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3131",
        "ok": "3131",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3131",
        "ok": "3131",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3131",
        "ok": "3131",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-30320": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_128704",
path: "Animal-api sync for customer NL_128704",
pathFormatted: "req_animal-api-sync-30320",
stats: {
    "name": "Animal-api sync for customer NL_128704",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2146",
        "ok": "2146",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2146",
        "ok": "2146",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2146",
        "ok": "2146",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2146",
        "ok": "2146",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2146",
        "ok": "2146",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2146",
        "ok": "2146",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2146",
        "ok": "2146",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c50f5": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111928",
path: "Animal-api sync for customer NL_111928",
pathFormatted: "req_animal-api-sync-c50f5",
stats: {
    "name": "Animal-api sync for customer NL_111928",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2825",
        "ok": "2825",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2825",
        "ok": "2825",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2825",
        "ok": "2825",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2825",
        "ok": "2825",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2825",
        "ok": "2825",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2825",
        "ok": "2825",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2825",
        "ok": "2825",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-294ca": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111076",
path: "Animal-api sync for customer NL_111076",
pathFormatted: "req_animal-api-sync-294ca",
stats: {
    "name": "Animal-api sync for customer NL_111076",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2240",
        "ok": "2240",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2240",
        "ok": "2240",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2240",
        "ok": "2240",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2240",
        "ok": "2240",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2240",
        "ok": "2240",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2240",
        "ok": "2240",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2240",
        "ok": "2240",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-080dc": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_131881",
path: "Animal-api sync for customer NL_131881",
pathFormatted: "req_animal-api-sync-080dc",
stats: {
    "name": "Animal-api sync for customer NL_131881",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1737",
        "ok": "1737",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1737",
        "ok": "1737",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1737",
        "ok": "1737",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1737",
        "ok": "1737",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1737",
        "ok": "1737",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1737",
        "ok": "1737",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1737",
        "ok": "1737",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-9f153": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_113848",
path: "Animal-api sync for customer NL_113848",
pathFormatted: "req_animal-api-sync-9f153",
stats: {
    "name": "Animal-api sync for customer NL_113848",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2308",
        "ok": "2308",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2308",
        "ok": "2308",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2308",
        "ok": "2308",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2308",
        "ok": "2308",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2308",
        "ok": "2308",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2308",
        "ok": "2308",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2308",
        "ok": "2308",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-084e7": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_108590",
path: "Animal-api sync for customer NL_108590",
pathFormatted: "req_animal-api-sync-084e7",
stats: {
    "name": "Animal-api sync for customer NL_108590",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2623",
        "ok": "2623",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2623",
        "ok": "2623",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2623",
        "ok": "2623",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2623",
        "ok": "2623",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2623",
        "ok": "2623",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2623",
        "ok": "2623",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2623",
        "ok": "2623",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-93bcd": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_107077",
path: "Animal-api sync for customer NL_107077",
pathFormatted: "req_animal-api-sync-93bcd",
stats: {
    "name": "Animal-api sync for customer NL_107077",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1692",
        "ok": "1692",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1692",
        "ok": "1692",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1692",
        "ok": "1692",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1692",
        "ok": "1692",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1692",
        "ok": "1692",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1692",
        "ok": "1692",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1692",
        "ok": "1692",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-43480": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_211973",
path: "Animal-api sync for customer BE_211973",
pathFormatted: "req_animal-api-sync-43480",
stats: {
    "name": "Animal-api sync for customer BE_211973",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3768",
        "ok": "3768",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3768",
        "ok": "3768",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3768",
        "ok": "3768",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3768",
        "ok": "3768",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3768",
        "ok": "3768",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3768",
        "ok": "3768",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3768",
        "ok": "3768",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a5109": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_124277",
path: "Animal-api sync for customer NL_124277",
pathFormatted: "req_animal-api-sync-a5109",
stats: {
    "name": "Animal-api sync for customer NL_124277",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2584",
        "ok": "2584",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2584",
        "ok": "2584",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2584",
        "ok": "2584",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2584",
        "ok": "2584",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2584",
        "ok": "2584",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2584",
        "ok": "2584",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2584",
        "ok": "2584",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-53304": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_108829",
path: "Animal-api sync for customer NL_108829",
pathFormatted: "req_animal-api-sync-53304",
stats: {
    "name": "Animal-api sync for customer NL_108829",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3107",
        "ok": "3107",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3107",
        "ok": "3107",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3107",
        "ok": "3107",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3107",
        "ok": "3107",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3107",
        "ok": "3107",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3107",
        "ok": "3107",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3107",
        "ok": "3107",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-84fc1": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_131210",
path: "Animal-api sync for customer NL_131210",
pathFormatted: "req_animal-api-sync-84fc1",
stats: {
    "name": "Animal-api sync for customer NL_131210",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1781",
        "ok": "1781",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1781",
        "ok": "1781",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1781",
        "ok": "1781",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1781",
        "ok": "1781",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1781",
        "ok": "1781",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1781",
        "ok": "1781",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1781",
        "ok": "1781",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-fc8bc": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_119277",
path: "Animal-api sync for customer NL_119277",
pathFormatted: "req_animal-api-sync-fc8bc",
stats: {
    "name": "Animal-api sync for customer NL_119277",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2246",
        "ok": "2246",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2246",
        "ok": "2246",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2246",
        "ok": "2246",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2246",
        "ok": "2246",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2246",
        "ok": "2246",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2246",
        "ok": "2246",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2246",
        "ok": "2246",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b0637": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_109813",
path: "Animal-api sync for customer NL_109813",
pathFormatted: "req_animal-api-sync-b0637",
stats: {
    "name": "Animal-api sync for customer NL_109813",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2342",
        "ok": "2342",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2342",
        "ok": "2342",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2342",
        "ok": "2342",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2342",
        "ok": "2342",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2342",
        "ok": "2342",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2342",
        "ok": "2342",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2342",
        "ok": "2342",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d7914": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_189320",
path: "Animal-api sync for customer NL_189320",
pathFormatted: "req_animal-api-sync-d7914",
stats: {
    "name": "Animal-api sync for customer NL_189320",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2643",
        "ok": "2643",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2643",
        "ok": "2643",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2643",
        "ok": "2643",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2643",
        "ok": "2643",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2643",
        "ok": "2643",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2643",
        "ok": "2643",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2643",
        "ok": "2643",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-acdb8": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_113754",
path: "Animal-api sync for customer NL_113754",
pathFormatted: "req_animal-api-sync-acdb8",
stats: {
    "name": "Animal-api sync for customer NL_113754",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2987",
        "ok": "2987",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2987",
        "ok": "2987",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2987",
        "ok": "2987",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2987",
        "ok": "2987",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2987",
        "ok": "2987",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2987",
        "ok": "2987",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2987",
        "ok": "2987",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-36730": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_135526",
path: "Animal-api sync for customer NL_135526",
pathFormatted: "req_animal-api-sync-36730",
stats: {
    "name": "Animal-api sync for customer NL_135526",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1907",
        "ok": "1907",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1907",
        "ok": "1907",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1907",
        "ok": "1907",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1907",
        "ok": "1907",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1907",
        "ok": "1907",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1907",
        "ok": "1907",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1907",
        "ok": "1907",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-1e909": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_154872",
path: "Animal-api sync for customer BE_154872",
pathFormatted: "req_animal-api-sync-1e909",
stats: {
    "name": "Animal-api sync for customer BE_154872",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2479",
        "ok": "2479",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2479",
        "ok": "2479",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2479",
        "ok": "2479",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2479",
        "ok": "2479",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2479",
        "ok": "2479",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2479",
        "ok": "2479",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2479",
        "ok": "2479",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-f54d2": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_125689",
path: "Animal-api sync for customer NL_125689",
pathFormatted: "req_animal-api-sync-f54d2",
stats: {
    "name": "Animal-api sync for customer NL_125689",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2940",
        "ok": "2940",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2940",
        "ok": "2940",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2940",
        "ok": "2940",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2940",
        "ok": "2940",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2940",
        "ok": "2940",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2940",
        "ok": "2940",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2940",
        "ok": "2940",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-baa35": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110539",
path: "Animal-api sync for customer NL_110539",
pathFormatted: "req_animal-api-sync-baa35",
stats: {
    "name": "Animal-api sync for customer NL_110539",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2737",
        "ok": "2737",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2737",
        "ok": "2737",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2737",
        "ok": "2737",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2737",
        "ok": "2737",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2737",
        "ok": "2737",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2737",
        "ok": "2737",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2737",
        "ok": "2737",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-14454": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104271",
path: "Animal-api sync for customer NL_104271",
pathFormatted: "req_animal-api-sync-14454",
stats: {
    "name": "Animal-api sync for customer NL_104271",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4066",
        "ok": "4066",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4066",
        "ok": "4066",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4066",
        "ok": "4066",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4066",
        "ok": "4066",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4066",
        "ok": "4066",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4066",
        "ok": "4066",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4066",
        "ok": "4066",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b6907": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_123943",
path: "Animal-api sync for customer NL_123943",
pathFormatted: "req_animal-api-sync-b6907",
stats: {
    "name": "Animal-api sync for customer NL_123943",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2580",
        "ok": "2580",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2580",
        "ok": "2580",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2580",
        "ok": "2580",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2580",
        "ok": "2580",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2580",
        "ok": "2580",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2580",
        "ok": "2580",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2580",
        "ok": "2580",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c4646": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110070",
path: "Animal-api sync for customer NL_110070",
pathFormatted: "req_animal-api-sync-c4646",
stats: {
    "name": "Animal-api sync for customer NL_110070",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2515",
        "ok": "2515",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2515",
        "ok": "2515",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2515",
        "ok": "2515",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2515",
        "ok": "2515",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2515",
        "ok": "2515",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2515",
        "ok": "2515",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2515",
        "ok": "2515",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-0eeba": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_114348",
path: "Animal-api sync for customer NL_114348",
pathFormatted: "req_animal-api-sync-0eeba",
stats: {
    "name": "Animal-api sync for customer NL_114348",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2897",
        "ok": "2897",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2897",
        "ok": "2897",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2897",
        "ok": "2897",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2897",
        "ok": "2897",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2897",
        "ok": "2897",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2897",
        "ok": "2897",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2897",
        "ok": "2897",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-5c35b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_114376",
path: "Animal-api sync for customer NL_114376",
pathFormatted: "req_animal-api-sync-5c35b",
stats: {
    "name": "Animal-api sync for customer NL_114376",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1408",
        "ok": "1408",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1408",
        "ok": "1408",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1408",
        "ok": "1408",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1408",
        "ok": "1408",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1408",
        "ok": "1408",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1408",
        "ok": "1408",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1408",
        "ok": "1408",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-451e8": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_121268",
path: "Animal-api sync for customer NL_121268",
pathFormatted: "req_animal-api-sync-451e8",
stats: {
    "name": "Animal-api sync for customer NL_121268",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2176",
        "ok": "2176",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2176",
        "ok": "2176",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2176",
        "ok": "2176",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2176",
        "ok": "2176",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2176",
        "ok": "2176",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2176",
        "ok": "2176",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2176",
        "ok": "2176",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e42e2": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_128581",
path: "Animal-api sync for customer NL_128581",
pathFormatted: "req_animal-api-sync-e42e2",
stats: {
    "name": "Animal-api sync for customer NL_128581",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1559",
        "ok": "1559",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1559",
        "ok": "1559",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1559",
        "ok": "1559",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1559",
        "ok": "1559",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1559",
        "ok": "1559",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1559",
        "ok": "1559",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1559",
        "ok": "1559",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b164c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_120434",
path: "Animal-api sync for customer NL_120434",
pathFormatted: "req_animal-api-sync-b164c",
stats: {
    "name": "Animal-api sync for customer NL_120434",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3634",
        "ok": "3634",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3634",
        "ok": "3634",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3634",
        "ok": "3634",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3634",
        "ok": "3634",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3634",
        "ok": "3634",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3634",
        "ok": "3634",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3634",
        "ok": "3634",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-7461a": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_120398",
path: "Animal-api sync for customer NL_120398",
pathFormatted: "req_animal-api-sync-7461a",
stats: {
    "name": "Animal-api sync for customer NL_120398",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2447",
        "ok": "2447",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2447",
        "ok": "2447",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2447",
        "ok": "2447",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2447",
        "ok": "2447",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2447",
        "ok": "2447",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2447",
        "ok": "2447",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2447",
        "ok": "2447",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-07a69": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_103947",
path: "Animal-api sync for customer NL_103947",
pathFormatted: "req_animal-api-sync-07a69",
stats: {
    "name": "Animal-api sync for customer NL_103947",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4084",
        "ok": "4084",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4084",
        "ok": "4084",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4084",
        "ok": "4084",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4084",
        "ok": "4084",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4084",
        "ok": "4084",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4084",
        "ok": "4084",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4084",
        "ok": "4084",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c6405": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104467",
path: "Animal-api sync for customer NL_104467",
pathFormatted: "req_animal-api-sync-c6405",
stats: {
    "name": "Animal-api sync for customer NL_104467",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2484",
        "ok": "2484",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2484",
        "ok": "2484",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2484",
        "ok": "2484",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2484",
        "ok": "2484",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2484",
        "ok": "2484",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2484",
        "ok": "2484",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2484",
        "ok": "2484",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-23f13": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_125273",
path: "Animal-api sync for customer NL_125273",
pathFormatted: "req_animal-api-sync-23f13",
stats: {
    "name": "Animal-api sync for customer NL_125273",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2678",
        "ok": "2678",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2678",
        "ok": "2678",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2678",
        "ok": "2678",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2678",
        "ok": "2678",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2678",
        "ok": "2678",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2678",
        "ok": "2678",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2678",
        "ok": "2678",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-96127": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_161584",
path: "Animal-api sync for customer BE_161584",
pathFormatted: "req_animal-api-sync-96127",
stats: {
    "name": "Animal-api sync for customer BE_161584",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2370",
        "ok": "2370",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2370",
        "ok": "2370",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2370",
        "ok": "2370",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2370",
        "ok": "2370",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2370",
        "ok": "2370",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2370",
        "ok": "2370",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2370",
        "ok": "2370",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-614ac": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104734",
path: "Animal-api sync for customer NL_104734",
pathFormatted: "req_animal-api-sync-614ac",
stats: {
    "name": "Animal-api sync for customer NL_104734",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2428",
        "ok": "2428",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2428",
        "ok": "2428",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2428",
        "ok": "2428",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2428",
        "ok": "2428",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2428",
        "ok": "2428",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2428",
        "ok": "2428",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2428",
        "ok": "2428",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-2d56c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110590",
path: "Animal-api sync for customer NL_110590",
pathFormatted: "req_animal-api-sync-2d56c",
stats: {
    "name": "Animal-api sync for customer NL_110590",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2475",
        "ok": "2475",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2475",
        "ok": "2475",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2475",
        "ok": "2475",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2475",
        "ok": "2475",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2475",
        "ok": "2475",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2475",
        "ok": "2475",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2475",
        "ok": "2475",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e31c7": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_210459",
path: "Animal-api sync for customer NL_210459",
pathFormatted: "req_animal-api-sync-e31c7",
stats: {
    "name": "Animal-api sync for customer NL_210459",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2592",
        "ok": "2592",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2592",
        "ok": "2592",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2592",
        "ok": "2592",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2592",
        "ok": "2592",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2592",
        "ok": "2592",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2592",
        "ok": "2592",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2592",
        "ok": "2592",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-7fca2": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_158798",
path: "Animal-api sync for customer NL_158798",
pathFormatted: "req_animal-api-sync-7fca2",
stats: {
    "name": "Animal-api sync for customer NL_158798",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2904",
        "ok": "2904",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2904",
        "ok": "2904",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2904",
        "ok": "2904",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2904",
        "ok": "2904",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2904",
        "ok": "2904",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2904",
        "ok": "2904",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2904",
        "ok": "2904",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b8f29": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_153558",
path: "Animal-api sync for customer BE_153558",
pathFormatted: "req_animal-api-sync-b8f29",
stats: {
    "name": "Animal-api sync for customer BE_153558",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2103",
        "ok": "2103",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2103",
        "ok": "2103",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2103",
        "ok": "2103",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2103",
        "ok": "2103",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2103",
        "ok": "2103",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2103",
        "ok": "2103",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2103",
        "ok": "2103",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-aa22e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_137034",
path: "Animal-api sync for customer NL_137034",
pathFormatted: "req_animal-api-sync-aa22e",
stats: {
    "name": "Animal-api sync for customer NL_137034",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1193",
        "ok": "1193",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1193",
        "ok": "1193",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1193",
        "ok": "1193",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1193",
        "ok": "1193",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1193",
        "ok": "1193",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1193",
        "ok": "1193",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1193",
        "ok": "1193",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b5283": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_112270",
path: "Animal-api sync for customer NL_112270",
pathFormatted: "req_animal-api-sync-b5283",
stats: {
    "name": "Animal-api sync for customer NL_112270",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3508",
        "ok": "3508",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3508",
        "ok": "3508",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3508",
        "ok": "3508",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3508",
        "ok": "3508",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3508",
        "ok": "3508",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3508",
        "ok": "3508",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3508",
        "ok": "3508",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-10e14": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_113903",
path: "Animal-api sync for customer NL_113903",
pathFormatted: "req_animal-api-sync-10e14",
stats: {
    "name": "Animal-api sync for customer NL_113903",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5581",
        "ok": "5581",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5581",
        "ok": "5581",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5581",
        "ok": "5581",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5581",
        "ok": "5581",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5581",
        "ok": "5581",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5581",
        "ok": "5581",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5581",
        "ok": "5581",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-3dd2c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105439",
path: "Animal-api sync for customer NL_105439",
pathFormatted: "req_animal-api-sync-3dd2c",
stats: {
    "name": "Animal-api sync for customer NL_105439",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2821",
        "ok": "2821",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2821",
        "ok": "2821",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2821",
        "ok": "2821",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2821",
        "ok": "2821",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2821",
        "ok": "2821",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2821",
        "ok": "2821",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2821",
        "ok": "2821",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-8785d": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_207024",
path: "Animal-api sync for customer BE_207024",
pathFormatted: "req_animal-api-sync-8785d",
stats: {
    "name": "Animal-api sync for customer BE_207024",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2659",
        "ok": "2659",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2659",
        "ok": "2659",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2659",
        "ok": "2659",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2659",
        "ok": "2659",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2659",
        "ok": "2659",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2659",
        "ok": "2659",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2659",
        "ok": "2659",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-010fe": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_129945",
path: "Animal-api sync for customer NL_129945",
pathFormatted: "req_animal-api-sync-010fe",
stats: {
    "name": "Animal-api sync for customer NL_129945",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2180",
        "ok": "2180",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2180",
        "ok": "2180",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2180",
        "ok": "2180",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2180",
        "ok": "2180",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2180",
        "ok": "2180",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2180",
        "ok": "2180",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2180",
        "ok": "2180",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-58bfa": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_131425",
path: "Animal-api sync for customer NL_131425",
pathFormatted: "req_animal-api-sync-58bfa",
stats: {
    "name": "Animal-api sync for customer NL_131425",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2330",
        "ok": "2330",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2330",
        "ok": "2330",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2330",
        "ok": "2330",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2330",
        "ok": "2330",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2330",
        "ok": "2330",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2330",
        "ok": "2330",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2330",
        "ok": "2330",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-9a226": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_113819",
path: "Animal-api sync for customer NL_113819",
pathFormatted: "req_animal-api-sync-9a226",
stats: {
    "name": "Animal-api sync for customer NL_113819",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1785",
        "ok": "1785",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1785",
        "ok": "1785",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1785",
        "ok": "1785",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1785",
        "ok": "1785",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1785",
        "ok": "1785",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1785",
        "ok": "1785",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1785",
        "ok": "1785",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-44a53": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_121469",
path: "Animal-api sync for customer NL_121469",
pathFormatted: "req_animal-api-sync-44a53",
stats: {
    "name": "Animal-api sync for customer NL_121469",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3344",
        "ok": "3344",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3344",
        "ok": "3344",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3344",
        "ok": "3344",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3344",
        "ok": "3344",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3344",
        "ok": "3344",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3344",
        "ok": "3344",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3344",
        "ok": "3344",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c3fd5": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104563",
path: "Animal-api sync for customer NL_104563",
pathFormatted: "req_animal-api-sync-c3fd5",
stats: {
    "name": "Animal-api sync for customer NL_104563",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2899",
        "ok": "2899",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2899",
        "ok": "2899",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2899",
        "ok": "2899",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2899",
        "ok": "2899",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2899",
        "ok": "2899",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2899",
        "ok": "2899",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2899",
        "ok": "2899",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-2f86f": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_122640",
path: "Animal-api sync for customer NL_122640",
pathFormatted: "req_animal-api-sync-2f86f",
stats: {
    "name": "Animal-api sync for customer NL_122640",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5257",
        "ok": "5257",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5257",
        "ok": "5257",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5257",
        "ok": "5257",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5257",
        "ok": "5257",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5257",
        "ok": "5257",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5257",
        "ok": "5257",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5257",
        "ok": "5257",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-447ce": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_136064",
path: "Animal-api sync for customer NL_136064",
pathFormatted: "req_animal-api-sync-447ce",
stats: {
    "name": "Animal-api sync for customer NL_136064",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1701",
        "ok": "1701",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1701",
        "ok": "1701",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1701",
        "ok": "1701",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1701",
        "ok": "1701",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1701",
        "ok": "1701",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1701",
        "ok": "1701",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1701",
        "ok": "1701",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-02723": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_106633",
path: "Animal-api sync for customer NL_106633",
pathFormatted: "req_animal-api-sync-02723",
stats: {
    "name": "Animal-api sync for customer NL_106633",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2326",
        "ok": "2326",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2326",
        "ok": "2326",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2326",
        "ok": "2326",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2326",
        "ok": "2326",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2326",
        "ok": "2326",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2326",
        "ok": "2326",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2326",
        "ok": "2326",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-19174": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117339",
path: "Animal-api sync for customer NL_117339",
pathFormatted: "req_animal-api-sync-19174",
stats: {
    "name": "Animal-api sync for customer NL_117339",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2159",
        "ok": "2159",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2159",
        "ok": "2159",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2159",
        "ok": "2159",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2159",
        "ok": "2159",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2159",
        "ok": "2159",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2159",
        "ok": "2159",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2159",
        "ok": "2159",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-9a47c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_115141",
path: "Animal-api sync for customer NL_115141",
pathFormatted: "req_animal-api-sync-9a47c",
stats: {
    "name": "Animal-api sync for customer NL_115141",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2601",
        "ok": "2601",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2601",
        "ok": "2601",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2601",
        "ok": "2601",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2601",
        "ok": "2601",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2601",
        "ok": "2601",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2601",
        "ok": "2601",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2601",
        "ok": "2601",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-0a9fa": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_125735",
path: "Animal-api sync for customer NL_125735",
pathFormatted: "req_animal-api-sync-0a9fa",
stats: {
    "name": "Animal-api sync for customer NL_125735",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2839",
        "ok": "2839",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2839",
        "ok": "2839",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2839",
        "ok": "2839",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2839",
        "ok": "2839",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2839",
        "ok": "2839",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2839",
        "ok": "2839",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2839",
        "ok": "2839",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-56dad": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_112805",
path: "Animal-api sync for customer NL_112805",
pathFormatted: "req_animal-api-sync-56dad",
stats: {
    "name": "Animal-api sync for customer NL_112805",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2070",
        "ok": "2070",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2070",
        "ok": "2070",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2070",
        "ok": "2070",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2070",
        "ok": "2070",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2070",
        "ok": "2070",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2070",
        "ok": "2070",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2070",
        "ok": "2070",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-4c3a5": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110894",
path: "Animal-api sync for customer NL_110894",
pathFormatted: "req_animal-api-sync-4c3a5",
stats: {
    "name": "Animal-api sync for customer NL_110894",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2571",
        "ok": "2571",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2571",
        "ok": "2571",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2571",
        "ok": "2571",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2571",
        "ok": "2571",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2571",
        "ok": "2571",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2571",
        "ok": "2571",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2571",
        "ok": "2571",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-86c49": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_145826",
path: "Animal-api sync for customer BE_145826",
pathFormatted: "req_animal-api-sync-86c49",
stats: {
    "name": "Animal-api sync for customer BE_145826",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2259",
        "ok": "2259",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2259",
        "ok": "2259",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2259",
        "ok": "2259",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2259",
        "ok": "2259",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2259",
        "ok": "2259",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2259",
        "ok": "2259",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2259",
        "ok": "2259",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-9e7ff": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110428",
path: "Animal-api sync for customer NL_110428",
pathFormatted: "req_animal-api-sync-9e7ff",
stats: {
    "name": "Animal-api sync for customer NL_110428",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2164",
        "ok": "2164",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2164",
        "ok": "2164",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2164",
        "ok": "2164",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2164",
        "ok": "2164",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2164",
        "ok": "2164",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2164",
        "ok": "2164",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2164",
        "ok": "2164",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-6c072": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_115279",
path: "Animal-api sync for customer NL_115279",
pathFormatted: "req_animal-api-sync-6c072",
stats: {
    "name": "Animal-api sync for customer NL_115279",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2285",
        "ok": "2285",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2285",
        "ok": "2285",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2285",
        "ok": "2285",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2285",
        "ok": "2285",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2285",
        "ok": "2285",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2285",
        "ok": "2285",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2285",
        "ok": "2285",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-2de80": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_129343",
path: "Animal-api sync for customer NL_129343",
pathFormatted: "req_animal-api-sync-2de80",
stats: {
    "name": "Animal-api sync for customer NL_129343",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2316",
        "ok": "2316",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2316",
        "ok": "2316",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2316",
        "ok": "2316",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2316",
        "ok": "2316",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2316",
        "ok": "2316",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2316",
        "ok": "2316",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2316",
        "ok": "2316",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-925cc": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_120686",
path: "Animal-api sync for customer NL_120686",
pathFormatted: "req_animal-api-sync-925cc",
stats: {
    "name": "Animal-api sync for customer NL_120686",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3150",
        "ok": "3150",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3150",
        "ok": "3150",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3150",
        "ok": "3150",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3150",
        "ok": "3150",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3150",
        "ok": "3150",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3150",
        "ok": "3150",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3150",
        "ok": "3150",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-f6a28": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_129516",
path: "Animal-api sync for customer NL_129516",
pathFormatted: "req_animal-api-sync-f6a28",
stats: {
    "name": "Animal-api sync for customer NL_129516",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "9625",
        "ok": "9625",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "9625",
        "ok": "9625",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "9625",
        "ok": "9625",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "9625",
        "ok": "9625",
        "ko": "-"
    },
    "percentiles2": {
        "total": "9625",
        "ok": "9625",
        "ko": "-"
    },
    "percentiles3": {
        "total": "9625",
        "ok": "9625",
        "ko": "-"
    },
    "percentiles4": {
        "total": "9625",
        "ok": "9625",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-10907": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_155615",
path: "Animal-api sync for customer NL_155615",
pathFormatted: "req_animal-api-sync-10907",
stats: {
    "name": "Animal-api sync for customer NL_155615",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2627",
        "ok": "2627",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2627",
        "ok": "2627",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2627",
        "ok": "2627",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2627",
        "ok": "2627",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2627",
        "ok": "2627",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2627",
        "ok": "2627",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2627",
        "ok": "2627",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-83fad": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_161486",
path: "Animal-api sync for customer NL_161486",
pathFormatted: "req_animal-api-sync-83fad",
stats: {
    "name": "Animal-api sync for customer NL_161486",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4441",
        "ok": "4441",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4441",
        "ok": "4441",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4441",
        "ok": "4441",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4441",
        "ok": "4441",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4441",
        "ok": "4441",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4441",
        "ok": "4441",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4441",
        "ok": "4441",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-6eeed": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_136560",
path: "Animal-api sync for customer NL_136560",
pathFormatted: "req_animal-api-sync-6eeed",
stats: {
    "name": "Animal-api sync for customer NL_136560",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2020",
        "ok": "2020",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2020",
        "ok": "2020",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2020",
        "ok": "2020",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2020",
        "ok": "2020",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2020",
        "ok": "2020",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2020",
        "ok": "2020",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2020",
        "ok": "2020",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-678da": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_116304",
path: "Animal-api sync for customer NL_116304",
pathFormatted: "req_animal-api-sync-678da",
stats: {
    "name": "Animal-api sync for customer NL_116304",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4852",
        "ok": "4852",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4852",
        "ok": "4852",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4852",
        "ok": "4852",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4852",
        "ok": "4852",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4852",
        "ok": "4852",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4852",
        "ok": "4852",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4852",
        "ok": "4852",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-23e9e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_136624",
path: "Animal-api sync for customer NL_136624",
pathFormatted: "req_animal-api-sync-23e9e",
stats: {
    "name": "Animal-api sync for customer NL_136624",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2614",
        "ok": "2614",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2614",
        "ok": "2614",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2614",
        "ok": "2614",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2614",
        "ok": "2614",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2614",
        "ok": "2614",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2614",
        "ok": "2614",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2614",
        "ok": "2614",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b45fa": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_160578",
path: "Animal-api sync for customer BE_160578",
pathFormatted: "req_animal-api-sync-b45fa",
stats: {
    "name": "Animal-api sync for customer BE_160578",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5029",
        "ok": "5029",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5029",
        "ok": "5029",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5029",
        "ok": "5029",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5029",
        "ok": "5029",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5029",
        "ok": "5029",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5029",
        "ok": "5029",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5029",
        "ok": "5029",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-ed8de": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117329",
path: "Animal-api sync for customer NL_117329",
pathFormatted: "req_animal-api-sync-ed8de",
stats: {
    "name": "Animal-api sync for customer NL_117329",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3558",
        "ok": "3558",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3558",
        "ok": "3558",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3558",
        "ok": "3558",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3558",
        "ok": "3558",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3558",
        "ok": "3558",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3558",
        "ok": "3558",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3558",
        "ok": "3558",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-7699b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_115653",
path: "Animal-api sync for customer NL_115653",
pathFormatted: "req_animal-api-sync-7699b",
stats: {
    "name": "Animal-api sync for customer NL_115653",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1718",
        "ok": "1718",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1718",
        "ok": "1718",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1718",
        "ok": "1718",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1718",
        "ok": "1718",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1718",
        "ok": "1718",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1718",
        "ok": "1718",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1718",
        "ok": "1718",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-f6750": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_145660",
path: "Animal-api sync for customer BE_145660",
pathFormatted: "req_animal-api-sync-f6750",
stats: {
    "name": "Animal-api sync for customer BE_145660",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3547",
        "ok": "3547",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3547",
        "ok": "3547",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3547",
        "ok": "3547",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3547",
        "ok": "3547",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3547",
        "ok": "3547",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3547",
        "ok": "3547",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3547",
        "ok": "3547",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-de4e1": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_196422",
path: "Animal-api sync for customer BE_196422",
pathFormatted: "req_animal-api-sync-de4e1",
stats: {
    "name": "Animal-api sync for customer BE_196422",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2099",
        "ok": "2099",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2099",
        "ok": "2099",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2099",
        "ok": "2099",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2099",
        "ok": "2099",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2099",
        "ok": "2099",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2099",
        "ok": "2099",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2099",
        "ok": "2099",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-94458": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111031",
path: "Animal-api sync for customer NL_111031",
pathFormatted: "req_animal-api-sync-94458",
stats: {
    "name": "Animal-api sync for customer NL_111031",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2764",
        "ok": "2764",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2764",
        "ok": "2764",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2764",
        "ok": "2764",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2764",
        "ok": "2764",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2764",
        "ok": "2764",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2764",
        "ok": "2764",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2764",
        "ok": "2764",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-1114b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105737",
path: "Animal-api sync for customer NL_105737",
pathFormatted: "req_animal-api-sync-1114b",
stats: {
    "name": "Animal-api sync for customer NL_105737",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2688",
        "ok": "2688",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2688",
        "ok": "2688",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2688",
        "ok": "2688",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2688",
        "ok": "2688",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2688",
        "ok": "2688",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2688",
        "ok": "2688",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2688",
        "ok": "2688",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-34261": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_112642",
path: "Animal-api sync for customer NL_112642",
pathFormatted: "req_animal-api-sync-34261",
stats: {
    "name": "Animal-api sync for customer NL_112642",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3426",
        "ok": "3426",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3426",
        "ok": "3426",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3426",
        "ok": "3426",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3426",
        "ok": "3426",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3426",
        "ok": "3426",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3426",
        "ok": "3426",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3426",
        "ok": "3426",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-88106": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_112559",
path: "Animal-api sync for customer NL_112559",
pathFormatted: "req_animal-api-sync-88106",
stats: {
    "name": "Animal-api sync for customer NL_112559",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2465",
        "ok": "2465",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2465",
        "ok": "2465",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2465",
        "ok": "2465",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2465",
        "ok": "2465",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2465",
        "ok": "2465",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2465",
        "ok": "2465",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2465",
        "ok": "2465",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-12cf4": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_103806",
path: "Animal-api sync for customer NL_103806",
pathFormatted: "req_animal-api-sync-12cf4",
stats: {
    "name": "Animal-api sync for customer NL_103806",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3039",
        "ok": "3039",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3039",
        "ok": "3039",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3039",
        "ok": "3039",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3039",
        "ok": "3039",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3039",
        "ok": "3039",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3039",
        "ok": "3039",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3039",
        "ok": "3039",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-0c8bf": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_135056",
path: "Animal-api sync for customer NL_135056",
pathFormatted: "req_animal-api-sync-0c8bf",
stats: {
    "name": "Animal-api sync for customer NL_135056",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3168",
        "ok": "3168",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3168",
        "ok": "3168",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3168",
        "ok": "3168",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3168",
        "ok": "3168",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3168",
        "ok": "3168",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3168",
        "ok": "3168",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3168",
        "ok": "3168",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-cd315": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_115640",
path: "Animal-api sync for customer NL_115640",
pathFormatted: "req_animal-api-sync-cd315",
stats: {
    "name": "Animal-api sync for customer NL_115640",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2025",
        "ok": "2025",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2025",
        "ok": "2025",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2025",
        "ok": "2025",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2025",
        "ok": "2025",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2025",
        "ok": "2025",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2025",
        "ok": "2025",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2025",
        "ok": "2025",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-708ff": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111792",
path: "Animal-api sync for customer NL_111792",
pathFormatted: "req_animal-api-sync-708ff",
stats: {
    "name": "Animal-api sync for customer NL_111792",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2727",
        "ok": "2727",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2727",
        "ok": "2727",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2727",
        "ok": "2727",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2727",
        "ok": "2727",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2727",
        "ok": "2727",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2727",
        "ok": "2727",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2727",
        "ok": "2727",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c74f5": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110479",
path: "Animal-api sync for customer NL_110479",
pathFormatted: "req_animal-api-sync-c74f5",
stats: {
    "name": "Animal-api sync for customer NL_110479",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3485",
        "ok": "3485",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3485",
        "ok": "3485",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3485",
        "ok": "3485",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3485",
        "ok": "3485",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3485",
        "ok": "3485",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3485",
        "ok": "3485",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3485",
        "ok": "3485",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b4519": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110383",
path: "Animal-api sync for customer NL_110383",
pathFormatted: "req_animal-api-sync-b4519",
stats: {
    "name": "Animal-api sync for customer NL_110383",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2588",
        "ok": "2588",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2588",
        "ok": "2588",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2588",
        "ok": "2588",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2588",
        "ok": "2588",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2588",
        "ok": "2588",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2588",
        "ok": "2588",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2588",
        "ok": "2588",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-ccec3": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_159045",
path: "Animal-api sync for customer BE_159045",
pathFormatted: "req_animal-api-sync-ccec3",
stats: {
    "name": "Animal-api sync for customer BE_159045",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3356",
        "ok": "3356",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3356",
        "ok": "3356",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3356",
        "ok": "3356",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3356",
        "ok": "3356",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3356",
        "ok": "3356",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3356",
        "ok": "3356",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3356",
        "ok": "3356",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-f73d7": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_112171",
path: "Animal-api sync for customer NL_112171",
pathFormatted: "req_animal-api-sync-f73d7",
stats: {
    "name": "Animal-api sync for customer NL_112171",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3173",
        "ok": "3173",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3173",
        "ok": "3173",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3173",
        "ok": "3173",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3173",
        "ok": "3173",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3173",
        "ok": "3173",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3173",
        "ok": "3173",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3173",
        "ok": "3173",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c61a4": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_158630",
path: "Animal-api sync for customer NL_158630",
pathFormatted: "req_animal-api-sync-c61a4",
stats: {
    "name": "Animal-api sync for customer NL_158630",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3675",
        "ok": "3675",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3675",
        "ok": "3675",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3675",
        "ok": "3675",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3675",
        "ok": "3675",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3675",
        "ok": "3675",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3675",
        "ok": "3675",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3675",
        "ok": "3675",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-3df21": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111890",
path: "Animal-api sync for customer NL_111890",
pathFormatted: "req_animal-api-sync-3df21",
stats: {
    "name": "Animal-api sync for customer NL_111890",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3175",
        "ok": "3175",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3175",
        "ok": "3175",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3175",
        "ok": "3175",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3175",
        "ok": "3175",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3175",
        "ok": "3175",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3175",
        "ok": "3175",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3175",
        "ok": "3175",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-11d0e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_119978",
path: "Animal-api sync for customer NL_119978",
pathFormatted: "req_animal-api-sync-11d0e",
stats: {
    "name": "Animal-api sync for customer NL_119978",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3018",
        "ok": "3018",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3018",
        "ok": "3018",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3018",
        "ok": "3018",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3018",
        "ok": "3018",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3018",
        "ok": "3018",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3018",
        "ok": "3018",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3018",
        "ok": "3018",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-cd0f8": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117391",
path: "Animal-api sync for customer NL_117391",
pathFormatted: "req_animal-api-sync-cd0f8",
stats: {
    "name": "Animal-api sync for customer NL_117391",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2816",
        "ok": "2816",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2816",
        "ok": "2816",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2816",
        "ok": "2816",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2816",
        "ok": "2816",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2816",
        "ok": "2816",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2816",
        "ok": "2816",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2816",
        "ok": "2816",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-997ea": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111277",
path: "Animal-api sync for customer NL_111277",
pathFormatted: "req_animal-api-sync-997ea",
stats: {
    "name": "Animal-api sync for customer NL_111277",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3369",
        "ok": "3369",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3369",
        "ok": "3369",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3369",
        "ok": "3369",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3369",
        "ok": "3369",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3369",
        "ok": "3369",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3369",
        "ok": "3369",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3369",
        "ok": "3369",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-998da": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_120046",
path: "Animal-api sync for customer NL_120046",
pathFormatted: "req_animal-api-sync-998da",
stats: {
    "name": "Animal-api sync for customer NL_120046",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1944",
        "ok": "1944",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1944",
        "ok": "1944",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1944",
        "ok": "1944",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1944",
        "ok": "1944",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1944",
        "ok": "1944",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1944",
        "ok": "1944",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1944",
        "ok": "1944",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-08b09": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_122753",
path: "Animal-api sync for customer NL_122753",
pathFormatted: "req_animal-api-sync-08b09",
stats: {
    "name": "Animal-api sync for customer NL_122753",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3301",
        "ok": "3301",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3301",
        "ok": "3301",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3301",
        "ok": "3301",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3301",
        "ok": "3301",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3301",
        "ok": "3301",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3301",
        "ok": "3301",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3301",
        "ok": "3301",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a5635": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_193658",
path: "Animal-api sync for customer BE_193658",
pathFormatted: "req_animal-api-sync-a5635",
stats: {
    "name": "Animal-api sync for customer BE_193658",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3228",
        "ok": "3228",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3228",
        "ok": "3228",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3228",
        "ok": "3228",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3228",
        "ok": "3228",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3228",
        "ok": "3228",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3228",
        "ok": "3228",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3228",
        "ok": "3228",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-f89f8": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_121091",
path: "Animal-api sync for customer NL_121091",
pathFormatted: "req_animal-api-sync-f89f8",
stats: {
    "name": "Animal-api sync for customer NL_121091",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1224",
        "ok": "1224",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1224",
        "ok": "1224",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1224",
        "ok": "1224",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1224",
        "ok": "1224",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1224",
        "ok": "1224",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1224",
        "ok": "1224",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1224",
        "ok": "1224",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-6c82d": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_143962",
path: "Animal-api sync for customer NL_143962",
pathFormatted: "req_animal-api-sync-6c82d",
stats: {
    "name": "Animal-api sync for customer NL_143962",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2190",
        "ok": "2190",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2190",
        "ok": "2190",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2190",
        "ok": "2190",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2190",
        "ok": "2190",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2190",
        "ok": "2190",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2190",
        "ok": "2190",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2190",
        "ok": "2190",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-afe85": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104819",
path: "Animal-api sync for customer NL_104819",
pathFormatted: "req_animal-api-sync-afe85",
stats: {
    "name": "Animal-api sync for customer NL_104819",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2980",
        "ok": "2980",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2980",
        "ok": "2980",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2980",
        "ok": "2980",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2980",
        "ok": "2980",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2980",
        "ok": "2980",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2980",
        "ok": "2980",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2980",
        "ok": "2980",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d9f9d": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_156439",
path: "Animal-api sync for customer BE_156439",
pathFormatted: "req_animal-api-sync-d9f9d",
stats: {
    "name": "Animal-api sync for customer BE_156439",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2841",
        "ok": "2841",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2841",
        "ok": "2841",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2841",
        "ok": "2841",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2841",
        "ok": "2841",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2841",
        "ok": "2841",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2841",
        "ok": "2841",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2841",
        "ok": "2841",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-038e6": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117080",
path: "Animal-api sync for customer NL_117080",
pathFormatted: "req_animal-api-sync-038e6",
stats: {
    "name": "Animal-api sync for customer NL_117080",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2883",
        "ok": "2883",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2883",
        "ok": "2883",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2883",
        "ok": "2883",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2883",
        "ok": "2883",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2883",
        "ok": "2883",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2883",
        "ok": "2883",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2883",
        "ok": "2883",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-9a2c1": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_142403",
path: "Animal-api sync for customer NL_142403",
pathFormatted: "req_animal-api-sync-9a2c1",
stats: {
    "name": "Animal-api sync for customer NL_142403",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2491",
        "ok": "2491",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2491",
        "ok": "2491",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2491",
        "ok": "2491",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2491",
        "ok": "2491",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2491",
        "ok": "2491",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2491",
        "ok": "2491",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2491",
        "ok": "2491",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e1749": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_121066",
path: "Animal-api sync for customer NL_121066",
pathFormatted: "req_animal-api-sync-e1749",
stats: {
    "name": "Animal-api sync for customer NL_121066",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2467",
        "ok": "2467",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2467",
        "ok": "2467",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2467",
        "ok": "2467",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2467",
        "ok": "2467",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2467",
        "ok": "2467",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2467",
        "ok": "2467",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2467",
        "ok": "2467",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c09d9": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_126693",
path: "Animal-api sync for customer NL_126693",
pathFormatted: "req_animal-api-sync-c09d9",
stats: {
    "name": "Animal-api sync for customer NL_126693",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2211",
        "ok": "2211",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2211",
        "ok": "2211",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2211",
        "ok": "2211",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2211",
        "ok": "2211",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2211",
        "ok": "2211",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2211",
        "ok": "2211",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2211",
        "ok": "2211",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-cdc3c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_125737",
path: "Animal-api sync for customer NL_125737",
pathFormatted: "req_animal-api-sync-cdc3c",
stats: {
    "name": "Animal-api sync for customer NL_125737",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2628",
        "ok": "2628",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2628",
        "ok": "2628",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2628",
        "ok": "2628",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2628",
        "ok": "2628",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2628",
        "ok": "2628",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2628",
        "ok": "2628",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2628",
        "ok": "2628",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-339e7": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_115945",
path: "Animal-api sync for customer NL_115945",
pathFormatted: "req_animal-api-sync-339e7",
stats: {
    "name": "Animal-api sync for customer NL_115945",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1714",
        "ok": "1714",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1714",
        "ok": "1714",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1714",
        "ok": "1714",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1714",
        "ok": "1714",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1714",
        "ok": "1714",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1714",
        "ok": "1714",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1714",
        "ok": "1714",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c1773": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105858",
path: "Animal-api sync for customer NL_105858",
pathFormatted: "req_animal-api-sync-c1773",
stats: {
    "name": "Animal-api sync for customer NL_105858",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2673",
        "ok": "2673",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2673",
        "ok": "2673",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2673",
        "ok": "2673",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2673",
        "ok": "2673",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2673",
        "ok": "2673",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2673",
        "ok": "2673",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2673",
        "ok": "2673",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c41a4": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_118516",
path: "Animal-api sync for customer NL_118516",
pathFormatted: "req_animal-api-sync-c41a4",
stats: {
    "name": "Animal-api sync for customer NL_118516",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2133",
        "ok": "2133",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2133",
        "ok": "2133",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2133",
        "ok": "2133",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2133",
        "ok": "2133",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2133",
        "ok": "2133",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2133",
        "ok": "2133",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2133",
        "ok": "2133",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e8c3a": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_154725",
path: "Animal-api sync for customer BE_154725",
pathFormatted: "req_animal-api-sync-e8c3a",
stats: {
    "name": "Animal-api sync for customer BE_154725",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2758",
        "ok": "2758",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2758",
        "ok": "2758",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2758",
        "ok": "2758",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2758",
        "ok": "2758",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2758",
        "ok": "2758",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2758",
        "ok": "2758",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2758",
        "ok": "2758",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-fc13a": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_154710",
path: "Animal-api sync for customer BE_154710",
pathFormatted: "req_animal-api-sync-fc13a",
stats: {
    "name": "Animal-api sync for customer BE_154710",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2869",
        "ok": "2869",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2869",
        "ok": "2869",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2869",
        "ok": "2869",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2869",
        "ok": "2869",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2869",
        "ok": "2869",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2869",
        "ok": "2869",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2869",
        "ok": "2869",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d0d7d": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_147835",
path: "Animal-api sync for customer BE_147835",
pathFormatted: "req_animal-api-sync-d0d7d",
stats: {
    "name": "Animal-api sync for customer BE_147835",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3072",
        "ok": "3072",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3072",
        "ok": "3072",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3072",
        "ok": "3072",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3072",
        "ok": "3072",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3072",
        "ok": "3072",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3072",
        "ok": "3072",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3072",
        "ok": "3072",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-f852c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_129269",
path: "Animal-api sync for customer NL_129269",
pathFormatted: "req_animal-api-sync-f852c",
stats: {
    "name": "Animal-api sync for customer NL_129269",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2990",
        "ok": "2990",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2990",
        "ok": "2990",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2990",
        "ok": "2990",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2990",
        "ok": "2990",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2990",
        "ok": "2990",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2990",
        "ok": "2990",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2990",
        "ok": "2990",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
