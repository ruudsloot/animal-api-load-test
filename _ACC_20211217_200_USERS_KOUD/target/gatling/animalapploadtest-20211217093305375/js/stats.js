var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "4500",
        "ok": "4293",
        "ko": "207"
    },
    "minResponseTime": {
        "total": "8",
        "ok": "8",
        "ko": "64"
    },
    "maxResponseTime": {
        "total": "61652",
        "ok": "61652",
        "ko": "60008"
    },
    "meanResponseTime": {
        "total": "9238",
        "ok": "6819",
        "ko": "59422"
    },
    "standardDeviation": {
        "total": "19825",
        "ok": "16824",
        "ko": "5861"
    },
    "percentiles1": {
        "total": "60",
        "ok": "57",
        "ko": "60000"
    },
    "percentiles2": {
        "total": "1301",
        "ok": "776",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "60022",
        "ok": "60023",
        "ko": "60002"
    },
    "percentiles4": {
        "total": "60059",
        "ok": "60061",
        "ko": "60006"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 3075,
    "percentage": 68
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 229,
    "percentage": 5
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 989,
    "percentage": 22
},
    "group4": {
    "name": "failed",
    "count": 207,
    "percentage": 5
},
    "meanNumberOfRequestsPerSecond": {
        "total": "7.759",
        "ok": "7.402",
        "ko": "0.357"
    }
},
contents: {
"req_get-landing-pag-93979": {
        type: "REQUEST",
        name: "GET landing page",
path: "GET landing page",
pathFormatted: "req_get-landing-pag-93979",
stats: {
    "name": "GET landing page",
    "numberOfRequests": {
        "total": "375",
        "ok": "375",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "8",
        "ok": "8",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1461",
        "ok": "1461",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "62",
        "ok": "62",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "145",
        "ok": "145",
        "ko": "-"
    },
    "percentiles1": {
        "total": "27",
        "ok": "27",
        "ko": "-"
    },
    "percentiles2": {
        "total": "52",
        "ok": "52",
        "ko": "-"
    },
    "percentiles3": {
        "total": "178",
        "ok": "178",
        "ko": "-"
    },
    "percentiles4": {
        "total": "991",
        "ok": "991",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 369,
    "percentage": 98
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 2,
    "percentage": 1
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 4,
    "percentage": 1
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.647",
        "ok": "0.647",
        "ko": "-"
    }
}
    },"req_get-database-05cff": {
        type: "REQUEST",
        name: "GET Database",
path: "GET Database",
pathFormatted: "req_get-database-05cff",
stats: {
    "name": "GET Database",
    "numberOfRequests": {
        "total": "375",
        "ok": "375",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "17",
        "ok": "17",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1535",
        "ok": "1535",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "87",
        "ok": "87",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "156",
        "ok": "156",
        "ko": "-"
    },
    "percentiles1": {
        "total": "47",
        "ok": "47",
        "ko": "-"
    },
    "percentiles2": {
        "total": "75",
        "ok": "75",
        "ko": "-"
    },
    "percentiles3": {
        "total": "228",
        "ok": "228",
        "ko": "-"
    },
    "percentiles4": {
        "total": "985",
        "ok": "985",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 366,
    "percentage": 98
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 5,
    "percentage": 1
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 4,
    "percentage": 1
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.647",
        "ok": "0.647",
        "ko": "-"
    }
}
    },"req_get-suppressed--91726": {
        type: "REQUEST",
        name: "GET suppressed-ui",
path: "GET suppressed-ui",
pathFormatted: "req_get-suppressed--91726",
stats: {
    "name": "GET suppressed-ui",
    "numberOfRequests": {
        "total": "375",
        "ok": "375",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "9",
        "ok": "9",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "507",
        "ok": "507",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "40",
        "ok": "40",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "46",
        "ok": "46",
        "ko": "-"
    },
    "percentiles1": {
        "total": "31",
        "ok": "31",
        "ko": "-"
    },
    "percentiles2": {
        "total": "44",
        "ok": "44",
        "ko": "-"
    },
    "percentiles3": {
        "total": "96",
        "ok": "96",
        "ko": "-"
    },
    "percentiles4": {
        "total": "164",
        "ok": "164",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 374,
    "percentage": 100
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 1,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.647",
        "ok": "0.647",
        "ko": "-"
    }
}
    },"req_post-sync-custo-6f7e3": {
        type: "REQUEST",
        name: "POST sync-customer-info",
path: "POST sync-customer-info",
pathFormatted: "req_post-sync-custo-6f7e3",
stats: {
    "name": "POST sync-customer-info",
    "numberOfRequests": {
        "total": "375",
        "ok": "373",
        "ko": "2"
    },
    "minResponseTime": {
        "total": "110",
        "ok": "190",
        "ko": "110"
    },
    "maxResponseTime": {
        "total": "60000",
        "ok": "47379",
        "ko": "60000"
    },
    "meanResponseTime": {
        "total": "13071",
        "ok": "12979",
        "ko": "30055"
    },
    "standardDeviation": {
        "total": "13700",
        "ok": "13503",
        "ko": "29945"
    },
    "percentiles1": {
        "total": "8216",
        "ok": "8216",
        "ko": "30055"
    },
    "percentiles2": {
        "total": "18236",
        "ok": "18226",
        "ko": "45028"
    },
    "percentiles3": {
        "total": "39828",
        "ok": "39495",
        "ko": "57006"
    },
    "percentiles4": {
        "total": "43683",
        "ok": "43064",
        "ko": "59401"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 14,
    "percentage": 4
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 47,
    "percentage": 13
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 312,
    "percentage": 83
},
    "group4": {
    "name": "failed",
    "count": 2,
    "percentage": 1
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.647",
        "ok": "0.643",
        "ko": "0.003"
    }
}
    },"req_get-customer-in-bfc70": {
        type: "REQUEST",
        name: "GET customer-info",
path: "GET customer-info",
pathFormatted: "req_get-customer-in-bfc70",
stats: {
    "name": "GET customer-info",
    "numberOfRequests": {
        "total": "375",
        "ok": "375",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "9",
        "ok": "9",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1348",
        "ok": "1348",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "91",
        "ok": "91",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "209",
        "ok": "209",
        "ko": "-"
    },
    "percentiles1": {
        "total": "37",
        "ok": "37",
        "ko": "-"
    },
    "percentiles2": {
        "total": "61",
        "ok": "61",
        "ko": "-"
    },
    "percentiles3": {
        "total": "295",
        "ok": "295",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1329",
        "ok": "1329",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 361,
    "percentage": 96
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 4,
    "percentage": 1
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 10,
    "percentage": 3
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.647",
        "ok": "0.647",
        "ko": "-"
    }
}
    },"req_get-helixs-sync-9b1f7": {
        type: "REQUEST",
        name: "GET helixs-sync",
path: "GET helixs-sync",
pathFormatted: "req_get-helixs-sync-9b1f7",
stats: {
    "name": "GET helixs-sync",
    "numberOfRequests": {
        "total": "375",
        "ok": "375",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "9",
        "ok": "9",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1330",
        "ok": "1330",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "61",
        "ok": "61",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "107",
        "ok": "107",
        "ko": "-"
    },
    "percentiles1": {
        "total": "35",
        "ok": "35",
        "ko": "-"
    },
    "percentiles2": {
        "total": "53",
        "ok": "53",
        "ko": "-"
    },
    "percentiles3": {
        "total": "181",
        "ok": "181",
        "ko": "-"
    },
    "percentiles4": {
        "total": "480",
        "ok": "480",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 372,
    "percentage": 99
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 2,
    "percentage": 1
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.647",
        "ok": "0.647",
        "ko": "-"
    }
}
    },"req_post-sync-66165": {
        type: "REQUEST",
        name: "POST SYNC",
path: "POST SYNC",
pathFormatted: "req_post-sync-66165",
stats: {
    "name": "POST SYNC",
    "numberOfRequests": {
        "total": "375",
        "ok": "170",
        "ko": "205"
    },
    "minResponseTime": {
        "total": "64",
        "ok": "1348",
        "ko": "64"
    },
    "maxResponseTime": {
        "total": "60008",
        "ok": "59906",
        "ko": "60008"
    },
    "meanResponseTime": {
        "total": "45601",
        "ok": "28590",
        "ko": "59708"
    },
    "standardDeviation": {
        "total": "19710",
        "ok": "17510",
        "ko": "4176"
    },
    "percentiles1": {
        "total": "60000",
        "ok": "24415",
        "ko": "60000"
    },
    "percentiles2": {
        "total": "60000",
        "ok": "45216",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "60001",
        "ok": "56564",
        "ko": "60002"
    },
    "percentiles4": {
        "total": "60003",
        "ok": "59421",
        "ko": "60006"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 170,
    "percentage": 45
},
    "group4": {
    "name": "failed",
    "count": 205,
    "percentage": 55
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.647",
        "ok": "0.293",
        "ko": "0.353"
    }
}
    },"req_get-all-docs--2-9867e": {
        type: "REQUEST",
        name: "GET ALL_DOCS #2",
path: "GET ALL_DOCS #2",
pathFormatted: "req_get-all-docs--2-9867e",
stats: {
    "name": "GET ALL_DOCS #2",
    "numberOfRequests": {
        "total": "375",
        "ok": "375",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "43",
        "ok": "43",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "7305",
        "ok": "7305",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1018",
        "ok": "1018",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "1033",
        "ok": "1033",
        "ko": "-"
    },
    "percentiles1": {
        "total": "755",
        "ok": "752",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1119",
        "ok": "1119",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2813",
        "ok": "2813",
        "ko": "-"
    },
    "percentiles4": {
        "total": "6389",
        "ok": "6389",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 98,
    "percentage": 26
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 163,
    "percentage": 43
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 114,
    "percentage": 30
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.647",
        "ok": "0.647",
        "ko": "-"
    }
}
    },"req_get-info-43845": {
        type: "REQUEST",
        name: "GET INFO",
path: "GET INFO",
pathFormatted: "req_get-info-43845",
stats: {
    "name": "GET INFO",
    "numberOfRequests": {
        "total": "375",
        "ok": "375",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "8",
        "ok": "8",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2418",
        "ok": "2418",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "54",
        "ok": "54",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "176",
        "ok": "176",
        "ko": "-"
    },
    "percentiles1": {
        "total": "23",
        "ok": "23",
        "ko": "-"
    },
    "percentiles2": {
        "total": "43",
        "ok": "43",
        "ko": "-"
    },
    "percentiles3": {
        "total": "132",
        "ok": "132",
        "ko": "-"
    },
    "percentiles4": {
        "total": "473",
        "ok": "473",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 371,
    "percentage": 99
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 1,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 3,
    "percentage": 1
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.647",
        "ok": "0.647",
        "ko": "-"
    }
}
    },"req_get-health-chec-caf6c": {
        type: "REQUEST",
        name: "GET HEALTH-CHECK",
path: "GET HEALTH-CHECK",
pathFormatted: "req_get-health-chec-caf6c",
stats: {
    "name": "GET HEALTH-CHECK",
    "numberOfRequests": {
        "total": "375",
        "ok": "375",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "52",
        "ok": "52",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "725",
        "ok": "725",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "74",
        "ok": "74",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "47",
        "ok": "47",
        "ko": "-"
    },
    "percentiles1": {
        "total": "63",
        "ok": "63",
        "ko": "-"
    },
    "percentiles2": {
        "total": "75",
        "ok": "75",
        "ko": "-"
    },
    "percentiles3": {
        "total": "112",
        "ok": "112",
        "ko": "-"
    },
    "percentiles4": {
        "total": "268",
        "ok": "268",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 374,
    "percentage": 100
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 1,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.647",
        "ok": "0.647",
        "ko": "-"
    }
}
    },"req_get-database--2-e526d": {
        type: "REQUEST",
        name: "GET Database #2",
path: "GET Database #2",
pathFormatted: "req_get-database--2-e526d",
stats: {
    "name": "GET Database #2",
    "numberOfRequests": {
        "total": "375",
        "ok": "375",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "8",
        "ok": "8",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "416",
        "ok": "416",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "37",
        "ok": "37",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "42",
        "ok": "42",
        "ko": "-"
    },
    "percentiles1": {
        "total": "27",
        "ok": "26",
        "ko": "-"
    },
    "percentiles2": {
        "total": "39",
        "ok": "39",
        "ko": "-"
    },
    "percentiles3": {
        "total": "94",
        "ok": "94",
        "ko": "-"
    },
    "percentiles4": {
        "total": "239",
        "ok": "239",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 375,
    "percentage": 100
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.647",
        "ok": "0.647",
        "ko": "-"
    }
}
    },"req_changes-feed-4d6ae": {
        type: "REQUEST",
        name: "CHANGES FEED",
path: "CHANGES FEED",
pathFormatted: "req_changes-feed-4d6ae",
stats: {
    "name": "CHANGES FEED",
    "numberOfRequests": {
        "total": "375",
        "ok": "375",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "75",
        "ok": "75",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "61652",
        "ok": "61652",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "50665",
        "ok": "50665",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "18571",
        "ok": "18571",
        "ko": "-"
    },
    "percentiles1": {
        "total": "60025",
        "ok": "60025",
        "ko": "-"
    },
    "percentiles2": {
        "total": "60039",
        "ok": "60039",
        "ko": "-"
    },
    "percentiles3": {
        "total": "60126",
        "ok": "60126",
        "ko": "-"
    },
    "percentiles4": {
        "total": "60375",
        "ok": "60375",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 1,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 3,
    "percentage": 1
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 371,
    "percentage": 99
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.647",
        "ok": "0.647",
        "ko": "-"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
