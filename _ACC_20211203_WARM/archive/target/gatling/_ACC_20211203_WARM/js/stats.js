var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "8099",
        "ok": "7682",
        "ko": "417"
    },
    "minResponseTime": {
        "total": "4",
        "ok": "4",
        "ko": "13"
    },
    "maxResponseTime": {
        "total": "61198",
        "ok": "61198",
        "ko": "60048"
    },
    "meanResponseTime": {
        "total": "7786",
        "ok": "6342",
        "ko": "34379"
    },
    "standardDeviation": {
        "total": "18034",
        "ok": "16112",
        "ko": "28090"
    },
    "percentiles1": {
        "total": "80",
        "ok": "77",
        "ko": "60000"
    },
    "percentiles2": {
        "total": "1338",
        "ok": "888",
        "ko": "60000"
    },
    "percentiles3": {
        "total": "60030",
        "ok": "60033",
        "ko": "60001"
    },
    "percentiles4": {
        "total": "60100",
        "ok": "60103",
        "ko": "60005"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 5538,
    "percentage": 68
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 280,
    "percentage": 3
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1864,
    "percentage": 23
},
    "group4": {
    "name": "failed",
    "count": 417,
    "percentage": 5
},
    "meanNumberOfRequestsPerSecond": {
        "total": "12.052",
        "ok": "11.432",
        "ko": "0.621"
    }
},
contents: {
"req_get-landing-pag-93979": {
        type: "REQUEST",
        name: "GET landing page",
path: "GET landing page",
pathFormatted: "req_get-landing-pag-93979",
stats: {
    "name": "GET landing page",
    "numberOfRequests": {
        "total": "675",
        "ok": "675",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "8",
        "ok": "8",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "557",
        "ok": "557",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "55",
        "ok": "55",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "65",
        "ok": "65",
        "ko": "-"
    },
    "percentiles1": {
        "total": "30",
        "ok": "30",
        "ko": "-"
    },
    "percentiles2": {
        "total": "66",
        "ok": "66",
        "ko": "-"
    },
    "percentiles3": {
        "total": "191",
        "ok": "191",
        "ko": "-"
    },
    "percentiles4": {
        "total": "338",
        "ok": "338",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 674,
    "percentage": 100
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 1,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.004",
        "ok": "1.004",
        "ko": "-"
    }
}
    },"req_get-database-05cff": {
        type: "REQUEST",
        name: "GET Database",
path: "GET Database",
pathFormatted: "req_get-database-05cff",
stats: {
    "name": "GET Database",
    "numberOfRequests": {
        "total": "675",
        "ok": "674",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "12",
        "ok": "12",
        "ko": "74"
    },
    "maxResponseTime": {
        "total": "944",
        "ok": "944",
        "ko": "74"
    },
    "meanResponseTime": {
        "total": "98",
        "ok": "98",
        "ko": "74"
    },
    "standardDeviation": {
        "total": "96",
        "ok": "96",
        "ko": "0"
    },
    "percentiles1": {
        "total": "64",
        "ok": "63",
        "ko": "74"
    },
    "percentiles2": {
        "total": "124",
        "ok": "124",
        "ko": "74"
    },
    "percentiles3": {
        "total": "286",
        "ok": "286",
        "ko": "74"
    },
    "percentiles4": {
        "total": "434",
        "ok": "434",
        "ko": "74"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 670,
    "percentage": 99
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 4,
    "percentage": 1
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.004",
        "ok": "1.003",
        "ko": "0.001"
    }
}
    },"req_get-suppressed--91726": {
        type: "REQUEST",
        name: "GET suppressed-ui",
path: "GET suppressed-ui",
pathFormatted: "req_get-suppressed--91726",
stats: {
    "name": "GET suppressed-ui",
    "numberOfRequests": {
        "total": "675",
        "ok": "675",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "8",
        "ok": "8",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "371",
        "ok": "371",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "68",
        "ok": "68",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "67",
        "ok": "67",
        "ko": "-"
    },
    "percentiles1": {
        "total": "43",
        "ok": "43",
        "ko": "-"
    },
    "percentiles2": {
        "total": "83",
        "ok": "83",
        "ko": "-"
    },
    "percentiles3": {
        "total": "213",
        "ok": "213",
        "ko": "-"
    },
    "percentiles4": {
        "total": "302",
        "ok": "302",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 675,
    "percentage": 100
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.004",
        "ok": "1.004",
        "ko": "-"
    }
}
    },"req_post-sync-custo-6f7e3": {
        type: "REQUEST",
        name: "POST sync-customer-info",
path: "POST sync-customer-info",
pathFormatted: "req_post-sync-custo-6f7e3",
stats: {
    "name": "POST sync-customer-info",
    "numberOfRequests": {
        "total": "675",
        "ok": "611",
        "ko": "64"
    },
    "minResponseTime": {
        "total": "15",
        "ok": "199",
        "ko": "15"
    },
    "maxResponseTime": {
        "total": "60000",
        "ok": "50783",
        "ko": "60000"
    },
    "meanResponseTime": {
        "total": "8823",
        "ok": "9492",
        "ko": "2436"
    },
    "standardDeviation": {
        "total": "10245",
        "ok": "10211",
        "ko": "8155"
    },
    "percentiles1": {
        "total": "5331",
        "ok": "5941",
        "ko": "40"
    },
    "percentiles2": {
        "total": "11785",
        "ok": "12812",
        "ko": "107"
    },
    "percentiles3": {
        "total": "32887",
        "ok": "33624",
        "ko": "11581"
    },
    "percentiles4": {
        "total": "44260",
        "ok": "43997",
        "ko": "29613"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 23,
    "percentage": 3
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 68,
    "percentage": 10
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 520,
    "percentage": 77
},
    "group4": {
    "name": "failed",
    "count": 64,
    "percentage": 9
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.004",
        "ok": "0.909",
        "ko": "0.095"
    }
}
    },"req_get-customer-in-bfc70": {
        type: "REQUEST",
        name: "GET customer-info",
path: "GET customer-info",
pathFormatted: "req_get-customer-in-bfc70",
stats: {
    "name": "GET customer-info",
    "numberOfRequests": {
        "total": "675",
        "ok": "674",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "9",
        "ok": "9",
        "ko": "61"
    },
    "maxResponseTime": {
        "total": "483",
        "ok": "483",
        "ko": "61"
    },
    "meanResponseTime": {
        "total": "76",
        "ok": "76",
        "ko": "61"
    },
    "standardDeviation": {
        "total": "75",
        "ok": "75",
        "ko": "0"
    },
    "percentiles1": {
        "total": "43",
        "ok": "43",
        "ko": "61"
    },
    "percentiles2": {
        "total": "102",
        "ok": "103",
        "ko": "61"
    },
    "percentiles3": {
        "total": "232",
        "ok": "232",
        "ko": "61"
    },
    "percentiles4": {
        "total": "337",
        "ok": "337",
        "ko": "61"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 674,
    "percentage": 100
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.004",
        "ok": "1.003",
        "ko": "0.001"
    }
}
    },"req_get-helixs-sync-9b1f7": {
        type: "REQUEST",
        name: "GET helixs-sync",
path: "GET helixs-sync",
pathFormatted: "req_get-helixs-sync-9b1f7",
stats: {
    "name": "GET helixs-sync",
    "numberOfRequests": {
        "total": "675",
        "ok": "674",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "8",
        "ok": "8",
        "ko": "71"
    },
    "maxResponseTime": {
        "total": "1696",
        "ok": "1696",
        "ko": "71"
    },
    "meanResponseTime": {
        "total": "73",
        "ok": "73",
        "ko": "71"
    },
    "standardDeviation": {
        "total": "104",
        "ok": "104",
        "ko": "0"
    },
    "percentiles1": {
        "total": "43",
        "ok": "42",
        "ko": "71"
    },
    "percentiles2": {
        "total": "94",
        "ok": "94",
        "ko": "71"
    },
    "percentiles3": {
        "total": "218",
        "ok": "218",
        "ko": "71"
    },
    "percentiles4": {
        "total": "369",
        "ok": "369",
        "ko": "71"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 671,
    "percentage": 99
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 1,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 2,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.004",
        "ok": "1.003",
        "ko": "0.001"
    }
}
    },"req_post-sync-66165": {
        type: "REQUEST",
        name: "POST SYNC",
path: "POST SYNC",
pathFormatted: "req_post-sync-66165",
stats: {
    "name": "POST SYNC",
    "numberOfRequests": {
        "total": "675",
        "ok": "327",
        "ko": "348"
    },
    "minResponseTime": {
        "total": "13",
        "ok": "723",
        "ko": "13"
    },
    "maxResponseTime": {
        "total": "60048",
        "ok": "59726",
        "ko": "60048"
    },
    "meanResponseTime": {
        "total": "32670",
        "ok": "24073",
        "ko": "40747"
    },
    "standardDeviation": {
        "total": "23553",
        "ok": "16414",
        "ko": "26233"
    },
    "percentiles1": {
        "total": "29022",
        "ok": "21647",
        "ko": "60000"
    },
    "percentiles2": {
        "total": "60000",
        "ok": "32418",
        "ko": "60000"
    },
    "percentiles3": {
        "total": "60001",
        "ok": "57386",
        "ko": "60002"
    },
    "percentiles4": {
        "total": "60004",
        "ok": "58949",
        "ko": "60005"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 1,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 326,
    "percentage": 48
},
    "group4": {
    "name": "failed",
    "count": 348,
    "percentage": 52
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.004",
        "ok": "0.487",
        "ko": "0.518"
    }
}
    },"req_get-all-docs--2-9867e": {
        type: "REQUEST",
        name: "GET ALL_DOCS #2",
path: "GET ALL_DOCS #2",
pathFormatted: "req_get-all-docs--2-9867e",
stats: {
    "name": "GET ALL_DOCS #2",
    "numberOfRequests": {
        "total": "675",
        "ok": "674",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "24",
        "ok": "35",
        "ko": "24"
    },
    "maxResponseTime": {
        "total": "49163",
        "ok": "49163",
        "ko": "24"
    },
    "meanResponseTime": {
        "total": "2349",
        "ok": "2352",
        "ko": "24"
    },
    "standardDeviation": {
        "total": "4342",
        "ok": "4344",
        "ko": "0"
    },
    "percentiles1": {
        "total": "1012",
        "ok": "1015",
        "ko": "24"
    },
    "percentiles2": {
        "total": "2337",
        "ok": "2339",
        "ko": "24"
    },
    "percentiles3": {
        "total": "9154",
        "ok": "9170",
        "ko": "24"
    },
    "percentiles4": {
        "total": "21291",
        "ok": "21352",
        "ko": "24"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 137,
    "percentage": 20
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 198,
    "percentage": 29
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 339,
    "percentage": 50
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.004",
        "ok": "1.003",
        "ko": "0.001"
    }
}
    },"req_get-info-43845": {
        type: "REQUEST",
        name: "GET INFO",
path: "GET INFO",
pathFormatted: "req_get-info-43845",
stats: {
    "name": "GET INFO",
    "numberOfRequests": {
        "total": "675",
        "ok": "675",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4",
        "ok": "4",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1890",
        "ok": "1890",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "63",
        "ok": "63",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "98",
        "ok": "98",
        "ko": "-"
    },
    "percentiles1": {
        "total": "40",
        "ok": "40",
        "ko": "-"
    },
    "percentiles2": {
        "total": "71",
        "ok": "71",
        "ko": "-"
    },
    "percentiles3": {
        "total": "206",
        "ok": "206",
        "ko": "-"
    },
    "percentiles4": {
        "total": "341",
        "ok": "341",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 673,
    "percentage": 100
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 1,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.004",
        "ok": "1.004",
        "ko": "-"
    }
}
    },"req_get-health-chec-caf6c": {
        type: "REQUEST",
        name: "GET HEALTH-CHECK",
path: "GET HEALTH-CHECK",
pathFormatted: "req_get-health-chec-caf6c",
stats: {
    "name": "GET HEALTH-CHECK",
    "numberOfRequests": {
        "total": "675",
        "ok": "675",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "51",
        "ok": "51",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2230",
        "ok": "2230",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "98",
        "ok": "98",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "157",
        "ok": "157",
        "ko": "-"
    },
    "percentiles1": {
        "total": "71",
        "ok": "71",
        "ko": "-"
    },
    "percentiles2": {
        "total": "89",
        "ok": "89",
        "ko": "-"
    },
    "percentiles3": {
        "total": "190",
        "ok": "190",
        "ko": "-"
    },
    "percentiles4": {
        "total": "402",
        "ok": "402",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 670,
    "percentage": 99
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 1,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 4,
    "percentage": 1
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.004",
        "ok": "1.004",
        "ko": "-"
    }
}
    },"req_get-database--2-e526d": {
        type: "REQUEST",
        name: "GET Database #2",
path: "GET Database #2",
pathFormatted: "req_get-database--2-e526d",
stats: {
    "name": "GET Database #2",
    "numberOfRequests": {
        "total": "675",
        "ok": "674",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "8",
        "ok": "8",
        "ko": "41"
    },
    "maxResponseTime": {
        "total": "1576",
        "ok": "1576",
        "ko": "41"
    },
    "meanResponseTime": {
        "total": "58",
        "ok": "58",
        "ko": "41"
    },
    "standardDeviation": {
        "total": "85",
        "ok": "85",
        "ko": "0"
    },
    "percentiles1": {
        "total": "38",
        "ok": "37",
        "ko": "41"
    },
    "percentiles2": {
        "total": "62",
        "ok": "62",
        "ko": "41"
    },
    "percentiles3": {
        "total": "172",
        "ok": "172",
        "ko": "41"
    },
    "percentiles4": {
        "total": "330",
        "ok": "330",
        "ko": "41"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 671,
    "percentage": 99
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 2,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.004",
        "ok": "1.003",
        "ko": "0.001"
    }
}
    },"req_changes-feed-4d6ae": {
        type: "REQUEST",
        name: "CHANGES FEED",
path: "CHANGES FEED",
pathFormatted: "req_changes-feed-4d6ae",
stats: {
    "name": "CHANGES FEED",
    "numberOfRequests": {
        "total": "674",
        "ok": "674",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "534",
        "ok": "534",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "61198",
        "ok": "61198",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "49062",
        "ok": "49062",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "19937",
        "ok": "19937",
        "ko": "-"
    },
    "percentiles1": {
        "total": "60038",
        "ok": "60038",
        "ko": "-"
    },
    "percentiles2": {
        "total": "60065",
        "ok": "60065",
        "ko": "-"
    },
    "percentiles3": {
        "total": "60155",
        "ok": "60155",
        "ko": "-"
    },
    "percentiles4": {
        "total": "60265",
        "ok": "60265",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 3,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 671,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.003",
        "ok": "1.003",
        "ko": "-"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
