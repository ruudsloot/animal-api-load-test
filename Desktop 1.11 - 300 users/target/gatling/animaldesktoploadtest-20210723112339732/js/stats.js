var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "6000",
        "ok": "5981",
        "ko": "19"
    },
    "minResponseTime": {
        "total": "7",
        "ok": "7",
        "ko": "205"
    },
    "maxResponseTime": {
        "total": "61971",
        "ok": "61971",
        "ko": "60005"
    },
    "meanResponseTime": {
        "total": "7353",
        "ok": "7266",
        "ko": "34900"
    },
    "standardDeviation": {
        "total": "17436",
        "ok": "17315",
        "ko": "29433"
    },
    "percentiles1": {
        "total": "214",
        "ok": "210",
        "ko": "60000"
    },
    "percentiles2": {
        "total": "2627",
        "ok": "2573",
        "ko": "60000"
    },
    "percentiles3": {
        "total": "60077",
        "ok": "60077",
        "ko": "60001"
    },
    "percentiles4": {
        "total": "60330",
        "ok": "60331",
        "ko": "60004"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 3824,
    "percentage": 64
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 309,
    "percentage": 5
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1848,
    "percentage": 31
},
    "group4": {
    "name": "failed",
    "count": 19,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "18.072",
        "ok": "18.015",
        "ko": "0.057"
    }
},
contents: {
"req_get-landing-pag-93979": {
        type: "REQUEST",
        name: "GET landing page",
path: "GET landing page",
pathFormatted: "req_get-landing-pag-93979",
stats: {
    "name": "GET landing page",
    "numberOfRequests": {
        "total": "500",
        "ok": "500",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "7",
        "ok": "7",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "741",
        "ok": "741",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "48",
        "ok": "48",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "90",
        "ok": "90",
        "ko": "-"
    },
    "percentiles1": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "percentiles2": {
        "total": "45",
        "ok": "45",
        "ko": "-"
    },
    "percentiles3": {
        "total": "190",
        "ok": "190",
        "ko": "-"
    },
    "percentiles4": {
        "total": "523",
        "ok": "523",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 494,
    "percentage": 99
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 6,
    "percentage": 1
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.506",
        "ok": "1.506",
        "ko": "-"
    }
}
    },"req_get-database-05cff": {
        type: "REQUEST",
        name: "GET Database",
path: "GET Database",
pathFormatted: "req_get-database-05cff",
stats: {
    "name": "GET Database",
    "numberOfRequests": {
        "total": "500",
        "ok": "496",
        "ko": "4"
    },
    "minResponseTime": {
        "total": "17",
        "ok": "17",
        "ko": "205"
    },
    "maxResponseTime": {
        "total": "2515",
        "ok": "2515",
        "ko": "608"
    },
    "meanResponseTime": {
        "total": "228",
        "ok": "227",
        "ko": "411"
    },
    "standardDeviation": {
        "total": "250",
        "ok": "250",
        "ko": "143"
    },
    "percentiles1": {
        "total": "159",
        "ok": "156",
        "ko": "416"
    },
    "percentiles2": {
        "total": "324",
        "ok": "321",
        "ko": "475"
    },
    "percentiles3": {
        "total": "539",
        "ok": "537",
        "ko": "581"
    },
    "percentiles4": {
        "total": "863",
        "ok": "879",
        "ko": "603"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 457,
    "percentage": 91
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 34,
    "percentage": 7
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 5,
    "percentage": 1
},
    "group4": {
    "name": "failed",
    "count": 4,
    "percentage": 1
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.506",
        "ok": "1.494",
        "ko": "0.012"
    }
}
    },"req_get-suppressed--91726": {
        type: "REQUEST",
        name: "GET suppressed-ui",
path: "GET suppressed-ui",
pathFormatted: "req_get-suppressed--91726",
stats: {
    "name": "GET suppressed-ui",
    "numberOfRequests": {
        "total": "500",
        "ok": "500",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "12",
        "ok": "12",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1468",
        "ok": "1468",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "208",
        "ok": "208",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "185",
        "ok": "185",
        "ko": "-"
    },
    "percentiles1": {
        "total": "152",
        "ok": "152",
        "ko": "-"
    },
    "percentiles2": {
        "total": "309",
        "ok": "309",
        "ko": "-"
    },
    "percentiles3": {
        "total": "524",
        "ok": "524",
        "ko": "-"
    },
    "percentiles4": {
        "total": "690",
        "ok": "690",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 466,
    "percentage": 93
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 31,
    "percentage": 6
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 3,
    "percentage": 1
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.506",
        "ok": "1.506",
        "ko": "-"
    }
}
    },"req_post-sync-custo-6f7e3": {
        type: "REQUEST",
        name: "POST sync-customer-info",
path: "POST sync-customer-info",
pathFormatted: "req_post-sync-custo-6f7e3",
stats: {
    "name": "POST sync-customer-info",
    "numberOfRequests": {
        "total": "500",
        "ok": "500",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "292",
        "ok": "292",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "53131",
        "ok": "53131",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "6614",
        "ok": "6614",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "6743",
        "ok": "6743",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3376",
        "ok": "3376",
        "ko": "-"
    },
    "percentiles2": {
        "total": "9645",
        "ok": "9645",
        "ko": "-"
    },
    "percentiles3": {
        "total": "20160",
        "ok": "20160",
        "ko": "-"
    },
    "percentiles4": {
        "total": "28069",
        "ok": "28069",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 7,
    "percentage": 1
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 29,
    "percentage": 6
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 464,
    "percentage": 93
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.506",
        "ok": "1.506",
        "ko": "-"
    }
}
    },"req_get-customer-in-bfc70": {
        type: "REQUEST",
        name: "GET customer-info",
path: "GET customer-info",
pathFormatted: "req_get-customer-in-bfc70",
stats: {
    "name": "GET customer-info",
    "numberOfRequests": {
        "total": "500",
        "ok": "500",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "11",
        "ok": "11",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1075",
        "ok": "1075",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "217",
        "ok": "217",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "188",
        "ok": "188",
        "ko": "-"
    },
    "percentiles1": {
        "total": "160",
        "ok": "160",
        "ko": "-"
    },
    "percentiles2": {
        "total": "325",
        "ok": "325",
        "ko": "-"
    },
    "percentiles3": {
        "total": "558",
        "ok": "558",
        "ko": "-"
    },
    "percentiles4": {
        "total": "928",
        "ok": "928",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 463,
    "percentage": 93
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 34,
    "percentage": 7
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 3,
    "percentage": 1
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.506",
        "ok": "1.506",
        "ko": "-"
    }
}
    },"req_get-helixs-sync-9b1f7": {
        type: "REQUEST",
        name: "GET helixs-sync",
path: "GET helixs-sync",
pathFormatted: "req_get-helixs-sync-9b1f7",
stats: {
    "name": "GET helixs-sync",
    "numberOfRequests": {
        "total": "500",
        "ok": "496",
        "ko": "4"
    },
    "minResponseTime": {
        "total": "11",
        "ok": "11",
        "ko": "266"
    },
    "maxResponseTime": {
        "total": "1076",
        "ok": "1076",
        "ko": "433"
    },
    "meanResponseTime": {
        "total": "222",
        "ok": "221",
        "ko": "363"
    },
    "standardDeviation": {
        "total": "196",
        "ok": "196",
        "ko": "61"
    },
    "percentiles1": {
        "total": "153",
        "ok": "150",
        "ko": "376"
    },
    "percentiles2": {
        "total": "336",
        "ok": "335",
        "ko": "398"
    },
    "percentiles3": {
        "total": "528",
        "ok": "530",
        "ko": "426"
    },
    "percentiles4": {
        "total": "969",
        "ok": "969",
        "ko": "432"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 450,
    "percentage": 90
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 42,
    "percentage": 8
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 4,
    "percentage": 1
},
    "group4": {
    "name": "failed",
    "count": 4,
    "percentage": 1
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.506",
        "ok": "1.494",
        "ko": "0.012"
    }
}
    },"req_post-sync-66165": {
        type: "REQUEST",
        name: "POST SYNC",
path: "POST SYNC",
pathFormatted: "req_post-sync-66165",
stats: {
    "name": "POST SYNC",
    "numberOfRequests": {
        "total": "500",
        "ok": "489",
        "ko": "11"
    },
    "minResponseTime": {
        "total": "973",
        "ok": "973",
        "ko": "60000"
    },
    "maxResponseTime": {
        "total": "60005",
        "ok": "59520",
        "ko": "60005"
    },
    "meanResponseTime": {
        "total": "16551",
        "ok": "15574",
        "ko": "60001"
    },
    "standardDeviation": {
        "total": "17011",
        "ok": "15889",
        "ko": "1"
    },
    "percentiles1": {
        "total": "8256",
        "ok": "8085",
        "ko": "60000"
    },
    "percentiles2": {
        "total": "20389",
        "ok": "18571",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "53945",
        "ok": "52479",
        "ko": "60003"
    },
    "percentiles4": {
        "total": "60000",
        "ok": "58090",
        "ko": "60005"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 1,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 488,
    "percentage": 98
},
    "group4": {
    "name": "failed",
    "count": 11,
    "percentage": 2
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.506",
        "ok": "1.473",
        "ko": "0.033"
    }
}
    },"req_get-all-docs--2-9867e": {
        type: "REQUEST",
        name: "GET ALL_DOCS #2",
path: "GET ALL_DOCS #2",
pathFormatted: "req_get-all-docs--2-9867e",
stats: {
    "name": "GET ALL_DOCS #2",
    "numberOfRequests": {
        "total": "500",
        "ok": "500",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "47",
        "ok": "47",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "28970",
        "ok": "28970",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3754",
        "ok": "3754",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "4086",
        "ok": "4086",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1885",
        "ok": "1885",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5366",
        "ok": "5366",
        "ko": "-"
    },
    "percentiles3": {
        "total": "12350",
        "ok": "12350",
        "ko": "-"
    },
    "percentiles4": {
        "total": "16425",
        "ok": "16425",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 34,
    "percentage": 7
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 91,
    "percentage": 18
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 375,
    "percentage": 75
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.506",
        "ok": "1.506",
        "ko": "-"
    }
}
    },"req_get-info-43845": {
        type: "REQUEST",
        name: "GET INFO",
path: "GET INFO",
pathFormatted: "req_get-info-43845",
stats: {
    "name": "GET INFO",
    "numberOfRequests": {
        "total": "500",
        "ok": "500",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "9",
        "ok": "9",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1401",
        "ok": "1401",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "57",
        "ok": "57",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "125",
        "ok": "125",
        "ko": "-"
    },
    "percentiles1": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "percentiles2": {
        "total": "44",
        "ok": "44",
        "ko": "-"
    },
    "percentiles3": {
        "total": "165",
        "ok": "165",
        "ko": "-"
    },
    "percentiles4": {
        "total": "751",
        "ok": "751",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 489,
    "percentage": 98
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 9,
    "percentage": 2
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 2,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.506",
        "ok": "1.506",
        "ko": "-"
    }
}
    },"req_get-health-chec-caf6c": {
        type: "REQUEST",
        name: "GET HEALTH-CHECK",
path: "GET HEALTH-CHECK",
pathFormatted: "req_get-health-chec-caf6c",
stats: {
    "name": "GET HEALTH-CHECK",
    "numberOfRequests": {
        "total": "500",
        "ok": "500",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "56",
        "ok": "56",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2209",
        "ok": "2209",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "105",
        "ok": "105",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "138",
        "ok": "138",
        "ko": "-"
    },
    "percentiles1": {
        "total": "73",
        "ok": "73",
        "ko": "-"
    },
    "percentiles2": {
        "total": "91",
        "ok": "91",
        "ko": "-"
    },
    "percentiles3": {
        "total": "208",
        "ok": "208",
        "ko": "-"
    },
    "percentiles4": {
        "total": "692",
        "ok": "692",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 488,
    "percentage": 98
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 11,
    "percentage": 2
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.506",
        "ok": "1.506",
        "ko": "-"
    }
}
    },"req_get-database--2-e526d": {
        type: "REQUEST",
        name: "GET Database #2",
path: "GET Database #2",
pathFormatted: "req_get-database--2-e526d",
stats: {
    "name": "GET Database #2",
    "numberOfRequests": {
        "total": "500",
        "ok": "500",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "9",
        "ok": "9",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2472",
        "ok": "2472",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "177",
        "ok": "177",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "208",
        "ok": "208",
        "ko": "-"
    },
    "percentiles1": {
        "total": "100",
        "ok": "100",
        "ko": "-"
    },
    "percentiles2": {
        "total": "255",
        "ok": "255",
        "ko": "-"
    },
    "percentiles3": {
        "total": "495",
        "ok": "495",
        "ko": "-"
    },
    "percentiles4": {
        "total": "907",
        "ok": "907",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 476,
    "percentage": 95
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 21,
    "percentage": 4
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 3,
    "percentage": 1
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.506",
        "ok": "1.506",
        "ko": "-"
    }
}
    },"req_changes-feed-4d6ae": {
        type: "REQUEST",
        name: "CHANGES FEED",
path: "CHANGES FEED",
pathFormatted: "req_changes-feed-4d6ae",
stats: {
    "name": "CHANGES FEED",
    "numberOfRequests": {
        "total": "500",
        "ok": "500",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3585",
        "ok": "3585",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "61971",
        "ok": "61971",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "60057",
        "ok": "60057",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "2537",
        "ok": "2537",
        "ko": "-"
    },
    "percentiles1": {
        "total": "60099",
        "ok": "60099",
        "ko": "-"
    },
    "percentiles2": {
        "total": "60210",
        "ok": "60210",
        "ko": "-"
    },
    "percentiles3": {
        "total": "60471",
        "ok": "60471",
        "ko": "-"
    },
    "percentiles4": {
        "total": "61119",
        "ok": "61119",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 500,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.506",
        "ok": "1.506",
        "ko": "-"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
