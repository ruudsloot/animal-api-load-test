var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "4500",
        "ok": "4500",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "7",
        "ok": "7",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "60963",
        "ok": "60963",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "6054",
        "ok": "6054",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "16541",
        "ok": "16541",
        "ko": "-"
    },
    "percentiles1": {
        "total": "85",
        "ok": "85",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1060",
        "ok": "1060",
        "ko": "-"
    },
    "percentiles3": {
        "total": "60043",
        "ok": "60043",
        "ko": "-"
    },
    "percentiles4": {
        "total": "60160",
        "ok": "60160",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 3103,
    "percentage": 69
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 254,
    "percentage": 6
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1143,
    "percentage": 25
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "8.806",
        "ok": "8.806",
        "ko": "-"
    }
},
contents: {
"req_get-landing-pag-93979": {
        type: "REQUEST",
        name: "GET landing page",
path: "GET landing page",
pathFormatted: "req_get-landing-pag-93979",
stats: {
    "name": "GET landing page",
    "numberOfRequests": {
        "total": "375",
        "ok": "375",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "7",
        "ok": "7",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "297",
        "ok": "297",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "23",
        "ok": "23",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "26",
        "ok": "26",
        "ko": "-"
    },
    "percentiles1": {
        "total": "13",
        "ok": "13",
        "ko": "-"
    },
    "percentiles2": {
        "total": "27",
        "ok": "27",
        "ko": "-"
    },
    "percentiles3": {
        "total": "56",
        "ok": "56",
        "ko": "-"
    },
    "percentiles4": {
        "total": "132",
        "ok": "132",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 375,
    "percentage": 100
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.734",
        "ok": "0.734",
        "ko": "-"
    }
}
    },"req_get-database-05cff": {
        type: "REQUEST",
        name: "GET Database",
path: "GET Database",
pathFormatted: "req_get-database-05cff",
stats: {
    "name": "GET Database",
    "numberOfRequests": {
        "total": "375",
        "ok": "375",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "16",
        "ok": "16",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "579",
        "ok": "579",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "107",
        "ok": "107",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "101",
        "ok": "101",
        "ko": "-"
    },
    "percentiles1": {
        "total": "58",
        "ok": "58",
        "ko": "-"
    },
    "percentiles2": {
        "total": "145",
        "ok": "145",
        "ko": "-"
    },
    "percentiles3": {
        "total": "316",
        "ok": "316",
        "ko": "-"
    },
    "percentiles4": {
        "total": "418",
        "ok": "418",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 374,
    "percentage": 100
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 1,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.734",
        "ok": "0.734",
        "ko": "-"
    }
}
    },"req_get-suppressed--91726": {
        type: "REQUEST",
        name: "GET suppressed-ui",
path: "GET suppressed-ui",
pathFormatted: "req_get-suppressed--91726",
stats: {
    "name": "GET suppressed-ui",
    "numberOfRequests": {
        "total": "375",
        "ok": "375",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "11",
        "ok": "11",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "747",
        "ok": "747",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "103",
        "ok": "103",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "110",
        "ok": "110",
        "ko": "-"
    },
    "percentiles1": {
        "total": "50",
        "ok": "50",
        "ko": "-"
    },
    "percentiles2": {
        "total": "147",
        "ok": "147",
        "ko": "-"
    },
    "percentiles3": {
        "total": "329",
        "ok": "329",
        "ko": "-"
    },
    "percentiles4": {
        "total": "418",
        "ok": "418",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 373,
    "percentage": 99
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 2,
    "percentage": 1
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.734",
        "ok": "0.734",
        "ko": "-"
    }
}
    },"req_post-sync-custo-6f7e3": {
        type: "REQUEST",
        name: "POST sync-customer-info",
path: "POST sync-customer-info",
pathFormatted: "req_post-sync-custo-6f7e3",
stats: {
    "name": "POST sync-customer-info",
    "numberOfRequests": {
        "total": "375",
        "ok": "375",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "114",
        "ok": "114",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "20784",
        "ok": "20784",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3302",
        "ok": "3302",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "4134",
        "ok": "4134",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1256",
        "ok": "1256",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4414",
        "ok": "4414",
        "ko": "-"
    },
    "percentiles3": {
        "total": "12127",
        "ok": "12127",
        "ko": "-"
    },
    "percentiles4": {
        "total": "18477",
        "ok": "18477",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 24,
    "percentage": 6
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 122,
    "percentage": 33
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 229,
    "percentage": 61
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.734",
        "ok": "0.734",
        "ko": "-"
    }
}
    },"req_get-customer-in-bfc70": {
        type: "REQUEST",
        name: "GET customer-info",
path: "GET customer-info",
pathFormatted: "req_get-customer-in-bfc70",
stats: {
    "name": "GET customer-info",
    "numberOfRequests": {
        "total": "375",
        "ok": "375",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "9",
        "ok": "9",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "646",
        "ok": "646",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "108",
        "ok": "108",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "118",
        "ok": "118",
        "ko": "-"
    },
    "percentiles1": {
        "total": "48",
        "ok": "48",
        "ko": "-"
    },
    "percentiles2": {
        "total": "143",
        "ok": "143",
        "ko": "-"
    },
    "percentiles3": {
        "total": "362",
        "ok": "362",
        "ko": "-"
    },
    "percentiles4": {
        "total": "456",
        "ok": "456",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 373,
    "percentage": 99
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 2,
    "percentage": 1
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.734",
        "ok": "0.734",
        "ko": "-"
    }
}
    },"req_get-helixs-sync-9b1f7": {
        type: "REQUEST",
        name: "GET helixs-sync",
path: "GET helixs-sync",
pathFormatted: "req_get-helixs-sync-9b1f7",
stats: {
    "name": "GET helixs-sync",
    "numberOfRequests": {
        "total": "375",
        "ok": "375",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "9",
        "ok": "9",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "973",
        "ok": "973",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "109",
        "ok": "109",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "123",
        "ok": "123",
        "ko": "-"
    },
    "percentiles1": {
        "total": "44",
        "ok": "44",
        "ko": "-"
    },
    "percentiles2": {
        "total": "184",
        "ok": "184",
        "ko": "-"
    },
    "percentiles3": {
        "total": "363",
        "ok": "363",
        "ko": "-"
    },
    "percentiles4": {
        "total": "435",
        "ok": "435",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 373,
    "percentage": 99
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 2,
    "percentage": 1
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.734",
        "ok": "0.734",
        "ko": "-"
    }
}
    },"req_post-sync-66165": {
        type: "REQUEST",
        name: "POST SYNC",
path: "POST SYNC",
pathFormatted: "req_post-sync-66165",
stats: {
    "name": "POST SYNC",
    "numberOfRequests": {
        "total": "375",
        "ok": "375",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "912",
        "ok": "912",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "46020",
        "ok": "46020",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "6799",
        "ok": "6799",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "5389",
        "ok": "5389",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4531",
        "ok": "4531",
        "ko": "-"
    },
    "percentiles2": {
        "total": "9247",
        "ok": "9247",
        "ko": "-"
    },
    "percentiles3": {
        "total": "16412",
        "ok": "16412",
        "ko": "-"
    },
    "percentiles4": {
        "total": "23216",
        "ok": "23216",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 1,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 374,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.734",
        "ok": "0.734",
        "ko": "-"
    }
}
    },"req_get-all-docs--2-9867e": {
        type: "REQUEST",
        name: "GET ALL_DOCS #2",
path: "GET ALL_DOCS #2",
pathFormatted: "req_get-all-docs--2-9867e",
stats: {
    "name": "GET ALL_DOCS #2",
    "numberOfRequests": {
        "total": "375",
        "ok": "375",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "29",
        "ok": "29",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "28754",
        "ok": "28754",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1819",
        "ok": "1819",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "2562",
        "ok": "2562",
        "ko": "-"
    },
    "percentiles1": {
        "total": "868",
        "ok": "868",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1981",
        "ok": "1981",
        "ko": "-"
    },
    "percentiles3": {
        "total": "6803",
        "ok": "6803",
        "ko": "-"
    },
    "percentiles4": {
        "total": "10327",
        "ok": "10327",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 88,
    "percentage": 23
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 122,
    "percentage": 33
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 165,
    "percentage": 44
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.734",
        "ok": "0.734",
        "ko": "-"
    }
}
    },"req_get-info-43845": {
        type: "REQUEST",
        name: "GET INFO",
path: "GET INFO",
pathFormatted: "req_get-info-43845",
stats: {
    "name": "GET INFO",
    "numberOfRequests": {
        "total": "375",
        "ok": "375",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "10",
        "ok": "10",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "173",
        "ok": "173",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "26",
        "ok": "26",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "19",
        "ok": "19",
        "ko": "-"
    },
    "percentiles1": {
        "total": "20",
        "ok": "20",
        "ko": "-"
    },
    "percentiles2": {
        "total": "31",
        "ok": "31",
        "ko": "-"
    },
    "percentiles3": {
        "total": "60",
        "ok": "60",
        "ko": "-"
    },
    "percentiles4": {
        "total": "103",
        "ok": "103",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 375,
    "percentage": 100
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.734",
        "ok": "0.734",
        "ko": "-"
    }
}
    },"req_get-health-chec-caf6c": {
        type: "REQUEST",
        name: "GET HEALTH-CHECK",
path: "GET HEALTH-CHECK",
pathFormatted: "req_get-health-chec-caf6c",
stats: {
    "name": "GET HEALTH-CHECK",
    "numberOfRequests": {
        "total": "375",
        "ok": "375",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "56",
        "ok": "56",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "914",
        "ok": "914",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "86",
        "ok": "86",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "63",
        "ok": "63",
        "ko": "-"
    },
    "percentiles1": {
        "total": "71",
        "ok": "71",
        "ko": "-"
    },
    "percentiles2": {
        "total": "84",
        "ok": "84",
        "ko": "-"
    },
    "percentiles3": {
        "total": "153",
        "ok": "153",
        "ko": "-"
    },
    "percentiles4": {
        "total": "262",
        "ok": "262",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 373,
    "percentage": 99
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 2,
    "percentage": 1
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.734",
        "ok": "0.734",
        "ko": "-"
    }
}
    },"req_get-database--2-e526d": {
        type: "REQUEST",
        name: "GET Database #2",
path: "GET Database #2",
pathFormatted: "req_get-database--2-e526d",
stats: {
    "name": "GET Database #2",
    "numberOfRequests": {
        "total": "375",
        "ok": "375",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "10",
        "ok": "10",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "454",
        "ok": "454",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "86",
        "ok": "86",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "91",
        "ok": "91",
        "ko": "-"
    },
    "percentiles1": {
        "total": "42",
        "ok": "42",
        "ko": "-"
    },
    "percentiles2": {
        "total": "119",
        "ok": "119",
        "ko": "-"
    },
    "percentiles3": {
        "total": "285",
        "ok": "285",
        "ko": "-"
    },
    "percentiles4": {
        "total": "385",
        "ok": "385",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 375,
    "percentage": 100
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.734",
        "ok": "0.734",
        "ko": "-"
    }
}
    },"req_changes-feed-4d6ae": {
        type: "REQUEST",
        name: "CHANGES FEED",
path: "CHANGES FEED",
pathFormatted: "req_changes-feed-4d6ae",
stats: {
    "name": "CHANGES FEED",
    "numberOfRequests": {
        "total": "375",
        "ok": "375",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "60017",
        "ok": "60017",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "60963",
        "ok": "60963",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "60083",
        "ok": "60083",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "77",
        "ok": "77",
        "ko": "-"
    },
    "percentiles1": {
        "total": "60053",
        "ok": "60053",
        "ko": "-"
    },
    "percentiles2": {
        "total": "60114",
        "ok": "60114",
        "ko": "-"
    },
    "percentiles3": {
        "total": "60210",
        "ok": "60210",
        "ko": "-"
    },
    "percentiles4": {
        "total": "60317",
        "ok": "60317",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 375,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.734",
        "ok": "0.734",
        "ko": "-"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
