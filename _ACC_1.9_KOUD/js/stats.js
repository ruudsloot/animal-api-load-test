var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "495",
        "ok": "253",
        "ko": "242"
    },
    "minResponseTime": {
        "total": "1590",
        "ok": "1590",
        "ko": "60000"
    },
    "maxResponseTime": {
        "total": "60006",
        "ok": "59995",
        "ko": "60006"
    },
    "meanResponseTime": {
        "total": "43144",
        "ok": "27018",
        "ko": "60003"
    },
    "standardDeviation": {
        "total": "20191",
        "ok": "16302",
        "ko": "2"
    },
    "percentiles1": {
        "total": "58065",
        "ok": "25201",
        "ko": "60003"
    },
    "percentiles2": {
        "total": "60003",
        "ok": "38467",
        "ko": "60004"
    },
    "percentiles3": {
        "total": "60005",
        "ok": "56001",
        "ko": "60005"
    },
    "percentiles4": {
        "total": "60005",
        "ok": "58441",
        "ko": "60006"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 253,
    "percentage": 51
},
    "group4": {
    "name": "failed",
    "count": 242,
    "percentage": 49
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.117",
        "ok": "0.571",
        "ko": "0.546"
    }
},
contents: {
"req_animal-api-sync-9e54f": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_112168",
path: "Animal-api sync for customer NL_112168",
pathFormatted: "req_animal-api-sync-9e54f",
stats: {
    "name": "Animal-api sync for customer NL_112168",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21825",
        "ok": "21825",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "21825",
        "ok": "21825",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "21825",
        "ok": "21825",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "21825",
        "ok": "21825",
        "ko": "-"
    },
    "percentiles2": {
        "total": "21825",
        "ok": "21825",
        "ko": "-"
    },
    "percentiles3": {
        "total": "21825",
        "ok": "21825",
        "ko": "-"
    },
    "percentiles4": {
        "total": "21825",
        "ok": "21825",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-f5a2a": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_125092",
path: "Animal-api sync for customer NL_125092",
pathFormatted: "req_animal-api-sync-f5a2a",
stats: {
    "name": "Animal-api sync for customer NL_125092",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "19239",
        "ok": "19239",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "19239",
        "ok": "19239",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "19239",
        "ok": "19239",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "19239",
        "ok": "19239",
        "ko": "-"
    },
    "percentiles2": {
        "total": "19239",
        "ok": "19239",
        "ko": "-"
    },
    "percentiles3": {
        "total": "19239",
        "ok": "19239",
        "ko": "-"
    },
    "percentiles4": {
        "total": "19239",
        "ok": "19239",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-0f1c3": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_128277",
path: "Animal-api sync for customer NL_128277",
pathFormatted: "req_animal-api-sync-0f1c3",
stats: {
    "name": "Animal-api sync for customer NL_128277",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "37265",
        "ok": "37265",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "37265",
        "ok": "37265",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "37265",
        "ok": "37265",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "37265",
        "ok": "37265",
        "ko": "-"
    },
    "percentiles2": {
        "total": "37265",
        "ok": "37265",
        "ko": "-"
    },
    "percentiles3": {
        "total": "37265",
        "ok": "37265",
        "ko": "-"
    },
    "percentiles4": {
        "total": "37265",
        "ok": "37265",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-1d77d": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_109531",
path: "Animal-api sync for customer NL_109531",
pathFormatted: "req_animal-api-sync-1d77d",
stats: {
    "name": "Animal-api sync for customer NL_109531",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "14178",
        "ok": "14178",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "14178",
        "ok": "14178",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "14178",
        "ok": "14178",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "14178",
        "ok": "14178",
        "ko": "-"
    },
    "percentiles2": {
        "total": "14178",
        "ok": "14178",
        "ko": "-"
    },
    "percentiles3": {
        "total": "14178",
        "ok": "14178",
        "ko": "-"
    },
    "percentiles4": {
        "total": "14178",
        "ok": "14178",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e9ad2": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_121119",
path: "Animal-api sync for customer NL_121119",
pathFormatted: "req_animal-api-sync-e9ad2",
stats: {
    "name": "Animal-api sync for customer NL_121119",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "12855",
        "ok": "12855",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "12855",
        "ok": "12855",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "12855",
        "ok": "12855",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "12855",
        "ok": "12855",
        "ko": "-"
    },
    "percentiles2": {
        "total": "12855",
        "ok": "12855",
        "ko": "-"
    },
    "percentiles3": {
        "total": "12855",
        "ok": "12855",
        "ko": "-"
    },
    "percentiles4": {
        "total": "12855",
        "ok": "12855",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-86f14": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_121285",
path: "Animal-api sync for customer NL_121285",
pathFormatted: "req_animal-api-sync-86f14",
stats: {
    "name": "Animal-api sync for customer NL_121285",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "18831",
        "ok": "18831",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "18831",
        "ok": "18831",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "18831",
        "ok": "18831",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "18831",
        "ok": "18831",
        "ko": "-"
    },
    "percentiles2": {
        "total": "18831",
        "ok": "18831",
        "ko": "-"
    },
    "percentiles3": {
        "total": "18831",
        "ok": "18831",
        "ko": "-"
    },
    "percentiles4": {
        "total": "18831",
        "ok": "18831",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d5299": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104311",
path: "Animal-api sync for customer NL_104311",
pathFormatted: "req_animal-api-sync-d5299",
stats: {
    "name": "Animal-api sync for customer NL_104311",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "9195",
        "ok": "9195",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "9195",
        "ok": "9195",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "9195",
        "ok": "9195",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "9195",
        "ok": "9195",
        "ko": "-"
    },
    "percentiles2": {
        "total": "9195",
        "ok": "9195",
        "ko": "-"
    },
    "percentiles3": {
        "total": "9195",
        "ok": "9195",
        "ko": "-"
    },
    "percentiles4": {
        "total": "9195",
        "ok": "9195",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-51e6e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117693",
path: "Animal-api sync for customer NL_117693",
pathFormatted: "req_animal-api-sync-51e6e",
stats: {
    "name": "Animal-api sync for customer NL_117693",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "19323",
        "ok": "19323",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "19323",
        "ok": "19323",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "19323",
        "ok": "19323",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "19323",
        "ok": "19323",
        "ko": "-"
    },
    "percentiles2": {
        "total": "19323",
        "ok": "19323",
        "ko": "-"
    },
    "percentiles3": {
        "total": "19323",
        "ok": "19323",
        "ko": "-"
    },
    "percentiles4": {
        "total": "19323",
        "ok": "19323",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-44547": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117813",
path: "Animal-api sync for customer NL_117813",
pathFormatted: "req_animal-api-sync-44547",
stats: {
    "name": "Animal-api sync for customer NL_117813",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "33951",
        "ok": "33951",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "33951",
        "ok": "33951",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "33951",
        "ok": "33951",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "33951",
        "ok": "33951",
        "ko": "-"
    },
    "percentiles2": {
        "total": "33951",
        "ok": "33951",
        "ko": "-"
    },
    "percentiles3": {
        "total": "33951",
        "ok": "33951",
        "ko": "-"
    },
    "percentiles4": {
        "total": "33951",
        "ok": "33951",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-76899": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_121301",
path: "Animal-api sync for customer NL_121301",
pathFormatted: "req_animal-api-sync-76899",
stats: {
    "name": "Animal-api sync for customer NL_121301",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "40929",
        "ok": "40929",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "40929",
        "ok": "40929",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "40929",
        "ok": "40929",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "40929",
        "ok": "40929",
        "ko": "-"
    },
    "percentiles2": {
        "total": "40929",
        "ok": "40929",
        "ko": "-"
    },
    "percentiles3": {
        "total": "40929",
        "ok": "40929",
        "ko": "-"
    },
    "percentiles4": {
        "total": "40929",
        "ok": "40929",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-857a0": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_127307",
path: "Animal-api sync for customer NL_127307",
pathFormatted: "req_animal-api-sync-857a0",
stats: {
    "name": "Animal-api sync for customer NL_127307",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2935",
        "ok": "2935",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2935",
        "ok": "2935",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2935",
        "ok": "2935",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2935",
        "ok": "2935",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2935",
        "ok": "2935",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2935",
        "ok": "2935",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2935",
        "ok": "2935",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-8b245": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_120606",
path: "Animal-api sync for customer NL_120606",
pathFormatted: "req_animal-api-sync-8b245",
stats: {
    "name": "Animal-api sync for customer NL_120606",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "15057",
        "ok": "15057",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "15057",
        "ok": "15057",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "15057",
        "ok": "15057",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "15057",
        "ok": "15057",
        "ko": "-"
    },
    "percentiles2": {
        "total": "15057",
        "ok": "15057",
        "ko": "-"
    },
    "percentiles3": {
        "total": "15057",
        "ok": "15057",
        "ko": "-"
    },
    "percentiles4": {
        "total": "15057",
        "ok": "15057",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-1ba07": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105320",
path: "Animal-api sync for customer NL_105320",
pathFormatted: "req_animal-api-sync-1ba07",
stats: {
    "name": "Animal-api sync for customer NL_105320",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "7356",
        "ok": "7356",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "7356",
        "ok": "7356",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "7356",
        "ok": "7356",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "7356",
        "ok": "7356",
        "ko": "-"
    },
    "percentiles2": {
        "total": "7356",
        "ok": "7356",
        "ko": "-"
    },
    "percentiles3": {
        "total": "7356",
        "ok": "7356",
        "ko": "-"
    },
    "percentiles4": {
        "total": "7356",
        "ok": "7356",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-07d8c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_115607",
path: "Animal-api sync for customer NL_115607",
pathFormatted: "req_animal-api-sync-07d8c",
stats: {
    "name": "Animal-api sync for customer NL_115607",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "14922",
        "ok": "14922",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "14922",
        "ok": "14922",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "14922",
        "ok": "14922",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "14922",
        "ok": "14922",
        "ko": "-"
    },
    "percentiles2": {
        "total": "14922",
        "ok": "14922",
        "ko": "-"
    },
    "percentiles3": {
        "total": "14922",
        "ok": "14922",
        "ko": "-"
    },
    "percentiles4": {
        "total": "14922",
        "ok": "14922",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d643b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_129420",
path: "Animal-api sync for customer NL_129420",
pathFormatted: "req_animal-api-sync-d643b",
stats: {
    "name": "Animal-api sync for customer NL_129420",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2357",
        "ok": "2357",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2357",
        "ok": "2357",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2357",
        "ok": "2357",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2357",
        "ok": "2357",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2357",
        "ok": "2357",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2357",
        "ok": "2357",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2357",
        "ok": "2357",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-5b7d7": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_113415",
path: "Animal-api sync for customer NL_113415",
pathFormatted: "req_animal-api-sync-5b7d7",
stats: {
    "name": "Animal-api sync for customer NL_113415",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "11556",
        "ok": "11556",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "11556",
        "ok": "11556",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "11556",
        "ok": "11556",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "11556",
        "ok": "11556",
        "ko": "-"
    },
    "percentiles2": {
        "total": "11556",
        "ok": "11556",
        "ko": "-"
    },
    "percentiles3": {
        "total": "11556",
        "ok": "11556",
        "ko": "-"
    },
    "percentiles4": {
        "total": "11556",
        "ok": "11556",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-05129": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117717",
path: "Animal-api sync for customer NL_117717",
pathFormatted: "req_animal-api-sync-05129",
stats: {
    "name": "Animal-api sync for customer NL_117717",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "7887",
        "ok": "7887",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "7887",
        "ok": "7887",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "7887",
        "ok": "7887",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "7887",
        "ok": "7887",
        "ko": "-"
    },
    "percentiles2": {
        "total": "7887",
        "ok": "7887",
        "ko": "-"
    },
    "percentiles3": {
        "total": "7887",
        "ok": "7887",
        "ko": "-"
    },
    "percentiles4": {
        "total": "7887",
        "ok": "7887",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a175e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_207835",
path: "Animal-api sync for customer NL_207835",
pathFormatted: "req_animal-api-sync-a175e",
stats: {
    "name": "Animal-api sync for customer NL_207835",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2069",
        "ok": "2069",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2069",
        "ok": "2069",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2069",
        "ok": "2069",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2069",
        "ok": "2069",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2069",
        "ok": "2069",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2069",
        "ok": "2069",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2069",
        "ok": "2069",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-3bf26": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_108699",
path: "Animal-api sync for customer NL_108699",
pathFormatted: "req_animal-api-sync-3bf26",
stats: {
    "name": "Animal-api sync for customer NL_108699",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2135",
        "ok": "2135",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2135",
        "ok": "2135",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2135",
        "ok": "2135",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2135",
        "ok": "2135",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2135",
        "ok": "2135",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2135",
        "ok": "2135",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2135",
        "ok": "2135",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-2b97a": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_161984",
path: "Animal-api sync for customer NL_161984",
pathFormatted: "req_animal-api-sync-2b97a",
stats: {
    "name": "Animal-api sync for customer NL_161984",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "10669",
        "ok": "10669",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "10669",
        "ok": "10669",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "10669",
        "ok": "10669",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "10669",
        "ok": "10669",
        "ko": "-"
    },
    "percentiles2": {
        "total": "10669",
        "ok": "10669",
        "ko": "-"
    },
    "percentiles3": {
        "total": "10669",
        "ok": "10669",
        "ko": "-"
    },
    "percentiles4": {
        "total": "10669",
        "ok": "10669",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-5243f": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111939",
path: "Animal-api sync for customer NL_111939",
pathFormatted: "req_animal-api-sync-5243f",
stats: {
    "name": "Animal-api sync for customer NL_111939",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "58500",
        "ok": "58500",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "58500",
        "ok": "58500",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "58500",
        "ok": "58500",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "58500",
        "ok": "58500",
        "ko": "-"
    },
    "percentiles2": {
        "total": "58500",
        "ok": "58500",
        "ko": "-"
    },
    "percentiles3": {
        "total": "58500",
        "ok": "58500",
        "ko": "-"
    },
    "percentiles4": {
        "total": "58500",
        "ok": "58500",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b6219": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_144583",
path: "Animal-api sync for customer NL_144583",
pathFormatted: "req_animal-api-sync-b6219",
stats: {
    "name": "Animal-api sync for customer NL_144583",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "16670",
        "ok": "16670",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "16670",
        "ok": "16670",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "16670",
        "ok": "16670",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "16670",
        "ok": "16670",
        "ko": "-"
    },
    "percentiles2": {
        "total": "16670",
        "ok": "16670",
        "ko": "-"
    },
    "percentiles3": {
        "total": "16670",
        "ok": "16670",
        "ko": "-"
    },
    "percentiles4": {
        "total": "16670",
        "ok": "16670",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-43dcc": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_112405",
path: "Animal-api sync for customer NL_112405",
pathFormatted: "req_animal-api-sync-43dcc",
stats: {
    "name": "Animal-api sync for customer NL_112405",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "30645",
        "ok": "30645",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "30645",
        "ok": "30645",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "30645",
        "ok": "30645",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "30645",
        "ok": "30645",
        "ko": "-"
    },
    "percentiles2": {
        "total": "30645",
        "ok": "30645",
        "ko": "-"
    },
    "percentiles3": {
        "total": "30645",
        "ok": "30645",
        "ko": "-"
    },
    "percentiles4": {
        "total": "30645",
        "ok": "30645",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d545c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_131064",
path: "Animal-api sync for customer NL_131064",
pathFormatted: "req_animal-api-sync-d545c",
stats: {
    "name": "Animal-api sync for customer NL_131064",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "12144",
        "ok": "12144",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "12144",
        "ok": "12144",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "12144",
        "ok": "12144",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "12144",
        "ok": "12144",
        "ko": "-"
    },
    "percentiles2": {
        "total": "12144",
        "ok": "12144",
        "ko": "-"
    },
    "percentiles3": {
        "total": "12144",
        "ok": "12144",
        "ko": "-"
    },
    "percentiles4": {
        "total": "12144",
        "ok": "12144",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-93205": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_120176",
path: "Animal-api sync for customer NL_120176",
pathFormatted: "req_animal-api-sync-93205",
stats: {
    "name": "Animal-api sync for customer NL_120176",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "11290",
        "ok": "11290",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "11290",
        "ok": "11290",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "11290",
        "ok": "11290",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "11290",
        "ok": "11290",
        "ko": "-"
    },
    "percentiles2": {
        "total": "11290",
        "ok": "11290",
        "ko": "-"
    },
    "percentiles3": {
        "total": "11290",
        "ok": "11290",
        "ko": "-"
    },
    "percentiles4": {
        "total": "11290",
        "ok": "11290",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-bf978": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_118830",
path: "Animal-api sync for customer NL_118830",
pathFormatted: "req_animal-api-sync-bf978",
stats: {
    "name": "Animal-api sync for customer NL_118830",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "11279",
        "ok": "11279",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "11279",
        "ok": "11279",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "11279",
        "ok": "11279",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "11279",
        "ok": "11279",
        "ko": "-"
    },
    "percentiles2": {
        "total": "11279",
        "ok": "11279",
        "ko": "-"
    },
    "percentiles3": {
        "total": "11279",
        "ok": "11279",
        "ko": "-"
    },
    "percentiles4": {
        "total": "11279",
        "ok": "11279",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-28852": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_116904",
path: "Animal-api sync for customer NL_116904",
pathFormatted: "req_animal-api-sync-28852",
stats: {
    "name": "Animal-api sync for customer NL_116904",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "50928",
        "ok": "50928",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "50928",
        "ok": "50928",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "50928",
        "ok": "50928",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "50928",
        "ok": "50928",
        "ko": "-"
    },
    "percentiles2": {
        "total": "50928",
        "ok": "50928",
        "ko": "-"
    },
    "percentiles3": {
        "total": "50928",
        "ok": "50928",
        "ko": "-"
    },
    "percentiles4": {
        "total": "50928",
        "ok": "50928",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-3566a": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_107884",
path: "Animal-api sync for customer NL_107884",
pathFormatted: "req_animal-api-sync-3566a",
stats: {
    "name": "Animal-api sync for customer NL_107884",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "28664",
        "ok": "28664",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "28664",
        "ok": "28664",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "28664",
        "ok": "28664",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "28664",
        "ok": "28664",
        "ko": "-"
    },
    "percentiles2": {
        "total": "28664",
        "ok": "28664",
        "ko": "-"
    },
    "percentiles3": {
        "total": "28664",
        "ok": "28664",
        "ko": "-"
    },
    "percentiles4": {
        "total": "28664",
        "ok": "28664",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c24b5": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_122429",
path: "Animal-api sync for customer NL_122429",
pathFormatted: "req_animal-api-sync-c24b5",
stats: {
    "name": "Animal-api sync for customer NL_122429",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "34228",
        "ok": "34228",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "34228",
        "ok": "34228",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "34228",
        "ok": "34228",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "34228",
        "ok": "34228",
        "ko": "-"
    },
    "percentiles2": {
        "total": "34228",
        "ok": "34228",
        "ko": "-"
    },
    "percentiles3": {
        "total": "34228",
        "ok": "34228",
        "ko": "-"
    },
    "percentiles4": {
        "total": "34228",
        "ok": "34228",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-31f54": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_122305",
path: "Animal-api sync for customer NL_122305",
pathFormatted: "req_animal-api-sync-31f54",
stats: {
    "name": "Animal-api sync for customer NL_122305",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "11613",
        "ok": "11613",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "11613",
        "ok": "11613",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "11613",
        "ok": "11613",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "11613",
        "ok": "11613",
        "ko": "-"
    },
    "percentiles2": {
        "total": "11613",
        "ok": "11613",
        "ko": "-"
    },
    "percentiles3": {
        "total": "11613",
        "ok": "11613",
        "ko": "-"
    },
    "percentiles4": {
        "total": "11613",
        "ok": "11613",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-8ebc3": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_212864",
path: "Animal-api sync for customer BE_212864",
pathFormatted: "req_animal-api-sync-8ebc3",
stats: {
    "name": "Animal-api sync for customer BE_212864",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "24149",
        "ok": "24149",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "24149",
        "ok": "24149",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "24149",
        "ok": "24149",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "24149",
        "ok": "24149",
        "ko": "-"
    },
    "percentiles2": {
        "total": "24149",
        "ok": "24149",
        "ko": "-"
    },
    "percentiles3": {
        "total": "24149",
        "ok": "24149",
        "ko": "-"
    },
    "percentiles4": {
        "total": "24149",
        "ok": "24149",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a5160": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_156494",
path: "Animal-api sync for customer NL_156494",
pathFormatted: "req_animal-api-sync-a5160",
stats: {
    "name": "Animal-api sync for customer NL_156494",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "54285",
        "ok": "54285",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "54285",
        "ok": "54285",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "54285",
        "ok": "54285",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "54285",
        "ok": "54285",
        "ko": "-"
    },
    "percentiles2": {
        "total": "54285",
        "ok": "54285",
        "ko": "-"
    },
    "percentiles3": {
        "total": "54285",
        "ok": "54285",
        "ko": "-"
    },
    "percentiles4": {
        "total": "54285",
        "ok": "54285",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-34ba1": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_165365",
path: "Animal-api sync for customer BE_165365",
pathFormatted: "req_animal-api-sync-34ba1",
stats: {
    "name": "Animal-api sync for customer BE_165365",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "17679",
        "ok": "17679",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "17679",
        "ok": "17679",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "17679",
        "ok": "17679",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "17679",
        "ok": "17679",
        "ko": "-"
    },
    "percentiles2": {
        "total": "17679",
        "ok": "17679",
        "ko": "-"
    },
    "percentiles3": {
        "total": "17679",
        "ok": "17679",
        "ko": "-"
    },
    "percentiles4": {
        "total": "17679",
        "ok": "17679",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-9ec36": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_124747",
path: "Animal-api sync for customer NL_124747",
pathFormatted: "req_animal-api-sync-9ec36",
stats: {
    "name": "Animal-api sync for customer NL_124747",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "13657",
        "ok": "13657",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "13657",
        "ok": "13657",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "13657",
        "ok": "13657",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "13657",
        "ok": "13657",
        "ko": "-"
    },
    "percentiles2": {
        "total": "13657",
        "ok": "13657",
        "ko": "-"
    },
    "percentiles3": {
        "total": "13657",
        "ok": "13657",
        "ko": "-"
    },
    "percentiles4": {
        "total": "13657",
        "ok": "13657",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b9862": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_161770",
path: "Animal-api sync for customer NL_161770",
pathFormatted: "req_animal-api-sync-b9862",
stats: {
    "name": "Animal-api sync for customer NL_161770",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "maxResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "meanResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles2": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles3": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles4": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-b2272": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105223",
path: "Animal-api sync for customer NL_105223",
pathFormatted: "req_animal-api-sync-b2272",
stats: {
    "name": "Animal-api sync for customer NL_105223",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "13175",
        "ok": "13175",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "13175",
        "ok": "13175",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "13175",
        "ok": "13175",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "13175",
        "ok": "13175",
        "ko": "-"
    },
    "percentiles2": {
        "total": "13175",
        "ok": "13175",
        "ko": "-"
    },
    "percentiles3": {
        "total": "13175",
        "ok": "13175",
        "ko": "-"
    },
    "percentiles4": {
        "total": "13175",
        "ok": "13175",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-49a96": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_128642",
path: "Animal-api sync for customer NL_128642",
pathFormatted: "req_animal-api-sync-49a96",
stats: {
    "name": "Animal-api sync for customer NL_128642",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "31015",
        "ok": "31015",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "31015",
        "ok": "31015",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "31015",
        "ok": "31015",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "31015",
        "ok": "31015",
        "ko": "-"
    },
    "percentiles2": {
        "total": "31015",
        "ok": "31015",
        "ko": "-"
    },
    "percentiles3": {
        "total": "31015",
        "ok": "31015",
        "ko": "-"
    },
    "percentiles4": {
        "total": "31015",
        "ok": "31015",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-5c4eb": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_123093",
path: "Animal-api sync for customer NL_123093",
pathFormatted: "req_animal-api-sync-5c4eb",
stats: {
    "name": "Animal-api sync for customer NL_123093",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3659",
        "ok": "3659",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3659",
        "ok": "3659",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3659",
        "ok": "3659",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3659",
        "ok": "3659",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3659",
        "ok": "3659",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3659",
        "ok": "3659",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3659",
        "ok": "3659",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-90ee6": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105389",
path: "Animal-api sync for customer NL_105389",
pathFormatted: "req_animal-api-sync-90ee6",
stats: {
    "name": "Animal-api sync for customer NL_105389",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "34972",
        "ok": "34972",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "34972",
        "ok": "34972",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "34972",
        "ok": "34972",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "34972",
        "ok": "34972",
        "ko": "-"
    },
    "percentiles2": {
        "total": "34972",
        "ok": "34972",
        "ko": "-"
    },
    "percentiles3": {
        "total": "34972",
        "ok": "34972",
        "ko": "-"
    },
    "percentiles4": {
        "total": "34972",
        "ok": "34972",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-0908d": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105707",
path: "Animal-api sync for customer NL_105707",
pathFormatted: "req_animal-api-sync-0908d",
stats: {
    "name": "Animal-api sync for customer NL_105707",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "10695",
        "ok": "10695",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "10695",
        "ok": "10695",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "10695",
        "ok": "10695",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "10695",
        "ok": "10695",
        "ko": "-"
    },
    "percentiles2": {
        "total": "10695",
        "ok": "10695",
        "ko": "-"
    },
    "percentiles3": {
        "total": "10695",
        "ok": "10695",
        "ko": "-"
    },
    "percentiles4": {
        "total": "10695",
        "ok": "10695",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-0a82c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_124910",
path: "Animal-api sync for customer NL_124910",
pathFormatted: "req_animal-api-sync-0a82c",
stats: {
    "name": "Animal-api sync for customer NL_124910",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2713",
        "ok": "2713",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2713",
        "ok": "2713",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2713",
        "ok": "2713",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2713",
        "ok": "2713",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2713",
        "ok": "2713",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2713",
        "ok": "2713",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2713",
        "ok": "2713",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d54bb": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_108024",
path: "Animal-api sync for customer NL_108024",
pathFormatted: "req_animal-api-sync-d54bb",
stats: {
    "name": "Animal-api sync for customer NL_108024",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "maxResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "meanResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles2": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles3": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles4": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-c0ea2": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_126187",
path: "Animal-api sync for customer NL_126187",
pathFormatted: "req_animal-api-sync-c0ea2",
stats: {
    "name": "Animal-api sync for customer NL_126187",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "6496",
        "ok": "6496",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "6496",
        "ok": "6496",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "6496",
        "ok": "6496",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "6496",
        "ok": "6496",
        "ko": "-"
    },
    "percentiles2": {
        "total": "6496",
        "ok": "6496",
        "ko": "-"
    },
    "percentiles3": {
        "total": "6496",
        "ok": "6496",
        "ko": "-"
    },
    "percentiles4": {
        "total": "6496",
        "ok": "6496",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-7a1f1": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_151229",
path: "Animal-api sync for customer BE_151229",
pathFormatted: "req_animal-api-sync-7a1f1",
stats: {
    "name": "Animal-api sync for customer BE_151229",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1657",
        "ok": "1657",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1657",
        "ok": "1657",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1657",
        "ok": "1657",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1657",
        "ok": "1657",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1657",
        "ok": "1657",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1657",
        "ok": "1657",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1657",
        "ok": "1657",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-72904": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_103783",
path: "Animal-api sync for customer NL_103783",
pathFormatted: "req_animal-api-sync-72904",
stats: {
    "name": "Animal-api sync for customer NL_103783",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "maxResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "meanResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles2": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles3": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles4": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-54314": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_103881",
path: "Animal-api sync for customer NL_103881",
pathFormatted: "req_animal-api-sync-54314",
stats: {
    "name": "Animal-api sync for customer NL_103881",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "25449",
        "ok": "25449",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "25449",
        "ok": "25449",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "25449",
        "ok": "25449",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "25449",
        "ok": "25449",
        "ko": "-"
    },
    "percentiles2": {
        "total": "25449",
        "ok": "25449",
        "ko": "-"
    },
    "percentiles3": {
        "total": "25449",
        "ok": "25449",
        "ko": "-"
    },
    "percentiles4": {
        "total": "25449",
        "ok": "25449",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-fb3ec": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_115893",
path: "Animal-api sync for customer NL_115893",
pathFormatted: "req_animal-api-sync-fb3ec",
stats: {
    "name": "Animal-api sync for customer NL_115893",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "27899",
        "ok": "27899",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "27899",
        "ok": "27899",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "27899",
        "ok": "27899",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "27899",
        "ok": "27899",
        "ko": "-"
    },
    "percentiles2": {
        "total": "27899",
        "ok": "27899",
        "ko": "-"
    },
    "percentiles3": {
        "total": "27899",
        "ok": "27899",
        "ko": "-"
    },
    "percentiles4": {
        "total": "27899",
        "ok": "27899",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d90a9": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_119649",
path: "Animal-api sync for customer NL_119649",
pathFormatted: "req_animal-api-sync-d90a9",
stats: {
    "name": "Animal-api sync for customer NL_119649",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "5487",
        "ok": "5487",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5487",
        "ok": "5487",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5487",
        "ok": "5487",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "5487",
        "ok": "5487",
        "ko": "-"
    },
    "percentiles2": {
        "total": "5487",
        "ok": "5487",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5487",
        "ok": "5487",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5487",
        "ok": "5487",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c4ec4": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104910",
path: "Animal-api sync for customer NL_104910",
pathFormatted: "req_animal-api-sync-c4ec4",
stats: {
    "name": "Animal-api sync for customer NL_104910",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "9003",
        "ok": "9003",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "9003",
        "ok": "9003",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "9003",
        "ok": "9003",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "9003",
        "ok": "9003",
        "ko": "-"
    },
    "percentiles2": {
        "total": "9003",
        "ok": "9003",
        "ko": "-"
    },
    "percentiles3": {
        "total": "9003",
        "ok": "9003",
        "ko": "-"
    },
    "percentiles4": {
        "total": "9003",
        "ok": "9003",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e498b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110382",
path: "Animal-api sync for customer NL_110382",
pathFormatted: "req_animal-api-sync-e498b",
stats: {
    "name": "Animal-api sync for customer NL_110382",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "15324",
        "ok": "15324",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "15324",
        "ok": "15324",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "15324",
        "ok": "15324",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "15324",
        "ok": "15324",
        "ko": "-"
    },
    "percentiles2": {
        "total": "15324",
        "ok": "15324",
        "ko": "-"
    },
    "percentiles3": {
        "total": "15324",
        "ok": "15324",
        "ko": "-"
    },
    "percentiles4": {
        "total": "15324",
        "ok": "15324",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-2a4dd": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_125297",
path: "Animal-api sync for customer NL_125297",
pathFormatted: "req_animal-api-sync-2a4dd",
stats: {
    "name": "Animal-api sync for customer NL_125297",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "11013",
        "ok": "11013",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "11013",
        "ok": "11013",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "11013",
        "ok": "11013",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "11013",
        "ok": "11013",
        "ko": "-"
    },
    "percentiles2": {
        "total": "11013",
        "ok": "11013",
        "ko": "-"
    },
    "percentiles3": {
        "total": "11013",
        "ok": "11013",
        "ko": "-"
    },
    "percentiles4": {
        "total": "11013",
        "ok": "11013",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c1c6a": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_135207",
path: "Animal-api sync for customer NL_135207",
pathFormatted: "req_animal-api-sync-c1c6a",
stats: {
    "name": "Animal-api sync for customer NL_135207",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "11849",
        "ok": "11849",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "11849",
        "ok": "11849",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "11849",
        "ok": "11849",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "11849",
        "ok": "11849",
        "ko": "-"
    },
    "percentiles2": {
        "total": "11849",
        "ok": "11849",
        "ko": "-"
    },
    "percentiles3": {
        "total": "11849",
        "ok": "11849",
        "ko": "-"
    },
    "percentiles4": {
        "total": "11849",
        "ok": "11849",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c170f": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_109428",
path: "Animal-api sync for customer NL_109428",
pathFormatted: "req_animal-api-sync-c170f",
stats: {
    "name": "Animal-api sync for customer NL_109428",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "41470",
        "ok": "41470",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "41470",
        "ok": "41470",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "41470",
        "ok": "41470",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "41470",
        "ok": "41470",
        "ko": "-"
    },
    "percentiles2": {
        "total": "41470",
        "ok": "41470",
        "ko": "-"
    },
    "percentiles3": {
        "total": "41470",
        "ok": "41470",
        "ko": "-"
    },
    "percentiles4": {
        "total": "41470",
        "ok": "41470",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-690f7": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_129681",
path: "Animal-api sync for customer NL_129681",
pathFormatted: "req_animal-api-sync-690f7",
stats: {
    "name": "Animal-api sync for customer NL_129681",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "6716",
        "ok": "6716",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "6716",
        "ok": "6716",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "6716",
        "ok": "6716",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "6716",
        "ok": "6716",
        "ko": "-"
    },
    "percentiles2": {
        "total": "6716",
        "ok": "6716",
        "ko": "-"
    },
    "percentiles3": {
        "total": "6716",
        "ok": "6716",
        "ko": "-"
    },
    "percentiles4": {
        "total": "6716",
        "ok": "6716",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-de0b1": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_164131",
path: "Animal-api sync for customer NL_164131",
pathFormatted: "req_animal-api-sync-de0b1",
stats: {
    "name": "Animal-api sync for customer NL_164131",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "41939",
        "ok": "41939",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "41939",
        "ok": "41939",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "41939",
        "ok": "41939",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "41939",
        "ok": "41939",
        "ko": "-"
    },
    "percentiles2": {
        "total": "41939",
        "ok": "41939",
        "ko": "-"
    },
    "percentiles3": {
        "total": "41939",
        "ok": "41939",
        "ko": "-"
    },
    "percentiles4": {
        "total": "41939",
        "ok": "41939",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-3e31b": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_149431",
path: "Animal-api sync for customer BE_149431",
pathFormatted: "req_animal-api-sync-3e31b",
stats: {
    "name": "Animal-api sync for customer BE_149431",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "6882",
        "ok": "6882",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "6882",
        "ok": "6882",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "6882",
        "ok": "6882",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "6882",
        "ok": "6882",
        "ko": "-"
    },
    "percentiles2": {
        "total": "6882",
        "ok": "6882",
        "ko": "-"
    },
    "percentiles3": {
        "total": "6882",
        "ok": "6882",
        "ko": "-"
    },
    "percentiles4": {
        "total": "6882",
        "ok": "6882",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-cb71c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_120709",
path: "Animal-api sync for customer NL_120709",
pathFormatted: "req_animal-api-sync-cb71c",
stats: {
    "name": "Animal-api sync for customer NL_120709",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "maxResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "meanResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles2": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles3": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles4": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-5a7e6": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105010",
path: "Animal-api sync for customer NL_105010",
pathFormatted: "req_animal-api-sync-5a7e6",
stats: {
    "name": "Animal-api sync for customer NL_105010",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "29428",
        "ok": "29428",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "29428",
        "ok": "29428",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "29428",
        "ok": "29428",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "29428",
        "ok": "29428",
        "ko": "-"
    },
    "percentiles2": {
        "total": "29428",
        "ok": "29428",
        "ko": "-"
    },
    "percentiles3": {
        "total": "29428",
        "ok": "29428",
        "ko": "-"
    },
    "percentiles4": {
        "total": "29428",
        "ok": "29428",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-0eddf": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110820",
path: "Animal-api sync for customer NL_110820",
pathFormatted: "req_animal-api-sync-0eddf",
stats: {
    "name": "Animal-api sync for customer NL_110820",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21269",
        "ok": "21269",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "21269",
        "ok": "21269",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "21269",
        "ok": "21269",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "21269",
        "ok": "21269",
        "ko": "-"
    },
    "percentiles2": {
        "total": "21269",
        "ok": "21269",
        "ko": "-"
    },
    "percentiles3": {
        "total": "21269",
        "ok": "21269",
        "ko": "-"
    },
    "percentiles4": {
        "total": "21269",
        "ok": "21269",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-558a2": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_116270",
path: "Animal-api sync for customer NL_116270",
pathFormatted: "req_animal-api-sync-558a2",
stats: {
    "name": "Animal-api sync for customer NL_116270",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "11481",
        "ok": "11481",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "11481",
        "ok": "11481",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "11481",
        "ok": "11481",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "11481",
        "ok": "11481",
        "ko": "-"
    },
    "percentiles2": {
        "total": "11481",
        "ok": "11481",
        "ko": "-"
    },
    "percentiles3": {
        "total": "11481",
        "ok": "11481",
        "ko": "-"
    },
    "percentiles4": {
        "total": "11481",
        "ok": "11481",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-3833c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_124824",
path: "Animal-api sync for customer NL_124824",
pathFormatted: "req_animal-api-sync-3833c",
stats: {
    "name": "Animal-api sync for customer NL_124824",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "6863",
        "ok": "6863",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "6863",
        "ok": "6863",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "6863",
        "ok": "6863",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "6863",
        "ok": "6863",
        "ko": "-"
    },
    "percentiles2": {
        "total": "6863",
        "ok": "6863",
        "ko": "-"
    },
    "percentiles3": {
        "total": "6863",
        "ok": "6863",
        "ko": "-"
    },
    "percentiles4": {
        "total": "6863",
        "ok": "6863",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a3967": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117770",
path: "Animal-api sync for customer NL_117770",
pathFormatted: "req_animal-api-sync-a3967",
stats: {
    "name": "Animal-api sync for customer NL_117770",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "16483",
        "ok": "16483",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "16483",
        "ok": "16483",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "16483",
        "ok": "16483",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "16483",
        "ok": "16483",
        "ko": "-"
    },
    "percentiles2": {
        "total": "16483",
        "ok": "16483",
        "ko": "-"
    },
    "percentiles3": {
        "total": "16483",
        "ok": "16483",
        "ko": "-"
    },
    "percentiles4": {
        "total": "16483",
        "ok": "16483",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-5a613": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_194583",
path: "Animal-api sync for customer NL_194583",
pathFormatted: "req_animal-api-sync-5a613",
stats: {
    "name": "Animal-api sync for customer NL_194583",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "17874",
        "ok": "17874",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "17874",
        "ok": "17874",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "17874",
        "ok": "17874",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "17874",
        "ok": "17874",
        "ko": "-"
    },
    "percentiles2": {
        "total": "17874",
        "ok": "17874",
        "ko": "-"
    },
    "percentiles3": {
        "total": "17874",
        "ok": "17874",
        "ko": "-"
    },
    "percentiles4": {
        "total": "17874",
        "ok": "17874",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c7c5d": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_129379",
path: "Animal-api sync for customer NL_129379",
pathFormatted: "req_animal-api-sync-c7c5d",
stats: {
    "name": "Animal-api sync for customer NL_129379",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "15306",
        "ok": "15306",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "15306",
        "ok": "15306",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "15306",
        "ok": "15306",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "15306",
        "ok": "15306",
        "ko": "-"
    },
    "percentiles2": {
        "total": "15306",
        "ok": "15306",
        "ko": "-"
    },
    "percentiles3": {
        "total": "15306",
        "ok": "15306",
        "ko": "-"
    },
    "percentiles4": {
        "total": "15306",
        "ok": "15306",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-91ebb": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_120778",
path: "Animal-api sync for customer NL_120778",
pathFormatted: "req_animal-api-sync-91ebb",
stats: {
    "name": "Animal-api sync for customer NL_120778",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "6552",
        "ok": "6552",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "6552",
        "ok": "6552",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "6552",
        "ok": "6552",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "6552",
        "ok": "6552",
        "ko": "-"
    },
    "percentiles2": {
        "total": "6552",
        "ok": "6552",
        "ko": "-"
    },
    "percentiles3": {
        "total": "6552",
        "ok": "6552",
        "ko": "-"
    },
    "percentiles4": {
        "total": "6552",
        "ok": "6552",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-6d45c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_123012",
path: "Animal-api sync for customer NL_123012",
pathFormatted: "req_animal-api-sync-6d45c",
stats: {
    "name": "Animal-api sync for customer NL_123012",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "6168",
        "ok": "6168",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "6168",
        "ok": "6168",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "6168",
        "ok": "6168",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "6168",
        "ok": "6168",
        "ko": "-"
    },
    "percentiles2": {
        "total": "6168",
        "ok": "6168",
        "ko": "-"
    },
    "percentiles3": {
        "total": "6168",
        "ok": "6168",
        "ko": "-"
    },
    "percentiles4": {
        "total": "6168",
        "ok": "6168",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-3ff93": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111287",
path: "Animal-api sync for customer NL_111287",
pathFormatted: "req_animal-api-sync-3ff93",
stats: {
    "name": "Animal-api sync for customer NL_111287",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21353",
        "ok": "21353",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "21353",
        "ok": "21353",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "21353",
        "ok": "21353",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "21353",
        "ok": "21353",
        "ko": "-"
    },
    "percentiles2": {
        "total": "21353",
        "ok": "21353",
        "ko": "-"
    },
    "percentiles3": {
        "total": "21353",
        "ok": "21353",
        "ko": "-"
    },
    "percentiles4": {
        "total": "21353",
        "ok": "21353",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-968ea": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_152278",
path: "Animal-api sync for customer BE_152278",
pathFormatted: "req_animal-api-sync-968ea",
stats: {
    "name": "Animal-api sync for customer BE_152278",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "10489",
        "ok": "10489",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "10489",
        "ok": "10489",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "10489",
        "ok": "10489",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "10489",
        "ok": "10489",
        "ko": "-"
    },
    "percentiles2": {
        "total": "10489",
        "ok": "10489",
        "ko": "-"
    },
    "percentiles3": {
        "total": "10489",
        "ok": "10489",
        "ko": "-"
    },
    "percentiles4": {
        "total": "10489",
        "ok": "10489",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b979d": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_134839",
path: "Animal-api sync for customer NL_134839",
pathFormatted: "req_animal-api-sync-b979d",
stats: {
    "name": "Animal-api sync for customer NL_134839",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "10269",
        "ok": "10269",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "10269",
        "ok": "10269",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "10269",
        "ok": "10269",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "10269",
        "ok": "10269",
        "ko": "-"
    },
    "percentiles2": {
        "total": "10269",
        "ok": "10269",
        "ko": "-"
    },
    "percentiles3": {
        "total": "10269",
        "ok": "10269",
        "ko": "-"
    },
    "percentiles4": {
        "total": "10269",
        "ok": "10269",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-4a2f0": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_119479",
path: "Animal-api sync for customer NL_119479",
pathFormatted: "req_animal-api-sync-4a2f0",
stats: {
    "name": "Animal-api sync for customer NL_119479",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "9283",
        "ok": "9283",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "9283",
        "ok": "9283",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "9283",
        "ok": "9283",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "9283",
        "ok": "9283",
        "ko": "-"
    },
    "percentiles2": {
        "total": "9283",
        "ok": "9283",
        "ko": "-"
    },
    "percentiles3": {
        "total": "9283",
        "ok": "9283",
        "ko": "-"
    },
    "percentiles4": {
        "total": "9283",
        "ok": "9283",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-04f19": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_142633",
path: "Animal-api sync for customer NL_142633",
pathFormatted: "req_animal-api-sync-04f19",
stats: {
    "name": "Animal-api sync for customer NL_142633",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "maxResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "meanResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles2": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles3": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles4": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-c6934": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_144844",
path: "Animal-api sync for customer BE_144844",
pathFormatted: "req_animal-api-sync-c6934",
stats: {
    "name": "Animal-api sync for customer BE_144844",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "17745",
        "ok": "17745",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "17745",
        "ok": "17745",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "17745",
        "ok": "17745",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "17745",
        "ok": "17745",
        "ko": "-"
    },
    "percentiles2": {
        "total": "17745",
        "ok": "17745",
        "ko": "-"
    },
    "percentiles3": {
        "total": "17745",
        "ok": "17745",
        "ko": "-"
    },
    "percentiles4": {
        "total": "17745",
        "ok": "17745",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-ee05d": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_106934",
path: "Animal-api sync for customer NL_106934",
pathFormatted: "req_animal-api-sync-ee05d",
stats: {
    "name": "Animal-api sync for customer NL_106934",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "11585",
        "ok": "11585",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "11585",
        "ok": "11585",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "11585",
        "ok": "11585",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "11585",
        "ok": "11585",
        "ko": "-"
    },
    "percentiles2": {
        "total": "11585",
        "ok": "11585",
        "ko": "-"
    },
    "percentiles3": {
        "total": "11585",
        "ok": "11585",
        "ko": "-"
    },
    "percentiles4": {
        "total": "11585",
        "ok": "11585",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-26bf9": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110058",
path: "Animal-api sync for customer NL_110058",
pathFormatted: "req_animal-api-sync-26bf9",
stats: {
    "name": "Animal-api sync for customer NL_110058",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "14083",
        "ok": "14083",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "14083",
        "ok": "14083",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "14083",
        "ok": "14083",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "14083",
        "ok": "14083",
        "ko": "-"
    },
    "percentiles2": {
        "total": "14083",
        "ok": "14083",
        "ko": "-"
    },
    "percentiles3": {
        "total": "14083",
        "ok": "14083",
        "ko": "-"
    },
    "percentiles4": {
        "total": "14083",
        "ok": "14083",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a42d0": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105360",
path: "Animal-api sync for customer NL_105360",
pathFormatted: "req_animal-api-sync-a42d0",
stats: {
    "name": "Animal-api sync for customer NL_105360",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "48141",
        "ok": "48141",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "48141",
        "ok": "48141",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "48141",
        "ok": "48141",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "48141",
        "ok": "48141",
        "ko": "-"
    },
    "percentiles2": {
        "total": "48141",
        "ok": "48141",
        "ko": "-"
    },
    "percentiles3": {
        "total": "48141",
        "ok": "48141",
        "ko": "-"
    },
    "percentiles4": {
        "total": "48141",
        "ok": "48141",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-35753": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_118795",
path: "Animal-api sync for customer NL_118795",
pathFormatted: "req_animal-api-sync-35753",
stats: {
    "name": "Animal-api sync for customer NL_118795",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "19538",
        "ok": "19538",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "19538",
        "ok": "19538",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "19538",
        "ok": "19538",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "19538",
        "ok": "19538",
        "ko": "-"
    },
    "percentiles2": {
        "total": "19538",
        "ok": "19538",
        "ko": "-"
    },
    "percentiles3": {
        "total": "19538",
        "ok": "19538",
        "ko": "-"
    },
    "percentiles4": {
        "total": "19538",
        "ok": "19538",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-09a8d": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117444",
path: "Animal-api sync for customer NL_117444",
pathFormatted: "req_animal-api-sync-09a8d",
stats: {
    "name": "Animal-api sync for customer NL_117444",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "6739",
        "ok": "6739",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "6739",
        "ok": "6739",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "6739",
        "ok": "6739",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "6739",
        "ok": "6739",
        "ko": "-"
    },
    "percentiles2": {
        "total": "6739",
        "ok": "6739",
        "ko": "-"
    },
    "percentiles3": {
        "total": "6739",
        "ok": "6739",
        "ko": "-"
    },
    "percentiles4": {
        "total": "6739",
        "ok": "6739",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-7c5cb": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_118794",
path: "Animal-api sync for customer NL_118794",
pathFormatted: "req_animal-api-sync-7c5cb",
stats: {
    "name": "Animal-api sync for customer NL_118794",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "9391",
        "ok": "9391",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "9391",
        "ok": "9391",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "9391",
        "ok": "9391",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "9391",
        "ok": "9391",
        "ko": "-"
    },
    "percentiles2": {
        "total": "9391",
        "ok": "9391",
        "ko": "-"
    },
    "percentiles3": {
        "total": "9391",
        "ok": "9391",
        "ko": "-"
    },
    "percentiles4": {
        "total": "9391",
        "ok": "9391",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e202d": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_112066",
path: "Animal-api sync for customer NL_112066",
pathFormatted: "req_animal-api-sync-e202d",
stats: {
    "name": "Animal-api sync for customer NL_112066",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "12313",
        "ok": "12313",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "12313",
        "ok": "12313",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "12313",
        "ok": "12313",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "12313",
        "ok": "12313",
        "ko": "-"
    },
    "percentiles2": {
        "total": "12313",
        "ok": "12313",
        "ko": "-"
    },
    "percentiles3": {
        "total": "12313",
        "ok": "12313",
        "ko": "-"
    },
    "percentiles4": {
        "total": "12313",
        "ok": "12313",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-be37d": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_106236",
path: "Animal-api sync for customer NL_106236",
pathFormatted: "req_animal-api-sync-be37d",
stats: {
    "name": "Animal-api sync for customer NL_106236",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "15003",
        "ok": "15003",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "15003",
        "ok": "15003",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "15003",
        "ok": "15003",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "15003",
        "ok": "15003",
        "ko": "-"
    },
    "percentiles2": {
        "total": "15003",
        "ok": "15003",
        "ko": "-"
    },
    "percentiles3": {
        "total": "15003",
        "ok": "15003",
        "ko": "-"
    },
    "percentiles4": {
        "total": "15003",
        "ok": "15003",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-ca7e3": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_134297",
path: "Animal-api sync for customer NL_134297",
pathFormatted: "req_animal-api-sync-ca7e3",
stats: {
    "name": "Animal-api sync for customer NL_134297",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "14469",
        "ok": "14469",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "14469",
        "ok": "14469",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "14469",
        "ok": "14469",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "14469",
        "ok": "14469",
        "ko": "-"
    },
    "percentiles2": {
        "total": "14469",
        "ok": "14469",
        "ko": "-"
    },
    "percentiles3": {
        "total": "14469",
        "ok": "14469",
        "ko": "-"
    },
    "percentiles4": {
        "total": "14469",
        "ok": "14469",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-993d0": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_160647",
path: "Animal-api sync for customer NL_160647",
pathFormatted: "req_animal-api-sync-993d0",
stats: {
    "name": "Animal-api sync for customer NL_160647",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "maxResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "meanResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles2": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles3": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles4": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-1e171": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111917",
path: "Animal-api sync for customer NL_111917",
pathFormatted: "req_animal-api-sync-1e171",
stats: {
    "name": "Animal-api sync for customer NL_111917",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "maxResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "meanResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles2": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles3": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles4": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-5e0b3": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_106214",
path: "Animal-api sync for customer NL_106214",
pathFormatted: "req_animal-api-sync-5e0b3",
stats: {
    "name": "Animal-api sync for customer NL_106214",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "9580",
        "ok": "9580",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "9580",
        "ok": "9580",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "9580",
        "ok": "9580",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "9580",
        "ok": "9580",
        "ko": "-"
    },
    "percentiles2": {
        "total": "9580",
        "ok": "9580",
        "ko": "-"
    },
    "percentiles3": {
        "total": "9580",
        "ok": "9580",
        "ko": "-"
    },
    "percentiles4": {
        "total": "9580",
        "ok": "9580",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a7313": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_123953",
path: "Animal-api sync for customer NL_123953",
pathFormatted: "req_animal-api-sync-a7313",
stats: {
    "name": "Animal-api sync for customer NL_123953",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "maxResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "meanResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles2": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles3": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles4": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-7cf01": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104284",
path: "Animal-api sync for customer NL_104284",
pathFormatted: "req_animal-api-sync-7cf01",
stats: {
    "name": "Animal-api sync for customer NL_104284",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "9500",
        "ok": "9500",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "9500",
        "ok": "9500",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "9500",
        "ok": "9500",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "9500",
        "ok": "9500",
        "ko": "-"
    },
    "percentiles2": {
        "total": "9500",
        "ok": "9500",
        "ko": "-"
    },
    "percentiles3": {
        "total": "9500",
        "ok": "9500",
        "ko": "-"
    },
    "percentiles4": {
        "total": "9500",
        "ok": "9500",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-588aa": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_214938",
path: "Animal-api sync for customer NL_214938",
pathFormatted: "req_animal-api-sync-588aa",
stats: {
    "name": "Animal-api sync for customer NL_214938",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21370",
        "ok": "21370",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "21370",
        "ok": "21370",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "21370",
        "ok": "21370",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "21370",
        "ok": "21370",
        "ko": "-"
    },
    "percentiles2": {
        "total": "21370",
        "ok": "21370",
        "ko": "-"
    },
    "percentiles3": {
        "total": "21370",
        "ok": "21370",
        "ko": "-"
    },
    "percentiles4": {
        "total": "21370",
        "ok": "21370",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-6ebe1": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104335",
path: "Animal-api sync for customer NL_104335",
pathFormatted: "req_animal-api-sync-6ebe1",
stats: {
    "name": "Animal-api sync for customer NL_104335",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "maxResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "meanResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles2": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles3": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles4": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-d86cb": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105153",
path: "Animal-api sync for customer NL_105153",
pathFormatted: "req_animal-api-sync-d86cb",
stats: {
    "name": "Animal-api sync for customer NL_105153",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "12072",
        "ok": "12072",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "12072",
        "ok": "12072",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "12072",
        "ok": "12072",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "12072",
        "ok": "12072",
        "ko": "-"
    },
    "percentiles2": {
        "total": "12072",
        "ok": "12072",
        "ko": "-"
    },
    "percentiles3": {
        "total": "12072",
        "ok": "12072",
        "ko": "-"
    },
    "percentiles4": {
        "total": "12072",
        "ok": "12072",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b6645": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_126020",
path: "Animal-api sync for customer NL_126020",
pathFormatted: "req_animal-api-sync-b6645",
stats: {
    "name": "Animal-api sync for customer NL_126020",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "34945",
        "ok": "34945",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "34945",
        "ok": "34945",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "34945",
        "ok": "34945",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "34945",
        "ok": "34945",
        "ko": "-"
    },
    "percentiles2": {
        "total": "34945",
        "ok": "34945",
        "ko": "-"
    },
    "percentiles3": {
        "total": "34945",
        "ok": "34945",
        "ko": "-"
    },
    "percentiles4": {
        "total": "34945",
        "ok": "34945",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-7ae4d": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_137252",
path: "Animal-api sync for customer NL_137252",
pathFormatted: "req_animal-api-sync-7ae4d",
stats: {
    "name": "Animal-api sync for customer NL_137252",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "11133",
        "ok": "11133",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "11133",
        "ok": "11133",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "11133",
        "ok": "11133",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "11133",
        "ok": "11133",
        "ko": "-"
    },
    "percentiles2": {
        "total": "11133",
        "ok": "11133",
        "ko": "-"
    },
    "percentiles3": {
        "total": "11133",
        "ok": "11133",
        "ko": "-"
    },
    "percentiles4": {
        "total": "11133",
        "ok": "11133",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-f2327": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_107409",
path: "Animal-api sync for customer NL_107409",
pathFormatted: "req_animal-api-sync-f2327",
stats: {
    "name": "Animal-api sync for customer NL_107409",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "43697",
        "ok": "43697",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "43697",
        "ok": "43697",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "43697",
        "ok": "43697",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "43697",
        "ok": "43697",
        "ko": "-"
    },
    "percentiles2": {
        "total": "43697",
        "ok": "43697",
        "ko": "-"
    },
    "percentiles3": {
        "total": "43697",
        "ok": "43697",
        "ko": "-"
    },
    "percentiles4": {
        "total": "43697",
        "ok": "43697",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-9b90a": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_114569",
path: "Animal-api sync for customer NL_114569",
pathFormatted: "req_animal-api-sync-9b90a",
stats: {
    "name": "Animal-api sync for customer NL_114569",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "9467",
        "ok": "9467",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "9467",
        "ok": "9467",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "9467",
        "ok": "9467",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "9467",
        "ok": "9467",
        "ko": "-"
    },
    "percentiles2": {
        "total": "9467",
        "ok": "9467",
        "ko": "-"
    },
    "percentiles3": {
        "total": "9467",
        "ok": "9467",
        "ko": "-"
    },
    "percentiles4": {
        "total": "9467",
        "ok": "9467",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-56e97": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_130716",
path: "Animal-api sync for customer NL_130716",
pathFormatted: "req_animal-api-sync-56e97",
stats: {
    "name": "Animal-api sync for customer NL_130716",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "11874",
        "ok": "11874",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "11874",
        "ok": "11874",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "11874",
        "ok": "11874",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "11874",
        "ok": "11874",
        "ko": "-"
    },
    "percentiles2": {
        "total": "11874",
        "ok": "11874",
        "ko": "-"
    },
    "percentiles3": {
        "total": "11874",
        "ok": "11874",
        "ko": "-"
    },
    "percentiles4": {
        "total": "11874",
        "ok": "11874",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-4b1c0": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_164635",
path: "Animal-api sync for customer BE_164635",
pathFormatted: "req_animal-api-sync-4b1c0",
stats: {
    "name": "Animal-api sync for customer BE_164635",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "9675",
        "ok": "9675",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "9675",
        "ok": "9675",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "9675",
        "ok": "9675",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "9675",
        "ok": "9675",
        "ko": "-"
    },
    "percentiles2": {
        "total": "9675",
        "ok": "9675",
        "ko": "-"
    },
    "percentiles3": {
        "total": "9675",
        "ok": "9675",
        "ko": "-"
    },
    "percentiles4": {
        "total": "9675",
        "ok": "9675",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-0be59": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_137530",
path: "Animal-api sync for customer NL_137530",
pathFormatted: "req_animal-api-sync-0be59",
stats: {
    "name": "Animal-api sync for customer NL_137530",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "8099",
        "ok": "8099",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "8099",
        "ok": "8099",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "8099",
        "ok": "8099",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "8099",
        "ok": "8099",
        "ko": "-"
    },
    "percentiles2": {
        "total": "8099",
        "ok": "8099",
        "ko": "-"
    },
    "percentiles3": {
        "total": "8099",
        "ok": "8099",
        "ko": "-"
    },
    "percentiles4": {
        "total": "8099",
        "ok": "8099",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e4170": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_205597",
path: "Animal-api sync for customer BE_205597",
pathFormatted: "req_animal-api-sync-e4170",
stats: {
    "name": "Animal-api sync for customer BE_205597",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "17027",
        "ok": "17027",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "17027",
        "ok": "17027",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "17027",
        "ok": "17027",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "17027",
        "ok": "17027",
        "ko": "-"
    },
    "percentiles2": {
        "total": "17027",
        "ok": "17027",
        "ko": "-"
    },
    "percentiles3": {
        "total": "17027",
        "ok": "17027",
        "ko": "-"
    },
    "percentiles4": {
        "total": "17027",
        "ok": "17027",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-5e066": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_118716",
path: "Animal-api sync for customer NL_118716",
pathFormatted: "req_animal-api-sync-5e066",
stats: {
    "name": "Animal-api sync for customer NL_118716",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "13969",
        "ok": "13969",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "13969",
        "ok": "13969",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "13969",
        "ok": "13969",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "13969",
        "ok": "13969",
        "ko": "-"
    },
    "percentiles2": {
        "total": "13969",
        "ok": "13969",
        "ko": "-"
    },
    "percentiles3": {
        "total": "13969",
        "ok": "13969",
        "ko": "-"
    },
    "percentiles4": {
        "total": "13969",
        "ok": "13969",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-3598c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_107584",
path: "Animal-api sync for customer NL_107584",
pathFormatted: "req_animal-api-sync-3598c",
stats: {
    "name": "Animal-api sync for customer NL_107584",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "16775",
        "ok": "16775",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "16775",
        "ok": "16775",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "16775",
        "ok": "16775",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "16775",
        "ok": "16775",
        "ko": "-"
    },
    "percentiles2": {
        "total": "16775",
        "ok": "16775",
        "ko": "-"
    },
    "percentiles3": {
        "total": "16775",
        "ok": "16775",
        "ko": "-"
    },
    "percentiles4": {
        "total": "16775",
        "ok": "16775",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-6b068": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111950",
path: "Animal-api sync for customer NL_111950",
pathFormatted: "req_animal-api-sync-6b068",
stats: {
    "name": "Animal-api sync for customer NL_111950",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "30405",
        "ok": "30405",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "30405",
        "ok": "30405",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "30405",
        "ok": "30405",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "30405",
        "ok": "30405",
        "ko": "-"
    },
    "percentiles2": {
        "total": "30405",
        "ok": "30405",
        "ko": "-"
    },
    "percentiles3": {
        "total": "30405",
        "ok": "30405",
        "ko": "-"
    },
    "percentiles4": {
        "total": "30405",
        "ok": "30405",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-34e6f": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_129959",
path: "Animal-api sync for customer NL_129959",
pathFormatted: "req_animal-api-sync-34e6f",
stats: {
    "name": "Animal-api sync for customer NL_129959",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "18296",
        "ok": "18296",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "18296",
        "ok": "18296",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "18296",
        "ok": "18296",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "18296",
        "ok": "18296",
        "ko": "-"
    },
    "percentiles2": {
        "total": "18296",
        "ok": "18296",
        "ko": "-"
    },
    "percentiles3": {
        "total": "18296",
        "ok": "18296",
        "ko": "-"
    },
    "percentiles4": {
        "total": "18296",
        "ok": "18296",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-21b03": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_109721",
path: "Animal-api sync for customer NL_109721",
pathFormatted: "req_animal-api-sync-21b03",
stats: {
    "name": "Animal-api sync for customer NL_109721",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "33278",
        "ok": "33278",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "33278",
        "ok": "33278",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "33278",
        "ok": "33278",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "33278",
        "ok": "33278",
        "ko": "-"
    },
    "percentiles2": {
        "total": "33278",
        "ok": "33278",
        "ko": "-"
    },
    "percentiles3": {
        "total": "33278",
        "ok": "33278",
        "ko": "-"
    },
    "percentiles4": {
        "total": "33278",
        "ok": "33278",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-24d15": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117331",
path: "Animal-api sync for customer NL_117331",
pathFormatted: "req_animal-api-sync-24d15",
stats: {
    "name": "Animal-api sync for customer NL_117331",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "19550",
        "ok": "19550",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "19550",
        "ok": "19550",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "19550",
        "ok": "19550",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "19550",
        "ok": "19550",
        "ko": "-"
    },
    "percentiles2": {
        "total": "19550",
        "ok": "19550",
        "ko": "-"
    },
    "percentiles3": {
        "total": "19550",
        "ok": "19550",
        "ko": "-"
    },
    "percentiles4": {
        "total": "19550",
        "ok": "19550",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-4479f": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_134641",
path: "Animal-api sync for customer NL_134641",
pathFormatted: "req_animal-api-sync-4479f",
stats: {
    "name": "Animal-api sync for customer NL_134641",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2288",
        "ok": "2288",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2288",
        "ok": "2288",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2288",
        "ok": "2288",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2288",
        "ok": "2288",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2288",
        "ok": "2288",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2288",
        "ok": "2288",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2288",
        "ok": "2288",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-2ac98": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_119983",
path: "Animal-api sync for customer NL_119983",
pathFormatted: "req_animal-api-sync-2ac98",
stats: {
    "name": "Animal-api sync for customer NL_119983",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "maxResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "meanResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles4": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-53e53": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_122724",
path: "Animal-api sync for customer NL_122724",
pathFormatted: "req_animal-api-sync-53e53",
stats: {
    "name": "Animal-api sync for customer NL_122724",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "28203",
        "ok": "28203",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "28203",
        "ok": "28203",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "28203",
        "ok": "28203",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "28203",
        "ok": "28203",
        "ko": "-"
    },
    "percentiles2": {
        "total": "28203",
        "ok": "28203",
        "ko": "-"
    },
    "percentiles3": {
        "total": "28203",
        "ok": "28203",
        "ko": "-"
    },
    "percentiles4": {
        "total": "28203",
        "ok": "28203",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-97824": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_129903",
path: "Animal-api sync for customer NL_129903",
pathFormatted: "req_animal-api-sync-97824",
stats: {
    "name": "Animal-api sync for customer NL_129903",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "26358",
        "ok": "26358",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "26358",
        "ok": "26358",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "26358",
        "ok": "26358",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "26358",
        "ok": "26358",
        "ko": "-"
    },
    "percentiles2": {
        "total": "26358",
        "ok": "26358",
        "ko": "-"
    },
    "percentiles3": {
        "total": "26358",
        "ok": "26358",
        "ko": "-"
    },
    "percentiles4": {
        "total": "26358",
        "ok": "26358",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-9db7a": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_125745",
path: "Animal-api sync for customer NL_125745",
pathFormatted: "req_animal-api-sync-9db7a",
stats: {
    "name": "Animal-api sync for customer NL_125745",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "maxResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "meanResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles2": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles3": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles4": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-62afe": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_128808",
path: "Animal-api sync for customer NL_128808",
pathFormatted: "req_animal-api-sync-62afe",
stats: {
    "name": "Animal-api sync for customer NL_128808",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "16525",
        "ok": "16525",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "16525",
        "ok": "16525",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "16525",
        "ok": "16525",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "16525",
        "ok": "16525",
        "ko": "-"
    },
    "percentiles2": {
        "total": "16525",
        "ok": "16525",
        "ko": "-"
    },
    "percentiles3": {
        "total": "16525",
        "ok": "16525",
        "ko": "-"
    },
    "percentiles4": {
        "total": "16525",
        "ok": "16525",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d8b1c": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_214192",
path: "Animal-api sync for customer BE_214192",
pathFormatted: "req_animal-api-sync-d8b1c",
stats: {
    "name": "Animal-api sync for customer BE_214192",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1813",
        "ok": "1813",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1813",
        "ok": "1813",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1813",
        "ok": "1813",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1813",
        "ok": "1813",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1813",
        "ok": "1813",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1813",
        "ok": "1813",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1813",
        "ok": "1813",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-22fcc": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110755",
path: "Animal-api sync for customer NL_110755",
pathFormatted: "req_animal-api-sync-22fcc",
stats: {
    "name": "Animal-api sync for customer NL_110755",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "23791",
        "ok": "23791",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "23791",
        "ok": "23791",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "23791",
        "ok": "23791",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "23791",
        "ok": "23791",
        "ko": "-"
    },
    "percentiles2": {
        "total": "23791",
        "ok": "23791",
        "ko": "-"
    },
    "percentiles3": {
        "total": "23791",
        "ok": "23791",
        "ko": "-"
    },
    "percentiles4": {
        "total": "23791",
        "ok": "23791",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-1330d": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_145902",
path: "Animal-api sync for customer BE_145902",
pathFormatted: "req_animal-api-sync-1330d",
stats: {
    "name": "Animal-api sync for customer BE_145902",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "12396",
        "ok": "12396",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "12396",
        "ok": "12396",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "12396",
        "ok": "12396",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "12396",
        "ok": "12396",
        "ko": "-"
    },
    "percentiles2": {
        "total": "12396",
        "ok": "12396",
        "ko": "-"
    },
    "percentiles3": {
        "total": "12396",
        "ok": "12396",
        "ko": "-"
    },
    "percentiles4": {
        "total": "12396",
        "ok": "12396",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-2cece": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110572",
path: "Animal-api sync for customer NL_110572",
pathFormatted: "req_animal-api-sync-2cece",
stats: {
    "name": "Animal-api sync for customer NL_110572",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "26249",
        "ok": "26249",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "26249",
        "ok": "26249",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "26249",
        "ok": "26249",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "26249",
        "ok": "26249",
        "ko": "-"
    },
    "percentiles2": {
        "total": "26249",
        "ok": "26249",
        "ko": "-"
    },
    "percentiles3": {
        "total": "26249",
        "ok": "26249",
        "ko": "-"
    },
    "percentiles4": {
        "total": "26249",
        "ok": "26249",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-184df": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110881",
path: "Animal-api sync for customer NL_110881",
pathFormatted: "req_animal-api-sync-184df",
stats: {
    "name": "Animal-api sync for customer NL_110881",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "23206",
        "ok": "23206",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "23206",
        "ok": "23206",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "23206",
        "ok": "23206",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "23206",
        "ok": "23206",
        "ko": "-"
    },
    "percentiles2": {
        "total": "23206",
        "ok": "23206",
        "ko": "-"
    },
    "percentiles3": {
        "total": "23206",
        "ok": "23206",
        "ko": "-"
    },
    "percentiles4": {
        "total": "23206",
        "ok": "23206",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-61fa0": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_214412",
path: "Animal-api sync for customer NL_214412",
pathFormatted: "req_animal-api-sync-61fa0",
stats: {
    "name": "Animal-api sync for customer NL_214412",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "13073",
        "ok": "13073",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "13073",
        "ok": "13073",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "13073",
        "ok": "13073",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "13073",
        "ok": "13073",
        "ko": "-"
    },
    "percentiles2": {
        "total": "13073",
        "ok": "13073",
        "ko": "-"
    },
    "percentiles3": {
        "total": "13073",
        "ok": "13073",
        "ko": "-"
    },
    "percentiles4": {
        "total": "13073",
        "ok": "13073",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-2f673": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_112286",
path: "Animal-api sync for customer NL_112286",
pathFormatted: "req_animal-api-sync-2f673",
stats: {
    "name": "Animal-api sync for customer NL_112286",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "2568",
        "ok": "2568",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2568",
        "ok": "2568",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "2568",
        "ok": "2568",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "2568",
        "ok": "2568",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2568",
        "ok": "2568",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2568",
        "ok": "2568",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2568",
        "ok": "2568",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-7a391": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_125864",
path: "Animal-api sync for customer NL_125864",
pathFormatted: "req_animal-api-sync-7a391",
stats: {
    "name": "Animal-api sync for customer NL_125864",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "23570",
        "ok": "23570",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "23570",
        "ok": "23570",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "23570",
        "ok": "23570",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "23570",
        "ok": "23570",
        "ko": "-"
    },
    "percentiles2": {
        "total": "23570",
        "ok": "23570",
        "ko": "-"
    },
    "percentiles3": {
        "total": "23570",
        "ok": "23570",
        "ko": "-"
    },
    "percentiles4": {
        "total": "23570",
        "ok": "23570",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-844f0": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_128832",
path: "Animal-api sync for customer NL_128832",
pathFormatted: "req_animal-api-sync-844f0",
stats: {
    "name": "Animal-api sync for customer NL_128832",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "49656",
        "ok": "49656",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "49656",
        "ok": "49656",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "49656",
        "ok": "49656",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "49656",
        "ok": "49656",
        "ko": "-"
    },
    "percentiles2": {
        "total": "49656",
        "ok": "49656",
        "ko": "-"
    },
    "percentiles3": {
        "total": "49656",
        "ok": "49656",
        "ko": "-"
    },
    "percentiles4": {
        "total": "49656",
        "ok": "49656",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-4e25d": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104043",
path: "Animal-api sync for customer NL_104043",
pathFormatted: "req_animal-api-sync-4e25d",
stats: {
    "name": "Animal-api sync for customer NL_104043",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "33910",
        "ok": "33910",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "33910",
        "ok": "33910",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "33910",
        "ok": "33910",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "33910",
        "ok": "33910",
        "ko": "-"
    },
    "percentiles2": {
        "total": "33910",
        "ok": "33910",
        "ko": "-"
    },
    "percentiles3": {
        "total": "33910",
        "ok": "33910",
        "ko": "-"
    },
    "percentiles4": {
        "total": "33910",
        "ok": "33910",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-25426": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_114351",
path: "Animal-api sync for customer NL_114351",
pathFormatted: "req_animal-api-sync-25426",
stats: {
    "name": "Animal-api sync for customer NL_114351",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "8761",
        "ok": "8761",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "8761",
        "ok": "8761",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "8761",
        "ok": "8761",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "8761",
        "ok": "8761",
        "ko": "-"
    },
    "percentiles2": {
        "total": "8761",
        "ok": "8761",
        "ko": "-"
    },
    "percentiles3": {
        "total": "8761",
        "ok": "8761",
        "ko": "-"
    },
    "percentiles4": {
        "total": "8761",
        "ok": "8761",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-dfb65": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_108668",
path: "Animal-api sync for customer NL_108668",
pathFormatted: "req_animal-api-sync-dfb65",
stats: {
    "name": "Animal-api sync for customer NL_108668",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "26732",
        "ok": "26732",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "26732",
        "ok": "26732",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "26732",
        "ok": "26732",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "26732",
        "ok": "26732",
        "ko": "-"
    },
    "percentiles2": {
        "total": "26732",
        "ok": "26732",
        "ko": "-"
    },
    "percentiles3": {
        "total": "26732",
        "ok": "26732",
        "ko": "-"
    },
    "percentiles4": {
        "total": "26732",
        "ok": "26732",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-997d2": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111823",
path: "Animal-api sync for customer NL_111823",
pathFormatted: "req_animal-api-sync-997d2",
stats: {
    "name": "Animal-api sync for customer NL_111823",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "maxResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "meanResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles4": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-d5cc8": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_103579",
path: "Animal-api sync for customer NL_103579",
pathFormatted: "req_animal-api-sync-d5cc8",
stats: {
    "name": "Animal-api sync for customer NL_103579",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "22723",
        "ok": "22723",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "22723",
        "ok": "22723",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "22723",
        "ok": "22723",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "22723",
        "ok": "22723",
        "ko": "-"
    },
    "percentiles2": {
        "total": "22723",
        "ok": "22723",
        "ko": "-"
    },
    "percentiles3": {
        "total": "22723",
        "ok": "22723",
        "ko": "-"
    },
    "percentiles4": {
        "total": "22723",
        "ok": "22723",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-96467": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_134577",
path: "Animal-api sync for customer NL_134577",
pathFormatted: "req_animal-api-sync-96467",
stats: {
    "name": "Animal-api sync for customer NL_134577",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "17058",
        "ok": "17058",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "17058",
        "ok": "17058",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "17058",
        "ok": "17058",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "17058",
        "ok": "17058",
        "ko": "-"
    },
    "percentiles2": {
        "total": "17058",
        "ok": "17058",
        "ko": "-"
    },
    "percentiles3": {
        "total": "17058",
        "ok": "17058",
        "ko": "-"
    },
    "percentiles4": {
        "total": "17058",
        "ok": "17058",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-4c254": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105650",
path: "Animal-api sync for customer NL_105650",
pathFormatted: "req_animal-api-sync-4c254",
stats: {
    "name": "Animal-api sync for customer NL_105650",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "maxResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "meanResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles2": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles3": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles4": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-5ea6b": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_194847",
path: "Animal-api sync for customer BE_194847",
pathFormatted: "req_animal-api-sync-5ea6b",
stats: {
    "name": "Animal-api sync for customer BE_194847",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "55999",
        "ok": "55999",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "55999",
        "ok": "55999",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "55999",
        "ok": "55999",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "55999",
        "ok": "55999",
        "ko": "-"
    },
    "percentiles2": {
        "total": "55999",
        "ok": "55999",
        "ko": "-"
    },
    "percentiles3": {
        "total": "55999",
        "ok": "55999",
        "ko": "-"
    },
    "percentiles4": {
        "total": "55999",
        "ok": "55999",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a4924": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117341",
path: "Animal-api sync for customer NL_117341",
pathFormatted: "req_animal-api-sync-a4924",
stats: {
    "name": "Animal-api sync for customer NL_117341",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "15018",
        "ok": "15018",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "15018",
        "ok": "15018",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "15018",
        "ok": "15018",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "15018",
        "ok": "15018",
        "ko": "-"
    },
    "percentiles2": {
        "total": "15018",
        "ok": "15018",
        "ko": "-"
    },
    "percentiles3": {
        "total": "15018",
        "ok": "15018",
        "ko": "-"
    },
    "percentiles4": {
        "total": "15018",
        "ok": "15018",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-12331": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_113115",
path: "Animal-api sync for customer NL_113115",
pathFormatted: "req_animal-api-sync-12331",
stats: {
    "name": "Animal-api sync for customer NL_113115",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "43846",
        "ok": "43846",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "43846",
        "ok": "43846",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "43846",
        "ok": "43846",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "43846",
        "ok": "43846",
        "ko": "-"
    },
    "percentiles2": {
        "total": "43846",
        "ok": "43846",
        "ko": "-"
    },
    "percentiles3": {
        "total": "43846",
        "ok": "43846",
        "ko": "-"
    },
    "percentiles4": {
        "total": "43846",
        "ok": "43846",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-13f62": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_149314",
path: "Animal-api sync for customer BE_149314",
pathFormatted: "req_animal-api-sync-13f62",
stats: {
    "name": "Animal-api sync for customer BE_149314",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "17180",
        "ok": "17180",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "17180",
        "ok": "17180",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "17180",
        "ok": "17180",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "17180",
        "ok": "17180",
        "ko": "-"
    },
    "percentiles2": {
        "total": "17180",
        "ok": "17180",
        "ko": "-"
    },
    "percentiles3": {
        "total": "17180",
        "ok": "17180",
        "ko": "-"
    },
    "percentiles4": {
        "total": "17180",
        "ok": "17180",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-500bc": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117068",
path: "Animal-api sync for customer NL_117068",
pathFormatted: "req_animal-api-sync-500bc",
stats: {
    "name": "Animal-api sync for customer NL_117068",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "11373",
        "ok": "11373",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "11373",
        "ok": "11373",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "11373",
        "ok": "11373",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "11373",
        "ok": "11373",
        "ko": "-"
    },
    "percentiles2": {
        "total": "11373",
        "ok": "11373",
        "ko": "-"
    },
    "percentiles3": {
        "total": "11373",
        "ok": "11373",
        "ko": "-"
    },
    "percentiles4": {
        "total": "11373",
        "ok": "11373",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-6d4c5": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_122660",
path: "Animal-api sync for customer NL_122660",
pathFormatted: "req_animal-api-sync-6d4c5",
stats: {
    "name": "Animal-api sync for customer NL_122660",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "3125",
        "ok": "3125",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3125",
        "ok": "3125",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3125",
        "ok": "3125",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "3125",
        "ok": "3125",
        "ko": "-"
    },
    "percentiles2": {
        "total": "3125",
        "ok": "3125",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3125",
        "ok": "3125",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3125",
        "ok": "3125",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-cb570": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104777",
path: "Animal-api sync for customer NL_104777",
pathFormatted: "req_animal-api-sync-cb570",
stats: {
    "name": "Animal-api sync for customer NL_104777",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "maxResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "meanResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles4": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-49e6e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_122219",
path: "Animal-api sync for customer NL_122219",
pathFormatted: "req_animal-api-sync-49e6e",
stats: {
    "name": "Animal-api sync for customer NL_122219",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "16146",
        "ok": "16146",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "16146",
        "ok": "16146",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "16146",
        "ok": "16146",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "16146",
        "ok": "16146",
        "ko": "-"
    },
    "percentiles2": {
        "total": "16146",
        "ok": "16146",
        "ko": "-"
    },
    "percentiles3": {
        "total": "16146",
        "ok": "16146",
        "ko": "-"
    },
    "percentiles4": {
        "total": "16146",
        "ok": "16146",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b0552": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_122008",
path: "Animal-api sync for customer NL_122008",
pathFormatted: "req_animal-api-sync-b0552",
stats: {
    "name": "Animal-api sync for customer NL_122008",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "maxResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "meanResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles2": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles3": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles4": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-7d526": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105576",
path: "Animal-api sync for customer NL_105576",
pathFormatted: "req_animal-api-sync-7d526",
stats: {
    "name": "Animal-api sync for customer NL_105576",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "25097",
        "ok": "25097",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "25097",
        "ok": "25097",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "25097",
        "ok": "25097",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "25097",
        "ok": "25097",
        "ko": "-"
    },
    "percentiles2": {
        "total": "25097",
        "ok": "25097",
        "ko": "-"
    },
    "percentiles3": {
        "total": "25097",
        "ok": "25097",
        "ko": "-"
    },
    "percentiles4": {
        "total": "25097",
        "ok": "25097",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a6cf9": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104168",
path: "Animal-api sync for customer NL_104168",
pathFormatted: "req_animal-api-sync-a6cf9",
stats: {
    "name": "Animal-api sync for customer NL_104168",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "maxResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "meanResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles2": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles3": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles4": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-6809e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117264",
path: "Animal-api sync for customer NL_117264",
pathFormatted: "req_animal-api-sync-6809e",
stats: {
    "name": "Animal-api sync for customer NL_117264",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "maxResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "meanResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles2": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles3": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles4": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-cb15a": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_114531",
path: "Animal-api sync for customer NL_114531",
pathFormatted: "req_animal-api-sync-cb15a",
stats: {
    "name": "Animal-api sync for customer NL_114531",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "27211",
        "ok": "27211",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "27211",
        "ok": "27211",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "27211",
        "ok": "27211",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "27211",
        "ok": "27211",
        "ko": "-"
    },
    "percentiles2": {
        "total": "27211",
        "ok": "27211",
        "ko": "-"
    },
    "percentiles3": {
        "total": "27211",
        "ok": "27211",
        "ko": "-"
    },
    "percentiles4": {
        "total": "27211",
        "ok": "27211",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-aa469": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_158913",
path: "Animal-api sync for customer BE_158913",
pathFormatted: "req_animal-api-sync-aa469",
stats: {
    "name": "Animal-api sync for customer BE_158913",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "38629",
        "ok": "38629",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "38629",
        "ok": "38629",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "38629",
        "ok": "38629",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "38629",
        "ok": "38629",
        "ko": "-"
    },
    "percentiles2": {
        "total": "38629",
        "ok": "38629",
        "ko": "-"
    },
    "percentiles3": {
        "total": "38629",
        "ok": "38629",
        "ko": "-"
    },
    "percentiles4": {
        "total": "38629",
        "ok": "38629",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-fc480": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_121609",
path: "Animal-api sync for customer NL_121609",
pathFormatted: "req_animal-api-sync-fc480",
stats: {
    "name": "Animal-api sync for customer NL_121609",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "maxResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "meanResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles2": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles3": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles4": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-2fa8f": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_103719",
path: "Animal-api sync for customer NL_103719",
pathFormatted: "req_animal-api-sync-2fa8f",
stats: {
    "name": "Animal-api sync for customer NL_103719",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "maxResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "meanResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles4": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-c7578": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105839",
path: "Animal-api sync for customer NL_105839",
pathFormatted: "req_animal-api-sync-c7578",
stats: {
    "name": "Animal-api sync for customer NL_105839",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "maxResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "meanResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles2": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles3": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles4": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-ca9a4": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_123159",
path: "Animal-api sync for customer NL_123159",
pathFormatted: "req_animal-api-sync-ca9a4",
stats: {
    "name": "Animal-api sync for customer NL_123159",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "10606",
        "ok": "10606",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "10606",
        "ok": "10606",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "10606",
        "ok": "10606",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "10606",
        "ok": "10606",
        "ko": "-"
    },
    "percentiles2": {
        "total": "10606",
        "ok": "10606",
        "ko": "-"
    },
    "percentiles3": {
        "total": "10606",
        "ok": "10606",
        "ko": "-"
    },
    "percentiles4": {
        "total": "10606",
        "ok": "10606",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d58c5": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_115162",
path: "Animal-api sync for customer NL_115162",
pathFormatted: "req_animal-api-sync-d58c5",
stats: {
    "name": "Animal-api sync for customer NL_115162",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4012",
        "ok": "4012",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4012",
        "ok": "4012",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4012",
        "ok": "4012",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4012",
        "ok": "4012",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4012",
        "ok": "4012",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4012",
        "ok": "4012",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4012",
        "ok": "4012",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d43d8": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_112453",
path: "Animal-api sync for customer NL_112453",
pathFormatted: "req_animal-api-sync-d43d8",
stats: {
    "name": "Animal-api sync for customer NL_112453",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "20738",
        "ok": "20738",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "20738",
        "ok": "20738",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "20738",
        "ok": "20738",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "20738",
        "ok": "20738",
        "ko": "-"
    },
    "percentiles2": {
        "total": "20738",
        "ok": "20738",
        "ko": "-"
    },
    "percentiles3": {
        "total": "20738",
        "ok": "20738",
        "ko": "-"
    },
    "percentiles4": {
        "total": "20738",
        "ok": "20738",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-fa50f": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_194938",
path: "Animal-api sync for customer NL_194938",
pathFormatted: "req_animal-api-sync-fa50f",
stats: {
    "name": "Animal-api sync for customer NL_194938",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "maxResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "meanResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles4": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-d3cf7": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_123704",
path: "Animal-api sync for customer NL_123704",
pathFormatted: "req_animal-api-sync-d3cf7",
stats: {
    "name": "Animal-api sync for customer NL_123704",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "11250",
        "ok": "11250",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "11250",
        "ok": "11250",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "11250",
        "ok": "11250",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "11250",
        "ok": "11250",
        "ko": "-"
    },
    "percentiles2": {
        "total": "11250",
        "ok": "11250",
        "ko": "-"
    },
    "percentiles3": {
        "total": "11250",
        "ok": "11250",
        "ko": "-"
    },
    "percentiles4": {
        "total": "11250",
        "ok": "11250",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e5ad2": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_108858",
path: "Animal-api sync for customer NL_108858",
pathFormatted: "req_animal-api-sync-e5ad2",
stats: {
    "name": "Animal-api sync for customer NL_108858",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "35347",
        "ok": "35347",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "35347",
        "ok": "35347",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "35347",
        "ok": "35347",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "35347",
        "ok": "35347",
        "ko": "-"
    },
    "percentiles2": {
        "total": "35347",
        "ok": "35347",
        "ko": "-"
    },
    "percentiles3": {
        "total": "35347",
        "ok": "35347",
        "ko": "-"
    },
    "percentiles4": {
        "total": "35347",
        "ok": "35347",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-23adc": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110836",
path: "Animal-api sync for customer NL_110836",
pathFormatted: "req_animal-api-sync-23adc",
stats: {
    "name": "Animal-api sync for customer NL_110836",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "19826",
        "ok": "19826",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "19826",
        "ok": "19826",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "19826",
        "ok": "19826",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "19826",
        "ok": "19826",
        "ko": "-"
    },
    "percentiles2": {
        "total": "19826",
        "ok": "19826",
        "ko": "-"
    },
    "percentiles3": {
        "total": "19826",
        "ok": "19826",
        "ko": "-"
    },
    "percentiles4": {
        "total": "19826",
        "ok": "19826",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-8d897": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_133100",
path: "Animal-api sync for customer NL_133100",
pathFormatted: "req_animal-api-sync-8d897",
stats: {
    "name": "Animal-api sync for customer NL_133100",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "maxResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "meanResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles2": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles3": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles4": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-e921e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_122446",
path: "Animal-api sync for customer NL_122446",
pathFormatted: "req_animal-api-sync-e921e",
stats: {
    "name": "Animal-api sync for customer NL_122446",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "37657",
        "ok": "37657",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "37657",
        "ok": "37657",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "37657",
        "ok": "37657",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "37657",
        "ok": "37657",
        "ko": "-"
    },
    "percentiles2": {
        "total": "37657",
        "ok": "37657",
        "ko": "-"
    },
    "percentiles3": {
        "total": "37657",
        "ok": "37657",
        "ko": "-"
    },
    "percentiles4": {
        "total": "37657",
        "ok": "37657",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-fd312": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117690",
path: "Animal-api sync for customer NL_117690",
pathFormatted: "req_animal-api-sync-fd312",
stats: {
    "name": "Animal-api sync for customer NL_117690",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "36575",
        "ok": "36575",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "36575",
        "ok": "36575",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "36575",
        "ok": "36575",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "36575",
        "ok": "36575",
        "ko": "-"
    },
    "percentiles2": {
        "total": "36575",
        "ok": "36575",
        "ko": "-"
    },
    "percentiles3": {
        "total": "36575",
        "ok": "36575",
        "ko": "-"
    },
    "percentiles4": {
        "total": "36575",
        "ok": "36575",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c6f3a": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_119414",
path: "Animal-api sync for customer NL_119414",
pathFormatted: "req_animal-api-sync-c6f3a",
stats: {
    "name": "Animal-api sync for customer NL_119414",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "32584",
        "ok": "32584",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "32584",
        "ok": "32584",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "32584",
        "ok": "32584",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "32584",
        "ok": "32584",
        "ko": "-"
    },
    "percentiles2": {
        "total": "32584",
        "ok": "32584",
        "ko": "-"
    },
    "percentiles3": {
        "total": "32584",
        "ok": "32584",
        "ko": "-"
    },
    "percentiles4": {
        "total": "32584",
        "ok": "32584",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a8229": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_129350",
path: "Animal-api sync for customer NL_129350",
pathFormatted: "req_animal-api-sync-a8229",
stats: {
    "name": "Animal-api sync for customer NL_129350",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "maxResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "meanResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles2": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles3": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles4": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-e7e4c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105106",
path: "Animal-api sync for customer NL_105106",
pathFormatted: "req_animal-api-sync-e7e4c",
stats: {
    "name": "Animal-api sync for customer NL_105106",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "23200",
        "ok": "23200",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "23200",
        "ok": "23200",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "23200",
        "ok": "23200",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "23200",
        "ok": "23200",
        "ko": "-"
    },
    "percentiles2": {
        "total": "23200",
        "ok": "23200",
        "ko": "-"
    },
    "percentiles3": {
        "total": "23200",
        "ok": "23200",
        "ko": "-"
    },
    "percentiles4": {
        "total": "23200",
        "ok": "23200",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c3423": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_112704",
path: "Animal-api sync for customer NL_112704",
pathFormatted: "req_animal-api-sync-c3423",
stats: {
    "name": "Animal-api sync for customer NL_112704",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "maxResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "meanResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles2": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles3": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles4": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-c2f39": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_157878",
path: "Animal-api sync for customer BE_157878",
pathFormatted: "req_animal-api-sync-c2f39",
stats: {
    "name": "Animal-api sync for customer BE_157878",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "29649",
        "ok": "29649",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "29649",
        "ok": "29649",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "29649",
        "ok": "29649",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "29649",
        "ok": "29649",
        "ko": "-"
    },
    "percentiles2": {
        "total": "29649",
        "ok": "29649",
        "ko": "-"
    },
    "percentiles3": {
        "total": "29649",
        "ok": "29649",
        "ko": "-"
    },
    "percentiles4": {
        "total": "29649",
        "ok": "29649",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-fee59": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117006",
path: "Animal-api sync for customer NL_117006",
pathFormatted: "req_animal-api-sync-fee59",
stats: {
    "name": "Animal-api sync for customer NL_117006",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4909",
        "ok": "4909",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4909",
        "ok": "4909",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4909",
        "ok": "4909",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4909",
        "ok": "4909",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4909",
        "ok": "4909",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4909",
        "ok": "4909",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4909",
        "ok": "4909",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-bab68": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_134874",
path: "Animal-api sync for customer NL_134874",
pathFormatted: "req_animal-api-sync-bab68",
stats: {
    "name": "Animal-api sync for customer NL_134874",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "38735",
        "ok": "38735",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "38735",
        "ok": "38735",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "38735",
        "ok": "38735",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "38735",
        "ok": "38735",
        "ko": "-"
    },
    "percentiles2": {
        "total": "38735",
        "ok": "38735",
        "ko": "-"
    },
    "percentiles3": {
        "total": "38735",
        "ok": "38735",
        "ko": "-"
    },
    "percentiles4": {
        "total": "38735",
        "ok": "38735",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-3e99c": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_214193",
path: "Animal-api sync for customer BE_214193",
pathFormatted: "req_animal-api-sync-3e99c",
stats: {
    "name": "Animal-api sync for customer BE_214193",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "57238",
        "ok": "57238",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "57238",
        "ok": "57238",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "57238",
        "ok": "57238",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "57238",
        "ok": "57238",
        "ko": "-"
    },
    "percentiles2": {
        "total": "57238",
        "ok": "57238",
        "ko": "-"
    },
    "percentiles3": {
        "total": "57238",
        "ok": "57238",
        "ko": "-"
    },
    "percentiles4": {
        "total": "57238",
        "ok": "57238",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b2bc0": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_162281",
path: "Animal-api sync for customer BE_162281",
pathFormatted: "req_animal-api-sync-b2bc0",
stats: {
    "name": "Animal-api sync for customer BE_162281",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "maxResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "meanResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles2": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles3": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles4": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-046eb": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_127329",
path: "Animal-api sync for customer NL_127329",
pathFormatted: "req_animal-api-sync-046eb",
stats: {
    "name": "Animal-api sync for customer NL_127329",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "1590",
        "ok": "1590",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1590",
        "ok": "1590",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1590",
        "ok": "1590",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "1590",
        "ok": "1590",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1590",
        "ok": "1590",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1590",
        "ok": "1590",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1590",
        "ok": "1590",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-7cc26": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_131009",
path: "Animal-api sync for customer NL_131009",
pathFormatted: "req_animal-api-sync-7cc26",
stats: {
    "name": "Animal-api sync for customer NL_131009",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "51127",
        "ok": "51127",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "51127",
        "ok": "51127",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "51127",
        "ok": "51127",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "51127",
        "ok": "51127",
        "ko": "-"
    },
    "percentiles2": {
        "total": "51127",
        "ok": "51127",
        "ko": "-"
    },
    "percentiles3": {
        "total": "51127",
        "ok": "51127",
        "ko": "-"
    },
    "percentiles4": {
        "total": "51127",
        "ok": "51127",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-52d91": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_163987",
path: "Animal-api sync for customer BE_163987",
pathFormatted: "req_animal-api-sync-52d91",
stats: {
    "name": "Animal-api sync for customer BE_163987",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "17046",
        "ok": "17046",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "17046",
        "ok": "17046",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "17046",
        "ok": "17046",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "17046",
        "ok": "17046",
        "ko": "-"
    },
    "percentiles2": {
        "total": "17046",
        "ok": "17046",
        "ko": "-"
    },
    "percentiles3": {
        "total": "17046",
        "ok": "17046",
        "ko": "-"
    },
    "percentiles4": {
        "total": "17046",
        "ok": "17046",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-9b768": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_115732",
path: "Animal-api sync for customer NL_115732",
pathFormatted: "req_animal-api-sync-9b768",
stats: {
    "name": "Animal-api sync for customer NL_115732",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "34095",
        "ok": "34095",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "34095",
        "ok": "34095",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "34095",
        "ok": "34095",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "34095",
        "ok": "34095",
        "ko": "-"
    },
    "percentiles2": {
        "total": "34095",
        "ok": "34095",
        "ko": "-"
    },
    "percentiles3": {
        "total": "34095",
        "ok": "34095",
        "ko": "-"
    },
    "percentiles4": {
        "total": "34095",
        "ok": "34095",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c1337": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104350",
path: "Animal-api sync for customer NL_104350",
pathFormatted: "req_animal-api-sync-c1337",
stats: {
    "name": "Animal-api sync for customer NL_104350",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "maxResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "meanResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles2": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles3": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles4": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-de861": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_138595",
path: "Animal-api sync for customer NL_138595",
pathFormatted: "req_animal-api-sync-de861",
stats: {
    "name": "Animal-api sync for customer NL_138595",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "10458",
        "ok": "10458",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "10458",
        "ok": "10458",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "10458",
        "ok": "10458",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "10458",
        "ok": "10458",
        "ko": "-"
    },
    "percentiles2": {
        "total": "10458",
        "ok": "10458",
        "ko": "-"
    },
    "percentiles3": {
        "total": "10458",
        "ok": "10458",
        "ko": "-"
    },
    "percentiles4": {
        "total": "10458",
        "ok": "10458",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-cc8e5": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_123944",
path: "Animal-api sync for customer NL_123944",
pathFormatted: "req_animal-api-sync-cc8e5",
stats: {
    "name": "Animal-api sync for customer NL_123944",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "15768",
        "ok": "15768",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "15768",
        "ok": "15768",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "15768",
        "ok": "15768",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "15768",
        "ok": "15768",
        "ko": "-"
    },
    "percentiles2": {
        "total": "15768",
        "ok": "15768",
        "ko": "-"
    },
    "percentiles3": {
        "total": "15768",
        "ok": "15768",
        "ko": "-"
    },
    "percentiles4": {
        "total": "15768",
        "ok": "15768",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-033e7": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_113983",
path: "Animal-api sync for customer NL_113983",
pathFormatted: "req_animal-api-sync-033e7",
stats: {
    "name": "Animal-api sync for customer NL_113983",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "9553",
        "ok": "9553",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "9553",
        "ok": "9553",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "9553",
        "ok": "9553",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "9553",
        "ok": "9553",
        "ko": "-"
    },
    "percentiles2": {
        "total": "9553",
        "ok": "9553",
        "ko": "-"
    },
    "percentiles3": {
        "total": "9553",
        "ok": "9553",
        "ko": "-"
    },
    "percentiles4": {
        "total": "9553",
        "ok": "9553",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-cb74a": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_131236",
path: "Animal-api sync for customer NL_131236",
pathFormatted: "req_animal-api-sync-cb74a",
stats: {
    "name": "Animal-api sync for customer NL_131236",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "20261",
        "ok": "20261",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "20261",
        "ok": "20261",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "20261",
        "ok": "20261",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "20261",
        "ok": "20261",
        "ko": "-"
    },
    "percentiles2": {
        "total": "20261",
        "ok": "20261",
        "ko": "-"
    },
    "percentiles3": {
        "total": "20261",
        "ok": "20261",
        "ko": "-"
    },
    "percentiles4": {
        "total": "20261",
        "ok": "20261",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-4c33d": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_131701",
path: "Animal-api sync for customer NL_131701",
pathFormatted: "req_animal-api-sync-4c33d",
stats: {
    "name": "Animal-api sync for customer NL_131701",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "14873",
        "ok": "14873",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "14873",
        "ok": "14873",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "14873",
        "ok": "14873",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "14873",
        "ok": "14873",
        "ko": "-"
    },
    "percentiles2": {
        "total": "14873",
        "ok": "14873",
        "ko": "-"
    },
    "percentiles3": {
        "total": "14873",
        "ok": "14873",
        "ko": "-"
    },
    "percentiles4": {
        "total": "14873",
        "ok": "14873",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-98e1b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_129146",
path: "Animal-api sync for customer NL_129146",
pathFormatted: "req_animal-api-sync-98e1b",
stats: {
    "name": "Animal-api sync for customer NL_129146",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "maxResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "meanResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles2": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles3": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles4": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-e8633": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117217",
path: "Animal-api sync for customer NL_117217",
pathFormatted: "req_animal-api-sync-e8633",
stats: {
    "name": "Animal-api sync for customer NL_117217",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "11982",
        "ok": "11982",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "11982",
        "ok": "11982",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "11982",
        "ok": "11982",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "11982",
        "ok": "11982",
        "ko": "-"
    },
    "percentiles2": {
        "total": "11982",
        "ok": "11982",
        "ko": "-"
    },
    "percentiles3": {
        "total": "11982",
        "ok": "11982",
        "ko": "-"
    },
    "percentiles4": {
        "total": "11982",
        "ok": "11982",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-748ee": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_118193",
path: "Animal-api sync for customer NL_118193",
pathFormatted: "req_animal-api-sync-748ee",
stats: {
    "name": "Animal-api sync for customer NL_118193",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "maxResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "meanResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles2": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles3": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles4": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-ac942": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_162293",
path: "Animal-api sync for customer BE_162293",
pathFormatted: "req_animal-api-sync-ac942",
stats: {
    "name": "Animal-api sync for customer BE_162293",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "maxResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "meanResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles2": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles3": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles4": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-18df0": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_132381",
path: "Animal-api sync for customer NL_132381",
pathFormatted: "req_animal-api-sync-18df0",
stats: {
    "name": "Animal-api sync for customer NL_132381",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "maxResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "meanResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles2": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles3": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles4": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-7e345": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111388",
path: "Animal-api sync for customer NL_111388",
pathFormatted: "req_animal-api-sync-7e345",
stats: {
    "name": "Animal-api sync for customer NL_111388",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "maxResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "meanResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles2": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles3": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles4": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-d3a0e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_118547",
path: "Animal-api sync for customer NL_118547",
pathFormatted: "req_animal-api-sync-d3a0e",
stats: {
    "name": "Animal-api sync for customer NL_118547",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "38952",
        "ok": "38952",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "38952",
        "ok": "38952",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "38952",
        "ok": "38952",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "38952",
        "ok": "38952",
        "ko": "-"
    },
    "percentiles2": {
        "total": "38952",
        "ok": "38952",
        "ko": "-"
    },
    "percentiles3": {
        "total": "38952",
        "ok": "38952",
        "ko": "-"
    },
    "percentiles4": {
        "total": "38952",
        "ok": "38952",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-3631b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105143",
path: "Animal-api sync for customer NL_105143",
pathFormatted: "req_animal-api-sync-3631b",
stats: {
    "name": "Animal-api sync for customer NL_105143",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "maxResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "meanResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles2": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles3": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles4": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-f99c5": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_131859",
path: "Animal-api sync for customer NL_131859",
pathFormatted: "req_animal-api-sync-f99c5",
stats: {
    "name": "Animal-api sync for customer NL_131859",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "4801",
        "ok": "4801",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4801",
        "ok": "4801",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4801",
        "ok": "4801",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "4801",
        "ok": "4801",
        "ko": "-"
    },
    "percentiles2": {
        "total": "4801",
        "ok": "4801",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4801",
        "ok": "4801",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4801",
        "ok": "4801",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-ba235": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_109254",
path: "Animal-api sync for customer NL_109254",
pathFormatted: "req_animal-api-sync-ba235",
stats: {
    "name": "Animal-api sync for customer NL_109254",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21695",
        "ok": "21695",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "21695",
        "ok": "21695",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "21695",
        "ok": "21695",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "21695",
        "ok": "21695",
        "ko": "-"
    },
    "percentiles2": {
        "total": "21695",
        "ok": "21695",
        "ko": "-"
    },
    "percentiles3": {
        "total": "21695",
        "ok": "21695",
        "ko": "-"
    },
    "percentiles4": {
        "total": "21695",
        "ok": "21695",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e87b4": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_118457",
path: "Animal-api sync for customer NL_118457",
pathFormatted: "req_animal-api-sync-e87b4",
stats: {
    "name": "Animal-api sync for customer NL_118457",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "50388",
        "ok": "50388",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "50388",
        "ok": "50388",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "50388",
        "ok": "50388",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "50388",
        "ok": "50388",
        "ko": "-"
    },
    "percentiles2": {
        "total": "50388",
        "ok": "50388",
        "ko": "-"
    },
    "percentiles3": {
        "total": "50388",
        "ok": "50388",
        "ko": "-"
    },
    "percentiles4": {
        "total": "50388",
        "ok": "50388",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-84066": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_136422",
path: "Animal-api sync for customer NL_136422",
pathFormatted: "req_animal-api-sync-84066",
stats: {
    "name": "Animal-api sync for customer NL_136422",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "6982",
        "ok": "6982",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "6982",
        "ok": "6982",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "6982",
        "ok": "6982",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "6982",
        "ok": "6982",
        "ko": "-"
    },
    "percentiles2": {
        "total": "6982",
        "ok": "6982",
        "ko": "-"
    },
    "percentiles3": {
        "total": "6982",
        "ok": "6982",
        "ko": "-"
    },
    "percentiles4": {
        "total": "6982",
        "ok": "6982",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-f4249": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_138300",
path: "Animal-api sync for customer NL_138300",
pathFormatted: "req_animal-api-sync-f4249",
stats: {
    "name": "Animal-api sync for customer NL_138300",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "maxResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "meanResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles2": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles3": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles4": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-32bc3": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_103585",
path: "Animal-api sync for customer NL_103585",
pathFormatted: "req_animal-api-sync-32bc3",
stats: {
    "name": "Animal-api sync for customer NL_103585",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "26552",
        "ok": "26552",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "26552",
        "ok": "26552",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "26552",
        "ok": "26552",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "26552",
        "ok": "26552",
        "ko": "-"
    },
    "percentiles2": {
        "total": "26552",
        "ok": "26552",
        "ko": "-"
    },
    "percentiles3": {
        "total": "26552",
        "ok": "26552",
        "ko": "-"
    },
    "percentiles4": {
        "total": "26552",
        "ok": "26552",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-787cb": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_143564",
path: "Animal-api sync for customer NL_143564",
pathFormatted: "req_animal-api-sync-787cb",
stats: {
    "name": "Animal-api sync for customer NL_143564",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "8056",
        "ok": "8056",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "8056",
        "ok": "8056",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "8056",
        "ok": "8056",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "8056",
        "ok": "8056",
        "ko": "-"
    },
    "percentiles2": {
        "total": "8056",
        "ok": "8056",
        "ko": "-"
    },
    "percentiles3": {
        "total": "8056",
        "ok": "8056",
        "ko": "-"
    },
    "percentiles4": {
        "total": "8056",
        "ok": "8056",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-0f436": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_107376",
path: "Animal-api sync for customer NL_107376",
pathFormatted: "req_animal-api-sync-0f436",
stats: {
    "name": "Animal-api sync for customer NL_107376",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "27784",
        "ok": "27784",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "27784",
        "ok": "27784",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "27784",
        "ok": "27784",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "27784",
        "ok": "27784",
        "ko": "-"
    },
    "percentiles2": {
        "total": "27784",
        "ok": "27784",
        "ko": "-"
    },
    "percentiles3": {
        "total": "27784",
        "ok": "27784",
        "ko": "-"
    },
    "percentiles4": {
        "total": "27784",
        "ok": "27784",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-20cd3": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110661",
path: "Animal-api sync for customer NL_110661",
pathFormatted: "req_animal-api-sync-20cd3",
stats: {
    "name": "Animal-api sync for customer NL_110661",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "maxResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "meanResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles2": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles3": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles4": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-eb26b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_118799",
path: "Animal-api sync for customer NL_118799",
pathFormatted: "req_animal-api-sync-eb26b",
stats: {
    "name": "Animal-api sync for customer NL_118799",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "23704",
        "ok": "23704",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "23704",
        "ok": "23704",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "23704",
        "ok": "23704",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "23704",
        "ok": "23704",
        "ko": "-"
    },
    "percentiles2": {
        "total": "23704",
        "ok": "23704",
        "ko": "-"
    },
    "percentiles3": {
        "total": "23704",
        "ok": "23704",
        "ko": "-"
    },
    "percentiles4": {
        "total": "23704",
        "ok": "23704",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c1388": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_122597",
path: "Animal-api sync for customer NL_122597",
pathFormatted: "req_animal-api-sync-c1388",
stats: {
    "name": "Animal-api sync for customer NL_122597",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60006",
        "ok": "-",
        "ko": "60006"
    },
    "maxResponseTime": {
        "total": "60006",
        "ok": "-",
        "ko": "60006"
    },
    "meanResponseTime": {
        "total": "60006",
        "ok": "-",
        "ko": "60006"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60006",
        "ok": "-",
        "ko": "60006"
    },
    "percentiles2": {
        "total": "60006",
        "ok": "-",
        "ko": "60006"
    },
    "percentiles3": {
        "total": "60006",
        "ok": "-",
        "ko": "60006"
    },
    "percentiles4": {
        "total": "60006",
        "ok": "-",
        "ko": "60006"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-28731": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_136181",
path: "Animal-api sync for customer NL_136181",
pathFormatted: "req_animal-api-sync-28731",
stats: {
    "name": "Animal-api sync for customer NL_136181",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "maxResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "meanResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles2": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles3": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles4": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-da9fa": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_212118",
path: "Animal-api sync for customer NL_212118",
pathFormatted: "req_animal-api-sync-da9fa",
stats: {
    "name": "Animal-api sync for customer NL_212118",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "15234",
        "ok": "15234",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "15234",
        "ok": "15234",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "15234",
        "ok": "15234",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "15234",
        "ok": "15234",
        "ko": "-"
    },
    "percentiles2": {
        "total": "15234",
        "ok": "15234",
        "ko": "-"
    },
    "percentiles3": {
        "total": "15234",
        "ok": "15234",
        "ko": "-"
    },
    "percentiles4": {
        "total": "15234",
        "ok": "15234",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-2b764": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111875",
path: "Animal-api sync for customer NL_111875",
pathFormatted: "req_animal-api-sync-2b764",
stats: {
    "name": "Animal-api sync for customer NL_111875",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "maxResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "meanResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles2": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles3": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles4": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-56095": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_125359",
path: "Animal-api sync for customer NL_125359",
pathFormatted: "req_animal-api-sync-56095",
stats: {
    "name": "Animal-api sync for customer NL_125359",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "26772",
        "ok": "26772",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "26772",
        "ok": "26772",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "26772",
        "ok": "26772",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "26772",
        "ok": "26772",
        "ko": "-"
    },
    "percentiles2": {
        "total": "26772",
        "ok": "26772",
        "ko": "-"
    },
    "percentiles3": {
        "total": "26772",
        "ok": "26772",
        "ko": "-"
    },
    "percentiles4": {
        "total": "26772",
        "ok": "26772",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-7c52f": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_112020",
path: "Animal-api sync for customer NL_112020",
pathFormatted: "req_animal-api-sync-7c52f",
stats: {
    "name": "Animal-api sync for customer NL_112020",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "maxResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "meanResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles2": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles3": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles4": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-1586b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_113408",
path: "Animal-api sync for customer NL_113408",
pathFormatted: "req_animal-api-sync-1586b",
stats: {
    "name": "Animal-api sync for customer NL_113408",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "maxResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "meanResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles2": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles3": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles4": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-bca62": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_112986",
path: "Animal-api sync for customer NL_112986",
pathFormatted: "req_animal-api-sync-bca62",
stats: {
    "name": "Animal-api sync for customer NL_112986",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "maxResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "meanResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles4": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-d8a50": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_109324",
path: "Animal-api sync for customer NL_109324",
pathFormatted: "req_animal-api-sync-d8a50",
stats: {
    "name": "Animal-api sync for customer NL_109324",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "maxResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "meanResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles2": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles3": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles4": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-d4b4d": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_123556",
path: "Animal-api sync for customer NL_123556",
pathFormatted: "req_animal-api-sync-d4b4d",
stats: {
    "name": "Animal-api sync for customer NL_123556",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "maxResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "meanResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles2": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles3": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles4": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-bc7d0": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_151016",
path: "Animal-api sync for customer BE_151016",
pathFormatted: "req_animal-api-sync-bc7d0",
stats: {
    "name": "Animal-api sync for customer BE_151016",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "28006",
        "ok": "28006",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "28006",
        "ok": "28006",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "28006",
        "ok": "28006",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "28006",
        "ok": "28006",
        "ko": "-"
    },
    "percentiles2": {
        "total": "28006",
        "ok": "28006",
        "ko": "-"
    },
    "percentiles3": {
        "total": "28006",
        "ok": "28006",
        "ko": "-"
    },
    "percentiles4": {
        "total": "28006",
        "ok": "28006",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a0b51": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_118379",
path: "Animal-api sync for customer NL_118379",
pathFormatted: "req_animal-api-sync-a0b51",
stats: {
    "name": "Animal-api sync for customer NL_118379",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "maxResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "meanResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles2": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles3": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles4": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-b6f25": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_119444",
path: "Animal-api sync for customer NL_119444",
pathFormatted: "req_animal-api-sync-b6f25",
stats: {
    "name": "Animal-api sync for customer NL_119444",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "44557",
        "ok": "44557",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "44557",
        "ok": "44557",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "44557",
        "ok": "44557",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "44557",
        "ok": "44557",
        "ko": "-"
    },
    "percentiles2": {
        "total": "44557",
        "ok": "44557",
        "ko": "-"
    },
    "percentiles3": {
        "total": "44557",
        "ok": "44557",
        "ko": "-"
    },
    "percentiles4": {
        "total": "44557",
        "ok": "44557",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b9666": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_207943",
path: "Animal-api sync for customer BE_207943",
pathFormatted: "req_animal-api-sync-b9666",
stats: {
    "name": "Animal-api sync for customer BE_207943",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "maxResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "meanResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles2": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles3": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles4": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-662d1": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_211608",
path: "Animal-api sync for customer BE_211608",
pathFormatted: "req_animal-api-sync-662d1",
stats: {
    "name": "Animal-api sync for customer BE_211608",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "37165",
        "ok": "37165",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "37165",
        "ok": "37165",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "37165",
        "ok": "37165",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "37165",
        "ok": "37165",
        "ko": "-"
    },
    "percentiles2": {
        "total": "37165",
        "ok": "37165",
        "ko": "-"
    },
    "percentiles3": {
        "total": "37165",
        "ok": "37165",
        "ko": "-"
    },
    "percentiles4": {
        "total": "37165",
        "ok": "37165",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-58eb2": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_125881",
path: "Animal-api sync for customer NL_125881",
pathFormatted: "req_animal-api-sync-58eb2",
stats: {
    "name": "Animal-api sync for customer NL_125881",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "15279",
        "ok": "15279",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "15279",
        "ok": "15279",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "15279",
        "ok": "15279",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "15279",
        "ok": "15279",
        "ko": "-"
    },
    "percentiles2": {
        "total": "15279",
        "ok": "15279",
        "ko": "-"
    },
    "percentiles3": {
        "total": "15279",
        "ok": "15279",
        "ko": "-"
    },
    "percentiles4": {
        "total": "15279",
        "ok": "15279",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-cd728": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117817",
path: "Animal-api sync for customer NL_117817",
pathFormatted: "req_animal-api-sync-cd728",
stats: {
    "name": "Animal-api sync for customer NL_117817",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "maxResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "meanResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles2": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles3": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles4": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-d0381": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_126231",
path: "Animal-api sync for customer NL_126231",
pathFormatted: "req_animal-api-sync-d0381",
stats: {
    "name": "Animal-api sync for customer NL_126231",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "maxResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "meanResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles2": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles3": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles4": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-1164c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104369",
path: "Animal-api sync for customer NL_104369",
pathFormatted: "req_animal-api-sync-1164c",
stats: {
    "name": "Animal-api sync for customer NL_104369",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "maxResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "meanResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles2": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles3": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles4": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-c2774": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_130077",
path: "Animal-api sync for customer NL_130077",
pathFormatted: "req_animal-api-sync-c2774",
stats: {
    "name": "Animal-api sync for customer NL_130077",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "maxResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "meanResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles4": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-da2a1": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105775",
path: "Animal-api sync for customer NL_105775",
pathFormatted: "req_animal-api-sync-da2a1",
stats: {
    "name": "Animal-api sync for customer NL_105775",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "maxResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "meanResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles4": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-12657": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_212029",
path: "Animal-api sync for customer BE_212029",
pathFormatted: "req_animal-api-sync-12657",
stats: {
    "name": "Animal-api sync for customer BE_212029",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "maxResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "meanResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles2": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles3": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles4": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-a231f": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_119073",
path: "Animal-api sync for customer NL_119073",
pathFormatted: "req_animal-api-sync-a231f",
stats: {
    "name": "Animal-api sync for customer NL_119073",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "maxResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "meanResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles2": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles3": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles4": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-a5dcb": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_113563",
path: "Animal-api sync for customer NL_113563",
pathFormatted: "req_animal-api-sync-a5dcb",
stats: {
    "name": "Animal-api sync for customer NL_113563",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21967",
        "ok": "21967",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "21967",
        "ok": "21967",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "21967",
        "ok": "21967",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "21967",
        "ok": "21967",
        "ko": "-"
    },
    "percentiles2": {
        "total": "21967",
        "ok": "21967",
        "ko": "-"
    },
    "percentiles3": {
        "total": "21967",
        "ok": "21967",
        "ko": "-"
    },
    "percentiles4": {
        "total": "21967",
        "ok": "21967",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-0d921": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_124334",
path: "Animal-api sync for customer NL_124334",
pathFormatted: "req_animal-api-sync-0d921",
stats: {
    "name": "Animal-api sync for customer NL_124334",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "maxResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "meanResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles2": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles3": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles4": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-a5308": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_129853",
path: "Animal-api sync for customer NL_129853",
pathFormatted: "req_animal-api-sync-a5308",
stats: {
    "name": "Animal-api sync for customer NL_129853",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "35084",
        "ok": "35084",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "35084",
        "ok": "35084",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "35084",
        "ok": "35084",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "35084",
        "ok": "35084",
        "ko": "-"
    },
    "percentiles2": {
        "total": "35084",
        "ok": "35084",
        "ko": "-"
    },
    "percentiles3": {
        "total": "35084",
        "ok": "35084",
        "ko": "-"
    },
    "percentiles4": {
        "total": "35084",
        "ok": "35084",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a8587": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_112236",
path: "Animal-api sync for customer NL_112236",
pathFormatted: "req_animal-api-sync-a8587",
stats: {
    "name": "Animal-api sync for customer NL_112236",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "maxResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "meanResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles2": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles3": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles4": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-c1a9b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105397",
path: "Animal-api sync for customer NL_105397",
pathFormatted: "req_animal-api-sync-c1a9b",
stats: {
    "name": "Animal-api sync for customer NL_105397",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "maxResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "meanResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles2": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles3": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles4": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-f47d9": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_162268",
path: "Animal-api sync for customer NL_162268",
pathFormatted: "req_animal-api-sync-f47d9",
stats: {
    "name": "Animal-api sync for customer NL_162268",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "maxResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "meanResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles2": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles3": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles4": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-7141e": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_195066",
path: "Animal-api sync for customer BE_195066",
pathFormatted: "req_animal-api-sync-7141e",
stats: {
    "name": "Animal-api sync for customer BE_195066",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "maxResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "meanResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles2": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles3": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles4": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-9fb14": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_115364",
path: "Animal-api sync for customer NL_115364",
pathFormatted: "req_animal-api-sync-9fb14",
stats: {
    "name": "Animal-api sync for customer NL_115364",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "maxResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "meanResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles4": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-29ae3": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_107774",
path: "Animal-api sync for customer NL_107774",
pathFormatted: "req_animal-api-sync-29ae3",
stats: {
    "name": "Animal-api sync for customer NL_107774",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "maxResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "meanResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles2": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles3": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles4": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-e24f9": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_125084",
path: "Animal-api sync for customer NL_125084",
pathFormatted: "req_animal-api-sync-e24f9",
stats: {
    "name": "Animal-api sync for customer NL_125084",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "37999",
        "ok": "37999",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "37999",
        "ok": "37999",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "37999",
        "ok": "37999",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "37999",
        "ok": "37999",
        "ko": "-"
    },
    "percentiles2": {
        "total": "37999",
        "ok": "37999",
        "ko": "-"
    },
    "percentiles3": {
        "total": "37999",
        "ok": "37999",
        "ko": "-"
    },
    "percentiles4": {
        "total": "37999",
        "ok": "37999",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-6bcf1": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_130692",
path: "Animal-api sync for customer NL_130692",
pathFormatted: "req_animal-api-sync-6bcf1",
stats: {
    "name": "Animal-api sync for customer NL_130692",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "52835",
        "ok": "52835",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "52835",
        "ok": "52835",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "52835",
        "ok": "52835",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "52835",
        "ok": "52835",
        "ko": "-"
    },
    "percentiles2": {
        "total": "52835",
        "ok": "52835",
        "ko": "-"
    },
    "percentiles3": {
        "total": "52835",
        "ok": "52835",
        "ko": "-"
    },
    "percentiles4": {
        "total": "52835",
        "ok": "52835",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-42cac": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117138",
path: "Animal-api sync for customer NL_117138",
pathFormatted: "req_animal-api-sync-42cac",
stats: {
    "name": "Animal-api sync for customer NL_117138",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "56484",
        "ok": "56484",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "56484",
        "ok": "56484",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "56484",
        "ok": "56484",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "56484",
        "ok": "56484",
        "ko": "-"
    },
    "percentiles2": {
        "total": "56484",
        "ok": "56484",
        "ko": "-"
    },
    "percentiles3": {
        "total": "56484",
        "ok": "56484",
        "ko": "-"
    },
    "percentiles4": {
        "total": "56484",
        "ok": "56484",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-7ddaa": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_127532",
path: "Animal-api sync for customer NL_127532",
pathFormatted: "req_animal-api-sync-7ddaa",
stats: {
    "name": "Animal-api sync for customer NL_127532",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "maxResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "meanResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles4": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-35067": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_145405",
path: "Animal-api sync for customer BE_145405",
pathFormatted: "req_animal-api-sync-35067",
stats: {
    "name": "Animal-api sync for customer BE_145405",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "maxResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "meanResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles2": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles3": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles4": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-2e40c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_131445",
path: "Animal-api sync for customer NL_131445",
pathFormatted: "req_animal-api-sync-2e40c",
stats: {
    "name": "Animal-api sync for customer NL_131445",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "maxResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "meanResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles2": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles3": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles4": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-3eb24": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_103773",
path: "Animal-api sync for customer NL_103773",
pathFormatted: "req_animal-api-sync-3eb24",
stats: {
    "name": "Animal-api sync for customer NL_103773",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "maxResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "meanResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles2": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles3": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles4": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-db524": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104670",
path: "Animal-api sync for customer NL_104670",
pathFormatted: "req_animal-api-sync-db524",
stats: {
    "name": "Animal-api sync for customer NL_104670",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "maxResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "meanResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles4": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-8824d": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_129197",
path: "Animal-api sync for customer NL_129197",
pathFormatted: "req_animal-api-sync-8824d",
stats: {
    "name": "Animal-api sync for customer NL_129197",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "maxResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "meanResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles2": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles3": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles4": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-d2007": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_214023",
path: "Animal-api sync for customer BE_214023",
pathFormatted: "req_animal-api-sync-d2007",
stats: {
    "name": "Animal-api sync for customer BE_214023",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "maxResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "meanResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles2": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles3": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles4": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-e79fc": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_114849",
path: "Animal-api sync for customer NL_114849",
pathFormatted: "req_animal-api-sync-e79fc",
stats: {
    "name": "Animal-api sync for customer NL_114849",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "maxResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "meanResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles2": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles3": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles4": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-6b259": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_118526",
path: "Animal-api sync for customer NL_118526",
pathFormatted: "req_animal-api-sync-6b259",
stats: {
    "name": "Animal-api sync for customer NL_118526",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "48047",
        "ok": "48047",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "48047",
        "ok": "48047",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "48047",
        "ok": "48047",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "48047",
        "ok": "48047",
        "ko": "-"
    },
    "percentiles2": {
        "total": "48047",
        "ok": "48047",
        "ko": "-"
    },
    "percentiles3": {
        "total": "48047",
        "ok": "48047",
        "ko": "-"
    },
    "percentiles4": {
        "total": "48047",
        "ok": "48047",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-1f585": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_123243",
path: "Animal-api sync for customer NL_123243",
pathFormatted: "req_animal-api-sync-1f585",
stats: {
    "name": "Animal-api sync for customer NL_123243",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "56004",
        "ok": "56004",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "56004",
        "ok": "56004",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "56004",
        "ok": "56004",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "56004",
        "ok": "56004",
        "ko": "-"
    },
    "percentiles2": {
        "total": "56004",
        "ok": "56004",
        "ko": "-"
    },
    "percentiles3": {
        "total": "56004",
        "ok": "56004",
        "ko": "-"
    },
    "percentiles4": {
        "total": "56004",
        "ok": "56004",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-360a2": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_120728",
path: "Animal-api sync for customer NL_120728",
pathFormatted: "req_animal-api-sync-360a2",
stats: {
    "name": "Animal-api sync for customer NL_120728",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "maxResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "meanResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles4": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-964ea": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_131647",
path: "Animal-api sync for customer NL_131647",
pathFormatted: "req_animal-api-sync-964ea",
stats: {
    "name": "Animal-api sync for customer NL_131647",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "50486",
        "ok": "50486",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "50486",
        "ok": "50486",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "50486",
        "ok": "50486",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "50486",
        "ok": "50486",
        "ko": "-"
    },
    "percentiles2": {
        "total": "50486",
        "ok": "50486",
        "ko": "-"
    },
    "percentiles3": {
        "total": "50486",
        "ok": "50486",
        "ko": "-"
    },
    "percentiles4": {
        "total": "50486",
        "ok": "50486",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-90a7a": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_158870",
path: "Animal-api sync for customer NL_158870",
pathFormatted: "req_animal-api-sync-90a7a",
stats: {
    "name": "Animal-api sync for customer NL_158870",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "maxResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "meanResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles2": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles3": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles4": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-ee74d": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_149842",
path: "Animal-api sync for customer BE_149842",
pathFormatted: "req_animal-api-sync-ee74d",
stats: {
    "name": "Animal-api sync for customer BE_149842",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "maxResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "meanResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles2": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles3": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles4": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-edbd5": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_156716",
path: "Animal-api sync for customer NL_156716",
pathFormatted: "req_animal-api-sync-edbd5",
stats: {
    "name": "Animal-api sync for customer NL_156716",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "maxResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "meanResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles4": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-b6490": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_128966",
path: "Animal-api sync for customer NL_128966",
pathFormatted: "req_animal-api-sync-b6490",
stats: {
    "name": "Animal-api sync for customer NL_128966",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "maxResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "meanResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles2": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles3": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles4": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-5eb0d": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_129758",
path: "Animal-api sync for customer NL_129758",
pathFormatted: "req_animal-api-sync-5eb0d",
stats: {
    "name": "Animal-api sync for customer NL_129758",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "maxResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "meanResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles2": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles3": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles4": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-7665a": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_107608",
path: "Animal-api sync for customer NL_107608",
pathFormatted: "req_animal-api-sync-7665a",
stats: {
    "name": "Animal-api sync for customer NL_107608",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "maxResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "meanResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles2": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles3": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles4": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-352f1": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_120975",
path: "Animal-api sync for customer NL_120975",
pathFormatted: "req_animal-api-sync-352f1",
stats: {
    "name": "Animal-api sync for customer NL_120975",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "maxResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "meanResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles2": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles3": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles4": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-a1b1c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105501",
path: "Animal-api sync for customer NL_105501",
pathFormatted: "req_animal-api-sync-a1b1c",
stats: {
    "name": "Animal-api sync for customer NL_105501",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "maxResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "meanResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles2": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles3": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles4": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-558ec": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_115428",
path: "Animal-api sync for customer NL_115428",
pathFormatted: "req_animal-api-sync-558ec",
stats: {
    "name": "Animal-api sync for customer NL_115428",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "12652",
        "ok": "12652",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "12652",
        "ok": "12652",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "12652",
        "ok": "12652",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "12652",
        "ok": "12652",
        "ko": "-"
    },
    "percentiles2": {
        "total": "12652",
        "ok": "12652",
        "ko": "-"
    },
    "percentiles3": {
        "total": "12652",
        "ok": "12652",
        "ko": "-"
    },
    "percentiles4": {
        "total": "12652",
        "ok": "12652",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e201e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110096",
path: "Animal-api sync for customer NL_110096",
pathFormatted: "req_animal-api-sync-e201e",
stats: {
    "name": "Animal-api sync for customer NL_110096",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "maxResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "meanResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles2": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles3": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles4": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-66a67": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_115297",
path: "Animal-api sync for customer NL_115297",
pathFormatted: "req_animal-api-sync-66a67",
stats: {
    "name": "Animal-api sync for customer NL_115297",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "maxResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "meanResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles2": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles3": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles4": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-e614b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_114481",
path: "Animal-api sync for customer NL_114481",
pathFormatted: "req_animal-api-sync-e614b",
stats: {
    "name": "Animal-api sync for customer NL_114481",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "maxResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "meanResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles2": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles3": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles4": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-a326b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_109292",
path: "Animal-api sync for customer NL_109292",
pathFormatted: "req_animal-api-sync-a326b",
stats: {
    "name": "Animal-api sync for customer NL_109292",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "maxResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "meanResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles2": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles3": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles4": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-ec201": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105082",
path: "Animal-api sync for customer NL_105082",
pathFormatted: "req_animal-api-sync-ec201",
stats: {
    "name": "Animal-api sync for customer NL_105082",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "maxResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "meanResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles2": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles3": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles4": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-c9dab": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_122464",
path: "Animal-api sync for customer NL_122464",
pathFormatted: "req_animal-api-sync-c9dab",
stats: {
    "name": "Animal-api sync for customer NL_122464",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "maxResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "meanResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles2": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles3": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles4": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-2092d": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_150518",
path: "Animal-api sync for customer BE_150518",
pathFormatted: "req_animal-api-sync-2092d",
stats: {
    "name": "Animal-api sync for customer BE_150518",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "maxResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "meanResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles2": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles3": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles4": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-cdf93": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_118194",
path: "Animal-api sync for customer NL_118194",
pathFormatted: "req_animal-api-sync-cdf93",
stats: {
    "name": "Animal-api sync for customer NL_118194",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "maxResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "meanResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles2": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles3": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles4": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-ac6d4": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_121952",
path: "Animal-api sync for customer NL_121952",
pathFormatted: "req_animal-api-sync-ac6d4",
stats: {
    "name": "Animal-api sync for customer NL_121952",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "maxResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "meanResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles2": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles3": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles4": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-0ec02": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_114079",
path: "Animal-api sync for customer NL_114079",
pathFormatted: "req_animal-api-sync-0ec02",
stats: {
    "name": "Animal-api sync for customer NL_114079",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "23837",
        "ok": "23837",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "23837",
        "ok": "23837",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "23837",
        "ok": "23837",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "23837",
        "ok": "23837",
        "ko": "-"
    },
    "percentiles2": {
        "total": "23837",
        "ok": "23837",
        "ko": "-"
    },
    "percentiles3": {
        "total": "23837",
        "ok": "23837",
        "ko": "-"
    },
    "percentiles4": {
        "total": "23837",
        "ok": "23837",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-2901a": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_136544",
path: "Animal-api sync for customer NL_136544",
pathFormatted: "req_animal-api-sync-2901a",
stats: {
    "name": "Animal-api sync for customer NL_136544",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "maxResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "meanResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles2": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles3": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles4": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-1a27a": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_120522",
path: "Animal-api sync for customer NL_120522",
pathFormatted: "req_animal-api-sync-1a27a",
stats: {
    "name": "Animal-api sync for customer NL_120522",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "maxResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "meanResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles2": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles3": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles4": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-317c3": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_132801",
path: "Animal-api sync for customer NL_132801",
pathFormatted: "req_animal-api-sync-317c3",
stats: {
    "name": "Animal-api sync for customer NL_132801",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "50635",
        "ok": "50635",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "50635",
        "ok": "50635",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "50635",
        "ok": "50635",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "50635",
        "ok": "50635",
        "ko": "-"
    },
    "percentiles2": {
        "total": "50635",
        "ok": "50635",
        "ko": "-"
    },
    "percentiles3": {
        "total": "50635",
        "ok": "50635",
        "ko": "-"
    },
    "percentiles4": {
        "total": "50635",
        "ok": "50635",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-83baa": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_114916",
path: "Animal-api sync for customer NL_114916",
pathFormatted: "req_animal-api-sync-83baa",
stats: {
    "name": "Animal-api sync for customer NL_114916",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "maxResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "meanResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles2": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles3": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles4": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-3eaed": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_108223",
path: "Animal-api sync for customer NL_108223",
pathFormatted: "req_animal-api-sync-3eaed",
stats: {
    "name": "Animal-api sync for customer NL_108223",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "maxResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "meanResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles2": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles3": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles4": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-6ad60": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_124372",
path: "Animal-api sync for customer NL_124372",
pathFormatted: "req_animal-api-sync-6ad60",
stats: {
    "name": "Animal-api sync for customer NL_124372",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "59480",
        "ok": "59480",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "59480",
        "ok": "59480",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "59480",
        "ok": "59480",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "59480",
        "ok": "59480",
        "ko": "-"
    },
    "percentiles2": {
        "total": "59480",
        "ok": "59480",
        "ko": "-"
    },
    "percentiles3": {
        "total": "59480",
        "ok": "59480",
        "ko": "-"
    },
    "percentiles4": {
        "total": "59480",
        "ok": "59480",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-967fa": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_113311",
path: "Animal-api sync for customer NL_113311",
pathFormatted: "req_animal-api-sync-967fa",
stats: {
    "name": "Animal-api sync for customer NL_113311",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "maxResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "meanResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles2": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles3": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles4": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-3736b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_114775",
path: "Animal-api sync for customer NL_114775",
pathFormatted: "req_animal-api-sync-3736b",
stats: {
    "name": "Animal-api sync for customer NL_114775",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "maxResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "meanResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles4": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-2e3f2": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_124578",
path: "Animal-api sync for customer NL_124578",
pathFormatted: "req_animal-api-sync-2e3f2",
stats: {
    "name": "Animal-api sync for customer NL_124578",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "maxResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "meanResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles2": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles3": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles4": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-f6bd1": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_118770",
path: "Animal-api sync for customer NL_118770",
pathFormatted: "req_animal-api-sync-f6bd1",
stats: {
    "name": "Animal-api sync for customer NL_118770",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "maxResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "meanResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles2": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles3": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles4": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-cfdfb": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111312",
path: "Animal-api sync for customer NL_111312",
pathFormatted: "req_animal-api-sync-cfdfb",
stats: {
    "name": "Animal-api sync for customer NL_111312",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "maxResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "meanResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles2": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles3": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles4": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-c9778": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_119273",
path: "Animal-api sync for customer NL_119273",
pathFormatted: "req_animal-api-sync-c9778",
stats: {
    "name": "Animal-api sync for customer NL_119273",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "maxResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "meanResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles2": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles3": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles4": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-39d31": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_118474",
path: "Animal-api sync for customer NL_118474",
pathFormatted: "req_animal-api-sync-39d31",
stats: {
    "name": "Animal-api sync for customer NL_118474",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "maxResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "meanResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles2": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles3": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles4": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-53fa3": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_108741",
path: "Animal-api sync for customer NL_108741",
pathFormatted: "req_animal-api-sync-53fa3",
stats: {
    "name": "Animal-api sync for customer NL_108741",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "maxResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "meanResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles2": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles3": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles4": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-99401": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111178",
path: "Animal-api sync for customer NL_111178",
pathFormatted: "req_animal-api-sync-99401",
stats: {
    "name": "Animal-api sync for customer NL_111178",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "maxResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "meanResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles2": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles3": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles4": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-09c2e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_135864",
path: "Animal-api sync for customer NL_135864",
pathFormatted: "req_animal-api-sync-09c2e",
stats: {
    "name": "Animal-api sync for customer NL_135864",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "maxResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "meanResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles4": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-236d8": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_124049",
path: "Animal-api sync for customer NL_124049",
pathFormatted: "req_animal-api-sync-236d8",
stats: {
    "name": "Animal-api sync for customer NL_124049",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "maxResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "meanResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles2": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles3": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles4": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-882c2": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_128097",
path: "Animal-api sync for customer NL_128097",
pathFormatted: "req_animal-api-sync-882c2",
stats: {
    "name": "Animal-api sync for customer NL_128097",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "maxResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "meanResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles2": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles3": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles4": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-e7614": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_159766",
path: "Animal-api sync for customer BE_159766",
pathFormatted: "req_animal-api-sync-e7614",
stats: {
    "name": "Animal-api sync for customer BE_159766",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "maxResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "meanResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles2": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles3": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles4": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-52a69": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_212921",
path: "Animal-api sync for customer BE_212921",
pathFormatted: "req_animal-api-sync-52a69",
stats: {
    "name": "Animal-api sync for customer BE_212921",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "maxResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "meanResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles2": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles3": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles4": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-53e2f": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_127894",
path: "Animal-api sync for customer NL_127894",
pathFormatted: "req_animal-api-sync-53e2f",
stats: {
    "name": "Animal-api sync for customer NL_127894",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "maxResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "meanResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles2": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles3": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles4": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-b4a99": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104247",
path: "Animal-api sync for customer NL_104247",
pathFormatted: "req_animal-api-sync-b4a99",
stats: {
    "name": "Animal-api sync for customer NL_104247",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "maxResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "meanResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles2": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles3": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles4": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-c4c5c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_122906",
path: "Animal-api sync for customer NL_122906",
pathFormatted: "req_animal-api-sync-c4c5c",
stats: {
    "name": "Animal-api sync for customer NL_122906",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "maxResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "meanResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles2": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles3": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles4": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-e42dd": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_108296",
path: "Animal-api sync for customer NL_108296",
pathFormatted: "req_animal-api-sync-e42dd",
stats: {
    "name": "Animal-api sync for customer NL_108296",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "maxResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "meanResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles2": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles3": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles4": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-b8f6b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_120259",
path: "Animal-api sync for customer NL_120259",
pathFormatted: "req_animal-api-sync-b8f6b",
stats: {
    "name": "Animal-api sync for customer NL_120259",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "maxResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "meanResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles2": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles3": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles4": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-10dc5": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_113215",
path: "Animal-api sync for customer NL_113215",
pathFormatted: "req_animal-api-sync-10dc5",
stats: {
    "name": "Animal-api sync for customer NL_113215",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "maxResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "meanResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles2": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles3": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles4": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-ee676": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_119868",
path: "Animal-api sync for customer NL_119868",
pathFormatted: "req_animal-api-sync-ee676",
stats: {
    "name": "Animal-api sync for customer NL_119868",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "maxResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "meanResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles2": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles3": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles4": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-12c8b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_107507",
path: "Animal-api sync for customer NL_107507",
pathFormatted: "req_animal-api-sync-12c8b",
stats: {
    "name": "Animal-api sync for customer NL_107507",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "43974",
        "ok": "43974",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "43974",
        "ok": "43974",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "43974",
        "ok": "43974",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "43974",
        "ok": "43974",
        "ko": "-"
    },
    "percentiles2": {
        "total": "43974",
        "ok": "43974",
        "ko": "-"
    },
    "percentiles3": {
        "total": "43974",
        "ok": "43974",
        "ko": "-"
    },
    "percentiles4": {
        "total": "43974",
        "ok": "43974",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-70c3b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_108064",
path: "Animal-api sync for customer NL_108064",
pathFormatted: "req_animal-api-sync-70c3b",
stats: {
    "name": "Animal-api sync for customer NL_108064",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "maxResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "meanResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles2": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles3": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles4": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-d5bee": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_114423",
path: "Animal-api sync for customer NL_114423",
pathFormatted: "req_animal-api-sync-d5bee",
stats: {
    "name": "Animal-api sync for customer NL_114423",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "maxResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "meanResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles2": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles3": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles4": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-f94cb": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_107241",
path: "Animal-api sync for customer NL_107241",
pathFormatted: "req_animal-api-sync-f94cb",
stats: {
    "name": "Animal-api sync for customer NL_107241",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "maxResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "meanResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles2": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles3": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles4": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-4b1f5": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_129590",
path: "Animal-api sync for customer NL_129590",
pathFormatted: "req_animal-api-sync-4b1f5",
stats: {
    "name": "Animal-api sync for customer NL_129590",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "36488",
        "ok": "36488",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "36488",
        "ok": "36488",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "36488",
        "ok": "36488",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "36488",
        "ok": "36488",
        "ko": "-"
    },
    "percentiles2": {
        "total": "36488",
        "ok": "36488",
        "ko": "-"
    },
    "percentiles3": {
        "total": "36488",
        "ok": "36488",
        "ko": "-"
    },
    "percentiles4": {
        "total": "36488",
        "ok": "36488",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-25bc2": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111740",
path: "Animal-api sync for customer NL_111740",
pathFormatted: "req_animal-api-sync-25bc2",
stats: {
    "name": "Animal-api sync for customer NL_111740",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "maxResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "meanResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles2": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles3": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles4": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-2efa4": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117125",
path: "Animal-api sync for customer NL_117125",
pathFormatted: "req_animal-api-sync-2efa4",
stats: {
    "name": "Animal-api sync for customer NL_117125",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "maxResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "meanResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles2": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles3": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles4": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-c4fc3": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_216527",
path: "Animal-api sync for customer NL_216527",
pathFormatted: "req_animal-api-sync-c4fc3",
stats: {
    "name": "Animal-api sync for customer NL_216527",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "maxResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "meanResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles4": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-41435": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_119693",
path: "Animal-api sync for customer NL_119693",
pathFormatted: "req_animal-api-sync-41435",
stats: {
    "name": "Animal-api sync for customer NL_119693",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "35144",
        "ok": "35144",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "35144",
        "ok": "35144",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "35144",
        "ok": "35144",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "35144",
        "ok": "35144",
        "ko": "-"
    },
    "percentiles2": {
        "total": "35144",
        "ok": "35144",
        "ko": "-"
    },
    "percentiles3": {
        "total": "35144",
        "ok": "35144",
        "ko": "-"
    },
    "percentiles4": {
        "total": "35144",
        "ok": "35144",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e3e3d": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_126029",
path: "Animal-api sync for customer NL_126029",
pathFormatted: "req_animal-api-sync-e3e3d",
stats: {
    "name": "Animal-api sync for customer NL_126029",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "maxResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "meanResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles2": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles3": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles4": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-d8bef": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_114961",
path: "Animal-api sync for customer NL_114961",
pathFormatted: "req_animal-api-sync-d8bef",
stats: {
    "name": "Animal-api sync for customer NL_114961",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "32673",
        "ok": "32673",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "32673",
        "ok": "32673",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "32673",
        "ok": "32673",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "32673",
        "ok": "32673",
        "ko": "-"
    },
    "percentiles2": {
        "total": "32673",
        "ok": "32673",
        "ko": "-"
    },
    "percentiles3": {
        "total": "32673",
        "ok": "32673",
        "ko": "-"
    },
    "percentiles4": {
        "total": "32673",
        "ok": "32673",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-40763": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_127944",
path: "Animal-api sync for customer NL_127944",
pathFormatted: "req_animal-api-sync-40763",
stats: {
    "name": "Animal-api sync for customer NL_127944",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "maxResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "meanResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles4": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-9f4a5": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_108715",
path: "Animal-api sync for customer NL_108715",
pathFormatted: "req_animal-api-sync-9f4a5",
stats: {
    "name": "Animal-api sync for customer NL_108715",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "23815",
        "ok": "23815",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "23815",
        "ok": "23815",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "23815",
        "ok": "23815",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "23815",
        "ok": "23815",
        "ko": "-"
    },
    "percentiles2": {
        "total": "23815",
        "ok": "23815",
        "ko": "-"
    },
    "percentiles3": {
        "total": "23815",
        "ok": "23815",
        "ko": "-"
    },
    "percentiles4": {
        "total": "23815",
        "ok": "23815",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-0355c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_133793",
path: "Animal-api sync for customer NL_133793",
pathFormatted: "req_animal-api-sync-0355c",
stats: {
    "name": "Animal-api sync for customer NL_133793",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "maxResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "meanResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles2": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles3": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles4": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-6bc35": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_127470",
path: "Animal-api sync for customer NL_127470",
pathFormatted: "req_animal-api-sync-6bc35",
stats: {
    "name": "Animal-api sync for customer NL_127470",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60006",
        "ok": "-",
        "ko": "60006"
    },
    "maxResponseTime": {
        "total": "60006",
        "ok": "-",
        "ko": "60006"
    },
    "meanResponseTime": {
        "total": "60006",
        "ok": "-",
        "ko": "60006"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60006",
        "ok": "-",
        "ko": "60006"
    },
    "percentiles2": {
        "total": "60006",
        "ok": "-",
        "ko": "60006"
    },
    "percentiles3": {
        "total": "60006",
        "ok": "-",
        "ko": "60006"
    },
    "percentiles4": {
        "total": "60006",
        "ok": "-",
        "ko": "60006"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-7eb9d": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104934",
path: "Animal-api sync for customer NL_104934",
pathFormatted: "req_animal-api-sync-7eb9d",
stats: {
    "name": "Animal-api sync for customer NL_104934",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "44430",
        "ok": "44430",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "44430",
        "ok": "44430",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "44430",
        "ok": "44430",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "44430",
        "ok": "44430",
        "ko": "-"
    },
    "percentiles2": {
        "total": "44430",
        "ok": "44430",
        "ko": "-"
    },
    "percentiles3": {
        "total": "44430",
        "ok": "44430",
        "ko": "-"
    },
    "percentiles4": {
        "total": "44430",
        "ok": "44430",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-3a032": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_122396",
path: "Animal-api sync for customer NL_122396",
pathFormatted: "req_animal-api-sync-3a032",
stats: {
    "name": "Animal-api sync for customer NL_122396",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "maxResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "meanResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles4": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-1cf92": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105173",
path: "Animal-api sync for customer NL_105173",
pathFormatted: "req_animal-api-sync-1cf92",
stats: {
    "name": "Animal-api sync for customer NL_105173",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "maxResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "meanResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles4": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-b22b6": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_131590",
path: "Animal-api sync for customer NL_131590",
pathFormatted: "req_animal-api-sync-b22b6",
stats: {
    "name": "Animal-api sync for customer NL_131590",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "maxResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "meanResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles2": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles3": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles4": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-1080b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110702",
path: "Animal-api sync for customer NL_110702",
pathFormatted: "req_animal-api-sync-1080b",
stats: {
    "name": "Animal-api sync for customer NL_110702",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "maxResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "meanResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles2": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles3": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles4": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-8580c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_119732",
path: "Animal-api sync for customer NL_119732",
pathFormatted: "req_animal-api-sync-8580c",
stats: {
    "name": "Animal-api sync for customer NL_119732",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "maxResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "meanResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles2": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles3": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles4": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-8dfa9": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_116373",
path: "Animal-api sync for customer NL_116373",
pathFormatted: "req_animal-api-sync-8dfa9",
stats: {
    "name": "Animal-api sync for customer NL_116373",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "maxResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "meanResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles2": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles3": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles4": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-c298e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_124623",
path: "Animal-api sync for customer NL_124623",
pathFormatted: "req_animal-api-sync-c298e",
stats: {
    "name": "Animal-api sync for customer NL_124623",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "maxResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "meanResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles2": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles3": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles4": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-18c50": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_124684",
path: "Animal-api sync for customer NL_124684",
pathFormatted: "req_animal-api-sync-18c50",
stats: {
    "name": "Animal-api sync for customer NL_124684",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "maxResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "meanResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles2": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles3": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles4": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-11d7b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111681",
path: "Animal-api sync for customer NL_111681",
pathFormatted: "req_animal-api-sync-11d7b",
stats: {
    "name": "Animal-api sync for customer NL_111681",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "maxResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "meanResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles4": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-f0b42": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_107121",
path: "Animal-api sync for customer NL_107121",
pathFormatted: "req_animal-api-sync-f0b42",
stats: {
    "name": "Animal-api sync for customer NL_107121",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "maxResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "meanResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles2": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles3": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles4": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-de92c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_109851",
path: "Animal-api sync for customer NL_109851",
pathFormatted: "req_animal-api-sync-de92c",
stats: {
    "name": "Animal-api sync for customer NL_109851",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "maxResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "meanResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles2": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles3": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles4": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-31610": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_109147",
path: "Animal-api sync for customer NL_109147",
pathFormatted: "req_animal-api-sync-31610",
stats: {
    "name": "Animal-api sync for customer NL_109147",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "27740",
        "ok": "27740",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "27740",
        "ok": "27740",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "27740",
        "ok": "27740",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "27740",
        "ok": "27740",
        "ko": "-"
    },
    "percentiles2": {
        "total": "27740",
        "ok": "27740",
        "ko": "-"
    },
    "percentiles3": {
        "total": "27740",
        "ok": "27740",
        "ko": "-"
    },
    "percentiles4": {
        "total": "27740",
        "ok": "27740",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a6474": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_217194",
path: "Animal-api sync for customer NL_217194",
pathFormatted: "req_animal-api-sync-a6474",
stats: {
    "name": "Animal-api sync for customer NL_217194",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "maxResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "meanResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles2": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles3": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles4": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-735e5": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_162624",
path: "Animal-api sync for customer BE_162624",
pathFormatted: "req_animal-api-sync-735e5",
stats: {
    "name": "Animal-api sync for customer BE_162624",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "44629",
        "ok": "44629",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "44629",
        "ok": "44629",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "44629",
        "ok": "44629",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "44629",
        "ok": "44629",
        "ko": "-"
    },
    "percentiles2": {
        "total": "44629",
        "ok": "44629",
        "ko": "-"
    },
    "percentiles3": {
        "total": "44629",
        "ok": "44629",
        "ko": "-"
    },
    "percentiles4": {
        "total": "44629",
        "ok": "44629",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-da049": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111349",
path: "Animal-api sync for customer NL_111349",
pathFormatted: "req_animal-api-sync-da049",
stats: {
    "name": "Animal-api sync for customer NL_111349",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "maxResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "meanResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles2": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles3": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles4": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-0945e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_120830",
path: "Animal-api sync for customer NL_120830",
pathFormatted: "req_animal-api-sync-0945e",
stats: {
    "name": "Animal-api sync for customer NL_120830",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "57605",
        "ok": "57605",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "57605",
        "ok": "57605",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "57605",
        "ok": "57605",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "57605",
        "ok": "57605",
        "ko": "-"
    },
    "percentiles2": {
        "total": "57605",
        "ok": "57605",
        "ko": "-"
    },
    "percentiles3": {
        "total": "57605",
        "ok": "57605",
        "ko": "-"
    },
    "percentiles4": {
        "total": "57605",
        "ok": "57605",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d1db6": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_154426",
path: "Animal-api sync for customer BE_154426",
pathFormatted: "req_animal-api-sync-d1db6",
stats: {
    "name": "Animal-api sync for customer BE_154426",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "maxResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "meanResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles2": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles3": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles4": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-3b46a": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_160734",
path: "Animal-api sync for customer NL_160734",
pathFormatted: "req_animal-api-sync-3b46a",
stats: {
    "name": "Animal-api sync for customer NL_160734",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "maxResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "meanResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles2": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles3": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles4": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-4b701": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_122804",
path: "Animal-api sync for customer NL_122804",
pathFormatted: "req_animal-api-sync-4b701",
stats: {
    "name": "Animal-api sync for customer NL_122804",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "maxResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "meanResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles2": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles3": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles4": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-3f46c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110510",
path: "Animal-api sync for customer NL_110510",
pathFormatted: "req_animal-api-sync-3f46c",
stats: {
    "name": "Animal-api sync for customer NL_110510",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "maxResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "meanResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles2": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles3": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles4": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-e1650": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_189253",
path: "Animal-api sync for customer BE_189253",
pathFormatted: "req_animal-api-sync-e1650",
stats: {
    "name": "Animal-api sync for customer BE_189253",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "maxResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "meanResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles2": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles3": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles4": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-5a990": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_196831",
path: "Animal-api sync for customer BE_196831",
pathFormatted: "req_animal-api-sync-5a990",
stats: {
    "name": "Animal-api sync for customer BE_196831",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "maxResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "meanResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles2": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles3": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles4": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-8b170": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_128376",
path: "Animal-api sync for customer NL_128376",
pathFormatted: "req_animal-api-sync-8b170",
stats: {
    "name": "Animal-api sync for customer NL_128376",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "maxResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "meanResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles2": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles3": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles4": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-9e2d7": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_127839",
path: "Animal-api sync for customer NL_127839",
pathFormatted: "req_animal-api-sync-9e2d7",
stats: {
    "name": "Animal-api sync for customer NL_127839",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "maxResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "meanResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles2": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles3": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles4": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-cb3f0": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_109354",
path: "Animal-api sync for customer NL_109354",
pathFormatted: "req_animal-api-sync-cb3f0",
stats: {
    "name": "Animal-api sync for customer NL_109354",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "49888",
        "ok": "49888",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "49888",
        "ok": "49888",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "49888",
        "ok": "49888",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "49888",
        "ok": "49888",
        "ko": "-"
    },
    "percentiles2": {
        "total": "49888",
        "ok": "49888",
        "ko": "-"
    },
    "percentiles3": {
        "total": "49888",
        "ok": "49888",
        "ko": "-"
    },
    "percentiles4": {
        "total": "49888",
        "ok": "49888",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-77e7f": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_116276",
path: "Animal-api sync for customer NL_116276",
pathFormatted: "req_animal-api-sync-77e7f",
stats: {
    "name": "Animal-api sync for customer NL_116276",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "maxResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "meanResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles4": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-e91bf": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111232",
path: "Animal-api sync for customer NL_111232",
pathFormatted: "req_animal-api-sync-e91bf",
stats: {
    "name": "Animal-api sync for customer NL_111232",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "maxResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "meanResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles2": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles3": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles4": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-df608": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_106231",
path: "Animal-api sync for customer NL_106231",
pathFormatted: "req_animal-api-sync-df608",
stats: {
    "name": "Animal-api sync for customer NL_106231",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "maxResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "meanResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles2": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles3": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles4": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-96235": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_119086",
path: "Animal-api sync for customer NL_119086",
pathFormatted: "req_animal-api-sync-96235",
stats: {
    "name": "Animal-api sync for customer NL_119086",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "maxResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "meanResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles4": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-b66f8": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110851",
path: "Animal-api sync for customer NL_110851",
pathFormatted: "req_animal-api-sync-b66f8",
stats: {
    "name": "Animal-api sync for customer NL_110851",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "maxResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "meanResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles2": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles3": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles4": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-fc706": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_108712",
path: "Animal-api sync for customer NL_108712",
pathFormatted: "req_animal-api-sync-fc706",
stats: {
    "name": "Animal-api sync for customer NL_108712",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "maxResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "meanResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles2": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles3": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles4": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-77463": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_142162",
path: "Animal-api sync for customer NL_142162",
pathFormatted: "req_animal-api-sync-77463",
stats: {
    "name": "Animal-api sync for customer NL_142162",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "maxResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "meanResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles2": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles3": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles4": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-3e0f1": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_119459",
path: "Animal-api sync for customer NL_119459",
pathFormatted: "req_animal-api-sync-3e0f1",
stats: {
    "name": "Animal-api sync for customer NL_119459",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "maxResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "meanResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles4": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-f9107": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_121807",
path: "Animal-api sync for customer NL_121807",
pathFormatted: "req_animal-api-sync-f9107",
stats: {
    "name": "Animal-api sync for customer NL_121807",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "maxResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "meanResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles2": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles3": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles4": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-82f9e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110685",
path: "Animal-api sync for customer NL_110685",
pathFormatted: "req_animal-api-sync-82f9e",
stats: {
    "name": "Animal-api sync for customer NL_110685",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "maxResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "meanResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles4": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-39816": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_145536",
path: "Animal-api sync for customer BE_145536",
pathFormatted: "req_animal-api-sync-39816",
stats: {
    "name": "Animal-api sync for customer BE_145536",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "maxResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "meanResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles2": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles3": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles4": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-66198": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_107001",
path: "Animal-api sync for customer NL_107001",
pathFormatted: "req_animal-api-sync-66198",
stats: {
    "name": "Animal-api sync for customer NL_107001",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "maxResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "meanResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles2": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles3": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles4": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-299ce": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_119621",
path: "Animal-api sync for customer NL_119621",
pathFormatted: "req_animal-api-sync-299ce",
stats: {
    "name": "Animal-api sync for customer NL_119621",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "55274",
        "ok": "55274",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "55274",
        "ok": "55274",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "55274",
        "ok": "55274",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "55274",
        "ok": "55274",
        "ko": "-"
    },
    "percentiles2": {
        "total": "55274",
        "ok": "55274",
        "ko": "-"
    },
    "percentiles3": {
        "total": "55274",
        "ok": "55274",
        "ko": "-"
    },
    "percentiles4": {
        "total": "55274",
        "ok": "55274",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-cb563": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_195750",
path: "Animal-api sync for customer BE_195750",
pathFormatted: "req_animal-api-sync-cb563",
stats: {
    "name": "Animal-api sync for customer BE_195750",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "maxResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "meanResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles2": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles3": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles4": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-86128": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110109",
path: "Animal-api sync for customer NL_110109",
pathFormatted: "req_animal-api-sync-86128",
stats: {
    "name": "Animal-api sync for customer NL_110109",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "maxResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "meanResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles2": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles3": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles4": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-2a7a0": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_119620",
path: "Animal-api sync for customer NL_119620",
pathFormatted: "req_animal-api-sync-2a7a0",
stats: {
    "name": "Animal-api sync for customer NL_119620",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "52476",
        "ok": "52476",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "52476",
        "ok": "52476",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "52476",
        "ok": "52476",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "52476",
        "ok": "52476",
        "ko": "-"
    },
    "percentiles2": {
        "total": "52476",
        "ok": "52476",
        "ko": "-"
    },
    "percentiles3": {
        "total": "52476",
        "ok": "52476",
        "ko": "-"
    },
    "percentiles4": {
        "total": "52476",
        "ok": "52476",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-f1bc5": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_106522",
path: "Animal-api sync for customer NL_106522",
pathFormatted: "req_animal-api-sync-f1bc5",
stats: {
    "name": "Animal-api sync for customer NL_106522",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "55443",
        "ok": "55443",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "55443",
        "ok": "55443",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "55443",
        "ok": "55443",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "55443",
        "ok": "55443",
        "ko": "-"
    },
    "percentiles2": {
        "total": "55443",
        "ok": "55443",
        "ko": "-"
    },
    "percentiles3": {
        "total": "55443",
        "ok": "55443",
        "ko": "-"
    },
    "percentiles4": {
        "total": "55443",
        "ok": "55443",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-fa8fb": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110675",
path: "Animal-api sync for customer NL_110675",
pathFormatted: "req_animal-api-sync-fa8fb",
stats: {
    "name": "Animal-api sync for customer NL_110675",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "maxResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "meanResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles2": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles3": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles4": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-9a989": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105564",
path: "Animal-api sync for customer NL_105564",
pathFormatted: "req_animal-api-sync-9a989",
stats: {
    "name": "Animal-api sync for customer NL_105564",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "maxResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "meanResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles2": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles3": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles4": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-1d50e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_107593",
path: "Animal-api sync for customer NL_107593",
pathFormatted: "req_animal-api-sync-1d50e",
stats: {
    "name": "Animal-api sync for customer NL_107593",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "maxResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "meanResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles4": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-3fad8": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_119768",
path: "Animal-api sync for customer NL_119768",
pathFormatted: "req_animal-api-sync-3fad8",
stats: {
    "name": "Animal-api sync for customer NL_119768",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "maxResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "meanResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles2": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles3": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles4": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-1cdda": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_106961",
path: "Animal-api sync for customer NL_106961",
pathFormatted: "req_animal-api-sync-1cdda",
stats: {
    "name": "Animal-api sync for customer NL_106961",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "maxResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "meanResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles2": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles3": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles4": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-028d0": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_139052",
path: "Animal-api sync for customer NL_139052",
pathFormatted: "req_animal-api-sync-028d0",
stats: {
    "name": "Animal-api sync for customer NL_139052",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "maxResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "meanResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles2": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles3": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles4": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-3df0e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_136411",
path: "Animal-api sync for customer NL_136411",
pathFormatted: "req_animal-api-sync-3df0e",
stats: {
    "name": "Animal-api sync for customer NL_136411",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "maxResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "meanResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles2": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles3": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles4": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-34074": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104611",
path: "Animal-api sync for customer NL_104611",
pathFormatted: "req_animal-api-sync-34074",
stats: {
    "name": "Animal-api sync for customer NL_104611",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "maxResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "meanResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles2": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles3": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles4": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-4e4dd": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_132571",
path: "Animal-api sync for customer NL_132571",
pathFormatted: "req_animal-api-sync-4e4dd",
stats: {
    "name": "Animal-api sync for customer NL_132571",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "maxResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "meanResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles2": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles3": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles4": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-b76bc": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_121619",
path: "Animal-api sync for customer NL_121619",
pathFormatted: "req_animal-api-sync-b76bc",
stats: {
    "name": "Animal-api sync for customer NL_121619",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "maxResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "meanResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles2": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles3": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles4": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-67b51": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_208384",
path: "Animal-api sync for customer NL_208384",
pathFormatted: "req_animal-api-sync-67b51",
stats: {
    "name": "Animal-api sync for customer NL_208384",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "maxResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "meanResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles2": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles3": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles4": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-f5467": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_164463",
path: "Animal-api sync for customer NL_164463",
pathFormatted: "req_animal-api-sync-f5467",
stats: {
    "name": "Animal-api sync for customer NL_164463",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "maxResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "meanResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles2": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles3": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles4": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-a921a": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_158726",
path: "Animal-api sync for customer BE_158726",
pathFormatted: "req_animal-api-sync-a921a",
stats: {
    "name": "Animal-api sync for customer BE_158726",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "maxResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "meanResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles2": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles3": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles4": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-43d17": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_159367",
path: "Animal-api sync for customer NL_159367",
pathFormatted: "req_animal-api-sync-43d17",
stats: {
    "name": "Animal-api sync for customer NL_159367",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "maxResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "meanResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles2": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles3": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles4": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-7b4fd": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_207082",
path: "Animal-api sync for customer BE_207082",
pathFormatted: "req_animal-api-sync-7b4fd",
stats: {
    "name": "Animal-api sync for customer BE_207082",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "maxResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "meanResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles2": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles3": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles4": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-774bc": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105254",
path: "Animal-api sync for customer NL_105254",
pathFormatted: "req_animal-api-sync-774bc",
stats: {
    "name": "Animal-api sync for customer NL_105254",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "maxResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "meanResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles2": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles3": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles4": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-72b5a": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_129227",
path: "Animal-api sync for customer NL_129227",
pathFormatted: "req_animal-api-sync-72b5a",
stats: {
    "name": "Animal-api sync for customer NL_129227",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "maxResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "meanResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles2": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles3": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles4": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-e7477": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_120288",
path: "Animal-api sync for customer NL_120288",
pathFormatted: "req_animal-api-sync-e7477",
stats: {
    "name": "Animal-api sync for customer NL_120288",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "maxResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "meanResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles2": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles3": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles4": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-b6fe1": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_112227",
path: "Animal-api sync for customer NL_112227",
pathFormatted: "req_animal-api-sync-b6fe1",
stats: {
    "name": "Animal-api sync for customer NL_112227",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "maxResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "meanResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles2": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles3": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles4": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-6891c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111812",
path: "Animal-api sync for customer NL_111812",
pathFormatted: "req_animal-api-sync-6891c",
stats: {
    "name": "Animal-api sync for customer NL_111812",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "maxResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "meanResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles4": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-5324e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_112685",
path: "Animal-api sync for customer NL_112685",
pathFormatted: "req_animal-api-sync-5324e",
stats: {
    "name": "Animal-api sync for customer NL_112685",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "maxResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "meanResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles4": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-39487": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_153606",
path: "Animal-api sync for customer BE_153606",
pathFormatted: "req_animal-api-sync-39487",
stats: {
    "name": "Animal-api sync for customer BE_153606",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "maxResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "meanResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles4": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-115a1": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_106548",
path: "Animal-api sync for customer NL_106548",
pathFormatted: "req_animal-api-sync-115a1",
stats: {
    "name": "Animal-api sync for customer NL_106548",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "maxResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "meanResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles4": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-dfd7c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_139331",
path: "Animal-api sync for customer NL_139331",
pathFormatted: "req_animal-api-sync-dfd7c",
stats: {
    "name": "Animal-api sync for customer NL_139331",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "maxResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "meanResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles4": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-c6932": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_133968",
path: "Animal-api sync for customer NL_133968",
pathFormatted: "req_animal-api-sync-c6932",
stats: {
    "name": "Animal-api sync for customer NL_133968",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "maxResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "meanResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles2": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles3": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles4": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-4cd78": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111414",
path: "Animal-api sync for customer NL_111414",
pathFormatted: "req_animal-api-sync-4cd78",
stats: {
    "name": "Animal-api sync for customer NL_111414",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "maxResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "meanResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles2": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles3": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles4": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-0fed7": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_109760",
path: "Animal-api sync for customer NL_109760",
pathFormatted: "req_animal-api-sync-0fed7",
stats: {
    "name": "Animal-api sync for customer NL_109760",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "maxResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "meanResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles2": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles3": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles4": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-f47e6": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_120627",
path: "Animal-api sync for customer NL_120627",
pathFormatted: "req_animal-api-sync-f47e6",
stats: {
    "name": "Animal-api sync for customer NL_120627",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "maxResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "meanResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles2": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles3": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles4": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-66c16": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_112114",
path: "Animal-api sync for customer NL_112114",
pathFormatted: "req_animal-api-sync-66c16",
stats: {
    "name": "Animal-api sync for customer NL_112114",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "maxResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "meanResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles2": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles3": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles4": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-79b8e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_116848",
path: "Animal-api sync for customer NL_116848",
pathFormatted: "req_animal-api-sync-79b8e",
stats: {
    "name": "Animal-api sync for customer NL_116848",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "maxResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "meanResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles4": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-5420f": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110280",
path: "Animal-api sync for customer NL_110280",
pathFormatted: "req_animal-api-sync-5420f",
stats: {
    "name": "Animal-api sync for customer NL_110280",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "maxResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "meanResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles2": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles3": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles4": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-63b47": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111966",
path: "Animal-api sync for customer NL_111966",
pathFormatted: "req_animal-api-sync-63b47",
stats: {
    "name": "Animal-api sync for customer NL_111966",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "maxResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "meanResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles4": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-fbcf4": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_128711",
path: "Animal-api sync for customer NL_128711",
pathFormatted: "req_animal-api-sync-fbcf4",
stats: {
    "name": "Animal-api sync for customer NL_128711",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "maxResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "meanResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles4": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-d05fc": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_131083",
path: "Animal-api sync for customer NL_131083",
pathFormatted: "req_animal-api-sync-d05fc",
stats: {
    "name": "Animal-api sync for customer NL_131083",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "maxResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "meanResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles4": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-ed470": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_115514",
path: "Animal-api sync for customer NL_115514",
pathFormatted: "req_animal-api-sync-ed470",
stats: {
    "name": "Animal-api sync for customer NL_115514",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "maxResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "meanResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles2": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles3": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles4": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-29375": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_121682",
path: "Animal-api sync for customer NL_121682",
pathFormatted: "req_animal-api-sync-29375",
stats: {
    "name": "Animal-api sync for customer NL_121682",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "maxResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "meanResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles2": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles3": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles4": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-4e713": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111266",
path: "Animal-api sync for customer NL_111266",
pathFormatted: "req_animal-api-sync-4e713",
stats: {
    "name": "Animal-api sync for customer NL_111266",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "maxResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "meanResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles4": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-5df96": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_114543",
path: "Animal-api sync for customer NL_114543",
pathFormatted: "req_animal-api-sync-5df96",
stats: {
    "name": "Animal-api sync for customer NL_114543",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "maxResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "meanResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles2": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles3": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles4": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-38246": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_118506",
path: "Animal-api sync for customer NL_118506",
pathFormatted: "req_animal-api-sync-38246",
stats: {
    "name": "Animal-api sync for customer NL_118506",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "58387",
        "ok": "58387",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "58387",
        "ok": "58387",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "58387",
        "ok": "58387",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "58387",
        "ok": "58387",
        "ko": "-"
    },
    "percentiles2": {
        "total": "58387",
        "ok": "58387",
        "ko": "-"
    },
    "percentiles3": {
        "total": "58387",
        "ok": "58387",
        "ko": "-"
    },
    "percentiles4": {
        "total": "58387",
        "ok": "58387",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-3d430": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_134950",
path: "Animal-api sync for customer NL_134950",
pathFormatted: "req_animal-api-sync-3d430",
stats: {
    "name": "Animal-api sync for customer NL_134950",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "maxResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "meanResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles2": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles3": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles4": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-a667b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_103980",
path: "Animal-api sync for customer NL_103980",
pathFormatted: "req_animal-api-sync-a667b",
stats: {
    "name": "Animal-api sync for customer NL_103980",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "maxResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "meanResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles2": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles3": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles4": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-92bed": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105164",
path: "Animal-api sync for customer NL_105164",
pathFormatted: "req_animal-api-sync-92bed",
stats: {
    "name": "Animal-api sync for customer NL_105164",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "maxResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "meanResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles2": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles3": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles4": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-74e33": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_189313",
path: "Animal-api sync for customer BE_189313",
pathFormatted: "req_animal-api-sync-74e33",
stats: {
    "name": "Animal-api sync for customer BE_189313",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "maxResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "meanResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles2": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles3": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles4": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-aaf86": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_130803",
path: "Animal-api sync for customer NL_130803",
pathFormatted: "req_animal-api-sync-aaf86",
stats: {
    "name": "Animal-api sync for customer NL_130803",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "maxResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "meanResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles2": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles3": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles4": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-390c0": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_113006",
path: "Animal-api sync for customer NL_113006",
pathFormatted: "req_animal-api-sync-390c0",
stats: {
    "name": "Animal-api sync for customer NL_113006",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "maxResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "meanResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles2": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles3": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles4": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-363df": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_120238",
path: "Animal-api sync for customer NL_120238",
pathFormatted: "req_animal-api-sync-363df",
stats: {
    "name": "Animal-api sync for customer NL_120238",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "53354",
        "ok": "53354",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "53354",
        "ok": "53354",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "53354",
        "ok": "53354",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "53354",
        "ok": "53354",
        "ko": "-"
    },
    "percentiles2": {
        "total": "53354",
        "ok": "53354",
        "ko": "-"
    },
    "percentiles3": {
        "total": "53354",
        "ok": "53354",
        "ko": "-"
    },
    "percentiles4": {
        "total": "53354",
        "ok": "53354",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e70df": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_129794",
path: "Animal-api sync for customer NL_129794",
pathFormatted: "req_animal-api-sync-e70df",
stats: {
    "name": "Animal-api sync for customer NL_129794",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "53337",
        "ok": "53337",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "53337",
        "ok": "53337",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "53337",
        "ok": "53337",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "53337",
        "ok": "53337",
        "ko": "-"
    },
    "percentiles2": {
        "total": "53337",
        "ok": "53337",
        "ko": "-"
    },
    "percentiles3": {
        "total": "53337",
        "ok": "53337",
        "ko": "-"
    },
    "percentiles4": {
        "total": "53337",
        "ok": "53337",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-a9219": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_103449",
path: "Animal-api sync for customer NL_103449",
pathFormatted: "req_animal-api-sync-a9219",
stats: {
    "name": "Animal-api sync for customer NL_103449",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "maxResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "meanResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles2": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles3": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles4": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-30320": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_128704",
path: "Animal-api sync for customer NL_128704",
pathFormatted: "req_animal-api-sync-30320",
stats: {
    "name": "Animal-api sync for customer NL_128704",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "maxResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "meanResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles2": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles3": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles4": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-c50f5": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111928",
path: "Animal-api sync for customer NL_111928",
pathFormatted: "req_animal-api-sync-c50f5",
stats: {
    "name": "Animal-api sync for customer NL_111928",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "maxResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "meanResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles2": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles3": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles4": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-294ca": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111076",
path: "Animal-api sync for customer NL_111076",
pathFormatted: "req_animal-api-sync-294ca",
stats: {
    "name": "Animal-api sync for customer NL_111076",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "maxResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "meanResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles2": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles3": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles4": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-080dc": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_131881",
path: "Animal-api sync for customer NL_131881",
pathFormatted: "req_animal-api-sync-080dc",
stats: {
    "name": "Animal-api sync for customer NL_131881",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "53830",
        "ok": "53830",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "53830",
        "ok": "53830",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "53830",
        "ok": "53830",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "53830",
        "ok": "53830",
        "ko": "-"
    },
    "percentiles2": {
        "total": "53830",
        "ok": "53830",
        "ko": "-"
    },
    "percentiles3": {
        "total": "53830",
        "ok": "53830",
        "ko": "-"
    },
    "percentiles4": {
        "total": "53830",
        "ok": "53830",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-9f153": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_113848",
path: "Animal-api sync for customer NL_113848",
pathFormatted: "req_animal-api-sync-9f153",
stats: {
    "name": "Animal-api sync for customer NL_113848",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "51126",
        "ok": "51126",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "51126",
        "ok": "51126",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "51126",
        "ok": "51126",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "51126",
        "ok": "51126",
        "ko": "-"
    },
    "percentiles2": {
        "total": "51126",
        "ok": "51126",
        "ko": "-"
    },
    "percentiles3": {
        "total": "51126",
        "ok": "51126",
        "ko": "-"
    },
    "percentiles4": {
        "total": "51126",
        "ok": "51126",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-084e7": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_108590",
path: "Animal-api sync for customer NL_108590",
pathFormatted: "req_animal-api-sync-084e7",
stats: {
    "name": "Animal-api sync for customer NL_108590",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "maxResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "meanResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles2": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles3": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles4": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-93bcd": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_107077",
path: "Animal-api sync for customer NL_107077",
pathFormatted: "req_animal-api-sync-93bcd",
stats: {
    "name": "Animal-api sync for customer NL_107077",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "43737",
        "ok": "43737",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "43737",
        "ok": "43737",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "43737",
        "ok": "43737",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "43737",
        "ok": "43737",
        "ko": "-"
    },
    "percentiles2": {
        "total": "43737",
        "ok": "43737",
        "ko": "-"
    },
    "percentiles3": {
        "total": "43737",
        "ok": "43737",
        "ko": "-"
    },
    "percentiles4": {
        "total": "43737",
        "ok": "43737",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-43480": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_211973",
path: "Animal-api sync for customer BE_211973",
pathFormatted: "req_animal-api-sync-43480",
stats: {
    "name": "Animal-api sync for customer BE_211973",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "maxResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "meanResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles2": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles3": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles4": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-a5109": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_124277",
path: "Animal-api sync for customer NL_124277",
pathFormatted: "req_animal-api-sync-a5109",
stats: {
    "name": "Animal-api sync for customer NL_124277",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "maxResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "meanResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles2": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles3": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles4": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-53304": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_108829",
path: "Animal-api sync for customer NL_108829",
pathFormatted: "req_animal-api-sync-53304",
stats: {
    "name": "Animal-api sync for customer NL_108829",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "maxResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "meanResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles2": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles3": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles4": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-84fc1": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_131210",
path: "Animal-api sync for customer NL_131210",
pathFormatted: "req_animal-api-sync-84fc1",
stats: {
    "name": "Animal-api sync for customer NL_131210",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "52960",
        "ok": "52960",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "52960",
        "ok": "52960",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "52960",
        "ok": "52960",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "52960",
        "ok": "52960",
        "ko": "-"
    },
    "percentiles2": {
        "total": "52960",
        "ok": "52960",
        "ko": "-"
    },
    "percentiles3": {
        "total": "52960",
        "ok": "52960",
        "ko": "-"
    },
    "percentiles4": {
        "total": "52960",
        "ok": "52960",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-fc8bc": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_119277",
path: "Animal-api sync for customer NL_119277",
pathFormatted: "req_animal-api-sync-fc8bc",
stats: {
    "name": "Animal-api sync for customer NL_119277",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "maxResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "meanResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles2": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles3": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles4": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-b0637": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_109813",
path: "Animal-api sync for customer NL_109813",
pathFormatted: "req_animal-api-sync-b0637",
stats: {
    "name": "Animal-api sync for customer NL_109813",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "maxResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "meanResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles2": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles3": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles4": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-d7914": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_189320",
path: "Animal-api sync for customer NL_189320",
pathFormatted: "req_animal-api-sync-d7914",
stats: {
    "name": "Animal-api sync for customer NL_189320",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "49988",
        "ok": "49988",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "49988",
        "ok": "49988",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "49988",
        "ok": "49988",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "49988",
        "ok": "49988",
        "ko": "-"
    },
    "percentiles2": {
        "total": "49988",
        "ok": "49988",
        "ko": "-"
    },
    "percentiles3": {
        "total": "49988",
        "ok": "49988",
        "ko": "-"
    },
    "percentiles4": {
        "total": "49988",
        "ok": "49988",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-acdb8": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_113754",
path: "Animal-api sync for customer NL_113754",
pathFormatted: "req_animal-api-sync-acdb8",
stats: {
    "name": "Animal-api sync for customer NL_113754",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "maxResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "meanResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles4": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-36730": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_135526",
path: "Animal-api sync for customer NL_135526",
pathFormatted: "req_animal-api-sync-36730",
stats: {
    "name": "Animal-api sync for customer NL_135526",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "37498",
        "ok": "37498",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "37498",
        "ok": "37498",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "37498",
        "ok": "37498",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "37498",
        "ok": "37498",
        "ko": "-"
    },
    "percentiles2": {
        "total": "37498",
        "ok": "37498",
        "ko": "-"
    },
    "percentiles3": {
        "total": "37498",
        "ok": "37498",
        "ko": "-"
    },
    "percentiles4": {
        "total": "37498",
        "ok": "37498",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-1e909": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_154872",
path: "Animal-api sync for customer BE_154872",
pathFormatted: "req_animal-api-sync-1e909",
stats: {
    "name": "Animal-api sync for customer BE_154872",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "53637",
        "ok": "53637",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "53637",
        "ok": "53637",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "53637",
        "ok": "53637",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "53637",
        "ok": "53637",
        "ko": "-"
    },
    "percentiles2": {
        "total": "53637",
        "ok": "53637",
        "ko": "-"
    },
    "percentiles3": {
        "total": "53637",
        "ok": "53637",
        "ko": "-"
    },
    "percentiles4": {
        "total": "53637",
        "ok": "53637",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-f54d2": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_125689",
path: "Animal-api sync for customer NL_125689",
pathFormatted: "req_animal-api-sync-f54d2",
stats: {
    "name": "Animal-api sync for customer NL_125689",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "maxResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "meanResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles2": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles3": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles4": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-baa35": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110539",
path: "Animal-api sync for customer NL_110539",
pathFormatted: "req_animal-api-sync-baa35",
stats: {
    "name": "Animal-api sync for customer NL_110539",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "maxResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "meanResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles2": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles3": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles4": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-14454": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104271",
path: "Animal-api sync for customer NL_104271",
pathFormatted: "req_animal-api-sync-14454",
stats: {
    "name": "Animal-api sync for customer NL_104271",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "maxResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "meanResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles2": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles3": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles4": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-b6907": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_123943",
path: "Animal-api sync for customer NL_123943",
pathFormatted: "req_animal-api-sync-b6907",
stats: {
    "name": "Animal-api sync for customer NL_123943",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "maxResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "meanResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles2": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles3": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles4": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-c4646": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110070",
path: "Animal-api sync for customer NL_110070",
pathFormatted: "req_animal-api-sync-c4646",
stats: {
    "name": "Animal-api sync for customer NL_110070",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "maxResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "meanResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles2": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles3": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles4": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-0eeba": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_114348",
path: "Animal-api sync for customer NL_114348",
pathFormatted: "req_animal-api-sync-0eeba",
stats: {
    "name": "Animal-api sync for customer NL_114348",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "maxResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "meanResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles2": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles3": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles4": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-5c35b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_114376",
path: "Animal-api sync for customer NL_114376",
pathFormatted: "req_animal-api-sync-5c35b",
stats: {
    "name": "Animal-api sync for customer NL_114376",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "26144",
        "ok": "26144",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "26144",
        "ok": "26144",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "26144",
        "ok": "26144",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "26144",
        "ok": "26144",
        "ko": "-"
    },
    "percentiles2": {
        "total": "26144",
        "ok": "26144",
        "ko": "-"
    },
    "percentiles3": {
        "total": "26144",
        "ok": "26144",
        "ko": "-"
    },
    "percentiles4": {
        "total": "26144",
        "ok": "26144",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-451e8": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_121268",
path: "Animal-api sync for customer NL_121268",
pathFormatted: "req_animal-api-sync-451e8",
stats: {
    "name": "Animal-api sync for customer NL_121268",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "33266",
        "ok": "33266",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "33266",
        "ok": "33266",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "33266",
        "ok": "33266",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "33266",
        "ok": "33266",
        "ko": "-"
    },
    "percentiles2": {
        "total": "33266",
        "ok": "33266",
        "ko": "-"
    },
    "percentiles3": {
        "total": "33266",
        "ok": "33266",
        "ko": "-"
    },
    "percentiles4": {
        "total": "33266",
        "ok": "33266",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e42e2": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_128581",
path: "Animal-api sync for customer NL_128581",
pathFormatted: "req_animal-api-sync-e42e2",
stats: {
    "name": "Animal-api sync for customer NL_128581",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "31832",
        "ok": "31832",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "31832",
        "ok": "31832",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "31832",
        "ok": "31832",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "31832",
        "ok": "31832",
        "ko": "-"
    },
    "percentiles2": {
        "total": "31832",
        "ok": "31832",
        "ko": "-"
    },
    "percentiles3": {
        "total": "31832",
        "ok": "31832",
        "ko": "-"
    },
    "percentiles4": {
        "total": "31832",
        "ok": "31832",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b164c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_120434",
path: "Animal-api sync for customer NL_120434",
pathFormatted: "req_animal-api-sync-b164c",
stats: {
    "name": "Animal-api sync for customer NL_120434",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "maxResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "meanResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles2": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles3": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles4": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-7461a": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_120398",
path: "Animal-api sync for customer NL_120398",
pathFormatted: "req_animal-api-sync-7461a",
stats: {
    "name": "Animal-api sync for customer NL_120398",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "58232",
        "ok": "58232",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "58232",
        "ok": "58232",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "58232",
        "ok": "58232",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "58232",
        "ok": "58232",
        "ko": "-"
    },
    "percentiles2": {
        "total": "58232",
        "ok": "58232",
        "ko": "-"
    },
    "percentiles3": {
        "total": "58232",
        "ok": "58232",
        "ko": "-"
    },
    "percentiles4": {
        "total": "58232",
        "ok": "58232",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-07a69": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_103947",
path: "Animal-api sync for customer NL_103947",
pathFormatted: "req_animal-api-sync-07a69",
stats: {
    "name": "Animal-api sync for customer NL_103947",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "maxResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "meanResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles2": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles3": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles4": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-c6405": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104467",
path: "Animal-api sync for customer NL_104467",
pathFormatted: "req_animal-api-sync-c6405",
stats: {
    "name": "Animal-api sync for customer NL_104467",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "maxResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "meanResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles2": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles3": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles4": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-23f13": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_125273",
path: "Animal-api sync for customer NL_125273",
pathFormatted: "req_animal-api-sync-23f13",
stats: {
    "name": "Animal-api sync for customer NL_125273",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "maxResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "meanResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles2": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles3": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles4": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-96127": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_161584",
path: "Animal-api sync for customer BE_161584",
pathFormatted: "req_animal-api-sync-96127",
stats: {
    "name": "Animal-api sync for customer BE_161584",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "maxResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "meanResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles2": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles3": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles4": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-614ac": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104734",
path: "Animal-api sync for customer NL_104734",
pathFormatted: "req_animal-api-sync-614ac",
stats: {
    "name": "Animal-api sync for customer NL_104734",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "maxResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "meanResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles2": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles3": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles4": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-2d56c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110590",
path: "Animal-api sync for customer NL_110590",
pathFormatted: "req_animal-api-sync-2d56c",
stats: {
    "name": "Animal-api sync for customer NL_110590",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "maxResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "meanResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles2": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles3": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles4": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-e31c7": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_210459",
path: "Animal-api sync for customer NL_210459",
pathFormatted: "req_animal-api-sync-e31c7",
stats: {
    "name": "Animal-api sync for customer NL_210459",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "maxResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "meanResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles2": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles3": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles4": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-7fca2": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_158798",
path: "Animal-api sync for customer NL_158798",
pathFormatted: "req_animal-api-sync-7fca2",
stats: {
    "name": "Animal-api sync for customer NL_158798",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "maxResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "meanResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles2": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles3": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles4": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-b8f29": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_153558",
path: "Animal-api sync for customer BE_153558",
pathFormatted: "req_animal-api-sync-b8f29",
stats: {
    "name": "Animal-api sync for customer BE_153558",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "30892",
        "ok": "30892",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "30892",
        "ok": "30892",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "30892",
        "ok": "30892",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "30892",
        "ok": "30892",
        "ko": "-"
    },
    "percentiles2": {
        "total": "30892",
        "ok": "30892",
        "ko": "-"
    },
    "percentiles3": {
        "total": "30892",
        "ok": "30892",
        "ko": "-"
    },
    "percentiles4": {
        "total": "30892",
        "ok": "30892",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-aa22e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_137034",
path: "Animal-api sync for customer NL_137034",
pathFormatted: "req_animal-api-sync-aa22e",
stats: {
    "name": "Animal-api sync for customer NL_137034",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "29169",
        "ok": "29169",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "29169",
        "ok": "29169",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "29169",
        "ok": "29169",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "29169",
        "ok": "29169",
        "ko": "-"
    },
    "percentiles2": {
        "total": "29169",
        "ok": "29169",
        "ko": "-"
    },
    "percentiles3": {
        "total": "29169",
        "ok": "29169",
        "ko": "-"
    },
    "percentiles4": {
        "total": "29169",
        "ok": "29169",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b5283": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_112270",
path: "Animal-api sync for customer NL_112270",
pathFormatted: "req_animal-api-sync-b5283",
stats: {
    "name": "Animal-api sync for customer NL_112270",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "maxResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "meanResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles2": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles3": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles4": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-10e14": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_113903",
path: "Animal-api sync for customer NL_113903",
pathFormatted: "req_animal-api-sync-10e14",
stats: {
    "name": "Animal-api sync for customer NL_113903",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "maxResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "meanResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles4": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-3dd2c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105439",
path: "Animal-api sync for customer NL_105439",
pathFormatted: "req_animal-api-sync-3dd2c",
stats: {
    "name": "Animal-api sync for customer NL_105439",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "maxResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "meanResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles2": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles3": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles4": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-8785d": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_207024",
path: "Animal-api sync for customer BE_207024",
pathFormatted: "req_animal-api-sync-8785d",
stats: {
    "name": "Animal-api sync for customer BE_207024",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "40238",
        "ok": "40238",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "40238",
        "ok": "40238",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "40238",
        "ok": "40238",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "40238",
        "ok": "40238",
        "ko": "-"
    },
    "percentiles2": {
        "total": "40238",
        "ok": "40238",
        "ko": "-"
    },
    "percentiles3": {
        "total": "40238",
        "ok": "40238",
        "ko": "-"
    },
    "percentiles4": {
        "total": "40238",
        "ok": "40238",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-010fe": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_129945",
path: "Animal-api sync for customer NL_129945",
pathFormatted: "req_animal-api-sync-010fe",
stats: {
    "name": "Animal-api sync for customer NL_129945",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "maxResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "meanResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles2": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles3": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles4": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-58bfa": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_131425",
path: "Animal-api sync for customer NL_131425",
pathFormatted: "req_animal-api-sync-58bfa",
stats: {
    "name": "Animal-api sync for customer NL_131425",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "maxResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "meanResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles2": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles3": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles4": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-9a226": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_113819",
path: "Animal-api sync for customer NL_113819",
pathFormatted: "req_animal-api-sync-9a226",
stats: {
    "name": "Animal-api sync for customer NL_113819",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "31504",
        "ok": "31504",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "31504",
        "ok": "31504",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "31504",
        "ok": "31504",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "31504",
        "ok": "31504",
        "ko": "-"
    },
    "percentiles2": {
        "total": "31504",
        "ok": "31504",
        "ko": "-"
    },
    "percentiles3": {
        "total": "31504",
        "ok": "31504",
        "ko": "-"
    },
    "percentiles4": {
        "total": "31504",
        "ok": "31504",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-44a53": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_121469",
path: "Animal-api sync for customer NL_121469",
pathFormatted: "req_animal-api-sync-44a53",
stats: {
    "name": "Animal-api sync for customer NL_121469",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "maxResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "meanResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles2": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles3": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles4": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-c3fd5": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104563",
path: "Animal-api sync for customer NL_104563",
pathFormatted: "req_animal-api-sync-c3fd5",
stats: {
    "name": "Animal-api sync for customer NL_104563",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60006",
        "ok": "-",
        "ko": "60006"
    },
    "maxResponseTime": {
        "total": "60006",
        "ok": "-",
        "ko": "60006"
    },
    "meanResponseTime": {
        "total": "60006",
        "ok": "-",
        "ko": "60006"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60006",
        "ok": "-",
        "ko": "60006"
    },
    "percentiles2": {
        "total": "60006",
        "ok": "-",
        "ko": "60006"
    },
    "percentiles3": {
        "total": "60006",
        "ok": "-",
        "ko": "60006"
    },
    "percentiles4": {
        "total": "60006",
        "ok": "-",
        "ko": "60006"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-2f86f": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_122640",
path: "Animal-api sync for customer NL_122640",
pathFormatted: "req_animal-api-sync-2f86f",
stats: {
    "name": "Animal-api sync for customer NL_122640",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "58384",
        "ok": "58384",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "58384",
        "ok": "58384",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "58384",
        "ok": "58384",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "58384",
        "ok": "58384",
        "ko": "-"
    },
    "percentiles2": {
        "total": "58384",
        "ok": "58384",
        "ko": "-"
    },
    "percentiles3": {
        "total": "58384",
        "ok": "58384",
        "ko": "-"
    },
    "percentiles4": {
        "total": "58384",
        "ok": "58384",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-447ce": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_136064",
path: "Animal-api sync for customer NL_136064",
pathFormatted: "req_animal-api-sync-447ce",
stats: {
    "name": "Animal-api sync for customer NL_136064",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "26180",
        "ok": "26180",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "26180",
        "ok": "26180",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "26180",
        "ok": "26180",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "26180",
        "ok": "26180",
        "ko": "-"
    },
    "percentiles2": {
        "total": "26180",
        "ok": "26180",
        "ko": "-"
    },
    "percentiles3": {
        "total": "26180",
        "ok": "26180",
        "ko": "-"
    },
    "percentiles4": {
        "total": "26180",
        "ok": "26180",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-02723": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_106633",
path: "Animal-api sync for customer NL_106633",
pathFormatted: "req_animal-api-sync-02723",
stats: {
    "name": "Animal-api sync for customer NL_106633",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "53090",
        "ok": "53090",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "53090",
        "ok": "53090",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "53090",
        "ok": "53090",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "53090",
        "ok": "53090",
        "ko": "-"
    },
    "percentiles2": {
        "total": "53090",
        "ok": "53090",
        "ko": "-"
    },
    "percentiles3": {
        "total": "53090",
        "ok": "53090",
        "ko": "-"
    },
    "percentiles4": {
        "total": "53090",
        "ok": "53090",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-19174": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117339",
path: "Animal-api sync for customer NL_117339",
pathFormatted: "req_animal-api-sync-19174",
stats: {
    "name": "Animal-api sync for customer NL_117339",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "15077",
        "ok": "15077",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "15077",
        "ok": "15077",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "15077",
        "ok": "15077",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "15077",
        "ok": "15077",
        "ko": "-"
    },
    "percentiles2": {
        "total": "15077",
        "ok": "15077",
        "ko": "-"
    },
    "percentiles3": {
        "total": "15077",
        "ok": "15077",
        "ko": "-"
    },
    "percentiles4": {
        "total": "15077",
        "ok": "15077",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-9a47c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_115141",
path: "Animal-api sync for customer NL_115141",
pathFormatted: "req_animal-api-sync-9a47c",
stats: {
    "name": "Animal-api sync for customer NL_115141",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "maxResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "meanResponseTime": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "percentiles4": {
        "total": "60001",
        "ok": "-",
        "ko": "60001"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-0a9fa": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_125735",
path: "Animal-api sync for customer NL_125735",
pathFormatted: "req_animal-api-sync-0a9fa",
stats: {
    "name": "Animal-api sync for customer NL_125735",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "16366",
        "ok": "16366",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "16366",
        "ok": "16366",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "16366",
        "ok": "16366",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "16366",
        "ok": "16366",
        "ko": "-"
    },
    "percentiles2": {
        "total": "16366",
        "ok": "16366",
        "ko": "-"
    },
    "percentiles3": {
        "total": "16366",
        "ok": "16366",
        "ko": "-"
    },
    "percentiles4": {
        "total": "16366",
        "ok": "16366",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-56dad": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_112805",
path: "Animal-api sync for customer NL_112805",
pathFormatted: "req_animal-api-sync-56dad",
stats: {
    "name": "Animal-api sync for customer NL_112805",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "26140",
        "ok": "26140",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "26140",
        "ok": "26140",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "26140",
        "ok": "26140",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "26140",
        "ok": "26140",
        "ko": "-"
    },
    "percentiles2": {
        "total": "26140",
        "ok": "26140",
        "ko": "-"
    },
    "percentiles3": {
        "total": "26140",
        "ok": "26140",
        "ko": "-"
    },
    "percentiles4": {
        "total": "26140",
        "ok": "26140",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-4c3a5": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110894",
path: "Animal-api sync for customer NL_110894",
pathFormatted: "req_animal-api-sync-4c3a5",
stats: {
    "name": "Animal-api sync for customer NL_110894",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "57635",
        "ok": "57635",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "57635",
        "ok": "57635",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "57635",
        "ok": "57635",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "57635",
        "ok": "57635",
        "ko": "-"
    },
    "percentiles2": {
        "total": "57635",
        "ok": "57635",
        "ko": "-"
    },
    "percentiles3": {
        "total": "57635",
        "ok": "57635",
        "ko": "-"
    },
    "percentiles4": {
        "total": "57635",
        "ok": "57635",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-86c49": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_145826",
path: "Animal-api sync for customer BE_145826",
pathFormatted: "req_animal-api-sync-86c49",
stats: {
    "name": "Animal-api sync for customer BE_145826",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "25655",
        "ok": "25655",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "25655",
        "ok": "25655",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "25655",
        "ok": "25655",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "25655",
        "ok": "25655",
        "ko": "-"
    },
    "percentiles2": {
        "total": "25655",
        "ok": "25655",
        "ko": "-"
    },
    "percentiles3": {
        "total": "25655",
        "ok": "25655",
        "ko": "-"
    },
    "percentiles4": {
        "total": "25655",
        "ok": "25655",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-9e7ff": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110428",
path: "Animal-api sync for customer NL_110428",
pathFormatted: "req_animal-api-sync-9e7ff",
stats: {
    "name": "Animal-api sync for customer NL_110428",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "maxResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "meanResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles2": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles3": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles4": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-6c072": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_115279",
path: "Animal-api sync for customer NL_115279",
pathFormatted: "req_animal-api-sync-6c072",
stats: {
    "name": "Animal-api sync for customer NL_115279",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "maxResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "meanResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles2": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles3": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles4": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-2de80": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_129343",
path: "Animal-api sync for customer NL_129343",
pathFormatted: "req_animal-api-sync-2de80",
stats: {
    "name": "Animal-api sync for customer NL_129343",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "40354",
        "ok": "40354",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "40354",
        "ok": "40354",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "40354",
        "ok": "40354",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "40354",
        "ok": "40354",
        "ko": "-"
    },
    "percentiles2": {
        "total": "40354",
        "ok": "40354",
        "ko": "-"
    },
    "percentiles3": {
        "total": "40354",
        "ok": "40354",
        "ko": "-"
    },
    "percentiles4": {
        "total": "40354",
        "ok": "40354",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-925cc": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_120686",
path: "Animal-api sync for customer NL_120686",
pathFormatted: "req_animal-api-sync-925cc",
stats: {
    "name": "Animal-api sync for customer NL_120686",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "maxResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "meanResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles2": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles3": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles4": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-f6a28": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_129516",
path: "Animal-api sync for customer NL_129516",
pathFormatted: "req_animal-api-sync-f6a28",
stats: {
    "name": "Animal-api sync for customer NL_129516",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "maxResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "meanResponseTime": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles2": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles3": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "percentiles4": {
        "total": "60002",
        "ok": "-",
        "ko": "60002"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-10907": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_155615",
path: "Animal-api sync for customer NL_155615",
pathFormatted: "req_animal-api-sync-10907",
stats: {
    "name": "Animal-api sync for customer NL_155615",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "maxResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "meanResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles2": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles3": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles4": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-83fad": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_161486",
path: "Animal-api sync for customer NL_161486",
pathFormatted: "req_animal-api-sync-83fad",
stats: {
    "name": "Animal-api sync for customer NL_161486",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "51786",
        "ok": "51786",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "51786",
        "ok": "51786",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "51786",
        "ok": "51786",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "51786",
        "ok": "51786",
        "ko": "-"
    },
    "percentiles2": {
        "total": "51786",
        "ok": "51786",
        "ko": "-"
    },
    "percentiles3": {
        "total": "51786",
        "ok": "51786",
        "ko": "-"
    },
    "percentiles4": {
        "total": "51786",
        "ok": "51786",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-6eeed": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_136560",
path: "Animal-api sync for customer NL_136560",
pathFormatted: "req_animal-api-sync-6eeed",
stats: {
    "name": "Animal-api sync for customer NL_136560",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "27927",
        "ok": "27927",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "27927",
        "ok": "27927",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "27927",
        "ok": "27927",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "27927",
        "ok": "27927",
        "ko": "-"
    },
    "percentiles2": {
        "total": "27927",
        "ok": "27927",
        "ko": "-"
    },
    "percentiles3": {
        "total": "27927",
        "ok": "27927",
        "ko": "-"
    },
    "percentiles4": {
        "total": "27927",
        "ok": "27927",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-678da": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_116304",
path: "Animal-api sync for customer NL_116304",
pathFormatted: "req_animal-api-sync-678da",
stats: {
    "name": "Animal-api sync for customer NL_116304",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "48190",
        "ok": "48190",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "48190",
        "ok": "48190",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "48190",
        "ok": "48190",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "48190",
        "ok": "48190",
        "ko": "-"
    },
    "percentiles2": {
        "total": "48190",
        "ok": "48190",
        "ko": "-"
    },
    "percentiles3": {
        "total": "48190",
        "ok": "48190",
        "ko": "-"
    },
    "percentiles4": {
        "total": "48190",
        "ok": "48190",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-23e9e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_136624",
path: "Animal-api sync for customer NL_136624",
pathFormatted: "req_animal-api-sync-23e9e",
stats: {
    "name": "Animal-api sync for customer NL_136624",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "32127",
        "ok": "32127",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "32127",
        "ok": "32127",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "32127",
        "ok": "32127",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "32127",
        "ok": "32127",
        "ko": "-"
    },
    "percentiles2": {
        "total": "32127",
        "ok": "32127",
        "ko": "-"
    },
    "percentiles3": {
        "total": "32127",
        "ok": "32127",
        "ko": "-"
    },
    "percentiles4": {
        "total": "32127",
        "ok": "32127",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-b45fa": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_160578",
path: "Animal-api sync for customer BE_160578",
pathFormatted: "req_animal-api-sync-b45fa",
stats: {
    "name": "Animal-api sync for customer BE_160578",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "maxResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "meanResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles2": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles3": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles4": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-ed8de": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117329",
path: "Animal-api sync for customer NL_117329",
pathFormatted: "req_animal-api-sync-ed8de",
stats: {
    "name": "Animal-api sync for customer NL_117329",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "maxResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "meanResponseTime": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles2": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles3": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "percentiles4": {
        "total": "60003",
        "ok": "-",
        "ko": "60003"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-7699b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_115653",
path: "Animal-api sync for customer NL_115653",
pathFormatted: "req_animal-api-sync-7699b",
stats: {
    "name": "Animal-api sync for customer NL_115653",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "25018",
        "ok": "25018",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "25018",
        "ok": "25018",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "25018",
        "ok": "25018",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "25018",
        "ok": "25018",
        "ko": "-"
    },
    "percentiles2": {
        "total": "25018",
        "ok": "25018",
        "ko": "-"
    },
    "percentiles3": {
        "total": "25018",
        "ok": "25018",
        "ko": "-"
    },
    "percentiles4": {
        "total": "25018",
        "ok": "25018",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-f6750": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_145660",
path: "Animal-api sync for customer BE_145660",
pathFormatted: "req_animal-api-sync-f6750",
stats: {
    "name": "Animal-api sync for customer BE_145660",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "maxResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "meanResponseTime": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles2": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles3": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "percentiles4": {
        "total": "60000",
        "ok": "-",
        "ko": "60000"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-de4e1": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_196422",
path: "Animal-api sync for customer BE_196422",
pathFormatted: "req_animal-api-sync-de4e1",
stats: {
    "name": "Animal-api sync for customer BE_196422",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "11915",
        "ok": "11915",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "11915",
        "ok": "11915",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "11915",
        "ok": "11915",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "11915",
        "ok": "11915",
        "ko": "-"
    },
    "percentiles2": {
        "total": "11915",
        "ok": "11915",
        "ko": "-"
    },
    "percentiles3": {
        "total": "11915",
        "ok": "11915",
        "ko": "-"
    },
    "percentiles4": {
        "total": "11915",
        "ok": "11915",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-94458": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111031",
path: "Animal-api sync for customer NL_111031",
pathFormatted: "req_animal-api-sync-94458",
stats: {
    "name": "Animal-api sync for customer NL_111031",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "36926",
        "ok": "36926",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "36926",
        "ok": "36926",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "36926",
        "ok": "36926",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "36926",
        "ok": "36926",
        "ko": "-"
    },
    "percentiles2": {
        "total": "36926",
        "ok": "36926",
        "ko": "-"
    },
    "percentiles3": {
        "total": "36926",
        "ok": "36926",
        "ko": "-"
    },
    "percentiles4": {
        "total": "36926",
        "ok": "36926",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-1114b": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105737",
path: "Animal-api sync for customer NL_105737",
pathFormatted: "req_animal-api-sync-1114b",
stats: {
    "name": "Animal-api sync for customer NL_105737",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "43148",
        "ok": "43148",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "43148",
        "ok": "43148",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "43148",
        "ok": "43148",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "43148",
        "ok": "43148",
        "ko": "-"
    },
    "percentiles2": {
        "total": "43148",
        "ok": "43148",
        "ko": "-"
    },
    "percentiles3": {
        "total": "43148",
        "ok": "43148",
        "ko": "-"
    },
    "percentiles4": {
        "total": "43148",
        "ok": "43148",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-34261": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_112642",
path: "Animal-api sync for customer NL_112642",
pathFormatted: "req_animal-api-sync-34261",
stats: {
    "name": "Animal-api sync for customer NL_112642",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "52660",
        "ok": "52660",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "52660",
        "ok": "52660",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "52660",
        "ok": "52660",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "52660",
        "ok": "52660",
        "ko": "-"
    },
    "percentiles2": {
        "total": "52660",
        "ok": "52660",
        "ko": "-"
    },
    "percentiles3": {
        "total": "52660",
        "ok": "52660",
        "ko": "-"
    },
    "percentiles4": {
        "total": "52660",
        "ok": "52660",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-88106": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_112559",
path: "Animal-api sync for customer NL_112559",
pathFormatted: "req_animal-api-sync-88106",
stats: {
    "name": "Animal-api sync for customer NL_112559",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "36926",
        "ok": "36926",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "36926",
        "ok": "36926",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "36926",
        "ok": "36926",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "36926",
        "ok": "36926",
        "ko": "-"
    },
    "percentiles2": {
        "total": "36926",
        "ok": "36926",
        "ko": "-"
    },
    "percentiles3": {
        "total": "36926",
        "ok": "36926",
        "ko": "-"
    },
    "percentiles4": {
        "total": "36926",
        "ok": "36926",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-12cf4": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_103806",
path: "Animal-api sync for customer NL_103806",
pathFormatted: "req_animal-api-sync-12cf4",
stats: {
    "name": "Animal-api sync for customer NL_103806",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "32245",
        "ok": "32245",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "32245",
        "ok": "32245",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "32245",
        "ok": "32245",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "32245",
        "ok": "32245",
        "ko": "-"
    },
    "percentiles2": {
        "total": "32245",
        "ok": "32245",
        "ko": "-"
    },
    "percentiles3": {
        "total": "32245",
        "ok": "32245",
        "ko": "-"
    },
    "percentiles4": {
        "total": "32245",
        "ok": "32245",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-0c8bf": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_135056",
path: "Animal-api sync for customer NL_135056",
pathFormatted: "req_animal-api-sync-0c8bf",
stats: {
    "name": "Animal-api sync for customer NL_135056",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "34889",
        "ok": "34889",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "34889",
        "ok": "34889",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "34889",
        "ok": "34889",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "34889",
        "ok": "34889",
        "ko": "-"
    },
    "percentiles2": {
        "total": "34889",
        "ok": "34889",
        "ko": "-"
    },
    "percentiles3": {
        "total": "34889",
        "ok": "34889",
        "ko": "-"
    },
    "percentiles4": {
        "total": "34889",
        "ok": "34889",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-cd315": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_115640",
path: "Animal-api sync for customer NL_115640",
pathFormatted: "req_animal-api-sync-cd315",
stats: {
    "name": "Animal-api sync for customer NL_115640",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "26253",
        "ok": "26253",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "26253",
        "ok": "26253",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "26253",
        "ok": "26253",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "26253",
        "ok": "26253",
        "ko": "-"
    },
    "percentiles2": {
        "total": "26253",
        "ok": "26253",
        "ko": "-"
    },
    "percentiles3": {
        "total": "26253",
        "ok": "26253",
        "ko": "-"
    },
    "percentiles4": {
        "total": "26253",
        "ok": "26253",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-708ff": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111792",
path: "Animal-api sync for customer NL_111792",
pathFormatted: "req_animal-api-sync-708ff",
stats: {
    "name": "Animal-api sync for customer NL_111792",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "57705",
        "ok": "57705",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "57705",
        "ok": "57705",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "57705",
        "ok": "57705",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "57705",
        "ok": "57705",
        "ko": "-"
    },
    "percentiles2": {
        "total": "57705",
        "ok": "57705",
        "ko": "-"
    },
    "percentiles3": {
        "total": "57705",
        "ok": "57705",
        "ko": "-"
    },
    "percentiles4": {
        "total": "57705",
        "ok": "57705",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c74f5": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110479",
path: "Animal-api sync for customer NL_110479",
pathFormatted: "req_animal-api-sync-c74f5",
stats: {
    "name": "Animal-api sync for customer NL_110479",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "maxResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "meanResponseTime": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles2": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles3": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "percentiles4": {
        "total": "60005",
        "ok": "-",
        "ko": "60005"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-b4519": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_110383",
path: "Animal-api sync for customer NL_110383",
pathFormatted: "req_animal-api-sync-b4519",
stats: {
    "name": "Animal-api sync for customer NL_110383",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "29733",
        "ok": "29733",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "29733",
        "ok": "29733",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "29733",
        "ok": "29733",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "29733",
        "ok": "29733",
        "ko": "-"
    },
    "percentiles2": {
        "total": "29733",
        "ok": "29733",
        "ko": "-"
    },
    "percentiles3": {
        "total": "29733",
        "ok": "29733",
        "ko": "-"
    },
    "percentiles4": {
        "total": "29733",
        "ok": "29733",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-ccec3": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_159045",
path: "Animal-api sync for customer BE_159045",
pathFormatted: "req_animal-api-sync-ccec3",
stats: {
    "name": "Animal-api sync for customer BE_159045",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "36775",
        "ok": "36775",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "36775",
        "ok": "36775",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "36775",
        "ok": "36775",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "36775",
        "ok": "36775",
        "ko": "-"
    },
    "percentiles2": {
        "total": "36775",
        "ok": "36775",
        "ko": "-"
    },
    "percentiles3": {
        "total": "36775",
        "ok": "36775",
        "ko": "-"
    },
    "percentiles4": {
        "total": "36775",
        "ok": "36775",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-f73d7": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_112171",
path: "Animal-api sync for customer NL_112171",
pathFormatted: "req_animal-api-sync-f73d7",
stats: {
    "name": "Animal-api sync for customer NL_112171",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "54117",
        "ok": "54117",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "54117",
        "ok": "54117",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "54117",
        "ok": "54117",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "54117",
        "ok": "54117",
        "ko": "-"
    },
    "percentiles2": {
        "total": "54117",
        "ok": "54117",
        "ko": "-"
    },
    "percentiles3": {
        "total": "54117",
        "ok": "54117",
        "ko": "-"
    },
    "percentiles4": {
        "total": "54117",
        "ok": "54117",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c61a4": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_158630",
path: "Animal-api sync for customer NL_158630",
pathFormatted: "req_animal-api-sync-c61a4",
stats: {
    "name": "Animal-api sync for customer NL_158630",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "54503",
        "ok": "54503",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "54503",
        "ok": "54503",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "54503",
        "ok": "54503",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "54503",
        "ok": "54503",
        "ko": "-"
    },
    "percentiles2": {
        "total": "54503",
        "ok": "54503",
        "ko": "-"
    },
    "percentiles3": {
        "total": "54503",
        "ok": "54503",
        "ko": "-"
    },
    "percentiles4": {
        "total": "54503",
        "ok": "54503",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-3df21": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111890",
path: "Animal-api sync for customer NL_111890",
pathFormatted: "req_animal-api-sync-3df21",
stats: {
    "name": "Animal-api sync for customer NL_111890",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "56439",
        "ok": "56439",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "56439",
        "ok": "56439",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "56439",
        "ok": "56439",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "56439",
        "ok": "56439",
        "ko": "-"
    },
    "percentiles2": {
        "total": "56439",
        "ok": "56439",
        "ko": "-"
    },
    "percentiles3": {
        "total": "56439",
        "ok": "56439",
        "ko": "-"
    },
    "percentiles4": {
        "total": "56439",
        "ok": "56439",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-11d0e": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_119978",
path: "Animal-api sync for customer NL_119978",
pathFormatted: "req_animal-api-sync-11d0e",
stats: {
    "name": "Animal-api sync for customer NL_119978",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "59995",
        "ok": "59995",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "59995",
        "ok": "59995",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "59995",
        "ok": "59995",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "59995",
        "ok": "59995",
        "ko": "-"
    },
    "percentiles2": {
        "total": "59995",
        "ok": "59995",
        "ko": "-"
    },
    "percentiles3": {
        "total": "59995",
        "ok": "59995",
        "ko": "-"
    },
    "percentiles4": {
        "total": "59995",
        "ok": "59995",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-cd0f8": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117391",
path: "Animal-api sync for customer NL_117391",
pathFormatted: "req_animal-api-sync-cd0f8",
stats: {
    "name": "Animal-api sync for customer NL_117391",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "38049",
        "ok": "38049",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "38049",
        "ok": "38049",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "38049",
        "ok": "38049",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "38049",
        "ok": "38049",
        "ko": "-"
    },
    "percentiles2": {
        "total": "38049",
        "ok": "38049",
        "ko": "-"
    },
    "percentiles3": {
        "total": "38049",
        "ok": "38049",
        "ko": "-"
    },
    "percentiles4": {
        "total": "38049",
        "ok": "38049",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-997ea": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_111277",
path: "Animal-api sync for customer NL_111277",
pathFormatted: "req_animal-api-sync-997ea",
stats: {
    "name": "Animal-api sync for customer NL_111277",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "48208",
        "ok": "48208",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "48208",
        "ok": "48208",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "48208",
        "ok": "48208",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "48208",
        "ok": "48208",
        "ko": "-"
    },
    "percentiles2": {
        "total": "48208",
        "ok": "48208",
        "ko": "-"
    },
    "percentiles3": {
        "total": "48208",
        "ok": "48208",
        "ko": "-"
    },
    "percentiles4": {
        "total": "48208",
        "ok": "48208",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-998da": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_120046",
path: "Animal-api sync for customer NL_120046",
pathFormatted: "req_animal-api-sync-998da",
stats: {
    "name": "Animal-api sync for customer NL_120046",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "16379",
        "ok": "16379",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "16379",
        "ok": "16379",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "16379",
        "ok": "16379",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "16379",
        "ok": "16379",
        "ko": "-"
    },
    "percentiles2": {
        "total": "16379",
        "ok": "16379",
        "ko": "-"
    },
    "percentiles3": {
        "total": "16379",
        "ok": "16379",
        "ko": "-"
    },
    "percentiles4": {
        "total": "16379",
        "ok": "16379",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-08b09": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_122753",
path: "Animal-api sync for customer NL_122753",
pathFormatted: "req_animal-api-sync-08b09",
stats: {
    "name": "Animal-api sync for customer NL_122753",
    "numberOfRequests": {
        "total": "1",
        "ok": "0",
        "ko": "1"
    },
    "minResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "maxResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "meanResponseTime": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "-",
        "ko": "0"
    },
    "percentiles1": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles2": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles3": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "percentiles4": {
        "total": "60004",
        "ok": "-",
        "ko": "60004"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "-",
        "ko": "0.002"
    }
}
    },"req_animal-api-sync-a5635": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_193658",
path: "Animal-api sync for customer BE_193658",
pathFormatted: "req_animal-api-sync-a5635",
stats: {
    "name": "Animal-api sync for customer BE_193658",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "34440",
        "ok": "34440",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "34440",
        "ok": "34440",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "34440",
        "ok": "34440",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "34440",
        "ok": "34440",
        "ko": "-"
    },
    "percentiles2": {
        "total": "34440",
        "ok": "34440",
        "ko": "-"
    },
    "percentiles3": {
        "total": "34440",
        "ok": "34440",
        "ko": "-"
    },
    "percentiles4": {
        "total": "34440",
        "ok": "34440",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-f89f8": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_121091",
path: "Animal-api sync for customer NL_121091",
pathFormatted: "req_animal-api-sync-f89f8",
stats: {
    "name": "Animal-api sync for customer NL_121091",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "13156",
        "ok": "13156",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "13156",
        "ok": "13156",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "13156",
        "ok": "13156",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "13156",
        "ok": "13156",
        "ko": "-"
    },
    "percentiles2": {
        "total": "13156",
        "ok": "13156",
        "ko": "-"
    },
    "percentiles3": {
        "total": "13156",
        "ok": "13156",
        "ko": "-"
    },
    "percentiles4": {
        "total": "13156",
        "ok": "13156",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-6c82d": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_143962",
path: "Animal-api sync for customer NL_143962",
pathFormatted: "req_animal-api-sync-6c82d",
stats: {
    "name": "Animal-api sync for customer NL_143962",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "22523",
        "ok": "22523",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "22523",
        "ok": "22523",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "22523",
        "ok": "22523",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "22523",
        "ok": "22523",
        "ko": "-"
    },
    "percentiles2": {
        "total": "22523",
        "ok": "22523",
        "ko": "-"
    },
    "percentiles3": {
        "total": "22523",
        "ok": "22523",
        "ko": "-"
    },
    "percentiles4": {
        "total": "22523",
        "ok": "22523",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-afe85": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_104819",
path: "Animal-api sync for customer NL_104819",
pathFormatted: "req_animal-api-sync-afe85",
stats: {
    "name": "Animal-api sync for customer NL_104819",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "41793",
        "ok": "41793",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "41793",
        "ok": "41793",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "41793",
        "ok": "41793",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "41793",
        "ok": "41793",
        "ko": "-"
    },
    "percentiles2": {
        "total": "41793",
        "ok": "41793",
        "ko": "-"
    },
    "percentiles3": {
        "total": "41793",
        "ok": "41793",
        "ko": "-"
    },
    "percentiles4": {
        "total": "41793",
        "ok": "41793",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d9f9d": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_156439",
path: "Animal-api sync for customer BE_156439",
pathFormatted: "req_animal-api-sync-d9f9d",
stats: {
    "name": "Animal-api sync for customer BE_156439",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "37601",
        "ok": "37601",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "37601",
        "ok": "37601",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "37601",
        "ok": "37601",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "37601",
        "ok": "37601",
        "ko": "-"
    },
    "percentiles2": {
        "total": "37601",
        "ok": "37601",
        "ko": "-"
    },
    "percentiles3": {
        "total": "37601",
        "ok": "37601",
        "ko": "-"
    },
    "percentiles4": {
        "total": "37601",
        "ok": "37601",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-038e6": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_117080",
path: "Animal-api sync for customer NL_117080",
pathFormatted: "req_animal-api-sync-038e6",
stats: {
    "name": "Animal-api sync for customer NL_117080",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "38467",
        "ok": "38467",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "38467",
        "ok": "38467",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "38467",
        "ok": "38467",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "38467",
        "ok": "38467",
        "ko": "-"
    },
    "percentiles2": {
        "total": "38467",
        "ok": "38467",
        "ko": "-"
    },
    "percentiles3": {
        "total": "38467",
        "ok": "38467",
        "ko": "-"
    },
    "percentiles4": {
        "total": "38467",
        "ok": "38467",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-9a2c1": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_142403",
path: "Animal-api sync for customer NL_142403",
pathFormatted: "req_animal-api-sync-9a2c1",
stats: {
    "name": "Animal-api sync for customer NL_142403",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "49327",
        "ok": "49327",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "49327",
        "ok": "49327",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "49327",
        "ok": "49327",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "49327",
        "ok": "49327",
        "ko": "-"
    },
    "percentiles2": {
        "total": "49327",
        "ok": "49327",
        "ko": "-"
    },
    "percentiles3": {
        "total": "49327",
        "ok": "49327",
        "ko": "-"
    },
    "percentiles4": {
        "total": "49327",
        "ok": "49327",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e1749": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_121066",
path: "Animal-api sync for customer NL_121066",
pathFormatted: "req_animal-api-sync-e1749",
stats: {
    "name": "Animal-api sync for customer NL_121066",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "28623",
        "ok": "28623",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "28623",
        "ok": "28623",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "28623",
        "ok": "28623",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "28623",
        "ok": "28623",
        "ko": "-"
    },
    "percentiles2": {
        "total": "28623",
        "ok": "28623",
        "ko": "-"
    },
    "percentiles3": {
        "total": "28623",
        "ok": "28623",
        "ko": "-"
    },
    "percentiles4": {
        "total": "28623",
        "ok": "28623",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c09d9": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_126693",
path: "Animal-api sync for customer NL_126693",
pathFormatted: "req_animal-api-sync-c09d9",
stats: {
    "name": "Animal-api sync for customer NL_126693",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "41942",
        "ok": "41942",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "41942",
        "ok": "41942",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "41942",
        "ok": "41942",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "41942",
        "ok": "41942",
        "ko": "-"
    },
    "percentiles2": {
        "total": "41942",
        "ok": "41942",
        "ko": "-"
    },
    "percentiles3": {
        "total": "41942",
        "ok": "41942",
        "ko": "-"
    },
    "percentiles4": {
        "total": "41942",
        "ok": "41942",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-cdc3c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_125737",
path: "Animal-api sync for customer NL_125737",
pathFormatted: "req_animal-api-sync-cdc3c",
stats: {
    "name": "Animal-api sync for customer NL_125737",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "41983",
        "ok": "41983",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "41983",
        "ok": "41983",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "41983",
        "ok": "41983",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "41983",
        "ok": "41983",
        "ko": "-"
    },
    "percentiles2": {
        "total": "41983",
        "ok": "41983",
        "ko": "-"
    },
    "percentiles3": {
        "total": "41983",
        "ok": "41983",
        "ko": "-"
    },
    "percentiles4": {
        "total": "41983",
        "ok": "41983",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-339e7": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_115945",
path: "Animal-api sync for customer NL_115945",
pathFormatted: "req_animal-api-sync-339e7",
stats: {
    "name": "Animal-api sync for customer NL_115945",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "13266",
        "ok": "13266",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "13266",
        "ok": "13266",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "13266",
        "ok": "13266",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "13266",
        "ok": "13266",
        "ko": "-"
    },
    "percentiles2": {
        "total": "13266",
        "ok": "13266",
        "ko": "-"
    },
    "percentiles3": {
        "total": "13266",
        "ok": "13266",
        "ko": "-"
    },
    "percentiles4": {
        "total": "13266",
        "ok": "13266",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c1773": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_105858",
path: "Animal-api sync for customer NL_105858",
pathFormatted: "req_animal-api-sync-c1773",
stats: {
    "name": "Animal-api sync for customer NL_105858",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "48869",
        "ok": "48869",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "48869",
        "ok": "48869",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "48869",
        "ok": "48869",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "48869",
        "ok": "48869",
        "ko": "-"
    },
    "percentiles2": {
        "total": "48869",
        "ok": "48869",
        "ko": "-"
    },
    "percentiles3": {
        "total": "48869",
        "ok": "48869",
        "ko": "-"
    },
    "percentiles4": {
        "total": "48869",
        "ok": "48869",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-c41a4": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_118516",
path: "Animal-api sync for customer NL_118516",
pathFormatted: "req_animal-api-sync-c41a4",
stats: {
    "name": "Animal-api sync for customer NL_118516",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "18168",
        "ok": "18168",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "18168",
        "ok": "18168",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "18168",
        "ok": "18168",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "18168",
        "ok": "18168",
        "ko": "-"
    },
    "percentiles2": {
        "total": "18168",
        "ok": "18168",
        "ko": "-"
    },
    "percentiles3": {
        "total": "18168",
        "ok": "18168",
        "ko": "-"
    },
    "percentiles4": {
        "total": "18168",
        "ok": "18168",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-e8c3a": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_154725",
path: "Animal-api sync for customer BE_154725",
pathFormatted: "req_animal-api-sync-e8c3a",
stats: {
    "name": "Animal-api sync for customer BE_154725",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "25489",
        "ok": "25489",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "25489",
        "ok": "25489",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "25489",
        "ok": "25489",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "25489",
        "ok": "25489",
        "ko": "-"
    },
    "percentiles2": {
        "total": "25489",
        "ok": "25489",
        "ko": "-"
    },
    "percentiles3": {
        "total": "25489",
        "ok": "25489",
        "ko": "-"
    },
    "percentiles4": {
        "total": "25489",
        "ok": "25489",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-fc13a": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_154710",
path: "Animal-api sync for customer BE_154710",
pathFormatted: "req_animal-api-sync-fc13a",
stats: {
    "name": "Animal-api sync for customer BE_154710",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "33483",
        "ok": "33483",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "33483",
        "ok": "33483",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "33483",
        "ok": "33483",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "33483",
        "ok": "33483",
        "ko": "-"
    },
    "percentiles2": {
        "total": "33483",
        "ok": "33483",
        "ko": "-"
    },
    "percentiles3": {
        "total": "33483",
        "ok": "33483",
        "ko": "-"
    },
    "percentiles4": {
        "total": "33483",
        "ok": "33483",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-d0d7d": {
        type: "REQUEST",
        name: "Animal-api sync for customer BE_147835",
path: "Animal-api sync for customer BE_147835",
pathFormatted: "req_animal-api-sync-d0d7d",
stats: {
    "name": "Animal-api sync for customer BE_147835",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "23895",
        "ok": "23895",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "23895",
        "ok": "23895",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "23895",
        "ok": "23895",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "23895",
        "ok": "23895",
        "ko": "-"
    },
    "percentiles2": {
        "total": "23895",
        "ok": "23895",
        "ko": "-"
    },
    "percentiles3": {
        "total": "23895",
        "ok": "23895",
        "ko": "-"
    },
    "percentiles4": {
        "total": "23895",
        "ok": "23895",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    },"req_animal-api-sync-f852c": {
        type: "REQUEST",
        name: "Animal-api sync for customer NL_129269",
path: "Animal-api sync for customer NL_129269",
pathFormatted: "req_animal-api-sync-f852c",
stats: {
    "name": "Animal-api sync for customer NL_129269",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "29684",
        "ok": "29684",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "29684",
        "ok": "29684",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "29684",
        "ok": "29684",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "29684",
        "ok": "29684",
        "ko": "-"
    },
    "percentiles2": {
        "total": "29684",
        "ok": "29684",
        "ko": "-"
    },
    "percentiles3": {
        "total": "29684",
        "ok": "29684",
        "ko": "-"
    },
    "percentiles4": {
        "total": "29684",
        "ok": "29684",
        "ko": "-"
    },
    "group1": {
    "name": "t < 500 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "500 ms < t < 1000 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1000 ms",
    "count": 1,
    "percentage": 100
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.002",
        "ok": "0.002",
        "ko": "-"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
